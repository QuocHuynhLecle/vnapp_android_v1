package com.aveapp.wave_master;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.data.db.DbHelper;
import com.aveapp.wave_master.data.db.IDbHelper;
import com.aveapp.wave_master.data.network.ApiHelper;
import com.aveapp.wave_master.data.network.IApiHelper;
import com.aveapp.wave_master.data.prefs.ISharedPrefsHelper;
import com.aveapp.wave_master.data.prefs.SharedPrefsHelper;
import com.aveapp.wave_master.utils.Utils;
import com.aveapp.wave_master.utils.auth.kakao.KakaoSDKAdapter;
import com.aveapp.wave_master.utils.constant.AppConstants;
import com.kakao.auth.KakaoSDK;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/29/18
 * Time: 3:11 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class WAVEApp extends Application {

    private static volatile WAVEApp instance = null;

    public static WAVEApp getInstance() {
        return instance;
    }

    private DataManager mDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initialize();
    }

    /**
     * Init library/module/... for app
     */
    private void initialize() {
        // KakaoTalk
        KakaoSDK.init(new KakaoSDKAdapter());

        // Android Networking
        AndroidNetworking.initialize(getApplicationContext());

        // Utils initialization
        Utils.init(this);

        // Init Data Manager
        ISharedPrefsHelper sharedPrefsHelper = new SharedPrefsHelper(getApplicationContext(), AppConstants.PREF_NAME);
        IDbHelper dbHelper = new DbHelper(getApplicationContext());
        IApiHelper apiHelper = new ApiHelper(getApplicationContext());
        mDataManager = new DataManager(getApplicationContext(), dbHelper, sharedPrefsHelper, apiHelper);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }

    /**
     * Get Data Manager
     *
     * @return DataManager
     */
    public DataManager getDataManager() {
        return mDataManager;
    }
}
