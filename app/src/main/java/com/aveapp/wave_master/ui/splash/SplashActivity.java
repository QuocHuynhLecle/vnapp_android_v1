package com.aveapp.wave_master.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.aveapp.wave_master.R;
import com.aveapp.wave_master.WAVEApp;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.ui.WaveMessengerActivity;
import com.aveapp.wave_master.ui.login.LoginSocialActivity;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/28/18
 * Time: 10:12 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SplashActivity extends AppCompatActivity implements SplashMvpView {

    private SplashPresenter<SplashMvpView> mSplashPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManager dataManager = ((WAVEApp) getApplication()).getDataManager();

        mSplashPresenter = new SplashPresenter<>(dataManager);

        mSplashPresenter.onAttach(this);

        mSplashPresenter.decideNextActivity();

        // testing ....
        //startActivity(new Intent(this, ProfileSettingActivity.class));
        //finish();
        //openLoginActivity();
        //testing....
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void openMainActivity(String userId, String userName, String userPassword, String userIdKey) {
        Intent intent = new Intent(this, WaveMessengerActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        intent.putExtra("userPassword", userPassword);
        intent.putExtra("userIdKey", userIdKey);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginSocialActivity.class));
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}
