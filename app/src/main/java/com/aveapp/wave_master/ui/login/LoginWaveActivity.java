package com.aveapp.wave_master.ui.login;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.aveapp.wave_master.R;
import com.aveapp.wave_master.WAVEApp;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.ui.WaveMessengerActivity;
import com.aveapp.wave_master.utils.ValidationUtils;
import com.aveapp.wave_master.utils.auth.AuthManager;
import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.aveapp.wave_master.utils.auth.wave.WaveNetwork;
import com.aveapp.wave_master.utils.widget.ClearableEditText;
import com.wave.messenger.activity.Activity_FindId;
import com.wave.messenger.activity.Activity_FindPassword;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 9:26 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class LoginWaveActivity extends AppCompatActivity {

    private EditText mEdtPassword;
    private ClearableEditText mEdtEmail;
    private TextView mErrorEmail;

    private DataManager mDataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_wave);

        mDataManager = ((WAVEApp) getApplication()).getDataManager();
        initView();
    }

    private void initView() {
        TextView mMembership = findViewById(R.id.tv_membership);
        mMembership.setPaintFlags(mMembership.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mMembership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoJoinWave();
            }
        });

        mErrorEmail = findViewById(R.id.tv_error_email);

        mEdtEmail = findViewById(R.id.edt_email);
        mEdtPassword = findViewById(R.id.edt_password);

        mEdtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (ValidationUtils.isValidEmail(charSequence.toString())) {
                    mErrorEmail.setVisibility(View.INVISIBLE);
                } else {
                    mErrorEmail.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.bt_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputValidation();
            }
        });

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoLoginSocial();
            }
        });

        findViewById(R.id.bt_findId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoFindId();
            }
        });

        findViewById(R.id.bt_findPwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoFindPassword();
            }
        });
    }

    private void inputValidation() {
        String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        if (ValidationUtils.isValidEmail(email) && !password.isEmpty()) {
            loginWave(email, password);
        } else {
            showErrorDialog();
        }
    }

    private void gotoLoginSocial() {
        finish();
    }

    private void gotoJoinWave() {
        startActivity(new Intent(this, JoinWaveActivity.class));
    }

    private void loginWave(final String email, final String password) {
        AuthManager.getInstance().login(
                this,
                new WaveNetwork(mDataManager, email, password, ""),
                new SocialLoginCallback() {
                    @Override
                    public void onLoginSuccess(SocialNetwork socialNetwork, SocialData data) {
                        // Login success -> go to main screens
                        // ToastUtils.showShort("Login success!!! -> go to main screen.");
                        gotoMainScreen(data.getUserId(), data.getUserName(), password, mDataManager.getCurrentUserIdKey());
                    }

                    @Override
                    public void onLoginError(SocialNetwork socialNetwork, SocialLoginError error) {
                        // Show error - dialog
                        showErrorDialog();
                    }
                });
    }

    private void gotoMainScreen(String userId, String userName, String userPassword, String userIdKey) {
        Intent intent = new Intent(this, WaveMessengerActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        intent.putExtra("userPassword", userPassword);
        intent.putExtra("userIdKey", userIdKey);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void gotoFindId() {
        Intent goFindId = new Intent(this, Activity_FindId.class);
        startActivity(goFindId);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void gotoFindPassword() {
        Intent goFindPassword = new Intent(this, Activity_FindPassword.class);
        startActivity(goFindPassword);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void showErrorDialog() {
        new MaterialDialog.Builder(this)
                .content(R.string.textErrorLogin)
                .positiveText(R.string.textErrorInputAgain)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .negativeText(R.string.textMembership)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        gotoJoinWave();
                        dialog.dismiss();
                    }
                })
                .autoDismiss(true)
                .show();
    }
}
