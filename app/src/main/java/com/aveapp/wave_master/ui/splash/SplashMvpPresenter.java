package com.aveapp.wave_master.ui.splash;

import com.aveapp.wave_master.ui.base.MvpPresenter;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 10:55 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {
    void decideNextActivity();
}
