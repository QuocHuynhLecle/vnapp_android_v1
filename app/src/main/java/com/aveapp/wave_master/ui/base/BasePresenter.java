package com.aveapp.wave_master.ui.base;

import com.aveapp.wave_master.data.DataManager;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 3:16 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    DataManager mDataManager;
    private V mMvpView;


    public BasePresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public DataManager getDataManager() {
        return mDataManager;
    }
}
