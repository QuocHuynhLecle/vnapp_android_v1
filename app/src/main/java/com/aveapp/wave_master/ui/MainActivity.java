package com.aveapp.wave_master.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.aveapp.wave_master.R;
import com.aveapp.wave_master.ui.login.LoginWaveActivity;
import com.aveapp.wave_master.utils.auth.AuthManager;
import com.aveapp.wave_master.utils.auth.facebook.FacebookNetwork;
import com.aveapp.wave_master.utils.auth.google.GoogleNetwork;
import com.aveapp.wave_master.utils.auth.kakao.KakaoNetwork;
import com.aveapp.wave_master.utils.auth.line.LineNetwork;
import com.aveapp.wave_master.utils.auth.naver.NaverNetwork;
import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.wave.messenger.Info.DeviceInfo;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.Info.UserInfo;
import com.wave.messenger.activity.Activity_FindId;
import com.wave.messenger.activity.Activity_FindPassword;
import com.wave.messenger.activity.Activity_Join;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BUSMOAData;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

public class MainActivity extends AppCompatActivity implements SocialLoginCallback, MOACommonListener, MOAEventListener {

    private String pushData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_social);

        initView();
        setEvent();
    }

    private void initView() {

    }

    private void setEvent() {

    }//end of setEvent

    /**
     * Main 화면으로 이동 (전 세팅)
     * 20180502
     */
    private void mainStart(String userId, String userPassword) {
        UserInfo user = new UserInfo();
        user.setUserId(userId);
        user.setPassword(userPassword);
        user.setUserName(userId);
        user.setUserType("UT_ME_JU");
        user.setPhoneNumber("");
        user.setPushKey("");
        user.setEmpNo("");
        MessengerInterface.getInstance().HanaToMessengerLogin(MainActivity.this, user);
    }

    public void initMOAClient() {
        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);

        MOAClient.getInstance().setProperties("userId", MessengerInfo.getUserId(getBaseContext()));
        MOAClient.getInstance().setProperties("userPassword", MessengerInfo.getUserPw(getBaseContext()));
        MOAClient.getInstance().setProperties("tel", MessengerInfo.getUserPhoneNumber(getBaseContext()));
        MOAClient.getInstance().setProperties("userName", MessengerInfo.getUserName(getBaseContext()));
        MOAClient.getInstance().setProperties("candleUserType", MessengerInfo.getUserType());
        MOAClient.getInstance().setProperties("riskType", String.valueOf(MessengerInfo.getRiskType()));
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken", MessengerInfo.getUserTocken(getBaseContext()));
        MOAClient.getInstance().setProperties("branchName", MessengerInfo.getBranchName());
        MOAClient.getInstance().setProperties("login_ver", "1");
        MOAClient.getInstance().setProperties("agree", "1"); // 약관 동의 값을 최초 1회만 보내면됩니다
        MOAClient.getInstance().login();

        MOAClient.getInstance().addCommonListener(this);
        MOAClient.getInstance().addEventListener(this);
    }

    private void doLoginAction() {
        mainStart("id", "password");
        initMOAClient();
    }

    public void login(View view) {
        switch (view.getId()) {
            case R.id.bt_login_naver:
                AuthManager.getInstance().login(this, new NaverNetwork(), this);
                break;
            case R.id.bt_login_kakaotalk:
                AuthManager.getInstance().login(this, new KakaoNetwork(), this);
                break;
            case R.id.bt_login_line:
                AuthManager.getInstance().login(this, new LineNetwork(), this);
                break;
            case R.id.bt_login_facebook:
                AuthManager.getInstance().login(this, new FacebookNetwork(), this);
                break;
            case R.id.bt_login_google:
                AuthManager.getInstance().login(this, new GoogleNetwork(), this);
                break;
            case R.id.bt_login_wave:
                // TODO: login using server of wave (id, pw)
                // start new login activity here .....
                startActivity(new Intent(this, LoginWaveActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onLoginSuccess(SocialNetwork socialNetwork, SocialData data) {

    }

    @Override
    public void onLoginError(SocialNetwork socialNetwork, SocialLoginError error) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AuthManager.getInstance().onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void OnEvent(MOAData moaData) {
        mMainActvtHandler.sendMessage(mMainActvtHandler.obtainMessage(moaData.ptc, moaData));
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                final MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_LOGIN_LOGIN:
                        String result = data.body.get("result").toString();

                        switch (result) {
                            case "wrong password":
                                Toast.makeText(MainActivity.this, getString(R.string.toastWarningWrongPassword), Toast.LENGTH_SHORT).show();
                                closeMOAClient();
                                break;

                            case "user not found":
                                Toast.makeText(MainActivity.this, getString(R.string.toastWarningIdNotFound), Toast.LENGTH_SHORT).show();
                                closeMOAClient();
                                break;

                            case "success":
                                goMain();
                                break;

                            default:
                                Toast.makeText(MainActivity.this, getString(R.string.toastWarningLoginFiled), Toast.LENGTH_SHORT).show();
                                closeMOAClient();
                                break;
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    private void goFindId() {
        Intent goFindId = new Intent(this, Activity_FindId.class);
        startActivity(goFindId);
    }

    private void goFindPassword() {
        Intent goFindPassword = new Intent(this, Activity_FindPassword.class);
        startActivity(goFindPassword);
    }

    private void join() {
        Intent goJoin = new Intent(this, Activity_Join.class);
        startActivity(goJoin);
    }

    private void goMain() {
        Fragment_Main fragmentMain = new Fragment_Main();
        FragmentManager fm = MainActivity.this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.waveMainLayout, fragmentMain);
        fragmentTransaction.commit();

        MOAClient.getInstance().removeCommonListener(this);
        MOAClient.getInstance().removeEventListener(this);
    }

    private void closeMOAClient() {
        MOAClient.getInstance().logout();   // 로그아웃 호출하여 소켓 연결 해제
        MOAClient.getInstance().removeCommonListener(this);
        MOAClient.getInstance().removeEventListener(this);
    }

    @Override
    public void onMOALogin(MOAData moaData) {
        MOAClient.getInstance().removeCommonListener(this);
        BUSMOAData.getInstantce().setmMOAData(moaData);
    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {

    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {
        MOAClient.getInstance().removeCommonListener(this);
        BUSMOAData.getInstantce().setmMOAData(moaData);
    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {

    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {

    }
}
