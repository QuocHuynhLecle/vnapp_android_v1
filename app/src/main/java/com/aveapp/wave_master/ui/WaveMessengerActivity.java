package com.aveapp.wave_master.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.aveapp.wave_master.R;
import com.wave.messenger.fragment.Fragment_Main;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 1:48 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class WaveMessengerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave_messenger);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            String userId = args.getString("userId");
            String userName = args.getString("userName");
            String userPassword = args.getString("userPassword");
            String userIdKey = args.getString("userIdKey");
            boolean isUseDefaultProfileImg = args.getBoolean("isUseDefaultProfileImg", false);
            setFragment(userId, userName, userPassword, userIdKey, isUseDefaultProfileImg);
        }
    }

    private void setFragment(String userId, String userName, String userPassword, String userIdKey, boolean isUseDefaultProfileImg) {
        Fragment_Main fragmentMain = Fragment_Main.newInstance(userId, userName, userPassword, userIdKey, isUseDefaultProfileImg);
        FragmentManager fm = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.waveMainLayout, fragmentMain);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
