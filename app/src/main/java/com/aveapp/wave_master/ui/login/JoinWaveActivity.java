package com.aveapp.wave_master.ui.login;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.aveapp.wave_master.R;
import com.aveapp.wave_master.WAVEApp;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.utils.ToastUtils;
import com.aveapp.wave_master.utils.ValidationUtils;
import com.aveapp.wave_master.utils.auth.AuthManager;
import com.aveapp.wave_master.utils.auth.wave.WaveNetwork;
import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.aveapp.wave_master.utils.widget.ClearableEditText;
import com.aveapp.wave_master.utils.widget.iconswitch.IconSwitch;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 3:44 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class JoinWaveActivity extends AppCompatActivity implements SocialLoginCallback {

    private TextView mErrorEmail, mErrorPassword;
    private ClearableEditText mEdtEmail;
    private EditText mEdtPassword, mEdtName;

    private DataManager mDataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_wave);
        mDataManager = ((WAVEApp) getApplication()).getDataManager();
        initView();
    }

    private void initView() {
        // Email & Password error textview
        mErrorEmail = findViewById(R.id.tv_error_email);
        mErrorPassword = findViewById(R.id.tv_error_password);

        // Check duplication button
        findViewById(R.id.btn_duplication_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Check duplication
                if (mErrorEmail.getVisibility() == View.VISIBLE) {
                    ToastUtils.showShort(R.string.textErrorEmail);
                } else {
                    // checking duplication email
                    findUser(mEdtEmail.getText().toString());
                }
            }
        });

        // Close button
        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Email input
        mEdtEmail = findViewById(R.id.edt_email);
        mEdtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (ValidationUtils.isValidEmail(charSequence.toString())) {
                    mErrorEmail.setVisibility(View.INVISIBLE);
                } else {
                    mErrorEmail.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // Password Input
        mEdtPassword = findViewById(R.id.edt_password);
        mEdtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (ValidationUtils.isValidPassword(charSequence.toString())) {
                    mErrorPassword.setVisibility(View.INVISIBLE);
                } else {
                    mErrorPassword.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mEdtName = findViewById(R.id.edt_name);

        IconSwitch mSwGender = findViewById(R.id.sw_gender);
        //mSwGender.setActiveTintIconLeft(Color.parseColor("#00F8FC"));
        mSwGender.setThumbColorLeft(Color.parseColor("#00F8FC"));
        // mSwGender.setInactiveTintIconLeft(Color.parseColor("#00F8FC"));

        //mSwGender.setActiveTintIconRight(Color.parseColor("#FB4D7A"));
        mSwGender.setThumbColorRight(Color.parseColor("#FB4D7A"));
        // mSwGender.setInactiveTintIconRight(Color.parseColor("#FB4D7A"));
    }

    private void findUser(final String email) {
        AuthManager.getInstance().login(this,
                new WaveNetwork(mDataManager, email, "", mEdtName.getText().toString()), this);
    }

    @Override
    public void onLoginSuccess(SocialNetwork socialNetwork, SocialData data) {
        // User exist
        showConfirmDialog(mEdtEmail.getText().toString(), false);
    }

    @Override
    public void onLoginError(SocialNetwork socialNetwork, SocialLoginError error) {
        // User not found or wrong password
        showConfirmDialog(mEdtEmail.getText().toString(), true);
    }

    private void showConfirmDialog(String id, boolean flag) {
        if (flag) {
            String content = id + getString(R.string.textMembershipAvailableId);
            new MaterialDialog.Builder(this)
                    .content(content)
                    .negativeText(R.string.no)
                    .positiveText(R.string.yes)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    })
                    .autoDismiss(true)
                    .cancelable(false)
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .content(R.string.textMembershipUsedId)
                    .positiveText(R.string.confirm)
                    .autoDismiss(true)
                    .cancelable(true)
                    .show();
        }
    }
}
