package com.aveapp.wave_master.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.ImageCrop.Crop;
import com.afollestad.materialdialogs.MaterialDialog;
import com.aveapp.wave_master.R;
import com.aveapp.wave_master.WAVEApp;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.ui.WaveMessengerActivity;
import com.aveapp.wave_master.utils.PathUtils;
import com.aveapp.wave_master.utils.ToastUtils;
import com.aveapp.wave_master.utils.widget.ClearableEditText;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/27/18
 * Time: 9:48 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ProfileSettingActivity extends AppCompatActivity {

    private static final String TAG = ProfileSettingActivity.class.getSimpleName();

    private static final int RC_SELECT_IMG = 9001;
    private static final int RC_CAPTURE_IMG = 9002;

    private static final int PERMISSION_CAMERA = 9003;
    private static final int PERMISSION_PICK_GALLERY = 9004;

    private ClearableEditText mEdtName;
    private CircleImageView mImageProfile;
    private String pictureFilePath = "", mUserId;
    private Button btnComplete;
    private boolean isUseDefaultProfileImg = true;

    private DataManager mDataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            mUserId = args.getString("userId");
        }

        mDataManager = ((WAVEApp) getApplication()).getDataManager();

        initView();
    }

    private void initView() {
        mImageProfile = findViewById(R.id.img_profile);
        mImageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu();
            }
        });

        mEdtName = findViewById(R.id.edt_name);
        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    btnComplete.setEnabled(true);
                } else {
                    btnComplete.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnComplete = findViewById(R.id.btn_complete);
        btnComplete.setEnabled(false);
        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mEdtName.getText().toString();
                if (!username.isEmpty()) {
                    proceedLogin(username);
                }
            }
        });
    }

    private void proceedLogin(String username) {
        updateUserName(username);
        if (isUseDefaultProfileImg) {
            new DeleteProfile(mUserId).execute();
        } else {
            new FileUploader(this, pictureFilePath).execute();
        }
    }

    private void openContextMenu() {
        new MaterialDialog.Builder(this)
                .autoDismiss(true)
                .items(R.array.profile_settings_values)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                checkGalleryPermission();
                                break;
                            case 1:
                                checkCameraPermission();
                                break;
                            case 2:
                                setProfilePicture(null);
                                break;
                        }
                    }
                }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // ToastUtils.showShort("camera permission granted");
                    openCamera();
                } else {
                    ToastUtils.showShort("camera permission denied");
                }
                break;
            case PERMISSION_PICK_GALLERY:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // ToastUtils.showShort("gallery permission granted");
                    openGallery();
                } else {
                    ToastUtils.showShort("gallery permission denied");
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RC_SELECT_IMG:
                    final Uri imageUri = data.getData();

                    if (imageUri != null) {
                        startCropImageActivity(imageUri);
                    }
                    break;
                case RC_CAPTURE_IMG:
                    if (data != null && null != data.getExtras() && null != data.getExtras().get("data")) {
                        mImageProfile.setImageBitmap((Bitmap) data.getExtras().get("data"));
                    } else {
                        if (pictureFilePath != null) {
                            startCropImageActivity(Uri.fromFile(new File(pictureFilePath)));
                        }
                    }
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    setProfilePicture(resultUri);
                    break;
                case Crop.REQUEST_CROP:
                    Uri uri = Crop.getOutput(data);
                    setProfilePicture(uri);
                    break;
            }
        } else {
            ToastUtils.showShort("Failed!!!");
        }
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RC_SELECT_IMG);
    }

    private void checkGalleryPermission() {
        if (Build.VERSION.SDK_INT >= 23 &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
            // Request permisson: gallery
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_PICK_GALLERY);
        } else {
            openGallery();
        }
    }

    private void checkCameraPermission() {
        if (Build.VERSION.SDK_INT >= 23 &&
                checkSelfPermission(Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {
            // Request permisson: camera
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CAMERA);
        } else {
            openCamera();
        }
    }

    private File createTemporaryFile(String part, String ext) throws IOException {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private void openCamera() {
        File photoFile = new File("");
        try {
            photoFile = createTemporaryFile("tmp_img_", ".jpg");
            pictureFilePath = photoFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
        } else {
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(),
                    getApplicationContext().getPackageName() + ".provider",
                    photoFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        startActivityForResult(cameraIntent, RC_CAPTURE_IMG);
    }

    private void setProfilePicture(Uri imgUri) {
        if (null == imgUri) {
            // Set default profile image
            isUseDefaultProfileImg = true;
            Picasso.get().load(R.drawable.ic_profile).noFade().into(mImageProfile);
        } else {
            // Set new profile image
            pictureFilePath = PathUtils.getFilePathByUri(ProfileSettingActivity.this, imgUri);
            isUseDefaultProfileImg = false;
            Picasso.get().load(imgUri).noFade().into(mImageProfile);
        }
    }

    private void updateUserName(String userName) {
        Moa.UsernameChangeRequest(this, userName, new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                try {
                    MOAData data = (MOAData) message.obj;
                    MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                    String strResult;
                    switch (data.ptc) {
                        case PacketTypes.PTC_IMS_USER_CHANGE_USERNAME:
                            strResult = (String) data.body.get("result");
                            LBJSONObject params = data.body.getJson("params");
                            if (strResult.equals(Constants.SUCCESS)) {
                                MessengerInfo.setUsername(getApplicationContext(), params.getString("userName"));
                                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                if (getCurrentFocus() != null && inputMethodManager != null) {
                                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                }
                            }
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }));
    }

    private void gotoMainScreen(String userId, String userName, String userPassword, String userIdKey) {
        Intent intent = new Intent(this, WaveMessengerActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        intent.putExtra("userPassword", userPassword);
        intent.putExtra("userIdKey", userIdKey);
        intent.putExtra("isUseDefaultProfileImg", isUseDefaultProfileImg);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setInitialCropWindowPaddingRatio(0)
                .start(this);
    }

    public void onUploadComplete(boolean isSuccess) {
        if (isSuccess) {
            String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));

            // Toast.makeText(this, com.wave.massenger.piggy.R.string.profile_image_success, Toast.LENGTH_SHORT).show();
            ToastUtils.showShort("Profile success!!!");

            try {
                PacketBody body = new PacketBody();
                JSONObject value = new JSONObject();
                value.put("userId", mUserId);
                value.put("photoType", "1");
                value.put("photoData", time);
                body.put("params", value);
                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_PHOTO, body);
                LoadingManager.with(this).hideLoadingDialog();
                gotoMainScreen(mUserId, mEdtName.getText().toString(), "pw", mDataManager.getCurrentUserIdKey());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // Toast.makeText(this, com.wave.massenger.piggy.R.string.profile_image_fail, Toast.LENGTH_SHORT).show();
            ToastUtils.showShort("Failed to upload!!!!");
            // isChange = false;
        }
        LoadingManager.with(this).hideLoadingDialog();
    }

    private class DeleteProfile extends AsyncTask<Void, Void, String> {

        private String mUserId;

        public DeleteProfile(String userId) {
            mUserId = userId;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String address = Constants.DEL_URL + mUserId;
            ErrorController.showMessage("[ProfileModel] deleteUserPicture : " + address);
            try {
                URL url_obj = new URL(address);
                HttpURLConnection con = (HttpURLConnection) url_obj.openConnection();
                con.setRequestMethod("GET"); // default is GET
                con.setDoInput(true);  // default is true
                con.setDoOutput(true); // default is false
                InputStream in = con.getInputStream();

                InputStreamReader isw = new InputStreamReader(in);

                int data = isw.read();
                String result = "";
                while (data != -1) {
                    char current = (char) data;
                    data = isw.read();
                    result += current;
                }
                ErrorController.showMessage("[ProfileModel] result : " + result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            com.wave.messenger.util.SharedObject.setProperty_string(ProfileSettingActivity.this, Constants.pfImg, "");
            gotoMainScreen(mUserId, mEdtName.getText().toString(), "pw", mDataManager.getCurrentUserIdKey());
        }
    }

    private class FileUploader extends AsyncTask<Void, Void, String> {

        int maxFileSize = 0;
        int currentFileSize = 0;
        private Context mContext;
        private String mFilePath;

        public FileUploader(Context context, String path) {
            mContext = context;
            mFilePath = path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoadingManager.with(mContext).showLoadingDialog();
        }

        @Override
        protected String doInBackground(Void... voids) {
            File file;
            String result = "";
            String boundary = "---------------------------This is the boundary";
            try {
                HttpClient httpclient = new DefaultHttpClient();

                HttpPost httppost = new HttpPost(Constants.PROFILEUPLOAD_PROC);

                CustomMultiPartEntity entity = new CustomMultiPartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE,
                        boundary,
                        Charset.forName("UTF-8"),
                        new CustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                                try {
                                    currentFileSize = (int) num;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                entity.addPart("userId", new StringBody(mDataManager.getCurrentUserIdKey()));
                currentFileSize = 0;
                file = new File(mFilePath);
                maxFileSize = (int) file.length();
                entity.addPart("file", new FileBody(file));
                httppost.setEntity(entity);
                httppost.setHeader("Accept-Charset", "UTF-8");
                httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();
                while ((sResponse = reader.readLine()) != null) {
                    s = s.append(sResponse);
                }
                result = s.toString();
                ErrorController.showMessage("PROFILESETTING:::::Result : " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null && result.length() > 0) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    LogTrace.E(jsonObject.getString("result"));

                    if (jsonObject.getString("result").equals("success")) {
                        onUploadComplete(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    onUploadComplete(false);
                }
            } else {
                onUploadComplete(false);
            }
        }
    }
}
