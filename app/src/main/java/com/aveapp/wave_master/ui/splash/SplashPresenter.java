package com.aveapp.wave_master.ui.splash;

import android.os.Handler;

import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.ui.base.BasePresenter;
import com.aveapp.wave_master.utils.ActivityUtils;
import com.aveapp.wave_master.utils.auth.AuthManager;
import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.aveapp.wave_master.utils.auth.wave.WaveNetwork;
import com.aveapp.wave_master.utils.constant.AppConstants;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 10:56 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void decideNextActivity() {
        final String userId = getDataManager().getCurrentUserId();
        final String userName = getDataManager().getCurrentUserName();
        final String userPassword = getDataManager().getCurrentUserPassword();
        final String userIdKey = getDataManager().getCurrentUserIdKey();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!"".equalsIgnoreCase(userId)) {
                    AuthManager.getInstance().login(ActivityUtils.getTopActivity(),
                            new WaveNetwork(getDataManager(), userId, userName, userPassword), new SocialLoginCallback() {
                                @Override
                                public void onLoginSuccess(SocialNetwork socialNetwork, SocialData data) {
                                    getMvpView().openMainActivity(userId, userName, userPassword, userIdKey);
                                }

                                @Override
                                public void onLoginError(SocialNetwork socialNetwork, SocialLoginError error) {
                                    getMvpView().openLoginActivity();
                                }
                            });
                } else {
                    getMvpView().openLoginActivity();
                }
            }
        }, AppConstants.DURATION_SPLASH);
    }
}
