package com.aveapp.wave_master.ui.base;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 10:52 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface MvpPresenter<V extends MvpView> {
    void onAttach(V mvpView);
}
