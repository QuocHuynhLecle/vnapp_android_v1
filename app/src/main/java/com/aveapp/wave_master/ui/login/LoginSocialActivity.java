package com.aveapp.wave_master.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.aveapp.wave_master.R;
import com.aveapp.wave_master.WAVEApp;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.ui.WaveMessengerActivity;
import com.aveapp.wave_master.utils.PermissionUtils;
import com.aveapp.wave_master.utils.auth.AuthManager;
import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.aveapp.wave_master.utils.auth.facebook.FacebookNetwork;
import com.aveapp.wave_master.utils.auth.google.GoogleNetwork;
import com.aveapp.wave_master.utils.auth.kakao.KakaoNetwork;
import com.aveapp.wave_master.utils.auth.line.LineNetwork;
import com.aveapp.wave_master.utils.auth.naver.NaverNetwork;
import com.aveapp.wave_master.utils.auth.wave.WaveNetwork;
import com.aveapp.wave_master.utils.constant.PermissionConstants;

import java.util.List;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 11:50 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class LoginSocialActivity extends AppCompatActivity implements SocialLoginCallback {

    private static final String TAG = LoginSocialActivity.class.getSimpleName();

    private DataManager mDataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_social);
        mDataManager = ((WAVEApp) getApplication()).getDataManager();

        requestAppPermissions();
    }

    public void login(View view) {
        switch (view.getId()) {
            case R.id.bt_login_naver:
                AuthManager.getInstance().login(this, new NaverNetwork(), this);
                break;
            case R.id.bt_login_kakaotalk:
                AuthManager.getInstance().login(this, new KakaoNetwork(), this);
                break;
            case R.id.bt_login_line:
                AuthManager.getInstance().login(this, new LineNetwork(), this);
                break;
            case R.id.bt_login_facebook:
                AuthManager.getInstance().login(this, new FacebookNetwork(), this);
                break;
            case R.id.bt_login_google:
                AuthManager.getInstance().login(this, new GoogleNetwork(), this);
                break;
            case R.id.bt_login_wave:
                startActivity(new Intent(this, LoginWaveActivity.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult:::" + requestCode);
        AuthManager.getInstance().onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onLoginSuccess(SocialNetwork socialNetwork, SocialData data) {
        Log.i(TAG, "SocialNetwork: SOCIAL_DATA_LOGIN:::::" + data.toString());
        if (socialNetwork instanceof WaveNetwork) {
            gotoMainScreen(data.getUserId(), data.getUserName(), "", mDataManager.getCurrentUserIdKey());
        } else {
            // Handle login data
            handleLoginResult(data);
        }
    }

    @Override
    public void onLoginError(SocialNetwork socialNetwork, SocialLoginError error) {
        Log.e(TAG, "SOCIAL_DATA_LOGIN_ERROR:::::" + error.getMessage());
        if (socialNetwork instanceof WaveNetwork) {
            // TODO: Check API protocol of WAVE again to implement later ...
            switch (error.getMessage()) {
                case "not set profile":
                    gotoProfileSettingScreen("", "", "");
                    break;
                case "not register":
                    AuthManager.getInstance().register();
                    gotoProfileSettingScreen("", "", "");
                    break;
                default:
                    gotoMainScreen("", "", "", mDataManager.getCurrentUserIdKey());
            }
        }
    }

    /**
     * Handle the result of login social network
     *
     * @param data login returned data
     */
    private void handleLoginResult(SocialData data) {
        // Try Login to login WAVE -> if login success -> go to main screen
        AuthManager.getInstance().login(this, new WaveNetwork(mDataManager, data.getUserId(), "pw", data.getUserName()), null);
        gotoProfileSettingScreen(data.getUserId(), data.getUserName(), "pw");
    }

    private void gotoMainScreen(String userId, String userName, String userPassword, String userIdKey) {
        Intent intent = new Intent(this, WaveMessengerActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        intent.putExtra("userPassword", userPassword);
        intent.putExtra("userIdKey", userIdKey);
        startActivity(intent);
    }

    private void gotoProfileSettingScreen(String userId, String userName, String userPassword) {
        Intent intent = new Intent(this, ProfileSettingActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("userName", userName);
        intent.putExtra("userPassword", userPassword);
        startActivity(intent);
    }

    /**
     * Request app permissions
     */
    private void requestAppPermissions() {
        PermissionUtils.permission(PermissionConstants.SMS, PermissionConstants.PHONE)
                .rationale(new PermissionUtils.OnRationaleListener() {
                    @Override
                    public void rationale(ShouldRequest shouldRequest) {
                        PermissionUtils.showRationaleDialog(shouldRequest);
                    }
                })
                .callback(new PermissionUtils.FullCallback() {
                    @Override
                    public void onGranted(List<String> permissionsGranted) {
                        // Do something here when permission is granted...
                    }

                    @Override
                    public void onDenied(List<String> permissionsDeniedForever, List<String> permissionsDenied) {
                        if (!permissionsDeniedForever.isEmpty()) {
                            PermissionUtils.showOpenAppSettingDialog();
                        }
                    }
                })
                .request();
    }
}
