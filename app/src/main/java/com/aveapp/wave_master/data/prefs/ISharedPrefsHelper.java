package com.aveapp.wave_master.data.prefs;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 11:05 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface ISharedPrefsHelper {
    void clearSharedPrefs();

    String getCurrentUserIdKey();

    void setCurrentUserIdKey(String userIdKey);

    Boolean getLoggedInMode();

    String getCurrentUserId();

    String getCurrentUserName();

    String getCurrentUserPassword();
}
