package com.aveapp.wave_master.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.wave.messenger.Info.MessengerInfo;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 11:05 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SharedPrefsHelper implements ISharedPrefsHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_ID_KEY = "PREF_KEY_CURRENT_USER_ID_KEY";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";

    private final SharedPreferences mPrefs;
    private final Context mContext;

    public SharedPrefsHelper(Context context, String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        mContext = context;
    }

    @Override
    public void clearSharedPrefs() {
        mPrefs.edit().clear().apply();
    }

    @Override
    public String getCurrentUserIdKey() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_ID_KEY, "");
    }

    @Override
    public void setCurrentUserIdKey(String userIdKey) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID_KEY, userIdKey).apply();
    }

    @Override
    public Boolean getLoggedInMode() {
        return null;
    }

    @Override
    public String getCurrentUserId() {
        return MessengerInfo.getUserId(mContext);
    }

    @Override
    public String getCurrentUserName() {
        return MessengerInfo.getUserName(mContext);
    }

    @Override
    public String getCurrentUserPassword() {
        return MessengerInfo.getUserPw(mContext);
    }
}
