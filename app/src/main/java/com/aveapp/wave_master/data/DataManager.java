package com.aveapp.wave_master.data;

import android.content.Context;

import com.aveapp.wave_master.data.db.IDbHelper;
import com.aveapp.wave_master.data.network.IApiHelper;
import com.aveapp.wave_master.data.prefs.ISharedPrefsHelper;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 2:38 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class DataManager implements IDataManager {

    private final Context mContext;
    private final ISharedPrefsHelper mSharedPrefsHelper;
    private final IDbHelper mDbHelper;
    private final IApiHelper mApiHelper;

    public DataManager(Context context, IDbHelper dbHelper, ISharedPrefsHelper sharedPrefsHelper, IApiHelper apiHelper) {
        mContext = context;
        mSharedPrefsHelper = sharedPrefsHelper;
        mApiHelper = apiHelper;
        mDbHelper = dbHelper;
    }

    @Override
    public void clearSharedPrefs() {
        mSharedPrefsHelper.clearSharedPrefs();
    }

    @Override
    public String getCurrentUserIdKey() {
        return mSharedPrefsHelper.getCurrentUserIdKey();
    }

    @Override
    public void setCurrentUserIdKey(String userIdKey) {
        mSharedPrefsHelper.setCurrentUserIdKey(userIdKey);
    }

    @Override
    public Boolean getLoggedInMode() {
        return mSharedPrefsHelper.getLoggedInMode();
    }

    @Override
    public String getCurrentUserId() {
        return mSharedPrefsHelper.getCurrentUserId();
    }

    @Override
    public String getCurrentUserName() {
        return mSharedPrefsHelper.getCurrentUserName();
    }

    @Override
    public String getCurrentUserPassword() {
        return mSharedPrefsHelper.getCurrentUserPassword();
    }
}
