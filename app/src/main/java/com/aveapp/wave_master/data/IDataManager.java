package com.aveapp.wave_master.data;

import com.aveapp.wave_master.data.db.IDbHelper;
import com.aveapp.wave_master.data.network.IApiHelper;
import com.aveapp.wave_master.data.prefs.ISharedPrefsHelper;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/12/18
 * Time: 5:19 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface IDataManager extends IDbHelper, ISharedPrefsHelper, IApiHelper {

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_NAVER(1),
        LOGGED_IN_MODE_KAKAOTALK(2),
        LOGGED_IN_MODE_LINE(3),
        LOGGED_IN_MODE_FACEBOOK(4),
        LOGGED_IN_MODE_GOOGLE(5),
        LOGGED_IN_MODE_WAVE_SERVER(6);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
