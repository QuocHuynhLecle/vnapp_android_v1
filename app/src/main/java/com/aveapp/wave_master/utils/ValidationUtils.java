package com.aveapp.wave_master.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 11:28 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public final class ValidationUtils {

    /**
     * Confirm Password: checks does password matches with confirmation password
     *
     * @param confirmPassword
     * @param password
     * @return
     */
    public static boolean isValidConfirmPasswrod(String confirmPassword, String password) {
        if (!confirmPassword.equals(password)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Address: will check if empty or not
     *
     * @param address
     * @return
     */
    public static boolean isValidAddress(String address) {
        if (address == null || address.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * pincode: maximum 6 digits allowed
     *
     * @param pincode
     * @return
     */
    public static boolean isValidPincode(String pincode) {
        if (pincode == null) {
            return false;
        } else {
            String PINCODE_PATTERN = "^[0-9]{6}$";

            Pattern pattern = Pattern.compile(PINCODE_PATTERN);
            Matcher matcher = pattern.matcher(pincode);
            return matcher.matches();
        }
    }

    /**
     * Mobile no: will check phone number empty or not,
     * if filled will check for 10 digits, starting with 7/8/9 numeric.
     *
     * @param mobile
     * @return
     */
    public static boolean isValidMobile(String mobile) {
        Pattern p = Pattern.compile("^[789]\\d{9,9}$");
        if (mobile == null) {
            return false;
        } else {
            Matcher m = p.matcher(mobile);
            return m.matches();
        }
    }

    /**
     * password: length 8-20 char, prevention from sql-injection
     * ("((?!\\s)\\A)(\\s|(?<!\\s)\\S){8,20}\\Z")
     *
     * @param password passsword
     * @return true | false
     */
    public static boolean isValidPassword(String password) {
        // Pattern p = Pattern.compile("((?!\\s)\\A)(\\s|(?<!\\s)\\S){8,20}\\Z");
        // Over 6 characters including alphabet and number
        Pattern p = Pattern.compile("[a-zA-Z0-9]{6,}");
        if (password == null) {
            return false;
        } else {
            Matcher m = p.matcher(password);
            return m.matches();
        }
    }

    /**
     * Email: if email is not in vald format it will throw error.
     *
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    /**
     * Last name: if last name is blank or other than alphabets it will throw error.
     *
     * @param lastName
     * @return
     */
    public static boolean isValidLastName(String lastName) {
        Pattern p = Pattern.compile("^[a-zA-Z]{3,20}$");
        if (lastName == null) {
            return false;
        } else {
            Matcher m = p.matcher(lastName);
            return m.matches();
        }
    }

    /**
     * First name: if first name is blank or other than alphabets it will throw error.
     *
     * @param firstname String
     * @return
     */
    public static boolean isValidFirstName(String firstname) {
        Pattern p = Pattern.compile("^[a-zA-Z]{3,20}$");
        if (firstname == null) {
            return false;
        } else {
            Matcher m = p.matcher(firstname);
            return m.matches();
        }
    }

    /**
     * Age: maximum 3 digits allowed, if empty will throw error.
     *
     * @param age
     * @return
     */
    public static boolean isValidAge(String age) {
        Pattern p = Pattern.compile("^[1-9]{1,3}$");
        if (age == null || age.equals("")) {
            return false;
        } else {
            Matcher m = p.matcher(age);
            return m.matches();
        }
    }

    /**
     * any string: if edit text is empty it will throw error.
     *
     * @param s
     * @return
     */
    public static boolean isEmptyEditText(String s) {
        if (s == null || s.equals("")) {
            return false;
        } else {
            return true;
        }
    }
}
