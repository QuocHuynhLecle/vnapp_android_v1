package com.aveapp.wave_master.utils.auth.base;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface SocialNetwork {

    void login(Activity activity, SocialLoginCallback callback);

    void logout();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
