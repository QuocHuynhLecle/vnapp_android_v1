package com.aveapp.wave_master.utils.auth.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class FacebookNetwork implements SocialNetwork {

    private SocialLoginCallback mLoginCallback;
    private CallbackManager mFBCallbackManager;

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mLoginCallback = callback;

        mFBCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(activity,
                Arrays.asList("public_profile", "email", "user_birthday", "user_photos"));
        LoginManager.getInstance().registerCallback(mFBCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("FacebookNetwork", "Login_Success");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("FacebookNetwork", response.toString());
                                        if (response.getError() != null) {
                                            // handle error
                                            handleError(response.getError().getErrorMessage());
                                        } else {
                                            handleSignInResult(object);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,link,email,picture,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        handleError("LoginFacebook: Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        handleError("LoginFacebook: Exception: " + exception.getMessage());
                    }
                });
    }

    @Override
    public void logout() {
        LoginManager.getInstance().logOut();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mFBCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void handleError(String cause) {
        mLoginCallback.onLoginError(this, new SocialLoginError(cause));
    }

    private void handleSignInResult(JSONObject object) {
        if (object != null) {
            try {
                String id = object.getString("id");
                String name = object.getString("name");
                String email = object.getString("email");
                String photoUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                String birthday = object.getString("birthday"); // mm/dd/yyyy format
                mLoginCallback.onLoginSuccess(this,
                        new SocialData("", id, name, email, birthday, "", photoUrl));
            } catch (JSONException e) {
                e.printStackTrace();
                handleError(e.getMessage());
            }
        }
    }
}
