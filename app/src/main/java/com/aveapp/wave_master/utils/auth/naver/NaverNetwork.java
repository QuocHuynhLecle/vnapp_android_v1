package com.aveapp.wave_master.utils.auth.naver;

import android.app.Activity;
import android.content.Intent;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.aveapp.wave_master.utils.constant.AppConstants;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;

import java.lang.ref.WeakReference;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/26/18
 * Time: 12:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class NaverNetwork implements SocialNetwork {

    public static String API_REQUEST_PROFILE = "https://openapi.naver.com/v1/nid/me";

    private WeakReference<Activity> mContext;
    private SocialLoginCallback mLoginCallback;

    private final String OAUTH_CLIENT_NAME = "네이버 아이디로 로그인";

    private OAuthLogin mOAuthLoginInstance;

    private OAuthLoginHandler mOAuthLoginHandler;

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mLoginCallback = callback;
        mContext = new WeakReference<>(activity);

        mOAuthLoginInstance = OAuthLogin.getInstance();
        mOAuthLoginInstance.showDevelopersLog(true);
        mOAuthLoginInstance.init(mContext.get(),
                AppConstants.NAVER_CLIENT_ID,
                AppConstants.NAVER_CLIENT_SECRET,
                OAUTH_CLIENT_NAME);

        mOAuthLoginHandler = new NaverOAuthLoginHandler(mContext.get(),
                mOAuthLoginInstance,
                new NaverLoginCallback() {
                    @Override
                    public void onNaverLoginSuccess(SocialData data) {
                        handleCallback(data, "");
                    }

                    @Override
                    public void onNaverLoginError(String error) {
                        handleCallback(null, error);
                    }
                });
        mOAuthLoginInstance.startOauthLoginActivity(mContext.get(), mOAuthLoginHandler);
    }

    @Override
    public void logout() {
        mOAuthLoginHandler = null;
        mOAuthLoginInstance.logoutAndDeleteToken(mContext.get());
        mOAuthLoginInstance = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    /**
     * Handle the callback for requesting network
     *
     * @param data  login social data
     * @param error error message
     */
    private void handleCallback(SocialData data, String error) {
        if (mLoginCallback != null) {
            if (data != null) {
                mLoginCallback.onLoginSuccess(this, data);
            } else {
                mLoginCallback.onLoginError(this, new SocialLoginError(error));
            }
        }
    }
}
