package com.aveapp.wave_master.utils.auth.line;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.linecorp.linesdk.LineCredential;
import com.linecorp.linesdk.LineProfile;
import com.linecorp.linesdk.auth.LineLoginApi;
import com.linecorp.linesdk.auth.LineLoginResult;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class LineNetwork implements SocialNetwork {

    private static final String TAG = LineNetwork.class.getSimpleName();

    private static final int RC_SIGN_IN = 8002;
    private static final String LINE_CHANNEL_ID = "1590698187";
    private SocialLoginCallback mLoginCallback;

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mLoginCallback = callback;
        // Start Login screen
        Intent loginIntent = LineLoginApi.getLoginIntent(activity, LINE_CHANNEL_ID);
        activity.startActivityForResult(loginIntent, RC_SIGN_IN);
    }

    @Override
    public void logout() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != RC_SIGN_IN) {
            mLoginCallback.onLoginError(this,
                    new SocialLoginError("Unsupported Request"));
        } else {
            handleLoginResult(data);
        }
    }

    private void handleLoginResult(Intent data) {
        LineLoginResult result = LineLoginApi.getLoginResultFromIntent(data);

        switch (result.getResponseCode()) {
            case SUCCESS:
                // Login successful
                LineProfile profile = result.getLineProfile();
                if (profile != null) {
                    String accessToken = "";
                    LineCredential credential = result.getLineCredential();
                    if (credential != null) {
                        accessToken = credential.getAccessToken().getAccessToken();
                    }
                    String id = profile.getUserId();
                    String name = profile.getDisplayName();
                    String photoUrl = getString(profile.getPictureUrl());

                    Log.i(TAG, "LineNetwork:::" + id + "---" + name + "---" + photoUrl + "---"
                            + profile.getStatusMessage());

                    mLoginCallback.onLoginSuccess(this, new SocialData(
                            accessToken,
                            id,
                            name,
                            "",
                            "",
                            "",
                            photoUrl
                    ));
                } else {
                    mLoginCallback.onLoginError(this,
                            new SocialLoginError("Cannot parse the LINE Profile!!!"));
                }
                break;

            case CANCEL:
                // Login canceled by user
                mLoginCallback.onLoginError(this,
                        new SocialLoginError("LINE Login: Canceled by user!!"));
                break;

            default:
                // Login canceled due to other error
                mLoginCallback.onLoginError(this,
                        new SocialLoginError("LINE Login: FAILED! " + result.getErrorData().toString()));
        }
    }

    private String getString(Uri uri) {
        if (uri != null) {
            return uri.toString();
        }
        return "";
    }
}
