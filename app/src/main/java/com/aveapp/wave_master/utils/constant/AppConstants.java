package com.aveapp.wave_master.utils.constant;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 12:50 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public final class AppConstants {

    private AppConstants() {
        // This utility class is not publicly instantiable
    }

    public static final String UTF8 = "utf-8";

    public static final String DB_NAME = "wave_app.db";
    public static final String PREF_NAME = "wave_app_pref";

    public static final int DURATION_SPLASH = 500; // splash duration: 0.5 seconds

    public static final String LINE_CHANNEL_ID = "1590698187";
    public static final String LINE_CHANNEL_SECRET = "a835d1da008b856dcbe258207febaac7";

    public static final String GOOGLE_CLIENT_ID = "697481019440cme71cfipsisbu5i7iii43cto4004j9b.apps.googleusercontent.com";

    public static final String KAKAO_APP_KEY = "df70bc72718a38238936534463e5ad1b";

    public static final String FB_APP_ID = "1736923259721750";
    public static final String FB_SECRET_CODE = "afdb858529a20ab7eb2497f9c5b33f95";

    public static final String NAVER_CLIENT_ID = "cgTea2R0vJswJwY2owM_";
    public static final String NAVER_CLIENT_SECRET = "ysV7dMkDy3";
}
