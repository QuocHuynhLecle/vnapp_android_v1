package com.aveapp.wave_master.utils.auth.base;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:55 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SocialData {

    private String token;
    private String userId;
    private String userName;
    private String email;
    private String photoUrl;
    private String birthday;
    private String phoneNumber;

    /**
     * Data class that store user data from social network
     *
     * @param token       authorization token provided by social network api
     * @param userId      user identifier in social network
     * @param userName    user display name
     * @param email       user email
     * @param birthday    user birthday
     * @param phoneNumber user phone number
     * @param photoUrl    user profile image url
     */
    public SocialData(String token, String userId, String userName, String email, String birthday, String phoneNumber, String photoUrl) {
        this.token = token;
        this.userId = userId;
        this.userName = userName;
        this.email = email;
        this.birthday = birthday;
        this.photoUrl = photoUrl;
        this.phoneNumber = phoneNumber;
    }

    public SocialData() {
        token = userId = userName = email = "";
    }

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getToken() {
        return token;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String toString() {
        return "token: " + token +
                "-userId: " + userId +
                "-userName: " + userName +
                "-phoneNumber: " + phoneNumber +
                "-birthday: " + birthday +
                "-email: " + email +
                "-photoUrl: " + photoUrl;
    }
}
