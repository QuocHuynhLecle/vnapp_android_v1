package com.aveapp.wave_master.utils.auth.google;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 2:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class GoogleNetwork implements SocialNetwork {

    private static final String TAG = "GoogleNetwork";

    private static final int RC_SIGN_IN = 8001;

    private GoogleSignInClient mGoogleSignInClient;
    private SocialLoginCallback mLoginCallback;

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mLoginCallback = callback;

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        // setup google sign in
        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        // GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void logout() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, return authenticated account.
            if (account != null) {
                mLoginCallback.onLoginSuccess(this, createSocialData(account));
            } else {
                mLoginCallback.onLoginError(this, new SocialLoginError("Cannot get account"));
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode() + ":" + e.getMessage());
            handleError(e.getLocalizedMessage());
        }
    }

    private SocialData createSocialData(GoogleSignInAccount account) {
        return new SocialData(
                account.getIdToken(),
                account.getId(),
                account.getDisplayName(),
                account.getEmail(),
                "", "",
                getString(account.getPhotoUrl()));
    }

    private void handleError(String cause) {
        mLoginCallback.onLoginError(this, new SocialLoginError(cause));
    }

    private String getString(Uri uri) {
        if (uri != null) {
            return uri.toString();
        }
        return "";
    }
}
