package com.aveapp.wave_master.utils.auth.naver;

import android.app.Activity;

import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;

import java.lang.ref.WeakReference;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 9:59 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class NaverOAuthLoginHandler extends OAuthLoginHandler {

    private WeakReference<Activity> mContext;
    private OAuthLogin mOAuthLoginInstance;
    private NaverLoginCallback mCallback;

    public NaverOAuthLoginHandler(Activity activity, OAuthLogin oAuthLoginInstance, NaverLoginCallback callback) {
        mContext = new WeakReference<>(activity);
        mOAuthLoginInstance = oAuthLoginInstance;
        mCallback = callback;
    }

    @Override
    public void run(boolean success) {
        new NaverRequestApiTask(mContext.get(), mOAuthLoginInstance, mCallback).execute();
    }
}
