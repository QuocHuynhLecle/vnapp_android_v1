package com.aveapp.wave_master.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/11/18
 * Time: 2:32 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public final class PathUtils {

    private PathUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * Get the Android system root directory
     * <pre>path: /system</pre>
     *
     * @return System root directory
     */
    public static String getRootPath() {
        return Environment.getRootDirectory().getAbsolutePath();
    }

    /**
     * Get the data directory
     * <pre>path: /data</pre>
     *
     * @return data directory
     */
    public static String getDataPath() {
        return Environment.getDataDirectory().getAbsolutePath();
    }

    /**
     * Get the cache directory
     * <pre>path: data/cache</pre>
     *
     * @return cache directory
     */
    public static String getIntDownloadCachePath() {
        return Environment.getDownloadCacheDirectory().getAbsolutePath();
    }

    /**
     * Get the cache directory for this app
     * <pre>path: /data/data/package/cache</pre>
     *
     * @return Cache directory for this app
     */
    public static String getAppIntCachePath() {
        return Utils.getApp().getCacheDir().getAbsolutePath();
    }

    /**
     * Get the file directory for this app
     * <pre>path: /data/data/package/files</pre>
     *
     * @return file directory for this app
     */
    public static String getAppIntFilesPath() {
        return Utils.getApp().getFilesDir().getAbsolutePath();
    }

    /**
     * Get the database file directory for this app
     * <p>
     * <pre>path: /data/data/package/databases/name</pre>
     *
     * @param name DB file name
     * @return db file directory
     */
    public static String getAppIntDbPath(String name) {
        return Utils.getApp().getDatabasePath(name).getAbsolutePath();
    }

    /**
     * Get the root directory of Android external storage
     * <pre>path: /storage/emulated/0</pre>
     *
     * @return External storage root directory
     */
    public static String getExtStoragePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    /**
     * Get the alarm ringtone directory
     * <pre>path: /storage/emulated/0/Alarms</pre>
     *
     * @return the alarm ringtone directory
     */
    public static String getExtAlarmsPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS)
                .getAbsolutePath();
    }

    /**
     * Get a catalog of photos and videos taken by the camera
     * <pre>path: /storage/emulated/0/DCIM</pre>
     *
     * @return a catalog of photos and videos taken by the camera
     */
    public static String getExtDcimPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                .getAbsolutePath();
    }

    /**
     * Get the documents directory
     * <pre>path: /storage/emulated/0/Documents</pre>
     *
     * @return the documents directory
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getExtDocumentsPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
                .getAbsolutePath();
    }

    /**
     * Get the download directory
     * <pre>path: /storage/emulated/0/Download</pre>
     *
     * @return the download directory
     */
    public static String getExtDownloadsPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath();
    }

    /**
     * Get the video directory
     * <pre>path: /storage/emulated/0/Movies</pre>
     *
     * @return the video directory
     */
    public static String getExtMoviesPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
                .getAbsolutePath();
    }

    /**
     * Get the music directory
     * <pre>path: /storage/emulated/0/Music</pre>
     *
     * @return the music directory
     */
    public static String getExtMusicPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
                .getAbsolutePath();
    }

    /**
     * Get the prompt directory
     * <pre>path: /storage/emulated/0/Notifications</pre>
     *
     * @return the prompt directory
     */
    public static String getExtNotificationsPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS)
                .getAbsolutePath();
    }

    /**
     * Get image directory
     * <pre>path: /storage/emulated/0/Pictures</pre>
     *
     * @return image directory
     */
    public static String getExtPicturesPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .getAbsolutePath();
    }

    /**
     * Get the Podcasts directory
     * <pre>path: /storage/emulated/0/Podcasts</pre>
     *
     * @return the Podcasts directory
     */
    public static String getExtPodcastsPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS)
                .getAbsolutePath();
    }

    /**
     * Get the ringtone directory
     * <pre>path: /storage/emulated/0/Ringtones</pre>
     *
     * @return the ringtone directory
     */
    public static String getExtRingtonesPath() {
        return Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES)
                .getAbsolutePath();
    }

    /**
     * Get the cache directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/cache</pre>
     *
     * @return the cache directory of this app in external storage
     */
    public static String getAppExtCachePath() {
        return Utils.getApp().getExternalCacheDir().getAbsolutePath();
    }

    /**
     * Get the file directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files</pre>
     *
     * @return the file directory of this app in external storage
     */
    public static String getAppExtFilePath() {
        return Utils.getApp().getExternalFilesDir(null).getAbsolutePath();
    }

    /**
     * Get the alarm ringtone directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Alarms</pre>
     *
     * @return the alarm ringtone directory of this app in external storage
     */
    public static String getAppExtAlarmsPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_ALARMS)
                .getAbsolutePath();
    }

    /**
     * Get the camera catalog of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/DCIM</pre>
     *
     * @return the camera catalog of this app in external storage
     */
    public static String getAppExtDcimPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                .getAbsolutePath();
    }

    /**
     * Get the document directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Documents</pre>
     *
     * @return the document directory of this app in external storage
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getAppExtDocumentsPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
                .getAbsolutePath();
    }

    /**
     * Get the download directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Download</pre>
     *
     * @return the download directory of this app in external storage
     */
    public static String getAppExtDownloadPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath();
    }

    /**
     * Get the video directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Movies</pre>
     *
     * @return the video directory of this app in external storage
     */
    public static String getAppExtMoviesPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_MOVIES)
                .getAbsolutePath();
    }

    /**
     * Get the music catalog of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Music</pre>
     *
     * @return the music catalog of this app in external storage
     */
    public static String getAppExtMusicPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_MUSIC)
                .getAbsolutePath();
    }

    /**
     * Get the sound directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Notifications</pre>
     *
     * @return the sound directory of this app in external storage
     */
    public static String getAppExtNotificationsPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_NOTIFICATIONS)
                .getAbsolutePath();
    }

    /**
     * Get the image directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Pictures</pre>
     *
     * @return the image directory of this app in external storage
     */
    public static String getAppExtPicturesPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                .getAbsolutePath();
    }

    /**
     * Get the Podcasts directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Podcasts</pre>
     *
     * @return the Podcasts directory of this app in external storage
     */
    public static String getAppExtPodcastsPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_PODCASTS)
                .getAbsolutePath();
    }

    /**
     * Get the ringtone directory of this app in external storage
     * <pre>path: /storage/emulated/0/Android/data/package/files/Ringtones</pre>
     *
     * @return the ringtone directory of this app in external storage
     */
    public static String getAppExtRingtonesPath() {
        return Utils.getApp().getExternalFilesDir(Environment.DIRECTORY_RINGTONES)
                .getAbsolutePath();
    }

    /**
     * Get the Obb directory for this app
     * <pre>path: /storage/emulated/0/Android/obb/package</pre>
     * <pre>Generally used to store game data packets</pre>
     *
     * @return Obb directory for this app
     */
    public static String getObbPath() {
        return Utils.getApp().getObbDir().getAbsolutePath();
    }

    public static String getFilePathByUri(Context context, Uri uri) {
        String path = null;
        // 以 file:// 开头的
        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            path = uri.getPath();
            return path;
        }
        // 以 content:// 开头的，比如 content://media/extenral/images/media/17766
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme()) && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.Media.DATA}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    if (columnIndex > -1) {
                        path = cursor.getString(columnIndex);
                    }
                }
                cursor.close();
            }
            return path;
        }
        // 4.4及之后的 是以 content:// 开头的，比如 content://com.android.providers.media.documents/document/image%3A235700
        if (ContentResolver.SCHEME_CONTENT.equals(uri.getScheme()) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (DocumentsContract.isDocumentUri(context, uri)) {
                if (isExternalStorageDocument(uri)) {
                    // ExternalStorageProvider
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        path = Environment.getExternalStorageDirectory() + "/" + split[1];
                        return path;
                    }
                } else if (isDownloadsDocument(uri)) {
                    // DownloadsProvider
                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(id));
                    path = getDataColumn(context, contentUri, null, null);
                    return path;
                } else if (isMediaDocument(uri)) {
                    // MediaProvider
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};
                    path = getDataColumn(context, contentUri, selection, selectionArgs);
                    return path;
                }
            }
        }
        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
