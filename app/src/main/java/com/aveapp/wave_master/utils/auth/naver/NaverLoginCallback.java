package com.aveapp.wave_master.utils.auth.naver;

import com.aveapp.wave_master.utils.auth.base.SocialData;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 9:50 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface NaverLoginCallback {
    void onNaverLoginSuccess(SocialData data);
    void onNaverLoginError(String error);
}
