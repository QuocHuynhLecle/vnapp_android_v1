package com.aveapp.wave_master.utils.auth.kakao;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialLoginError;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.usermgmt.response.model.UserAccount;
import com.kakao.util.OptionalBoolean;
import com.kakao.util.exception.KakaoException;

import java.lang.ref.WeakReference;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class KakaoNetwork implements SocialNetwork {

    private static final String TAG = KakaoNetwork.class.getSimpleName();
    private WeakReference<Activity> mContext;
    private SocialLoginCallback mCallback;

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mContext = new WeakReference<>(activity);
        mCallback = callback;

        Session.getCurrentSession().open(AuthType.KAKAO_ACCOUNT, activity);

        Session.getCurrentSession().addCallback(new ISessionCallback() {
            @Override
            public void onSessionOpened() {
                Log.i(TAG, "Kakao:::onSessionOpened");
                requestUserProfile();
            }

            @Override
            public void onSessionOpenFailed(KakaoException exception) {
                Log.e(TAG, "Kakao:::onSessionOpenFailed:::" + exception.getMessage());
                handleLoginResult(null, exception.getMessage());
            }
        });
    }

    @Override
    public void logout() {
        UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    /**
     * Handle Login result callback
     *
     * @param data  social login data
     * @param error error message
     */
    private void handleLoginResult(SocialData data, String error) {
        if (data != null) {
            mCallback.onLoginSuccess(this, data);
        } else {
            mCallback.onLoginError(this, new SocialLoginError(error));
        }
    }

    private void requestUserProfile() {
        UserManagement.getInstance().me(new MeV2ResponseCallback() {
            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.e(TAG, "KAKAO:::Error:::" + errorResult.getErrorMessage());
            }

            @Override
            public void onSuccess(MeV2Response result) {
                Log.i(TAG, "KAKAO:::AppUserSessionId:::" + result.getId());
                String email = "", phone = "", birthday = "";
                String userId = String.valueOf(result.getId());
                String name = result.getNickname();

                String photoUrl = result.getProfileImagePath();

                UserAccount acc = result.getKakaoAccount();
                if (acc.getEmail() != null && acc.hasEmail() == OptionalBoolean.TRUE) {
                    email = acc.getEmail();
                }
                if (acc.getPhoneNumber() != null && acc.hasPhoneNumber() == OptionalBoolean.TRUE) {
                    phone = acc.getPhoneNumber();
                }
                if (acc.getBirthday() != null && acc.hasBirthday() == OptionalBoolean.TRUE) {
                    birthday = acc.getBirthday();
                }
                handleLoginResult(new SocialData(
                        "",
                        userId,
                        name,
                        email,
                        birthday,
                        phone,
                        photoUrl), "");
            }
        });
    }
}
