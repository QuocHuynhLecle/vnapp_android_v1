package com.aveapp.wave_master.utils;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import static com.aveapp.wave_master.utils.constant.AppConstants.UTF8;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 9:48 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public final class StringUtils {

    private static final String TAG = "StringUtils";

    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    private static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static <T> String join(Collection<T> coll, String separator) {
        return join(coll, separator, null);
    }

    public static String join(Object[] arr, String separator) {
        return join(arr, separator, null);
    }

    private static <T> String join(Collection<T> coll, String separator, String terminator) {
        return join(coll.toArray(new Object[coll.size()]), separator, terminator);
    }

    private static String join(Object[] arr, String separator, String terminator) {
        StringBuilder sb = new StringBuilder(arr.length * 2);
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(separator);
            } else if (terminator != null && arr.length > 0) {
                sb.append(terminator);
            }
        }
        return sb.toString();
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("failed to encode", e);
        }
    }

    public static String urlDecode(String str) {
        try {
            return URLDecoder.decode(str, UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("failed to decode", e);
        }
    }

    public static final String SHA1 = "SHA-1";
    public static final String MD5 = "MD5";

    public static String getMD5(String str) {
        try {
            return getHash(str, MD5, 32);
        } catch (Exception e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static String getSHA1(String str) {
        try {
            return getHash(str, SHA1, 40);
        } catch (Exception e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static String getHash(String str, String algorithm, int length)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytes = str.getBytes(UTF8);
        MessageDigest md = MessageDigest.getInstance(algorithm);
        byte[] digest = md.digest(bytes);
        BigInteger bigInt = new BigInteger(1, digest);
        String hash = bigInt.toString(16);
        while (hash.length() < length) {
            hash = "0" + hash;
        }
        return hash;
    }
}
