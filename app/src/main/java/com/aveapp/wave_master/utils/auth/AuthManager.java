package com.aveapp.wave_master.utils.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class AuthManager {

    private static AuthManager instance = null;
    private SocialNetwork mSocialNetwork;

    private AuthManager() {
    }

    public static synchronized AuthManager getInstance() {
        if (instance == null) {
            instance = new AuthManager();
        }
        return instance;
    }

    public AuthManager login(Activity activity, SocialNetwork socialNetwork, SocialLoginCallback callback) {
        mSocialNetwork = socialNetwork;
        mSocialNetwork.login(activity, callback);
        return this;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSocialNetwork.onActivityResult(requestCode, resultCode, data);
    }

    public void logout() {

    }

    public boolean isLoggedin() {
        // TODO Implement is login or not
        return true;
    }

    public void register() {

    }

    public boolean isRegistered(Context context, String userId) {
        // TODO: implement is registered
        return true;
    }

}
