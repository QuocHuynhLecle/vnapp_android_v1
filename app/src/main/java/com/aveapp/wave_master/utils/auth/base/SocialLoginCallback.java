package com.aveapp.wave_master.utils.auth.base;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface SocialLoginCallback {
    /**
     * Calls when login was successful
     *
     * @param socialNetwork [SocialNetwork] implementation in which login was requested
     * @param data [SocialData] authorization token and some user data
     */
    void onLoginSuccess(SocialNetwork socialNetwork , SocialData data);

    /**
     * Calls when some error occurred
     *
     * @param socialNetwork [SocialNetwork] implementation with which request was unsuccessful
     * @param error [SocialLoginError] error a social login error
     */
    void onLoginError(SocialNetwork socialNetwork, SocialLoginError error);
}
