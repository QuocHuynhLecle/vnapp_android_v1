package com.aveapp.wave_master.utils.auth.base;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 06/25/18
 * Time: 1:54 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SocialLoginError {

    private String errorMessage;

    public SocialLoginError(String message) {
        errorMessage = message;
    }

    public String getMessage() {
        return errorMessage;
    }
}
