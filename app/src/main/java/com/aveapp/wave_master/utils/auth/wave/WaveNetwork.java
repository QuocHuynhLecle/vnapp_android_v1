package com.aveapp.wave_master.utils.auth.wave;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.aveapp.wave_master.R;
import com.aveapp.wave_master.data.DataManager;
import com.aveapp.wave_master.utils.ToastUtils;
import com.aveapp.wave_master.utils.auth.base.SocialLoginCallback;
import com.aveapp.wave_master.utils.auth.base.SocialNetwork;
import com.wave.messenger.Info.DeviceInfo;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.Info.UserInfo;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BUSMOAData;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 6/26/18
 * Time: 10:53 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class WaveNetwork implements SocialNetwork, MOACommonListener, MOAEventListener {

    private String mUserId, mUserPassword, mUserName;
    private SocialLoginCallback mLoginCallback;
    private DataManager mDataManager;

    public WaveNetwork(DataManager dataManager, String userId, String userPassword, String userName) {
        mUserId = userId;
        mUserPassword = userPassword;
        mUserName = userName;
        mDataManager = dataManager;
    }

    @Override
    public void login(Activity activity, SocialLoginCallback callback) {
        mLoginCallback = callback;
        setupUserInfo(activity);
        initMOAClient(activity);
    }

    @Override
    public void logout() {
        closeMOAClient();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    /*******************
     * Login WAVE
     *******************/
    private void setupUserInfo(Activity activity) {
        UserInfo user = new UserInfo();
        user.setUserId(mUserId);
        user.setPassword(mUserPassword);
        if (!"".equalsIgnoreCase(mUserName))
            user.setUserName(mUserName);
        user.setUserType("UT_ME_JU");
        user.setPhoneNumber("");
        user.setPushKey("");
        user.setEmpNo("");
        MessengerInterface.getInstance().HanaToMessengerLogin(activity, user);
    }

    private void initMOAClient(Activity activity) {
        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);

        MOAClient.getInstance().setProperties("userId", MessengerInfo.getUserId(activity.getBaseContext()));
        MOAClient.getInstance().setProperties("userPassword", MessengerInfo.getUserPw(activity.getBaseContext()));
        MOAClient.getInstance().setProperties("tel", MessengerInfo.getUserPhoneNumber(activity.getBaseContext()));
        MOAClient.getInstance().setProperties("userName", MessengerInfo.getUserName(activity.getBaseContext()));
        MOAClient.getInstance().setProperties("candleUserType", MessengerInfo.getUserType());
        MOAClient.getInstance().setProperties("riskType", String.valueOf(MessengerInfo.getRiskType()));
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken", MessengerInfo.getUserTocken(activity.getBaseContext()));
        MOAClient.getInstance().setProperties("branchName", MessengerInfo.getBranchName());
        MOAClient.getInstance().setProperties("login_ver", "1");
        MOAClient.getInstance().setProperties("agree", "1"); // 약관 동의 값을 최초 1회만 보내면됩니다
        MOAClient.getInstance().login();

        MOAClient.getInstance().addCommonListener(this);
        MOAClient.getInstance().addEventListener(this);
    }

    private Handler mLoginWaveHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                final MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_LOGIN_LOGIN:
                        String result = data.body.get("result").toString();

                        switch (result) {
                            case "wrong password":
                                ToastUtils.showShort(R.string.toastWarningWrongPassword);
                                closeMOAClient();
                                break;

                            case "user not found":
                                ToastUtils.showShort(R.string.toastWarningIdNotFound);
                                closeMOAClient();
                                break;

                            case "success":
                                String userIdKey = data.body.getJson("params").getString("userIdKey");
                                mDataManager.setCurrentUserIdKey(userIdKey);
                                loginSuccess();
                                break;

                            default:
                                ToastUtils.showShort(R.string.toastWarningLoginFiled);
                                closeMOAClient();
                                break;
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    private void loginSuccess() {
        MOAClient.getInstance().removeCommonListener(this);
        MOAClient.getInstance().removeEventListener(this);
        mLoginCallback.onLoginSuccess(this, null);
    }

    private void closeMOAClient() {
        // 로그아웃 호출하여 소켓 연결 해제
        MOAClient.getInstance().logout();
        MOAClient.getInstance().removeCommonListener(this);
        MOAClient.getInstance().removeEventListener(this);
    }

    @Override
    public void OnEvent(MOAData moaData) {
        mLoginWaveHandler.sendMessage(mLoginWaveHandler.obtainMessage(moaData.ptc, moaData));
    }

    @Override
    public void onMOALogin(MOAData moaData) {
        MOAClient.getInstance().removeCommonListener(this);
        BUSMOAData.getInstantce().setmMOAData(moaData);
    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {
        MOAClient.getInstance().removeCommonListener(this);
        BUSMOAData.getInstantce().setmMOAData(moaData);
    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {

    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {

    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {

    }
}
