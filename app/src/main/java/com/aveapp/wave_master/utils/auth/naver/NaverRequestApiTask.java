package com.aveapp.wave_master.utils.auth.naver;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aveapp.wave_master.utils.auth.base.SocialData;
import com.nhn.android.naverlogin.OAuthLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * Created by hungnm.
 * User: Hung Nguyen
 * Date: 7/4/18
 * Time: 9:49 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class NaverRequestApiTask extends AsyncTask<Void, Void, String> {

    private OAuthLogin mOAuthLoginInstance;
    private WeakReference<Context> mContext;
    private NaverLoginCallback mCallback;

    public NaverRequestApiTask(Context context, OAuthLogin oAuthLoginInstance, NaverLoginCallback callback) {
        mOAuthLoginInstance = oAuthLoginInstance;
        mContext = new WeakReference<>(context);
        mCallback = callback;
    }

    @Override
    protected String doInBackground(Void... params) {
        return mOAuthLoginInstance.requestApi(mContext.get(),
                mOAuthLoginInstance.getAccessToken(mContext.get()),
                NaverNetwork.API_REQUEST_PROFILE);
    }

    protected void onPostExecute(String content) {
        //TODO: parse NAVER content
        Log.i("NaverNetwork", "NaverNetwork:::" + content);
        handleLoginResult(content);
    }

    private void handleLoginResult(String content) {
        try {
            JSONObject obj = new JSONObject(content);
            if (obj.has("message") && obj.get("message").equals("success")) {
                JSONObject data = obj.getJSONObject("response");
                String id = data.getString("id");
                String email = data.getString("email");
                String name = data.getString("name");
                mCallback.onNaverLoginSuccess(new SocialData(
                        id,
                        id,
                        name,
                        email,
                        "",
                        "",
                        ""
                ));
            } else {
                mCallback.onNaverLoginError("NAVER:::Cannot parse JSON");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            mCallback.onNaverLoginError(e.getMessage());
        }
    }
}
