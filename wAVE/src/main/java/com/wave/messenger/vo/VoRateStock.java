package com.wave.messenger.vo;

public class VoRateStock {

    String itemCode;   // 종목코드
    String itemName;   // 종목명
    String avgPrice;   // 평균가
    String profit;     // 손익률
    String profitVal;  // 평가손익

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(String avgPrice) {
        this.avgPrice = avgPrice;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public String getProfitVal() {
        return profitVal;
    }

    public void setProfitVal(String profitVal) {
        this.profitVal = profitVal;
    }
}
