package com.wave.messenger.vo;

public class VoPortfolioList {

    String m_ItemCode           ; //종목코드
    String m_ItemName			; //종목명
    String m_Purchase           ; //편입가
    String m_PurchasePs         ; //매입비중
    String m_ProfitPs           ; //손익률

    public String getM_ItemCode() {
        return m_ItemCode;
    }

    public void setM_ItemCode(String m_ItemCode) {
        this.m_ItemCode = m_ItemCode;
    }

    public String getM_ItemName() {
        return m_ItemName;
    }

    public void setM_ItemName(String m_ItemName) {
        this.m_ItemName = m_ItemName;
    }

    public String getM_Purchase() {
        return m_Purchase;
    }

    public void setM_Purchase(String m_Purchase) {
        this.m_Purchase = m_Purchase;
    }

    public String getM_PurchasePs() {
        return m_PurchasePs;
    }

    public void setM_PurchasePs(String m_PurchasePs) {
        this.m_PurchasePs = m_PurchasePs;
    }

    public String getM_ProfitPs() {
        return m_ProfitPs;
    }

    public void setM_ProfitPs(String m_ProfitPs) {
        this.m_ProfitPs = m_ProfitPs;
    }
}
