package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 3. 9..
 */


public class VoMoim {
    String title;
    String type="0"; //일반: 0   고정:1
    int nMoimtype;

    public VoMoim(String title, int i) {
        this.title = title;
        this.nMoimtype=i;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getnMoimtype() {
        return nMoimtype;
    }

    public void setnMoimtype(int nMoimtype) {
        this.nMoimtype = nMoimtype;
    }
}