package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 3. 9..
 * 모임 설정
 */

public class VoMoimSet {

    String mmNm;
    String maxUsr;

    String imgTy;
    String imgOrg;
    String imgTmb;
    String intr;
    String qstn;
    String mmPw;


    //공개설정
    String opnTy;
    String entTy;
    //String conTy;
    //String reTy;

    //멤버들의 권한설정
    String covty;
    String acptTy;
    String ivtTy;
    String notiTy;
    String wrtTy;
    String abmTy;
    String rpyTy;
    String delTy;
    String frcTy;

    //채팅 설정 및 메뉴 우선순위 결정
    String mnuTy;
    String opchtTy;
    String snglTy;

    String csltTy;   //종목상담여부(int)0:OFF, 1:ON
    String ldngTy;   //종목리딩여부(int)0:OFF, 1:ON

    public String getCsltTy() {
        return csltTy;
    }

    public void setCsltTy(String csltTy) {
        this.csltTy = csltTy;
    }

    public String getLdngTy() {
        return ldngTy;
    }

    public void setLdngTy(String ldngTy) {
        this.ldngTy = ldngTy;
    }

    public String getImgTy() {
        return imgTy;
    }

    public void setImgTy(String imgTy) {
        this.imgTy = imgTy;
    }

    public String getImgOrg() {
        return imgOrg;
    }

    public void setImgOrg(String imgOrg) {
        this.imgOrg = imgOrg;
    }

    public String getImgTmb() {
        return imgTmb;
    }

    public void setImgTmb(String imgTmb) {
        this.imgTmb = imgTmb;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }

    public String getMmNm() {
        return mmNm;
    }

    public void setMmNm(String mmNm) {
        this.mmNm = mmNm;
    }

    public String getMaxUsr() {
        return maxUsr;
    }

    public void setMaxUsr(String maxUsr) {
        this.maxUsr = maxUsr;
    }

    public String getOpnTy() {
        return opnTy;
    }

    public void setOpnTy(String opnTy) {
        this.opnTy = opnTy;
    }

    public String getEntTy() {
        return entTy;
    }

    public void setEntTy(String entTy) {
        this.entTy = entTy;
    }

    public String getCovty() {
        return covty;
    }

    public void setCovty(String covty) {
        this.covty = covty;
    }

    public String getAcptTy() {
        return acptTy;
    }

    public void setAcptTy(String acptTy) {
        this.acptTy = acptTy;
    }

    public String getIvtTy() {
        return ivtTy;
    }

    public void setIvtTy(String ivtTy) {
        this.ivtTy = ivtTy;
    }

    public String getNotiTy() {
        return notiTy;
    }

    public void setNotiTy(String notiTy) {
        this.notiTy = notiTy;
    }

    public String getWrtTy() {
        return wrtTy;
    }

    public void setWrtTy(String wrtTy) {
        this.wrtTy = wrtTy;
    }

    public String getAbmTy() {
        return abmTy;
    }

    public void setAbmTy(String abmTy) {
        this.abmTy = abmTy;
    }

    public String getRpyTy() {
        return rpyTy;
    }

    public void setRpyTy(String rpyTy) {
        this.rpyTy = rpyTy;
    }

    public String getDelTy() {
        return delTy;
    }

    public void setDelTy(String delTy) {
        this.delTy = delTy;
    }

    public String getFrcTy() {
        return frcTy;
    }

    public void setFrcTy(String frcTy) {
        this.frcTy = frcTy;
    }

    public String getMnuTy() {
        return mnuTy;
    }

    public void setMnuTy(String mnuTy) {
        this.mnuTy = mnuTy;
    }

    public String getOpchtTy() {
        return opchtTy;
    }

    public void setOpchtTy(String opchtTy) {
        this.opchtTy = opchtTy;
    }

    public String getSnglTy() {
        return snglTy;
    }

    public void setSnglTy(String snglTy) {
        this.snglTy = snglTy;
    }

    public String getQstn(){
        return qstn;
    }

    public void setQstn(String qstn) {
        this.qstn = qstn;
    }

    public String getMmPw() {
        return mmPw;
    }

    public void setMmPw(String mmPw) {
        this.mmPw = mmPw;
    }
}
