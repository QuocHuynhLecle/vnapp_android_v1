package com.wave.messenger.vo;

/**
 * Created by apple on 2017. 4. 10..
 */

public class VoNotice {

    String notice;
    String title;

    public VoNotice(String notice, String title) {
        this.notice = notice;
        this.title = title;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
