//
//  UserInfo.java
//  pipe
//
//  Created by 안경종 on 2015. 10. 20..
//  Copyright © 2015년 Misslee. All rights reserved.
//

package com.wave.messenger.vo;

public class VoItemCode {
	String name = "";
	String code = "";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
