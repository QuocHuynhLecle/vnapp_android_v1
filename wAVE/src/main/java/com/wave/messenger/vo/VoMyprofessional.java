package com.wave.messenger.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.util.json.LBJSONObject;

/**
 * Created by apple on 2017. 4. 6..
 * 전문가대화 나의전문가
 */

public class VoMyprofessional extends VoFriendList {

    private String userId;
    private String usNm;
    private String mmId;
    private String rmId;
    private String qstn;
    private String xprtGrpId;
    private String xprtGrpNm;
    private int pro_seq;
    private int xprtTy;     //0:매니저, 1:애널리스트, 2:전문가
    private int mmTg;       //1:주식, 2:국내파생, 3:해외파생, 4:해외주식, 5:금융상품, 6:기타
    private int csltTy;     //0:OFF, 1:ON
    private int ldngTy;     //0:OFF, 1:ON
    private int entTy;      //0:승인, 1:자동가입(기본), 2:비밀번호
    private int mStt;       //0:가입안됨, 1:가입됨, 2:차단, 3:가입대기
    private String mmNm;
    private String intr;
    private String imgTmb;
    private String voice;
    private String branchName;
    private ArrayList<VoMyprofessional> list;

    public VoMyprofessional(String userId, String usNm, String mmId, String rmId, String qstn, int xprtTy, String xprtGrpId, int mmTg, int csltTy, int ldngTy, int entTy, int mStt, String mmNm, String intr, String imgTmb) {
        this.userId = userId;
        this.usNm = usNm;
        this.mmId = mmId;
        this.rmId = rmId;
        this.qstn = qstn;
        this.xprtTy = xprtTy;
        this.xprtGrpId = xprtGrpId;
        this.mmTg = mmTg;
        this.csltTy = csltTy;
        this.ldngTy = ldngTy;
        this.entTy = entTy;
        this.mStt = mStt;
        this.mmNm = mmNm;
        this.intr = intr;
        this.imgTmb = imgTmb;
    }

    public VoMyprofessional() {
        list = new ArrayList<>();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsNm() {
        return usNm;
    }

    public void setUsNm(String usNm) {
        this.usNm = usNm;
    }

    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public String getRmId() {
        return rmId;
    }

    public void setRmId(String rmId) {
        this.rmId = rmId;
    }

    public String getQstn() {
        return qstn;
    }

    public void setQstn(String qstn) {
        this.qstn = qstn;
    }

    public int getPro_seq() {
        return pro_seq;
    }

    public void setPro_seq(int pro_seq) {
        this.pro_seq = pro_seq;
    }

    public int getXprtTy() {
        return xprtTy;
    }

    public void setXprtTy(int xprtTy) {
        this.xprtTy = xprtTy;
    }

    public String getXprtGrpId() {
        return xprtGrpId;
    }

    public void setXprtGrpId(String xprtGrpId) {
        this.xprtGrpId = xprtGrpId;
    }

    public int getMmTg() {
        return mmTg;
    }

    public void setMmTg(int mmTg) {
        this.mmTg = mmTg;
    }

    public int getCsltTy() {
        return csltTy;
    }

    public void setCsltTy(int csltTy) {
        this.csltTy = csltTy;
    }

    public int getLdngTy() {
        return ldngTy;
    }

    public void setLdngTy(int ldngTy) {
        this.ldngTy = ldngTy;
    }

    public int getEntTy() {
        return entTy;
    }

    public void setEntTy(int entTy) {
        this.entTy = entTy;
    }

    public int getmStt() {
        return mStt;
    }

    public void setmStt(int mStt) {
        this.mStt = mStt;
    }

    public String getMmNm() {
        return mmNm;
    }

    public void setMmNm(String mmNm) {
        this.mmNm = mmNm;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }

    public String getImgTmb() {
        return imgTmb;
    }

    public void setImgTmb(String imgTmb) {
        this.imgTmb = imgTmb;
    }

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public ArrayList<VoMyprofessional> getProList() {
        return list;
    }

    public void setProList(ArrayList<VoMyprofessional> list) {
        this.list = list;
        sort();
    }

    public void sort() {
        Collections.sort(list, new NameDescCompare());
    }

    static class NameDescCompare implements Comparator<VoMyprofessional> {
        /**
         * 내림차순(DESC)
         */
        @Override
        public int compare(VoMyprofessional arg0, VoMyprofessional arg1) {
            return arg0.getUsNm().compareTo(arg1.getUsNm());
        }
    }

    static public VoMyprofessional loadFromJsonServerResponse(LBJSONObject jObject) {
        VoMyprofessional result = new VoMyprofessional();
        try {
            result.userId = jObject.getString("userId");
            result.usNm = jObject.getString("usNm");
            result.xprtTy = jObject.getInt("xprtTy");
            result.xprtGrpId = jObject.getString("xprtGrpId");
            result.mmId = jObject.getString("mmId");
            result.mmTg = jObject.getInt("mmTg");
            result.rmId = jObject.getString("rmId");
            result.csltTy = jObject.getInt("csltTy");
            result.ldngTy = jObject.getInt("ldngTy");
            result.entTy = jObject.getInt("entTy");
            result.qstn = jObject.getString("qstn");
            result.mStt = jObject.getInt("mStt");
            result.mmNm = jObject.getString("mmNm");
            result.intr = jObject.getString("intr");
            result.imgTmb = jObject.getString("imgTmb");
            result.voice = jObject.getString("voice");
            result.branchName = jObject.getString("branchName");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static public String loadFromJsonGroupResponse(LBJSONObject jObject) {
        String result = "";
        try {
            result = jObject.getString("xprtGrpNm");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static public String loadFromJsonGroupIdResponse(LBJSONObject jObject) {
        String result = "";
        try {
            result = jObject.getString("xprtGrpId");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
