package com.wave.messenger.vo;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 9..
 * 모임리스트
 */

public class VoMoimList implements Serializable, Comparable<VoMoimList> {
    ArrayList<VoMoimList> params = new ArrayList<>();


    /*모임 키값(Key)
    UUID 형식으로 소문자 문자열로 생성
    예) b99c96a1-6f6d-4696-b856-e81d7bf7041a */
    String mmId;

    /*모임 이름*/
    String mmNm;

    /*모임 타입(int)
    0:일반, 1:추천모임*/
    String mmTy;

    /*모임 종류(int)
    1:주식
    2:국내파생
    3:해외파생
    4:해외주식
    5:금융상품
    6:기타*/
    String mmTg;

    /*방 아이디*/
    String rmId;


    /*모임 이미지 타입(int)
    0:이미지 없음, 1:있음*/
    String imgTy;


    /*모임 이미지 원본*/
    String imgOrg;

    /*모임 썸네일 원본*/
    String imgTmb;

    /*모임 설명*/
    String intr;

    /*현재 가입 유저수(int)*/
    String usrCnt;

    /*가입 최대 사용자(int)
    50, 100, 500 중에 설정, 기본 : 100*/
    String maxUsr;

    /*공개 제한(int)
    0:비공개, 1:공개, 2:모임명만 공개(기본),*/
    String opnTy;

    /*가입 제한(int)
    0:자동가입(기본), 1:승인*/
    String entTy;

    /*모임 이름 및 커버 설정 권한(int)
    0:모든멤버, 1:리더, 2:리더와 공동리더(기본)*/
    String covTy;

    /*멤버 가입신청 수락 권한(int)
    0:모든멤버, 1:리더, 2:리더와 공동리더(기본)*/
    String acptTy;

    /*멤버 초대 권한(int)
    0:모든멤버(기본), 1:리더, 2:리더와 공동리더*/
    String ivtTy;

    /*공지글 등록 권한(int)
    0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
    */
    String notiTy;

    /*글쓰기 권한(int)
    0:모든멤버(기본), 1:리더, 2:리더와 공동리더*/
    String wrtTy;

    /*앨범 만들기 권한(int)
    0:모든멤버(기본), 1:리더, 2:리더와 공동리더*/
    String abmTy;

    /*댓글 쓰기 권한(int)
    0:모든멤버(기본), 1:리더, 2:리더와 공동리더*/
    String rpyTy;

    /*다른 멤버의 게시물, 댓글 삭제 권한(int)
    0:모든멤버, 1:리더(기본), 2:리더와 공동리더*/
    String delTy;

    /*멤버 탈퇴, 차단 권한(int)
    0:모든멤버, 1:리더(기본), 2:리더와 공동리더*/
    String frcTy;

    /*메뉴우선순위 종류(int)
    0:컨텐츠 타임라인, 1:오픈채팅방(기본)*/
    String mnuTy;

    /*오픈채팅방 개설가능 여부(int)
    0:개설불가(기본), 1:개설가능*/
    String opchtTy;

    /*모임 멤버간 1:1채팅 가능여부(int)
    0:불가(기본), 1:가능*/
    String snglTy;

    /*대화내용 서버 보관기간(int)
    0:보관안함(기본), 30:30일, 365:1년*/
    String chtPd;

    /*5MB 이상 파일 저장기간(int)
    30:30일, 60:60일(기본)
            */
    String fl5Pd;

    /*10MB 이상 파일 저장기간(int)
    15:15일(기본), 30:30일*/
    String fl10Pd;

    /*내 상태(int)
    0:가입안됨, 1:가입됨, 2:차단, 3:가입대기*/
    String mStt;

    /*모임 생성일*/
    String regDt;

    /*모임 생성자*/
    String regUsr;

    /*숨김 여부(int)
    0:안숨김, 1:숨김*/
    String hdn;

    /*
    모임 글 차단여부(int) 0:차단안함, 1:차단 */
    String bbsBlk;

    /*
    모임 정렬 순서(int)"":상단고정안함, 0:상단고정     */
    String mmOrd;

    String bLkCnt;  //좋아요 뱃지 카운트(int)    뱃지 기준값:100이상, 80이상, 60이상, 40이상
    String bCttCnt; //컨텐츠 뱃지 카운트(int)    뱃지 기준값:8이상, 7이상, 6이상, 5이상
    String bNew;    //금일 새 글 존재 여부(int) 0:없음, 1:있음

    public String getbNew() {
        return bNew;
    }

    public void setbNew(String bNew) {
        this.bNew = bNew;
    }

    public String getbLkCnt() {
        return bLkCnt;
    }

    public void setbLkCnt(String bLkCnt) {
        this.bLkCnt = bLkCnt;
    }

    public String getbCttCnt() {
        return bCttCnt;
    }

    public void setbCttCnt(String bCttCnt) {
        this.bCttCnt = bCttCnt;
    }

    public String getBbsBlk() {
        return bbsBlk;
    }

    public void setBbsBlk(String bbsBlk) {
        this.bbsBlk = bbsBlk;
    }

    public String getMmOrd() {
        return mmOrd;
    }

    public void setMmOrd(String mmOrd) {
        this.mmOrd = mmOrd;
    }

    public VoMoimList() {
    }

    public ArrayList<VoMoimList> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoMoimList> params) {
        this.params = params;
    }

    public VoMoimList(String mmId, String mmNm, String mmTy, String mmTg, String rmId, String imgTy,
                      String imgOrg, String imgTmb, String intr, String usrCnt, String maxUsr, String opnTy,
                      String entTy, String covTy, String acptTy, String ivtTy, String notiTy, String wrtTy,
                      String abmTy, String rpyTy, String delTy, String frcTy, String mnuTy, String opchtTy,
                      String snglTy, String chtPd, String fl5Pd, String fl10Pd, String mStt, String regDt,
                      String regUsr, String hdn) {
        this.mmId = mmId;
        this.mmNm = mmNm;
        this.mmTy = mmTy;
        this.mmTg = mmTg;
        this.rmId = rmId;
        this.imgTy = imgTy;
        this.imgOrg = imgOrg;
        this.imgTmb = imgTmb;
        this.intr = intr;
        this.usrCnt = usrCnt;
        this.maxUsr = maxUsr;
        this.opnTy = opnTy;
        this.entTy = entTy;
        this.covTy = covTy;
        this.acptTy = acptTy;
        this.ivtTy = ivtTy;
        this.notiTy = notiTy;
        this.wrtTy = wrtTy;
        this.abmTy = abmTy;
        this.rpyTy = rpyTy;
        this.delTy = delTy;
        this.frcTy = frcTy;
        this.mnuTy = mnuTy;
        this.opchtTy = opchtTy;
        this.snglTy = snglTy;
        this.chtPd = chtPd;
        this.fl5Pd = fl5Pd;
        this.fl10Pd = fl10Pd;
        this.mStt = mStt;
        this.regDt = regDt;
        this.regUsr = regUsr;
        this.hdn = hdn;
    }


    public String getMmId() {
        return mmId;
    }

    public void setMmId(String mmId) {
        this.mmId = mmId;
    }

    public String getMmNm() {
        return mmNm;
    }

    public void setMmNm(String mmNm) {
        this.mmNm = mmNm;
    }

    public String getMmTy() {
        return mmTy;
    }

    public void setMmTy(String mmTy) {
        this.mmTy = mmTy;
    }

    public String getMmTg() {
        return mmTg;
    }

    public void setMmTg(String mmTg) {
        this.mmTg = mmTg;
    }

    public String getRmId() {
        return rmId;
    }

    public void setRmId(String rmId) {
        this.rmId = rmId;
    }

    public String getImgTy() {
        return imgTy;
    }

    public void setImgTy(String imgTy) {
        this.imgTy = imgTy;
    }

    public String getImgOrg() {
        return imgOrg;
    }

    public void setImgOrg(String imgOrg) {
        this.imgOrg = imgOrg;
    }

    public String getImgTmb() {
        return imgTmb;
    }

    public void setImgTmb(String imgTmb) {
        this.imgTmb = imgTmb;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }

    public String getUsrCnt() {
        return usrCnt;
    }

    public void setUsrCnt(String usrCnt) {
        this.usrCnt = usrCnt;
    }

    public String getMaxUsr() {
        return maxUsr;
    }

    public void setMaxUsr(String maxUsr) {
        this.maxUsr = maxUsr;
    }

    public String getOpnTy() {
        return opnTy;
    }

    public void setOpnTy(String opnTy) {
        this.opnTy = opnTy;
    }

    public String getEntTy() {
        return entTy;
    }

    public void setEntTy(String entTy) {
        this.entTy = entTy;
    }

    public String getCovTy() {
        return covTy;
    }

    public void setCovTy(String covTy) {
        this.covTy = covTy;
    }

    public String getAcptTy() {
        return acptTy;
    }

    public void setAcptTy(String acptTy) {
        this.acptTy = acptTy;
    }

    public String getIvtTy() {
        return ivtTy;
    }

    public void setIvtTy(String ivtTy) {
        this.ivtTy = ivtTy;
    }

    public String getNotiTy() {
        return notiTy;
    }

    public void setNotiTy(String notiTy) {
        this.notiTy = notiTy;
    }

    public String getWrtTy() {
        return wrtTy;
    }

    public void setWrtTy(String wrtTy) {
        this.wrtTy = wrtTy;
    }

    public String getAbmTy() {
        return abmTy;
    }

    public void setAbmTy(String abmTy) {
        this.abmTy = abmTy;
    }

    public String getRpyTy() {
        return rpyTy;
    }

    public void setRpyTy(String rpyTy) {
        this.rpyTy = rpyTy;
    }

    public String getDelTy() {
        return delTy;
    }

    public void setDelTy(String delTy) {
        this.delTy = delTy;
    }

    public String getFrcTy() {
        return frcTy;
    }

    public void setFrcTy(String frcTy) {
        this.frcTy = frcTy;
    }

    public String getMnuTy() {
        return mnuTy;
    }

    public void setMnuTy(String mnuTy) {
        this.mnuTy = mnuTy;
    }

    public String getOpchtTy() {
        return opchtTy;
    }

    public void setOpchtTy(String opchtTy) {
        this.opchtTy = opchtTy;
    }

    public String getSnglTy() {
        return snglTy;
    }

    public void setSnglTy(String snglTy) {
        this.snglTy = snglTy;
    }

    public String getChtPd() {
        return chtPd;
    }

    public void setChtPd(String chtPd) {
        this.chtPd = chtPd;
    }

    public String getFl5Pd() {
        return fl5Pd;
    }

    public void setFl5Pd(String fl5Pd) {
        this.fl5Pd = fl5Pd;
    }

    public String getFl10Pd() {
        return fl10Pd;
    }

    public void setFl10Pd(String fl10Pd) {
        this.fl10Pd = fl10Pd;
    }

    public String getmStt() {
        return mStt;
    }

    public void setmStt(String mStt) {
        this.mStt = mStt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getRegUsr() {
        return regUsr;
    }

    public void setRegUsr(String regUsr) {
        this.regUsr = regUsr;
    }

    public String getHdn() {
        return hdn;
    }

    public void setHdn(String hdn) {
        this.hdn = hdn;
    }

    @Override
    public int compareTo(@NonNull VoMoimList o) {
        return 0;
    }
}
