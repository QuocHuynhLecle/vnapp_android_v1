package com.wave.messenger.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.util.json.LBJSONObject;

public class VoGroupList implements Serializable {

    private int group_seq;
    private String userId;
    private String gId;
    private String grpNm;
    private String rmId;
    private String grpTy;
    private String bId;
    private String buddyId;
    private ArrayList<VoGroupList> list;

    public int getGroup_seq() {
        return group_seq;
    }

    public void setGroup_seq(int group_seq) {
        this.group_seq = group_seq;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getGrpNm() {
        return grpNm;
    }

    public void setGrpNm(String grpNm) {
        this.grpNm = grpNm;
    }

    public String getRmId() {
        return rmId;
    }

    public void setRmId(String rmId) {
        this.rmId = rmId;
    }

    public String getGrpTy() {
        return grpTy;
    }

    public void setGrpTy(String grpTy) {
        this.grpTy = grpTy;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getBuddyId() {
        return buddyId;
    }

    public void setBuddyId(String buddyId) {
        this.buddyId = buddyId;
    }

    public ArrayList<VoGroupList> getList() {
        return list;
    }

    public void setList(ArrayList<VoGroupList> list) {
        this.list = list;
        sort();
    }

    public void sort(){
        Collections.sort(list, new GidDescCompare());
    }

    static public VoGroupList loadFromJsonServerResponse(LBJSONObject jObject) {
        VoGroupList result = new VoGroupList();
        try {
            result.gId = jObject.getString("gId");
            result.grpNm = jObject.getString("grpNm");
            result.rmId = jObject.getString("rmId");
            result.grpTy = jObject.getString("grpTy");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static public VoGroupList loadFromJsonGrpUsrsServerResponse(LBJSONObject jObject) {
        VoGroupList result = new VoGroupList();
        try {
            result.gId = jObject.getString("gId");
            result.bId = jObject.getString("bId");
            result.buddyId = jObject.getString("buddyId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static class GidDescCompare implements Comparator<VoGroupList> {
        /**
         * 내림차순(DESC)
         */
        @Override
        public int compare(VoGroupList arg0, VoGroupList arg1) {
            return arg0.getgId().compareTo(arg1.getgId());
        }
    }
}
