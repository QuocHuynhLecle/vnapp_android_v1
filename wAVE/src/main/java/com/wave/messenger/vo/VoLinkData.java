package com.wave.messenger.vo;

/**
 * Created by user on 2016-09-19.
 */
public class VoLinkData {
    private String linkid;
    private String link_tag;
    private String link_type;
    private String link_action;

    public String getLinkid() {
        return linkid;
    }

    public void setLinkid(String linkid) {
        this.linkid = linkid;
    }

    public String getLink_tag() {
        return link_tag;
    }

    public void setLink_tag(String link_tag) {
        this.link_tag = link_tag;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public String getLink_action() {
        return link_action;
    }

    public void setLink_action(String link_action) {
        this.link_action = link_action;
    }
}
