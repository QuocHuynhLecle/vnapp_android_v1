package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017-7-18.
 */

public class VoGroupt {
	private String result;
	private String cnt;

	ArrayList<VoGrouptItem> params=new ArrayList<>();

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getCnt() {
		return cnt;
	}

	public void setCnt(String cnt) {
		this.cnt = cnt;
	}

	public ArrayList<VoGrouptItem> getParams() {
		return params;
	}

	public void setParams(ArrayList<VoGrouptItem> params) {
		this.params = params;
	}

	/*public class VoGrouptItem {
		String xprtGrpId;
		String xprtGrpNm;

		public VoGrouptItem(String xprtGrpId, String xprtGrpNm) {
			this.xprtGrpId = xprtGrpId;
			this.xprtGrpNm = xprtGrpNm;
		}

		public String getXprtGrpId() {
			return xprtGrpId;
		}

		public void setXprtGrpId(String xprtGrpId) {
			this.xprtGrpId = xprtGrpId;
		}

		public String getXprtGrpNm() {
			return xprtGrpNm;
		}

		public void setXprtGrpNm(String xprtGrpNm) {
			this.xprtGrpNm = xprtGrpNm;
		}
	}*/
}
