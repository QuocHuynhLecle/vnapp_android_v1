package com.wave.messenger.vo;

/**
 * Created by aveapp on 2016-12-27.
 */

public class VoFriend {
	private String imgUrl;
	private String nick;
	private String id;

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
