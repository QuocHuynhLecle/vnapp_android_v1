package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 5. 16..
 * 7.15 모임 글 리스트
 *
 */
public class VoMsgItem {
    String idx; //줄순서

    /*"0": text
    "1":  image
    "3": 파일첨부
    "4":   이모티콘*/
    String type;

    String data; //내용
    //Sting linkData;
    //String linkType;


    public VoMsgItem(String idx, String type, String data) {
        this.idx = idx;
        this.type = type;
        this.data = data;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }


}