package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 8. 14..
 */

public class VoHiddenList {
    ArrayList<VoHiddenList> params = new ArrayList<>();
    String userId;
    String userName;
    String phone;
    String email;
    String user_type;
    String buddy_type;
    String buddy_status;
    String buddy_blocked;
    String buddy_hidden;
    String buddy_favorite;
    String profile_image;
    String profile_image_type;
    String profile_subject;

public VoHiddenList(String userId, String userName, String phone, String email, String user_type, String buddy_type, String buddy_status, String buddy_blocked, String buddy_hidden, String buddy_favorite, String profile_image, String profile_image_type, String profile_subject){
    this.userId = userId;
    this.userName = userName;
    this.phone = phone;
    this.email = email;
    this.user_type = user_type;
    this.buddy_type = buddy_type;
    this.buddy_status = buddy_status;
    this.buddy_blocked = buddy_blocked;
    this.buddy_hidden = buddy_hidden;
    this.buddy_favorite = buddy_favorite;
    this.profile_image = profile_image;
    this.profile_image_type = profile_image_type;
    this.profile_subject = profile_subject;
}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getBuddy_type() {
        return buddy_type;
    }

    public void setBuddy_type(String buddy_type) {
        this.buddy_type = buddy_type;
    }

    public String getBuddy_status() {
        return buddy_status;
    }

    public void setBuddy_status(String buddy_status) {
        this.buddy_status = buddy_status;
    }

    public String getBuddy_blocked() {
        return buddy_blocked;
    }

    public void setBuddy_blocked(String buddy_blocked) {
        this.buddy_blocked = buddy_blocked;
    }

    public String getBuddy_hidden() {
        return buddy_hidden;
    }

    public void setBuddy_hidden(String buddy_hidden) {
        this.buddy_hidden = buddy_hidden;
    }

    public String getBuddy_favorite() {
        return buddy_favorite;
    }

    public void setBuddy_favorite(String buddy_favorite) {
        this.buddy_favorite = buddy_favorite;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_image_type() {
        return profile_image_type;
    }

    public void setProfile_image_type(String profile_image_type) {
        this.profile_image_type = profile_image_type;
    }

    public String getProfile_subject() {
        return profile_subject;
    }

    public void setProfile_subject(String profile_subject) {
        this.profile_subject = profile_subject;
    }

    public ArrayList<VoHiddenList> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoHiddenList> params) {
        this.params = params;
    }
}
