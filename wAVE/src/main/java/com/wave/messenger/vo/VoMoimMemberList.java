package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * 7.9 모임 멤버 리스트
 * Created by yunsu on 2017. 6. 12..
 */

public class VoMoimMemberList {
    String result;
    String cnt;
    String md;
    ArrayList<VoMoimMemberlistItem> params;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public ArrayList<VoMoimMemberlistItem> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoMoimMemberlistItem> params) {
        this.params = params;
    }

    public class VoMoimMemberlistItem {

        String userId; //사용자 아이디
        String usNm;   //사용자 이름
        String tel;     //전화번호
        String usStt;  //가입 상태(int)0:가입대기, 1:가입완료, 2:차단
        String usrLv;  //사용자 레벨(int) 0:일반, 1:리더, 2:공동리더
        String pfImg;  //프로필 이미지 경로
        String pfImgTy; //프로필 이미지 종류(int) 0:사용안함1:이미지
        String regDt;   //모임 가입신청일/가입일
        int nNum;   //포지션 임의 저장
        boolean isSelected; //체크된 값인지 판단 - 공동리더

        String qstn;    //질문
        String answ;    //답변

        public String getQstn() {
            return qstn;
        }

        public void setQstn(String qstn) {
            this.qstn = qstn;
        }

        public String getAnsw() {
            return answ;
        }

        public void setAnsw(String answ) {
            this.answ = answ;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public int getnNum() {
            return nNum;
        }

        public void setnNum(int nNum) {
            this.nNum = nNum;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getUsStt() {
            return usStt;
        }

        public void setUsStt(String usStt) {
            this.usStt = usStt;
        }

        public String getUsrLv() {
            return usrLv;
        }

        public void setUsrLv(String usrLv) {
            this.usrLv = usrLv;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }
    }
}
