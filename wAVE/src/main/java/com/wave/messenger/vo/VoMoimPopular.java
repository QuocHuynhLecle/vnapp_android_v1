package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 3. 9..
 */

public class VoMoimPopular {
    String id;
    String time;
    String content;


    public VoMoimPopular(String id, String time, String content) {
        this.id = id;
        this.time = time;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
