package com.wave.messenger.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by yunsu on 2017. 5. 18..
 * 7.19 모임 글 조회
 */


public class VoBoardRead  {

    String result;    //결과 코드 "success" : 성공"error" : 실패
    String reason;    //결과 상세 "board_not_exist" : 글이 존재하지 않음
    String rpyCnt;    //댓글수

    VoReadItem params;
    ArrayList<VoReadReply> rpyLst=new ArrayList<>();

    public VoBoardRead(String result, String reason, String rpyCnt, VoReadItem params, ArrayList<VoReadReply> rpyLst) {
        this.result = result;
        this.reason = reason;
        this.rpyCnt = rpyCnt;
        this.params = params;
        this.rpyLst = rpyLst;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRpyCnt() {
        return rpyCnt;
    }

    public void setRpyCnt(String rpyCnt) {
        this.rpyCnt = rpyCnt;
    }

    public VoReadItem getParams() {
        return params;
    }

    public void setParams(VoReadItem params) {
        this.params = params;
    }

    public ArrayList<VoReadReply> getRpyLst() {
        return rpyLst;
    }

    public void setRpyLst(ArrayList<VoReadReply> rpyLst) {
        this.rpyLst = rpyLst;
    }



   /* @Override
    public int compare(VoReadItem o1, VoReadItem o2) {

        return Integer.parseInt(o2.getBbsOrd()) - Integer.parseInt(o1.getBbsOrd());
        //return Integer.compare(Integer.parseInt(o1.getBbsOrd()), Integer.parseInt(o2.getBbsOrd()));

    }*/


    /**
     * 모임글내용
     */
    public class VoReadItem {
        String mmId;    //모임 키값(Key)        UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String mmNm;    //모임 이름
        String mmTg;    //모임 종류(int) 1:주식2:국내파생3:해외파생4:해외주식5:금융상품6:기타
        String msgId;   //메시지 키값        UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String msgNo;   //메시지 시퀀스(int)        메시지 시퀀스 증가 필드값
        String grpNo;   //최상위 글 번호(int)
        String bbsDph;  //상위 글 depth(int)
        String bbsOrd;  //상위 글 order(int)
        String ttl; //제목        메시지에서 첫줄
        String msg; //메시지
        String msgMr;   //장문 메시지 여부(int)0:일반, 1:장문
        String msgTy;   //메시지 타입(int)0:일반 텍스트, 기본
        String noti;    //공지 여부(int) 0:일반, 1:공지
        String rdCnt;   //조회수(int)
        String rpyCnt;  //댓글수(int)
        String lkCnt;   //좋아요수(int)
        String dlkCnt;  //싫어요수(int)
        String atch;    //첨부 데이터        Json 스트링, 초대, 퇴장, 입장에서도 사용        카톡, SMS, 라인전송 타입도 여기에 저장        포인트 정보도 여기에 저장
        String regDt;   //글 등록일
        String regUsr;  //글 등록자
        String usNm;    //사용자 이름
        String pfImg;    //프로필 이미지 경로
        String pfImgTy;  //프로필 이미지 종류(int) 0:사용안함 1:이미지

        String emtStt;  //내 감정표현 상태 "":없음, 1:좋아요
        String usrLv;  //사용자 레벨 0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)



        public String getUsrLv() {
            return usrLv;
        }

        public void setUsrLv(String usrLv) {
            this.usrLv = usrLv;
        }

        public String getEmtStt() {
            return emtStt;
        }

        public void setEmtStt(String emtStt) {
            this.emtStt = emtStt;
        }

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }

        public String getMmTg() {
            return mmTg;
        }

        public void setMmTg(String mmTg) {
            this.mmTg = mmTg;
        }

        public String getMmNm() {
            return mmNm;
        }

        public void setMmNm(String mmNm) {
            this.mmNm = mmNm;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getMsgNo() {
            return msgNo;
        }

        public void setMsgNo(String msgNo) {
            this.msgNo = msgNo;
        }

        public String getGrpNo() {
            return grpNo;
        }

        public void setGrpNo(String grpNo) {
            this.grpNo = grpNo;
        }

        public String getBbsDph() {
            return bbsDph;
        }

        public void setBbsDph(String bbsDph) {
            this.bbsDph = bbsDph;
        }

        public String getBbsOrd() {
            return bbsOrd;
        }

        public void setBbsOrd(String bbsOrd) {
            this.bbsOrd = bbsOrd;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsgMr() {
            return msgMr;
        }

        public void setMsgMr(String msgMr) {
            this.msgMr = msgMr;
        }

        public String getMsgTy() {
            return msgTy;
        }

        public void setMsgTy(String msgTy) {
            this.msgTy = msgTy;
        }

        public String getNoti() {
            return noti;
        }

        public void setNoti(String noti) {
            this.noti = noti;
        }

        public String getRdCnt() {
            return rdCnt;
        }

        public void setRdCnt(String rdCnt) {
            this.rdCnt = rdCnt;
        }

        public String getRpyCnt() {
            return rpyCnt;
        }

        public void setRpyCnt(String rpyCnt) {
            this.rpyCnt = rpyCnt;
        }

        public String getLkCnt() {
            return lkCnt;
        }

        public void setLkCnt(String lkCnt) {
            this.lkCnt = lkCnt;
        }

        public String getDlkCnt() {
            return dlkCnt;
        }

        public void setDlkCnt(String dlkCnt) {
            this.dlkCnt = dlkCnt;
        }

        public String getAtch() {
            return atch;
        }

        public void setAtch(String atch) {
            this.atch = atch;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }
    }


    /**
     * 모임글 댓글
     */
    public class VoReadReply implements Serializable{// implements Comparator<VoReadReply>{



        String mmId;    //모임 키값(Key)        UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String msgId;   //메시지 키값        UUID 형식으로        소문자 문자열로 생성        예)b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String msgNo;   //메시지 시퀀스(int)        메시지 시퀀스        증가 필드값
        String grpNo;   //최상위 글 번호(int)
        String bbsDph;  //상위 글 depth(int)
        String bbsOrd;  //상위 글  order(int)
        String ttl;     //제목 메시지에서 첫줄
        String msg;     //메시지
        String msgMr;   //장문 메시지 여부(int)0:일반,1:장문
        String msgTy;   //메시지 타입(int)0: 일반 텍스트, 기본
        String noti;    //공지 여부(int) 0:일반,1:공지
        String rdCnt;   //조회수(int)
        String rpyCnt;  //댓글수(int)
        String atch;    //첨부 데이터 Json 스트링, 초대, 퇴장, 입장에서도사용 카톡, SMS, 라인전송 타입도 여기에 저장 포인트 정보도 여기에 저장
        String regDt;   //글 등록일
        String regUsr;  //글 등록자
        String lkCnt;   //좋아요 카운트
        String usNm;    //사용자 이름
        String pfImg;    //프로필 이미지 경로
        String pfImgTy;  //프로필 이미지 종류(int) 0:사용안함 1:이미지
        String img;
        String emtStt;  //댓글 좋아요
        String usrLv; //댓글 단사용자의 레벨


        public String getUsrLv() {
            return usrLv;
        }

        public void setUsrLv(String usrLv) {
            this.usrLv = usrLv;
        }

        public String getEmtStt() {
            return emtStt;
        }

        public void setEmtStt(String emtStt) {
            this.emtStt = emtStt;
        }

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }

        public String getLkCnt() {
            return lkCnt;
        }

        public void setLkCnt(String lkCnt) {
            this.lkCnt = lkCnt;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getMsgNo() {
            return msgNo;
        }

        public void setMsgNo(String msgNo) {
            this.msgNo = msgNo;
        }

        public String getGrpNo() {
            return grpNo;
        }

        public void setGrpNo(String grpNo) {
            this.grpNo = grpNo;
        }

        public String getBbsDph() {
            return bbsDph;
        }

        public void setBbsDph(String bbsDph) {
            this.bbsDph = bbsDph;
        }

        public String getBbsOrd() {
            return bbsOrd;
        }

        public void setBbsOrd(String bbsOrd) {
            this.bbsOrd = bbsOrd;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsgMr() {
            return msgMr;
        }

        public void setMsgMr(String msgMr) {
            this.msgMr = msgMr;
        }

        public String getMsgTy() {
            return msgTy;
        }

        public void setMsgTy(String msgTy) {
            this.msgTy = msgTy;
        }

        public String getNoti() {
            return noti;
        }

        public void setNoti(String noti) {
            this.noti = noti;
        }

        public String getRdCnt() {
            return rdCnt;
        }

        public void setRdCnt(String rdCnt) {
            this.rdCnt = rdCnt;
        }

        public String getRpyCnt() {
            return rpyCnt;
        }

        public void setRpyCnt(String rpyCnt) {
            this.rpyCnt = rpyCnt;
        }

        public String getAtch() {
            return atch;
        }

        public void setAtch(String atch) {
            this.atch = atch;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        /*@Override
        public int compare(VoReadReply o1, VoReadReply o2) {
            return Integer.parseInt(o2.getBbsOrd()) - Integer.parseInt(o1.getBbsOrd());
        }*/
    }

    public final static Comparator<VoReadReply> DESCENDING_COMPARATOR = new Comparator<VoReadReply>() {
        @Override
        public int compare(VoReadReply o1, VoReadReply o2) {
            int n1=Integer.parseInt(o1.getBbsOrd());
            int n2=Integer.parseInt(o2.getBbsOrd());

            if(n1==n2){
                return Integer.parseInt(o1.getBbsDph()) - Integer.parseInt(o2.getBbsDph());
            }
            return n2 - n1;

            //return Integer.parseInt(o2.getBbsOrd()) - Integer.parseInt(o1.getBbsOrd());
        }


    };
}