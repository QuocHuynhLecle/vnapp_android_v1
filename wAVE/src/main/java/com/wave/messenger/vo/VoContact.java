package com.wave.messenger.vo;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.regex.Pattern;

public class VoContact implements Serializable {
    private static final long serialVersionUID = 2419762775015392579L;

    private long mId;
    private String mUserId;
    private String mNumber;
    private String mName;
    private String mRealName;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        if (!TextUtils.isEmpty(number)) {
            mNumber = number.replace("-", "").replace(".", "");
            setUserId("$$" + number);
        }
    }

    public String getFormattedNumber() {
        if (!TextUtils.isEmpty(mNumber)) {
            String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";

            if(!Pattern.matches(regEx, mNumber)) {
                return mNumber;
            }

            return mNumber.replaceAll(regEx, "$1-$2-$3");
        }

        return "";
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getRealName() {
        return mRealName;
    }

    public void setRealName(String realName) {
        mRealName = realName;
    }
}
