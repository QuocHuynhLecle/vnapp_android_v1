package com.wave.messenger.vo;

public class VoStockData {

    String m_ItemCode          ; //종목코드
    String m_ItemName			; //종목명
    String m_CurrentValue 		; //현재가
    String m_CompareValue 		; //전일대비
    String m_CompareMark 		; //전일대비부호
    String m_CompareRate 		; //전일대비율
    String m_Amount 			; //누적거래량

    public String getM_ItemCode() {
        return m_ItemCode;
    }

    public void setM_ItemCode(String m_ItemCode) {
        this.m_ItemCode = m_ItemCode;
    }

    public String getM_ItemName() {
        return m_ItemName;
    }

    public void setM_ItemName(String m_ItemName) {
        this.m_ItemName = m_ItemName;
    }

    public String getM_CurrentValue() {
        return m_CurrentValue;
    }

    public void setM_CurrentValue(String m_CurrentValue) {
        this.m_CurrentValue = m_CurrentValue;
    }

    public String getM_CompareValue() {
        return m_CompareValue;
    }

    public void setM_CompareValue(String m_CompareValue) {
        this.m_CompareValue = m_CompareValue;
    }

    public String getM_CompareMark() {
        return m_CompareMark;
    }

    public void setM_CompareMark(String m_CompareMark) {
        this.m_CompareMark = m_CompareMark;
    }

    public String getM_CompareRate() {
        return m_CompareRate;
    }

    public void setM_CompareRate(String m_CompareRate) {
        this.m_CompareRate = m_CompareRate;
    }

    public String getM_Amount() {
        return m_Amount;
    }

    public void setM_Amount(String m_Amount) {
        this.m_Amount = m_Amount;
    }
}
