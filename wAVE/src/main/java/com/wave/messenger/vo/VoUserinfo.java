package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 4. 24..
 * 모임프로필
 *
 * @deprecated
 */

public class VoUserinfo {

    VoUserinfoItem params;
    String result;

    public VoUserinfoItem getParams() {
        return params;
    }

    public void setParams(VoUserinfoItem params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



    /**
     * 모임프로필 공지
     */
    public class VoUserinfoItem {
        String userId;  //아이디
        String userName;    //이름
        String user_type;    //사용자 타입 0 : 일반유저1 : 추천 유저(시스템)
        String tel;     //전화번호
        String email;   //이메일
        String birthDay;    //생년월일
        String sex;     //성별0:남자, 1:여자
        String occupation;   //직업군 0:금융업 종사자1:비제도권 전문가2:개미군단3:전업 투자자
        String investmentExperience;    //투자경험0:없음1:~5년2:~10년3:~20년4:20년+
        String targetRate;  //목표수익률0:10%이하1:~30%        2:30%+
        String investmentAccount;   //투자금액0:~1천만원1:~1억2:10억3:10억+
        String profile_image;   //프로필 이미지
        String profile_image_type; //프로필 이미지 종류
        String profile_subject; //대화명
        String blocked; //계정 블록 여부

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBirthDay() {
            return birthDay;
        }

        public void setBirthDay(String birthDay) {
            this.birthDay = birthDay;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getOccupation() {
            return occupation;
        }

        public void setOccupation(String occupation) {
            this.occupation = occupation;
        }

        public String getInvestmentExperience() {
            return investmentExperience;
        }

        public void setInvestmentExperience(String investmentExperience) {
            this.investmentExperience = investmentExperience;
        }

        public String getTargetRate() {
            return targetRate;
        }

        public void setTargetRate(String targetRate) {
            this.targetRate = targetRate;
        }

        public String getInvestmentAccount() {
            return investmentAccount;
        }

        public void setInvestmentAccount(String investmentAccount) {
            this.investmentAccount = investmentAccount;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getProfile_image_type() {
            return profile_image_type;
        }

        public void setProfile_image_type(String profile_image_type) {
            this.profile_image_type = profile_image_type;
        }

        public String getProfile_subject() {
            return profile_subject;
        }

        public void setProfile_subject(String profile_subject) {
            this.profile_subject = profile_subject;
        }

        public String getBlocked() {
            return blocked;
        }

        public void setBlocked(String blocked) {
            this.blocked = blocked;
        }
    }
}


