package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 3. 17..
 */

public class VoMoimReplyList {
    String id;
    String profileImg;
    String reply;
    String like;
    String time;

    public VoMoimReplyList(String id, String profileImg, String reply, String like, String time) {
        this.id = id;
        this.profileImg = profileImg;
        this.reply = reply;
        this.like = like;
        this.time = time;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
