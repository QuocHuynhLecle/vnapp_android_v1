package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 5. 22..
 * 7.27 모임 타임라인
 */


public class VoMoimTimeline {

    String result;  //결과 코드 "success" : 성공 "error" : 실패
    String cnt;    //메시지 카운트(int)


    ArrayList<VoMoimTimelineItem> params;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public ArrayList<VoMoimTimelineItem> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoMoimTimelineItem> params) {
        this.params = params;
    }

    /**
     *
     */
    public class VoMoimTimelineItem {
        String mmId;    //모임 키값(Key)        UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String msgId;  //메시지 키값 UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String msgNo;   //메시지 시퀀스(int)        메시지 시퀀스 증가 필드값
        String ttl;     //제목         메시지에서 첫줄
        String msg;     //메시지
        String msgMr;   //장문 메시지 여부(int)0:일반, 1:장문
        String msgTy;   //메시지 타입(int)0:일반 텍스트, 기본
        String noti;    //공지 여부(int)0:일반, 1:공지
        String rdCnt;   //조회수(int)
        String rpyCnt;  //댓글수(int)
        String lkCnt;   //좋아요수(int)
        String dlkCnt;  //싫어요수(int)
        String atch;    //첨부 데이터        Json 스트링, 초대, 퇴장, 입장에서도 사용        카톡, SMS, 라인전송 타입도 여기에 저장        포인트 정보도 여기에 저장
        String regDt;   //글 등록일
        String regUsr;  //글 등록자

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getMsgNo() {
            return msgNo;
        }

        public void setMsgNo(String msgNo) {
            this.msgNo = msgNo;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsgMr() {
            return msgMr;
        }

        public void setMsgMr(String msgMr) {
            this.msgMr = msgMr;
        }

        public String getMsgTy() {
            return msgTy;
        }

        public void setMsgTy(String msgTy) {
            this.msgTy = msgTy;
        }

        public String getNoti() {
            return noti;
        }

        public void setNoti(String noti) {
            this.noti = noti;
        }

        public String getRdCnt() {
            return rdCnt;
        }

        public void setRdCnt(String rdCnt) {
            this.rdCnt = rdCnt;
        }

        public String getRpyCnt() {
            return rpyCnt;
        }

        public void setRpyCnt(String rpyCnt) {
            this.rpyCnt = rpyCnt;
        }

        public String getLkCnt() {
            return lkCnt;
        }

        public void setLkCnt(String lkCnt) {
            this.lkCnt = lkCnt;
        }

        public String getDlkCnt() {
            return dlkCnt;
        }

        public void setDlkCnt(String dlkCnt) {
            this.dlkCnt = dlkCnt;
        }

        public String getAtch() {
            return atch;
        }

        public void setAtch(String atch) {
            this.atch = atch;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }
    }
}