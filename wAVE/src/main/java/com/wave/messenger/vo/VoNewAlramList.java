package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 9. 7..
 * 임의로 만들어둔 새로운 알람 리스트
 */


public class VoNewAlramList {
    ArrayList<VoNewAlramItem> params = new ArrayList<>();

    String result;
    String count;
    String roomId;

    public ArrayList<VoNewAlramItem> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoNewAlramItem> params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public class VoNewAlramItem {
        String cId;
        String chatId;
        String ownerId;
        String senderName;
        String chatType;
        String messageType;
        String staffMsg;
        String title;
        String message;
        String attachment;
        String reg_date;

        public String getcId() {
            return cId;
        }

        public void setcId(String cId) {
            this.cId = cId;
        }

        public String getChatId() {
            return chatId;
        }

        public void setChatId(String chatId) {
            this.chatId = chatId;
        }

        public String getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(String ownerId) {
            this.ownerId = ownerId;
        }

        public String getSenderName() {
            return senderName;
        }

        public void setSenderName(String senderName) {
            this.senderName = senderName;
        }

        public String getChatType() {
            return chatType;
        }

        public void setChatType(String chatType) {
            this.chatType = chatType;
        }

        public String getMessageType() {
            return messageType;
        }

        public void setMessageType(String messageType) {
            this.messageType = messageType;
        }

        public String getStaffMsg() {
            return staffMsg;
        }

        public void setStaffMsg(String staffMsg) {
            this.staffMsg = staffMsg;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAttachment() {
            return attachment;
        }

        public void setAttachment(String attachment) {
            this.attachment = attachment;
        }

        public String getReg_date() {
            return reg_date;
        }

        public void setReg_date(String reg_date) {
            this.reg_date = reg_date;
        }
    }

}