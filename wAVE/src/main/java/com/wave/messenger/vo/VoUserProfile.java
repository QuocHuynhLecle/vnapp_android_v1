package com.wave.messenger.vo;

/**
 * Created by yunsu on 2017. 4. 24..
 * 모임프로필
 *
 *
 */

public class VoUserProfile {

    VoUserProfileItem params;
    String result;


    public VoUserProfileItem getParams() {
        return params;
    }

    public void setParams(VoUserProfileItem params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    /**
     * 모임프로필 공지
     */
    public class VoUserProfileItem {

        String usNm;    //사용자 이름
        String pfImg;   //프로필 이미지 경로
        String pfImgTy; //프로필 이미지 종류(int)0:사용안함1:이미지
        String pfSbj;   //프로필 상태메시지
        String usrLv;   //사용자 레벨(int)  0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)
        String tel;     //전화번호
        String brthDy;  //생일
        String regDt;   //모임가입일
        String snglTy;  //대화권한

        public String getSnglTy() {
            return snglTy;
        }

        public void setSnglTy(String snglTy) {
            this.snglTy = snglTy;
        }

        public String getUsrLv() {
            return usrLv;
        }

        public void setUsrLv(String usrLv) {
            this.usrLv = usrLv;
        }

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }

        public String getPfSbj() {
            return pfSbj;
        }

        public void setPfSbj(String pfSbj) {
            this.pfSbj = pfSbj;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getBrthDy() {
            return brthDy;
        }

        public void setBrthDy(String brthDy) {
            this.brthDy = brthDy;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }
    }
}


