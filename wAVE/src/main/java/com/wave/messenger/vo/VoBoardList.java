package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 5. 16..
 * 7.15 모임 글 리스트
 */


public class VoBoardList {
    ArrayList<VoBoardItem> params = new ArrayList<>();
    ArrayList<VoBoardItemReply> rpyLst = new ArrayList<>();
    String result;
    String cnt;

    /*
    동작모드
    md 값이 없을시 0으로 처리됨
    0:모임글리스트(기본)
    1:모임공지사항
    2:모임작성글
    3:모임작성댓글
    4:모임별작성글
5:모임별작성댓글
*/
    String md;
    String pg;
    String rpyLstCnt;   //댓글 카운트(int) 0:모임글리스트(기본) 시에만 들어옴


    public ArrayList<VoBoardItemReply> getRpyLst() {
        return rpyLst;
    }

    public void setRpyLst(ArrayList<VoBoardItemReply> rpyLst) {
        this.rpyLst = rpyLst;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getRpyLstCnt() {
        return rpyLstCnt;
    }

    public void setRpyLstCnt(String rpyLstCnt) {
        this.rpyLstCnt = rpyLstCnt;
    }

    public ArrayList<VoBoardItem> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoBoardItem> params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }


    /**
     * 댓글
     */
    public class VoBoardItemReply {

        String mmId;
        String msgId;
        String msgNo;
        String prntNo;
        String ttl;
        String msg;
        String msgMr;
        String msgTy;
        String lkCnt;
        String emtStt;
        String regDt;
        String regUsr;
        String pfImg;
        String pfImgTy;
        String usNm;

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getMsgNo() {
            return msgNo;
        }

        public void setMsgNo(String msgNo) {
            this.msgNo = msgNo;
        }

        public String getPrntNo() {
            return prntNo;
        }

        public void setPrntNo(String prntNo) {
            this.prntNo = prntNo;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMsgMr() {
            return msgMr;
        }

        public void setMsgMr(String msgMr) {
            this.msgMr = msgMr;
        }

        public String getMsgTy() {
            return msgTy;
        }

        public void setMsgTy(String msgTy) {
            this.msgTy = msgTy;
        }

        public String getLkCnt() {
            return lkCnt;
        }

        public void setLkCnt(String lkCnt) {
            this.lkCnt = lkCnt;
        }

        public String getEmtStt() {
            return emtStt;
        }

        public void setEmtStt(String emtStt) {
            this.emtStt = emtStt;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }
    }

    public class VoBoardItem {

        ArrayList<VoBoardItemReply> alReply;//최신 댓글2개 저장
        String mmId;
        String mmNm;
        String mmTg;

        String msgId;
        String msgNo;   //메시지 시퀀스(int)  메시지 시퀀스 증가 필드값
        String ttl; //제목    메시지에서 첫줄
        String msg; //메시지
        //String msgMr;  //장문 메시지 여부(int)0:일반, 1:장문

        //ArrayList<VoMsgItem> msg;

        String msgMr;   //장문 메시지 여부(int) 0:일반, 1:장문


        String msgTy;  //메시지 타입(int)0 :일반 텍스트, 기본
        String noti;    //공지 여부(int)0:일반, 1:공지
        String rdCnt;   //조회수(int)
        String rpyCnt;  //댓글수(int)
        String lkCnt;   //좋아요수(int)
        String dlkCnt;  //싫어요수(int)
        String emtStt;  //내 감정표현 상태(int) "":없음, 1:좋아요, 2:싫어요
        String atch;    //첨부 데이터
        //String Json;    //스트링, 초대, 퇴장, 입장에서도 사용카톡, SMS, 라인전송 타입도 여기에 저장    포인트 정보도 여기에 저장
        String regDt;   //글 등록일
        String regUsr;  //글 등록자
        String usNm;    //사용자이름
        String pfImg;   //프로필 이미지 경로
        String pfImgTy; //프로필 이미지 종류(int) 0:사용안함 1:이미지
        String rpyTy;   //댓글 권한 0:모든멤버(기본), 1:리더, 2:리더와 공동리더

        String rpyLstCnt; //댓글수
        String usrLv;   //사용자 레벨(int) 0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)


        public String getUsrLv() {
            return usrLv;
        }

        public void setUsrLv(String usrLv) {
            this.usrLv = usrLv;
        }

        public String getRpyLstCnt() {
            return rpyLstCnt;
        }

        public void setRpyLstCnt(String rpyLstCnt) {
            this.rpyLstCnt = rpyLstCnt;
        }

        public String getRpyTy() {
            return rpyTy;
        }

        public void setRpyTy(String rpyTy) {
            this.rpyTy = rpyTy;
        }

        public ArrayList<VoBoardItemReply> getAlReply() {
            return alReply;
        }

        public void setAlReply(ArrayList<VoBoardItemReply> alReply) {
            this.alReply = alReply;
        }

        public String getMmNm() {
            return mmNm;
        }

        public void setMmNm(String mmNm) {
            this.mmNm = mmNm;
        }

        public String getMmTg() {
            return mmTg;
        }

        public void setMmTg(String mmTg) {
            this.mmTg = mmTg;
        }

        public String getMsgMr() {
            return msgMr;
        }

        public void setMsgMr(String msgMr) {
            this.msgMr = msgMr;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getPfImgTy() {
            return pfImgTy;
        }

        public void setPfImgTy(String pfImgTy) {
            this.pfImgTy = pfImgTy;
        }

        public String getEmtStt() {
            return emtStt;
        }

        public void setEmtStt(String emtStt) {
            this.emtStt = emtStt;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getMsgNo() {
            return msgNo;
        }

        public void setMsgNo(String msgNo) {
            this.msgNo = msgNo;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }


        public String getMsgTy() {
            return msgTy;
        }

        public void setMsgTy(String msgTy) {
            this.msgTy = msgTy;
        }

        public String getNoti() {
            return noti;
        }

        public void setNoti(String noti) {
            this.noti = noti;
        }

        public String getRdCnt() {
            return rdCnt;
        }

        public void setRdCnt(String rdCnt) {
            this.rdCnt = rdCnt;
        }

        public String getRpyCnt() {
            return rpyCnt;
        }

        public void setRpyCnt(String rpyCnt) {
            this.rpyCnt = rpyCnt;
        }

        public String getLkCnt() {
            return lkCnt;
        }

        public void setLkCnt(String lkCnt) {
            this.lkCnt = lkCnt;
        }

        public String getDlkCnt() {
            return dlkCnt;
        }

        public void setDlkCnt(String dlkCnt) {
            this.dlkCnt = dlkCnt;
        }

        public String getAtch() {
            return atch;
        }

        public void setAtch(String atch) {
            this.atch = atch;
        }


        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }
    }

}