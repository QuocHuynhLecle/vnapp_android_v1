package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 4. 24..
 * 모임프로필
 */

public class VoMoimProfile {

    VoMoimProfileNoticeParam params;
    ArrayList<VoMoimProfileNotice> notiLst;

    public VoMoimProfileNoticeParam getParams() {
        return params;
    }

    public void setParams(VoMoimProfileNoticeParam params) {
        this.params = params;
    }

    public ArrayList<VoMoimProfileNotice> getNotiLst() {
        return notiLst;
    }

    public void setNotiLst(ArrayList<VoMoimProfileNotice> notiLst) {
        this.notiLst = notiLst;
    }

    /**
     * 모임프로필 공지
     */
    public class VoMoimProfileNotice {

        String msgId; //공지 메시지 아이디
        String ttl; //공지 타이틀
        String msg; //공지 내용
        String atch; //공지 첨부 데이터
        String regDt; //공지 등록일
        String noti;    //공지 여부 0:일반, 1:공지 2:긴급공지

        public String getNoti() {
            return noti;
        }

        public void setNoti(String noti) {
            this.noti = noti;
        }

        public String getMsgId() {
            return msgId;
        }

        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        public String getTtl() {
            return ttl;
        }

        public void setTtl(String ttl) {
            this.ttl = ttl;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getAtch() {
            return atch;
        }

        public void setAtch(String atch) {
            this.atch = atch;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }
    }

    /**
     * 모임 타이틀
     */
    public class VoMoimProfileNoticeParam {
        String mmId;  //모임 키값(Key)
        String mmNm;   //모임 이름
        String mmTy;    // 모임 타입(int) 0:일반, 1:추천모임
        String mmTg;    /// 모임 종류(int): 주식 2 : 국내파생 3 : 해외파생 4 : 해외주식 5 : 금융상품 6 : 기타
        String rmId;    //방 아이디


        String imgTy;    //모임 이미지 타입(int) 0:이미지 없음, 1:있음
        String imgOrg;    //모임 이미지 원본
        String imgTmb;   //모임 썸네일 원본
        String intr;   //모임 설명
        String qstn;   //모임 가입질문
        String usrCnt; //현재 가입 유저수(int)

        String maxUsr; //가입 최대 사용자(int) 50, 100, 500 중에 설정, 기본 : 100
        String opnTy; //공개 제한(int) 0:비공개, 1:공개, 2:모임명만 공개(기본),
        String entTy; //가입 제한(int)0:자동가입(기본), 1:승인
        String covTy; //모임 이름 및 커버 설정 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        String acptTy; //멤버 가입신청 수락 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)

        String ivtTy; //멤버 초대 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String notiTy;  //공지글 등록 권한 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        String wrtTy; //글쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String abmTy; //앨범 만들기 권한 (int)0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String rpyTy; //댓글 쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더

        String delTy; //다른 멤버의 게시물, 댓글 삭제 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
        String frcTy; //멤버 탈퇴, 차단 권 한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
        String mnuTy; //메뉴우선순위 종류 (int)0:컨텐츠 타임라인, 1:오픈채팅방(기본)
        String opchtTy; //오픈채팅방 개설가능 여부(int)0:개설불가(기본), 1:개설가능
        String snglTy; //모임 멤버간 1:1채팅 가능여부(int) 0:불가(기본), 1:가능
        String mLv;     //내 사용자 레벨(int) 0:일반, 1:리더, 2:공동리더

        String useMmPush;   //모임 알림 사용여부(int)   0:사용안함, 1:사용
        String useMmBbsPush;   //모임 글 알림 사용여부(int) 0:사용안함, 1:사용
        String useMmRpyPush;   //모임 댓글 알림 사용여부(int) 0:사용안함, 1:사용

        String chtPd; //대화내용 서버 보관 기간(int) 0:보관안함(기본), 30:30 일, 365:1 년
        String fl5Pd; //5MB 이상 파일 저장 기간(int) 30:30 일, 60:60 일(기본)
        String fl10Pd; //10MB 이상 파일 저 장기간(int) 15:15 일(기본), 30:30 일
        String mStt; //내 상태(int) 0:가입안됨, 1:가입됨
        String regDt; //모임 생성일
        String regUsr; //모임 생성자
        String notiCnt; //공지 카운트(int)

        String pfImg;// 모임프로필이미지
        String mmShrtId; //모임숏아이디

        String xprt;    //전문가 모임 여부(int)"":미신청, 0:미승인, 1:승인완료
        String csltTy;  //종목상담여부(int)0:OFF, 1:ON
        String ldngTy;  //종목리딩여부(int)0:OFF, 1:ON
        String regUsrName; //모임 생성자 이름


        public String getCsltTy() {
            return csltTy;
        }

        public void setCsltTy(String csltTy) {
            this.csltTy = csltTy;
        }

        public String getLdngTy() {
            return ldngTy;
        }

        public void setLdngTy(String ldngTy) {
            this.ldngTy = ldngTy;
        }

        public String getXprt() {
            return xprt;
        }

        public void setXprt(String xprt) {
            this.xprt = xprt;
        }

        public String getPfImg() {
            return pfImg;
        }

        public void setPfImg(String pfImg) {
            this.pfImg = pfImg;
        }

        public String getQstn() {
            return qstn;
        }

        public void setQstn(String qstn) {
            this.qstn = qstn;
        }

        public String getUseMmPush() {
            return useMmPush;
        }

        public void setUseMmPush(String useMmPush) {
            this.useMmPush = useMmPush;
        }

        public String getUseMmBbsPush() {
            return useMmBbsPush;
        }

        public void setUseMmBbsPush(String useMmBbsPush) {
            this.useMmBbsPush = useMmBbsPush;
        }

        public String getUseMmRpyPush() {
            return useMmRpyPush;
        }

        public void setUseMmRpyPush(String useMmRpyPush) {
            this.useMmRpyPush = useMmRpyPush;
        }

        public String getmLv() {
            return mLv;
        }

        public void setmLv(String mLv) {
            this.mLv = mLv;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMmNm() {
            return mmNm;
        }

        public void setMmNm(String mmNm) {
            this.mmNm = mmNm;
        }

        public String getMmTy() {
            return mmTy;
        }

        public void setMmTy(String mmTy) {
            this.mmTy = mmTy;
        }

        public String getMmTg() {
            return mmTg;
        }

        public void setMmTg(String mmTg) {
            this.mmTg = mmTg;
        }

        public String getRmId() {
            return rmId;
        }

        public void setRmId(String rmId) {
            this.rmId = rmId;
        }

        public String getImgTy() {
            return imgTy;
        }

        public void setImgTy(String imgTy) {
            this.imgTy = imgTy;
        }

        public String getImgOrg() {
            return imgOrg;
        }

        public void setImgOrg(String imgOrg) {
            this.imgOrg = imgOrg;
        }

        public String getImgTmb() {
            return imgTmb;
        }

        public void setImgTmb(String imgTmb) {
            this.imgTmb = imgTmb;
        }

        public String getIntr() {
            return intr;
        }

        public void setIntr(String intr) {
            this.intr = intr;
        }

        public String getUsrCnt() {
            return usrCnt;
        }

        public void setUsrCnt(String usrCnt) {
            this.usrCnt = usrCnt;
        }

        public String getMaxUsr() {
            return maxUsr;
        }

        public void setMaxUsr(String maxUsr) {
            this.maxUsr = maxUsr;
        }

        public String getOpnTy() {
            return opnTy;
        }

        public void setOpnTy(String opnTy) {
            this.opnTy = opnTy;
        }

        public String getEntTy() {
            return entTy;
        }

        public void setEntTy(String entTy) {
            this.entTy = entTy;
        }

        public String getCovTy() {
            return covTy;
        }

        public void setCovTy(String covTy) {
            this.covTy = covTy;
        }

        public String getAcptTy() {
            return acptTy;
        }

        public void setAcptTy(String acptTy) {
            this.acptTy = acptTy;
        }

        public String getIvtTy() {
            return ivtTy;
        }

        public void setIvtTy(String ivtTy) {
            this.ivtTy = ivtTy;
        }

        public String getNotiTy() {
            return notiTy;
        }

        public void setNotiTy(String notiTy) {
            this.notiTy = notiTy;
        }

        public String getWrtTy() {
            return wrtTy;
        }

        public void setWrtTy(String wrtTy) {
            this.wrtTy = wrtTy;
        }

        public String getAbmTy() {
            return abmTy;
        }

        public void setAbmTy(String abmTy) {
            this.abmTy = abmTy;
        }

        public String getRpyTy() {
            return rpyTy;
        }

        public void setRpyTy(String rpyTy) {
            this.rpyTy = rpyTy;
        }

        public String getDelTy() {
            return delTy;
        }

        public void setDelTy(String delTy) {
            this.delTy = delTy;
        }

        public String getFrcTy() {
            return frcTy;
        }

        public void setFrcTy(String frcTy) {
            this.frcTy = frcTy;
        }

        public String getMnuTy() {
            return mnuTy;
        }

        public void setMnuTy(String mnuTy) {
            this.mnuTy = mnuTy;
        }

        public String getOpchtTy() {
            return opchtTy;
        }

        public void setOpchtTy(String opchtTy) {
            this.opchtTy = opchtTy;
        }

        public String getSnglTy() {
            return snglTy;
        }

        public void setSnglTy(String snglTy) {
            this.snglTy = snglTy;
        }

        public String getChtPd() {
            return chtPd;
        }

        public void setChtPd(String chtPd) {
            this.chtPd = chtPd;
        }

        public String getFl5Pd() {
            return fl5Pd;
        }

        public void setFl5Pd(String fl5Pd) {
            this.fl5Pd = fl5Pd;
        }

        public String getFl10Pd() {
            return fl10Pd;
        }

        public void setFl10Pd(String fl10Pd) {
            this.fl10Pd = fl10Pd;
        }

        public String getmStt() {
            return mStt;
        }

        public void setmStt(String mStt) {
            this.mStt = mStt;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }

        public String getNotiCnt() {
            return notiCnt;
        }

        public void setNotiCnt(String notiCnt) {
            this.notiCnt = notiCnt;
        }

        public void setMmShrtId(String mmShrtId) {
            this.mmShrtId = mmShrtId;
        }

        public String getMmShrtId() {
            return mmShrtId;
        }

        public String getRegUsrName() {
            return regUsrName;
        }

        public void setRegUsrName(String regUsrName) {
            this.regUsrName = regUsrName;
        }
    }
}


