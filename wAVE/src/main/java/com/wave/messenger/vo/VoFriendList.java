package com.wave.messenger.vo;

import android.support.annotation.NonNull;
import android.view.View;

import com.wave.messenger.util.LocalContactData;
import com.wave.messenger.utility.ErrorController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.util.json.LBJSONObject;

public class VoFriendList implements Serializable, Comparable<VoFriendList> {
	private boolean member;
	long buddySeq = 0;
	int remoteType = 0; //0 : 로컬, 1:일반
	String buddyHidden = "";
	String userId = "";
	String groupId = "";
	String profileImage = "";
	String email = "";
	String buddyStatus = "";
	String phone = "";
	String bid = "";
	String buddyType = "";
	String buddyBlocked = "";
	String profileSubject = "";
	String userType = "";
	String profileImageType = "";
	String userName = "";
	String buddyFavorite = "";
	String point="";
	String profile_subject;
	private String realUserName="";
	boolean editMoney=false;
	private String rawPhoneNumber = "";
	private boolean isSelected = false;
	private boolean NewImage = true;
	private ArrayList<VoFriendList> list;
	private String addDate;
	private String staff;
	public boolean isHeader;
	//전문가
	private String proId;
	private String usNm;
	private String mmId;
	private String rmId;
	private String qstn;
	private int pro_seq;
	private int xprtTy;  //0:매니저, 1:애널리스트, 2:전문가
	private int mmTg;    //1:주식, 2:국내파생, 3:해외파생, 4:해외주식, 5:금융상품, 6:기타
	private int csltTy;  //0:OFF, 1:ON
	private int ldngTy;  //0:OFF, 1:ON
	private int entTy;   //0:승인, 1:자동가입(기본), 2:비밀번호
	private int mStt;    //0:가입안됨, 1:가입됨, 2:차단, 3:가입대기
	public boolean isPro;
	private String gId;
	private String mmNm;
	private String intr;
	private String imgTmb;
	private String voice;
	private int iscustomer;
	private int risk_type;
	private String cuser_type;
	private String branchName;

	private View.OnLongClickListener listener;

	public VoFriendList(){
		member=false;
		isPro=false;
		list=new ArrayList<>();
	}

	public VoFriendList(String name, String phoneNumber) {
		this.userName = name;
		this.phone = phoneNumber;
	}

	public VoFriendList(String name, String userId, String phoneNumber, String profileImage, String userType) {
		this.userName = name;
		this.userId = userId;
		this.phone = phoneNumber;
		this.profileImage = profileImage;
		this.userType = userType;
	}

	public VoFriendList(String proId, String usNm, String mmId, String rmId, String qstn, int xprtTy, int mmTg, int csltTy, int ldngTy, int entTy, int mStt) {
		this.proId = proId;
		this.usNm = usNm;
		this.mmId = mmId;
		this.rmId = rmId;
		this.qstn = qstn;
		this.xprtTy = xprtTy;
		this.mmTg = mmTg;
		this.csltTy = csltTy;
		this.ldngTy = ldngTy;
		this.entTy = entTy;
		this.mStt = mStt;
		this.isPro = true;
	}

	public String getAddDate() {
		return addDate;
	}

	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}

	public boolean isNewImage() {
		return NewImage;
	}

	public void setNewImage(boolean newImage) {
		NewImage = newImage;
	}

	public boolean isMember() {
		if(userId == null){
			member = false;
			return member;
		}

		member = true;
		return member;
	}

	public void setMember(boolean member) {
		this.member = member;
	}

	public ArrayList<VoFriendList> getList() {
		return list;
	}

	public void setList(ArrayList<VoFriendList> list) {
		this.list = list;
		sort();
	}

	public boolean isEditMoney() {
		return editMoney;
	}

	public void setEditMoney(boolean editMoney) {
		this.editMoney = editMoney;
	}

	public void setUnalignedList(ArrayList<VoFriendList> list){
		this.list = list;
	}

	public void sort(){
		Collections.sort(list, new NameDescCompare());
	}

	public String getRawPhoneNumber() {
		return rawPhoneNumber;
	}

	public void setRawPhoneNumber(String rawPhoneNumber) {
		this.rawPhoneNumber = rawPhoneNumber;
	}

	public String getBuddyHidden() {
		return buddyHidden;
	}

	public String getRealUserName() {
		return realUserName;
	}

	public void setRealUserName(String realUserName) {
		this.realUserName = realUserName;
	}

	public void setBuddyHidden(String buddyHidden) {
		this.buddyHidden = buddyHidden;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getBuddySeq() {
		return buddySeq;
	}

	public void setBuddySeq(long buddySeq) {
		this.buddySeq = buddySeq;
	}

	public String getBuddyStatus() {
		return buddyStatus;
	}

	public void setBuddyStatus(String buddyStatus) {
		this.buddyStatus = buddyStatus;
	}

	public String getPhone() {
		return phone;
	}

	public String getUnalignedPhone(){
		String result = LocalContactData.getMatchingNum(phone);
		return result;
	}

	public String getPhoneNoBar(){
		String result = null;

		try{
			int tempResult = Integer.parseInt(phone);
			result = tempResult+"";
		}catch (Exception e){
			String [] temp = phone.split("-");
			for(int i = 0; i<temp.length; i++){
				result += temp[i];
			}
			ErrorController.showMessage("[FLVO] getPhoneNoBar : " + result);
		}

		return result;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getBuddyType() {
		return buddyType;
	}

	public void setBuddyType(String buddyType) {
		this.buddyType = buddyType;
	}

	public String getBuddyBlocked() {
		return buddyBlocked;
	}

	public void setBuddyBlocked(String buddyBlocked) {
		this.buddyBlocked = buddyBlocked;
	}

	public String getProfileSubject() {
		return profileSubject;
	}

	public void setProfileSubject(String profileSubject) {
		this.profileSubject = profileSubject;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getProfileImageType() {
		return profileImageType;
	}

	public void setProfileImageType(String profileImageType) {
		this.profileImageType = profileImageType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBuddyFavorite() {
		return buddyFavorite;
	}

	public void setBuddyFavorite(String buddyFavorite) {
		this.buddyFavorite = buddyFavorite;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getStaff() {
		return staff;
	}

	public void setStaff(String staff) {
		this.staff = staff;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	static class NameDescCompare implements Comparator<VoFriendList> {
		/**
		 * 내림차순(DESC)
		 */
		@Override
		public int compare(VoFriendList arg0, VoFriendList arg1) {
			return arg0.getUserName().compareTo(arg1.getUserName());
		}
	}

	public int getRemoteType() {
		return remoteType;
	}

	public void setRemoteType(int remoteType) {
		this.remoteType = remoteType;
	}

	public String getProfile_subject() {
		return profile_subject;
	}

	public void setProfile_subject(String profile_subject) {
		this.profile_subject = profile_subject;
	}

	public String getProId() {
		return proId;
	}

	public void setProId(String proId) {
		this.proId = proId;
	}

	public String getUsNm() {
		return usNm;
	}

	public void setUsNm(String usNm) {
		this.usNm = usNm;
	}

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getRmId() {
		return rmId;
	}

	public void setRmId(String rmId) {
		this.rmId = rmId;
	}

	public String getQstn() {
		return qstn;
	}

	public void setQstn(String qstn) {
		this.qstn = qstn;
	}

	public int getPro_seq() {
		return pro_seq;
	}

	public void setPro_seq(int pro_seq) {
		this.pro_seq = pro_seq;
	}

	public int getXprtTy() {
		return xprtTy;
	}

	public void setXprtTy(int xprtTy) {
		this.xprtTy = xprtTy;
	}

	public int getMmTg() {
		return mmTg;
	}

	public void setMmTg(int mmTg) {
		this.mmTg = mmTg;
	}

	public int getCsltTy() {
		return csltTy;
	}

	public void setCsltTy(int csltTy) {
		this.csltTy = csltTy;
	}

	public int getLdngTy() {
		return ldngTy;
	}

	public void setLdngTy(int ldngTy) {
		this.ldngTy = ldngTy;
	}

	public int getEntTy() {
		return entTy;
	}

	public void setEntTy(int entTy) {
		this.entTy = entTy;
	}

	public int getmStt() {
		return mStt;
	}

	public void setmStt(int mStt) {
		this.mStt = mStt;
	}

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public String getMmNm() {
		return mmNm;
	}

	public void setMmNm(String mmNm) {
		this.mmNm = mmNm;
	}

	public String getIntr() {
		return intr;
	}

	public void setIntr(String intr) {
		this.intr = intr;
	}

	public String getImgTmb() {
		return imgTmb;
	}

	public void setImgTmb(String imgTmb) {
		this.imgTmb = imgTmb;
	}

	public String getVoice() {
		return voice;
	}

	public void setVoice(String voice) {
		this.voice = voice;
	}

	public int getIscustomer() {
		return iscustomer;
	}

	public void setIscustomer(int iscustomer) {
		this.iscustomer = iscustomer;
	}

	public int getRisk_type() {
		return risk_type;
	}

	public void setRisk_type(int risk_type) {
		this.risk_type = risk_type;
	}

	public String getCuser_type() {
		return cuser_type;
	}

	public void setCuser_type(String cuser_type) {
		this.cuser_type = cuser_type;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public View.OnLongClickListener getListener() {
		return listener;
	}

	public void setListener(View.OnLongClickListener listener) {
		this.listener = listener;
	}

	static public VoFriendList loadFromJson(LBJSONObject jObject) {
		VoFriendList result = new VoFriendList();
		try {
			result.buddyHidden = jObject.getString("buddy_hidden");
			result.userId = jObject.getString("userId");
			result.groupId = jObject.getString("group_id");
			result.profileImage = jObject.getString("profile_image");
			result.email = jObject.getString("email");
			result.buddyStatus = jObject.getString("buddy_status");
			result.phone = jObject.getString("phone");
			result.bid = jObject.getString("bid");
			result.buddyType = jObject.getString("buddy_type");
			result.buddyBlocked = jObject.getString("buddy_blocked");
			result.profileSubject = jObject.getString("profile_subject");
			result.userType = jObject.getString("user_type");
			result.profileImageType = jObject.getString("profile_image_type");
			result.userName = jObject.getString("userName");
			result.buddyFavorite = jObject.getString("buddy_favorite");
			result.mmNm = jObject.getString("mmNm");
			result.intr = jObject.getString("intr");
			result.imgTmb = jObject.getString("imgTmb");
//			ErrorController.showMessage("FRIEND : " + result.userName + ", " + result.userId + ", " + result.userType + ", " + result.phone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	static public String getFormattedPhoneNumber(String rawPhoneNumber) {
		String result = "";

		if (rawPhoneNumber.length() == 10) {
			result = rawPhoneNumber.substring(0, 3) + "-"
					+ rawPhoneNumber.substring(3, 6) + "-"
					+ rawPhoneNumber.substring(6);

		} else if (rawPhoneNumber.length() > 8) {
			result = rawPhoneNumber.substring(0, 3) + "-"
					+ rawPhoneNumber.substring(3, 7) + "-"
					+ rawPhoneNumber.substring(7);
		}

		return result;
	}

	static public VoFriendList loadFromJsonServerResponse(LBJSONObject jObject) {
		VoFriendList result = new VoFriendList();
		try {
			//result.addDate = UtilityMethods.getDefaultDateString();
			result.remoteType = 1;
			result.buddyHidden = jObject.getString("buddy_hidden");
			result.userId = jObject.getString("userId");
			result.groupId = jObject.getString("group_id");
			result.profileImage = jObject.getString("profile_image");
			result.email = jObject.getString("email");
			result.buddyStatus = jObject.getString("buddy_status");
			result.phone = jObject.getString("phone");
			result.phone = getFormattedPhoneNumber(result.phone);
			result.bid = jObject.getString("bid");
			result.buddyType = jObject.getString("buddy_type");
			result.buddyBlocked = jObject.getString("buddy_blocked");
			result.profileSubject = jObject.getString("profile_subject");
			result.userType = jObject.getString("user_type");
			result.profileImageType = jObject.getString("profile_image_type");
			result.userName = jObject.getString("userName");
			result.buddyFavorite = jObject.getString("buddy_favorite");
//			ErrorController.showMessage("FRIEND : " + result.userName + ", " + result.userId + ", " + result.userType + ", " + result.phone);
			result.realUserName = jObject.getString("realUserName");
			result.iscustomer = jObject.getInt("iscustomer");
			result.risk_type = jObject.getInt("risk_type");
			result.cuser_type = jObject.getString("cuser_type");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	static public VoFriendList loadFromJsonChatUsers(LBJSONObject jObject) {
		VoFriendList result = new VoFriendList();
		try {
			result.userId = jObject.getString("userId");
			result.profileImage = jObject.getString("profile_image");
			result.profileSubject = jObject.getString("profile_subject");
			result.profileImageType = jObject.getString("profile_image_type");
			result.userName = jObject.getString("userName");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int compareTo(@NonNull VoFriendList another) {
		if(Integer.valueOf(this.staff) > Integer.valueOf(another.staff)) {
			return -1;
		} else if(Integer.valueOf(this.staff) == Integer.valueOf(another.staff)) {
			return 0;
		} else {
			return 1;
		}
	}
}
