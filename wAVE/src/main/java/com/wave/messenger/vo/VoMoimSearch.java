package com.wave.messenger.vo;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 5. 22..
 * 7.5 모임 검색
 */


public class VoMoimSearch {

    String result;   //결과 코드 "success" : 성공"error" : 실패
    String reason;   //"wrong_mode" : 잘못된 모드 설정
    String cnt;      //검색된 리스트 카운트(int)
    String md;      //검색모드 0:검색어로검색 1:주제별모임찾기 2:새로시작한모임
    String pg;      //페이지번호(int) 첫 페이지 번호 : 0
    String srchWrd; //검색어  검색어로검색 시에만 사용
    String mmTg;    //모임 종류(int) 주제별모임찾기 시에만 사용1:주식2:국내파생3:해외파생4:해외주식5:금융상품6:기타
    String strtDt;  //시작날짜    현재날짜
    String endDt;   //마지막날짜    마지막 검색된 모임의 생성날짜


    public String getStrtDt() {
        return strtDt;
    }

    public void setStrtDt(String strtDt) {
        this.strtDt = strtDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public String getEndDt() {
        return endDt;
    }



    ArrayList<VoMoimSearchItem> params;

    public ArrayList<VoMoimSearchItem> getParams() {
        return params;
    }

    public void setParams(ArrayList<VoMoimSearchItem> params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public String getSrchWrd() {
        return srchWrd;
    }

    public void setSrchWrd(String srchWrd) {
        this.srchWrd = srchWrd;
    }

    public String getMmTg() {
        return mmTg;
    }

    public void setMmTg(String mmTg) {
        this.mmTg = mmTg;
    }


    /**
     *
     */
    public class VoMoimSearchItem {
        String rnum;    //인덱스값(int)        디비 rownum, 참고용 데이터로 사용
        String mmId;    //모임 키값(Key)        UUID 형식으로 소문자 문자열로 생성        예) b99c96a1-6f6d-4696-b856-e81d7bf7041a
        String mmNm;    //모임 이름
        String mmTy;    //모임 타입(int)0:일반, 1:추천모임
        String mmTg;    //모임 종류(int)1:주식2:국내파생3:해외파생4:해외주식5:금융상품6:기타
        String rmId;    //방 아이디
        String imgTy;   //모임 이미지 타입(int)0:이미지 없음, 1:있음
        String imgOrg;  //모임 이미지 원본
        String imgTmb;  //모임 썸네일 원본
        String intr;    //모임 설명
        String usrCnt;  //현재 가입 유저수(int)
        String maxUsr;  //가입 최대 사용자(int)50, 100, 500 중에 설정, 기본 : 100
        String opnTy;   //공개 제한(int)0:비공개, 1:공개, 2:모임명만 공개(기본),
        String entTy;   //가입 제한(int)0:자동가입(기본), 1:승인
        String covTy;   //모임 이름 및 커버 설정 권한(int)0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        String acptTy;  //멤버 가입신청 수락 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        String ivtTy;   //멤버 초대 권한(int)0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String notiTy;  //공지글 등록 권한(int)0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        String wrtTy;   //글쓰기 권한(int)   0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String abmTy;   //앨범 만들기 권한(int)0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String rpyTy;   //댓글 쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        String delTy;   //다른 멤버의 게시물, 댓글 삭제 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
        String frcTy;   //멤버 탈퇴, 차단 권한(int)0:모든멤버, 1:리더(기본), 2:리더와 공동리더
        String mnuTy;   //메뉴우선순위 종류(int)0:컨텐츠 타임라인, 1:오픈채팅방(기본)
        String opchtTy; //오픈채팅방 개설가능 여부(int) 0:개설불가(기본), 1:개설가능
        String snglTy;  //모임 멤버간 1:1채팅 가능여부(int)0:불가(기본), 1:가능
        String chtPd;   //대화내용 서버 보관기간(int) 0:보관안함(기본), 30:30일, 365:1년
        String fl5Pd;   //5MB 이상 파일 저장기간(int)30:30일, 60:60일(기본)
        String fl10Pd;  //10MB 이상 파일 저장기간(int) 15:15일(기본), 30:30일
        String mStt;    //내 상태(int) 0:가입안됨, 1:가입됨, 2:차단, 3:가입대기
        String regDt;   //모임 생성일
        String regUsr;  //모임 생성자
        String usNm;  //이름

        public String getUsNm() {
            return usNm;
        }

        public void setUsNm(String usNm) {
            this.usNm = usNm;
        }

        public String getRnum() {
            return rnum;
        }

        public void setRnum(String rnum) {
            this.rnum = rnum;
        }

        public String getMmId() {
            return mmId;
        }

        public void setMmId(String mmId) {
            this.mmId = mmId;
        }

        public String getMmNm() {
            return mmNm;
        }

        public void setMmNm(String mmNm) {
            this.mmNm = mmNm;
        }

        public String getMmTy() {
            return mmTy;
        }

        public void setMmTy(String mmTy) {
            this.mmTy = mmTy;
        }

        public String getMmTg() {
            return mmTg;
        }

        public void setMmTg(String mmTg) {
            this.mmTg = mmTg;
        }

        public String getRmId() {
            return rmId;
        }

        public void setRmId(String rmId) {
            this.rmId = rmId;
        }

        public String getImgTy() {
            return imgTy;
        }

        public void setImgTy(String imgTy) {
            this.imgTy = imgTy;
        }

        public String getImgOrg() {
            return imgOrg;
        }

        public void setImgOrg(String imgOrg) {
            this.imgOrg = imgOrg;
        }

        public String getImgTmb() {
            return imgTmb;
        }

        public void setImgTmb(String imgTmb) {
            this.imgTmb = imgTmb;
        }

        public String getIntr() {
            return intr;
        }

        public void setIntr(String intr) {
            this.intr = intr;
        }

        public String getUsrCnt() {
            return usrCnt;
        }

        public void setUsrCnt(String usrCnt) {
            this.usrCnt = usrCnt;
        }

        public String getMaxUsr() {
            return maxUsr;
        }

        public void setMaxUsr(String maxUsr) {
            this.maxUsr = maxUsr;
        }

        public String getOpnTy() {
            return opnTy;
        }

        public void setOpnTy(String opnTy) {
            this.opnTy = opnTy;
        }

        public String getEntTy() {
            return entTy;
        }

        public void setEntTy(String entTy) {
            this.entTy = entTy;
        }

        public String getCovTy() {
            return covTy;
        }

        public void setCovTy(String covTy) {
            this.covTy = covTy;
        }

        public String getAcptTy() {
            return acptTy;
        }

        public void setAcptTy(String acptTy) {
            this.acptTy = acptTy;
        }

        public String getIvtTy() {
            return ivtTy;
        }

        public void setIvtTy(String ivtTy) {
            this.ivtTy = ivtTy;
        }

        public String getNotiTy() {
            return notiTy;
        }

        public void setNotiTy(String notiTy) {
            this.notiTy = notiTy;
        }

        public String getWrtTy() {
            return wrtTy;
        }

        public void setWrtTy(String wrtTy) {
            this.wrtTy = wrtTy;
        }

        public String getAbmTy() {
            return abmTy;
        }

        public void setAbmTy(String abmTy) {
            this.abmTy = abmTy;
        }

        public String getRpyTy() {
            return rpyTy;
        }

        public void setRpyTy(String rpyTy) {
            this.rpyTy = rpyTy;
        }

        public String getDelTy() {
            return delTy;
        }

        public void setDelTy(String delTy) {
            this.delTy = delTy;
        }

        public String getFrcTy() {
            return frcTy;
        }

        public void setFrcTy(String frcTy) {
            this.frcTy = frcTy;
        }

        public String getMnuTy() {
            return mnuTy;
        }

        public void setMnuTy(String mnuTy) {
            this.mnuTy = mnuTy;
        }

        public String getOpchtTy() {
            return opchtTy;
        }

        public void setOpchtTy(String opchtTy) {
            this.opchtTy = opchtTy;
        }

        public String getSnglTy() {
            return snglTy;
        }

        public void setSnglTy(String snglTy) {
            this.snglTy = snglTy;
        }

        public String getChtPd() {
            return chtPd;
        }

        public void setChtPd(String chtPd) {
            this.chtPd = chtPd;
        }

        public String getFl5Pd() {
            return fl5Pd;
        }

        public void setFl5Pd(String fl5Pd) {
            this.fl5Pd = fl5Pd;
        }

        public String getFl10Pd() {
            return fl10Pd;
        }

        public void setFl10Pd(String fl10Pd) {
            this.fl10Pd = fl10Pd;
        }

        public String getmStt() {
            return mStt;
        }

        public void setmStt(String mStt) {
            this.mStt = mStt;
        }

        public String getRegDt() {
            return regDt;
        }

        public void setRegDt(String regDt) {
            this.regDt = regDt;
        }

        public String getRegUsr() {
            return regUsr;
        }

        public void setRegUsr(String regUsr) {
            this.regUsr = regUsr;
        }
    }
}