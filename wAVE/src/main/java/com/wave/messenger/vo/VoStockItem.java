package com.wave.messenger.vo;

/**
 * Created by apple on 2017. 4. 10..
 */

public class VoStockItem {
    String stock;
    String number;
    String counter;

    public VoStockItem(String stock, String number, String counter) {
        this.stock = stock;
        this.number = number;
        this.counter = counter;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }
}
