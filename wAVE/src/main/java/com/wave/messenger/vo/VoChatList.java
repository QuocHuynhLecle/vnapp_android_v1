package com.wave.messenger.vo;

import android.content.Context;
import android.text.TextUtils;

import com.wave.messenger.Info.MessengerInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import moa.android.api.util.json.LBJSONObject;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class VoChatList implements Serializable {
    private List<VoFriendList> friends;
    private List<VoChatData> chats;
    private ArrayList<VoChatList> list;
    private int chatList_seq = 0;
    private String ruid = "";
    private String ownerId = "";
    private String roomSubject = "";
    private String roomId = "";
    private String roomShortId = "";
    private String room_type = "";
    private String room_name = "";
    private String roomComment = "";
    private String room_reg_date = "";
    private String room_profile_image = "";
    private String change_name = "";
    private String last_msg_date = "";
    private String title = "";
    private String unread = "";
    private String useNoti = "";
    private String usercount = "";
    private String start_cid = "";
    private String start_chat_id = "";
    private String last_read_cid = "";
    private String last_read_chat_id = "";
    private String enableSearch = "";
    private String enableWriteMsg = "";
    private String maxUser = "";
    private String hidden = "";
    private String notice_chat_id = "";
    private String subTitle = "";
    private String isMemberchat = "";
    private String entered = "";
    private String staff = "";
    private String roomStatus = "";
    private String chatType = "";
    private String mmid = "";
    private String gid = "";

    /** 채팅방 목록 편집에 사용되는 플래그*/
    private boolean isSelected =false;

    /** 채팅방 목록 검색에 사용되는 플래그*/
    private boolean isSearched = true;

    private boolean isJongmok = false;

    public VoChatList() {
        // TODO Auto-generated method stub
        friends=new ArrayList<>();
        chats= new ArrayList<>();
    }

    public boolean isJongmok() {
        return isJongmok;
    }

    public void setJongmok(boolean jongmok) {
        isJongmok = jongmok;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getChatList_seq() {
        return chatList_seq;
    }

    public void setChatList_seq(int chatList_seq) {
        this.chatList_seq = chatList_seq;
    }

    public String getRuid() {
        return ruid;
    }

    public void setRuid(String ruid) {
        this.ruid = ruid;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getRoomSubject() {
        return roomSubject;
    }

    public void setRoomSubject(String roomSubject) {
        this.roomSubject = roomSubject;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomShortId() {
        return roomShortId;
    }

    public void setRoomShortId(String roomShortId) {
        this.roomShortId = roomShortId;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getRoomComment() {
        return roomComment;
    }

    public void setRoomComment(String roomComment) {
        this.roomComment = roomComment;
    }

    public String getRoom_reg_date() {
        return room_reg_date;
    }

    public void setRoom_reg_date(String room_reg_date) {
        this.room_reg_date = room_reg_date;
    }

    public String getRoom_profile_image() {
        return room_profile_image;
    }

    public void setRoom_profile_image(String room_profile_image) {
        this.room_profile_image = room_profile_image;
    }

    public String getChange_name() {
        return change_name;
    }

    public void setChange_name(String change_name) {
        this.change_name = change_name;
    }

    public String getLast_msg_date() {
        return last_msg_date;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getIsMemberchat() {
        return isMemberchat;
    }

    public void setIsMemberchat(String isMemberchat) {
        this.isMemberchat = isMemberchat;
    }

    public boolean isSearched() {
        return isSearched;
    }

    public void setSearched(boolean searched) {
        isSearched = searched;
    }

    public String getFormattedDate(String time) {
        String result = "";
        String temp1;
        try {
            temp1=time.substring(0,10);
            if (time != null) {
                String[] temp = temp1.split("-");
                result = temp[0] + "년 " + temp[1] + "월 " + temp[2] + "일";
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

    public String getFormattedTime(String regDate){
        String result = "";
        try{
            if(regDate!=null && !TextUtils.isEmpty(regDate)){
                String [] temp = regDate.split(" ");
                String time = temp[1];

                String [] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute =  Integer.parseInt(timeTemp[1]);
                String hourString ="";
                String minuteString ="";
                if(hour > 12){
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour+"";
                    if(hour <10)
                        hourString = "0"+hour;
                }else{
                    noonText = "오전";
                    if(hour <10)
                        hourString = "0"+hour;
                }

                minuteString = minute+"";
                if(minute<10){
                    minuteString = "0" + minute;
                }

                result = noonText + " " + hourString + ":" + minuteString;
            }else{
                result = "오전 00:00";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public void setLast_msg_date(String last_msg_date) {
        this.last_msg_date = last_msg_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    public String getUseNoti() {
        return useNoti;
    }

    public void setUseNoti(String useNoti) {
        this.useNoti = useNoti;
    }

    public String getUsercount() {
        return usercount;
    }

    public void setUsercount(String usercount) {
        this.usercount = usercount;
    }

    public String getStart_cid() {
        return start_cid;
    }

    public void setStart_cid(String start_cid) {
        this.start_cid = start_cid;
    }

    public String getStart_chat_id() {
        return start_chat_id;
    }

    public void setStart_chat_id(String start_chat_id) {
        this.start_chat_id = start_chat_id;
    }

    public String getLast_read_cid() {
        return last_read_cid;
    }

    public void setLast_read_cid(String last_read_cid) {
        this.last_read_cid = last_read_cid;
    }

    public String getLast_read_chat_id() {
        return last_read_chat_id;
    }

    public void setLast_read_chat_id(String last_read_chat_id) {
        this.last_read_chat_id = last_read_chat_id;
    }

    public String getEnableSearch() {
        return enableSearch;
    }

    public void setEnableSearch(String enableSearch) {
        this.enableSearch = enableSearch;
    }

    public String getEnableWriteMsg() {
        return enableWriteMsg;
    }

    public void setEnableWriteMsg(String enableWriteMsg) {
        this.enableWriteMsg = enableWriteMsg;
    }

    public String getMaxUser() {
        return maxUser;
    }

    public void setMaxUser(String maxUser) {
        this.maxUser = maxUser;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getNotice_chat_id() {
        return notice_chat_id;
    }

    public void setNotice_chat_id(String notice_chat_id) {
        this.notice_chat_id = notice_chat_id;
    }

    public String getEntered() {
        return entered;
    }

    public void setEntered(String entered) {
        this.entered = entered;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public ArrayList<VoChatList> getList() {
        return list;
    }

    public void setList(ArrayList<VoChatList> list) {
        this.list = list;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getMmid() {
        return mmid;
    }

    public void setMmid(String mmid) {
        this.mmid = mmid;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public List<VoFriendList> getFriends() {
        return friends;
    }

    public void setFriends(List<VoFriendList> friendList, Context context) {
        friends.clear();
        friends = friendList;

        for(int i=0 ; i < friends.size() ; i ++) {
            VoFriendList item= friends.get(i);
            if(MessengerInfo.getUserId(context).equals(item.getUserId())) {
                friends.remove(i);
                friends.add(0,item);
            }
        }
    }

    public String getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(String roomStatus) {
        this.roomStatus = roomStatus;
    }

    public String loadFromRoomStatusJson(String data) {
        JSONArray jsonArray;
        JSONObject jsonObject = new JSONObject();
        String value = "";
        try {
            if (!roomStatus.equals("")) {
                jsonArray = new JSONArray(roomStatus);
                jsonObject = jsonArray.getJSONObject(0);
                value = jsonObject.getString(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return value;
    }

    public List<VoChatData> getChats() {
        return chats;
    }

    public void setChats(List<VoChatData> chats) {
        this.chats=null;
        this.chats = chats;
    }

    static public VoChatList loadFromJson(LBJSONObject jObject) {
        VoChatList result = new VoChatList();

        try {
            result.setRuid(jObject.getString("ruid"));
            result.setOwnerId(jObject.getString("ownerId"));
            result.setRoomId(jObject.getString("roomId"));
            result.setRoomSubject(jObject.getString("roomSubject"));
            result.setRoomShortId(jObject.getString("roomShortId"));
            result.setRoom_type(jObject.getString("room_type"));
            result.setRoom_name(jObject.getString("room_name"));
            result.setRoomComment(jObject.getString("roomComment"));
            result.setRoom_reg_date(jObject.getString("room_reg_date"));
            result.setRoom_profile_image(jObject.getString("room_profile_image"));
            result.setChange_name(jObject.getString("change_name"));
            result.setLast_msg_date(jObject.getString("last_msg_date"));
            result.setTitle(jObject.getString("title"));
            result.setUnread(jObject.getString("unread"));
            result.setUseNoti(jObject.getString("useNoti"));
            result.setUsercount(jObject.getString("userCount"));
            result.setStart_cid(jObject.getString("start_cid"));
            result.setStart_chat_id(jObject.getString("start_chat_id"));
            result.setLast_read_cid(jObject.getString("last_read_cid"));
            result.setLast_read_chat_id(jObject.getString("last_read_chat_id"));
            result.setEnableSearch(jObject.getString("enableSearch"));
            result.setEnableWriteMsg(jObject.getString("enableWriteMsg"));
            result.setMaxUser(jObject.getString("maxUser"));
            result.setHidden(jObject.getString("hidden"));
            result.setNotice_chat_id(jObject.getString("notice_chat_id"));
            result.setSubTitle(jObject.getString("subTitle"));
            result.setIsMemberchat(jObject.getString("isMembersChat"));
            result.setEntered(jObject.getString("entered"));
            result.setStaff(jObject.getString("staff"));
            result.setMmid(jObject.getString("mmId"));
            result.setRoomStatus(jObject.getString("roomStatus"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void addChat(VoChatData newData) {
        boolean exist = false;
        for(VoChatData item:chats){
            if(item.getChatId().equals(newData.getChatId())){
                exist = true;
                break;
            }
        }

        if(exist == false) {
            this.chats.add(newData);
        }
    }

    /**
     * 한명이라도 회원이 있다면 하나 true를 반환한다. <br/>
     * 채팅 목록에 맴버스 오버레이가 뜬다.
     * @return
     */
    public boolean isHanaMembers() {
        if(friends != null && friends.size() != 0) {
            for(VoFriendList fr : friends){
                if(fr.isMember())
                    return true;
            }
        }
        return false;
    }

    /**
     * 채팅 목록의 이름을 가져옴. <br/>
     * 두명 이상일 경우 두명 + ...
     * @return 채팅 방의 이름.
     */
    public String getChatListName() {
        String result = "";

        if(friends != null && friends.size()!=0) {
            int i = 0;
            for(VoFriendList users : friends){
                if(i == 2)
                    break;
                result += users.getUserName()+", ";
                i++;
            }
            if(friends.size()<=2){ //1:1 채팅 방
                result = result.trim();
                result = result.substring(0, result.length()-1);

            }else{
                result = result.trim();
                result += "...";
            }
        }
        return result;
    }

}
