package com.wave.messenger.vo;

import java.io.Serializable;

import moa.android.api.util.json.LBJSONObject;

/**
 * Created by user on 2016-09-02.
 */
public class VoUsers implements Serializable {
    private String roomId;
    private String userId;
    private String userName;
    private String tel;
    private String realUserName;
    private String profile_image;
    private String profile_image_type;
    private String profile_subject;
    private String staff;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getProfile_image_type() {
        return profile_image_type;
    }

    public void setProfile_image_type(String profile_image_type) {
        this.profile_image_type = profile_image_type;
    }

    public String getProfile_subject() {
        return profile_subject;
    }

    public void setProfile_subject(String profile_subject) {
        this.profile_subject = profile_subject;
    }

    public String getRealUserName() {
        return realUserName;
    }

    public void setRealUserName(String realUserName) {
        this.realUserName = realUserName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public static VoUsers loadFromJsonUsers(LBJSONObject jObject) {

        VoUsers result = new VoUsers();
        try {
            result.userId = jObject.getString("userId");
            result.userName = jObject.getString("userName");
            result.profile_image = jObject.getString("profile_image");
            result.tel=jObject.getString("tel");
            result.realUserName=jObject.getString("realUserName");
            result.profile_image_type = jObject.getString("profile_image_type");
            result.profile_subject = jObject.getString("profile_subject");
            result.staff = jObject.getString("staff");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
