package com.wave.messenger.vo;

import java.util.ArrayList;

import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by user on 2016-08-31.
 */
public class VoUserRead {
    private String roomId;
    private String userId;
    private String start_cid;
    private String start_chat_id;
    private String last_read_cid;
    private String last_read_chat_id;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStart_cid() {
        return start_cid;
    }

    public void setStart_cid(String start_cid) {
        this.start_cid = start_cid;
    }

    public String getStart_chat_id() {
        return start_chat_id;
    }

    public void setStart_chat_id(String start_chat_id) {
        this.start_chat_id = start_chat_id;
    }

    public String getLast_read_cid() {
        return last_read_cid;
    }

    public void setLast_read_cid(String last_read_cid) {
        this.last_read_cid = last_read_cid;
    }

    public String getLast_read_chat_id() {
        return last_read_chat_id;
    }

    public void setLast_read_chat_id(String last_read_chat_id) {
        this.last_read_chat_id = last_read_chat_id;
    }


    static public VoUserRead loadFromJson(LBJSONObject jObject) {

        VoUserRead result = new VoUserRead();
        try {
           // result.roomId = jObject.getString("roomId");
            result.userId = jObject.getString("userId");
            result.start_cid = jObject.getString("start_cid");
            result.start_chat_id = jObject.getString("start_chat_id");
            result.last_read_cid = jObject.getString("last_read_cid");
            result.last_read_chat_id = jObject.getString("last_read_chat_id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    static public VoUserRead loadFromJsonRev(LBJSONObject jObject) {

        VoUserRead result = new VoUserRead();
        try {
             result.roomId = jObject.getString("roomId");
            result.userId = jObject.getString("userId");
            result.start_cid = jObject.getString("start_cid");
            result.start_chat_id = jObject.getString("start_chat_id");
            result.last_read_cid = jObject.getString("last_read_cid");
            result.last_read_chat_id = jObject.getString("last_read_chat_id");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static public ArrayList<VoUserRead> loadFromJsonRev(LBJSONArray jArray) {
        ArrayList<VoUserRead> list = new ArrayList<>();

        try {
            for(Object o : jArray) {
                VoUserRead result = new VoUserRead();
                LBJSONObject jObject = (LBJSONObject) o;

                result.roomId = jObject.getString("roomId");
                result.userId = jObject.getString("userId");
                result.start_cid = jObject.getString("start_cid");
                result.start_chat_id = jObject.getString("start_chat_id");
                result.last_read_cid = jObject.getString("last_read_cid");
                result.last_read_chat_id = jObject.getString("last_read_chat_id");

                list.add(result);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
