package com.wave.messenger.vo;

import android.view.View;

import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.adapter.ChatRoomAdapter;

import java.io.Serializable;

public class VoChatData implements Serializable {
    private String chatData_seq = "";
    private String cid = "";
    private String chatId = "";
    private String ownerId = "";
    private String senderId = "";
    private String chatType = "";
    private String messageType = "";
    private String staffMsg = "";
    private String title = "";
    private String message = "";
    private String attachment = "";
    private String reg_date = "";
    private String imagePath = "";
    private String emoticonId = "";
    private String eventText = "";
    private String nonMemberUsers = "";
    private String messageMore = "";
    private int retryCnt = 0;
    private String status = "";
    private String attachmentLocal = "";
    private String cancel = "";
    private String unReadCount = "";
    private String senderName="";
    private String userId = "";
    private String userName = "";
    private String roomId = "";
    private String roomCreate = "";
    private String roomType = "";
    private String roomName = "";
    private String gId = "";
    private String inviteUser = "";
    private boolean showUnRead;
    private boolean showTimeLine;
    private String send = "";
    private String sendSuccess = "";
//    private boolean isMTS;
    private View.OnClickListener profileListener;
    private Activity_ChatRoom.PreViewListener mPreViewListener;
    private ChatRoomAdapter.ReSendListener mListener;
    boolean isSearched = false;

    public String getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(String unReadCount) {
        this.unReadCount = unReadCount;
    }

    public String getEventText() {
        return eventText;
    }

    public void setEventText(String eventText) {
        this.eventText = eventText;
    }

    public String getEmoticonId() {
        return emoticonId;
    }

    public void setEmoticonId(String emoticonId) {
        this.emoticonId = emoticonId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getStaffMsg() {
        return staffMsg;
    }

    public void setStaffMsg(String staffMsg) {
        this.staffMsg = staffMsg;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getNonMemberUsers() {
        return nonMemberUsers;
    }

    public void setNonMemberUsers(String nonMemberUsers) {
        this.nonMemberUsers = nonMemberUsers;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomCreate() {
        return roomCreate;
    }

    public void setRoomCreate(String roomCreate) {
        this.roomCreate = roomCreate;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getInviteUser() {
        return inviteUser;
    }

    public void setInviteUser(String inviteUser) {
        this.inviteUser = inviteUser;
    }

    public boolean isShowUnRead() {
        return showUnRead;
    }

    public void setShowUnRead(boolean showUnRead) {
        this.showUnRead = showUnRead;
    }

    public boolean isShowTimeLine() {
        return showTimeLine;
    }

    public void setShowTimeLine(boolean showTimeLine) {
        this.showTimeLine = showTimeLine;
    }

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public String getSendSuccess() {
        return sendSuccess;
    }

    public void setSendSuccess(String sendSuccess) {
        this.sendSuccess = sendSuccess;
    }

    public String getMessageMore() {
        return messageMore;
    }

    public void setMessageMore(String messageMore) {
        this.messageMore = messageMore;
    }

    public int getRetryCnt() {
        return retryCnt;
    }

    public void setRetryCnt(int retryCnt) {
        this.retryCnt = retryCnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttachmentLocal() {
        return attachmentLocal;
    }

    public void setAttachmentLocal(String attachmentLocal) {
        this.attachmentLocal = attachmentLocal;
    }

    public String getChatData_seq() {
        return chatData_seq;
    }

    public void setChatData_seq(String chatData_seq) {
        this.chatData_seq = chatData_seq;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    /*public boolean isMTS() {
        return isMTS;
    }*/

    /*public void setMTS(boolean MTS) {
        isMTS = MTS;
    }*/

    public View.OnClickListener getProfileListener() {
        return profileListener;
    }

    public void setProfileListener(View.OnClickListener profileListener) {
        this.profileListener = profileListener;
    }

    public ChatRoomAdapter.ReSendListener getListener() {
        return mListener;
    }

    public void setListener(ChatRoomAdapter.ReSendListener mListener) {
        this.mListener = mListener;
    }

    public Activity_ChatRoom.PreViewListener getPreView() {
        return mPreViewListener;
    }

    public void setPreView(Activity_ChatRoom.PreViewListener preViewListener) {
        mPreViewListener = preViewListener;
    }

    //
    public boolean isSearched() {
        return isSearched;
    }
    public void setSearched(boolean isSearched) {
        this.isSearched = isSearched;
    }

    @Override
    public String toString() {
        return "VoChatData{" +
                "chatData_seq='" + chatData_seq + '\'' +
                ", cid='" + cid + '\'' +
                ", chatId='" + chatId + '\'' +
                ", ownerId='" + ownerId + '\'' +
                ", senderId='" + senderId + '\'' +
                ", chatType='" + chatType + '\'' +
                ", messageType='" + messageType + '\'' +
                ", staffMsg='" + staffMsg + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", attachment='" + attachment + '\'' +
                ", reg_date='" + reg_date + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", emoticonId='" + emoticonId + '\'' +
                ", eventText='" + eventText + '\'' +
                ", nonMemberUsers='" + nonMemberUsers + '\'' +
                ", messageMore='" + messageMore + '\'' +
                ", retryCnt=" + retryCnt +
                ", status='" + status + '\'' +
                ", attachmentLocal='" + attachmentLocal + '\'' +
                ", cancel='" + cancel + '\'' +
                ", unReadCount='" + unReadCount + '\'' +
                ", senderName='" + senderName + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", roomId='" + roomId + '\'' +
                ", roomCreate='" + roomCreate + '\'' +
                ", roomType='" + roomType + '\'' +
                ", roomName='" + roomName + '\'' +
                ", gId='" + gId + '\'' +
                ", inviteUser='" + inviteUser + '\'' +
                ", showUnRead=" + showUnRead +
                ", showTimeLine=" + showTimeLine +
                ", send='" + send + '\'' +
                ", sendSuccess='" + sendSuccess + '\'' +
                ", profileListener=" + profileListener +
                ", mPreViewListener=" + mPreViewListener +
                ", mListener=" + mListener +
                ", isSearched=" + isSearched +
                '}';
    }
}
