package com.wave.messenger.vo;

import com.wave.messenger.Info.MessengerInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HanaMember {
	String userName = "";     //회원 이름 입니다.
	String userId = "";       //회원 ID 입니다.  
	String phoneNumber = "";  //회원의 전화번호입니다.
	int riskType = 0;         //투자위험도입니다. 종목 추천 을 받는 회원은 1 받지않는 회원은 0 입니다.
	String offYN = "";			//오프고객 여부
								//		Y.최근6개월 OFF약정만 발생시(ON 약정 미발생)
								//		N.최근6개월 OFF약정 미발생 또는 ON 약정 발생시"
	String gubunCode = "";
	String branchCode = "";
	String branchName = "";

		
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public int getRiskType() {
		return riskType;
	}
	public void setRiskType(int riskType) {
		this.riskType = riskType;
	}
	public String getOffYN()
	{
		return offYN;
	}
	public void setOffYN(String offYN)
	{
		this.offYN = offYN;
	}
	public String getGubunCode() {
		return gubunCode;
	}
	public void setGubunCode(String gubunCode) {
		this.gubunCode = gubunCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public static JSONArray loadCustomerContactData(ArrayList<HanaMember> buddyList) {
		JSONArray jArray = new JSONArray();
		try {

			for (int i = 0; i < buddyList.size(); i++) {
				try {
					JSONObject jo = new JSONObject();
					jo.put("addrPhone", buddyList.get(i).getPhoneNumber());
					jo.put("addrName", buddyList.get(i).getUserName());
					jo.put("addrId", buddyList.get(i).getUserId());
					if (MessengerInfo.getUserType().equals("UT_ME") || MessengerInfo.getUserType().equals("UT_ME_JU") || MessengerInfo.getUserType().equals("UT_ST_HQ")) {
						jo.put("customerType", "0");
					} else {
						if ("관리고객".equals(buddyList.get(i).getGubunCode()))
							jo.put("customerType", "0");
						else
							jo.put("customerType", "1");
					}
					jArray.put(jo);
				} catch (Exception ignored) {
				}
			}

		} catch (Exception ignored) {
		}

		return jArray;
	}
}
