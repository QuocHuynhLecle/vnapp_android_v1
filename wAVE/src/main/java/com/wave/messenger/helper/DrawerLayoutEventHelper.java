package com.wave.messenger.helper;

/**
 * Created by aveapp on 2017. 7. 6..
 */

public class DrawerLayoutEventHelper {
    public String event;
    public String item;

    public DrawerLayoutEventHelper(String event, String item) {
        this.event = event;
        this.item = item;
    }

    public String getEvent() {
        return event;
    }

    public String getItem() {
        return item;
    }
}