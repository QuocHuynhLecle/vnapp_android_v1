package com.wave.messenger.helper;

import android.graphics.Typeface;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Helper for topBar <br/>
 * Inits helper and sets onClick listeners
 */
public class UITopbarHelper {

    private LinearLayout title;
    private ImageButton imageButtonMenu;
    private TextView tv_topbar_title, textViewCount;
    private ImageView ivHomeBtn, ivSearchFriendBtn;

    public UITopbarHelper(LinearLayout layout) {
        this.imageButtonMenu = (ImageButton) layout.findViewById(R.id.imageButtonMenu);
        this.title = (LinearLayout) layout.findViewById(R.id.linearLayout1);
        this.tv_topbar_title = (TextView) layout.findViewById(R.id.tv_topbar_title);
        this.textViewCount = (TextView) layout.findViewById(R.id.textViewCount);
        this.ivHomeBtn = (ImageView) layout.findViewById(R.id.imgb_back_btn);
//        this.ivSearchFriendBtn = (ImageView) layout.findViewById(R.id.ivSearchFriendBtn);
    }

    public void setMenuIconVisibility(int Visibility){
        imageButtonMenu.setVisibility(Visibility);
    }

    public void setMenuIcon(int resId){
        this.imageButtonMenu.setBackgroundResource(resId);
    }


    public void setClickable(boolean imageButtonMenuClickable) {
        imageButtonMenu.setClickable(imageButtonMenuClickable);
    }

    /**
     * 탑바가 보이는지 안보이는지 설정한다.
     *
     * @param homeBtn 홈 버튼이 보임 안보임?
     * @param title   제목이 보임?
     * @param count   숫자(친구 숫자)가 보임?
     * @param menu    메뉴 아이콘 보임?
     */
    public void setVisibility(int homeBtn, int title, int count, int menu) {
        this.ivHomeBtn.setVisibility(homeBtn);
        this.tv_topbar_title.setVisibility(title);
        this.textViewCount.setVisibility(count);
        this.imageButtonMenu.setVisibility(menu);
    }

    public void setHomeVisibility(int homeBtn) {
        this.ivHomeBtn.setVisibility(homeBtn);
    }

    /**
     * 탑바의 title을 입력한다.
     *
     * @param title
     */
    public void setText(String title) {
        this.tv_topbar_title.setText(title);
    }

    public void setMargin(int dp) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(dp, 0, 0, 0);
        this.tv_topbar_title.setLayoutParams(params);
    }

    public void setLinearMargin(int dp) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) title.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.setMargins(dp, 0, 0, 0);
        this.title.setLayoutParams(params);
    }

    public void setGravity(int gravity) {
        this.title.setGravity(gravity);
    }

    public void setTextColor(int color) {
        this.tv_topbar_title.setTextColor(color);
    }

    public void setTextStyle(Typeface style) {
        this.tv_topbar_title.setTypeface(style);
    }

    public void setCount(String count) {
        this.textViewCount.setText(count);
    }

    public void setFriendVisibility(int visible) {
        this.textViewCount.setVisibility(visible);
    }


    public void setMenuBtnClickListener(OnClickListener listener) {
        this.imageButtonMenu.setOnClickListener(listener);
    }

    public void setHomeBtnClickListener(OnClickListener listener) {
        this.ivHomeBtn.setOnClickListener(listener);
    }

    /*public void setSearchFriendClickListener(OnClickListener listener) {
        this.ivSearchFriendBtn.setOnClickListener(listener);
    }

    public void setSearchVisibility(int visible) {
        ivSearchFriendBtn.setVisibility(visible);
    }*/

    public void setHomeIcon(int resid) {
        this.ivHomeBtn.setImageResource(resid);
    }
}
