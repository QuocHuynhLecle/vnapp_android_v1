package com.wave.messenger.helper;


import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

public class UIChatTopbarHelper {

	private ImageButton imageButtonBack;
	private ImageView imageButtonMenu;
	private ImageView imageButtonMTS;
	private ImageView imageButtonSearch;
	private TextView textViewTitle;
	private TextView textViewCount;

	public UIChatTopbarHelper(LinearLayout layout){
		this.imageButtonBack=(ImageButton)layout.findViewById(R.id.imageButtonBack);
		this.imageButtonMenu=(ImageView)layout.findViewById(R.id.imageButtonMenu);
		this.imageButtonMTS=(ImageView)layout.findViewById(R.id.imageButtonMTS);
		this.imageButtonSearch=(ImageView)layout.findViewById(R.id.imageButtonSearch);
		this.textViewTitle=(TextView)layout.findViewById(R.id.textViewTitle);
		this.textViewCount=(TextView)layout.findViewById(R.id.textViewCount);
	}

	public void setButtonBackClickListener(OnClickListener listener){
		imageButtonBack.setOnClickListener(listener);
	}
	public void setButtonMenuClickListener(OnClickListener listener){
		imageButtonMenu.setOnClickListener(listener);
	}
	public void setButtonMTSClickListener(OnClickListener listener) {
		imageButtonMTS.setOnClickListener(listener);
	}
	public void setButtonSearchClickListener(OnClickListener listener){
		imageButtonSearch.setOnClickListener(listener);
	}
	public void setCount(String count){
		textViewCount.setVisibility(View.VISIBLE);
		textViewCount.setText("(" + count + ")");
	}
	public void setCountGONE(){
		textViewCount.setVisibility(View.GONE);

	}
	public void setClickable(boolean imageButtonMenuClickable) {
		imageButtonMenu.setClickable(imageButtonMenuClickable);
	}
	public void setText(String title){
		textViewTitle.setText(title);
	}

	public TextView getTextTitle(){
		return textViewTitle;
	}

	public void setVisibleMenu(boolean value){
		if(value){
			imageButtonMenu.setVisibility(View.VISIBLE);
			imageButtonMTS.setVisibility(View.VISIBLE);
			imageButtonSearch.setVisibility(View.VISIBLE);
		}else{
			imageButtonMenu.setVisibility(View.INVISIBLE);
			imageButtonMTS.setVisibility(View.INVISIBLE);
			imageButtonSearch.setVisibility(View.INVISIBLE);
		}
	}
}
