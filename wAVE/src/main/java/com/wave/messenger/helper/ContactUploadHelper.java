package com.wave.messenger.helper;

import android.content.Context;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.LocalContactData;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;

import org.json.JSONArray;
import org.json.JSONObject;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class ContactUploadHelper {
    static boolean isRunning = false;
    static boolean isRunning1 = false;

    public static void requestUpload(final Context context, final boolean allSync, final OnCompleteListener onCompleteListener) {
        try {
            if (isRunning == false) {
                isRunning = true;

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        try {
                            String firstUploaded = MessengerInfo.getFirstContactUploaded(context);
                            String clearBuddy = MessengerInfo.getClearBuddy(context);
                            String userId = MessengerInfo.getUserId(context);

                            JSONArray contactList;

                            if ("1".equals(firstUploaded) || "1".equals(clearBuddy)) {
                                contactList = LocalContactData.loadContactData(context, true);
                            } else {
                                contactList = LocalContactData.loadContactData(context, allSync);
                            }

                            if (contactList != null && contactList.length() > 0) {
                                PacketBody body = new PacketBody();
                                body.put("first", firstUploaded);
                                body.put("userId", userId);
                                body.put("params", contactList);

                                ErrorController.showMessage("hoya", "[ContactUploadHelper.PTG_IMS_ADDRESS]::allSync::" + allSync);
                                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_UPLOAD, body);
                            } else {
                                LocalContactData.clear();

                                PacketBody body = new PacketBody();
                                JSONObject value = new JSONObject();
                                value.put("userId", userId);
                                value.put("mode", ""); // All Friends
                                body.put("params", value);

                                ErrorController.showMessage("hoya", "[ContactUploadHelper.PTC_IMS_ADDRESS_LIST]");
                                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_LIST, body);
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        } finally {
                            if (onCompleteListener != null) {
                                onCompleteListener.onComplete(true);
                            }

                            isRunning = false;
                        }
                    }
                });

                thread.start();
            } else {
                if (onCompleteListener != null) {
                    onCompleteListener.onComplete(false);
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * 대화방 목록요청
     * @param onCompleteListener
     * @param context
     */
    public static void requestChatList(final OnCompleteListener onCompleteListener, final Context context) {
        try {
            if (isRunning1 == false) {
                isRunning1 = true;

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        try {
                            PacketBody body = new PacketBody();
                            JSONObject value = new JSONObject();
                            value.put("userId", MessengerInfo.getUserId(context));
                            body.put("params", value);
                            LogTrace.E("ChatList request userID = " + MessengerInfo.getUserId(context));
                            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                                    PacketTypes.PTC_IMS_CHATROOM_LIST, body);

                        } catch (Exception e) {
                            // TODO: handle exception
                        } finally {
                            onCompleteListener.onComplete(true);
                            isRunning1 = false;
                        }
                    }
                });

                thread.start();
            } else {
                onCompleteListener.onComplete(false);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public interface OnCompleteListener {
        void onComplete(boolean result);
    }
}
