package com.wave.messenger.helper;

import com.wave.messenger.mvp.ChatList.ChatListListener;
import com.wave.messenger.mvp.ChatRoom.ChatRoomListener;

/**
 * Created by aveapp on 2017. 7. 6..
 */

public class FragmentEventHelper {
    public String event;
    public ChatRoomListener roomListener;
    public ChatListListener listListener;

    public FragmentEventHelper(String event, ChatRoomListener roomListener, ChatListListener listListener) {
        this.event = event;
        this.roomListener = roomListener;
        this.listListener = listListener;
    }

    public String getEvent() {
        return event;
    }

    public ChatRoomListener getRoomListener() {
        return roomListener;
    }

    public ChatListListener getListListener() {
        return listListener;
    }
}