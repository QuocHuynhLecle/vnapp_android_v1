package com.wave.messenger.mvp.Gallery;

import android.content.Context;
import android.view.View;

import com.wave.messenger.cell.CellGalleryDetail;
import com.wave.messenger.utility.OnListClickCallback;

import java.util.Collections;
import java.util.List;

public class GalleryDetailDataModel {

	private List<GalleryDataModel> data = Collections.emptyList();

	public GalleryDetailDataModel(List<GalleryDataModel> data) {
		this.data = data;
	}

	public View getView(Context context, OnListClickCallback callback){
		return new CellGalleryDetail(context, this, callback);
	}
	
	
	public List<GalleryDataModel> getData() {
		return data;
	}

	public void setData(List<GalleryDataModel> data) {
		this.data = data;
	}
	
}
