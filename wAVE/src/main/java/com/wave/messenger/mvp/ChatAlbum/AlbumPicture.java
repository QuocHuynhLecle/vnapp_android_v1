package com.wave.messenger.mvp.ChatAlbum;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by user on 2016-09-07.
 */
public class AlbumPicture implements Serializable {

    private String urlPath="";
    private Bitmap bitmap;
    private boolean isSelected = false;
    private boolean isSelectionMode = false;
    private String chatId="";
    private String type="0";
    private boolean existImage = false;
    public AlbumPicture(String urlPath, String chatId){
        this.urlPath = urlPath;
        this.isSelected = false;
        this.chatId=chatId;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelectionMode() {
        return isSelectionMode;
    }

    public void setSelectionMode(boolean selectionMode) {
        isSelectionMode = selectionMode;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isExistImage() {
        return existImage;
    }

    public void setExistImage(boolean existImage) {
        this.existImage = existImage;
    }
}

