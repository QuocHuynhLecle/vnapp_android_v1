package com.wave.messenger.mvp.ManageFriend;

import android.content.Context;
import android.os.Handler;

import com.wave.messenger.helper.ContactUploadHelper;
import com.wave.messenger.util.SharedObject;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;

/**
 * Created by user on 2016-09-01.
 */
public class ManageFriendPresenter implements MOAEventListener {
    private Handler handler;

    public ManageFriendPresenter() {
    }

    public void changeAutoSetting(Context context, boolean setting) {
        SharedObject.setProperty_boolean(context, "autoContact", setting);
    }

    public boolean getAutoSetting(Context context) {
        if (SharedObject.getProperty_boolean(context, "autoContact", true)) {
            return true;
        }
        return false;
    }

    public void RefreshFriendList(Context context, Handler handler) {
        this.handler = handler;
        MOAClient.getInstance().addEventListener(this);
        ContactUploadHelper.requestUpload(context, true, null);
    }

    public String getSyncDate(Context context) {
        String syncDate = SharedObject.getProperty_string(context, "syncDate", "");
        return syncDate;
    }

    public void setSyncDate(Context context, String date) {
        SharedObject.setProperty_string(context, "syncDate", date);
    }

    @Override
    public void OnEvent(MOAData data) {
        handler.sendMessage(handler.obtainMessage(data.ptc, data));
    }
}
