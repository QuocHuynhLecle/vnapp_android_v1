package com.wave.messenger.mvp.Profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.wave.messenger.sdk.Util.ImageUploadData;
import com.wave.messenger.util.Utility;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class ProfilePresenter implements IProfileImageDownloadListener {

	private IProfileView view;
	private ImageUploadData upload;
	private ProfileModel model;

    private Context context;

	public ProfileModel getModel() {
		return model;
	}

	public IProfileView getView() {
		return view;
	}

	public ProfilePresenter(IProfileView view) {
		this.view = view;
		upload = new ImageUploadData();
		model = new ProfileModel();
	}

	/**
	 * 콜백 : onImageDownloadSuccess, onImagedownloadFailed.
	 */
	/*public void downloadImageFromServer(String id) {
		model.loadUserPicture(this, id);
	}*/

	public String saveImageAsFile(Context context,Bitmap bmp) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		File file = new File(Utility.getChatImageUri(context).getPath());
		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray(); // convert camera photo to
													// byte array
		// save it in your external storage.
		FileOutputStream fo;
		try {
			fo = new FileOutputStream(file);
			fo.write(byteArray);
			fo.flush();
			fo.close();
			ErrorController.showMessage("File Size : " + file.length());
			view.onSavePictureSuccess(file.getPath());
		} catch (Exception e) {
			e.printStackTrace();
			view.onSavePictureSuccess(null);
		}

		return null;
	}
	
	/*public void hideFriend(VoFriendList friendToHide, Context context){
		LogTrace.E("ProfilePresenter hideFriend");

		try {
			if(!friendToHide.isMember()){
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				//db.hideBuddy(friendToHide.getUserId());
				return;
			}

			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToHide.getUserId());
			value.put("mode", "1");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body);
			BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
			db.hideBuddy(friendToHide.getUserId());
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("hide : " +friendToHide.getUserName());
		}
	}*/

	/*public void unHideFriend(VoFriendList friendToUnhide, Context context){
		LogTrace.E("ProfilePresenter unHideFriend");
		try {
			if(!friendToUnhide.isMember()){
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				db.unHideBuddy(friendToUnhide.getUserId(), null);
				return;
			}

			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToUnhide.getUserId());
			value.put("mode", "0");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body);
			BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
			db.unHideBuddy(friendToUnhide.getUserId(), null);
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("unhide : " + friendToUnhide.getUserName());
		}
	}*/
	
	/*public void blockFriend(VoFriendList friendToBlock, Context context){
		//TODO : Logic to hide friend 
		//Callback to : view.onBlockSuccess, onBlockFailed
		try {
			if(!friendToBlock.isMember()){
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				db.blockBuddy(friendToBlock.getUserId());
				return;
			}

			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToBlock.getUserId());
			value.put("mode", "1");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_BLOCK, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
            db.blockBuddy(friendToBlock.getUserId());

		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("block : " +friendToBlock.getUserName());
		}
	}*/

	/*public void unBlockFriend(VoFriendList friendToUnBlock, Context context){
		try {
			if(!friendToUnBlock.isMember()){
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				db.unblockBuddy(friendToUnBlock.getUserId(), null);
				return;
			}

			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToUnBlock.getUserId());
			value.put("mode", "0");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_BLOCK, body);
			BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
			db.unblockBuddy(friendToUnBlock.getUserId(), null);
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("block : " +friendToUnBlock.getUserName());
		}
	}*/

	/*public void booMarkFriend(VoFriendList friendToBookMark, Context context){
		try {
			if(!friendToBookMark.isMember()){
				ErrorController.showMessage("[ProfileState_Friend] bookmarkFriend : friend is not a member");
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				db.favoriteBuddy(friendToBookMark.getUserId());
				return;
			}

			ErrorController.showMessage("[ProfileState_Friend] bookmarkFriend : friend is a member - name : " + friendToBookMark.getUserId());
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToBookMark.getUserId());
			value.put("mode", "1");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_FAVORITE, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
            ErrorController.showMessage("[Framgnet_main] bookmark - id from server : " + friendToBookMark.getUserId());
            db.favoriteBuddy(friendToBookMark.getUserId());
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("[ProfilePresenter] add favorite : " +friendToBookMark.getUserName());
		}
	}*/

	/*public void unbooMarkFriend(VoFriendList friendToUnBookMark, Context context){
		try {
			if(!friendToUnBookMark.isMember()){
				BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
				db.unfavoriteBuddy(friendToUnBookMark.getUserId());
				return;
			}

			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("users", friendToUnBookMark.getUserId());
			value.put("mode", "0");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_FAVORITE, body);
			BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
			db.unfavoriteBuddy(friendToUnBookMark.getUserId());
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			ErrorController.showMessage("favorite : " +friendToUnBookMark.getUserName());
		}
	}*/

	@Override
	public void onImageDownloadSuccess(Bitmap bitmap) {
		//파일 다운로드 성공
		ErrorController.showMessage("[ProfilePresenter] Success to download image");
		view.onDownloadPictureComplete(bitmap);
	}

	@Override
	public void onImageDownloadFailed() {
		//파일 다운로드 실패
		ErrorController.showMessage("[ProfilePresenter] Failed to download image");
		view.onDownloadPictureFailed();
	}

	public void saveFileAsScaledImage(Context context, String fullPath, int sampleSize) {
		LogTrace.E("saveFileAsScaledImage "+fullPath);

		File image = new File(fullPath);
        Bitmap resultBitmap = null;
		Bitmap saveBitmap=null;
        try{
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getPath(), bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            return;
        }

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = sampleSize;
        resultBitmap = BitmapFactory.decodeFile(image.getPath(), opts);
		saveBitmap= Utility.modifyOrientation(resultBitmap, fullPath); // getRotatedBitmap(resultBitmap,getPhotoOrientationDegree(fullPath));
        }catch(Exception e){
    		e.printStackTrace();
        }
		saveImageAsFile(context, saveBitmap);
	}

	/*public void chaneUserName(Context context, String userId, String changedUserName){
        this.context = context;
		model.changeUserNameRequest(userId, changedUserName, new ResultHandler());
	}*/

	public void deleteUserPicture(String userId) {
		model.deleteUserPicture(userId);
	}

	/*public void changeFriendName(Context context, String userId, String changedUserName, String tel) {
		model.changeFriendNameRequest(MessengerInfo.getUserId(context), userId, changedUserName, tel, new ResultHandler());
	}*/

	/*private class ResultHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			try {
				MOAData data = (MOAData) msg.obj;
				LBJSONObject obj = data.body;
				String result;

				switch (msg.what) {
					case PacketTypes.PTC_IMS_USER_CHANGE_USERNAME:
						ErrorController.showMessage("ProfilePresenter resultHandler data check : " + obj.toString());
						result = obj.getString("result");

						if ("success".equals(result)) {
							String userId = obj.getJson("params").getString("userId");
							String userName = obj.getJson("params").getString("userName");
							if (userId.equals(MessengerInfo.getUserId(context))) {
								//내 이름이 변경된 경우...
								MessengerInfo.setUsername(context, userName);
							}
							model.saveNameChangeOnLocalDB(userId, userName, context, view);
						} else {
							view.onChangeNameFailed();
						}
						break;

					case PacketTypes.PTC_IMS_ADDRESS_CHANGE_NAME:
						ErrorController.showMessage("ProfilePresenter resultHandler data check [PTC_IMS_ADDRESS_CHANGE_NAME]: " + obj.toString());
						result = obj.getString("result");

						if ("success".equals(result)) {
							String bid = obj.getJson("params").getString("bid");
							String userName = obj.getJson("params").getString("userName");

							model.saveNameChangeOnLocalDB2(bid, userName, context, view);
						} else {
							view.onChangeNameFailed();
						}
						break;
				}
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}//end of ResultHandler*/
}
