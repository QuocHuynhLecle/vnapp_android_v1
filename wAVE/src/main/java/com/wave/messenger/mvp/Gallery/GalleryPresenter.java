package com.wave.messenger.mvp.Gallery;

import android.app.Activity;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.wave.messenger.utility.ErrorController;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class GalleryPresenter {

    public GalleryPresenter() {
    }

    public List<GalleryDataModel> getDetailDatas2(String fullDirectory, Activity context) {
        List<GalleryDataModel> tempList = new ArrayList<>();

        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media._ID;

        Cursor imageCursor = context.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);

        ErrorController.showMessage(fullDirectory);

        if (imageCursor != null && imageCursor.getCount() > 0) {

            while (imageCursor.moveToNext()) {
                int dataColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);

                String fullPath = imageCursor.getString(dataColumnIndex);

                // Drop empty files (Files with only names and no contents).
                if (fullPath != null && !TextUtils.isEmpty(fullPath) && new File(fullPath).length() != 0) {
                    if (fullPath.contains(fullDirectory)) {
                        tempList.add(new GalleryDataModel(fullPath,
                                getDirectoryName(fullPath),
                                getDirectoryFullName(fullPath)));
                    }
                }// end of if
            }// end of while
        }// end of if

        ErrorController.showMessage(tempList.size() + "");
        Collections.reverse(tempList);
        return tempList;
    }

    /**
     * Returns Title list of gallery. <br/>
     * Title refers to the name of each gallery.
     *
     * @param context
     * @return
     */
    public List<GalleryDataModel> getTitles(Activity context) {

        Map<String, String> map = new HashMap<String, String>();

        List<GalleryDataModel> data = new ArrayList<GalleryDataModel>();

        final String[] columns = {MediaStore.Images.Media.DATA,
                MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media._ID;

        Cursor imageCursor = context.managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        if (imageCursor != null && imageCursor.getCount() > 0) {
            String currentDirectory = " ";
            while (imageCursor.moveToNext()) {
                int dataColumnIndex = imageCursor
                        .getColumnIndex(MediaStore.Images.Media.DATA);

                String fullPath = imageCursor.getString(dataColumnIndex);

                // Drop empty files (Files with only names and no contents).
                if (new File(fullPath).length() != 0) {
                    String temp = getDirectoryFullName(fullPath);
                    if (!currentDirectory.equals(temp)) {
                        currentDirectory = temp;
                        try {
                            map.put(currentDirectory, fullPath);
                        } catch (Exception e) {
                        }
                    }
                }// end of if
            }// end of while

            for (Entry<String, String> e : map.entrySet()) {
                String key = e.getKey();
                String value = e.getValue();
                data.add(new GalleryDataModel(value, getDirectoryName(value),
                        key));
            }// end of for
        }//end of if
        List<GalleryDataModel> result = new ArrayList<>();


        for (GalleryDataModel galleryDataModel : data) {

            if (galleryDataModel.getDirectoryName().contains("Camera") || galleryDataModel.getDirectoryName().contains("카메라")) {
                if (result.size() != 0) {
                    ErrorController.showMessage("[GalleryPresenter] Camera if: " + galleryDataModel.getDirectoryName());
                    result.add(0, galleryDataModel);
                } else {
                    ErrorController.showMessage("[GalleryPresenter] Camera else: " + galleryDataModel.getDirectoryName());
                    result.add(galleryDataModel);
                }
                continue;
            }

            if (galleryDataModel.getDirectoryName().contains("Screenshot") || galleryDataModel.getDirectoryName().contains("스크린샷")) {
                if (result.size() > 1) {
                    ErrorController.showMessage("[GalleryPresenter] Screenshot if: " + galleryDataModel.getDirectoryName());
                    if(result.size()>2){
                        if(result.get(1).getDirectoryName().contains("Camera") || result.get(1).getDirectoryName().contains("카메라")){
                            result.add(2, galleryDataModel);
                        }else{
                            result.add(1, galleryDataModel);
                        }
                    }else{
                        result.add(1, galleryDataModel);
                    }

                } else {
                    ErrorController.showMessage("[GalleryPresenter] Screenshot else: " + galleryDataModel.getDirectoryName());
                    result.add(galleryDataModel);
                }
                continue;
            }

            if (galleryDataModel.getDirectoryName().contains("KakaoTalk") || galleryDataModel.getDirectoryName().contains("카카오톡")) {
                if(result.size()>2) {
                    ErrorController.showMessage("[GalleryPresenter] Kakaotalk if: " + galleryDataModel.getDirectoryName());
                    result.add(2, galleryDataModel);
                }else{
                    ErrorController.showMessage("[GalleryPresenter] Kakaotalk else: " + galleryDataModel.getDirectoryName());
                    result.add(galleryDataModel);
                }
                continue;
            }
        }

        for (GalleryDataModel galleryDataModel : data) {
               if(!result.contains(galleryDataModel)){
                   result.add(galleryDataModel);
               }
        }

        return result;
    }

    public String getDirectoryFullName(String fullPath) {
        String result = "/";
        String[] temp = fullPath.split("/");
        for (int i = 1; i < temp.length - 1; i++) {
            result += temp[i] + "/";
        }
        result = result.substring(0, result.length() - 1);

        return result;
    }

    public String getDirectoryName(String fullPath) {
        String result = "/";
        String[] temp = fullPath.split("/");

        result = temp[temp.length - 2];
        return result;
    }

}
