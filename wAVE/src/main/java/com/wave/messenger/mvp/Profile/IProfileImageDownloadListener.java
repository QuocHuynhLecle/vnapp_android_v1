package com.wave.messenger.mvp.Profile;

import android.graphics.Bitmap;

public interface IProfileImageDownloadListener {
	void onImageDownloadSuccess(Bitmap bitmap);
	void onImageDownloadFailed();
}
