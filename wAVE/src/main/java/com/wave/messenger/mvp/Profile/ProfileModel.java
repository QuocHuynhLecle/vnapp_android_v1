package com.wave.messenger.mvp.Profile;

import android.os.AsyncTask;
import android.os.Handler;

import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class ProfileModel implements MOAEventListener {

	private Handler handler;

	public void changeUserNameRequest(String userId, String userName, Handler handler){
		this.handler = handler;
		try {
			ErrorController.showMessage("ProfileModel - changeUserNameRequest");
			MOAClient client = MOAClient.getInstance();
			client.addEventListener(this);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", userId);
			value.put("userName", userName);
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER,
					PacketTypes.PTC_IMS_USER_CHANGE_USERNAME  , body);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void changeFriendNameRequest(String userId, String bid, String changedName, String tel, Handler handler){
		this.handler = handler;
		try {
			ErrorController.showMessage("ProfileModel - changeUserNameRequest - friend");
			MOAClient client = MOAClient.getInstance();
			client.addEventListener(this);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", userId);
			value.put("bid", bid);
			value.put("tel", tel);
			value.put("userName", changedName);
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_CHANGE_NAME, body);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void OnEvent(MOAData data) {
		ErrorController.showMessage("ProfileModel - OnEvent");
		handler.sendMessage(handler.obtainMessage(data.ptc, data));
	}

	/*public void saveNameChangeOnLocalDB(String userId, String userName, Context context, IProfileView view) {
		ErrorController.showMessage("ProfileModel - saveOnLocalDB : " + userId + ", " + userName);
		BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
		db1.updateBuddyName(userId, userName, view);
	}*/

	/*public void saveNameChangeOnLocalDB2(String bid, String userName, Context context, IProfileView view) {
		ErrorController.showMessage("ProfileModel - saveOnLocalDB member friend : " + bid + ", " + userName);
		BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
		db1.updateBuddyName2(bid, userName, view);
	}*/

	/*public void loadUserPicture(IProfileImageDownloadListener listener, String userId){
		new ImageDownloader(listener).execute("3", userId);
	}*/

	public void deleteUserPicture(final String userId) {
		new AsyncTask<Void, Void, Void>(){
			private String address="";

			@Override
			protected Void doInBackground(Void... params) {
				address = Constants.DEL_URL + userId;
				ErrorController.showMessage("[ProfileModel] deleteUserPicture : " + address);
				try {
					URL url_obj = new URL(address);
					HttpURLConnection con = (HttpURLConnection)url_obj.openConnection();
					con.setRequestMethod("GET");              // default is GET
					con.setDoInput(true);                            // default is true
					con.setDoOutput(true);                          //default is false
					InputStream in = con.getInputStream();

					InputStreamReader isw = new InputStreamReader(in);

					int data = isw.read();
					String result ="";
					while (data != -1) {
						char current = (char) data;
						data = isw.read();
						result +=current;
					}
					ErrorController.showMessage("[ProfileModel] result : " + result );
				}catch (Exception e){
					e.printStackTrace();
				}
				return null;
			}
		}.execute();

	}

	/*public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
		private String address = Constants.CHATDAWN_PROC + "?";
		private IProfileImageDownloadListener listener;

		public ImageDownloader(IProfileImageDownloadListener listener) {
			super();
			this.listener = listener;
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			try {
				String fullAddress = address + "type=" + params[0] + "&userid="	+ params[1];
				ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

				URL url = new URL(fullAddress);
				URLConnection conn = url.openConnection();
				conn.connect();
				BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
				Bitmap image = BitmapFactory.decodeStream(bis);
				bis.close();
				ErrorController.showMessage("end of doinBg");
				Statics.getProfileImageCache().put(params[1], new ThumbnailBitmap(image, false));
				return image;
			} catch (Exception e) {
				ErrorController.showMessage("[ProfileModel] downloadImage : error");
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			if(result!=null){
				listener.onImageDownloadSuccess(result);
			}else{
				listener.onImageDownloadFailed();
			}
		}
	}*/
}
