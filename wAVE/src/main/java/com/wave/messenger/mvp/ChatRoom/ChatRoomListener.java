package com.wave.messenger.mvp.ChatRoom;

import com.wave.messenger.vo.VoChatData;

import moa.android.api.MOAData;

/**
 * Created by user on 2016-09-02.
 */
public interface ChatRoomListener {
    void receiveMsg(String roomId, VoChatData data);
    void sendMsg(MOAData data);
    void exitRoom(boolean result, MOAData data);
    void createRoom(MOAData data);
    void chatList(MOAData data);
    void recRead();
    void onChatListResult(boolean result);
    void onChatListUpdate(String roomId);
    void onChatRoomInfo(MOAData data);
    void onChatReadMy(MOAData data);
}
