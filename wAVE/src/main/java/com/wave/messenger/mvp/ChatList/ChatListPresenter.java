package com.wave.messenger.mvp.ChatList;

import android.content.Context;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.vo.VoChatList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class ChatListPresenter {
	private ChatListListener view;
	public ChatListPresenter(ChatListListener view) {
		this.view = view;
	}

	public void chatRoomInfo(Context context, String roomId, String uinfo){
		try {
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("roomId", roomId);
			if (!"".equals(uinfo))
				value.put("uinfo", uinfo);
			else
				value.put("uinfo", "1");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
					PacketTypes.PTC_IMS_CHATROOM_INFO, body);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/*public List<String> search(CharSequence s, Context context) {
		ChatListDbHelper db = LocalDB.getChatListDbHelper(context);
		return db.getAllRoomName(s);
	}//end of search*/

	/**
	 * 대화 목록 검색 <br/>
	 * 글자 / 초성 순으로 검색함. <br/>
	 *
	 * @param query 사용자가 입력한 검색어
	 */
	public void searchJongmokChatList(String query, ArrayList<VoChatList> chatList, ArrayList<VoChatList> chatList2) {
		LogTrace.E("searchChatList");

		ArrayList<String> groupList = new ArrayList<String>();
		ArrayList<VoChatList> childList = new ArrayList<VoChatList>();
		groupList.add(Constants.CHATLISTTYPE_JONGMOK_ING);
		groupList.add(Constants.CHATLISTTYPE_JONGMOK);
		try {
			// 숫자가 검색어일 경우...
			int numberCheck = Integer.parseInt(query);
			ArrayList<VoChatList> tempList = new ArrayList<VoChatList>();
			// search with number
			for (VoChatList flvo : chatList2) {

				if (flvo.getRoom_name().contains(query)) {
					tempList.add(flvo);
				}
			}
			//ErrorController.showMessage("Num Search : result : " + tempList3.size());
			VoChatList vo32 = new VoChatList();
			vo32.setList(tempList);
			vo32.setRoom_name(Constants.CHATLISTTYPE_JONGMOK_ING);

			ArrayList<VoChatList> tempList2 = new ArrayList<VoChatList>();
			// search with number
			for (VoChatList flvo : chatList) {

				if (flvo.getRoom_name().contains(query)) {
					tempList2.add(flvo);
				}
			}
			//ErrorController.showMessage("Num Search : result : " + tempList3.size());
			VoChatList vo = new VoChatList();
			vo.setList(tempList2);
			vo.setRoom_name(Constants.CHATLISTTYPE_JONGMOK);
			chatList.add(vo32);
			childList.add(vo);
			view.showSearchList(groupList, childList);

		} catch (Exception e) {
			// 글자일 경우
			try {
				ArrayList<VoChatList> tempList = new ArrayList<VoChatList>();
				for (VoChatList flvo : chatList2) {
					if (flvo.getRoom_name().contains(query)) {
						tempList.add(flvo);
					}
				}

				ArrayList<VoChatList> tempList2 = new ArrayList<VoChatList>();
				for (VoChatList flvo : chatList) {
					if (flvo.getRoom_name().contains(query)) {
						tempList2.add(flvo);
					}
				}

				if (tempList.size() != 0) { // 글자 검색 결과가 있다.
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList);
					vo32.setRoom_name(Constants.CHATLISTTYPE_JONGMOK_ING);
					childList.add(vo32);
				} else { // 글자 검색 결과가 없다. -> 초성으로 검색
					for (VoChatList flvo : chatList2) {
						if (StringProcesser.matchString(flvo.getRoom_name(), query)) {
							tempList.add(flvo);
						}
					}
					//ErrorController.showMessage("Char Search : result : " + tempList3.size());
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList);
					vo32.setRoom_name(Constants.CHATLISTTYPE_JONGMOK_ING);
					childList.add(vo32);
				}

				if (tempList2.size() != 0) { // 글자 검색 결과가 있다.
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList2);
					vo32.setRoom_name(Constants.CHATLISTTYPE_JONGMOK);
					childList.add(vo32);
				} else { // 글자 검색 결과가 없다. -> 초성으로 검색
					for (VoChatList flvo : chatList) {
						if (StringProcesser.matchString(flvo.getRoom_name(), query)) {
							tempList2.add(flvo);
						}
					}
					//ErrorController.showMessage("Char Search : result : " + tempList3.size());
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList2);
					vo32.setRoom_name(Constants.CHATLISTTYPE_JONGMOK);
					childList.add(vo32);
				}
				view.showSearchList(groupList, childList);
			} catch (Exception ex) {

			}
		}// end of catch
	}

	/**
	 * 대화 목록 검색 <br/>
	 * 글자 / 초성 순으로 검색함. <br/>
	 *
	 * @param query 사용자가 입력한 검색어
	 */
	public void searchChatList(String query, ArrayList<VoChatList> chatList) {
		LogTrace.E("searchChatList");

		ArrayList<String> groupList = new ArrayList<String>();
		ArrayList<VoChatList> childList = new ArrayList<VoChatList>();
		groupList.add(Constants.CHATLISTTYPE_MYCHAT);
		try {
			// 숫자가 검색어일 경우...
			int numberCheck = Integer.parseInt(query);
			ArrayList<VoChatList> tempList3 = new ArrayList<VoChatList>();
			// search with number
			for (VoChatList flvo : chatList) {

				if (flvo.getRoom_name().contains(query)) {
					tempList3.add(flvo);
				}
			}
			//ErrorController.showMessage("Num Search : result : " + tempList3.size());
			VoChatList vo32 = new VoChatList();
			vo32.setList(tempList3);
			vo32.setRoom_name(Constants.CHATLISTTYPE_MYCHAT);
			childList.add(vo32);
			view.showSearchList(groupList, childList);

		} catch (Exception e) {
			// 글자일 경우
			try {
				ArrayList<VoChatList> tempList3 = new ArrayList<VoChatList>();
				for (VoChatList flvo : chatList) {
					if (flvo.getRoom_name().contains(query)) {
						tempList3.add(flvo);
					}
				}
				//ErrorController.showMessage("String Search : result : " + tempList3.size());
				if (tempList3.size() != 0) { // 글자 검색 결과가 있다.
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList3);
					vo32.setRoom_name(Constants.CHATLISTTYPE_MYCHAT);
					childList.add(vo32);
				} else { // 글자 검색 결과가 없다. -> 초성으로 검색
					for (VoChatList flvo : chatList) {
						if (StringProcesser.matchString(flvo.getRoom_name(), query)) {
							tempList3.add(flvo);
						}
					}
					//ErrorController.showMessage("Char Search : result : " + tempList3.size());
					VoChatList vo32 = new VoChatList();
					vo32.setList(tempList3);
					vo32.setRoom_name(Constants.CHATLISTTYPE_MYCHAT);
					childList.add(vo32);
				}
				view.showSearchList(groupList, childList);
			} catch (Exception ex) {

			}
		}// end of catch
	}
}
