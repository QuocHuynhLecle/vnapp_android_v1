package com.wave.messenger.mvp.AddFriend;

import com.wave.messenger.vo.VoFriend;

import java.util.ArrayList;

public interface IAddFriendView {
	void onSearchFailed();
	void onSearchSuccess(String userId, String tel, String userName, int buddy_type);
	void onSearchFailed(String userId, String tel, String userName);
	void addMemberFriend(String userId, String tel, String userName);
	void addNonMemberFriend(String userId, String tel, String userName);
	void onExist(String userName, String tel);
	void searchSuccess(ArrayList<VoFriend> list);
}
