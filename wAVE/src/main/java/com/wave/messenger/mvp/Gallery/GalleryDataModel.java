package com.wave.messenger.mvp.Gallery;

import android.content.Context;
import android.view.View;

import com.wave.messenger.cell.CellGalleryTitle;

import java.io.Serializable;

/**
 * String fullPath, String directoryName, String directoryPath
 * @author geniu
 *
 */
public class GalleryDataModel implements Serializable{

	private static final long serialVersionUID = 1L;
	private String fullPath;
	private String directoryName;
	private String directoryPath;

	public GalleryDataModel(String fullPath, String directoryName,
			String directoryPath) {
		this.fullPath = fullPath;
		this.directoryName = directoryName;
		this.directoryPath = directoryPath;
	}

	public View getView(Context context){
		return new CellGalleryTitle(context, this);
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getDirectoryName() {
		return directoryName;
	}

	public void setDirectoryName(String directoryName) {
		this.directoryName = directoryName;
	}

	public String getDirectoryPath() {
		return directoryPath;
	}

	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}
	

}
