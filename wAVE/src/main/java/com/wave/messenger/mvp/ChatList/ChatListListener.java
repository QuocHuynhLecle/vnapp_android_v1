package com.wave.messenger.mvp.ChatList;

import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;

import java.util.ArrayList;

import moa.android.api.MOAData;

/**
 * Created by user on 2016-09-02.
 */
public interface ChatListListener {
    void receiveMsg(String roomId, VoChatData data);
    void sendMsg(MOAData data);
    void exitRoom(boolean result, MOAData data);
    void createRoom(MOAData data);
    void chatList(MOAData data);
    void recRead();
    void onChatListResult(boolean result);
    void onChatListUpdate(String roomId);
    void onChatRoomInfo(MOAData data);
    void onChatReadMy(MOAData data);
    void showSearchList(ArrayList<String> groupList, ArrayList<VoChatList> chatList);
}
