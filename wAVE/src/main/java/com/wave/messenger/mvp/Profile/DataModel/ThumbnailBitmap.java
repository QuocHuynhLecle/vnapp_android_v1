package com.wave.messenger.mvp.Profile.DataModel;

import android.graphics.Bitmap;

/**
 * Created by user on 2016-09-02.
 */
public class ThumbnailBitmap {

    private Bitmap thumbnail;
    private boolean changed = true;

    public ThumbnailBitmap(Bitmap thumbnail, boolean changed) {
        this.thumbnail = thumbnail;
        this.changed = changed;
    }

    public ThumbnailBitmap() {
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
}
