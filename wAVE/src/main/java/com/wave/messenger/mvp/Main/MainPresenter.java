package com.wave.messenger.mvp.Main;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.LocalContactData;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.utility.UtilityMethods;
import com.wave.messenger.utility.ValidationUtil;
import com.wave.messenger.vo.VoContact;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoGroupList;
import com.wave.messenger.vo.VoMyprofessional;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class MainPresenter {

    private IMainView view;
    private ArrayList<VoFriendList> friendList;
    private static ArrayList<VoGroupList> groupbuddyLists;
    private static ArrayList<VoGroupList> groupUserbuddyLists;
    private static ArrayList<VoGroupList> groupLists;
    private static ArrayList<VoGroupList> groupUserLists;
    private static ArrayList<VoMyprofessional> proLists;
    private static ArrayList<VoMyprofessional> myproLists;
    public MainPresenter(IMainView view) {
        this.view = view;
    }

    public void addMemberFriend(String userId, String userName, String tel, Context context) {
        BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
        db1.addMemberFriend(userId, userName, tel, context);
    }

    public void addNonMemberFriend(String userId, String userName, String tel, Context context){
        BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
        db1.addNonMemberFriend(userId, userName, tel, context);
    }

    /**
     * 친구 목록 검색 <br/>
     * 숫자 / 글자 / 초성 순으로 검색함. <br/>
     *
     * @param query 사용자가 입력한 검색어
     */
    public void searchFriendList(String query, ArrayList<VoFriendList> friendList) {

        ArrayList<String> groupList = new ArrayList<String>();
        ArrayList<VoFriendList> childList = new ArrayList<VoFriendList>();
        groupList.add(Constants.LISTTYPE_MYFRIEND);

        try {
            // 숫자가 검색어일 경우...
            int numberCheck = Integer.parseInt(query);
            ArrayList<VoFriendList> tempList3 = new ArrayList<VoFriendList>();
            // search with number
            for (VoFriendList flvo : friendList) {

                if (flvo.getPhoneNoBar().contains(query)) {
                    tempList3.add(flvo);
                }
            }
            //ErrorController.showMessage("Num Search : result : " + tempList3.size());
            VoFriendList vo32 = new VoFriendList();
            vo32.setList(tempList3);
            vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
            childList.add(vo32);
            view.showFriendList2(groupList, childList, null);

        } catch (Exception e) {
            // 글자일 경우
            try {
                ArrayList<VoFriendList> tempList3 = new ArrayList<VoFriendList>();
                for (VoFriendList flvo : friendList) {
                    if (flvo.getUserName().contains(query)) {
                        tempList3.add(flvo);
                    }
                }
                //ErrorController.showMessage("String Search : result : " + tempList3.size());
                if (tempList3.size() != 0) { // 글자 검색 결과가 있다.
                    VoFriendList vo32 = new VoFriendList();
                    vo32.setList(tempList3);
                    vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
                    childList.add(vo32);
                } else { // 글자 검색 결과가 없다. -> 초성으로 검색
                    for (VoFriendList flvo : friendList) {
                        if (StringProcesser.matchString(flvo.getUserName(),
                                query)) {
                            tempList3.add(flvo);
                        }
                    }
                    //ErrorController.showMessage("Char Search : result : " + tempList3.size());
                    VoFriendList vo32 = new VoFriendList();
                    vo32.setList(tempList3);
                    vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
                    childList.add(vo32);
                }
                view.showFriendList2(groupList, childList, null);
            } catch (Exception ex) {

            }
        }// end of catch
    }// end of function.

    // 주소록을 읽어서 리스트로 반환함
    public ArrayList<VoFriendList> getContactList(Context context) {
        ArrayList<VoFriendList> resultList = new ArrayList<>();

        List<VoContact> contactList = LocalContactData.getContactList(context);

        for (VoContact vo : contactList) {
            String number = vo.getNumber();

            VoFriendList acontact = new VoFriendList();
            acontact.setRawPhoneNumber(number);

            acontact.setProfileImage(vo.getId() + "");
            acontact.setPhone(vo.getFormattedNumber());
            acontact.setUserName(vo.getName());
            acontact.setUserId(vo.getUserId());
            acontact.setAddDate(UtilityMethods.getDefaultDateString());
            acontact.setGroupId(Constants.LISTTYPE_MYFRIEND);

            if (ValidationUtil.isPhoneNumber(number)) {
                resultList.add(acontact);
            }
        }

        return resultList;
    }// end of getContactList

    public void hideFriend(VoFriendList friendToHide, Context context) {
        // Callback to : view.onHideSuccess, onHideFailed
        try {
            if(!friendToHide.isMember()){
                view.onHideSuccess(friendToHide);
                return;
            }

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", friendToHide.getPhone().replaceAll("-",""));
            value.put("mode", "1");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body);
            view.onHideSuccess(friendToHide);
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            //ErrorController.showMessage("hide :" + friendToHide.getUserName());
        }

    }

    public void blockFriend(VoFriendList friendToBlock, Context context) {
        // Callback to : view.onBlockSuccess, onBlockFailed
        try {
            if(!friendToBlock.isMember()){
                view.onBlockSuccess(friendToBlock);
                return;
            }

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", friendToBlock.getPhone().replaceAll("-",""));
            value.put("mode", "1");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_BLOCK, body);
            view.onBlockSuccess(friendToBlock);
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
			/*ErrorController.showMessage("block : "
					+ friendToBlock.getUserName());*/
        }
    }

    /**
     * 대화방 사용자 리스트조회
     * @param context
     * @param userId
     * @param roomId
     */
    public void userList(Context context, String userId , String roomId) {

        LogTrace.E("MainPresenter  userList  : " + userId +" roomId: "+roomId);

        // Callback to : view.onBlockSuccess, onBlockFailed
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId",roomId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_USERLIST, body);
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {

        }
    }

    public boolean validateInvite(List<VoFriendList> selectedFriendList) {
        for (VoFriendList flv : selectedFriendList) {
            if (!flv.isMember()) {
                return false;
            }
        }
        return true;
    }

    public List<VoFriendList> getMemberList(List<VoFriendList> data) {
        List<VoFriendList> result = new ArrayList<>();

        for (VoFriendList flv : data) {
            if (flv.isMember()) {
                result.add(flv);
            }
        }

        return result;
    }

    public void getGroupList(MOAData data) {
        groupbuddyLists = new ArrayList<>();
        groupUserbuddyLists = new ArrayList<>();
        myproLists = new ArrayList<>();
        proLists = new ArrayList<>();
        groupLists = new ArrayList<>();
        groupUserLists = new ArrayList<>();

        LBJSONArray myxprtLstArray = data.body.getArray("myXprtLst");
        LBJSONArray xprtLstArray = data.body.getArray("xprtLst");
        LBJSONArray grpLstArray = data.body.getArray("grpLst");
        LBJSONArray grpUsrsArray = data.body.getArray("grpUsrs");

        ArrayList<VoGroupList> groupbuddyList = new ArrayList<>();
        ArrayList<VoGroupList> groupUserbuddyList = new ArrayList<>();
        ArrayList<VoMyprofessional> myprolList = new ArrayList<>();
        ArrayList<VoMyprofessional> prolList = new ArrayList<>();
        ArrayList<VoGroupList> groupList = new ArrayList<>();
        ArrayList<VoGroupList> groupUserList = new ArrayList<>();

        //내관리고객, 담당직원
        for (int i = 0; i < grpLstArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) grpLstArray.get(i);
            VoGroupList item = VoGroupList.loadFromJsonServerResponse(jo);
            if (!item.getGrpTy().equals("0")) {
                groupbuddyList.add(item);
                for (int j = 0; j < grpUsrsArray.size(); j++) {
                    LBJSONObject jo2 = (LBJSONObject) grpUsrsArray.get(j);
                    VoGroupList item2 = VoGroupList.loadFromJsonGrpUsrsServerResponse(jo2);
                    if (item2.getgId().equals(item.getgId()))
                        groupUserbuddyList.add(item2);
                }
            } else {
                groupList.add(item);
                for (int j = 0; j < grpUsrsArray.size(); j++) {
                    LBJSONObject jo2 = (LBJSONObject) grpUsrsArray.get(j);
                    VoGroupList item2 = VoGroupList.loadFromJsonGrpUsrsServerResponse(jo2);
                    if (item2.getgId().equals(item.getgId()))
                        groupUserList.add(item2);
                }
            }
        }

        //나의전문가
        for (int i = 0; i < myxprtLstArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) myxprtLstArray.get(i);
            VoMyprofessional item = VoMyprofessional.loadFromJsonServerResponse(jo);
            myprolList.add(item);
        }
        //추천전문가
        for (int i = 0; i < xprtLstArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) xprtLstArray.get(i);
            VoMyprofessional item = VoMyprofessional.loadFromJsonServerResponse(jo);
            prolList.add(item);
        }
//		for (int i = 0; i < grpLstArray.size(); i++) {
//			LBJSONObject jo = (LBJSONObject) grpLstArray.get(i);
//			VoGroupList item = VoGroupList.loadFromJsonServerResponse(jo);
//			groupList.add(item);
//		}
//		for (int i = 0; i < grpUsrsArray.size(); i++) {
//			LBJSONObject jo = (LBJSONObject) grpUsrsArray.get(i);
//			VoGroupList item = VoGroupList.loadFromJsonGrpUsrsServerResponse(jo);
//			groupUserList.add(item);
//		}

        groupbuddyLists.addAll(groupbuddyList);
        groupUserbuddyLists.addAll(groupUserbuddyList);
        myproLists.addAll(myprolList);
        proLists.addAll(prolList); //
        groupLists.addAll(groupList);
        groupUserLists.addAll(groupUserList);
    }

    public void getFriendList(final Context context) {
        new AsyncTask<Void, Void, Void>() {
            int count = 0;
            ArrayList<VoFriendList> childList;
            ArrayList<String> groupList;

            @Override
            protected Void doInBackground(Void... params) {
                Log.d("", "getFriendList ??");
                groupList = new ArrayList<>();
                childList = LocalDB.getBuddyDbHelper(context).getBuddyList(context, groupbuddyLists, groupUserbuddyLists, groupList, myproLists, proLists, groupLists, groupUserLists);

                for(VoFriendList vo : childList) {
                    if(Constants.LISTTYPE_MYFRIEND.equals(vo.getUserName())) {
                        count = vo.getList().size();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                view.showFriendList(groupList, childList, String.valueOf(count));
                Log.d("", "showFriendList ??");

            }

        }.execute();

    }// end of getFriendList.

    public void refreshSearchList(Context context, ArrayList<String> searchGroupList, ArrayList<VoFriendList> searchChildList) {
        ArrayList<VoFriendList> childList = LocalDB.getBuddyDbHelper(context).getUpdatedSearchList(searchChildList, context);
        view.showFriendList2(searchGroupList, childList, "10");

    }
}// end of class
