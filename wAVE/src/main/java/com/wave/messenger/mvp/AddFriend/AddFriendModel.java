package com.wave.messenger.mvp.AddFriend;

import android.content.Context;
import android.os.Handler;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.utility.ErrorController;

import org.json.JSONException;
import org.json.JSONObject;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class AddFriendModel implements MOAEventListener {

	private Handler handler;
	
	public void askFriendExist(Context context, String name, String phone, AddFriendPresenter presenter) {
		BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
		db1.searchIfExists(context, name, phone, presenter);
	}

	public void lookUpDBForExist(Context context, String name, String phone, Handler handler, AddFriendPresenter presenter){
		ErrorController.showMessage("[AddFriendModel] userPhone : " + MessengerInfo.getUserPhoneNumber(context) + ", phone : " + phone);
		if(MessengerInfo.getUserPhoneNumber(context).equals(phone.replace("-", ""))){
			presenter.showExists(name, phone);
			return;
		}
		BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(context);
		db1.searchIfExists2(context, name, phone, this, handler, presenter);
	}

	public void searchFriend(Context context, String search, Handler handler) {
		try{
			ErrorController.showMessage("[AddFriendModel] searchFriend : searchFriend");
			this.handler = handler;
			MOAClient client = MOAClient.getInstance();
			client.addEventListener(this);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("searchWord", search);
//			value.put("tel", phone.replaceAll("-",""));
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_SEARCH_BUDDY, body);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void askFriendType(Context context, String name, String phone, Handler handler){
		try{
			ErrorController.showMessage("[AddFriendModel] askFriendType : askFriendType");
			this.handler = handler;
			MOAClient client = MOAClient.getInstance();
			client.addEventListener(this);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userName", name);
			value.put("tel", phone.replaceAll("-",""));
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER,
					PacketTypes.PTC_IMS_USER_CHECK_MEMBERS, body);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void addFriend(Context context, String name, String id, Handler handler){
		try {

			//presenter로 결과를 전달해줌.
			this.handler = handler;
			MOAClient client = MOAClient.getInstance();
			client.addEventListener(this);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			value.put("userId", MessengerInfo.getUserId(context));
			value.put("userName", name);
			value.put("buddy_id", id);
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_ADDBUDDY, body);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void OnEvent(MOAData data) {
		handler.sendMessage(handler.obtainMessage(data.ptc, data));
	}

	public void deleteFriend(Context context, String userId, String tel, Handler handler) {
		this.handler = handler;
		try {
			String [] telList =  new String [10];
			telList[0] = tel;
			ErrorController.showMessage("[AddFriendModel] deleteFriend valcheck : userId - " + MessengerInfo.getUserId(context) + ", addPhone - " + tel);
			PacketBody body = new PacketBody();
			JSONObject value = new JSONObject();
			body.put("userId", MessengerInfo.getUserId(context));
			value.put("addrPhone", "{" + tel + "}");
			body.put("params", value);
			MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
					PacketTypes.PTC_IMS_ADDRESS_DELETE, body);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
