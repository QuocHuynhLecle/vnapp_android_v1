package com.wave.messenger.mvp.Main;

import android.content.Context;
import android.os.AsyncTask;

import com.wave.messenger.db.LocalDB;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;
import java.util.List;

public class InvitePresenter {

	private IInviteView view;

	public InvitePresenter(IInviteView view) {
		this.view = view;
	}

	/**
	 * 친구 목록 검색 <br/>
	 * 숫자 / 글자 / 초성 순으로 검색함. <br/>
	 * 
     * @param query 사용자가 입력한 검색어
	 */
	public void searchFriendList(String query, ArrayList<VoFriendList> friendList) {

		ArrayList<String> groupList = new ArrayList<String>();
		ArrayList<VoFriendList> childList = new ArrayList<VoFriendList>();
		groupList.add(Constants.LISTTYPE_MYFRIEND);

		try {
			// 숫자가 검색어일 경우...
			int numberCheck = Integer.parseInt(query);
			ArrayList<VoFriendList> tempList3 = new ArrayList<VoFriendList>();
			// search with number
			for (VoFriendList flvo : friendList) {

				if (flvo.getPhoneNoBar().contains(query)) {
					tempList3.add(flvo);
				}
			}
			//ErrorController.showMessage("Num Search : result : " + tempList3.size());
			VoFriendList vo32 = new VoFriendList();
			vo32.setList(tempList3);
			vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
			childList.add(vo32);
			view.showFriendList2(groupList, childList, null);

		} catch (Exception e) {
			// 글자일 경우
			try {
				ArrayList<VoFriendList> tempList3 = new ArrayList<VoFriendList>();
				for (VoFriendList flvo : friendList) {
					if (flvo.getUserName().contains(query)) {
						tempList3.add(flvo);
					}
				}
				//ErrorController.showMessage("String Search : result : " + tempList3.size());
				if (tempList3.size() != 0) { // 글자 검색 결과가 있다.
					VoFriendList vo32 = new VoFriendList();
					vo32.setList(tempList3);
					vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
					childList.add(vo32);
				} else { // 글자 검색 결과가 없다. -> 초성으로 검색
					for (VoFriendList flvo : friendList) {
						if (StringProcesser.matchString(flvo.getUserName(),
								query)) {
							tempList3.add(flvo);
						}
					}
					//ErrorController.showMessage("Char Search : result : " + tempList3.size());
					VoFriendList vo32 = new VoFriendList();
					vo32.setList(tempList3);
					vo32.setUserName(Constants.LISTTYPE_MYFRIEND);
					childList.add(vo32);
				}
				view.showFriendList2(groupList, childList, null);
			} catch (Exception ex) {

			}
		}// end of catch
	}// end of function.

	public boolean validateInvite(List<VoFriendList> selectedFriendList) {
		for (VoFriendList flv : selectedFriendList) {
			if (!flv.isMember()) {
				return false;
			}
		}
		return true;
	}

	public List<VoFriendList> getMemberList(List<VoFriendList> data) {
		List<VoFriendList> result = new ArrayList<>();

		for (VoFriendList flv : data) {
			if (flv.isMember()) {
				result.add(flv);
			}
		}

		return result;
	}

	public void getFriendList(final Context context) {
		new AsyncTask<Void, Void, Void>() {
            int count = 0;
            ArrayList<VoFriendList> childList;
            ArrayList<String> groupList;

            @Override
            protected Void doInBackground(Void... params) {
                groupList = new ArrayList<>();
                childList = LocalDB.getBuddyDbHelper(context).getInviteBuddyList(context, groupList);

				for(VoFriendList vo : childList) {
                    if(Constants.LISTTYPE_MYFRIEND.equals(vo.getUserName())) {
                        count = vo.getList().size();
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                view.showFriendList(groupList, childList, String.valueOf(count));

            }

        }.execute();

	}// end of getFriendList.

	public void refreshSearchList(Context context, ArrayList<String> searchGroupList, ArrayList<VoFriendList> searchChildList) {
		ArrayList<VoFriendList> childList = LocalDB.getBuddyDbHelper(context).getUpdatedSearchList(searchChildList, context);
		view.showFriendList2(searchGroupList, childList, "10");

	}
}// end of class
