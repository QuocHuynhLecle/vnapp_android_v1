package com.wave.messenger.mvp.ChatRoom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.greendao.model.ChatEntity;
import com.wave.messenger.db.greendao.model.ChatModel;
import com.wave.messenger.util.Utility;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class ChatPresenter {

    private IChatView view;

    public ChatPresenter(IChatView view) {
        this.view = view;
    }

    public void showEmoticon(int resid) {
        view.onEmoticonSelected(resid);
    }

    public VoChatData makeNewInviteMessage(List<VoFriendList> newFriends) {
        VoChatData data = new VoChatData();
        String names = "";
        String nonMemberUsers = "";
        for (VoFriendList flvo : newFriends) {
            names += flvo.getUserName() + ", ";
            if (!flvo.isMember())
                nonMemberUsers += flvo.getUserName() + ", ";
        }
        names = names.substring(0, names.length() - 2);
        if (!TextUtils.isEmpty(nonMemberUsers)) {
            nonMemberUsers = nonMemberUsers.substring(0, nonMemberUsers.length() - 2);
        }
        data.setNonMemberUsers(nonMemberUsers);
        data.setOwnerId(names);
        data.setChatType("1");
        data.setMessageType("0");
        return data;
    }

    public void createChatRoom(Context context, String roomType, String inviteUser) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", uuid.toString());
            value.put("roomType", roomType);
            value.put("inviteUser", inviteUser);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_CREATE, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Context context, VoChatData data) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", data.getRoomId());
            value.put("roomCreate", data.getRoomCreate());
            value.put("roomType", data.getRoomType());
            value.put("roomName", data.getRoomName());
            value.put("gId", data.getgId());
            value.put("chatId", data.getChatId());
            value.put("title", data.getTitle());
            value.put("message", data.getMessage());
            value.put("chatType", data.getChatType());
            value.put("messageType", data.getMessageType());
            value.put("attachment", data.getAttachment());
            value.put("inviteUser", data.getInviteUser());
            value.put("userName", data.getUserName());

            LogTrace.E("send message : " + value.toString());

            body.put("params", value);
            ErrorController.showMessage("[ChatPresenter] attachment : " + data.getAttachment());
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_SEND_MSG, body);

            ChatEntity chatEntity = ChatModel.convertChatEntity(data);

            if (chatEntity != null) {
                chatEntity.setStatus(1);
                chatEntity.setOwnerId(data.getOwnerId());
                chatEntity.setSenderId(data.getSenderId());

                ErrorController.writeLog(chatEntity.getMessageType(), chatEntity.getChatId(), chatEntity.getOwnerId(), chatEntity.getSenderId());

                if (!TextUtils.isEmpty(chatEntity.getChatId()) && !TextUtils.isEmpty(chatEntity.getOwnerId()) && !TextUtils.isEmpty(chatEntity.getSenderId())) {
                    boolean result = ChatModel.insert(context, chatEntity);
                    ErrorController.writeLog("ChatEntity Insert::" + result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getMessage(Context context, String roomId, String lastCid, String unreadcount) {
        LogTrace.D("##getMessage roomId = " + roomId +" lastCid: "+lastCid +" unreadcount: "+unreadcount);
        try {
            String roomType = Statics.ROOMINFO.getRoom_type();

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            value.put("room_type", roomType);

            //20180323leeyunsu
            /*
            if(roomType.equals("0")){//1:1대화
                value.put("lastcid", "0");
                //value.put("mode", "2");
                //value.put("covl", unreadcount);
            }else{ //모임채팅,종목채팅 등...
                value.put("lastcid", lastCid);
                value.put("mode", "2");
                value.put("covl", unreadcount);
            }
            */

            //0508 test  leeyunsu
           /* if(lastCid.equals("0") || lastCid.equals("") ){
                value.put("lastcid", lastCid);
                LogTrace.D("##getMessage lastcid "+lastCid);

            }else{  //최신의 미확인 갯수만 가져오기
                value.put("lastcid", lastCid);
                value.put("covl", unreadcount);
                LogTrace.D("##getMessage mode 2");
            }*/
            value.put("lastcid", lastCid);
            body.put("params", value);

            LogTrace.D("getMessage value = " + value.toString());
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_LIST, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*public void getMessage(Context context, String roomId) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT,
                    PacketTypes.PTC_IMS_CHAT_LIST, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    public void setRead(Context context, String roomId, String cid, String chatId) {
        try {
            if (TextUtils.isEmpty(cid)) {
                ErrorController.showMessage("dddd");
                return;
            }
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            value.put("cid", cid);
            value.put("chatId", chatId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT,
                    PacketTypes.PTC_IMS_CHAT_READ, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void inviteFriend(Context context, String roomId, String inviteUser) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            value.put("inviteUser", inviteUser);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_INVITE, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void inviteFriend(Context context, String roomId, List<VoFriendList> newFriends) {
        String toInvite = "";
        List<VoFriendList> checkFriends = new ArrayList<>();
        for (VoFriendList friend : newFriends) {
            if (!TextUtils.isEmpty(friend.getUserId())) {
                if (!friend.getUserId().equals(MessengerInfo.getUserId(context))) {
                    if (checkFriends.size() > 0) {
                        for (int i = 0; i < checkFriends.size(); i++) {
                            VoFriendList item = checkFriends.get(i);
                            if (!friend.getUserId().equals(item.getUserId()))
                                checkFriends.add(friend);
                            toInvite += friend.getUserId() + ",";
                        }
                    } else {
                        checkFriends.add(friend);
                        toInvite += friend.getUserId() + ",";
                    }
                }
            }
        }
        toInvite = toInvite.substring(0, toInvite.length() - 1);
        if (TextUtils.isEmpty(roomId)) {
            createChatRoom(context, "1", toInvite);
        } else {
            inviteFriend(context, roomId, toInvite);
        }
    }

    public void chatRoomInfo(Context context, String roomId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
//            value.put("uinfo", "0");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_INFO, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void chatReadData(Context context, String roomId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_READ_DATA, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setOnlyStaff(String roomId, String flag) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("roomId", roomId);
            value.put("enableWriteMsg", flag);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_WRITE_CONFIG, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendShake(String roomId, String userId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("roomId", roomId);
            value.put("userId", userId);
            value.put("action", "0");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_ACTION, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendClean(String roomId, String userId, String cId) {
        try {
            PacketBody body = new PacketBody();

            JSONObject value = new JSONObject();
            value.put("roomId", roomId);
            value.put("userId", userId);
            value.put("cId", cId);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_ROOMCLEAR, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPageList(List<List<VoChatData>> pageList, List<VoChatData> list) {
        if(list!=null) {
            ErrorController.showMessage("[ChatPresenter] setPageList : check - list size = " + list.size());
        } else if(list.size()>0) {
            if ("1".equals(list.get(list.size()-1).getChatType())) {
                list.remove(list.size()-1);
            }
        } else if(list.size()>1) {
            if ("1".equals(list.get(list.size()-2).getChatType())) {
                list.remove(list.size()-2);
            }
        }

        if (list != null && list.size() > 0) {
            //리스트 크기가 50 이상인지 체크한다.
            if (list.size() > 50) {
                //리스트의 크기가 50이상이면 페이징을 해준다.
                //리스트를 50단위로 잘라서 삽입한다.
                List<VoChatData> partialList = new ArrayList<>();
                int internalCount = 0;
                for (int i = 0; i < list.size(); i++) {
                    if (internalCount == 50) {
                        //50개 이상의 메시지를 서브 리스트에 삽입했으면 이를 페이지 리스트에 삽입한다.
                        partialList.add(list.get(i));
                        Collections.reverse(partialList);
                        pageList.add(partialList);
                        internalCount = 0;
                        partialList = new ArrayList<>();
                    }else{
                        partialList.add(list.get(i));
                        internalCount++;
                    }
                }
                //마지막 리스트(50개 미민)이 추가되지 않았을 경우 추가한다.
                if(internalCount!=0){
                    Collections.reverse(partialList);
                    pageList.add(partialList);
                }
            } else {
                //리스트의 크기가 50미만이면 페이징을 하지 않고, 단일 리스트만을 삽입한다.
                Collections.reverse(list);
                pageList.add(list);
            }
        }
    }

    public void saveFileAsScaledImage(Context context, String fullPath, int sampleSize) {
        LogTrace.E("saveFileAsScaledImage : " + fullPath);

        Bitmap resultBitmap = null;
        Bitmap saveBitmap = null;

        try{
            BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fullPath, bounds);

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = sampleSize;
            resultBitmap = BitmapFactory.decodeFile(fullPath, opts);
            saveBitmap = Utility.modifyOrientation(resultBitmap, fullPath); // getRotatedBitmap(resultBitmap,getPhotoOrientationDegree(fullPath));
        } catch(Exception e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        File file = new File(Utility.getChatImageUri(context).getPath());
        saveBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

        byte[] byteArray = stream.toByteArray(); // convert camera photo to
        // byte array
        // save it in your external storage.
        FileOutputStream fo;
        try {
            fo = new FileOutputStream(file);
            fo.write(byteArray);
            fo.flush();
            fo.close();
            ErrorController.showMessage("File Size : " + file.length());
            view.onSavePictureSuccess(file.getPath());
        } catch (Exception e) {
            e.printStackTrace();
            view.onSavePictureSuccess(null);
        }
    }


}
