package com.wave.messenger.mvp.Main;

import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;

public interface IMainView {
	void showFriendList(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count);
	void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count);
	void onHideSuccess(VoFriendList hiddenFriend);
	void onBlockSuccess(VoFriendList blockedFriend);
}
