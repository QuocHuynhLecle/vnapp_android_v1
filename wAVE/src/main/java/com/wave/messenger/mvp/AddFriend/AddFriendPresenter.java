package com.wave.messenger.mvp.AddFriend;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoFriend;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.protocol.PacketTypes;

public class AddFriendPresenter {

    private IAddFriendView view;
    private AddFriendModel model;
    private Context context;
    private String mySetName;
    private ArrayList<VoFriend> friends;

    public AddFriendPresenter(IAddFriendView view) {
        this.view = view;
        this.model = new AddFriendModel();
    }

    public AddFriendModel getModel(){
        return model;
    }

    public void askFriendExist(Context context, String name, String phone) {
        this.context = context;
        //친구인지 아닌지 확인
        if (validate(name, phone)) {
            this.mySetName = name;
            this.model.lookUpDBForExist(context, name, phone, new WebHandler(), this);
        } else {
            view.onSearchFailed();
        }
    }

    public void searchFriend(Context context, String search) {
        this.model.searchFriend(context, search, new WebHandler());
    }

    public void showExists(String userName, String tel) {
        view.onExist(userName, tel);
    }


    public void askFriendType(Context context, String name, String phone){
        model.askFriendType(context, name, phone, new WebHandler());
    }

    public void addFriend(Context context, String name, String id) {
        //친구 추가
        this.model.addFriend(context, name, id, new WebHandler());
    }

    /**
     * 회원 친구 추가  - Dialogs에서 호출
     */
    public void addMemberFriend(String userId, String tel, String userName) {
        view.addMemberFriend(userId, tel, userName);
    }


    /**
     * 비회원 친구 추가 - Dialogs에서 호출
     */
    public void addNonMemberFriend(String userId, String tel, String userName) {
        view.addNonMemberFriend(userId, tel, userName);
    }


    public void deleteFriend(String tel){
        model.deleteFriend(context, MessengerInfo.getUserId(context), tel, new WebHandler());
    }

    /**
     * 이름과 폰번의 길이에 따라 친구 추가 조건을 만족하면 true, 만족하지 못하면 false를 리턴한다.
     *
     * @param nameCount  입력한 이름의 길이
     * @param phoneCount 입력한 폰번호의 길이
     * @return true : 폰번의 길이가 13자이고 이름이 1자 이상이다. <br/>false : 둘 중 하나를 만족 못함.
     */
    public boolean validate(int nameCount, int phoneCount, String phoneText) {
        if (nameCount == 0) {
            return false;
        }

        if (phoneCount < 12) {
            return false;
        }
        try{
            String noBarPhone="";
            String [] temp = phoneText.split("-");
            for(int i = 0; i<temp.length; i++) {
                noBarPhone += temp[i];
            }
           Integer.parseInt(noBarPhone);
        }catch (Exception e){
            return false;
        }

        if (phoneCount == 13 || phoneCount == 12) {
            if (phoneText.startsWith("010") && phoneCount == 13) {
                return true;
            }
            if(phoneText.startsWith("011") || phoneText.startsWith("016") || phoneText.startsWith("017") || phoneText.startsWith("018") || phoneText.startsWith("019")){
                if(phoneCount == 12)
                    return true;
            }
        }

        return false;
    }

    public boolean delNameBtnValidate(int nameCount) {
        if (nameCount > 0)
            return true;
        else
            return false;
    }

    public boolean delPhoneBtnValidate(int phoneCount) {
        if (phoneCount > 0)
            return true;
        else
            return false;
    }

    /**
     * check if all values are valid
     *
     * @param name
     * @param phone
     * @return
     */
    private boolean validate(String name, String phone) {

        if (TextUtils.isEmpty(name))
            return false;

        if (phone.length() < 12)
            return false;

        try {
            // int i = Integer.parseInt(phone);
            return true;
        } catch (Exception e) {
            return false;
        }
    }// end of validate

    private class WebHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

            MOAData data = (MOAData) msg.obj;

            switch (data.ptc) {

                case PacketTypes.PTC_IMS_ADDRESS_ADDBUDDY:
                    ErrorController.showMessage("[AddFriendPresenter] !!!! " +
                            "PTC_IMS_ADDRESS_ADDBUDDY : " + data.body.toString());
                    String result = data.body.getString("result");
                    if ("success".equals(result)) {
                        //성공한 경우(친구가 존재, 내 친구가 아니다.)
                        //친구 타입
                        int buddy_type = data.body.getJson("params").getInt("buddy_type");
                        String userId = data.body.getJson("params").getString("userId");
                        String tel = data.body.getJson("params").getString("tel");
                        String userName = data.body.getJson("params").getString("userName");
                        ErrorController.showMessage("[AddFriendPresenter] webHandler - addfriendSuccess : check buddy_type = " + buddy_type);
                        LogTrace.E("add success : " + data.body.toString());
                        view.onSearchSuccess(userId, tel, userName, buddy_type);
                    } else if ("buddy exist".equals(result)) {
                        //친구가 이미 추가된 경우
                        ErrorController.showMessage("Exists");
                        String userId = data.body.getJson("params").getString("userId");
                        String tel = data.body.getJson("params").getString("tel");
                        String userName = data.body.getJson("params").getString("userName");
                        view.onExist(userName, userId);
                    } else {
                        //친구가 존재하지 않은 경우.
                        ErrorController.showMessage("Fail");
                        view.onSearchFailed();
                    }
                    break;
                case PacketTypes.PTC_IMS_ADDRESS_DELETE:
                    ErrorController.showMessage("[AddFriendPresenter]" +
                            "PTC_IMS_ADDRESS_DELETE : " + data.body.toString());
                    break;
                case PacketTypes.PTC_IMS_ADDRESS_SEARCH_BUDDY :
                    String list = data.body.getString("params");
                    friends = new ArrayList<>();
                    try {
                        JSONArray jsonArray = new JSONArray(list);
                        for(int i = 0 ; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            VoFriend voFriend = new VoFriend();
                            voFriend.setId(object.getString("userId"));
                            voFriend.setNick(object.getString("userName"));
                            voFriend.setImgUrl(Constants.CHATDAWN_PROC + "?type=2&userid=" + object.getString("userId"));
                            friends.add(voFriend);
                        }
                        view.searchSuccess(friends);
                    } catch (Exception e) {

                    }
                    break;
                case PacketTypes.PTC_IMS_USER_CHECK_MEMBERS:
                    ErrorController.showMessage(("[AddFriendPresenter]" + data.body.toString()));
                    String result3 = data.body.getString("result");
                    if("success".equals(result3)){
                        int buddy_type = data.body.getJson("params").getInt("user_type");
                        String userId = data.body.getJson("params").getString("userId");
                        String tel = data.body.getJson("params").getString("tel");
                        String userName = data.body.getJson("params").getString("userName");

                        if(buddy_type == -1){
                            view.onSearchSuccess(userId, tel, userName, buddy_type);
                        }else{
//                            addMemberFriend(userId, tel, userName);
                            addFriend(context, mySetName, tel);
                        }

                    }else{
                        view.onSearchFailed();
                    }

                    break;
            }
        }
    }//end of WebHandler

}
