package com.wave.messenger.mvp.Profile;

import android.graphics.Bitmap;
import android.net.Uri;

public interface IProfileView {
	void onSavePictureSuccess(String path);
	void onReceivePictureSuccess(Uri path);
	void onDownloadPictureComplete(Bitmap bitmap);
	void onDownloadPictureFailed();
	void onChangeName(String newUserName);
	void onChangeNameFailed();
}
