package com.wave.messenger.mvp.ChatRoom;

public interface IChatView {
	void onEmoticonSelected(int resid);
	void onSavePictureSuccess(String path);
}
