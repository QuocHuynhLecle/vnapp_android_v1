package com.wave.messenger.Emoticon;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aveapp on 2017. 7. 27..
 */

public class Emoticon_Activity extends Fragment {
//public class Emoticon_Activity extends Fragment implements CompoundButton.OnCheckedChangeListener {
    private ViewPager vp_emoticon;
    private HorizontalScrollView hs_emoticon;
    private LinearLayout ll_emoticon;
    private Emoticon_PagerAdapter mAdapter;
//    private CheckBox cb_emo_1, cb_emo_2, cb_emo_3, cb_emo_4, cb_emo_5, cb_emo_6, cb_emo_7, cb_emo_8, cb_emo_9;
//    private Button btn_emo_1;
    private Map<EMOTICON, String[]> emoticonImageViewMap = new HashMap<>();
    private CircleAnimIndicator viewpager_indicator;
    private int PAGE;       // 0 : 글쓰기, 1 : 댓글쓰기, 2 : 대댓글쓰기
    private Context context;

    public Emoticon_Activity(int page, Context context) {
        this.PAGE = page;
        this.context = context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_emoticon, container, false);

        init(v);

        return v;
    }

    public void init(View v) {
        vp_emoticon = (ViewPager) v.findViewById(R.id.vp_emoticon);
        hs_emoticon = (HorizontalScrollView) v.findViewById(R.id.hs_emoticon);
        ll_emoticon = (LinearLayout) v.findViewById(R.id.ll_emoticon);
        viewpager_indicator = (CircleAnimIndicator) v.findViewById(R.id.viewpager_indicator);

//        cb_emo_1 = (CheckBox) v.findViewById(R.id.btn_emo_1);
//        cb_emo_2 = (CheckBox) v.findViewById(R.id.btn_emo_2);
//        cb_emo_3 = (CheckBox) v.findViewById(R.id.btn_emo_3);
//        cb_emo_4 = (CheckBox) v.findViewById(R.id.btn_emo_4);
//        cb_emo_5 = (CheckBox) v.findViewById(R.id.btn_emo_5);
//        cb_emo_6 = (CheckBox) v.findViewById(R.id.btn_emo_6);
//        cb_emo_7 = (CheckBox) v.findViewById(R.id.btn_emo_7);
//        cb_emo_8 = (CheckBox) v.findViewById(R.id.btn_emo_8);
//        cb_emo_9 = (CheckBox) v.findViewById(R.id.btn_emo_9);

//        btn_emo_1.setOnClickListener();
        drawViewPager(EMOTICON.FEELINGS);

//        cb_emo_1.setOnCheckedChangeListener(this);
//        cb_emo_2.setOnCheckedChangeListener(this);
//        cb_emo_3.setOnCheckedChangeListener(this);
//        cb_emo_4.setOnCheckedChangeListener(this);
//        cb_emo_5.setOnCheckedChangeListener(this);
//        cb_emo_6.setOnCheckedChangeListener(this);
//        cb_emo_7.setOnCheckedChangeListener(this);
//        cb_emo_8.setOnCheckedChangeListener(this);
//        cb_emo_9.setOnCheckedChangeListener(this);

//        String[] feelings = getResources().getStringArray(R.array.emoticon_feelings_ko);

//        cb_emo_1.setChecked(true);

//        String[] s_candle;
//        String[] defaultBani = getResources().getStringArray(R.array.emoticon_s_candle_ko);
//        s_candle = new String[defaultBani.length + 1];
////        blueBirdAry[0] = getString(R.string.event_emoticon_title);
//        System.arraycopy(defaultBani, 0, s_candle, 1, defaultBani.length);
//        String[] s_m = getResources().getStringArray(R.array.emoticon_s_m_ko);
        String[] feelings = getResources().getStringArray(R.array.emoticon_feelings_ko);
//        String[] s_squid = getResources().getStringArray(R.array.emoticon_s_squid_ko);
//        s_candle = getResources().getStringArray(R.array.emoticon_s_candle_ko);
//        String[] s_h = getResources().getStringArray(R.array.emoticon_s_h_ko);
//        String[] s_kum = getResources().getStringArray(R.array.emoticon_s_kum_ko);
//        String[] s_kim = getResources().getStringArray(R.array.emoticon_s_kim_ko);
//        String[] s_tdong = getResources().getStringArray(R.array.emoticon_s_tdong_ko);
//        String[] s_sushi = getResources().getStringArray(R.array.emoticon_s_sushi_ko);
//        String[] s_food = getResources().getStringArray(R.array.emoticon_s_food_ko);



        emoticonImageViewMap.clear();
//        emoticonImageViewMap.put(EMOTICON.FEELINGS, feelings);

        emoticonImageViewMap.put(EMOTICON.FEELINGS, feelings);
//        emoticonImageViewMap.put(EMOTICON.S_SQUID, s_squid);
//        emoticonImageViewMap.put(EMOTICON.S_M, s_m);
//        emoticonImageViewMap.put(EMOTICON.S_CANDLE, s_candle);
//        emoticonImageViewMap.put(EMOTICON.S_H, s_h);
//        emoticonImageViewMap.put(EMOTICON.S_KUM, s_kum);
//        emoticonImageViewMap.put(EMOTICON.S_KIM, s_kim);
//        emoticonImageViewMap.put(EMOTICON.S_TDONG, s_tdong);
//        emoticonImageViewMap.put(EMOTICON.S_SUSHI, s_sushi);
//        emoticonImageViewMap.put(EMOTICON.S_FOOD, s_food);

        vp_emoticon.addOnPageChangeListener(mOnPageChangeListener);
        setViewPager();

    }

//    public void emo_reset() {
//        cb_emo_1.setChecked(false);
//        cb_emo_2.setChecked(false);
//        cb_emo_3.setChecked(false);
//        cb_emo_4.setChecked(false);
//        cb_emo_5.setChecked(false);
//        cb_emo_6.setChecked(false);
//        cb_emo_7.setChecked(false);
//        cb_emo_8.setChecked(false);
//        cb_emo_9.setChecked(false);
//    }

    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            viewpager_indicator.selectDot(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    /**
     * Indicator 초기화
     */
    private void initIndicaotor() {
        viewpager_indicator.removeAllViews();

        //원사이의 간격
        viewpager_indicator.setItemMargin(15);
        //애니메이션 속도
        viewpager_indicator.setAnimDuration(300);
        //indecator 생성
        viewpager_indicator.createDotPanel(mAdapter.getCount(), R.drawable.shape_round_white, R.drawable.shape_round_blue);
    }

//    @Override
//    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//        if (isChecked) {
//            int i = buttonView.getId();
//            if (i == R.id.btn_emo_1) {
//                emo_reset();
//                cb_emo_1.setChecked(true);
//                drawViewPager(EMOTICON.S_SQUID);
//            }
//            else if (i == R.id.btn_emo_2) {
//                emo_reset();
//                cb_emo_2.setChecked(true);
//                drawViewPager(EMOTICON.S_TDONG);
//
//            } else if (i == R.id.btn_emo_3) {
//                emo_reset();
//                cb_emo_3.setChecked(true);
//                drawViewPager(EMOTICON.S_SQUID);
//
//            } else if (i == R.id.btn_emo_4) {
//                emo_reset();
//                cb_emo_4.setChecked(true);
//                drawViewPager(EMOTICON.S_SUSHI);
//
//            } else if (i == R.id.btn_emo_5) {
//                emo_reset();
//                cb_emo_5.setChecked(true);
//                drawViewPager(EMOTICON.S_KUM);
//
//            } else if (i == R.id.btn_emo_6) {
//                emo_reset();
//                cb_emo_6.setChecked(true);
//                drawViewPager(EMOTICON.S_KIM);
//
//            } else if (i == R.id.btn_emo_7) {
//                emo_reset();
//                cb_emo_7.setChecked(true);
//                drawViewPager(EMOTICON.S_M);
//
//            } else if (i == R.id.btn_emo_8) {
//                emo_reset();
//                cb_emo_8.setChecked(true);
//                drawViewPager(EMOTICON.S_FOOD);
//
//            } else if (i == R.id.btn_emo_9) {
//                emo_reset();
//                cb_emo_9.setChecked(true);
//                drawViewPager(EMOTICON.S_H);
//
//            }
//        } else {
//
//        }
//    }


    private void drawViewPager(EMOTICON emoticon) {
        if (mAdapter == null) {
            mAdapter = new Emoticon_PagerAdapter(getChildFragmentManager());
        }

        mAdapter.removeAllFragments();

        String[] ary = emoticonImageViewMap.get(emoticon);

        if (ary != null) {
            List<String> list = new ArrayList<>();

            for (String str : ary) {

                list.add(str);

                if (list.size() % 8 == 0) {
                    Fragment_Emoticon page = new Fragment_Emoticon(PAGE, context);
                    page.setIconSet(list);
                    mAdapter.addFragment(page, "page" + (mAdapter.getCount() + 1));
                    list = new ArrayList<>();
                }
            }

            // 마지막 페이지
            if (list.size() != 0) {
                Fragment_Emoticon page = new Fragment_Emoticon(PAGE, context);
                page.setIconSet(list);
                mAdapter.addFragment(page, "page" + (mAdapter.getCount() + 1));
            }
        }

        initIndicaotor();
//        setDotNumber(mAdapter.getCount());
//        setSelection(emoticon);
//        setDotSelection(0);

        vp_emoticon.setAdapter(mAdapter);
    }

//    private int getDrawableResourceByName(String str) {
//        String packageName = getActivity().getPackageName();
//        return getResources().getIdentifier(str, "drawable", packageName);
//    }

    private void setViewPager() {
        drawViewPager(EMOTICON.FEELINGS);
    }

    private enum EMOTICON {FEELINGS}

}
