package com.wave.messenger.Emoticon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_MoimWrite;
import com.wave.messenger.activity.Activity_Re_Reply;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by aveapp on 2017. 7. 31..
 */

public class Adapter_Emoticon_Recycler extends RecyclerView.Adapter {

    private List<String> data = Collections.emptyList();
    private Context context;
    private int Page;

    public Adapter_Emoticon_Recycler(List<String> data, Context context, int Page) {
        this.data = data;
        this.context = context;
        this.Page = Page;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_emoticon_simple, parent, false);
        return new AdapterEmoticonRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        AdapterEmoticonRecyclerViewHolder holderObject = (AdapterEmoticonRecyclerViewHolder) holder;
        holderObject.ivEmoticon.setImageResource(getDrawableResourceByName(data.get(position)));
    }

    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(context,str);
        String packageName = context.getPackageName();
        return context.getResources().getIdentifier(emoticon, "drawable", packageName);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class AdapterEmoticonRecyclerViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivEmoticon;

        public AdapterEmoticonRecyclerViewHolder(View itemView) {
            super(itemView);
            ivEmoticon = (ImageView) itemView.findViewById(R.id.ivEmoticon);
            ivEmoticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (Page == 0) {
                        //글쓰기
                        Log.i("TEST", "emoticon_num : " + data.get(getAdapterPosition()));

                        ((Activity_MoimWrite) Activity_MoimWrite.context).setEmoticon(data.get(getAdapterPosition()));

                    } else if (Page == 1) {
                        //댓글쓰기

                        ((Activity_TimeLineDetail) Activity_TimeLineDetail.context).setEmoticon(data.get(getAdapterPosition()));

                    } else if (Page == 2) {

                        ((Activity_Re_Reply) Activity_Re_Reply.context).setEmoticon(data.get(getAdapterPosition()));
                    } else if (Page == 3) {
                        //대화방
                        ((Activity_ChatRoom) Activity_ChatRoom.getInstance()).setEmoticon(data.get(getAdapterPosition()));
                    }

                }
            });
        }
    }

}
