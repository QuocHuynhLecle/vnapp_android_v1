package com.wave.messenger.Emoticon;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aveapp on 2017. 7. 27..
 */

public class Emoticon_PagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    private int currentFragment = 1;

    private FragmentManager manager;

    public Emoticon_PagerAdapter(FragmentManager fm) {
        super(fm);
        manager = fm;
    }


    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        notifyDataSetChanged();
    }

    public void removeFragment(Fragment fragment, String title) {
        mFragmentList.remove(fragment);
        mFragmentTitleList.remove(title);
        notifyDataSetChanged();
    }

    public void removeAllFragments() {

        mFragmentList.clear();
        mFragmentTitleList.clear();

        int count = manager.getBackStackEntryCount();

        for (int i = 0; i < count; i++) {
            manager.popBackStack();
        }


        notifyDataSetChanged();

    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
