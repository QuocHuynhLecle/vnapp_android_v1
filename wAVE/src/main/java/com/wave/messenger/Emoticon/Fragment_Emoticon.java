package com.wave.messenger.Emoticon;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wave.massenger.piggy.R;

import java.util.List;

/**
 * Created by aveapp on 2017. 7. 31..
 */

public class Fragment_Emoticon extends Fragment {

    private RecyclerView rvListView;
    private Adapter_Emoticon_Recycler adapter;
    private int page;
    private Context context;

    public Fragment_Emoticon(int page, Context context) {
        this.page = page;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(
                R.layout.fragment_emoticon, container, false);

        initView(view);
        setEvent();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rvListView.setAdapter(adapter);

    }

    private void initView(View view) {
        this.rvListView = (RecyclerView) view.findViewById(R.id.rvListView);
        this.rvListView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
    }


    private void setEvent() {
    }

    public void setIconSet(List<String> emoticon1) {
        adapter = new Adapter_Emoticon_Recycler(emoticon1, context, page);
    }
}
