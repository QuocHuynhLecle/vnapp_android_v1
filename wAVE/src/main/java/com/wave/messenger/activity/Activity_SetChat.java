package com.wave.messenger.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_MenuPriority;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimSet;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 채팅 설정 및 메뉴 우선순위 결정 화면
 */
public class Activity_SetChat extends Activity {

    String TAG = getClass().getSimpleName();

    Constants.SET_PERMISION_TYPE SET_PERMISION_TYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_chat);

        initView();

        getPref();


        setOnClickEvent();


    }


    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_set_chat));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명

        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
    }

    /**
     * 저장된 프피퍼런스 값을 가져와서
     * 화면세팅

     mnuTy        메뉴우선순위 종류(int)                0:컨텐츠 타임라인, 1:오픈채팅방(기본)
     opchtTy        오픈채팅방 개설가능 여부(int)        0:개설불가(기본), 1:개설가능
     snglTy        모임 멤버간 1:1채팅 가능여부(int)                0:불가(기본), 1:가능

     *
     */
    private void getPref() {

        /*if (SharedObject.getProperty_string(Activity_SetChat.this, Constants.mnuTy, "1").equals("0")) {   //0:컨텐츠 타임라인
            ((TextView) findViewById(R.id.tv_set_menu_priority)).setText(getResources().getString(R.string.content_timeline));
        } else {
            ((TextView) findViewById(R.id.tv_set_menu_priority)).setText(getResources().getString(R.string.open_chat));
        }*/


        ((ToggleButton)findViewById(R.id.toggle_open_chat)).setChecked(
                SharedObject.getProperty_string(Activity_SetChat.this, Constants.opchtTy, "0").equals("1"));


        ((ToggleButton)findViewById(R.id.toggle_member_chat)).setChecked(
                SharedObject.getProperty_string(Activity_SetChat.this, Constants.snglTy, "0").equals("1"));

        Log.e(TAG,"opchtTy: "+ SharedObject.getProperty_string(Activity_SetChat.this, Constants.opchtTy, "0")+" "
                + SharedObject.getProperty_string(Activity_SetChat.this, Constants.opchtTy, "0").equals("1"));

        Log.e(TAG,"snglTy: "+ SharedObject.getProperty_string(Activity_SetChat.this, Constants.snglTy, "0")+""
                + SharedObject.getProperty_string(Activity_SetChat.this, Constants.snglTy, "0").equals("1"));
    }




    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //우선순위
        /*findViewById(R.id.tv_set_menu_priority).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.mnuTy;
                showMenuPriorityDialog();
            }
        });*/


        //오픈채팅방 개설
        ((ToggleButton) findViewById(R.id.toggle_open_chat)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getApplicationContext(), "오픈채팅방개설 " + isChecked, Toast.LENGTH_SHORT).show();

                SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.opchtTy;

                VoMoimSet voMoimSet = new VoMoimSet();
                if (isChecked) {
                    voMoimSet.setOpchtTy("1");  //opchtTy
                } else {
                    voMoimSet.setOpchtTy("0");
                }
                MoimConfigRequest(voMoimSet);

            }
        });

        //모임 멤버간 1:1채팅
        ((ToggleButton) findViewById(R.id.toggle_member_chat)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getApplicationContext(), "모임 멤버간 1:1채팅 " + isChecked, Toast.LENGTH_SHORT).show();
                //if (isChecked) showOpenChatInfoDialog(false);

                SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.snglTy;

                VoMoimSet voMoimSet = new VoMoimSet();
                if (isChecked) {
                    voMoimSet.setSnglTy("1");
                } else {
                    voMoimSet.setSnglTy("0");
                }
                MoimConfigRequest(voMoimSet);

            }
        });


        //대화내용 서버 보관기간
        /*findViewById(R.id.ll_keep_server).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageKeepPeriodDialog();
            }
        });*/
    }


    /**
     * 모임설정
     * 공개 제한,가입 제한
     */
    private void MoimConfigRequest(VoMoimSet voMoimSet) {
        Moa.moimConfig(this, voMoimSet, mMainActivityHandler);
    }


    // 결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                String strResult;
                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_CONFIG:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + " SET_PERMISION_TYPE: " + SET_PERMISION_TYPE);

                        if (strResult.equals(Constants.SUCCESS)) {
                            // 결과 이후 값저장, 바뀐텍스트 처리
                            switch (SET_PERMISION_TYPE) {
                                case mnuTy:    //메뉴우선순위 종류(int)    0:컨텐츠 타임라인, 1:오픈채팅방(기본)
                                    String mnuTy = data.body.getJson("params").getString("mnuTy");
                                    SharedObject.setProperty_string(Activity_SetChat.this, Constants.mnuTy, mnuTy);
                                    //serResultTextChange((TextView)findViewById(R.id.tv_permission1), mnuTy);

                                    if (mnuTy.equals("0")) {
                                        ((TextView) findViewById(R.id.tv_set_menu_priority)).setText(getResources().getString(R.string.content_timeline));
                                    } else {
                                        ((TextView) findViewById(R.id.tv_set_menu_priority)).setText(getResources().getString(R.string.open_chat));
                                    }
                                    break;

                                case opchtTy:    //오픈채팅방 개설가능 여부(int)    0:개설불가(기본), 1:개설가능
                                    String opchtTy = data.body.getJson("params").getString("opchtTy");
                                    SharedObject.setProperty_string(Activity_SetChat.this, Constants.opchtTy, opchtTy);

                                    if (opchtTy.equals("1")) showOpenChatInfoDialog(true);

                                    //Log.e(TAG, "opchtTy : " + opchtTy + " " + opchtTy.equals("1"));

                                    ((ToggleButton)findViewById(R.id.toggle_open_chat)).setChecked(opchtTy.equals("1"));
                                    break;

                                case snglTy:    //모임 멤버간 1:1채팅 가능여부(int)    0:불가(기본), 1:가능
                                    String snglTy = data.body.getJson("params").getString("snglTy");
                                    SharedObject.setProperty_string(Activity_SetChat.this, Constants.snglTy, snglTy);

                                    //Log.e(TAG, "snglTy : " + snglTy + " " + snglTy.equals("1"));

                                    if (snglTy.equals("1")) showOpenChatInfoDialog(false);

                                    ((ToggleButton)findViewById(R.id.toggle_member_chat)).setChecked(snglTy.equals("1"));

                                    break;

                            }


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });


    /**
     * true: 오픈채팅방 개설 안내 다이얼로그
     * false: 모임 멤버간 1:1채팅 안내 다이얼로그
     *
     * @param type
     */
    private void showOpenChatInfoDialog(boolean type) {
        String strInfo;
        if (type) {
            strInfo = getString(R.string.set_chat_dialog_info, getResources().getString(R.string.open_chat));
        } else {
            strInfo = getString(R.string.set_chat_dialog_info, getResources().getString(R.string.member_chat));
        }

        final Dialog_Text dialog = new Dialog_Text(Activity_SetChat.this, strInfo, Constants.DIALOG_BUTTON_TYPE.confirm);
        //dialog.setData(getResources().getString(R.string.leader_remove_dialog_info));
        dialog.setConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * 메뉴우선순위 다이얼로그
     */
    private void showMenuPriorityDialog() {
        //변경 팝업
        final Dialog_MenuPriority mDialog = new Dialog_MenuPriority(Activity_SetChat.this);
        mDialog.show();
        mDialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VoMoimSet voMoimSet = new VoMoimSet();
                if (v.isSelected()) {
                    //컨텐츠 타임라인
                    Toast.makeText(getApplicationContext(), "컨텐츠 타임라인", Toast.LENGTH_SHORT).show();
                    voMoimSet.setMnuTy("0");
                } else {
                    //모든채팅방
                    Toast.makeText(getApplicationContext(), "오픈채팅방", Toast.LENGTH_SHORT).show();
                    voMoimSet.setMnuTy("1");
                }

                MoimConfigRequest(voMoimSet);

                mDialog.dismiss();
            }
        });
    }


}
