package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;

import java.util.UUID;

/**
 * 마이페이지
 * Created by aveapp on 2017. 7. 25..
 */

public class Activity_MyPage extends BaseActivity implements View.OnClickListener {
    private ImageView iv_profile;
    private RelativeLayout ll_back;
    private TextView tv_name;
    private LinearLayout ll_moim_join_ing, ll_talkProfile_Set;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_mypage);

        init();
        setView();
    }

    private void init() {

        iv_profile = (ImageView) findViewById(R.id.iv_profile);
        ll_back = (RelativeLayout) findViewById(R.id.ll_back);
        tv_name = (TextView) findViewById(R.id.tv_name);
        ll_moim_join_ing = (LinearLayout) findViewById(R.id.ll_moim_join_ing);
        ll_talkProfile_Set = (LinearLayout) findViewById(R.id.ll_talkProfile_Set);

        ll_back.setOnClickListener(this);
        ll_moim_join_ing.setOnClickListener(this);
        ll_talkProfile_Set.setOnClickListener(this);

    }

    private void setView() {
        final String url = Constants.CHATDAWN_PROC + "?type=2&userid=" + MessengerInfo.getUserId(this);

        tv_name.setText(MessengerInfo.getUserName(getBaseContext()));

        Glide.with(Activity_MyPage.this).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.ic_default_profile).dontAnimate().into(iv_profile);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setView();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ll_back) {
            finish();

        } else if (i == R.id.ll_moim_join_ing) { //가입 신청 중인 모임
            startActivity(new Intent(Activity_MyPage.this, Activity_MoimJoin_Ing.class));

        } else if (i == R.id.ll_talkProfile_Set) {  //톡 프로필 설정
            Bundle bundle = new Bundle();
            Intent intent = new Intent(Activity_MyPage.this, Activity_Profile.class);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }
}
