package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.adapter.GalleryDetailAdapter;
import com.wave.messenger.helper.UITopbarHelper;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.mvp.Gallery.GalleryPresenter;
import com.wave.messenger.utility.ErrorController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Activity_GalleryDetail extends Activity implements GalleryDetailAdapter.OnDetailClickListener {

    private UITopbarHelper llTopBar;
    private RecyclerView rvImageList;
    private GalleryPresenter galleryPresenter;

    private GalleryDetailAdapter adapter;
    private GalleryDataModel item;

    private int maxCount;
    private int count =0;

    private List<GalleryDataModel> resultList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_gallerydetail2);
        this.galleryPresenter = new GalleryPresenter();

        getData();
        initView();
        setEvent();
        setListView();

    }

    private void setListView() {
        List<GalleryDataModel> model = galleryPresenter.getDetailDatas2(item.getDirectoryPath(), this);
        adapter = new GalleryDetailAdapter(model, this, this);
        rvImageList.setAdapter(adapter);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();

        if(bundle.containsKey("maxCount") && bundle.containsKey("item")){
            maxCount = bundle.getInt("maxCount");
            item =(GalleryDataModel) bundle.getSerializable("item");
        }else{ //no parameter, finish.
            finish();
        }
    }

    private void initView() {
        llTopBar = new UITopbarHelper((LinearLayout)findViewById(R.id.llTopBar));
        llTopBar.setText(getResources().getString(R.string.title_gallery));
        llTopBar.setGravity(Gravity.CENTER);
        llTopBar.setTextColor(getResources().getColor(R.color.black));
//        llTopBar.setCount(null);
//        llTopBar.setSearchFriendClickListener(null);
        llTopBar.setHomeVisibility(View.VISIBLE);
        llTopBar.setMenuIconVisibility(View.GONE);
        llTopBar.setMenuIcon(R.drawable.img_top_tlttle_complete_android);

        rvImageList = (RecyclerView) findViewById(R.id.rvImageList);
        rvImageList.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void setEvent() {
        llTopBar.setHomeBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //'완료' 버튼. 선택된 사진을 전송한다.
        llTopBar.setMenuBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("item", (Serializable)resultList);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }


    @Override
    public void onClickListItem(GalleryDataModel item, int position, boolean isSelected) {
        ErrorController.showMessage(item.getFullPath());

        if(isSelected){
            llTopBar.setMenuIconVisibility(View.VISIBLE);
            count++;
            resultList.add(item);
        }else{
            llTopBar.setMenuIconVisibility(View.GONE);
            count--;
            resultList.remove(item);
        }


    }

    //이미 선택이 되있는데 다른 사진을 클릭한 경우..
    @Override
    public void onAttemptNewSelection() {
        Toast.makeText(this, "이미 선택된 사진이 있습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_right);
    }
}
