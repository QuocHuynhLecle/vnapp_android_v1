package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 10..
 *
 * 주제별 모임 찾기 화면
 * &
 * 모임개설 화면
 */
public class Activity_MoimFindBySubject extends BaseActivity {

    String TAG=getClass().getSimpleName();
    public static Activity_MoimFindBySubject activity=null;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_searchbysubject);

        activity=this;

        Intent intent = getIntent();
        //int bType=intent.getIntExtra("TYPE",0); //주제별 모임:0  모임개설화면:1

        //if(Constants.SET_MOIM_LIST.moim_create==intent.getSerializableExtra(Constants.MOIM_TYPE)){
        //}

        Serializable bType=intent.getSerializableExtra(Constants.MOIM_TYPE);
        initView(bType);
        setOnClickEvent(bType);
    }

    private void setOnClickEvent(final Serializable bType) {

        ArrayList<LinearLayout> arTextview = new ArrayList<>();
        for (int i = 0; i < 2; i++) { // 카테고리 2개만 쓰므로 6->2로 수정 (i < 6 -> i < 2)
            {
                String buttonID = "imgb_subject" + (i+1);
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                arTextview.add((LinearLayout) findViewById(resID));
                final int nSubject = i;
                arTextview.get(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            intentMoimList(nSubject+1, bType);

                    }
                });
            }
        }


        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    /**
     * @param nSubject 선택한 주제
     * @param bType 0:모임리스트 1:모임개설
     */
    private void intentMoimList(int nSubject, Serializable bType) {
        Intent intent;

        if(bType== Constants.SET_MOIM_LIST.search_by_subject){
            intent= new Intent(Activity_MoimFindBySubject.this, Activity_MoimList.class);
            intent.putExtra("TYPE", nSubject-1); //Activity_MoimList 에서는 0~5 로 분류됨
        }else{
            intent= new Intent(Activity_MoimFindBySubject.this, Activity_MoimCoverSet.class);   //모임 이름 커버 설정, 모임개설
            //intent.addFlags(FLAG_ACTIVITY_NO_HISTORY);
            intent.putExtra(Constants.SET_COVER, Constants.NEW_COVER);
            intent.putExtra(Constants.MOIM_TAG, nSubject);



        }
        startActivity(intent);
    }

    private void initView(Serializable bType) {
        if(bType== Constants.SET_MOIM_LIST.search_by_subject){
            findViewById(R.id.ll_moim_open_info1).setVisibility(View.GONE);
            findViewById(R.id.ll_moim_open_info2).setVisibility(View.GONE);
            ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.search_by_subject));
        }else{
            ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.moim_open));
        }



    }
}
