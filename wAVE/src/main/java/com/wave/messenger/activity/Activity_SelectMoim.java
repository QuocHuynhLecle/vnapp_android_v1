package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.SelectMoimAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.vo.VoMoimHiddenList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임선택 화면
 */
public class Activity_SelectMoim extends BaseActivity {

    String TAG = getClass().getSimpleName();
    public static Activity_SelectMoim activity;

    SelectMoimAdapter mSelectMoimAdapter;


    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_select_moim);

        activity = this;
        initView();

        setOnClickEvent();
        setRecyclerView();

    }

    /**
     * 상단 타이틀 설정
     */
    private void initView() {

        //Log.e(TAG,"SharedObject.getProperty_boolean(this, Constants.MOIM_PUBLIC, false): "+SharedObject.getProperty_boolean(this, Constants.MOIM_PUBLIC, false));

        if(SharedObject.getProperty_string(Activity_SelectMoim.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_SHARE).equals(Constants.MOIM_WRITE_TYPE_SHARE)){

        //if (SharedObject.getProperty_boolean(this, Constants.MOIM_PUBLIC, false)) { //공유할 모임 선택
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.public_moim_select));//공유할 모임선택
            //SharedObject.setProperty_boolean(Activity_SelectMoim.this, Constants.MOIM_WRITE_MAIN, false);
            SharedObject.setProperty_string(Activity_SelectMoim.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_SHARE); //글쓰기_공유


        } else {//메인타임라인 상단 글쓰기 - 모임선택
            //모임선택
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_select));//모임선택
            SharedObject.setProperty_string(Activity_SelectMoim.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_MAIN_WRITE); //글쓰기_공유
            //SharedObject.setProperty_boolean(Activity_SelectMoim.this, Constants.MOIM_WRITE_MAIN, true);

        }

        findViewById(R.id.tv_complete).setVisibility(View.GONE);

    }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        //확인
        /*findViewById(R.id.tv_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSelectMoimAdapter.getMmid() != null) {

                    VoMoimHiddenList.VoMoimHiddenListItem item = mSelectMoimAdapter.getMmid();


                    //모임 아이디, 모임명 저장
                    SharedObject.setProperty_string(Activity_SelectMoim.this, Constants.MOIM_ID, item.getMmId());
                    SharedObject.setProperty_string(Activity_SelectMoim.this, Constants.mmNm, item.getMmNm());

                    //글쓰기페이지
                    intentWrite();

                }else{
                    Toast.makeText(Activity_SelectMoim.this, "모임을 선택해주세요.", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }

    /**
     * 글쓰기화면으로 넘어감
     */
    private void intentWrite() {
        Intent intent = new Intent(this, Activity_MoimWrite.class);
        startActivity(intent);
    }


    /**
     * 모임리스트를 호출한다.
     */
    private void moimListRequest() {
        MOALog.w("Fragment_MoimTab2_MyMoimList moimListRequest");
        Moa.moimListRequest(this, "0", mMainActvtHandler);
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST:    //모임리스트
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoMoimHiddenList voMoimList;
                            Gson gson = new Gson();
                            voMoimList = gson.fromJson(data.body.toString(), VoMoimHiddenList.class);

                            Log.e(TAG, "voMoimHiddenList size: " + voMoimList.getParams().size());

                            //setRecyclerView(voMoimHiddenList);

                            setDate(voMoimList);
                        }


                        break;

                    case PacketTypes.PTC_IMS_MOIM_HIDDEN:    //모임 숨김/해제


                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     *
     * @param voBoardList
     */
    private void setDate(VoMoimHiddenList voBoardList) {

        mSelectMoimAdapter.setItem(voBoardList.getParams());
        mSelectMoimAdapter.notifyDataSetChanged();
        //mSelectMoimAdapter=false;  //
    }


    private void setRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        mSelectMoimAdapter = new SelectMoimAdapter(this);
        recyclerView.setAdapter(mSelectMoimAdapter);
        moimListRequest();

    }

}
