package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;

import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * 알림 설정 화면
 */
public class Activity_SetNotify extends BaseActivity {

    private static final String TAG = "Activity_SetNotify";

    Constants.SET_NOTY_TYPE SET_NOTY_TYPE;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_notify);

        getPref();
        initView();
        setOnClickEvent();
    }

    /**
     *  7.4모임프로필에서 받아와서 프리퍼런스로 저장된 값 가져와서 화면에 세팅
     */
    private void getPref() {
        ((ToggleButton) findViewById(R.id.toggle_recive_push)).setChecked(
                SharedObject.getProperty_string(this,Constants.useMmPush, "1").equals("1")); //모임 알림 사용여부(int)        0:사용안함, 1:사용

        if(SharedObject.getProperty_string(this,Constants.useMmPush, "1").equals("1")){
            findViewById(R.id.ll_toggle_writing_notify).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_toggle_reply_notify).setVisibility(View.VISIBLE);

            ((TextView)findViewById(R.id.tv_info)).setText(getResources().getString(R.string.set_notify_info_on));

        }else{
            findViewById(R.id.ll_toggle_writing_notify).setVisibility(View.GONE);
            findViewById(R.id.ll_toggle_reply_notify).setVisibility(View.GONE);

            ((TextView)findViewById(R.id.tv_info)).setText(getResources().getString(R.string.set_notify_info_off));

        }

        ((ToggleButton) findViewById(R.id.toggle_writing_notify)).setChecked(
                SharedObject.getProperty_string(this,Constants.useMmBbsPush, "1").equals("1")); //모임 글 알림 사용여부(int)                0:사용안함, 1:사용

        ((ToggleButton) findViewById(R.id.toggle_reply_notify)).setChecked(
                SharedObject.getProperty_string(this,Constants.useMmRpyPush, "1").equals("1"));  //모임 댓글 알림 사용여부(int)                0:사용안함, 1:사용
    }


    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.set_notify)); //알림 설정
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명

        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);

        findViewById(R.id.view_top_line).setVisibility(View.GONE);
    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //푸시알림 받기
        ((ToggleButton) findViewById(R.id.toggle_recive_push)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SET_NOTY_TYPE=Constants.SET_NOTY_TYPE.useMmPush;

                if(isChecked){
                    moimNotiConfigRequest("1",null,null);

                    findViewById(R.id.ll_toggle_writing_notify).setVisibility(View.VISIBLE);
                    findViewById(R.id.ll_toggle_reply_notify).setVisibility(View.VISIBLE);

                    ((TextView)findViewById(R.id.tv_info)).setText(getResources().getString(R.string.set_notify_info_on));

                }else{
                    moimNotiConfigRequest("0",null,null);

                    findViewById(R.id.ll_toggle_writing_notify).setVisibility(View.GONE);
                    findViewById(R.id.ll_toggle_reply_notify).setVisibility(View.GONE);

                    ((TextView)findViewById(R.id.tv_info)).setText(getResources().getString(R.string.set_notify_info_off));
                }

            }
        });

        //글알림
        ((ToggleButton) findViewById(R.id.toggle_writing_notify)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SET_NOTY_TYPE=Constants.SET_NOTY_TYPE.useMmBbsPush;

                if(isChecked){
                    moimNotiConfigRequest(null,"1",null);
                }else{
                    moimNotiConfigRequest(null,"0",null);
                }
            }
        });

        //댓글 알림
        ((ToggleButton) findViewById(R.id.toggle_reply_notify)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SET_NOTY_TYPE=Constants.SET_NOTY_TYPE.useMmRpyPush;

                if(isChecked){
                    moimNotiConfigRequest(null,null,"1");
                }else{
                    moimNotiConfigRequest(null,null,"0");
                }

            }
        });
        
        //채팅 알림
        /*((ToggleButton) findViewById(R.id.toggle_chat_notify)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getApplicationContext(), "" + isChecked, Toast.LENGTH_SHORT).show();
                SharedObject.setProperty_boolean(Activity_SetNotify.this,Constants.SET_NOTIFY_CHAT,isChecked);
            }
        });*/

    }


    /**
     * 7.35. 모임 알림 설정Activity_SetPermission
     *
     */
    private void moimNotiConfigRequest(String useMmPush, String useMmBbsPush, String useMmRpyPush) {
        Moa.moimNotiConfig(Activity_SetNotify.this, useMmPush, useMmBbsPush, useMmRpyPush, mMainActvtHandler);
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;

                switch (data.ptc) {

                    case 0x00080038:    //
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            //Toast.makeText(Activity_SetNotify.this, "알림설정 완료", Toast.LENGTH_SHORT).show();

                            //결과값 받아와서 화면 설정.
                            switch (SET_NOTY_TYPE){
                                case useMmPush: //푸시알림 받기

                                    String useMmPush = data.body.getJson("params").getString("useMmPush");
                                    ((ToggleButton) findViewById(R.id.toggle_recive_push)).setChecked( useMmPush.equals("1"));

                                    SharedObject.setProperty_string(Activity_SetNotify.this,Constants.useMmPush, useMmPush);


                                    //Log.e(TAG,"useMmPush : "+useMmPush+" "+useMmPush.equals("1"));
                                    break;

                                case useMmBbsPush:  //글알림

                                    String useMmBbsPush = data.body.getJson("params").getString("useMmBbsPush");
                                    ((ToggleButton) findViewById(R.id.toggle_writing_notify)).setChecked( useMmBbsPush.equals("1"));

                                    SharedObject.setProperty_string(Activity_SetNotify.this,Constants.useMmBbsPush, useMmBbsPush);


                                    //Log.e(TAG,"useMmBbsPush : "+useMmBbsPush +" "+useMmBbsPush.equals("1"));
                                    break;

                                case useMmRpyPush:  //댓글 알림

                                    String useMmRpyPush = data.body.getJson("params").getString("useMmRpyPush");
                                    ((ToggleButton) findViewById(R.id.toggle_reply_notify)).setChecked( useMmRpyPush.equals("1"));

                                    SharedObject.setProperty_string(Activity_SetNotify.this,Constants.useMmRpyPush, useMmRpyPush);

                                    //Log.e(TAG,"useMmRpyPush : "+useMmRpyPush+" "+useMmRpyPush.equals("1"));
                                    break;
                            }


                        } else {
                            Toast.makeText(Activity_SetNotify.this, "알림설정 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



    /**
     * 대화내용 서버보관기간 다이얼로그
     */
    /*private void showMessageKeepPeriodDialog() {
        final Dialog_MessageKeepPeriod dialog = new Dialog_MessageKeepPeriod(Activity_SetNotify.this);
        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case 0:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.no_keep));
                        break;
                    case 1:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.day30));
                        break;
                    case 2:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.one_year));
                        break;
                }

                //Toast.makeText(getApplicationContext(),""+v.getId(), Toast.LENGTH_SHORT).show();
                //TODO 서버통신 완료후처리

                dialog.dismiss();
            }
        });
        dialog.show();
    }*/



}
