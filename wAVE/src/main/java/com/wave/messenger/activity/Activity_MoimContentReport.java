package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Report;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;

import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * 모임 컨텐츠 신고하기 화면
 */
public class Activity_MoimContentReport extends BaseActivity {

    String TAG=getClass().getSimpleName();


    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_content_report);


        initView();
        setOnClickEvent();
    }




    private void initView() {


        Log.e(TAG,"MOIM_REGUSER: "+ SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_REGUSER,""));
        Log.e(TAG,"MOIM_TTL: "+SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_TTL,""));
        Log.e(TAG,"MOIM_ID: "+SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_ID,""));
        Log.e(TAG,"MOIM_MSGID: "+SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_MSGID,""));

        ((TextView)findViewById(R.id.tv_reguser)).setText(SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_REGUSER,"")); //작성자
        ((TextView)findViewById(R.id.tv_ttl)).setText(SharedObject.getProperty_string(Activity_MoimContentReport.this, Constants.MOIM_TTL,"")); //첫줄

        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.content_report));


        ((ImageView)findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.selector_close);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
        //((TextView)findViewById(R.id.tv_topbar_name)).setText("주식하는 사람들");
        //findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);

        }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.ll_report_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReport("1");
            }
        });

        findViewById(R.id.ll_report_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReport("2");
            }
        });

        findViewById(R.id.ll_report_3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReport("3");
            }
        });

        findViewById(R.id.ll_report_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //moimSearchRequest("4");
                dialogReport("4");
            }
        });
    }


    /**
     * 신고하기 다이얼로그
     * @param type
     */
    private void dialogReport(final String type) {

        final Dialog_Report dialog_Report = new Dialog_Report(Activity_MoimContentReport.this,this.getResources().getString(R.string.report_info1));
        dialog_Report.show();

        dialog_Report.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Report.dismiss();

                int i = v.getId();
                if (i == R.id.tv_cancle) {

                }else if(i == R.id.tv_confirm){
                    moimSearchRequest(type);
                }
            }
        });

    }







    /**
     *7.26 모임 글 신고
     * @param rsn
     */
    private void moimSearchRequest(String rsn) {
        //MOALog.w(TAG+" moimWriteRequest" +title+" message: "+message);
        Moa.moimReportRequest(this, rsn,  mMainActvtHandler);
    }


    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case 0x00080029 :    //
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());


                            if(data.body.get("reason")!=null){
                                String reason=(String) data.body.get("reason");

                                if ((reason).equals("already_reported")){
                                    Toast.makeText(Activity_MoimContentReport.this, "이미 신고한 글 입니다", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(Activity_MoimContentReport.this, "reason: "+reason, Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(Activity_MoimContentReport.this, "신고되었습니다", Toast.LENGTH_SHORT).show();
                            }

                            /*String reason=(String) data.body.get("reason");

                            if ((reason).equals("already_reported")){
                                Toast.makeText(Activity_MoimContentReport.this, "이미 신고한 글 입니다", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(Activity_MoimContentReport.this, "신고하기 완료", Toast.LENGTH_SHORT).show();
                            }*/

                            finish();

                            //TODO
                            //setDate(voMoimSearch.getParams());
                            //setRecyclerView(voMoimSearch);

                        }else{
                            Toast.makeText(Activity_MoimContentReport.this, "신고하기 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



}
