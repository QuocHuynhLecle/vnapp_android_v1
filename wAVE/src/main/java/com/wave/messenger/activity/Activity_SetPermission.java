package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_SetPermission;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimSet;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임설정관리 - 멤버들의 권한 설정 화면
 */
public class Activity_SetPermission extends BaseActivity {

    String TAG = getClass().getSimpleName();
    Constants.SET_PERMISION_TYPE SET_PERMISION_TYPE;  //설정 프로토콜 결과 구분

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_permissioin);

        initView();

        setOnClickEvent();

        setDefaultScreen();

    }

    /**
     * 저장된 값 화면세팅
     * [7.4 모임 프로필 에서 받아온 설정 값] 의 텍스트를
     * 화면에 뿌려준다.
     */
    private void setDefaultScreen() {
        Log.e(TAG,"setDefaultScreen");


        for (int i = 0; i < 9; i++) {
            String textViewID = "tv_permission" + (i + 1);
            int resID = getResources().getIdentifier(textViewID, "id", getPackageName());

            Log.e(TAG," etInitValue("+i+") : "+getInitValue(i)+" "+textViewID);
            serResultTextChange((TextView)findViewById(resID) , getInitValue(i));
        }
    }



    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.set_member_permission));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayList<LinearLayout> arImgBtn = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            {
                String buttonID = "ll_select" + (i + 1);
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                arImgBtn.add((LinearLayout) findViewById(resID));
                final int finalI = i;


                String stringId = "set_permission" + (i + 1);
                final int nStrId = getResources().getIdentifier(stringId, "string", getPackageName());

                arImgBtn.get(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPermissionDialog(finalI, nStrId);
                    }
                });
            }
        }
    }

    /**
     * 퍼미션 설정 다이얼로그
     */
    private void showPermissionDialog(int nDialogNum, int nTitleId) {
        Dialog_SetPermission dialog;

        IonEventListener ionEventListener = new IonEventListener() {    //퍼미션 확인 이후
            @Override
            public void onEvent(int selectDialog, int selectItem) {     //다이얼로그 번호, 선택 아이템
                Log.e(TAG, "selectDialog: " + selectDialog + " selectItem: " + selectItem);

                String strPerText = "tv_permission" + (selectDialog + 1); //tv_permission1 부터 차례대로
                int resID = getResources().getIdentifier(strPerText, "id", getPackageName());

                //((TextView) findViewById(resID)).setText( getResources().getIdentifier("set_permission_select" + (selectItem + 1), "string", getPackageName()));

                VoMoimSet voMoimSet = new VoMoimSet();

                switch (selectDialog) {

                    case 0: //모임 이름및 커버설정
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.covTy;
                        voMoimSet.setCovty(selectItem + "");
                        break;

                    case 1: //멤버 가입신청 수락
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.acptTy;
                        voMoimSet.setAcptTy(selectItem + "");
                        break;

                    case 2: //멤버초대
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.ivtTy;
                        voMoimSet.setIvtTy(selectItem + "");
                        break;

                    case 3: //공지글 등록
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.notiTy;
                        voMoimSet.setNotiTy(selectItem + "");
                        break;
                    //-----------------
                    case 4: //글쓰기
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.wrtTy;
                        voMoimSet.setWrtTy(selectItem + "");
                        break;

                    case 5: //앨범 만들기
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.abmTy;
                        voMoimSet.setAbmTy(selectItem + "");
                        break;

                    case 6: //댓글 쓰기
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.rpyTy;
                        voMoimSet.setRpyTy(selectItem + "");
                        break;

                    //-----------------
                    case 7: //다른 멤버의 게시물 댓글 삭제
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.delTy;
                        voMoimSet.setDelTy(selectItem + "");
                        break;

                    case 8: //멤버 탈퇴 차단 설정
                        SET_PERMISION_TYPE = Constants.SET_PERMISION_TYPE.frcTy;
                        voMoimSet.setFrcTy(selectItem + "");
                        break;
                }
                MoimConfigRequest(voMoimSet);
            }
        };

        String initValue = getInitValue(nDialogNum);  //저장된 설정값 가져오기

        Log.e(TAG,"initValue: "+initValue);


        if (nDialogNum > 6) {   //7,8번 구분 선택 2개
            dialog = new Dialog_SetPermission(Activity_SetPermission.this, nDialogNum, Constants.SET_PERMISSION_DIALOG_TYPE.leader, getResources().getString(nTitleId), initValue,ionEventListener);
        } else {    //선택 3개
            dialog = new Dialog_SetPermission(Activity_SetPermission.this, nDialogNum, Constants.SET_PERMISSION_DIALOG_TYPE.all_member, getResources().getString(nTitleId),initValue, ionEventListener);
        }

        //멤버가입 신청 수락 일반 제거
        if(nDialogNum==1){
            dialog = new Dialog_SetPermission(Activity_SetPermission.this, nDialogNum, Constants.SET_PERMISSION_DIALOG_TYPE.leader, getResources().getString(nTitleId),initValue, ionEventListener);
        }

        dialog.show();
    }

    /**
     * 모임프로필 화면에서 저장한 설정값을 프리퍼런스로 가져온다
     * @param nDialogNum
     * @return
     */
    private String getInitValue(int nDialogNum) {

        switch (nDialogNum) {

            case 0: //모임 이름및 커버설정
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.covTy, "2");
            case 1: //멤버 가입신청 수락
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.acptTy, "2");
            case 2: //멤버초대
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.ivtTy, "0");
            case 3: //공지글 등록
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.notiTy, "2");


            //-----------------
            case 4: //글쓰기
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.wrtTy, "0");
            case 5: //앨범 만들기
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.abmTy, "0");
            case 6: //댓글 쓰기
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.rpyTy, "0");


            //-----------------
            case 7: //다른 멤버의 게시물 댓글 삭제
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.delTy, "1");

            case 8: //멤버 탈퇴 차단 설정
                return SharedObject.getProperty_string(Activity_SetPermission.this, Constants.frcTy, "1");

        }
        return null;
    }


    public interface IonEventListener {
        void onEvent(int selectDialog, int selectItem);
    }

    /**
     * 모임설정
     * 공개 제한,가입 제한
     */
    private void MoimConfigRequest(VoMoimSet voMoimSet) {
        Moa.moimConfig(this, voMoimSet, mMainActivityHandler);
    }


    // 결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                String strResult;
                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_CONFIG:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + " SET_PERMISION_TYPE: " + SET_PERMISION_TYPE);

                        if (strResult.equals(Constants.SUCCESS)) {
                            // 결과 이후 값저장, 바뀐텍스트 처리
                            switch (SET_PERMISION_TYPE) {
                                case covTy:    //모임 이름및 커버설정
                                    String covTy = data.body.getJson("params").getString("covTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.covTy, covTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission1), covTy);
                                    break;
                                case acptTy:    //멤버 가입신청 수락
                                    String acptTy = data.body.getJson("params").getString("acptTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.acptTy, acptTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission2), acptTy);
                                    break;

                                case ivtTy:     //멤버초대
                                    String ivtTy = data.body.getJson("params").getString("ivtTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.ivtTy, ivtTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission3), ivtTy);
                                    break;

                                case notiTy:     //공지글 등록
                                    String notiTy = data.body.getJson("params").getString("notiTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.notiTy, notiTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission4), notiTy);
                                    break;
                                //--------------------------
                                case wrtTy:     //글쓰기
                                    String wrtTy = data.body.getJson("params").getString("wrtTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.wrtTy, wrtTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission5), wrtTy);
                                    break;
                                case abmTy:     //앨범 만들기
                                    String abmTy = data.body.getJson("params").getString("abmTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.abmTy, abmTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission6), abmTy);
                                    break;
                                case rpyTy:     //댓글 쓰기
                                    String rpyTy = data.body.getJson("params").getString("rpyTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.rpyTy, rpyTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission7), rpyTy);
                                    break;
                                //---------------------------
                                case delTy:     //다른 멤버의 게시물 댓글 삭제
                                    String delTy = data.body.getJson("params").getString("delTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.delTy, delTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission8), delTy);
                                    break;
                                case frcTy:     //멤버 탈퇴 차단 설정
                                    String frcTy = data.body.getJson("params").getString("frcTy");
                                    SharedObject.setProperty_string(Activity_SetPermission.this, Constants.frcTy, frcTy);
                                    serResultTextChange((TextView)findViewById(R.id.tv_permission9), frcTy);
                                    break;
                            }



                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    /**
     * 서버 통신후 결과 화면
     * 선택한 옵션으로 텍스트를 바꿔준다.
     *
     <string name="set_permission_select1">모든멤버</string>
     <string name="set_permission_select2">리더와 공동리더</string>
     <string name="set_permission_select3">리더만</string>

     * @param viewById
     * @param covTy
     */
    private void serResultTextChange(TextView viewById, String covTy) {

        switch (covTy){
            case "0":   //모든멤버
                viewById.setText(getString(R.string.set_permission_select1));

                //Log.e(TAG,"serResultTextChange: "+viewById.getId()+" "+covTy+"모든멤버" );
                break;

            case "1":   //리더
                viewById.setText(getString(R.string.set_permission_select3));
                //Log.e(TAG,"serResultTextChange: "+viewById.getId()+" "+covTy+"리더" );
                break;

            case "2":   //리더와 공동리더
                viewById.setText(getString(R.string.set_permission_select2));
                //Log.e(TAG,"serResultTextChange: "+viewById.getId()+" "+covTy+"리더와 공동리더" );
                break;
        }
    }
}
