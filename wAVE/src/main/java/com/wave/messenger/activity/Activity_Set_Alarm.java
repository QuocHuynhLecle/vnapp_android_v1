package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 7. 25..
 */

public class Activity_Set_Alarm extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private RelativeLayout ll_back;
    private ToggleButton tb_allow_alarm, tb_allow_ring, tb_allow_vibration;
    private TextView tv_push_text;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_alarm);

        ll_back = (RelativeLayout) findViewById(R.id.ll_back);

        tb_allow_alarm = (ToggleButton) findViewById(R.id.toggle_allow_alarm);
        tb_allow_ring = (ToggleButton) findViewById(R.id.toggle_allow_push_ring);
        tb_allow_vibration = (ToggleButton) findViewById(R.id.toggle_allow_push_vibration);
        tv_push_text = (TextView) findViewById(R.id.tv_push_text);

        if (SharedObject.getProperty_string(Activity_Set_Alarm.this, Constants.push_set, "").equals("0")) {
            tb_allow_alarm.setChecked(false);
            tv_push_text.setText("푸쉬 알림이 꺼져서 소식을 받을 수 없습니다.");
        } else {
            tb_allow_alarm.setChecked(true);
            tv_push_text.setText("푸쉬 알림 수신중입니다.");
        }

        if(SharedObject.getProperty_boolean(Activity_Set_Alarm.this, "push_ring", true)){
            tb_allow_ring.setChecked(true);
        }else {
            tb_allow_ring.setChecked(false);
        }

        if(SharedObject.getProperty_boolean(Activity_Set_Alarm.this, "push_vibration", true)){
            tb_allow_vibration.setChecked(true);
        }else {
            tb_allow_vibration.setChecked(false);
        }

        tb_allow_alarm.setOnCheckedChangeListener(this);
        tb_allow_ring.setOnCheckedChangeListener(this);
        tb_allow_vibration.setOnCheckedChangeListener(this);

        ((LinearLayout) findViewById(R.id.ll_interference)).setOnClickListener(this);
        ll_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ll_interference) {
            startActivity(new Intent(Activity_Set_Alarm.this, Activity_Interference_Off.class));

        } else if (i == R.id.ll_back) {
            finish();

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int i = buttonView.getId();
        if (i == R.id.toggle_allow_alarm) {
            if (isChecked) {
                Moa.UserPushRequest(Activity_Set_Alarm.this, 1, mActivityhandler);
                tv_push_text.setText("푸쉬 알림 수신중입니다.");
            } else {
                Moa.UserPushRequest(Activity_Set_Alarm.this, 0, mActivityhandler);
                tb_allow_vibration.setChecked(false);
                tb_allow_ring.setChecked(false);
                tv_push_text.setText("푸쉬 알림이 꺼져서 소식을 받을 수 없습니다.");
            }


        } else if (i == R.id.toggle_allow_push_ring) {
            if (isChecked) {
                tb_allow_alarm.setChecked(true);
                SharedObject.setProperty_boolean(Activity_Set_Alarm.this, "push_ring", true);
            } else {
                SharedObject.setProperty_boolean(Activity_Set_Alarm.this, "push_ring", false);
            }


        } else if (i == R.id.toggle_allow_push_vibration) {
            if (isChecked) {
                tb_allow_alarm.setChecked(true);
                SharedObject.setProperty_boolean(Activity_Set_Alarm.this, "push_vibration", true);
            } else {
                SharedObject.setProperty_boolean(Activity_Set_Alarm.this, "push_vibration", false);
            }

        }


    }

    private Handler mActivityhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_USER_NOTI_CONFIG:    //모임리스트
                        strResult = (String) data.body.get("result");

                        if (strResult.equals(Constants.SUCCESS)) {

                            SharedObject.setProperty_string(Activity_Set_Alarm.this, Constants.push_set, String.valueOf(data.body.getJson("params").getInt("useNoti")));

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });
}
