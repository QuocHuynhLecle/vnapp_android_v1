package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_TermsHtml;
import com.wave.messenger.util.Constants;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;

/**
 * Created by Joo on 2018. 4. 16.
 */


public class Activity_Join extends AppCompatActivity implements MOACommonListener {

    public static final String TAG = Activity_Join.class.getSimpleName();
    private EditText edit_id;
    private EditText edit_passwd;
    private EditText edit_passwdChk;
    private EditText edit_name;

    private EditText edit_phone_first;
    private EditText edit_phone_middle;
    private EditText edit_phone_last;

    private EditText edit_email;

    private Button btn_find_id;
    private Button btn_find_password;
    private Button btn_join;
    private Button btn_add_info;


    private CheckBox checkBox_agree;
    private boolean agreeFlag;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.titleJoin));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initView();
        setOnClickEvent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    private void initView() {
        edit_id = findViewById(R.id.et_id);
        edit_passwd = findViewById(R.id.et_password);
        edit_passwdChk = findViewById(R.id.et_passwordCheck);
        edit_name = findViewById(R.id.et_name);
        edit_phone_first = findViewById(R.id.et_phone_first);
        edit_phone_middle = findViewById(R.id.et_phone_middle);
        edit_phone_last = findViewById(R.id.et_phone_last);
        edit_email = findViewById(R.id.et_email);

        checkBox_agree = findViewById(R.id.cb_agree);

        btn_join = findViewById(R.id.bt_join);
        btn_add_info=findViewById(R.id.btn_add_info);

        progressBar = findViewById(R.id.progressBar);
    }

    private void setOnClickEvent() {
        checkBox_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (agreeFlag) {
                    agreeFlag = false;
                    checkBox_agree.setChecked(false);
                    return;
                }
                showTerms();
                agreeFlag = true;
                checkBox_agree.setChecked(true);
            }
        });

        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestJoin();
            }
        });

        btn_add_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goAdd_Info();
            }
        });
    }

    private void goAdd_Info(){
        /*Intent go = new Intent(this,Activity_Add_Info.class);
        startActivity(go);*/
    }
    private void showTerms() {
        final Dialog_TermsHtml dialogDefault = new Dialog_TermsHtml(this);
        dialogDefault.setCancelEvent(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(0, 0);
                dialogDefault.dismiss();
                agreeFlag = false;
                checkBox_agree.setChecked(false);
            }
        });
        dialogDefault.show();
    }

    private void requestJoin() {
        Log.d(TAG, "requestJoin()");
        try {
            String inputId = edit_id.getText().toString().trim().toLowerCase();
            edit_id.setText(inputId);
            String inputPasswd = edit_passwd.getText().toString().trim();
            String inputPasswdChk = edit_passwdChk.getText().toString().trim();
            String phoneNumFirst = edit_phone_first.getText().toString().trim();
            String phoneNumMiddle = edit_phone_middle.getText().toString().trim();
            String phoneNumLast = edit_phone_last.getText().toString();
            String inputName = edit_name.getText().toString().trim();
            String inputPhoneNum = (phoneNumFirst + phoneNumMiddle + phoneNumLast).trim();
            String inputEmail = edit_email.getText().toString().trim();

            Log.d(TAG, "input Info? ID : " + inputId + ", Name : " + inputName + ", Password : " + inputPasswd + ", Phone : " + inputPhoneNum);

            if (TextUtils.isEmpty(inputId)) {
                Toast.makeText(this, getString(R.string.toastWarningInputId), Toast.LENGTH_SHORT).show();
                return;
            } else if (TextUtils.isEmpty(inputPasswd)) {
                Toast.makeText(this, getString(R.string.toastWarningInputPassword), Toast.LENGTH_SHORT).show();
                return;
            } else if (inputPasswd.length() < 4) {
                Toast.makeText(this, getString(R.string.toastWarningInputPasswordMore), Toast.LENGTH_SHORT).show();
                return;
            } else if (TextUtils.isEmpty(inputPasswdChk)) {
                Toast.makeText(this, getString(R.string.toastWarningInputPasswordMore), Toast.LENGTH_SHORT).show();
                return;
            } else if (!inputPasswd.equals(inputPasswdChk)) {
                Toast.makeText(this, getString(R.string.toastWarningInputPasswordNotCorrect), Toast.LENGTH_SHORT).show();
                return;
            } else if (TextUtils.isEmpty(inputName)) {
                Toast.makeText(this, getString(R.string.toastWarningInputName), Toast.LENGTH_SHORT).show();
                return;
            } else if (TextUtils.isEmpty(phoneNumFirst) || phoneNumMiddle.length() < 4 || phoneNumLast.length() < 4) {
                Toast.makeText(this, getString(R.string.toastWarningInputPhoneNum), Toast.LENGTH_SHORT).show();
                return;
            } else if (inputPhoneNum.length() < 10) {
                Toast.makeText(this, getString(R.string.toastWarningInputPhoneNumCheck), Toast.LENGTH_SHORT).show();
                return;
            } else if (!agreeFlag) {
                Toast.makeText(this, getString(R.string.toastWarningAgreement), Toast.LENGTH_SHORT).show();
                return;
            }

            MOAClient.getInstance().addEventListener(new MOAEventListener() {
                @Override
                public void OnEvent(MOAData moaData) {
                    mMainActvtHandler.sendMessage(mMainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }
            });
            MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);
            MOAClient.getInstance().setProperties("userId", inputId);
            MOAClient.getInstance().setProperties("userPassword", inputPasswd);
            MOAClient.getInstance().setProperties("userName", inputName);
            MOAClient.getInstance().setProperties("tel", inputPhoneNum);
            MOAClient.getInstance().setProperties("profile_subject", "");
            MOAClient.getInstance().setProperties("email", inputEmail);
            MOAClient.getInstance().register();
            progressBar.setVisibility(View.VISIBLE);

//            MOAClient.getInstance().setProperties("userId", "test01");
//            MOAClient.getInstance().setProperties("userPassword", "12345");
//            MOAClient.getInstance().setProperties("userName", "test01");
//            MOAClient.getInstance().setProperties("tel", "01000000001");
//            MOAClient.getInstance().setProperties("profile_subject", "");
//            MOAClient.getInstance().setProperties("email", "");
//            MOAClient.getInstance().register();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            try {
                final MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                switch (data.ptc) {
                    case 0x00030015:  //8.9 하나은행 회원 가입
                        String result = data.body.get("result").toString();
                        Log.d(TAG, "result = " + result);

                        switch (result) {
                            case "error":
                                String reason = data.body.get("reason").toString();
                                Toast.makeText(Activity_Join.this, getString(R.string.toastWarningDuplicatedId), Toast.LENGTH_SHORT).show();
                                break;
                            case "success":
                                Toast.makeText(Activity_Join.this, getString(R.string.toastSuccessJoin), Toast.LENGTH_SHORT).show();
                                finish();
                                break;

                            default:

                                break;

                        }
                        initMOAClient();
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    private void initMOAClient() {
        progressBar.setVisibility(View.GONE);
        MOAClient.getInstance().logout();   // 로그아웃 호출하여 소켓 연결 해제. 이후 회원가입 요청시 다시 소켓 연결
        MOAClient.getInstance().removeCommonListener(this);
    }

    @Override
    public void onMOALogin(MOAData moaData) {

    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {
        Log.e(TAG, "onMOALoginError");
    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {

    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {
        Log.d(TAG, "onMOARegister");
    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {
    }
}
