package com.wave.messenger.activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.HideFriend_Adapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoHiddenList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 14..
 */

public class Activity_HideFriend extends BaseActivity implements TextWatcher {

    String TAG = getClass().getSimpleName();
    private HideFriend_Adapter hideFriend_adapter;
    private RecyclerView recyclerView;
    private String type;    //0:숨김친구 , 1:차단친구
    private TextView tv_title;
    private EditText editTextFind;
    private VoHiddenList voHiddenList;
    private Context context;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_hidefriend);

        context = this;

        type = getIntent().getExtras().getString("type");

        tv_title = (TextView) findViewById(R.id.tv_title);
        recyclerView = (RecyclerView) findViewById(R.id.rv_hide_friend);
        editTextFind = (EditText) findViewById(R.id.editTextFind);
        ((RelativeLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        editTextFind.addTextChangedListener(this);
        if (type.equals("0")) {
            tv_title.setText("숨김친구 관리");
            Moa.hideListRequest(this, mActivityHandler);
        } else if (type.equals("1")) {
            tv_title.setText("차단친구 관리");
            Moa.blockListRequest(this, mActivityHandler);
        }

    }

    //결과 처리 핸들러
    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);
                String strResult = (String) data.body.get("result");
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_ADDRESS_HIDDEN_LIST:    //

                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Gson gson = new Gson();
                            voHiddenList = gson.fromJson(data.body.toString(), VoHiddenList.class);

                            setHideFriend(voHiddenList.getParams());

                        }
                        break;
                    case PacketTypes.PTC_IMS_ADDRESS_BLOCK_LIST:
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Gson gson = new Gson();
                            voHiddenList = gson.fromJson(data.body.toString(), VoHiddenList.class);

                            setHideFriend(voHiddenList.getParams());

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private void setHideFriend(ArrayList<VoHiddenList> params) {

        ArrayList<VoHiddenList> arVoMoimList = new ArrayList<>();

        for (VoHiddenList item : params) {
            arVoMoimList.add(item);
        }

        Collections.sort(arVoMoimList, new NameAscCompare());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        hideFriend_adapter = new HideFriend_Adapter(Activity_HideFriend.this, arVoMoimList, type);
        recyclerView.setAdapter(hideFriend_adapter);


        ((TextView) findViewById(R.id.tv_hide_count)).setText("친구(" + arVoMoimList.size() + ")");

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        String text = editTextFind.getText().toString().trim();
        if(TextUtils.isEmpty(text)){
            //리스트
            setHideFriend(voHiddenList.getParams());
        }else{//검색
            filter(text);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private ArrayList<VoHiddenList> mVoHiddenList;
    private ArrayList<VoHiddenList> mSearchVoHiddenList;

    public void filter(String charText) {
        mVoHiddenList = new ArrayList<>();
        mSearchVoHiddenList = new ArrayList<>();

        mVoHiddenList = voHiddenList.getParams();

        Collections.sort(mSearchVoHiddenList, new NameAscCompare());

        for (VoHiddenList potion : mVoHiddenList) {

            if (potion.getUserName().contains(charText)) {
                mSearchVoHiddenList.add(potion);
            }else if(potion.getPhone().contains(charText)){
                mSearchVoHiddenList.add(potion);
            }
        }
        ((TextView) findViewById(R.id.tv_hide_count)).setText("친구(" + mSearchVoHiddenList.size() + ")");

        hideFriend_adapter = new HideFriend_Adapter(Activity_HideFriend.this, mSearchVoHiddenList, type);
        recyclerView.setAdapter(hideFriend_adapter);

    }

    private static class NameAscCompare implements Comparator<VoHiddenList> {

        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(VoHiddenList lhs, VoHiddenList rhs) {
            return lhs.getUserName().compareTo(rhs.getUserName());
        }
    }

    public void RemoveAdapter(int index, int size) {
        Log.e(TAG, "RemoveAdapter index: " + index + " size: " + size);

        hideFriend_adapter.notifyItemRemoved(index);
        hideFriend_adapter.notifyItemRangeChanged(index, size);
        hideFriend_adapter.notifyDataSetChanged();

        ((TextView) findViewById(R.id.tv_hide_count)).setText("친구(" + hideFriend_adapter.getItemCount() + ")");

    }
}
