package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wave.messenger.util.Util;
import com.wave.massenger.piggy.R;


/**
 * Created by aveapp on 2017. 8. 8..
 */

public class Activity_ProgramInfo extends BaseActivity {

    private String device_version, store_version;
    private TextView tv_device_version, tv_store_version;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_programinfo);

        ((RelativeLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_device_version = (TextView) findViewById(R.id.device_version);
        tv_store_version = (TextView) findViewById(R.id.store_version);

//        Log.i("TEST", " 버전  : " + getPackageName());
//        int appVersion = Util.getAppVersion(getApplicationContext());
        String appVersion = Util.getAppVersion(getApplicationContext());

//        store_version = MarketVersionChecker.getMarketVersion(getPackageName());
//
//        try {
//            device_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
        tv_device_version.setText(getString(R.string.textVersion) + appVersion);
//        tv_store_version.setText("새 버전 " + store_version + " 업데이트");
//
//        if (store_version.compareTo(device_version) > 0) {
//            // 업데이트 필요
//
//        } else {
//            // 업데이트 불필요
//
//        }


    }
}
