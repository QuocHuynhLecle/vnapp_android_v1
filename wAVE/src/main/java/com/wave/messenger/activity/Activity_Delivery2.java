package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.DeliveryChatListAdapter;
import com.wave.messenger.adapter.FriendListViewAdapter;
import com.wave.messenger.adapter.InviteHorizontaListAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.mvp.Main.IMainView;
import com.wave.messenger.mvp.Main.MainPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.DialogCallback;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2016-10-08.
 */
public class Activity_Delivery2 extends Activity implements IMainView,ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener,
        DialogCallback, InviteHorizontaListAdapter.OnXBtnClickedListener{
    public static Activity_Delivery2 instance=null;
    public static Activity_Delivery2 getInstance(){
        return instance;
    }

    private ImageButton imageButtonBack;
    private TextView textViewInvite;
    private TextView textViewCount;
    private TextView imageViewFind;
    private TextView textViewTitle;
    private ImageView ivSearchIcon;
    private static EditText editTextFind;
    private ImageView ivDeleteText;
    private static ArrayList<String> searchGroupList;
    private static ArrayList<VoFriendList> searchChildList;
    private ExpandableListView expandableListView;
    private FriendListViewAdapter friendListViewAdapter;
    private DeliveryChatListAdapter adapter;
    //이미 있는 목록에 있는 친구
    //상단 리스트
    private static boolean isSearching = false;
    //Horizontal recyclerview
    // ListView Components
    private ArrayList<String> groupList;
    private ArrayList<VoFriendList> childList;
    // presenter
    private MainPresenter mainPresenter;
    private VoFriendList selectedFriend;
    public static boolean ok=false;
    private String type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery2);
//        Statics.setfListState(new FListStatePicker());
        instance=this;
        getData();
        initView();
        setEvent();

        selectedFriend=new VoFriendList();
    }
    private void initView() {
        // init ui component
        imageButtonBack=(ImageButton)findViewById(R.id.imageButtonBack);
        textViewInvite=(TextView)findViewById(R.id.textViewInvite);
        textViewCount=(TextView)findViewById(R.id.textViewCount);
        textViewTitle=(TextView)findViewById(R.id.textViewTitle);
        if("2".equals(type)){
            textViewTitle.setText("보내요대상선택");
        }else if("3".equals(type)){
            textViewTitle.setText("주세요대상선택");
        }
        initViewFriend();
    }
    private void initViewFriend(){

        imageViewFind = (TextView) findViewById(R.id.imageViewFind);
        ivSearchIcon = (ImageView) findViewById(R.id.ivSearchIcon);
        ivDeleteText = (ImageView)findViewById(R.id.ivDeleteText);
        editTextFind = (EditText) findViewById(R.id.editTextFind);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
    }
    private void setEvent() {
        // init listener
        editTextFind.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    imageViewFind.setVisibility(View.GONE);
                    ivDeleteText.setVisibility(View.VISIBLE);
                    isSearching = true;
                    mainPresenter.searchFriendList(s + "", searchText(childList));
                } else {
                    imageViewFind.setVisibility(View.VISIBLE);
                    ivSearchIcon.setVisibility(View.VISIBLE);
                    ivDeleteText.setVisibility(View.GONE);
                    ErrorController.showMessage("[Frag_Frag_main] onTextChanged()");
                    mainPresenter.getFriendList(Activity_Delivery2.this);
                    isSearching = false;
                    searchGroupList = null;
                    searchChildList = null;
                    try {
                        View cView = getCurrentFocus();
                        if (cView != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(cView.getWindowToken(), 0);
                        }
                        expandableListView.requestFocus();
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        expandableListView.setOnChildClickListener(this);
        expandableListView.setOnGroupClickListener(this);
        ivDeleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextFind.setText("");
            }
        });
        textViewInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("onClick");
                /*if ("2".equals(type)) {
                    Fragment_Profile.getInstance().boneyo = true;
                } else if ("3".equals(type)) {
                    Fragment_Profile.getInstance().juseyo = true;
                }*/
                goChatroom();
//                Statics.setfListState(new FListStateNormal());
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // go back to chat room
//                Statics.setfListState(new FListStateNormal());
                ErrorController.showMessage("Click Home");
                setResult(RESULT_CANCELED);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();
        mainPresenter = new MainPresenter(this);
        this.mainPresenter.getFriendList(this);
    }

    private void getData() {
        if(getIntent().hasExtra("type")) {
            try {
                type =  getIntent().getStringExtra("type");

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showFriendList(final ArrayList<String> groupList, final ArrayList<VoFriendList> friendList, final String count) {
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                Activity_Delivery2.this.groupList = groupList;
                Activity_Delivery2.this.childList = friendList;
                friendListViewAdapter = new FriendListViewAdapter(Activity_Delivery2.this,Activity_Delivery2.this.groupList, Activity_Delivery2.this.childList, mainPresenter, true);
                expandableListView.setAdapter(friendListViewAdapter);

                for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }

                return false;
            }
        });
        try {
            handler.sendMessage(handler.obtainMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
        ErrorController.showMessage("Frag_Frag_main, showFriendList2 : friendList size : " + friendList.size() + ", innerList : " + friendList.get(0).getList().size());
        this.searchChildList = friendList;
        this.searchGroupList = groupList;
        friendListViewAdapter = new FriendListViewAdapter(this,
                groupList, friendList, mainPresenter, true);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onHideSuccess(VoFriendList hiddenFriend) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBlockSuccess(VoFriendList blockedFriend) {
        // TODO Auto-generated method stub

    }

    // ListView Item click.
    @Override
    public boolean onChildClick(ExpandableListView parent, View v,
                                int groupPosition, int childPosition, long id) {
        if (!isSearching) {
            VoFriendList item = childList.get(groupPosition).getList()
                    .get(childPosition);
            if(groupPosition==2&& Constants.LISTTYPE_BOOKMARK.equals(groupList.get(groupPosition))){
                for(int i=0;i<childList.get(groupPosition+1).getList().size();i++){
                    childList.get(groupPosition+1).getList().get(i).setSelected(false);
                }
            }
            for(int i=0;i<childList.get(groupPosition).getList().size();i++){
                if(childPosition==i){
                    selectedFriend=item;
                    childList.get(groupPosition).getList().get(i).setSelected(true);
                }else{
                    childList.get(groupPosition).getList().get(i).setSelected(false);
                }
            }
            if (!TextUtils.isEmpty(selectedFriend.getUserId())) {
                textViewInvite.setVisibility(View.VISIBLE);
            } else {
                textViewInvite.setVisibility(View.INVISIBLE);
            }
        }else{
            VoFriendList item = searchChildList.get(groupPosition).getList()
                    .get(childPosition);
            String type = Constants.LISTTYPE_MYFRIEND;
            for(int i=0;i<searchChildList.get(groupPosition).getList().size();i++){

                if(childPosition==i){
                    selectedFriend=item;
                    searchChildList.get(groupPosition).getList().get(i).setSelected(true);
                }else{
                    searchChildList.get(groupPosition).getList().get(i).setSelected(false);
                }
            }
            if (!TextUtils.isEmpty(selectedFriend.getUserId())) {
                textViewInvite.setVisibility(View.VISIBLE);
            } else {
                textViewInvite.setVisibility(View.INVISIBLE);
            }

        }

        friendListViewAdapter.notifyDataSetChanged();
        return true;
    }
    private void setClick(int position,ArrayList<VoFriendList> list){
        for(int i=0;i<list.size();i++){
            if(position==i){

            }else{

            }
        }
    }
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v,
                                int groupPosition, long id) {// from FriendListViewAdapter
        return true;
    }



    @Override
    public void onBackPressed() {
//        Statics.setfListState(new FListStateNormal());
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onConfirm(Object... params) {
        // Dialog callback
        List<VoFriendList> result = (List<VoFriendList>) params[0];
        Intent intent = new Intent();
        intent.putExtra("toInvite", (Serializable) result);
        setResult(Constants.RESULT_INVITE_EVERYONE, intent);
//        Statics.setfListState(new FListStateNormal());
        finish();
    }

    @Override
    public void onDecline(Object... params) {
        // Dialog callback - 비회원은 초대하지 않음.
        List<VoFriendList> data = (List<VoFriendList>) params[0];
        List<VoFriendList> result = mainPresenter.getMemberList(data);

        if (result.size() != 0) {
            // 초대할 사람 있음.
            Intent intent = new Intent();
            intent.putExtra("toInvite", (Serializable) result);
            setResult(Constants.RESULT_INVITE_ONLY_MEMBERS, intent);
//            Statics.setfListState(new FListStateNormal());
            finish();
        } else {
            // 초대할 사람 없음. 아직은 아무것도 안함.
        }

    }// end of onDecline.
    private void goChatroom(){
        ChatListDbHelper listDb = LocalDB.getChatListDbHelper(this);
        UsersDbHelper userDb = LocalDB.getUsersDbHelper(this);
        ArrayList<String> roomIdList=listDb.selectSingleRoom();
        String roomId=userDb.existUserCaht(roomIdList,selectedFriend.getUserId());
        VoChatList chatRoomInfo = new VoChatList();
        chatRoomInfo.setRoom_type("0");
        List<VoFriendList> friends = new ArrayList<VoFriendList>();
        List<VoChatData> chats = new ArrayList<VoChatData>();
        VoFriendList my = new VoFriendList();
        my.setUserId(MessengerInfo.getUserId(this));
        my.setUserName(MessengerInfo.getUserName(this));
        my.setRealUserName(MessengerInfo.getRealUserName(this));
        friends.add(my);
        friends.add(selectedFriend);
        chatRoomInfo.setFriends(friends,this);
        chatRoomInfo.setChats(chats);
        if(!"".equals(roomId)){

            chatRoomInfo.setRoomId(roomId);
        }
//        Statics.setChatRoomState(new ChatRoomStateOneFriend());
//        Statics.getChatRoomState().setChatRoomInfo(chatRoomInfo);
        Bundle bundle = new Bundle();
        bundle.putSerializable("roomInfo", chatRoomInfo);
        Statics.ROOMINFO = chatRoomInfo;
//        Fragment_ChatRoom.getInstance().setArguments(bundle);
//        NMFragmentManager.getInstance().addFragment(Fragment_ChatRoom.getInstance());
        finish();
    }
    //상단 리스트뷰 클릭 리스너
    @Override
    public void onXClicked(VoFriendList friend, int position) {



    }
    private ArrayList<VoFriendList> searchText(ArrayList<VoFriendList> childList) {
        for (VoFriendList item : childList) {
            if (Constants.LISTTYPE_MYFRIEND.equals(item.getUserName())) {
                return item.getList();
            }
        }
        ErrorController.showMessage("ffmain - searchText() - return null");
        return null;
    }

}

