package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.dummy.Testitem;

/**
 * 모임 공지 상세화면
 *
 *
 * Activity_TimeLineDetail 로 이동
 * 현재화면 이용안함
 *
 * @deprecated
 */
public class Activity_MoimNoticeDetail extends BaseActivity {

    Testitem mItem;
    String TAG = "MoimNoticeDetail";

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moim_notice_detail);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //mItem = (Testitem) getIntent().getSerializableExtra("TimeLineItem");
        //Log.e(TAG,"mItem "+mItem.getsContents());



        initView();
        setEventListener();
        setRecyclerView();
    }



    private void initView() {

            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.notice));
            ((TextView) findViewById(R.id.tv_topbar_name)).setText("주식하는 사람들");
            findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);






        //모임 이름
        //((TextView) findViewById(R.id.tv_topbar_title)).setText("모임이름");

        //id
        ((TextView) findViewById(R.id.tv_id)).setText("돈벌때까지 간다");

        //시간
        ((TextView) findViewById(R.id.tv_time)).setText("2017/1/1 오전 09:00");

        //읽음 카운트
        ((TextView) findViewById(R.id.tv_read_counter)).setText("11");

        //컨텐츠
        ((TextView) findViewById(R.id.tv_cotent)).setText("전 멤버들에게 의견을 구합니다");

        //좋아요 카운트
        ((TextView) findViewById(R.id.tv_like_count)).setText("11");


    }


    private void setEventListener() {

        //좋아요
        findViewById(R.id.imgv_like).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"Like");
            }
        });

        //뒤로가기
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void setRecyclerView() {
        /*RecyclerViewHeader header = (RecyclerViewHeader) findViewById(R.id.header);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        //mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        //임의 테스트 아이템 전달
        VoMoimReplyList item;
        ArrayList<VoMoimReplyList> arVoMoimList = new ArrayList<VoMoimReplyList>();
        for (int i = 0;  i< 5 ; i++) {
            item = new VoMoimReplyList("아이디"+i, "img"+i,"좋은정보 감사합니다"+i,"11","22");
            arVoMoimList.add(item);
        }*/

        //ReplyRecyclerAdapter replyRecyclerAdapter = new ReplyRecyclerAdapter(this, arVoMoimList);
        //recyclerView.setAdapter(replyRecyclerAdapter);
        //header.attachTo(recyclerView);
    }


    private void intentMoimList() {
        Intent intent = new Intent(Activity_MoimNoticeDetail.this, Activity_MoimTimeLineList.class);
        //intent.putExtra("TimeLineItem", item);
        startActivity(intent);
    }

}
