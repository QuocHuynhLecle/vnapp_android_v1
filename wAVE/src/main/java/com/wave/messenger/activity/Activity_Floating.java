package com.wave.messenger.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

/**
 * Created by aveapp on 2017. 3. 17..
 */

public class Activity_Floating extends Activity implements View.OnClickListener {

    private LinearLayout ll_back;
    private ImageButton bt_friend, bt_group, bt_close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floating);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        bt_friend = (ImageButton) findViewById(R.id.btn_addfriend);
        bt_group = (ImageButton) findViewById(R.id.btn_addgroup);
        bt_close = (ImageButton) findViewById(R.id.btn_close);

        ll_back.setOnClickListener(this);
        bt_friend.setOnClickListener(this);
        bt_group.setOnClickListener(this);
        bt_close.setOnClickListener(this);

        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.ll_back) {
            setResult(Activity.RESULT_CANCELED);
            finish();
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if(v.getId() == R.id.btn_addfriend) {
            setResult(Constants.RESULT_ADD_FRIEND);
            finish();
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if(v.getId() == R.id.btn_addgroup) {
            setResult(Constants.RESULT_ADD_GROUP);
            finish();
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if(v.getId() == R.id.btn_close) {
            setResult(Activity.RESULT_CANCELED);
            finish();
            this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }
}
