package com.wave.messenger.activity;

import android.os.Handler;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public class MlTimer {

    public interface OnTimerExecuteEvent {
        void onExecute(long milisec);
    }

    Timer mTimer = null;
    TimerTask mTask;
    MainHandler mHandler = null;
    OnTimerExecuteEvent onResponseEvent = null;
    int interval = 0;
    long tick = 0;
    Object tag = null;

    public Object getTag() {
        return tag;
    }

    public void setTag(Object value) {
        tag = value;
    }

    public MlTimer(int interval, OnTimerExecuteEvent onResponseEvent) {
        this.mHandler = new MainHandler();
        this.onResponseEvent = onResponseEvent;
        this.interval = interval;
    }

    public void openTimer() {
        try {
            if(mTimer != null) {
                mTimer.cancel();
                mTimer.purge();
            }

            mTask = new TimerTask() {
                @Override
                public void run() {
                    try {
                        tick += interval;
                        Message eventMsg = mHandler.obtainMessage(1, null);
                        mHandler.sendMessage(eventMsg);
                    } catch (Exception e) {

                    }
                }
            };

            mTimer = new Timer();
            mTimer.schedule(mTask, 0, interval);

        } catch (Exception e) {

        }
    }

    public void closeTimer() {
        if(mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        if (mTask != null) {
            try {
                mTask.cancel();
            } catch (Exception e) {

            }
        }
    }

    private final class MainHandler extends Handler {
        public MainHandler() {
            super();
        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case 1:
                    try {
                        if(onResponseEvent != null) {
                            onResponseEvent.onExecute(tick);
                        }
                    } catch (Exception e) {

                    }
                    return;
                default:
                    return;
            }
        }
    }
}
