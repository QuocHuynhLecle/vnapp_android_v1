package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * 상태메시지
 * Created by aveapp on 2017. 8. 11..
 */

public class Activity_StateMessageChange extends BaseActivity {
    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_statemessagechange);





    ((EditText) findViewById(R.id.et_name)).setText( MessengerInfo.getUserName(getBaseContext()));
        ((EditText) findViewById(R.id.et_statemessage)).setText(com.wave.messenger.util.SharedObject.getProperty_string(Activity_StateMessageChange.this, Constants.STATE_MESSAGE, ""));




        ((TextView) findViewById(R.id.tv_complete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((EditText) findViewById(R.id.et_name)).getText().length() != 0) {
                    Moa.UsernameChangeRequest(Activity_StateMessageChange.this, (((EditText) findViewById(R.id.et_name)).getText().toString()), mActivityhandler);
                } else {
                    Toast.makeText(Activity_StateMessageChange.this, "이름을 입력해 주세요.", Toast.LENGTH_SHORT).show();
                }

                Moa.StateMessageChange(Activity_StateMessageChange.this, mActivityhandler, (((EditText) findViewById(R.id.et_statemessage)).getText().toString()));
            }
        });

        ((ImageView) findViewById(R.id.iv_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private Handler mActivityhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_USER_CHANGE_TITLE:    //상태메시지 변경
                        strResult = (String) data.body.get("result");

                        if (strResult.equals(Constants.SUCCESS)) {

                            SharedObject.setProperty_string(Activity_StateMessageChange.this, Constants.STATE_MESSAGE, data.body.getJson("params").getString("profile_subject"));

                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            if(getCurrentFocus() != null)
                                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            //finish();

                        }
                        break;

                    case PacketTypes.PTC_IMS_USER_CHANGE_USERNAME: //이름변경
                        strResult = (String) data.body.get("result");
                        LBJSONObject params = data.body.getJson("params");
                        if (strResult.equals(Constants.SUCCESS)) {

                            Toast.makeText(Activity_StateMessageChange.this, R.string.profile_state_success, Toast.LENGTH_SHORT).show();

                            MessengerInfo.setUsername(getApplicationContext(), params.getString("userName"));
//                            SharedObject.setProperty_string(Activity_StateMessageChange.this, Constants.STATE_MESSAGE, data.body.getJson("params").getString("profile_subject"));

                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            if(getCurrentFocus() != null)
                                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            finish();

                        }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });
}
