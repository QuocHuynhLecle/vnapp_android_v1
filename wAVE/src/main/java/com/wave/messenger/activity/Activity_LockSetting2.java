package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public class Activity_LockSetting2 extends BaseActivity {

    EditText et_passcode_1 = null;
    EditText et_passcode_2 = null;
    EditText et_passcode_3 = null;
    EditText et_passcode_4 = null;

    TextView tv_num0 = null;
    TextView tv_num1 = null;
    TextView tv_num2 = null;
    TextView tv_num3 = null;
    TextView tv_num4 = null;
    TextView tv_num5 = null;
    TextView tv_num6 = null;
    TextView tv_num7 = null;
    TextView tv_num8 = null;
    TextView tv_num9 = null;

    TextView tv_password;

    LinearLayout ll_delete = null;

    String lockPassword = "";
    String orgLockPassword = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        this.overridePendingTransition(0, 0);
        setContentView(R.layout.activity_lockscreen);
        try {

            init();
            bindEvent();
        } catch (Exception e) {

        }
    }

    void init() {
        try {

            orgLockPassword = getIntent().getExtras().getString("lockPassword");

            et_passcode_1 = (EditText) findViewById(R.id.et_passcode_1);
            et_passcode_2 = (EditText) findViewById(R.id.et_passcode_2);
            et_passcode_3 = (EditText) findViewById(R.id.et_passcode_3);
            et_passcode_4 = (EditText) findViewById(R.id.et_passcode_4);

            tv_num0 = (TextView) findViewById(R.id.tv_num0);
            tv_num1 = (TextView) findViewById(R.id.tv_num1);
            tv_num2 = (TextView) findViewById(R.id.tv_num2);
            tv_num3 = (TextView) findViewById(R.id.tv_num3);
            tv_num4 = (TextView) findViewById(R.id.tv_num4);
            tv_num5 = (TextView) findViewById(R.id.tv_num5);
            tv_num6 = (TextView) findViewById(R.id.tv_num6);
            tv_num7 = (TextView) findViewById(R.id.tv_num7);
            tv_num8 = (TextView) findViewById(R.id.tv_num8);
            tv_num9 = (TextView) findViewById(R.id.tv_num9);

            tv_password = (TextView) findViewById(R.id.tv_password);
            tv_password.setText("암호를 한번 더 입력해주세요.");


            ll_delete = (LinearLayout) findViewById(R.id.ll_delete);

        	/*
        	ImageViewLock1.setImageBitmap(null);
        	ImageViewLock2.setImageBitmap(null);
        	ImageViewLock3.setImageBitmap(null);
        	ImageViewLock4.setImageBitmap(null);
        	*/
        } catch (Exception e) {

        }
    }

    void bindEvent() {
        try {


            tv_num0.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("0");
                    } catch (Exception e) {

                    }
                }
            });

            tv_num1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("1");
                    } catch (Exception e) {

                    }
                }
            });

            tv_num2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("2");
                    } catch (Exception e) {

                    }
                }
            });


            tv_num3.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("3");
                    } catch (Exception e) {

                    }
                }
            });


            tv_num4.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("4");
                    } catch (Exception e) {

                    }
                }
            });


            tv_num5.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("5");
                    } catch (Exception e) {

                    }
                }
            });


            tv_num6.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("6");
                    } catch (Exception e) {

                    }
                }
            });

            tv_num7.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("7");
                    } catch (Exception e) {

                    }
                }
            });

            tv_num8.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("8");
                    } catch (Exception e) {

                    }
                }
            });

            tv_num9.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setDigit("9");
                    } catch (Exception e) {

                    }
                }
            });

            ll_delete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    try {
                        setBackDigit();
                    } catch (Exception e) {

                    }
                }
            });
        } catch (Exception e) {

        }
    }


    void setBackDigit() {
        int len = lockPassword.length();

        int imgResource = R.drawable.shape_round_white;
        int imgResource_checked = R.drawable.shape_round_blue;


        if(len <= 0) {
            return;
        }

        lockPassword = lockPassword.substring(0, lockPassword.length()-1);

        len = lockPassword.length();


        if(len == 0) {
            et_passcode_1.setBackgroundResource(imgResource);
            et_passcode_2.setBackgroundResource(imgResource);
            et_passcode_3.setBackgroundResource(imgResource);
            et_passcode_4.setBackgroundResource(imgResource);
        } else if(len == 1) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource);
            et_passcode_3.setBackgroundResource(imgResource);
            et_passcode_4.setBackgroundResource(imgResource);
        } else if(len == 2) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
            et_passcode_3.setBackgroundResource(imgResource);
            et_passcode_4.setBackgroundResource(imgResource);
        } else if(len == 3) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
            et_passcode_3.setBackgroundResource(imgResource_checked);
            et_passcode_4.setBackgroundResource(imgResource);
        } else if(len == 4) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
            et_passcode_3.setBackgroundResource(imgResource_checked);
            et_passcode_4.setBackgroundResource(imgResource_checked);
        }
    }


    void setDigit(String value) {
        int len = lockPassword.length();
        int imgResource = R.drawable.shape_round_white;
        int imgResource_checked = R.drawable.shape_round_blue;

        if(len > 4) {
            return;
        }

        lockPassword += value;
        len = lockPassword.length();


        if(len == 1) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
        } else if(len == 2) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
        } else if(len == 3) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
            et_passcode_3.setBackgroundResource(imgResource_checked);

        } else if(len == 4) {
            et_passcode_1.setBackgroundResource(imgResource_checked);
            et_passcode_2.setBackgroundResource(imgResource_checked);
            et_passcode_3.setBackgroundResource(imgResource_checked);
            et_passcode_4.setBackgroundResource(imgResource_checked);
        }

        if(len > 3) {
            //다음 설정 화면으로 이동.

            if(!orgLockPassword.equals(lockPassword)) {

                lockPassword = "";
                et_passcode_1.setBackgroundResource(imgResource);
                et_passcode_2.setBackgroundResource(imgResource);
                et_passcode_3.setBackgroundResource(imgResource);
                et_passcode_4.setBackgroundResource(imgResource);

                Toast.makeText(Activity_LockSetting2.this
                        , "암호가 일치하지 않습니다.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Activity_LockSetting2.this
                        , "암호가 설정 되었습니다.", Toast.LENGTH_SHORT).show();



                String pass = Sha256Enc.getHeshKey(lockPassword);
                MessengerInfo.setLockPassowrd(Activity_LockSetting2.this, pass);
                finish();
            }
        }
    }

    @Override
    public void finish() {
        super.finish();

        this.overridePendingTransition(0, 0);
    }

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        lockPassword = "";
        finish();
    }
}
