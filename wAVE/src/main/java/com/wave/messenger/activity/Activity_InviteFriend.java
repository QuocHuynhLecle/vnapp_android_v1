package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.FriendInviteListViewAdapter;
import com.wave.messenger.adapter.InviteHorizontaListAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Main.IInviteView;
import com.wave.messenger.mvp.Main.InvitePresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.DialogCallback;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * @author geniu
 *         Intent serializable presentFriends - 이미 대화 중인 친구를 제외하기 위한 리스트.
 */
public class Activity_InviteFriend extends FragmentActivity implements IInviteView, TextWatcher, OnGroupClickListener, OnChildClickListener, DialogCallback, InviteHorizontaListAdapter.OnXBtnClickedListener {

    // UI components
    private ImageView imageButtonBack;
    private TextView textViewInvite;
    private TextView textViewCount;
    private TextView imageViewFind;
    private ImageView ivSearchIcon;
    private static EditText editTextFind;
    private ImageView ivDeleteText;
    private LinearLayout linearLayoutChat;
    private static ArrayList<String> searchGroupList;
    private static ArrayList<VoFriendList> searchChildList;
    private TextView buttonBanerClose;

    private LinearLayout linearLayoutBaner;
    private ExpandableListView expandableListView;
    private FriendInviteListViewAdapter friendListViewAdapter;

    //이미 있는 목록에 있는 친구
    private List<VoFriendList> presentList;

    //상단 리스트
    private InviteHorizontaListAdapter inviteHorizontaListAdapter;
    private static boolean isSearching = false;

    //Horizontal recyclerview
    private RecyclerView rvFriendToAdd;

    // ListView Components
    private ArrayList<String> groupList;
    private ArrayList<VoFriendList> childList;

    // presenter
    private InvitePresenter mainPresenter;

    private List<VoFriendList> selectedFriendList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_invite_friend);

        getData();
        initView();
        setEvent();
        mainPresenter = new InvitePresenter(this);
        mainPresenter.getFriendList(this);
    }

    private void getData() {
        if (getIntent().hasExtra("presentFriends")) {
            try {
                presentList = (List<VoFriendList>) getIntent().getSerializableExtra("presentFriends");

                if (presentList != null) {
                    ErrorController.showMessage("[Activity_InviteFriend] presentList size = " + presentList.size());
                } else {
                    ErrorController.showMessage("[Activity_InviteFriend] presentList size = null ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            VoFriendList voFriendList = new VoFriendList();
            presentList = voFriendList.getList();
        }
    }

    private void initView() {
        // init ui component

        imageButtonBack = (ImageView) findViewById(R.id.imageButtonBack);
        textViewInvite = (TextView) findViewById(R.id.textViewInvite);
        imageViewFind = (TextView) findViewById(R.id.imageViewFind);
        ivSearchIcon = (ImageView) findViewById(R.id.ivSearchIcon);
        ivDeleteText = (ImageView) findViewById(R.id.ivDeleteText);
        editTextFind = (EditText) findViewById(R.id.editTextFind);
        textViewCount = (TextView) findViewById(R.id.textViewCount);
        buttonBanerClose = (TextView) findViewById(R.id.buttonBanerClose);

        linearLayoutBaner = (LinearLayout) findViewById(R.id.linearLayoutBaner);
        linearLayoutChat = (LinearLayout) findViewById(R.id.linearLayoutChat);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        rvFriendToAdd = (RecyclerView) findViewById(R.id.rvFriendToAdd);
        rvFriendToAdd.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        linearLayoutBaner.setVisibility(View.GONE);

        //상단 리스트뷰는 미리 정의해둠
        inviteHorizontaListAdapter = new InviteHorizontaListAdapter(this, this);
        rvFriendToAdd.setAdapter(inviteHorizontaListAdapter);
    }

    private void setEvent() {
        // init listener
        editTextFind.addTextChangedListener(this);
        expandableListView.setOnChildClickListener(this);
        expandableListView.setOnGroupClickListener(this);
        ivDeleteText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextFind.setText("");
            }
        });
        textViewInvite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("onClick");
                if (selectedFriendList.size() > 0
                        && !mainPresenter.validateInvite(selectedFriendList)) {
                    // 비회원이 있을 경우
//					Dialog_Common.getInviteNonMemberDialog(Activity_InviteFriend.this, selectedFriendList).show();
                } else if (selectedFriendList.size() > 0 && mainPresenter.validateInvite(selectedFriendList)) {
                    //전부 회원인 경우
                    Intent intent = new Intent();
                    intent.putExtra("toInvite", (Serializable) selectedFriendList);
                    setResult(Constants.RESULT_INVITE_ONLY_MEMBERS, intent);
                    finish();
                } else {
                    finish();
                }
            }
        });

        imageButtonBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { // go back to chat room
                ErrorController.showMessage("Click Home");
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    public void showFriendList(ArrayList<String> groupTitleList, ArrayList<VoFriendList> friendList, String count) {
        this.groupList = groupTitleList;
        this.childList = friendList;

        for (VoFriendList vo : childList) {
            if (Constants.LISTTYPE_BOOKMARK.equals(vo.getUserName())) {
                ErrorController.showMessage("[Activity_InviteFriend] 1 favorite count ; " + vo.getList().size() + ", vo index : " + childList.indexOf(vo));
            }
        }

        //int c = 0;
        if (getIntent().hasExtra("presentFriends")) {
            try {//이미 채팅방에 있는 친구는 리스트에서 거름
                if (presentList != null && presentList.size() > 0) {
                    for (VoFriendList present : presentList) {

                        for (VoFriendList section : childList) {
                            List<Integer> toDeleteList = new ArrayList<>();

                            for (VoFriendList friend : section.getList()) {
                                if (present.getUserId().equals(friend.getUserId())) {
                                    toDeleteList.add(section.getList().indexOf(friend));
                                }
                            }

                            for (int i = 0; i < toDeleteList.size(); i++) {
                                int sectionIdx = childList.indexOf(section);
                                int removeIdx = toDeleteList.get(i);
                                childList.get(sectionIdx).getList().remove(removeIdx);
                            }
                        }
                    }
                } else {
                    ErrorController.showMessage("[Activity_InviteFriend] presentList is null so nothing will be deleted. ");
                }
            } catch (Exception e) {
                ErrorController.showMessage("[Activity_InviteFriend] presentList ERROR 02");
                e.printStackTrace();
            }
        }else {
            try {//이미 채팅방에 있는 친구는 리스트에서 거름

                        for (VoFriendList section : childList) {
                            List<Integer> toDeleteList = new ArrayList<>();

                            for (VoFriendList friend : section.getList()) {

                                if(MessengerInfo.getUserId(Activity_InviteFriend.this).equals(friend.getUserId())){
                                    toDeleteList.add(section.getList().indexOf(friend));
                                }

                            }

                            for (int i = 0; i < toDeleteList.size(); i++) {
                                int sectionIdx = childList.indexOf(section);
                                int removeIdx = toDeleteList.get(i);
                                childList.get(sectionIdx).getList().remove(removeIdx);
                            }
                        }


            } catch (Exception e) {
                ErrorController.showMessage("[Activity_InviteFriend] presentList ERROR 02");
                e.printStackTrace();
            }
        }
        for (VoFriendList vo : childList) {
            if (Constants.LISTTYPE_BOOKMARK.equals(vo.getUserName())) {
                ErrorController.showMessage("[Activity_InviteFriend] 2  favorite count ; " + vo.getList().size() + ", vo index : " + childList.indexOf(vo));
            }
        }

        friendListViewAdapter = new FriendInviteListViewAdapter(this, this.groupList, this.childList, mainPresenter, true);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
    }

    @Override
    public void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
//		ErrorController.showMessage("Frag_Frag_main, showFriendList2 : friendList size : " + friendList.size() + ", innerList : " + friendList.get(0).getList().size());

        this.searchChildList = friendList;
        this.searchGroupList = groupList;

        //타이틀바 세팅
        if (getIntent().hasExtra("presentFriends")) {
            try {//이미 있는 친구는 리스트에서 거름
                if (presentList != null && presentList.size() > 0) {

                    for (VoFriendList presentFriend : presentList) {
                        List<Integer> toDeleteList = new ArrayList<>();

                        for (VoFriendList child : searchChildList) {

                            for (VoFriendList child_child : child.getList()) {
                                if (child_child.getUserName().equals(presentFriend.getUserName())) {
                                    toDeleteList.add(child.getList().indexOf(child_child));
                                }
                            }
                            int eraseCount = 0;
                            for (int i : toDeleteList) {
                                child.getList().remove(i - eraseCount);
                                eraseCount++;
                                ErrorController.showMessage("[Activity_InviteFriend] deleting friend...");
                            }
                        }
                        //c++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        friendListViewAdapter = new FriendInviteListViewAdapter(this, groupList, friendList, mainPresenter, true);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onHideSuccess(VoFriendList hiddenFriend) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onBlockSuccess(VoFriendList blockedFriend) {
        // TODO Auto-generated method stub

    }

    // ListView Item click.
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        VoFriendList item;

        if (!isSearching) {
            item = childList.get(groupPosition).getList().get(childPosition);
            LogTrace.E("no Search");
        } else {
            item = searchChildList.get(groupPosition).getList().get(childPosition);
            LogTrace.E("Search");
        }

        if (item.isSelected()) {
            inviteHorizontaListAdapter.remove(item);
            selectedFriendList.remove(item);
            item.setSelected(false);
        } else {
            inviteHorizontaListAdapter.add(item);
            selectedFriendList.add(item);
            item.setSelected(true);
        }

        if (selectedFriendList.size() > 0) {
            textViewCount.setText("(" + String.valueOf(selectedFriendList.size()) + ")");
            textViewInvite.setVisibility(View.VISIBLE);
        } else {
            textViewCount.setText("");
            textViewInvite.setVisibility(View.INVISIBLE);
        }

        friendListViewAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {// from FriendListViewAdapter
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    // 검색 (초성, 단어, 숫자) - callback : showSearchResult();
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() != 0) {
            imageViewFind.setVisibility(View.GONE);
            ivDeleteText.setVisibility(View.GONE);
            ivSearchIcon.setSelected(true);
            this.isSearching = true;
            this.mainPresenter.searchFriendList(s + "", searchText(childList));
        } else {
            imageViewFind.setVisibility(View.VISIBLE);
            ivSearchIcon.setVisibility(View.VISIBLE);
            ivSearchIcon.setSelected(false);
            ivDeleteText.setVisibility(View.GONE);
            ErrorController.showMessage("[Frag_Frag_main] onTextChanged()");
            this.mainPresenter.getFriendList(this);
            this.isSearching = false;
            this.searchGroupList = null;
            this.searchChildList = null;
            try {
                View cView = getCurrentFocus();
                if (cView != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(cView.getWindowToken(), 0);
                }
                expandableListView.requestFocus();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // not used
    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onConfirm(Object... params) {
        // Dialog callback
        List<VoFriendList> result = (List<VoFriendList>) params[0];
        Intent intent = new Intent();
        intent.putExtra("toInvite", (Serializable) result);
        setResult(Constants.RESULT_INVITE_ONLY_MEMBERS, intent);
        finish();
    }

    @Override
    public void onDecline(Object... params) {
        // Dialog callback - 비회원은 초대하지 않음.
        List<VoFriendList> data = (List<VoFriendList>) params[0];
        List<VoFriendList> result = mainPresenter.getMemberList(data);

        if (result.size() != 0) {
            // 초대할 사람 있음.
            Intent intent = new Intent();
            intent.putExtra("toInvite", (Serializable) result);
            setResult(Constants.RESULT_INVITE_ONLY_MEMBERS, intent);
            finish();
        } else {
            // 초대할 사람 없음. 아직은 아무것도 안함.
        }

    }// end of onDecline.

    public void inviteFriend(Context context, String roomId, String inviteUser) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            value.put("inviteUser", inviteUser);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_INVITE, body);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    //상단 리스트뷰 클릭 리스너
    @Override
    public void onXClicked(VoFriendList friend, int position) {
        inviteHorizontaListAdapter.remove(friend);
        selectedFriendList.remove(friend);
        friend.setSelected(false);
        if (selectedFriendList.size() > 0) {

            textViewCount.setText("(" + String.valueOf(selectedFriendList.size()) + ")");
            textViewInvite.setVisibility(View.VISIBLE);
        } else {
            textViewCount.setText("");
            textViewInvite.setVisibility(View.INVISIBLE);
        }
        friendListViewAdapter.notifyDataSetChanged();
    }

    private ArrayList<VoFriendList> searchText(ArrayList<VoFriendList> childList) {
        for (VoFriendList item : childList) {
            if (Constants.LISTTYPE_MYFRIEND.equals(item.getUserName())) {
                return item.getList();
            }
        }
        ErrorController.showMessage("ffmain - searchText() - return null");
        return null;
    }

}
