package com.wave.messenger.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.ChatListExpandableAdapter_2;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.db.UsersReadDbHelper;
import com.wave.messenger.dialog.Dialog_Kick;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.mvp.ChatList.ChatListListener;
import com.wave.messenger.mvp.ChatList.ChatListPresenter;
import com.wave.messenger.mvp.ChatRoom.ChatRoomListener;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

import static com.wave.massenger.piggy.R.id.et_search;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_JongmokList extends Activity implements TextWatcher, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener, ChatListListener, ChatRoomListener {

    static Activity_JongmokList instance = null;

    public static Activity_JongmokList getInstance() {
        return instance;
    }

    public static void clearInstance(Activity_JongmokList value) {
        if (value == instance) {
            instance = null;
        }
    }

    // 검색
    public EditText editTextFind;

    String TAG = this.getClass().getName();

    private ChatListExpandableAdapter_2 adapter;
    private ExpandableListView expandableListView;

    private LinearLayout iv_jongmok_search, ll_none_jongmok, ll_nosearch;

    private boolean isClick = true;
    private ChatListPresenter chatListPresenter;
    private ArrayList<VoChatList> childList = new ArrayList<>();
    private ArrayList<VoChatList> childList_ing = new ArrayList<>();
    private ArrayList<VoChatList> voChatList2 = new ArrayList<>();
    private String roomName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_jongmoklist);

        instance = this;

        Moa.JongMokList(this, mMainActivityHandler);

    }

    public void displaySearchResults(List<String> roomIds) {
        LogTrace.E("adapter notify displaySearchResults");
        adapter.showPartialSearchedList(roomIds);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
//        Moa.JongMokList(this, mMainActivityHandler);
//                setUpdate();
//        ProListRequest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            BusProvider.getInstance().post(new FragmentEventHelper("setOnChatListListener", null, null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        ((TextView) findViewById(R.id.textViewTitle)).setText("종목채팅");
        editTextFind = (EditText) findViewById(et_search);
        iv_jongmok_search = (LinearLayout) findViewById(R.id.iv_jongmok_search);
        ll_none_jongmok = (LinearLayout) findViewById(R.id.ll_none_jongmok);
        ll_nosearch = (LinearLayout) findViewById(R.id.ll_nosearch);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        setListView();
        expandableListView.requestFocus();
    }

    private void setListner() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        editTextFind.addTextChangedListener(this);
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnChildClickListener(this);
        BusProvider.getInstance().post(new FragmentEventHelper("setOnChatListListener", null, this));

        editTextFind.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                chatListPresenter.searchJongmokChatList(editTextFind.getText().toString(), searchText(childList), searchText2(childList));
                return false;
            }
        });

        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final VoChatList item = (VoChatList) parent.getAdapter().getItem(position);

                LogTrace.E("room name : " + item.getRoom_name());
//                Toast.makeText(getApplicationContext(), item.getRoom_name() + "의 대화방 나가기", Toast.LENGTH_SHORT).show();

                if (!"0".equals(item.getEntered())) {
                    getAlert().setTitle("SSAM")
                            .setMessage("'" + item.getRoom_name() + "'" + "종목채팅방에서 나가시겠습니까?")
                            .setPositiveButton("예", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    LoadingManager.with(Activity_JongmokList.this).showLoadingDialog();
                                    exitChatRoom(item.getRoomId());
                                }
                            })
                            .setNegativeButton("아니요", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                }
                return true;
            }
        });

        iv_jongmok_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_JongmokList.this, Activity_jongmokCodeSearch.class));
            }
        });
//        findViewById(R.id.ivJongmokSearch).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mIntent;
//                mIntent = new Intent(Activity_JongmokList.this, Activity_JongmokSearch.class);
//                startActivity(mIntent);
//            }
//        });
    }

    public void setListView() {

        ChatListDbHelper db = LocalDB.getChatListDbHelper(this);
        ArrayList<String> groupList = new ArrayList<>();

        groupList.add(Constants.CHATLISTTYPE_JONGMOK_ING);
        groupList.add(Constants.CHATLISTTYPE_JONGMOK);
        childList = db.getChatJongmokList(voChatList2);

        adapter = new ChatListExpandableAdapter_2(groupList, db.getChatJongmokList(voChatList2), this);
        expandableListView.setAdapter(adapter);

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
    }

    public void setUpdate() {

        ChatListDbHelper db = LocalDB.getChatListDbHelper(this);
        ArrayList<String> groupList = new ArrayList<>();

        groupList.add(Constants.CHATLISTTYPE_JONGMOK_ING);
        groupList.add(Constants.CHATLISTTYPE_JONGMOK);
        adapter = new ChatListExpandableAdapter_2(groupList, db.getChatJongmokList(voChatList2), this);
        expandableListView.setAdapter(adapter);

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        LoadingManager.with(this).hideLoadingDialog();
    }

    public void kickUpdate(final String roomName) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Dialog_Kick dialog_kick = new Dialog_Kick(getApplicationContext());
                dialog_kick.show();
                dialog_kick.setData(roomName);

                ChatListDbHelper db = LocalDB.getChatListDbHelper(getApplicationContext());
                adapter.setChatList(db.getChatJongmokList(voChatList2));
//                lv_fragment_chat_list.setAdapter(adapter);
                expandableListView.setAdapter(adapter);

                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (isClick) {
            isClick = false;
            VoChatList voChatList = (VoChatList) adapter.getChild(groupPosition, childPosition);

            LogTrace.E("chat room : " + voChatList.getRoom_name() + " / roomType : " + voChatList.getRoom_type());

            Intent mIntent;

            if ("3".equals(voChatList.getRoom_type()) || "2".equals(voChatList.getRoom_type())) {
                LogTrace.E("chat list : " + voChatList.getRoomSubject());
                if ("1".equals(voChatList.getEntered()) && !"".equals(voChatList.getRoomSubject())) {
                    Statics.ROOMINFO = voChatList;
                    mIntent = new Intent(this, Activity_ChatRoom.class);
                    startActivity(mIntent);
                } else {
                    mIntent = new Intent(this, Activity_OpenProfile.class);
                    mIntent.putExtra("roomLink", voChatList.getRoomShortId());
                    mIntent.putExtra("roomThumb", voChatList.getRoom_profile_image());
                    mIntent.putExtra("roomTitle", voChatList.getRoom_name());
                    mIntent.putExtra("roomType", voChatList.getRoom_type());
                    mIntent.putExtra("enable", voChatList.getEnableWriteMsg());
                    mIntent.putExtra("roomComment", voChatList.getRoomComment());
                    mIntent.putExtra("roomId", voChatList.getRoomId());
                    mIntent.putExtra("maxUser", voChatList.getMaxUser());
                    mIntent.putExtra("isStaff", voChatList.getStaff());
                    mIntent.putExtra("ownerId", voChatList.getOwnerId());
                    mIntent.putExtra("isEnter", voChatList.getEntered());
                    mIntent.putExtra("roomSubject", "");
                    startActivity(mIntent);
                }
            } else {
                if (groupPosition == 0) {
                    Statics.ROOMINFO = voChatList;
                    LogTrace.E("RoomInfo : " + voChatList);
                    mIntent = new Intent(this, Activity_ChatRoom.class);
                    startActivity(mIntent);
                } else if (groupPosition == 1) {
                    String itemCode;
                    roomName = voChatList.getRoom_name();
                    Statics.ROOMINFO = voChatList;
                    if (LocalDB.getChatListDbHelper(Activity_JongmokList.this).existChatRoom(Statics.ROOMINFO.getRoomId())) {
                        Statics.ROOMINFO = voChatList;
                        mIntent = new Intent(this, Activity_ChatRoom.class);
                        startActivity(mIntent);
                    } else {
//                    itemCode = ItemMaster.getRealSearchName(StringUtil.setJongmok(roomName));
                        LoadingManager.with(Activity_JongmokList.this).showLoadingDialog();
                        itemCode = voChatList.getRoomId().replace("STC_", "");
                        Moa.opentalkenter(Activity_JongmokList.this, itemCode, voChatList.getRoom_name(), "8", mMainActivityHandler);
                    }
                }

            }

            isClick = true;
        }
        return false;
    }

    public VoChatList getRoomInfo(MOAData data, String roomName) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            info.setRoom_name(roomName);
            info.setRoom_type("8");

        }
        return info;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                searchText();
            }
        }, 100);
    }

    private ArrayList<VoChatList> searchText(ArrayList<VoChatList> childList) {
        for (VoChatList item : childList) {
            if (Constants.CHATLISTTYPE_JONGMOK.equals(item.getRoom_name())) {
                return item.getList();
            }
        }

        ErrorController.showMessage("ffmain - searchText() - return null");

        return null;
    }

    private ArrayList<VoChatList> searchText2(ArrayList<VoChatList> childList) {
        for (VoChatList item : childList) {
            if (Constants.CHATLISTTYPE_JONGMOK_ING.equals(item.getRoom_name())) {
                return item.getList();
            }
        }

        ErrorController.showMessage("ffmain - searchText() - return null");

        return null;
    }

    private void searchText() {
        if (editTextFind.getText().toString().length() != 0) {
//            imageViewFind.setVisibility(View.GONE);
//            iv_search.setSelected(true);
//            iv_search.setVisibility(View.GONE);
//            ivDeleteText.setVisibility(View.VISIBLE);

            String value = editTextFind.getText().toString() + "";
            chatListPresenter.searchJongmokChatList(value, searchText(childList), searchText2(childList));
        } else {
            adapter.resetSearch();
            setListView();
//            ivDeleteText.setVisibility(View.GONE);
//            iv_search.setSelected(false);
//            iv_search.setVisibility(View.VISIBLE);
        }
    }

    public AlertDialog.Builder getAlert() {
        AlertDialog.Builder alert = null;
        return alert;
    }

    public void exitChatRoom(String roomId) {
        try {
            ErrorController.showMessage("[FragFragChatList] exitChatRoom check RoomId: " + roomId);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(this));
            value.put("roomId", roomId);
            value.put("exitUserId", MessengerInfo.getUserId(this));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_EXIT, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void exitChatRoom(boolean result, MOAData data) {
        LBJSONObject go = data.body.getJson("params");
        String roomId = go.getString("roomId");

        LogTrace.E("exit : " + go.toString());

        if (result) {
            ChatDataDbHelper dataDb = LocalDB.getChatDataDbHelper(this);
            ChatListDbHelper listDb = LocalDB.getChatListDbHelper(this);
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(this);
            UsersReadDbHelper readDb = LocalDB.getUsersReadDbHelper(this);

            VoChatList item = listDb.getChatData(roomId);

            if ("3".equals(item.getRoom_type())) {
                listDb.updateEnter(item.getRoomId(), "0");
                listDb.updateRoomSubject(item.getRoomId(), "");
                listDb.updateNoti(item.getRoomId(), "1");
                listDb.updateStaff(item.getRoomId(), "0");
            } else {
                LogTrace.E("exit room : " + item.getRoomId());
                listDb.exitChatRoom(item.getRoomId());
            }

            dataDb.exitChatData(roomId);
            userDb.deleteUsers(roomId);
            readDb.deleteUsers(roomId);

            setUpdate();
            BusProvider.getInstance().post(new FragmentEventHelper("setMainTotalCount", null, null));
        }
    }

    @Override
    public void receiveMsg(String roomId, VoChatData data) {
        ChatListDbHelper db = LocalDB.getChatListDbHelper(this);

        if (db.existChatRoom(roomId)) {
            LogTrace.E("receiveMsg");
            setUpdate();
        }
    }

    @Override
    public void sendMsg(MOAData data) {

    }

    @Override
    public void exitRoom(boolean result, MOAData data) {
        exitChatRoom(result, data);
    }

    @Override
    public void createRoom(MOAData data) {

    }

    @Override
    public void chatList(MOAData data) {

    }

    @Override
    public void recRead() {

    }

    @Override
    public void onChatListResult(boolean result) {
        setListView();
    }

    @Override
    public void onChatListUpdate(String roomId) {
        LogTrace.E("onChatListUpdate");
//        setUpdate();
    }

    @Override
    public void onChatRoomInfo(MOAData data) {
        LogTrace.E("onChatRoomInfo");
        setUpdate();
    }

    @Override
    public void onChatReadMy(MOAData data) {
        LogTrace.E("onChatReadMy");
        setUpdate();
    }

    @Override
    public void showSearchList(ArrayList<String> groupList, ArrayList<VoChatList> chatList) {

        if (chatList.get(0).getList().size() == 0 && chatList.get(1).getList().size() == 0) {
            ll_nosearch.setVisibility(View.VISIBLE);
            expandableListView.setVisibility(View.GONE);
        } else {
            ll_nosearch.setVisibility(View.GONE);
            expandableListView.setVisibility(View.VISIBLE);
            adapter = new ChatListExpandableAdapter_2(groupList, chatList, this);
            expandableListView.setAdapter(adapter);

            for (int i = 0; i < adapter.getGroupCount(); i++) {
                expandableListView.expandGroup(i);
            }
            adapter.notifyDataSetChanged();
        }
    }


    //종목대화방결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;

                String strResult;
                switch (data.ptc) {

                    case 0x00040038:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            try {   //gson파싱

                                Gson gson = new Gson();
                                //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                                VoChatList vo;

                                vo = gson.fromJson(data.body.toString(), VoChatList.class);
                                LBJSONArray array = data.body.getArray("params");
                                for (int i = 0; i < array.size(); i++) {
                                    LBJSONObject jo = (LBJSONObject) array.get(i);
                                    vo = VoChatList.loadFromJson(jo);
                                    voChatList2.add(vo);
                                }

                                chatListPresenter = new ChatListPresenter(Activity_JongmokList.this);
                                initView();
                                setListner();
                                LoadingManager.with(Activity_JongmokList.this).hideLoadingDialog();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM:
                        LoadingManager.with(Activity_JongmokList.this).showLoadingDialog();
                        String strResult2 = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult2);

                        if (strResult2.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Intent mIntent;
                            Statics.ROOMINFO = getRoomInfo(data, roomName);
                            mIntent = new Intent(Activity_JongmokList.this, Activity_ChatRoom.class);
                            startActivity(mIntent);
                            LoadingManager.with(Activity_JongmokList.this).hideLoadingDialog();

                        } else {

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


}
