package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 7. 25..
 */

public class Activity_LockScreen_OnOff extends BaseActivity {

    public boolean isLocked = true;
    boolean isHomePressed = false;
    public static Context context;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_lockscreen_onoff);

        context = this;


        if ("".equals(MessengerInfo.getLockPassword(Activity_LockScreen_OnOff.this))) {
            //잠금이 안된 pref true , 토글 false
            ((ToggleButton) findViewById(R.id.toggle_allow_password)).setChecked(false);

            Log.e("LockScreen_OnOff", "false");
        } else {
            Log.e("LockScreen_OnOff", "true");
            ((ToggleButton) findViewById(R.id.toggle_allow_password)).setChecked(true);
        }

        ((RelativeLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((ToggleButton) findViewById(R.id.toggle_allow_password)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String lock = MessengerInfo.getLockPassword(Activity_LockScreen_OnOff.this);
                    if ("".equals(lock)) {
                        Intent intent = new Intent(Activity_LockScreen_OnOff.this, Activity_LockSetting1.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Activity_LockScreen_OnOff.this, ActivityLockCheck.class);
                        intent.putExtra("relaseMode", true);
                        startActivity(intent);
                    }
                } catch (Exception e) {

                }

            }
        });

        ((LinearLayout) findViewById(R.id.ll_password_change)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lock = MessengerInfo.getLockPassword(Activity_LockScreen_OnOff.this);
                if ("".equals(lock)) {
                    Toast.makeText(Activity_LockScreen_OnOff.this, "암호설정을 해주세요.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(Activity_LockScreen_OnOff.this, Activity_LockSetting1.class);
                    startActivity(intent);
                }
            }
        });

    }

}
