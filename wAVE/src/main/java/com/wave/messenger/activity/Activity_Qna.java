package com.wave.messenger.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 8. 8..
 */

public class Activity_Qna extends BaseActivity {

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qna);

        ((View) findViewById(R.id.finish)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
