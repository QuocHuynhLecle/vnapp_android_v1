package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.MyMoimRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_MyMoim_Edit;
import com.wave.messenger.dialog.Dialog_ViewType;
import com.wave.messenger.fragment.Fragment_MoimTab2_MyMoimList;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.HideShowScrollListener;
import com.wave.messenger.util.InterfaceMymoimListener;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimList;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * 2017.06.20
 * 내모임 리스트 화면
 */
public class Activity_MymoimList extends BaseActivity {

    String TAG=getClass().getSimpleName();

    boolean mbRequest=true;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    MyMoimRecyclerAdapter myMoimRecyclerAdapter;

    public  static  Activity_MymoimList activity;

    VoMoimList voMoimList;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_mymoim_list);

        activity=this;

        initView();

        mSwipeRefreshLayout=((SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MOALog.w("Fragment_MoimTab2_MyMoimList onRefresh");
                // Refresh items
                refreshItems();
            }
        });


        if(Util.isNetworkConnected(this))
            setRecyclerView();

        setOnClickEvent();
    }


    private void initView() {
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.mymoim));
    }

    /**
     * 새로고침
     */
    public void refreshItems() {
        MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");
        moimListRequest();

        // Load complete
        onItemsLoadComplete();
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
    }



    /**
     * 모임리스트를 호출한다.
     */
    private void moimListRequest() {
        MOALog.w("Fragment_MoimTab2_MyMoimList moimListRequest" );
        Moa.moimListRequest(Activity_MymoimList.this,"0",mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST :    //모임리스트
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){

                            //VoMoimList voMoimList;
                            Gson gson = new Gson();
                            voMoimList = gson.fromJson(data.body.toString(), VoMoimList.class);

                            //디비저장
                            //MoimDbHelper db1 = LocalDB.getMoimDbHelper(Activity_MymoimList.this);
                            //db1.addMoimList(data, Activity_MymoimList.this);

                            setMoimListDate(voMoimList.getParams());
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



    private void setOnClickEvent() {

        //검색
        findViewById(R.id.imgb_mymoim_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_MymoimList.this, Activity_MoimFind.class);
                startActivity(mIntent);
            }
        });


        //모임만들
        findViewById(R.id.imgb_mymoim_make).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_MymoimList.this, Activity_MoimFindBySubject.class);
                intent.putExtra("TYPE",1);
                startActivity(intent);

            }
        });

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //편집버튼
        findViewById(R.id.imgb_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InterfaceMymoimListener InterfaceMymoimListener=new InterfaceMymoimListener() {
                    @Override
                    public void onSelectItem(int n) {
                        Log.e("setOnClickEvent","onSelectItem "+n );
                        switch (n){ //뷰타입선택
                            case 0:
                                dialogViewType();
                                break;

                            case 1: //모임관리
                                intentMoimManage();
                                break;

                            case 2: //숨긴 모임관리
                                intentInvisivleMoimManage();
                                break;
                        }
                    }
                };
                //Dialog_MyMoim_Edit.getProfileEditCameraDialog(getActivity(),InterfaceMymoimListener).show();

                final Dialog_MyMoim_Edit mDialog = new Dialog_MyMoim_Edit(Activity_MymoimList.this, InterfaceMymoimListener);
                mDialog.show();
            }
        });
    }


    /**
     * 모임관리화면
     */
    private void intentMoimManage() {
        Intent mIntent = new Intent(Activity_MymoimList.this, Activity_MoimManageList.class);
        startActivity(mIntent);
    }

    /**
     * 숨김모임화면
     */
    private void intentInvisivleMoimManage() {
        Intent mIntent = new Intent(Activity_MymoimList.this, Activity_InvisibleMoimManageList.class);
        startActivity(mIntent);
    }



    /**
     * 뷰타입 팝업
     */
    private void dialogViewType() {
        Fragment_MoimTab2_MyMoimList.OnSelectViewTypeInterface onSelectViewTypeInterface = new Fragment_MoimTab2_MyMoimList.OnSelectViewTypeInterface() {
            @Override
            public void onSelectView() {    //뷰타입선택 큰카드형 작은카드형 목록형 설정

               /* setGridLayout(SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE,1));

                myMoimRecyclerAdapter.setViewType(SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE,1));
                myMoimRecyclerAdapter.notifyDataSetChanged();*/
                setGridLayout(SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE,1));
                setMoimListDate(voMoimList.getParams());


            }
        };
        Dialog_ViewType dialog= new Dialog_ViewType(Activity_MymoimList.this, onSelectViewTypeInterface);


        dialog.show();
    }

    /**
     * 리싸이클 뷰
     * 2칸 3칸 1칸 설정한다
     * @param property_int
     */
    private void setGridLayout(int property_int) {

        Log.e(TAG,"#setGridLayout "+property_int);
        switch (property_int){
            case 1:
                mRecyclerView.setLayoutManager(new GridLayoutManager(Activity_MymoimList.this, 2));    //2칸
                break;
            case 2:
                mRecyclerView.setLayoutManager(new GridLayoutManager(Activity_MymoimList.this, 3));    //3칸
                break;
            case 3:
                mRecyclerView.setLayoutManager(new GridLayoutManager(Activity_MymoimList.this, 1));    //1칸
                break;
        }



    }

    public interface OnSelectViewTypeInterface {
        void onSelectView();
    }

    private void setRecyclerView() {
        MOALog.w("Fragment_MoimTab2_MyMoimList setRecyclerView" );

        mRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        //RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 2);
        //mRecyclerView.setLayoutManager(recyclerViewLayoutManager);

        setGridLayout(SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE,1));

        setTopMenu(mRecyclerView);

        moimListRequest();
    }

    /**
     * 디비에서 모임리스트를 가져와서
     * 어댑터세팅
     * @param params
     */
    private void setMoimListDate(ArrayList<VoMoimList> params) {

        ArrayList<VoMoimList> arVoMoimList= new ArrayList<>();

        for (VoMoimList item : params) {
            if(item.getHdn().equals("0")){  //안숨김 리스트만 추가
                arVoMoimList.add(item);
            }
        }

        Collections.sort(arVoMoimList ,myComparator);

        //arVoMoimList = new MoimDbHelper(Activity_MymoimList.this).getContactMoimListInDB(Activity_MymoimList.this); //디비에서 불러올 필요가 없었음...
         //arVoMoimList =collectrion(params);

        arVoMoimList.add(new VoMoimList());//추가 버튼 위한 빈값


        myMoimRecyclerAdapter = new MyMoimRecyclerAdapter(Activity_MymoimList.this  ,arVoMoimList
        , SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE,1));
        mRecyclerView.setAdapter(myMoimRecyclerAdapter);
    }

    /**
     * MmOrd 순서대로 상단정렬
     */
    public final static Comparator<VoMoimList> myComparator = new Comparator<VoMoimList>() {

        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(VoMoimList o1, VoMoimList o2) {

            return collator.compare(o2.getMmOrd(), o1.getMmOrd());
        }
    };

    /**
     * 상,하단 베너 영역

     * @param recyclerView
     *
     */
    private void setTopMenu(RecyclerView recyclerView) {
        //하단 버튼
        recyclerView.addOnScrollListener(new HideShowScrollListener() {
            @Override
            public void onHide() {
                Log.e(TAG,"onHide");

                //싱딘 숨김
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    findViewById(R.id.ll_top_menu).animate().translationY(-findViewById(R.id.ll_top_menu).getHeight()).alpha(0);
                }else{
                    findViewById(R.id.ll_top_menu).setVisibility(View.GONE);
                }

               /* LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) findViewById(R.id.ll_top_menu).getLayoutParams();
                params.height = 0;
                findViewById(R.id.ll_top_menu).setLayoutParams(params);*/
                // do your hiding animation here

            }

            @Override
            public void onShow() {
                Log.e(TAG,"onShow");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    findViewById(R.id.ll_top_menu).animate().translationY(0).alpha(100);
                }else{
                    findViewById(R.id.ll_top_menu).setVisibility(View.VISIBLE);
                }
                // do your showing animation here
            }
        });

    }

}
