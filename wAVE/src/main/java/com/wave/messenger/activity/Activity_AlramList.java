package com.wave.messenger.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.adapter.NewAlramRecyclerAdapter;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoNewAlramList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * 새로운알림 화면
 */
public class Activity_AlramList extends Activity {

    String TAG = this.getClass().getName();

    NewAlramRecyclerAdapter newAlramRecyclerAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private EndlessRecyclerViewScrollListener scrollListener;

    boolean mbRefresh = false;
    // ArrayList<VoMoimSearch.VoMoimSearchItem> mArVoMoimSearchItem= new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_alram_list);


        initView();

        setRecyclerView();

        setOnClickEvent();


        //TODO 호출...
        alramRequest("");
    }


    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.activity_alram));

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        (findViewById(R.id.ll_top_Bar).findViewById(R.id.btn_new_alram)).setVisibility(View.VISIBLE);//모두읽음으로 처리.
    }


    private void setOnClickEvent() {

        (findViewById(R.id.ll_top_Bar).findViewById(R.id.btn_new_alram)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 모두 읽음으로 처리
                //다이얼로그
                if (newAlramRecyclerAdapter.getItemCount() > 0)
                    showAlramReadDialog();
                else
                    Toast.makeText(Activity_AlramList.this, "알림이 없습니다", Toast.LENGTH_SHORT).show();
            }
        });

    }


    /**
     * 5.16 알림방 메시지 리스트 요청
     *
     * @param cid
     */
    private void alramRequest(String cid) {
        Log.e(TAG, " moimNewAlramRequest");
        Moa.alarmListRequest(Activity_AlramList.this, cid,mMainActvtHandler);
    }

    /**
     * 5.15 알림방 메시지 삭제
     * 챗 아이디가 없으면 전체 알림방 메시지를 삭제함.
     *
     */
    public void chatDeleteRequest(String chatId) {
        Moa.alramDeleteRequest(Activity_AlramList.this, chatId, mMainActvtHandler);
    }


    private void setRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        newAlramRecyclerAdapter = new NewAlramRecyclerAdapter(this);
        recyclerView.setAdapter(newAlramRecyclerAdapter);

        //다음페이지 자동 로딩
        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e(TAG, "onLoadMore offset: "+page+" "+totalItemsCount);
                loadNextDataFromApi();

            }
        };
        recyclerView.addOnScrollListener(scrollListener);

        mSwipeRefreshLayout=((SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Log.e(TAG,"SwipeRefreshLayout");
                mbRefresh=true;
                alramRequest("");
            }
        });

    }

    /**
     * 다음페이지 로딩
     * getMsgId
     */
    public void loadNextDataFromApi( ) {
        Log.e(TAG, "loadNextDataFromApi");
        alramRequest(newAlramRecyclerAdapter.getLastCid());
    }

    private void setAdapterItem(ArrayList<VoNewAlramList.VoNewAlramItem> params) {

        newAlramRecyclerAdapter.setItem(params, mbRefresh);
        newAlramRecyclerAdapter.notifyDataSetChanged();

    }
    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
        mbRefresh = false;
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case 0x00050021:    //5.16 알림방 메시지 리스트 요청
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoNewAlramList voNewAlramList;
                            Gson gson = new Gson();
                            voNewAlramList = gson.fromJson(data.body.toString(), VoNewAlramList.class);


                            Log.e(TAG, "voNewAlramList size: " + voNewAlramList.getParams().size());
                            setAdapterItem(voNewAlramList.getParams());



                            onItemsLoadComplete();
                        } else {
                            //Toast.makeText(Activity_MoimFind.this, "새로시작한모임 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;

                    case 0x00050020:    //5.15 알림방 메시지 삭제
                        String strdeleteResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strdeleteResult);

                        if (strdeleteResult.equals(Constants.SUCCESS)) {

                            String chatId = data.body.getJson("params").getString("chatId");

                            if(chatId.equals("")){  //모두삭제
                                newAlramRecyclerAdapter.deleteAll();
                            }else{ //1개만 삭제.
                                newAlramRecyclerAdapter.deleteItem(chatId);
                            }

                            Fragment_Main.getInstance().chatroomReadCountRequest();
                        } else {
                            //Toast.makeText(Activity_MoimFind.this, "새로시작한모임 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 알림 삭제 팝업
     */
    private void showAlramReadDialog() {
        final Dialog_Text dialog = new Dialog_Text(Activity_AlramList.this
                , getString(R.string.alram_read)
                , Constants.DIALOG_BUTTON_TYPE.alram_read);

                dialog.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    chatDeleteRequest("");

                    dialog.dismiss();
                    finish();

            }
            });
            dialog.show();
    }


    public void finish() {
        super.finish();
    }


    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

}
