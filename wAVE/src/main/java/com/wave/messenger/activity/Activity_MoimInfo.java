package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;

import java.io.InputStream;
import java.net.URL;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임 정보 화면
 */
public class Activity_MoimInfo extends BaseActivity {

    String TAG = getClass().getSimpleName();
    String img;
    String moim_name;


    public static Activity_MoimInfo activity = null;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_info);

        activity = this;

        img = getIntent().getStringExtra("profile");
        moim_name = getIntent().getStringExtra("moim_name");

        getPref();
        initView();
        setOnClickEvent();
    }


    private void getPref() {
        ((ToggleButton) findViewById(R.id.toggle_allow_invite)).setChecked(SharedObject.getProperty_boolean(Activity_MoimInfo.this, Constants.SET_ALLOW_INVITE, true));

        //내아이디를  MOIM_USER_PROFILE_ID 로 저장
        SharedObject.setProperty_string(this, Constants.MOIM_USER_PROFILE_ID, MessengerInfo.getUserId(this));
    }


    public void resetThumnail() {
        Util.setMoimProfileGlide(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null)
                , SharedObject.getProperty_string(this, Constants.pfImg, ""), (ImageView) findViewById(R.id.imgv_profile));

    }

    public void initView() {
        //Log.e(TAG,"initView");
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moin_info)); //상단 타이틀 이름
        ((ImageButton) findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.ic_close_press); //뒤로가기 이미지
        findViewById(R.id.view_top_line).setVisibility(View.GONE);

        setPublicInfo(SharedObject.getProperty_string(this, Constants.opnTy, ""));  //공개제한 안내

        ((TextView) findViewById(R.id.tv_moim_title)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명

        Util.setMoimProfileGlide2(this, MessengerInfo.getUserId(this), (ImageView) findViewById(R.id.imgv_profile));

        // ll_stats 모임통계 일단 숨겨둠.

        //권한설정
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getCovTy(this))) {

            if (Util.getMymLv(this).equals("0")) {    //멤버는 통계 숨김
                findViewById(R.id.ll_stats).setVisibility(View.GONE);
                findViewById(R.id.line_center).setVisibility(View.GONE);
            }
        } else {

            if (Util.getMymLv(this).equals("0")) {    //멤버 다 숨김
                findViewById(R.id.ll_moim_set).setVisibility(View.GONE);
            } else if (Util.getMymLv(this).equals("2")) {  //"리더만" 설정시 공동리더는 통계만 보임
                findViewById(R.id.ll_set).setVisibility(View.GONE);
                findViewById(R.id.line_center).setVisibility(View.GONE);
            }
        }

        //리더일경우
        if (Util.getMymLv(this).equals("1")) {
            //탈퇴하기 숨김
            findViewById(R.id.ll_withdraw).setVisibility(View.GONE);
            findViewById(R.id.line_withdraw).setVisibility(View.GONE);

            //모임삭제하기 리더일때만 보임
            findViewById(R.id.ll_delete).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_delete).setVisibility(View.GONE);
        }

        /*
        멤버들의 권한설정 -밴드이름및 커버설정
        [리더만] 일경우
        -5번째메뉴 모임정보화면 [모임설정] 활성/비활성.

        멤버 가입신청 수락
        -4번째 모임정보화면 [가입신청자] 버튼 활성/비활성.

        멤버초대
        -모임타이틀 상단 [초대] 버튼. 활성/비활성 .

        공지글 등록
        -모임리스트 타임라인 게시글 옵션 다이얼로그  [공지로 등록] 활성/비활성.
        -------------------------------------

        글쓰기
        -모임리스트 [글쓰기] 활성/비활성.

        앨범만들기 //TODO 기획

        댓글쓰기
        -모임타임라인 [댓글쓰기] 활성/비활성.

        다른멤버의 게시물 댓글 삭제
        -모임타임라인 게시글 옵션 다이얼로그 [삭제하기] 활성/비활성.

        멤버 탈퇴/차단설정
        -모임정보 가입된 회원리스트 우측 설정 다이얼로그  (프로필, 1:1대화, 강제탈퇴)
        -강제탈퇴 활성/비활성.
        * */
    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //모임설정
        findViewById(R.id.ll_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_SetManage.class);
                startActivity(mIntent);
            }
        });

        //모임통계 웹으로하기로..
        findViewById(R.id.ll_stats).setOnClickListener(new View.OnClickListener() {   //모임통계 웹뷰로 제작 예정
            @Override
            public void onClick(View v) {
                //Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_MoimFindBySubject.class);
                //startActivity(mIntent);
                Toast.makeText(Activity_MoimInfo.this, "서비스 준비중입니다. ", Toast.LENGTH_SHORT).show();
            }
        });

        //알림설정
        findViewById(R.id.ll_set_notify).setOnClickListener(new View.OnClickListener() {   //모임통계 웹뷰로 제작 예정
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_SetNotify.class);
                startActivity(mIntent);
            }
        });

        //이 모임 프로필 설정
        findViewById(R.id.ll_set_profile).setOnClickListener(new View.OnClickListener() {   //모임통계 웹뷰로 제작 예정
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_MoimProfile.class);
                startActivity(mIntent);
            }
        });


        //채팅바로받기
        /*((ToggleButton) findViewById(R.id.toggle_recive_chat)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedObject.setProperty_boolean(Activity_MoimInfo.this, Constants.SET_RECIVE_CHAT, isChecked);
            }
        });*/

        //초대혀용
        ((ToggleButton) findViewById(R.id.toggle_allow_invite)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedObject.setProperty_boolean(Activity_MoimInfo.this, Constants.SET_ALLOW_INVITE, isChecked);
            }
        });

        //모임탈퇴하기
        findViewById(R.id.ll_withdraw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_WithdrawMoim.class);
                startActivity(mIntent);
            }
        });

        //모임 삭제하기
        findViewById(R.id.ll_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog
                showDeleteMoimDialog();
            }
        });

        findViewById(R.id.ll_add_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShortcut(Activity_MoimInfo.this);
            }
        });

    }

    private void addShortcut(Context context) {

        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        shortcutIntent.setClassName(context, getClass().getName());
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
                Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(context, R.drawable.ic_chat_onetoone));
        intent.putExtra("duplicate", false);
        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        sendBroadcast(intent);
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 모임 삭제 팝업
     */
    private void showDeleteMoimDialog() {
        final Dialog_Text dialog = new Dialog_Text(Activity_MoimInfo.this
                , getString(R.string.delete_moim_info)
                , Constants.DIALOG_BUTTON_TYPE.moim_delete);

        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moimDeleteRequest();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * 공개 설정 안내
     * 0:비공개, 1:공개(기본)
     *
     * @param select
     */
    private void setPublicInfo(String select) {

        switch (select) {
            case "0":
                ((TextView) findViewById(R.id.tv_opnty_info)).setText(getResources().getString(R.string.private_moim_info));
                break;

            case "1":
                ((TextView) findViewById(R.id.tv_opnty_info)).setText(getResources().getString(R.string.public_moim_info));
                break;
        }
    }

    /**
     * 모임 삭제
     * 리더이 모임을 삭제한다
     */
    private void moimDeleteRequest() {
        Moa.moimDelete(Activity_MoimInfo.this, mMainActvtHandler);
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_DELETE:    //모임삭제
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Toast.makeText(Activity_MoimInfo.this, "모임 삭제 완료", Toast.LENGTH_SHORT).show();

                            finish();

                            Activity_MoimTimeLineList.activity.finish();
                            //Fragment_MoimTab2_MyMoimList.Fragment.refreshItems();
                            Activity_MymoimList.activity.refreshItems();

                        } else {
                            Toast.makeText(Activity_MoimInfo.this, "모임삭제 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

}