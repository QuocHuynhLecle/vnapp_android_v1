package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.adapter.CustomerListRecyclerAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.util.Constants;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;

import static com.wave.massenger.piggy.R.id.recyclerview;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_CustomerList extends Activity {

    static public Activity_CustomerList activity;

    String TAG=this.getClass().getName();
    CustomerListRecyclerAdapter customerListRecyclerAdapter;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_grouplist);

        activity=this;

        initView();
        setOnClickEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initView() {
        ((TextView)findViewById(R.id.textViewTitle)).setText(Constants.LISTTYPE_CUSTOMER);
        recyclerView = (RecyclerView) findViewById(recyclerview);
        ((ImageView)findViewById(R.id.imgb_add)).setImageResource(0);
//        ArrayList<VoFriendList> customerList = (ArrayList<VoFriendList>) getIntent().getSerializableExtra("customerList");
        ArrayList<VoFriendList> customerList = MessengerInterface.voFriendLists;
        setCustomerListData(customerList);
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.imgb_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    /**
     * 리스트 설정
     */
    public void setRecyclerView(ArrayList<VoFriendList> friendLists) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        customerListRecyclerAdapter = new CustomerListRecyclerAdapter(this, friendLists);
        recyclerView.setAdapter(customerListRecyclerAdapter);
        customerListRecyclerAdapter.notifyDataSetChanged();
    }

    public void setCustomerListData(ArrayList<VoFriendList> customerListData) {

        setRecyclerView(customerListData);
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
