package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Kick;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BUSMOAData;
import com.wave.messenger.utility.DeviceInfo;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;

import org.json.simple.JSONObject;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by Ryan on 2017. 2. 13..
 */

public class Activity_OpenProfile extends Activity implements MOAEventListener, MOACommonListener {
    private TextView tv_title, tv_subtitle;
    private ImageView iv_close, iv_roomprofile, iv_setting, iv_share;
    private Button bt_link, bt_chat;

    private VoChatList room;

    String link, profile, name, comment, id, maxUser, isStaff, shortId, profileUrl, ownerId, isEnter, chatName, roomType, enableWrite;

    Intent mIntent;

    boolean isIng = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_openprofile);

        MOAClient.getInstance().addEventListener(this);

        if(getIntent().hasExtra("roomId")) {
            shortId = getIntent().getStringExtra("roomLink");
            profile = getIntent().getStringExtra("roomThumb");
            name = getIntent().getStringExtra("roomTitle");
            roomType = getIntent().getStringExtra("roomType");
            enableWrite = getIntent().getStringExtra("enable");
            comment = getIntent().getStringExtra("roomComment");
            id = getIntent().getStringExtra("roomId");
            maxUser = getIntent().getStringExtra("maxUser");
            isStaff = getIntent().getStringExtra("isStaff");
            ownerId = getIntent().getStringExtra("ownerId");
            isEnter = getIntent().getStringExtra("isEnter");
            chatName = getIntent().getStringExtra("roomSubject");

            room = new VoChatList();

            room.setRoomId(id);
            room.setRoom_name(name);
            room.setRoomShortId(shortId);
            room.setRoomComment(comment);
            room.setMaxUser(maxUser);
            room.setStaff(isStaff);
            room.setOwnerId(ownerId);
            room.setRoom_profile_image(profile);
            room.setEnableWriteMsg(enableWrite);
            room.setRoom_type(roomType);

            LogTrace.E("Open Profile getIntent : " + roomType);

            init();
        } /*else if(getIntent().hasExtra("shortId")) {
            shortId = getIntent().getStringExtra("shortId");
            LogTrace.E("open info : " + shortId);

            getRoomInfo();
        } */else if(getIntent().getAction().equals(Intent.ACTION_VIEW)) {
            LogTrace.E("open url");

            Uri mUri = getIntent().getData();
            shortId = mUri.getQueryParameter("l");

            LogTrace.E("open link info : " + shortId);

//            if(WAVEMainActivity.staticMainActivity == null) {
//                MOAClient.getInstance().addCommonListener(this);
//                doLogin();
//            } else {
                getRoomInfo();
//            }
        }

    }

    private void init() {
        LogTrace.E("init");

        /*tv_title = (TextView) findViewById(R.id.tv_title);
        tv_subtitle = (TextView) findViewById(R.id.tv_subtitle);
        iv_roomprofile = (ImageView) findViewById(R.id.iv_roomprofile);
        iv_close = (ImageView) findViewById(R.id.iv_close);
        iv_setting = (ImageView) findViewById(R.id.iv_setting);
        iv_share = (ImageView) findViewById(R.id.iv_share);
        bt_link = (Button) findViewById(R.id.bt_link);
        bt_chat = (Button) findViewById(R.id.bt_chat);

        link = Constants.OPEN_LINK + shortId;
        LogTrace.E("room link : " + link);*/

        if("".equals(profile) || profile == null)
            profileUrl = "";
        else {
            if(profile.contains(Constants.CHATDAWN_PROC)) {
                profileUrl = profile;
            } else {
                profileUrl = Constants.CHATDAWN_PROC + "?type=7&serverfile=" + profile;
            }
        }

        Glide.with(this).load(profileUrl).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(iv_roomprofile);

        if(ownerId.equals(MessengerInfo.getUserId(this))) {
            iv_setting.setVisibility(View.VISIBLE);
        } else {
            iv_setting.setVisibility(View.GONE);
        }

        tv_title.setText(name);
        tv_subtitle.setText(comment);

        if("1".equals(isEnter)) {
            bt_chat.setText(getString(R.string.entered_room));
        }

        setEvent();
    }

    private void setEvent() {
        LogTrace.E("setEvent");

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogTrace.E("close click");
                onBackPressed();
            }
        });

        iv_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goSetting();
            }
        });

        iv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliveryLink();
            }
        });

        bt_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLink();
            }
        });

        bt_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterOpen();
            }
        });
    }

    private void goSetting() {
        /*mIntent = new Intent(this, Activity_CreateOpen.class);
        mIntent.putExtra("roomId", id);
        mIntent.putExtra("roomName", name);
        mIntent.putExtra("roomComment", comment);
        mIntent.putExtra("roomLink", link);
        mIntent.putExtra("roomProfile", profileUrl);
        mIntent.putExtra("maxUser", maxUser);
        mIntent.putExtra("enable", enableWrite);
        mIntent.putExtra("isStaff", isStaff);
        mIntent.putExtra("ownerId", ownerId);
        mIntent.putExtra("roomType", roomType);

        startActivity(mIntent);
        finish();*/
    }

    private void deliveryLink() {
        //Dialog_Common.getShareDialog(this, link).show();

        /*Bundle bundle = new Bundle();
        bundle.putString("messageType", "0");
        bundle.putString("message", link);
        bundle.putString("attachment", "");
        Intent intent = new Intent(this, Activity_Delivery.class);
        intent.putExtras(bundle);
        startActivity(intent);*/
    }

    private void shareLink() {
        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addCategory(Intent.CATEGORY_DEFAULT);
        msg.putExtra(Intent.EXTRA_TEXT, link);
        msg.putExtra(Intent.EXTRA_TITLE, "Mango BAnana");
        msg.setType("text/plain");
        startActivity(Intent.createChooser(msg, "공유"));
    }

    private void enterOpen() {
        /*if(!isIng) {
            if ("1".equals(isEnter)) {
                if (!"".equals(chatName) && chatName != null) {
                    goChat();
                } else {
                    Dialog_ChatName dialog = new Dialog_ChatName(this);
                    dialog.show();
                    dialog.setData(name);
                }
            } else {
                Dialog_ChatName dialog = new Dialog_ChatName(this);
                dialog.show();
                dialog.setData(name);
            }
        }*/
    }

    public void sendEnter() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("userId", MessengerInfo.getUserId(this));
            obj.put("roomId", id);
            obj.put("roomShortId", shortId);
            obj.put("roomUserName", chatName);

            PacketBody body = new PacketBody();
            body.put("params", obj);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM, body);
            isIng = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendChatName() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("userId", MessengerInfo.getUserId(this));
            obj.put("roomId", id);
            obj.put("roomUserName", chatName);

            PacketBody body = new PacketBody();
            body.put("params", obj);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_CHANGE_USERNAME, body);
            isIng = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;

        if("1".equals(isEnter)) {
            sendChatName();
        } else {
            sendEnter();
        }

    }

    private void goChat() {
        LogTrace.E("goChat : " + room.getRoom_type());

        Statics.ROOMINFO = room;

        /*if(WAVEMainActivity.staticMainActivity == null) {
            LogTrace.E("main is null");
            mIntent = new Intent(this, WAVEMainActivity.class);
            mIntent.putExtra("isOpen", true);
            startActivity(mIntent);
            finish();
        } else {
            LogTrace.E("go chat activity");
            mIntent = new Intent(this, Activity_ChatRoom.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(mIntent);
            finish();
        }*/

        /*if (WAVEMainActivity.staticMainActivity != null)
            WAVEMainActivity.staticMainActivity.mFragmentChatRmList.setUpdate();*/

    }

    private void getRoomInfo() {
        try {
            JSONObject obj = new JSONObject();
            obj.put("userId", MessengerInfo.getUserId(this));
            obj.put("roomShortId", shortId);

            LogTrace.E("open get room : " + MessengerInfo.getUserId(this) + " / " + shortId);

            PacketBody body = new PacketBody();
            body.put("params", obj);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_OPEN_ROOM_INFO, body);
        } catch (Exception e) {
            LogTrace.E("open get room exception");
            e.printStackTrace();
        }
    }

    private void doLogin() {
        MOAClient.getInstance().addCommonListener(this);
        //LoadingManager.with(this).showLoadingDialog();

        MessengerInfo.setMode(this, "0");
        MessengerInfo.setData(this, "");

        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS_INFO);
        MOAClient.getInstance().setProperties("userId", MessengerInfo.getUserId(this));
        MOAClient.getInstance().setProperties("userPassword", MessengerInfo.getUserPw(this));
        MOAClient.getInstance().setProperties("tel", MessengerInfo.getUserPhoneNumber(this));
        MOAClient.getInstance().setProperties("userName", MessengerInfo.getUserName(this));
        MOAClient.getInstance().setProperties("staff", MessengerInfo.getStaff(this));
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken",MessengerInfo.getUserTocken(this));
        MOAClient.getInstance().login();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MOAClient.getInstance().removeEventListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        /*if(WAVEMainActivity.staticMainActivity != null)
            WAVEMainActivity.staticMainActivity.mFragmentChatRmList.setUpdate();*/
    }

    @Override
    public void onMOALogin(MOAData moaData) {
        //LoadingManager.with(this).hideLoadingDialog();

        MOAClient.getInstance().removeCommonListener(this);
        BUSMOAData.getInstantce().setmMOAData(moaData);

        getRoomInfo();
    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {

    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {

    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {

    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {

    }

    @Override
    public void OnEvent(MOAData moaData) {
        String result = moaData.body.getString("result");

        if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM) {
            LogTrace.E("open profile result : " + result);
            if (Constants.MOA_RESULT_OK.equals(result)) {
                LogTrace.E("result success");
                LocalDB.getChatListDbHelper(this).updateEnter(id, "1");
                LocalDB.getChatListDbHelper(this).updateRoomSubject(id, chatName);
                room.setEntered("1");
                room.setRoomSubject(chatName);

                isEnter = "1";
                isIng = false;

                enterOpen();

            } else if (Constants.MOA_RESULT_ERROR.equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Dialog_Kick dialog_kick = new Dialog_Kick(Activity_OpenProfile.this);
                        dialog_kick.show();
                        dialog_kick.setData(name, getString(R.string.error));
                    }
                });

            } else if ("user_full".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Dialog_Kick dialog_kick = new Dialog_Kick(Activity_OpenProfile.this);
                        dialog_kick.show();
                        dialog_kick.setData(name, getString(R.string.full_room));
                    }
                });

            } else if ("block_user".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Dialog_Kick dialog_kick = new Dialog_Kick(Activity_OpenProfile.this);
                        dialog_kick.show();
                        dialog_kick.setData(name, getString(R.string.kicked_user));
                    }
                });

            } else if ("room_not_exist".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Dialog_Kick dialog_kick = new Dialog_Kick(Activity_OpenProfile.this);
                        dialog_kick.show();
                        dialog_kick.setData(name, getString(R.string.room_exist));
                    }
                });

            }

        } else if(moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_CHANGE_USERNAME) {
            if (Constants.MOA_RESULT_OK.equals(result)) {
                LogTrace.E("result success");
                LocalDB.getChatListDbHelper(this).updateEnter(id, "1");
                LocalDB.getChatListDbHelper(this).updateRoomSubject(id, chatName);
                room.setEntered("1");
                room.setRoomSubject(chatName);

                isIng = false;

                enterOpen();

            }
        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_OPEN_ROOM_INFO) {
            LogTrace.E("get room info : " + result);

            if (Constants.MOA_RESULT_OK.equals(result)) {
                LogTrace.E("open info : " + moaData.body.getString("params"));
                LBJSONObject jObject = moaData.body.getJson("params");

                profile = jObject.getString("roomProfileImage");
                name = jObject.getString("roomName");
                comment = jObject.getString("roomComment");
                id = jObject.getString("roomId");
                shortId = jObject.getString("roomShortId");
                maxUser = jObject.getString("maxUser");
                isStaff = jObject.getString("staff");
                ownerId = jObject.getString("ownerId");
                isEnter = jObject.getString("entered");
                chatName = jObject.getString("roomSubject");
                enableWrite = jObject.getString("enableWriteMsg");
                roomType = jObject.getString("roomType");

                room = new VoChatList();

                room.setRoomId(id);
                room.setRoom_name(name);
                room.setRoomShortId(shortId);
                room.setRoomComment(comment);
                room.setMaxUser(maxUser);
                room.setStaff(isStaff);
                room.setOwnerId(ownerId);
                room.setRoom_profile_image(profile);
                room.setEnableWriteMsg(enableWrite);
                room.setRoom_type(roomType);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        init();
                    }
                });
            }
        }
    }
}
