package com.wave.messenger.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.ApplicantRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 가입신청자 화면
 */
public class Activity_MemberApplicant extends BaseActivity {

    String TAG=this.getClass().getName();


    ApplicantRecyclerAdapter mApplicantRecyclerAdapter;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_member_applicant);

        setRecyclerView();
        initView();
        setOnClickEvent();


    }


    /**
     * 7.9 모임 멤버 리스트
     * "0":가입신청자 리스트
     */
    private void MoimProfileRequest() {
        MOALog.w(TAG+" requestMoimProfile");
        Moa.moimMemberlistRequest(Activity_MemberApplicant.this,"0", "1", "0",mMainActivityHandler);
    }

    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_MEMBERLIST :
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());



                            try {   //gson파싱
                                VoMoimMemberList voMoimMemberList;
                                Gson gson = new Gson();
                                //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                                voMoimMemberList = gson.fromJson( data.body.toString() , VoMoimMemberList.class);

                                //가입제한 값저장
                                //SharedObject.setProperty_string(Activity_MoimTimeLineList.this,Constants.MOIM_ENTER_TYPE,voMoimProfile.getParams().getEntTy());
                                setDate(voMoimMemberList.getParams());


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 어댑터에 붙인다
     * @param voBoardList
     */
    private void setDate(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> voBoardList) {
        mApplicantRecyclerAdapter.setItem(voBoardList);
        mApplicantRecyclerAdapter.notifyDataSetChanged();

    }

    private void setRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        /*VoMoimMember item;
        ArrayList<VoMoimMember> arVoMoimList = new ArrayList<VoMoimMember>();
        for (int i = 0; i < 5; i++) {
            item = new VoMoimMember("id" + i,"https://www.android.com/static/2016/img/share/andy-lg.png", "똘이"+i , "010-1234-567" + i, "2017.02.0"+i);
            arVoMoimList.add(item);
        }*/

        mApplicantRecyclerAdapter = new ApplicantRecyclerAdapter(this);
        recyclerView.setAdapter(mApplicantRecyclerAdapter);


        MoimProfileRequest();
    }


    private void setOnClickEvent() {

    }

    private void initView() {
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.member_applicant));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);



        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).measure(0, 0);
        int nBackWidth= findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).getMeasuredWidth();//뒤로가기버튼사이즈

        findViewById(R.id.linearLayout1).setPadding(nBackWidth,0, nBackWidth,0); //버튼 가로사이즈+우측패딩 값으로 타이틀의 우측 패딩값으로 세팅.





        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        final SpannableStringBuilder sb = new SpannableStringBuilder(getResources().getString(R.string.applicant_info));
        //final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(158, 158, 158));
        final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.parseColor("#ce0000"));

        //final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        // Set the text color for first 4 characters
        sb.setSpan(fcs, 29, 34, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        // make them also bold
        //sb.setSpan(bss, 0, 4, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        ((TextView)findViewById(R.id.tv_applicant_info)).setText(sb);

    }

    /**
     * 초대 리스트 새로고침
     */
    public void itemRefresh(){
        MoimProfileRequest();
    }
}
