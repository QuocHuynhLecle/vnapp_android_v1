package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.ProfessionalListViewAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.dialog.Dialog_Moim_Join;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMyprofessional;

import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_ProfessionalList extends BaseActivity implements MOAEventListener, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {

    static public Activity_ProfessionalList activity;

    String TAG = this.getClass().getName();
    ProfessionalListViewAdapter professionalListViewAdapter;
    private ExpandableListView expandableListView;
    private ArrayList<VoMyprofessional> childList = new ArrayList<>();

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_professionallist);

        activity = this;

        initView();
        setOnClickEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
//        ProListRequest();
    }

    @Override
    public void OnEvent(MOAData moaData) {

        LogTrace.E("MOADATA OnEvent");
        mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    private void initView() {
        ((TextView) findViewById(R.id.textViewTitle)).setText(MessengerInfo.getUserName(this) + Constants.PROFESSIONAL_TITLE);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        ProListRequest();
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnChildClickListener(this);
    }

    /**
     * 리스트 설정
     */
    public void setListView(ArrayList<String> groupList, ArrayList<VoMyprofessional> items) {
        childList = items;
        professionalListViewAdapter = new ProfessionalListViewAdapter(this, groupList, items);
        expandableListView.setAdapter(professionalListViewAdapter);
        for (int i = 0; i < professionalListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        professionalListViewAdapter.notifyDataSetChanged();
    }

    public void setProListData(MOAData data) {
        LBJSONArray groupArray = data.body.getArray("xprtGrpLst");
        LBJSONArray array = data.body.getArray("params");
        ArrayList<String> groupList = new ArrayList<>();
        ArrayList<String> groupIdList = new ArrayList<>();
        ArrayList<String> groupitems = new ArrayList<>();
        ArrayList<VoMyprofessional> voProList = new ArrayList<>();
        ArrayList<VoMyprofessional> items = new ArrayList<>();

        for (int i = 0; i < groupArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) groupArray.get(i);
            String item = VoMyprofessional.loadFromJsonGroupResponse(jo);
            groupList.add(item);
        }

        for (int i = 0; i < groupArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) groupArray.get(i);
            String item = VoMyprofessional.loadFromJsonGroupIdResponse(jo);
            groupIdList.add(item);
        }

        for (int i = 0; i < array.size(); i++) {
            LBJSONObject jo = (LBJSONObject) array.get(i);
            VoMyprofessional item = VoMyprofessional.loadFromJsonServerResponse(jo);
            voProList.add(item);
        }

        //내 전문가
        VoMyprofessional myData = new VoMyprofessional();
        ArrayList<VoMyprofessional> tempList = new ArrayList<>();

//        for (int i = 0; i < voProList.size(); i++) {
//            if (voProList.get(i).getmStt() == 1) {
//                tempList.add(voProList.get(i));
//            }
//        }
//        myData.setProList(tempList);
//        myData.setUserName(Constants.PROFESSIONAL_LIST_MYPROFESSIONAL);

        //추천 매니저
        VoMyprofessional mnData = new VoMyprofessional();
        tempList = new ArrayList<>();

        for (int i = 0; i < voProList.size(); i++) {
            if (voProList.get(i).getXprtTy() == 0 && voProList.get(i).getmStt() != 1) {
                tempList.add(voProList.get(i));
            }
        }
        mnData.setProList(tempList);
        mnData.setUserName(Constants.PROFESSIONAL_LIST_MANAGER);

        //추천 애널리스트
        VoMyprofessional anData = new VoMyprofessional();
        tempList = new ArrayList<>();

        for (int i = 0; i < voProList.size(); i++) {
            if (voProList.get(i).getXprtTy() == 1 && voProList.get(i).getmStt() != 1) {
                tempList.add(voProList.get(i));
            }
        }
        anData.setProList(tempList);
        anData.setUserName(Constants.PROFESSIONAL_LIST_ANALYST);

        //추천 전문가
        VoMyprofessional prData = new VoMyprofessional();
        tempList = new ArrayList<>();

        for (int i = 0; i < voProList.size(); i++) {
            if (voProList.get(i).getXprtTy() == 2 && voProList.get(i).getmStt() != 1) {
                tempList.add(voProList.get(i));
            }
        }
        prData.setProList(tempList);
        prData.setUserName(Constants.PROFESSIONAL_LIST_PROFESSIONAL);

        //추천 전문가 집단
        VoMyprofessional prgData = new VoMyprofessional();
        tempList = new ArrayList<>();
        prgData.setProList(tempList);
        prgData.setUserName(Constants.PROFESSIONAL_LIST_PROFESSIONAL_GROUP);

        //전문가 그룹
        ArrayList<VoMyprofessional> grpList = new ArrayList<>();
        if (groupList != null) {
            for (int i = 0; i < groupList.size(); i++) {
                VoMyprofessional grpData = new VoMyprofessional();
                tempList = new ArrayList<>();
                for (int j = 0; j < voProList.size(); j++) {
                    if (groupIdList.get(i).equals(voProList.get(j).getXprtGrpId())) {
//                        tempList.add(voProList.get(j));
                    }
                }
                grpData.setProList(tempList);
                grpData.setUserName(groupList.get(i));
                grpData.setXprtGrpId(groupIdList.get(i));
                grpList.add(grpData);
            }
            VoMyprofessional grpData = new VoMyprofessional();
            tempList = new ArrayList<>();
            for (int i = 0; i < voProList.size(); i++) {
                if (voProList.get(i).getXprtGrpId().equals("")) {
//                    tempList.add(voProList.get(i));
                }
            }
            grpData.setProList(tempList);
            grpData.setUserName(Constants.PROFESSIONAL_LIST_PROFESSIONAL_NORMAL);
            grpData.setXprtGrpId("");
            grpList.add(grpData);
        }

//        //추천 전문가 집단
//        VoMyprofessional grData = new VoMyprofessional();
//        tempList = new ArrayList<>();
//
//        for (int i = 0; i < 3; i++) {
//            if (voProList.get(i).getXprtTy() == 1 && voProList.get(i).getmStt() != 1) {
//                tempList.add(voProList.get(i));
//            }
//        }
//        grData.setProList(tempList);
//        grData.setUserName(Constants.PROFESSIONAL_LIST_PROFESSIONAL_GROUP);

//        items.add(myData);
//        items.add(prData);
//        items.add(anData);
        items.add(mnData);
        items.add(prgData);
//        items.add(grData);

        if (groupList != null) {
            items.addAll(grpList);
        }

//        groupitems.add(Constants.PROFESSIONAL_LIST_MYPROFESSIONAL);
//        groupitems.add(Constants.PROFESSIONAL_LIST_PROFESSIONAL);
//        groupitems.add(Constants.PROFESSIONAL_LIST_ANALYST);
        groupitems.add(Constants.PROFESSIONAL_LIST_MANAGER);
        groupitems.add(Constants.PROFESSIONAL_LIST_PROFESSIONAL_GROUP);

        if (groupList != null) {
            for (int i = 0; i < groupList.size(); i++) {
                groupitems.add(groupList.get(i));
            }
            groupitems.add(Constants.PROFESSIONAL_LIST_PROFESSIONAL_NORMAL);
        }

        setListView(groupitems, items);
    }

    /**
     * 전문가 목록 요청
     */
    public void ProListRequest() {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_EXPERT_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void proChatRequest(String roomid) {
        Moa.chatRoomInfoRequest(this, roomid, mMainActivityHandler);
    }

    private VoChatList getRoomInfo(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("users");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }
        return info;
    }

    private void intentChat() {
        Intent intent = new Intent(this, Activity_ChatRoom.class);
        startActivity(intent);
    }

    //결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_EXPERT_LIST:    //전문가 리스트
                        MOALog.i("PTC_IMS_MOIM_EXPERT_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            LogTrace.E("params: " + data.body.get("result").toString());
                            setProListData(data);
                        }
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_INFO:
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            Statics.ROOMINFO = getRoomInfo(data);
                            intentChat();
                        }
                        break;
                    case PacketTypes.PTC_IMS_MOIM_APPLY:    //모임 가입
                        if (data.body.get("result").equals(Constants.SUCCESS)) {

                            String reason = (String) data.body.get("reason");
                            RefreshAdapter();
                            Fragment_Main.getInstance().FriendListRequest();
                            if (reason.equals("apply")) {
                                Toast.makeText(Activity_ProfessionalList.this, "가입신청을 완료하였습니다.", Toast.LENGTH_SHORT).show();
                                RefreshAdapter();
                                Fragment_Main.getInstance().FriendListRequest();
                            } else {
                                //완료 표시
                            }

                        } else {

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("already_applied")) {
                                Toast.makeText(Activity_ProfessionalList.this, "이미 가입신청함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_exist")) {
                                Toast.makeText(Activity_ProfessionalList.this, "이미 가입함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_blocked")) {
                                Toast.makeText(Activity_ProfessionalList.this, "차단됨", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("wrong_moim_password")) {
                                Toast.makeText(Activity_ProfessionalList.this, "비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        VoMyprofessional chekItem = childList.get(groupPosition);
        if (!chekItem.getUserName().equals(Constants.PROFESSIONAL_LIST_MYPROFESSIONAL) &&
                !chekItem.getUserName().equals(Constants.PROFESSIONAL_LIST_MANAGER) &&
                !chekItem.getUserName().equals(Constants.PROFESSIONAL_LIST_ANALYST) &&
                !chekItem.getUserName().equals(Constants.PROFESSIONAL_LIST_PROFESSIONAL) &&
                !chekItem.getUserName().equals(Constants.PROFESSIONAL_LIST_PROFESSIONAL_GROUP)) {
            if (chekItem.getUserName() != "") {
                Intent mIntent = new Intent(getActivity(), Activity_ProfessionalListGroup.class);
                mIntent.putExtra("groupId", chekItem.getXprtGrpId());
                mIntent.putExtra("groupNm", chekItem.getUserName());
                startActivity(mIntent);
            } else
                Toast.makeText(this, chekItem.getUserName() + " 프렌드가 없습니다", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        final VoMyprofessional item = childList.get(groupPosition).getProList().get(childPosition);
//        switch (item.getmStt()) {
//            case 0:
//                Toast.makeText(Activity_ProfessionalList.this, "가입 후 대화방 입장이 가능합니다", Toast.LENGTH_SHORT).show();
//                break;
//            case 1:
//                proChatRequest(item.getRmId());
//                break;
//            case 2:
//                Toast.makeText(Activity_ProfessionalList.this, "가입이 차단 되었습니다", Toast.LENGTH_SHORT).show();
//                break;
//            case 3:
//                Toast.makeText(Activity_ProfessionalList.this, "가입대기 중 입니다", Toast.LENGTH_SHORT).show();
//                break;
//        }
        if (item.getmStt() == 1) {
            proChatRequest(item.getRmId());
        } else {
            final Dialog_Moim_Join dialog_moim_join = new Dialog_Moim_Join(this, item.getMmNm(), item.getIntr(), item.getImgTmb());
            dialog_moim_join.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = v.getId();
                    if (i == R.id.bt_cancel) {
                        dialog_moim_join.dismiss();
                    } else if (i == R.id.bt_ok) {
                        showDialogJoin(item);
                        dialog_moim_join.dismiss();
                    } else if(i == R.id.ll_close){
                        dialog_moim_join.dismiss();
                    }
                }
            });
            dialog_moim_join.show();
        }
        return false;
    }

    public void RefreshAdapter() {
        ProListRequest();
    }

    /**
     * 모임 가입하기 다이얼로그
     * 현재 질문가입형
     * <p>
     * 0:승인, 1:자동가입(기본), 2:비밀번호
     */
    private void showDialogJoin(final VoMyprofessional param) {
        switch (param.getEntTy()) {
            case 1:  //팝업 필요없이 바로 가입
                Moa.moimApplyProfessional(this, "", "", mMainActivityHandler, param.getMmId());
                break;
            case 0:  //승인 질문입력 팝업
            case 2: //비밀번호 팝업
                //TODO
                final Dialog_MoimJoinType dialog_MakeMoim = new Dialog_MoimJoinType(this, param); //param.getQstn() getEntTy
                dialog_MakeMoim.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int id = view.getId();

                        if (id == R.id.tv_cancle) {
                            dialog_MakeMoim.dismiss();
                        } else if (id == R.id.tv_confirm) {
                            dialog_MakeMoim.getJoinTxt();   //비밀번호

                            String answ = null;   //모임 가입질문
                            String mmPw = null;  //모임 가입비밀번호

                            if (param.getEntTy() == 0) { //리더승인이면
                                answ = dialog_MakeMoim.getJoinTxt();   //가입질문

                            } else if (param.getEntTy() == 2) {
                                mmPw = dialog_MakeMoim.getJoinTxt();   //비밀번호
                            }

                            Util.hideKeyboard(Activity_ProfessionalList.this, ((EditText) dialog_MakeMoim.findViewById(R.id.edt_input)));
                            dialog_MakeMoim.dismiss();

                            Moa.moimApplyProfessional(Activity_ProfessionalList.this, answ, mmPw, mMainActivityHandler, param.getMmId());

                        }

                    }
                });
                dialog_MakeMoim.show();
                break;
        }
    }
}
