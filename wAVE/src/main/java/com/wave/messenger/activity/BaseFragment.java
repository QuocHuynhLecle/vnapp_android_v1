package com.wave.messenger.activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.wave.massenger.piggy.R;

import java.util.List;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public abstract class BaseFragment extends Fragment {


    abstract public void OnReceiveMsgFromActivity(int msgId, Object param1, Object param2);

    public void addFragment(FragmentActivity activity, int layoutId, BaseFragment baseFragment, String name) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(layoutId, baseFragment, name);
        ft.addToBackStack(name);
        ft.commit();
    }

    public void addFragmentReplace(FragmentActivity activity, int layoutId, BaseFragment baseFragment, String name) {
        clearBackStack(activity);
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(layoutId, baseFragment, name);
        ft.addToBackStack(name);
        ft.commit();
    }

    public void removeFragment(FragmentActivity activity, BaseFragment baseFragment) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.remove(baseFragment);
        ft.commit();

    }

    public int getBackStackCount(FragmentActivity activity){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        return count;
    }

    public void clearBackStack(FragmentActivity activity){
        try {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			/*
			for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
				fragmentManager.popBackStackImmediate();
			}
			*/
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }


    public BaseFragment findFragment(FragmentActivity activity, String fragmentTag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        android.support.v4.app.Fragment frament = fragmentManager.findFragmentByTag(fragmentTag);

        if(frament != null) {
            return (BaseFragment) frament;
        } else {
            return null;
        }
    }

    public Fragment getActiveFragment(FragmentActivity activity) {


        if (activity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        FragmentManager.BackStackEntry bse = activity.getSupportFragmentManager().getBackStackEntryAt(activity.getSupportFragmentManager().getBackStackEntryCount() - 1);

        String tag = bse.getName();
        return activity.getSupportFragmentManager().findFragmentByTag(tag);
    }

    protected FragmentActivity getBaseActivity(){
        FragmentActivity fragment = getActivity();
        return (FragmentActivity) fragment;
    }

    public AlertDialog.Builder getAlert() {
        int version = Integer.parseInt((Build.VERSION.RELEASE).substring(0, 1));
        AlertDialog.Builder alert = null;
        if(version >= 3){
            alert = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        alert.setTitle(R.string.app_name);
        return alert;
    }

    static BaseFragment topFragment = null;
    public static  BaseFragment getTopFragment() {
        return topFragment;
    }

    public static  void setTopFragment(BaseFragment atopFragment) {
        topFragment = atopFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            setTopFragment(this);
            if(getActivity() != null) {
//                PushProc.closeTaskBar(getActivity());
            } else {
//                if(Fragment_Main.getInstance() != null && Fragment_Main.getInstance().getActivity() != null) {
//                    PushProc.closeTaskBar(MlFragmentMain.getInstance().getActivity());
//                }
            }
        } catch (Exception e) {

        }
    }

    public void setParentLayout() {

    }

    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }
}
