package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.SharedUserInfo;
import com.wave.messenger.utility.Moa;

import java.lang.reflect.Field;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by Joo on 2017. 5. 31.
 */

public class Activity_ChangePassword extends AppCompatActivity implements MOAEventListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_change_password);
        MOAClient.getInstance().addEventListener(Activity_ChangePassword.this);

        final EditText edit_password = findViewById(R.id.et_password);
        setCursor(edit_password);
        final EditText edit_passwordChk = findViewById(R.id.et_passwordCheck);
        setCursor(edit_passwordChk);

        findViewById(R.id.tv_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_password.getText().length() > 7) {
                    if (edit_password.getText().toString().equals(SharedUserInfo.getInstance(Activity_ChangePassword.this).getUserPassWd())){
                        Toast.makeText(Activity_ChangePassword.this, "현재 비밀번호와 동일..", Toast.LENGTH_SHORT).show();
                    }

                    if (edit_password.getText().toString().equals(edit_passwordChk.getText().toString())){
                        changePassword(edit_password.getText().toString());  // 비밀번호 변경 요청
                    } else {
                        Toast.makeText(Activity_ChangePassword.this, "입력한 비밀번호와 재입력한 비밀번호가\n일치하지 않습니다.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Activity_ChangePassword.this, "비밀번호는 8자리 이상 입력해 주세요.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setCursor(EditText editText) {
        // 커서 포커스
        try {
            Field cursor = TextView.class.getDeclaredField("mCursorDrawableRes");
            cursor.setAccessible(true);
            cursor.set(editText, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changePassword(String password) {
        try {
            PacketBody body = new PacketBody();
            body.putParams("userId", SharedUserInfo.getInstance(this).getUserId());
            body.putParams("password", password);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, 0x00030019, body);    // 비밀번호 변경
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnEvent(MOAData moaData) {
        mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    private Handler mActivityHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                String strResult;
                switch (data.ptc) {

                    // TODO : 결과 처리 edittext 커서 처리
                    case 0x00030019:    // 비밀번호 변경
                        strResult = (String) data.body.get("result");

                        if (strResult.equals(Constants.SUCCESS)) {

                            String password = data.body.getJson("params").getString("password");
                            SharedUserInfo.getInstance(Activity_ChangePassword.this).setUserPassWd(password);

                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            if (getCurrentFocus() != null)
                                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                            Toast.makeText(Activity_ChangePassword.this, "비밀번호가 변경되었습니다.", Toast.LENGTH_SHORT).show();
                            finish();
                        } else { // result = error
//                            String reason = (String) data.body.get("reason");
                            Toast.makeText(Activity_ChangePassword.this, "비밀번호 변경에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });
}
