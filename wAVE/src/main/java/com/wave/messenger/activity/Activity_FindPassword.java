
package com.wave.messenger.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.DeviceInfo;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by Joo on 2018. 5. 29.
 */

public class Activity_FindPassword extends AppCompatActivity implements MOAEventListener {

    private static final String TAG = Activity_FindPassword.class.getSimpleName();
    private EditText edit_id;
    private EditText edit_name;
    private EditText edit_tel;
    private TextView tv_result;
    private Button btn_find_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.titleFindPassword));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MOAClient.getInstance().addEventListener(this);
        initView();
        setOnClickEvent();
    }

    private void setOnClickEvent() {
        btn_find_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempSocketOpen();
            }
        });
    }

    private void tempSocketOpen() { //Socket Open
        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);
        MOAClient.getInstance().setProperties("userId", "test01");
        MOAClient.getInstance().setProperties("userPassword", "12345");
        MOAClient.getInstance().setProperties("tel", "");
        MOAClient.getInstance().setProperties("userName", "");
        MOAClient.getInstance().setProperties("candleUserType", "");
        MOAClient.getInstance().setProperties("riskType", "");
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken", MessengerInfo.getUserTocken(getBaseContext()));
        MOAClient.getInstance().setProperties("branchName", MessengerInfo.getBranchName());
        MOAClient.getInstance().setProperties("login_ver", "1");
        MOAClient.getInstance().setProperties("agree", "1"); // 약관 동의 값을 최초 1회만 보내면됩니다
        MOAClient.getInstance().login();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void initView() {
        edit_id = findViewById(R.id.et_id);
        edit_name = findViewById(R.id.et_name);
        edit_tel = findViewById(R.id.et_tel);
        btn_find_password = findViewById(R.id.bt_findPwd);
        tv_result = findViewById(R.id.tv_result);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MOAClient.getInstance().removeEventListener(this);
    }

    private void requestFindPassword(){
        String userId = ""+ edit_id.getText().toString();
        String userName = "" + edit_name.getText().toString();
        String tel = ""+ edit_tel.getText().toString();

        if(TextUtils.isEmpty(userId)){
            Toast.makeText(this, "아이디를 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(userName)){
            Toast.makeText(this, "이름을 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(tel)){
            Toast.makeText(this, "전화번호를 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            PacketBody body = new PacketBody();
            body.putParams("userId", userId);
            body.putParams("userName", userName);
            body.putParams("tel", tel);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, 0x00030021, body); // 비밀번호 찾기
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void OnEvent(MOAData moaData) {
        mMainActvtHandler.sendMessage(mMainActvtHandler.obtainMessage(moaData.ptc, moaData));
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message message) {

            try{
                final MOAData data = (MOAData) message.obj;
                Log.e(TAG, "data Body : "+ data.body.toString());
                String jsonStr = data.body.toString();
                switch(data.ptc){

                    case PacketTypes.PTC_IMS_LOGIN_LOGIN:
                        requestFindPassword();
                        break;

                    case 0x00030021:
                        String result = (String) data.body.get("result");

                        JSONObject json = new JSONObject(jsonStr);
                        String params = json.getString("params"); // params array
                        Log.d(TAG, "수신 결과 : " + result +", 비밀번호 : "+ params);

                        switch(result){
                            case "success":
                                JSONObject jsonPassword = new JSONObject(params);
                                String userPassword = jsonPassword.getString("password");
                                Log.d(TAG, "유저 패스워드 : " + userPassword);

                                tv_result.setVisibility(View.VISIBLE);
                                tv_result.setText("패스워드는 "+ userPassword + "입니다.");

                                break;

                            case "error":
                                String reason = json.getString("reason");
                                Log.d(TAG , "원인 : "+ reason);
                                switch(reason){
                                    case "userid_empty":
                                        Toast.makeText(Activity_FindPassword.this,"아이디를 확인해주세요.", Toast.LENGTH_SHORT).show();
                                        break;

                                    case "username_empty":
                                        Toast.makeText(Activity_FindPassword.this,"이름을 확인해주세요.", Toast.LENGTH_SHORT).show();
                                        break;

                                    case "tel_empty":
                                        Toast.makeText(Activity_FindPassword.this,"핸드폰 번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                                        break;

                                    case "user_not_found":
                                        Toast.makeText(Activity_FindPassword.this,"사용자를 찾을 수 없습니다.", Toast.LENGTH_SHORT).show();
                                        break;

                                    default:
                                        Toast.makeText(Activity_FindPassword.this, reason, Toast.LENGTH_SHORT).show();
                                        break;

                                }
                                break;

                            default:
                                break;

                        }
                        MOAClient.getInstance().logout(); // 로그아웃 호출하여 소켓 연결 해제
                        Log.d(TAG, "소켓 연결 해제");
                        break;
                }

            }catch(Exception e){
                e.printStackTrace();
            }

            return false;
        }
    });
}
