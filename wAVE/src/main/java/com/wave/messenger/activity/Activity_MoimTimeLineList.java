package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.BoardListRecyclerAdapter;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.CompleteDialog;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.util.HideShowScrollListener;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoBoardList;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMoimProfile;
import com.wave.messenger.vo.VoMsgItem;
import com.kevin.wraprecyclerview.WrapAdapter;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by yunsu on 2017. 3. 10..
 * 상단 모임정보가 있는
 * 모임 타임라인 리스트
 */
public class Activity_MoimTimeLineList extends BaseActivity {

    public static Activity_MoimTimeLineList activity = null;

    String TAG = this.getClass().getName();

    private WrapAdapter<BoardListRecyclerAdapter> mWrapAdapter;
    private BoardListRecyclerAdapter mMoimListRecyclerAdapter;  //하단 타임라인 리스트. 메인타임라인과 같이 사용.
    private EndlessRecyclerViewScrollListener scrollListener;   //타임라인 마지막 스크롤일경우 자동로딩

    SwipeRefreshLayout mSwipeRefreshLayout;
    //LinearLayout llTimelineNull;  //타임라인 리스트없을때 화면

    boolean mDeleteList = false;  //true: 기존 리스트 삭제후 다시 불러옴 false: 기존 리스트에 다음페이지 추가
    public boolean bMoimProfile = false;   //true: 모임프로필만 호출  false: 모임프로필+리스트 호출 , 공지삭제이후 자동 리스트호출 막기.
    public String roomName;  //대화방명 변수
    private boolean no_moim = true;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        setContentView(R.layout.activity_moim_timeline_list);
        activity = this;

        LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();

        initView();

        setRecyclerView();
        setAddHeaderView();

        MoimProfileRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 상단 타이틀
     */
    private void initView() {
        findViewById(R.id.rl_bg).setBackgroundColor(Util.getColor(this, R.color.white));
        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).setVisibility(View.GONE);
        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_search).setBackgroundResource(R.drawable.ic_timeline_search);
        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).setBackgroundResource(R.drawable.ic_timeline_write);

        findViewById(R.id.view_top_line).setVisibility(View.VISIBLE);
        ((ImageButton) findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.ic_arrow_left);
        findViewById(R.id.ll_top_Bar).setBackgroundColor(Util.getColor(this, R.color.moim_timeline_bar)); // R.color.moim_timeline_bar);
        ((TextView) findViewById(R.id.tv_topbar_title)).setTextColor(Util.getColor(this, R.color.bg_purple));
        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * RecyclerView 설정.
     */
    private void setRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        mMoimListRecyclerAdapter = new BoardListRecyclerAdapter(this, Constants.SET_ENTRY_TYPE.moim_list);

        mWrapAdapter = new WrapAdapter<>(mMoimListRecyclerAdapter);
        mWrapAdapter.adjustSpanSize(recyclerView);

        recyclerView.setAdapter(mWrapAdapter);

        //다음페이지 자동 로딩
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi();
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        setBottomMenu(recyclerView);

        mSwipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
    }


    /**
     * 하단 전체 메뉴 버튼
     *
     * @param recyclerView
     */
    private void setBottomMenu(RecyclerView recyclerView) {

        findViewById(R.id.ll_bottom_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        //하단 버튼
        recyclerView.addOnScrollListener(new HideShowScrollListener() {
            @Override
            public void onHide() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    findViewById(R.id.ll_bottom_menu).animate().translationY(findViewById(R.id.ll_bottom_menu).getHeight());
                } else {
                    findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);
                }
            }

            @Override
            public void onShow() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    findViewById(R.id.ll_bottom_menu).animate().translationY(0);
                } else {
                    findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);
                }
            }
        });

    }

    /**
     * 페이지 로딩
     *
     * @param
     */
    public void loadNextDataFromApi() {

        LogTrace.E("loadNextDataFromApi offset: " + SharedObject.getProperty_string(this, Constants.MOIM_ID, null) + " getMsgId: " + mMoimListRecyclerAdapter.getMsgId() + " getItemCount: " + mMoimListRecyclerAdapter.getItemCount());
        if (mMoimListRecyclerAdapter.getItemCount() != 0 && bMoimProfile == false) {
            requestBoardList(SharedObject.getProperty_string(this, Constants.MOIM_ID, null), mMoimListRecyclerAdapter.getMsgId());
        }

        bMoimProfile = false;
    }

    /**
     * 새로고침
     */
    public void refreshItems() {
        MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");
        MoimProfileRequest();
        mDeleteList = true;
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
        LoadingManager.with(Activity_MoimTimeLineList.this).hideLoadingDialog();
        bMoimProfile=true;
    }

    /**
     * 모임프로필을 호출한다.
     */
    public void MoimProfileRequest() {
        Moa.moimProfileRequest(Activity_MoimTimeLineList.this, mMainActivityHandler);
    }

    /**
     * 하단 모임 글 리스트를 호출한다
     */
    private void requestBoardList(String mmid, String msgId) {
        //LogTrace.E("requestBoardList mmid: " + mmid + " msgId: " + msgId);
        Moa.moimBoardListRequest(Activity_MoimTimeLineList.this, MessengerInfo.getUserId(this), "0", mmid, msgId, "", mMainActivityHandler);
    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                //MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                //Log.e(TAG, "data.ptc  " + data.ptc);


                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_PROFILE:    //모임프로필
                        strResult = (String) data.body.get("result");
                        LogTrace.E( "PTC_IMS_MOIM_PROFILE strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            LogTrace.E( "params: " + data.body.get("params").toString());
                            LogTrace.E("notiLst: " + data.body.get("notiLst").toString());


                            try {   //gson파싱
                                VoMoimProfile voMoimProfile;
                                Gson gson = new Gson();
                                voMoimProfile = gson.fromJson(data.body.toString(), VoMoimProfile.class);
                                SharedObject.setProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_CHATROOMID, voMoimProfile.getParams().getRmId());

                                if (voMoimProfile.getParams().getmStt().equals("2")) {    //가입차단이면
                                    Toast.makeText(Activity_MoimTimeLineList.this, getResources().getString(R.string.block_info), Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {

                                    //가입제한 값저장
                                    SharedObject.setProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_ENTER_TYPE, voMoimProfile.getParams().getEntTy());

                                    //설정값을 저장한다
                                    saveConfig(voMoimProfile);

                                    // open_type 공개 제한 비회원이면..
                                    // 0:비공개,  안보임 특별히 처리 X
                                    // 1:공개, 리스트 보여주기
                                    // 2:모임명만 공개(기본), 리스트 숨기고 가입 안내표시
                                    // my_status 내 상태 0:가입안됨, 1:가입됨
//                                    ChatListDbHelper db = LocalDB.getChatListDbHelper(Activity_MoimTimeLineList.this);
//                                    Statics.ROOMINFO = db.getChatData(voMoimProfile.getParams().getRmId());
                                    addHeaderView(voMoimProfile);


                                    /*if (voMoimProfile.getParams().getmStt().equals("1")) { //가입된유저

                                        if (bMoimProfile == false) {
                                            //글리스트
                                            requestBoardList("", "");
                                        } else {
                                            bMoimProfile = false;
                                        }
                                    }else{
                                        onItemsLoadComplete();

                                    }*/

                                    LogTrace.E( "@@voMoimProfile.getParams().getmStt(): " + voMoimProfile.getParams().getmStt() + " voMoimProfile.getParams().getEntTy(): " + voMoimProfile.getParams().getEntTy());

                                    if (voMoimProfile.getParams().getmStt().equals("1") || voMoimProfile.getParams().getEntTy().equals("1")) { //가입된유저 || 자동가입모임

                                        if (bMoimProfile == false) {
                                            //글리스트
                                            requestBoardList("", "");


                                        } else {
                                            bMoimProfile = false;

                                        }

                                    } else {
                                        onItemsLoadComplete();

                                        if (no_moim) {
                                            //멤버만 게시글을 볼수.. 화면 표시.
                                            addOnlyMemberView();
                                            no_moim = false;
                                        }

                                    }
//                                    else if(!voMoimProfile.getParams().getmStt().equals("1") && !voMoimProfile.getParams().getEntTy().equals("1")){
//                                        onItemsLoadComplete();
//
//                                        //멤버만 게시글을 볼수.. 화면 표시.
//                                        addOnlyMemberView();
//
//                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            String errorResult = (String) data.body.get("result");
                            if (errorResult.equals("moim_not_found")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "모임이 삭제되었습니다.", Toast.LENGTH_SHORT).show();
                            }
                            finish();

                        }
                        break;


                    case PacketTypes.PTC_IMS_MOIM_APPLY:    //모임 가입
                        strResult = (String) data.body.get("result");
                        LogTrace.E( "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {


                            String reason = (String) data.body.get("reason");
                            if (reason.equals("apply")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "가입신청을 완료하였습니다.", Toast.LENGTH_SHORT).show();
                            } else {
                                completeDialog();   //완료 표시
                            }


                            //enter_type 가입 제한(int) 0:자동가입(기본), 1:승인 2:패스워드
                            //entTy   가입 제한(int) 0:승인, 1:자동가입(기본)
                            /*String enter_type = SharedObject.getProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_ENTER_TYPE, null);

                            Log.e(TAG, "enter_type: " + enter_type);
                            if (enter_type.equals("0")) { //0. 가입완료버튼 숨기고 가입신청중표시

                                findViewById(R.id.ll_join).setVisibility(View.GONE);
                                findViewById(R.id.ll_join_applied).setVisibility(View.VISIBLE); //가입신청중


                            } else if (enter_type.equals("1")) {  //1. 가입완료버튼 숨기고 메뉴 보이기
                                findViewById(R.id.ll_join).setVisibility(View.GONE);
                                findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);


                            } else if (enter_type.equals("2")) {
                                findViewById(R.id.ll_join).setVisibility(View.GONE);
                                findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);
                            }*/
                            //새로고침
                            MoimProfileRequest();

                        } else {

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("already_applied")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "이미 가입신청함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_exist")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "이미 가입함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_blocked")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "차단됨", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("wrong_moim_password")) {
                                Toast.makeText(Activity_MoimTimeLineList.this, "비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        break;

                    //모임리스트
                    case PacketTypes.PTC_IMS_MOIM_BOARD_LIST:
                        LogTrace.E( "result PTC_IMS_MOIM_BOARD_LIST ");

                        VoBoardList voBoardList = null;
                        Gson gson = new Gson();
                        voBoardList = gson.fromJson(data.body.toString(), VoBoardList.class);

                        setDate(voBoardList);

                        onItemsLoadComplete();



                        //리스트없을때의 처리. 메인타임라인만 처리.
                        /*if(voBoardList==null||voBoardList.getParams()==null||voBoardList.getParams().size()==0){

                            llTimelistNull = (LinearLayout) LayoutInflater.from(Activity_MoimTimeLineList.this).inflate(R.layout.null_moim_timeline, null);
                            mWrapAdapter.addFooterView(llTimelistNull);
                            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,1.0f);

                            llTimelistNull.setLayoutParams(param);

                            llTimelistNull.findViewById(R.id.btn_null_timelinelist_write).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    intentMoimWrite();
                                }
                            });

                            Log.e(TAG,"voBoardList==null!!");
                        }else{
                            Log.e(TAG,"voBoardList Not null!!");
                            setDate(voBoardList);
                        }*/

                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM:
                        LoadingManager.with(getActivity()).showLoadingDialog();
                        String strResult2 = (String) data.body.get("result");
                        LBJSONObject jo = data.body.getJson("params");

                        if (strResult2.equals(Constants.SUCCESS)) {
//                            ChatListDbHelper db = LocalDB.getChatListDbHelper(Activity_MoimTimeLineList.this);
                            moimChatRequest(jo.getString("roomId"));
//                            Intent mIntent;
//                            Statics.ROOMINFO = getRoomInfo(data, db.getRoomName((String)data.body.get("roomId")));
//                            mIntent = new Intent(Activity_MoimTimeLineList.this, Activity_ChatRoom.class);
//                            startActivity(mIntent);

                        } else {

                        }
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    LinearLayout layout;    //headlayout
    LinearLayout footer_layout;
    LinearLayout llTimelistNull;

    /**
     * 헤더뷰 세팅
     */
    public void setAddHeaderView() {
        layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.header_moim_timeline, null);
        mWrapAdapter.addHeaderView(layout);
    }

    /**
     * 헤더뷰를 붙인다
     *
     * @param voMoimProfile
     */
    public void addHeaderView(VoMoimProfile voMoimProfile) {

        //Log.e(TAG, "addHeaderView");
        footer_layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.footer_moim_timeline, null);

        if (SharedObject.getProperty_string(Activity_MoimTimeLineList.this, Constants.mLv, "").equals("1")      //리더이면
                || SharedObject.getProperty_string(Activity_MoimTimeLineList.this, Constants.mLv, "").equals("2")) {   //공동리더도 보이도록 수정.
            layout.findViewById(R.id.ll_setting).setVisibility(View.VISIBLE);

            if (mWrapAdapter.getFootersCount() == 0) {
                mWrapAdapter.addFooterView(footer_layout);
            }

        } else {
            layout.findViewById(R.id.ll_setting).setVisibility(View.GONE);
            //footer_layout.findViewById(R.id.footer_view).setVisibility(View.GONE); //초대부분 안숨겨짐

        }
        setOnClickEvent(layout, voMoimProfile, footer_layout);
        setMoimProfileScreen(voMoimProfile, layout, footer_layout);

        // mWrapAdapter.getHeadersView().get(0).findViewById()
    }

    /**
     * 멤버만 게시글을 볼수있습니다
     */
    public void addOnlyMemberView() {
        footer_layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.item_moim_nojoin, null);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        footer_layout.setLayoutParams(param);
        mWrapAdapter.addFooterView(footer_layout);
    }


    /**
     * 모임타임라인 상단 모임 소개말을 수정한다.
     * 모임설정에서 호출.
     *
     * @param intr
     */
    public void setMoimIntrChange(String intr) {

        if (!TextUtils.isEmpty(intr.trim())) {
            layout.findViewById(R.id.tv_moim_intr).setVisibility(View.VISIBLE);
            ((TextView) layout.findViewById(R.id.tv_moim_intr)).setText(intr);
        } else {
            layout.findViewById(R.id.tv_moim_intr).setVisibility(View.GONE);
        }
        SharedObject.setProperty_string(this, Constants.intr, intr);
    }

    /**
     * 모임 리스트를 제외한 화면을 세팅한다
     *
     * @param voMoimProfile 모임 프로필
     * @param layout        헤더화면
     */
    private void setMoimProfileScreen(final VoMoimProfile voMoimProfile, LinearLayout layout, LinearLayout footer_view) {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        footer_view.setLayoutParams(param);
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(voMoimProfile.getParams().getMmNm());


        ((TextView) layout.findViewById(R.id.tv_moim_name)).setText(voMoimProfile.getParams().getMmNm()); //모임이름
        ((TextView) layout.findViewById(R.id.tv_user_cnt)).setText(voMoimProfile.getParams().getUsrCnt());  //멤버
        ((TextView) layout.findViewById(R.id.tv_notice_seq)).setText(voMoimProfile.getParams().getNotiCnt());  //공지카운트
        ((TextView) footer_view.findViewById(R.id.tv_contents)).setText("[" + voMoimProfile.getParams().getMmNm() + "]모임을 시작합니다.함께하고 싶은 멤버를 초대하세요.");
        ((TextView) footer_view.findViewById(R.id.tv_contents)).setTextSize(SharedObject.getProperty_int(Activity_MoimTimeLineList.this, Constants.FONTSIZE, 14));

        //리더 이름 추가. TODO 리더이름추가 현재 아이디값뿐
        ((TextView) layout.findViewById(R.id.tv_reguser)).setText(voMoimProfile.getParams().getRegUsrName()); //리더이름

        //모임소개글
        setMoimIntrChange(voMoimProfile.getParams().getIntr());


        if (voMoimProfile.getParams().getImgTy().equals("1")) { //모임이미지 있음

            //상단이미지
            Glide.with(this).load(Constants.MOIM_LIST_IMAGE_URL + voMoimProfile.getParams().getImgTmb().replace("thumb_", "")).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into((ImageView) layout.findViewById(R.id.flexible_image));

        } else {

            ((ImageView) layout.findViewById(R.id.flexible_image)).setImageResource(R.drawable.meet_timeline_default_img);

            //종류별 이미지아닌, 임의로  배경 이미지 넣어둠
            /*switch (voMoimProfile.getParams().getMmTg()) {
                case "1":   //1 : 주식
                    ((ImageView)findViewById(R.id.flexible_image)).setImageResource(R.drawable.ic_stock);
                    break;

                case "2":   //2 : 국내파생
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_derivation);
                    break;

                case "3":    //3 : 해외파생
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_global_derivation);
                    break;

                case "4":   //4 : 해외주식
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_global_stock);
                    break;

                case "5":   //5 : 금융상품
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_bank_product);
                    break;

                case "6":   //6 : 기타
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_etc);
                    break;
            }*/
        }


        int nNotiCount = voMoimProfile.getNotiLst().size();

        layout.findViewById(R.id.ll_notice).setVisibility(View.VISIBLE);
        layout.findViewById(R.id.ll_notice_content).setVisibility(View.VISIBLE);

        //공지가 없거나 ,  내상태가 가입안되고 비공개이거나
        if (nNotiCount == 0 || (voMoimProfile.getParams().getmStt().equals("0") && voMoimProfile.getParams().getOpnTy().equals("0"))) {
            layout.findViewById(R.id.ll_notice).setVisibility(View.GONE);
            layout.findViewById(R.id.ll_notice_content).setVisibility(View.GONE);

        } else if (nNotiCount == 1) {
            //Log.e(TAG, "setMoimProfileScreen " + nNotiCount + "  getTtl: " + voMoimProfile.getNotiLst().get(0).getTtl());

            layout.findViewById(R.id.ll_notice).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.tv_notice_content1).setVisibility(View.VISIBLE);

            String strTtl = "\u2022 ";

            if (voMoimProfile.getNotiLst().get(0).getNoti().equals("2")) { //중요공지 일경우.

                strTtl = strTtl + getResources().getString(R.string.noti_important) + " ";  //[중요]

                strTtl = checkTtlNull(voMoimProfile.getNotiLst().get(0), strTtl); //사진,이모티콘표시.

                SpannableString spannableString = new SpannableString(strTtl);
                spannableString.setSpan(new ForegroundColorSpan(Util.getColor(Activity_MoimTimeLineList.this, R.color.red)), 1, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView) layout.findViewById(R.id.tv_notice_content1)).setText(spannableString);

            } else {  //그냥 공지

                strTtl = checkTtlNull(voMoimProfile.getNotiLst().get(0), strTtl);
                ((TextView) layout.findViewById(R.id.tv_notice_content1)).setText(strTtl);
            }

            layout.findViewById(R.id.tv_notice_content2).setVisibility(View.GONE);
            layout.findViewById(R.id.tv_notice_content1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentTimeLineDetail(voMoimProfile.getNotiLst().get(0).getMsgId(), false);
                }
            });


        } else if (nNotiCount > 1) { //1개이상.
            layout.findViewById(R.id.ll_notice).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.tv_notice_content1).setVisibility(View.VISIBLE);


            String strTtl1 = "\u2022 ";
            String strTtl2 = "\u2022 ";

            if (voMoimProfile.getNotiLst().get(0).getNoti().equals("2")) { //중요공지 일경우.

                strTtl1 = strTtl1 + getResources().getString(R.string.noti_important) + " ";  //[중요]

                strTtl1 = checkTtlNull(voMoimProfile.getNotiLst().get(0), strTtl1); //사진,이모티콘표시.

                SpannableString spannableString = new SpannableString(strTtl1);
                spannableString.setSpan(new ForegroundColorSpan(Util.getColor(Activity_MoimTimeLineList.this, R.color.red)), 1, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView) layout.findViewById(R.id.tv_notice_content1)).setText(spannableString);

            } else {  //그냥 공지

                strTtl1 = checkTtlNull(voMoimProfile.getNotiLst().get(0), strTtl1);
                ((TextView) layout.findViewById(R.id.tv_notice_content1)).setText(strTtl1);
            }


            layout.findViewById(R.id.tv_notice_content2).setVisibility(View.VISIBLE);

            if (voMoimProfile.getNotiLst().get(1).getNoti().equals("2")) { //중요공지 일경우.

                strTtl2 = strTtl2 + getResources().getString(R.string.noti_important) + " ";  //[중요]
                strTtl2 = checkTtlNull(voMoimProfile.getNotiLst().get(1), strTtl2); //사진,이모티콘표시.

                SpannableString spannableString = new SpannableString(strTtl2);
                spannableString.setSpan(new ForegroundColorSpan(Util.getColor(Activity_MoimTimeLineList.this, R.color.red)), 1, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((TextView) layout.findViewById(R.id.tv_notice_content2)).setText(spannableString);

            } else {  //그냥 공지
                strTtl2 = checkTtlNull(voMoimProfile.getNotiLst().get(1), strTtl2);
                ((TextView) layout.findViewById(R.id.tv_notice_content2)).setText(strTtl2);
            }

            layout.findViewById(R.id.tv_notice_content1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentTimeLineDetail(voMoimProfile.getNotiLst().get(0).getMsgId(), false);
                }
            });
            layout.findViewById(R.id.tv_notice_content2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentTimeLineDetail(voMoimProfile.getNotiLst().get(1).getMsgId(), false);
                }
            });

        }


        //회원일때와 아닐때 화면 내상태값으로 구분 0:가입안됨, 1:가입됨, 2:차단, 3:가입대기
        if (voMoimProfile.getParams().getmStt().equals("0")) { //0 이면 가입안됨

            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);       //글쓰기버튼 숨김
            findViewById(R.id.ll_join).setVisibility(View.VISIBLE);      //밴드가입하기버튼 보임
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);  //하단메뉴 숨김
            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);      //초대버튼

            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김
            layout.findViewById(R.id.ll_notice).setVisibility(View.GONE);  //공지사항 숨김


        } else if (voMoimProfile.getParams().getmStt().equals("1")) {  //1: 가입된상태

            findViewById(R.id.ll_join_applied).setVisibility(View.GONE); //가입신청중
            layout.findViewById(R.id.btn_write).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);
            findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.ll_invite).setVisibility(View.VISIBLE);
            findViewById(R.id.imgb_search).setVisibility(View.VISIBLE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.VISIBLE);     //탑메뉴 글쓰기

            setPermit(layout, voMoimProfile, footer_view);  //권한

        } else if (voMoimProfile.getParams().getmStt().equals("2")) {  //가입차단

            findViewById(R.id.ll_join_applied).setVisibility(View.VISIBLE); //가입신청중
            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);
            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);
            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
            layout.findViewById(R.id.ll_notice).setVisibility(View.GONE);  //공지사항 숨김


        } else if (voMoimProfile.getParams().getmStt().equals("3")) {  //가입신청 승인 전
            findViewById(R.id.ll_join_applied).setVisibility(View.VISIBLE); //가입신청중
            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);

            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김
            layout.findViewById(R.id.ll_notice).setVisibility(View.GONE);  //공지사항 숨김
        }


        // 유동적인 가로사이즈의 타이틀
        //findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).measure(0, 0);
        int nWidth = 0;
        float wt_padPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 19, getResources().getDisplayMetrics()); //우측 패딩값 가져와서

        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).measure(0, 0);
        int nBackWidth = findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).getMeasuredWidth();//뒤로가기버튼사이즈


        if (findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).getVisibility() == View.VISIBLE) {
            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).measure(0, 0);
            nWidth = findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).getMeasuredWidth(); //글쓰기버튼사이즈
            wt_padPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 19, getResources().getDisplayMetrics()); //우측 패딩값 가져와서
        }


        if (findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_search).getVisibility() == View.VISIBLE) { //검색 기능이 있으면 추가.
            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_search).measure(0, 0);
            nWidth = nWidth + findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_search).getMeasuredWidth();
            nWidth = (int) (nWidth + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics())); //우측 패딩값 가져와서
        }

        findViewById(R.id.tv_topbar_title).setPadding(nBackWidth, 0, (int) (nWidth + wt_padPx), 0); //버튼 가로사이즈+우측패딩 값으로 타이틀의 우측 패딩값으로 세팅.
    }

    /**
     * 공지사항
     * 내용이 없을때 처리.
     *
     * @param voMoimProfileNotice
     * @param strTtl
     * @return
     */
    private String checkTtlNull(VoMoimProfile.VoMoimProfileNotice voMoimProfileNotice, String strTtl) {
        if (voMoimProfileNotice.getTtl().trim() == "") { //내용이 없을때
            strTtl = strTtl + getNullTextMsg(voMoimProfileNotice.getMsg()); //사진 , 이모티콘 표시
        } else {
            strTtl = strTtl + voMoimProfileNotice.getTtl();
        }
        return strTtl;
    }

    /**
     * 공지사항
     * 내용 없을때
     * 사진,이모티콘 표시 메시지
     *
     * @param msg
     */
    private String getNullTextMsg(String msg) {
        if (isImageExist(msg)) {
            return "사진을 올렸습니다.";
        } else {
            return "이모티콘을 올렸습니다.";
        }
    }

    /**
     * 공지화면에서 받은 메시지에 이미지가있는지 체크.
     *
     * @param msg
     */
    private boolean isImageExist(String msg) {
        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(msg);

        for (int i = 0; i < arMsgItem.size(); i++) {
            if (arMsgItem.get(i).getType().equals("1")) {
                return true;
            }
        }
        return false;
    }


    /**
     * 타임라인 상세화면 intent
     *
     * @param msgId
     * @param reply true :댓글달기
     */
    private void intentTimeLineDetail(String msgId, boolean reply) {

        SharedObject.setProperty_string(this, Constants.MOIM_MSGID, msgId);
        SharedObject.setProperty_boolean(this, Constants.MOIM_REPLY, reply);

        Intent intent = new Intent(this, Activity_TimeLineDetail.class);
        startActivity(intent);
    }

    /**
     * 권한설정에 따른 뷰화면 세팅
     *
     * @param layout
     */
    private void setPermit(LinearLayout layout, VoMoimProfile voMoimProfile, LinearLayout footer_view) {

        //상단 초대 버튼
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getIvtTy(this))) {
            layout.findViewById(R.id.ll_invite).setVisibility(View.VISIBLE);
        } else {
            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
        }

        //글쓰기 권한
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getWrtTy(this))) {

            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.btn_write).setVisibility(View.VISIBLE);
            //findViewById(R.id.imgb_write).setVisibility(View.VISIBLE);

        } else {

            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_write).setVisibility(View.GONE);
            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);

        }

        //모임 설정 관리, 하단 멤버초대하기 게시글
        if (SharedObject.getProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_SETTING, "").equals(voMoimProfile.getParams().getMmId())) {
            footer_view.findViewById(R.id.ll_member_invite_layout).setVisibility(View.GONE);
        } else {
            if (Util.getMymLv(this).equals("0")) {
                footer_view.findViewById(R.id.ll_member_invite_layout).setVisibility(View.GONE);
            } else {
                footer_view.findViewById(R.id.ll_member_invite_layout).setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * 7.4 모임 프로필에서 받은 설정값을 저장한다.
     * <p>
     * <p>
     * 모임공개설정  opnTy       공개 제한(int)             0:비공개, 1:공개, 2:모임명만 공개(기본),
     * 최대멤버수   maxUsr      가입 최대 사용자(int)        50, 100, 500 중에 설정, 기본 : 100
     * 가입신청받기  entTy       가입 제한(int)             0:자동가입(기본), 1:승인
     * <p>
     * 멤버들의 권한설정
     * covTy     모임 이름 및 커버 설정 권한      0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     * acptTy    멤버 가입신청 수락 권한         0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     * ivtTy     멤버 초대 권한                0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     * notiTy    공지글 등록 권한(int)          0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     * wrtTy     글쓰기 권한(int)             0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     * abmTy     앨범 만들기 권한(int)          0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     * rpyTy     댓글 쓰기 권한(int)           0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     * delTy     다른 멤버의 게시물, 댓글 삭제 권한(int)     0:모든멤버, 1:리더(기본), 2:리더와 공동리더
     * frcTy     멤버 탈퇴, 차단 권한(int)     0:모든멤버, 1:리더(기본), 2:리더와 공동리더
     * <p>
     * 채팅 설정 및 메뉴 우선순위 결정
     * 메뉴우선순위      mnuTy        메뉴우선순위 종류(int)                0:컨텐츠 타임라인, 1:오픈채팅방(기본)
     * 오픈채팅방 개설    opchtTy        오픈채팅방 개설가능 여부(int)        0:개설불가(기본), 1:개설가능
     * 모임멤버간1:1채팅  snglTy        모임 멤버간 1:1채팅 가능여부(int)                0:불가(기본), 1:가능
     *
     * @param voMoimProfile
     */
    private void saveConfig(VoMoimProfile voMoimProfile) {

        SharedObject.setProperty_string(this, Constants.mmShrtId, voMoimProfile.getParams().getMmShrtId()); //모임 단축 아이디

        SharedObject.setProperty_string(this, Constants.imgOrg, voMoimProfile.getParams().getImgTmb()); //모임이미지원본
        SharedObject.setProperty_string(this, Constants.imgTmb, voMoimProfile.getParams().getImgTmb()); //모임타이틀이미지
        SharedObject.setProperty_string(this, Constants.mmNm, voMoimProfile.getParams().getMmNm()); //모임명
        SharedObject.setProperty_string(this, Constants.mmTg, voMoimProfile.getParams().getMmTg()); //모임종류
        SharedObject.setProperty_string(this, Constants.imgTy, voMoimProfile.getParams().getImgTy()); //이미지 있음,없음


        //모임정보 -> 모임설정
        //모임소개말

        SharedObject.setProperty_string(this, Constants.opnTy, voMoimProfile.getParams().getOpnTy());    //모임공개설정
        SharedObject.setProperty_string(this, Constants.intr, voMoimProfile.getParams().getIntr());      //모임소개말
        SharedObject.setProperty_string(this, Constants.maxUsr, voMoimProfile.getParams().getMaxUsr());  //최대멤버수
        SharedObject.setProperty_string(this, Constants.entTy, voMoimProfile.getParams().getEntTy());    //가입신청받기


        //모임개설자아이디
        SharedObject.setProperty_string(this, Constants.regUsr, voMoimProfile.getParams().getRegUsr());  //모임생성자


        //멤버들의 권한설정
        SharedObject.setProperty_string(this, Constants.covTy, voMoimProfile.getParams().getCovTy());    //모임 이름 및 커버 설정 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        SharedObject.setProperty_string(this, Constants.acptTy, voMoimProfile.getParams().getAcptTy());  //멤버 가입신청 수락 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
        SharedObject.setProperty_string(this, Constants.ivtTy, voMoimProfile.getParams().getIvtTy());    //멤버 초대 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        SharedObject.setProperty_string(this, Constants.notiTy, voMoimProfile.getParams().getNotiTy());  //공지글 등록 권한(int)    0:모든멤버, 1:리더, 2:리더와 공동리더(기본)

        SharedObject.setProperty_string(this, Constants.wrtTy, voMoimProfile.getParams().getWrtTy());    //글쓰기 권한(int)   0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        SharedObject.setProperty_string(this, Constants.abmTy, voMoimProfile.getParams().getAbmTy());    //앨범 만들기 권한(int)    0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        SharedObject.setProperty_string(this, Constants.mLv, voMoimProfile.getParams().getmLv());

        //Log.e(TAG,"voMoimProfile.getParams().getRpyTy(): "+voMoimProfile.getParams().getRpyTy());

        SharedObject.setProperty_string(this, Constants.rpyTy, voMoimProfile.getParams().getRpyTy());    //댓글 쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더

        SharedObject.setProperty_string(this, Constants.delTy, voMoimProfile.getParams().getDelTy());    //다른 멤버의 게시물, 댓글 삭제 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
        SharedObject.setProperty_string(this, Constants.frcTy, voMoimProfile.getParams().getFrcTy());    //멤버 탈퇴, 차단 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더


        //채팅 설정 및 메뉴 우선순위 결정
        SharedObject.setProperty_string(this, Constants.mnuTy, voMoimProfile.getParams().getMnuTy());    //메뉴우선순위 종류(int)    0:컨텐츠 타임라인, 1:오픈채팅방(기본)
        SharedObject.setProperty_string(this, Constants.opchtTy, voMoimProfile.getParams().getOpchtTy());    //오픈채팅방 개설가능 여부(int)    0:개설불가(기본), 1:개설가능
        SharedObject.setProperty_string(this, Constants.snglTy, voMoimProfile.getParams().getSnglTy());  //모임 멤버간 1:1채팅 가능여부(int)    0:불가(기본), 1:가능

        //사용자 레벨 저장
        SharedObject.setProperty_string(this, Constants.mLv, voMoimProfile.getParams().getmLv());    //내 사용자 레벨(int) 0:일반, 1:리더, 2:공동리더


        //알림 저장
        SharedObject.setProperty_string(this, Constants.useMmPush, voMoimProfile.getParams().getUseMmPush());        //모임 알림 사용여부(int)        0:사용안함, 1:사용
        SharedObject.setProperty_string(this, Constants.useMmBbsPush, voMoimProfile.getParams().getUseMmBbsPush());  //모임 글 알림 사용여부(int)                0:사용안함, 1:사용
        SharedObject.setProperty_string(this, Constants.useMmRpyPush, voMoimProfile.getParams().getUseMmRpyPush());  //모임 댓글 알림 사용여부(int)                0:사용안함, 1:사용

        //사용자의 모임프로필 이미지
        SharedObject.setProperty_string(this, Constants.pfImg, voMoimProfile.getParams().getPfImg());      //프로필이미지

        //종목상담
        SharedObject.setProperty_string(this, Constants.csltTy, voMoimProfile.getParams().getCsltTy());
        SharedObject.setProperty_string(this, Constants.ldngTy, voMoimProfile.getParams().getLdngTy());


        //전문가 구분값 저장
        SharedObject.setProperty_string(this, Constants.xprt, voMoimProfile.getParams().getXprt());

        //내상태 가입여부
        SharedObject.setProperty_string(this, Constants.mstt, voMoimProfile.getParams().getmStt());

        //타임라인에서 글쓰기
        //SharedObject.setProperty_boolean(this, Constants.MOIM_WRITE_MAIN, false);
    }


    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     */
    private void setDate(VoBoardList voBoardList) {
        mMoimListRecyclerAdapter.setItem(voBoardList, mDeleteList);
        mWrapAdapter.notifyDataSetChanged();
        mDeleteList = false;  //
    }


    /**
     * 상단타이틀 호출 완료후 이벤트 처리
     *
     * @param layout
     */
    private void setOnClickEvent(final LinearLayout layout, final VoMoimProfile voMoimProfile, final LinearLayout footer_layout) {

        //멤버 모임정보
        findViewById(R.id.imgb_member_moin_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMemberInvite();

            }
        });

        //설정
        findViewById(R.id.imgb_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentSet(voMoimProfile);
            }
        });


        //Top 글쓰기
        findViewById(R.id.imgb_write).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Util.intentActivity(Activity_MoimTimeLineList.this, Activity_MoimWrite.class);
                intentMoimWrite();
            }
        });


        //초대하기 버튼
        layout.findViewById(R.id.ll_invite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentMemberinfo();
            }
        });


        //모임 글검색화면
        findViewById(R.id.imgb_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.intentActivity(Activity_MoimTimeLineList.this, Activity_MoimTimelineSearch.class);
            }
        });

        //모임 가입하기 화면
        findViewById(R.id.ll_join).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogJoin(voMoimProfile.getParams());
            }
        });


        //타이틀 우측 글쓰기
        layout.findViewById(R.id.btn_write).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Util.intentActivity(Activity_MoimTimeLineList.this, Activity_MoimWrite.class);
                intentMoimWrite();
            }
        });

        //공지사항
        layout.findViewById(R.id.ll_notice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.intentActivity(Activity_MoimTimeLineList.this, Activity_MoimNotice.class);
            }
        });

        //모임 설정 관리 닫기
        layout.findViewById(R.id.ll_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.findViewById(R.id.ll_setting).setVisibility(View.GONE);
                SharedObject.setProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_SETTING, voMoimProfile.getParams().getMmId());

            }
        });

        //모임설정관리 바로가기
        layout.findViewById(R.id.ll_moim_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_MoimTimeLineList.this, Activity_SetManage.class));
                SharedObject.setProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_SETTING, voMoimProfile.getParams().getMmId());
            }
        });

        footer_layout.findViewById(R.id.ll_member_invite_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMemberinfo();
            }
        });


        //대화방으로 이동
        findViewById(R.id.imgb_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dbroomName = LocalDB.getChatListDbHelper(getActivity()).getRoomName(voMoimProfile.getParams().getRmId()); //DB 모임대화방 조회
//                if (voMoimProfile.getParams().getMmNm().equals(dbroomName)) {  //DB에 대화방에 있을경우 기존 대화방으로 입장
//                    moimChatRequest(voMoimProfile.getParams().getRmId());
//                } else {                                                       //DB에 대화방이 없을경우 입장처리
//                    roomName = voMoimProfile.getParams().getMmNm();
//                    Moa.opentalkenter(getActivity(), voMoimProfile.getParams().getRmId(), voMoimProfile.getParams().getMmNm(), "7", mMainActivityHandler);
//                }
//                LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();
//                ChatListDbHelper db = LocalDB.getChatListDbHelper(Activity_MoimTimeLineList.this);
//                Statics.ROOMINFO = db.getChatData(voMoimProfile.getParams().getRmId());
//                Intent mIntent = new Intent(getActivity(), Activity_ChatRoom.class);
//                startActivity(mIntent);

                if (LocalDB.getChatListDbHelper(getActivity()).existChatRoom(voMoimProfile.getParams().getRmId())) {  //DB에 대화방에 있을경우 기존 대화방으로 입장
                    LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                   ProgressDialog m_dlgProgress = new ProgressDialog(Activity_MoimTimeLineList.this, R.style.TranProgDialog);
//                    m_dlgProgress.setIndeterminate(true);
//                    m_dlgProgress.setCancelable(false);
//                    m_dlgProgress.show();

//                        ChatListDbHelper db = LocalDB.getChatListDbHelper(Activity_MoimTimeLineList.this);
//                        Statics.ROOMINFO = db.getChatData(voMoimProfile.getParams().getRmId());
//                            Intent mIntent = new Intent(getActivity(), Activity_ChatRoom.class);
//                            startActivity(mIntent);

                    moimChatRequest(voMoimProfile.getParams().getRmId());
//                            LoadingManager.with(Activity_MoimTimeLineList.this).hideLoadingDialog();
//                        }
//                    }, 5000);

                } else {                                                       //DB에 대화방이 없을경우 입장처리
//                    LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();

                    LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();
                    Moa.opentalkenter(getActivity(), voMoimProfile.getParams().getRmId(), voMoimProfile.getParams().getMmNm(), "7", mMainActivityHandler);


                }

            }
        });
    }

    /**
     * 4.9 대화방 정보 요청
     *
     * @param roomid
     */
    public void moimChatRequest(String roomid) {
//        LoadingManager.with(Activity_MoimTimeLineList.this).showLoadingDialog();
        Moa.chatRoomInfoRequest(Activity_MoimTimeLineList.this, roomid, mMainChatHandler);
    }

    //모임결과 처리 핸들러
    private Handler mMainChatHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                LogTrace.E( "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_CHATROOM_INFO:
                        strResult = (String) data.body.get("result");
                        LogTrace.E( "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {

                            try {
                                Statics.ROOMINFO = getRoomInfo(data);
                                intentChat();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private VoChatList getRoomId(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("roomId");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }
        return info;
    }

    private VoChatList getRoomInfo(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("users");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }
        return info;
    }

    private VoChatList getRoomInfo(MOAData data, String roomName) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            info.setRoom_name(roomName);
            info.setRoom_type("7");

        }
        return info;
    }


    /**
     * 모임 챗 이동.
     */
    private void intentChat() {

        Intent intent = new Intent(Activity_MoimTimeLineList.this, Activity_ChatRoom.class);
        startActivity(intent);
        LoadingManager.with(Activity_MoimTimeLineList.this).hideLoadingDialog();
    }


    /**
     * 모임글쓰기화면
     */
    private void intentMoimWrite() {
        SharedObject.setProperty_string(Activity_MoimTimeLineList.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_WRITE);

        Intent intent = new Intent(Activity_MoimTimeLineList.this, Activity_MoimWrite.class);
        startActivityForResult(intent, Constants.RESULT_MOIM_WRITE);
    }

    /**
     * 모임멤버정보리스트 화면
     */
    private void intentMemberinfo() {
        Intent intent = new Intent(Activity_MoimTimeLineList.this, Activity_MoimMemberInfoList.class);
        startActivityForResult(intent, Constants.RESULT_MOIM_WRITE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogTrace.E( "onActivityResult");
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == Constants.RESULT_MOIM_WRITE) { //글쓴이후 처리
            if (resultCode == RESULT_OK) {
                mDeleteList = true;
                MoimProfileRequest();
            }
        }
    }

    /**
     * 모임 정보
     * 멤버 초대
     */
    private void intentMemberInvite() {
        Intent intent = new Intent(Activity_MoimTimeLineList.this, Activity_MoimMemberInfoList.class);
        startActivity(intent);
    }


    /**
     * 모임정보 화면으로 intent
     */
    private void intentSet(VoMoimProfile voMoimProfile) {

        Intent intent = new Intent(Activity_MoimTimeLineList.this, Activity_MoimInfo.class);
        intent.putExtra("profile", Constants.MOIM_LIST_IMAGE_URL + voMoimProfile.getParams().getImgTmb().replace("thumb_", ""));
        intent.putExtra("moim_name", voMoimProfile.getParams().getMmNm());
        startActivity(intent);
    }


    /**
     * 모임 가입하기 다이얼로그
     * 현재 질문가입형
     * <p>
     * 0:승인, 1:자동가입(기본), 2:비밀번호
     */
    private void showDialogJoin(final VoMoimProfile.VoMoimProfileNoticeParam param) {
        switch (param.getEntTy()) {
            /*case "0":  //
                //질문가입 다이얼로그
                final Dialog_Join dialog_Join = new Dialog_Join(Activity_MoimTimeLineList.this, param.getQstn());
                dialog_Join.show();
                dialog_Join.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Moa.moimApply(Activity_MoimTimeLineList.this, mMainActivityHandler);
                        dialog_Join.dismiss();
                    }
                });
                break;*/

            case "1":  //팝업 필요없이 바로 가입
                Moa.moimApply(Activity_MoimTimeLineList.this, "", "", mMainActivityHandler);

                break;


            case "0":  //승인 질문입력 팝업
            case "2": //비밀번호 파법

                final Dialog_MoimJoinType dialog_MakeMoim = new Dialog_MoimJoinType(this, param); //param.getQstn() getEntTy
                dialog_MakeMoim.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int id = view.getId();

                        if (id == R.id.tv_cancle) {
                            dialog_MakeMoim.dismiss();
                        } else if (id == R.id.tv_confirm) {
                            dialog_MakeMoim.getJoinTxt();   //비밀번호

                            String answ = null;   //모임 가입질문
                            String mmPw = null;  //모임 가입비밀번호

                            if (param.getEntTy().equals("0")) { //리더승인이면
                                answ = dialog_MakeMoim.getJoinTxt();   //가입질문

                            } else if (param.getEntTy().equals("2")) {
                                mmPw = dialog_MakeMoim.getJoinTxt();   //비밀번호
                            }

                            Util.hideKeyboard(Activity_MoimTimeLineList.this, ((EditText) dialog_MakeMoim.findViewById(R.id.edt_input)));
                            dialog_MakeMoim.dismiss();

                            Moa.moimApply(Activity_MoimTimeLineList.this, answ, mmPw, mMainActivityHandler);
                        }

                    }
                });
                dialog_MakeMoim.show();

                break;
        }
    }


    /**
     * 완료메시지 다이얼로그
     */
    private void completeDialog() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final CompleteDialog mDialog = new CompleteDialog(Activity_MoimTimeLineList.this);
                mDialog.show();
                mDialog.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                    }
                });
            }
        });
    }

    /**
     * 삭제처리 이후.
     * 어뎁터에서 데이터를 삭제한다
     * <p>
     * WrapAdapter<BoardListRecyclerAdapter> -BoardListRecyclerAdapter
     *
     * @param index
     * @param size
     */
    public void RemoveAdapter(int index, int size) {
        mMoimListRecyclerAdapter.notifyItemRemoved(index);
        mMoimListRecyclerAdapter.notifyItemRangeChanged(index, size);
        mMoimListRecyclerAdapter.notifyDataSetChanged();
        mWrapAdapter.notifyDataSetChanged();
    }


    /**
     * 모임 글 리스트 어댑터를 새로고침한다.
     */
    public void reFreshAdapter() {
        LogTrace.E( "reFreshAdapter");
        mMoimListRecyclerAdapter.notifyDataSetChanged();
        mWrapAdapter.notifyDataSetChanged();
    }

    /**
     * 멤버 강제탈퇴후 상단 멤버카운트를 1 내린다.
     */
    public void memberCountDown() {
        int nMembercount = Integer.parseInt(((TextView) layout.findViewById(R.id.tv_user_cnt)).getText().toString()) - 1;
        ((TextView) layout.findViewById(R.id.tv_user_cnt)).setText(nMembercount + "");
    }


}
