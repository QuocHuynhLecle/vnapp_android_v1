package com.wave.messenger.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.fragment.Fragment_Invite;
import com.wave.messenger.fragment.Fragment_Invite_other;
import com.wave.messenger.fragment.dummy.TestInviteItem;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.SharedObject;

import java.util.ArrayList;

/**
 * 멤버 초대하기 화면
 */
public class Activity_MemberInvite extends BaseActivity {//implements Fragment_Invite.OnItemSelectedListener{

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    String TAG = "MemberInvite";
    private Context context;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_member_invite);

        setRecyclerView();
        initView();
        setOnClickEvent();

    }



    private void setRecyclerView() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);

        final int[] ICONS = new int[]{
                R.drawable.btn_phonenum_invite, R.drawable.btn_another_invite
        };


        mViewPager.setAdapter(mSectionsPagerAdapter);

        /*TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.getTabAt(0).setIcon(ICONS[0]);*/

//        tabLayout.getTabAt(1).setIcon(ICONS[1]);


    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fragment_Invite.getSelectecPhoneNumList().size() < 1) {
                    Toast.makeText(Activity_MemberInvite.this, "연락처를 선택하세요", Toast.LENGTH_SHORT).show();
                } else {
                    inviteLoop();
                }
                //Toast.makeText(Activity_MemberInvite.this, "" + fragment_Invite.getSelectecPhoneNumList().size(), Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.ll_top1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectTab(0);
            }
        });

        findViewById(R.id.ll_top2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectTab(1);
            }
        });


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setSelectTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {






            }
        });



    }


    /**
     * 상단 탭 선택
     * @param state
     */
    private void setSelectTab(int state) {
        Log.e(TAG,"setSelectTab "+state);

        switch (state){
            case 0:
                //findViewById(R.id.imgb_right).setVisibility(View.VISIBLE);

                mViewPager.setCurrentItem(0);
                findViewById(R.id.img_top1).setSelected(true);
                findViewById(R.id.img_top2).setSelected(false);
                findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
                findViewById(R.id.tab_bar2).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_MemberInvite.this,R.color.bg_purple));
                ((TextView)findViewById(R.id.imgb_top2_txt)).setTextColor(Util.getColor(Activity_MemberInvite.this,R.color.gray_9f));

                break;

            case 1:
                //findViewById(R.id.imgb_right).setVisibility(View.GONE);

                mViewPager.setCurrentItem(1);
                findViewById(R.id.img_top1).setSelected(false);
                findViewById(R.id.img_top2).setSelected(true);
                findViewById(R.id.tab_bar1).setVisibility(View.INVISIBLE);
                findViewById(R.id.tab_bar2).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_MemberInvite.this,R.color.gray_9f));
                ((TextView)findViewById(R.id.imgb_top2_txt)).setTextColor(Util.getColor(Activity_MemberInvite.this,R.color.bg_purple));
                break;

        }
    }

    /**
     * 문자 메시지 선택만큼 반복.
     */
    private void inviteLoop() {
        String PhoneNum = null;
        String msg = null;
        ArrayList<String> phone = new ArrayList();

        for (TestInviteItem item : fragment_Invite.getSelectecPhoneNumList()) {


            //Util.getMsg
            msg = SharedObject.getProperty_string(this, Constants.mmNm, "") + " "+getResources().getString(R.string.invite_sms_info)
                    +Constants.MOIM_INVITE_URL
                    + com.wave.messenger.util.SharedObject.getProperty_string(this, Constants.mmShrtId, "")  // 초대 링크 값 받아오기.
                    + "\nFrom " + MessengerInfo.getUserName(this);

            PhoneNum = item.getPhone();
            phone.add(item.getPhone().toString());

            //PhoneNum=MessengerInfo.getUserPhoneNumber(this);
            //PhoneNum ="01073606542";

//            sendMMS(PhoneNum, msg);
            Log.e("inviteLoop", "phone : " + PhoneNum);
//            sendSMS(PhoneNum, msg);
        }

        String toNumbers = "";
        for ( String s :  phone)
        {
            toNumbers = toNumbers + s + ";";
        }

        sendToSms(toNumbers, msg);
    }


    /**
     * 문자 메시지를 보낸다. -> 편집 불가능 바로 보내기
     *
     * @param phoneNum
     * @param text
     */
//    public void sendSMS(String phoneNum ,String text){
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phoneNum, null, text, null, null);
//
//
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(),"SMS faild, please try again later!",
//                    Toast.LENGTH_LONG).show();
//            e.printStackTrace();
//        }
//    }

    /**
     * mms로 보낸다.편집 가능한 상테로.
     *
     * @param phoneNum
     * @param text
     */
    public void sendMMS(String phoneNum, String text) {
        Intent sendIntent1 = new Intent(Intent.ACTION_SEND);
        sendIntent1.setType("text/x-vcard");
        sendIntent1.putExtra("address",phoneNum);
        sendIntent1.putExtra("sms_body",text);
        startActivity(sendIntent1);

        /*PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, Activity_MemberInvite.class), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNum, null, text, pi, null);*/


    }

    private void sendSMS(String phoneNum, String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);


        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "문자 메시지가 전송되었습니다.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNum, null, message, sentPI, deliveredPI);

    }


    /**
     * 문자를 보낸다.
     * @param number
     * @param msg
     */
    private void sendToSms(String number, String msg) {
        Log.e(TAG,"sendToSms : "+number+ " msg: "+msg );

        String strNumber=number.replace("[","");
        strNumber=strNumber.replace("]","");
        strNumber=strNumber.replace("-","");

        Log.e(TAG,"sendToSms : "+strNumber+ " msg: "+msg );

        try {
            /*Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", msg);   // 보낼 문자
            sendIntent.putExtra("address", number); // 받는사람 번호
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);*/

           /* ContentValues values = new ContentValues();
            values.put("address", number);
            values.put("body", msg);
            getContentResolver().insert(Uri.parse("content://sms/sent"), values);*/

            /*Intent sendIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms://"));
            sendIntent.putExtra("address", number);
            sendIntent.putExtra("sms_body", msg);
            startActivity(sendIntent);*/

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("smsto:"+number));
            //intent.putExtra("address", number);
            intent.putExtra("sms_body", msg);
            intent.putExtra("exit_on_sent", true);
            startActivityForResult(intent, 111);



        } catch (Exception ex) {
//            msg_toast(R.string.MSG_FAIL_MSG_LINE);
//            msg_error(ex);
        }
    }


    /**
     * //TODO 내용없이 보내는 소스
     * sms보내기 0905
     * @param
     * @param
     */
   /* private void sendSMS2(String number, String msg) {
        Log.e(TAG,"sendSMS2 : "+number+ " msg: "+msg );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, msg);

            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            {
                sendIntent.setPackage(defaultSmsPackageName);
            }
            //startActivity(sendIntent);
            startActivityForResult(sendIntent, 111);

            //


        }
        else // For early versions, do what worked for you before.
        {
            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address",number);
            smsIntent.putExtra("sms_body",msg);
            startActivity(smsIntent);
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG,"requestCode: "+requestCode+" resultCode: "+resultCode );


    }

    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.member_invite));

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.forceHideKeyboard(getActivity());
                finish();
            }
        });
        findViewById(R.id.imgb_right).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);


        findViewById(R.id.img_top1).setSelected(true);
        findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_MemberInvite.this,R.color.bg_purple));


    }

    /*@Override
    public void onRssItemSelected(ArrayList<TestInviteItem> items) {
        Log.e(TAG,"TestInviteItem "+items.size());
    }*/
    Fragment_Invite fragment_Invite;

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragment_Invite = Fragment_Invite.newInstance();
                case 1:
                    Fragment_Invite_other fragment_Invite_other = Fragment_Invite_other.newInstance(position + "");
                    return fragment_Invite_other;


            }
            return null;
        }


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "연락처로 초대";
//                case 1:
//                    return "다른 방법으로 초대";

            }
            return null;
        }
    }


}
