package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.MoimInfoRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_algin;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.vo.VoMoimMemberList;
import com.kevin.wraprecyclerview.WrapAdapter;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임 ->  회원정보 화면
 *
 */
public class Activity_MoimMemberInfoList extends BaseActivity {

    static public Activity_MoimMemberInfoList activity;

    String TAG=this.getClass().getName();
    private WrapAdapter<MoimInfoRecyclerAdapter> mWrapAdapter;
    MoimInfoRecyclerAdapter mMoimInfoRecyclerAdapter;

    String mOrdTy="0";  //0:이름, 1:가입일, 2:레벨
    String mOrd="0";       //정렬값(int) 0:내림차순, 1:올림차순
    static public boolean bRestart=false;   //가입신청자 화면에서 가입수락을 하고온경우 리스트 다시 요청

    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arMemberList;  //원본 멤버리스트
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arSearchMemberList;    //검색된 멤버리스트

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_member_info);

        activity=this;
        initView();
        setOnClickEvent();
        setRecyclerView();
    }

    /**
     * 회원리스트를 볼수있는 권한을 체크하고, 숨김처리.
     */
    private void checkMemberListPermit() {
        if(Util.getPermissionLv(Util.getMymLv(this),Util.getFrcTy(this))){
            //리더, 권한있는 공동리더 딱히 처리할건 없음.

        }else{
            //일반멤버   //리스트
            findViewById(R.id.ll_member_search).setVisibility(View.GONE);  //검색
            mWrapAdapter.getHeadersView().get(0).findViewById(R.id.ll_member_title).setVisibility(View.GONE); //리스트 타이틀
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        if(bRestart){
            MoimProfileRequest(mOrdTy);
            bRestart=false;
        }
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     리스트 설정
     */
    private void setRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        mMoimInfoRecyclerAdapter = new MoimInfoRecyclerAdapter(this);

        mWrapAdapter = new WrapAdapter<>(mMoimInfoRecyclerAdapter);
        mWrapAdapter.adjustSpanSize(recyclerView);
        recyclerView.setAdapter(mWrapAdapter);

        addHeaderView();

        MoimProfileRequest(mOrdTy);

//        checkMemberListPermit(); //회원권한에 따른 회원리스트 숨김 처리.

        //검색
        ((EditText)findViewById(R.id.et_topbar_search)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Log.e(TAG,"onTextChanged: "+i+" "+i1+" "+i2);
                if(i2==0){
                    findViewById(R.id.imgb_close).setVisibility(View.GONE);
                    setOriginalList();
                    setheadVisibility(false);
                }else{
                    findViewById(R.id.imgb_close).setVisibility(View.VISIBLE);
                }

                String strText=((EditText)findViewById(R.id.et_topbar_search)).getText().toString().trim();
                if(TextUtils.isEmpty(strText)){
                    //다시 보이기 처리
                    setOriginalList();

                }else{      //검색  , 상단 메뉴 숨김처리
                    setSearchList(strText);
                    setheadVisibility(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }


    /**
     *
     * 검색시 상단 헤더 화면 숨김/보임처리
     * @param type true 이면 높이 0
     */
    private void setheadVisibility(boolean type) {
        LinearLayout layout = (LinearLayout) mWrapAdapter.getHeadersView().get(0).findViewById(R.id.ll_head);
        ViewGroup.LayoutParams params = layout.getLayoutParams();

        if(type){
            params.height = 0;
        }else{
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        }

        params.width = LinearLayout.LayoutParams.WRAP_CONTENT;
        layout.setLayoutParams(params);
    }


    /**
     * 원래 리스트로 되돌리기
     */
    private void setOriginalList() {
        if(arMemberList!=null){
            setDate(arMemberList);
        }
    }

    /**
     * 입력한 텍스트로 리스트 재구성
     * @param strText
     */
    private void setSearchList(String strText) {
        arSearchMemberList= new ArrayList<>();

        for (VoMoimMemberList.VoMoimMemberlistItem item : arMemberList) {
            if (item.getUsNm().contains(strText)) {
                arSearchMemberList.add(item);
            }
        }

        if(arSearchMemberList.size()==0){
            for (VoMoimMemberList.VoMoimMemberlistItem item : arMemberList) { //초성검색
                if (StringProcesser.matchString(item.getUsNm(),strText)) {
                    arSearchMemberList.add(item);
                }
            }
        }
        mMoimInfoRecyclerAdapter.setItem(arSearchMemberList);
        mWrapAdapter.notifyDataSetChanged();
    }


    private void initView() {
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.member_info));
        findViewById(R.id.imgb_back_btn).measure(0, 0);
        int nWidthMoveToMoim=findViewById(R.id.imgb_back_btn).getMeasuredWidth();
        findViewById(R.id.linearLayout1).setPadding(nWidthMoveToMoim,0,nWidthMoveToMoim,0);
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, "")); //모임명
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);

        //Edtext 닫기버튼
        findViewById(R.id.imgb_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setheadVisibility(false);
                ((EditText)findViewById(R.id.et_topbar_search)).setText("");
                findViewById(R.id.imgb_close).setVisibility(View.GONE);
            }
        });
    }

    /**
     * 멤버초대하기
     */
    private void intentMemberInveite() {
        Intent intent = new Intent(Activity_MoimMemberInfoList.this, Activity_MemberInvite.class);
        startActivity(intent);
    }

    /**
     * 가입신청자
     */
    private void intentMemberApplicant() {
        Intent intent = new Intent(Activity_MoimMemberInfoList.this, Activity_MemberApplicant.class);
        startActivity(intent);
    }

    /**
     * 7.9 모임 멤버 리스트

     "0":가입신청자 리스트
     "1":가입된 사용자 리스트
     "2":차단된 사용자 리스트

     mOrdTy    //0:이름, 1:가입일, 2:레벨
     mOrd 정렬값(int) 0:내림차순, 1:올림차순
     */
    private void MoimProfileRequest(String ordTy) {
        //Log.e(TAG,"MoimProfileRequest: "+ordTy);

        if(mOrdTy.equals(ordTy)){
            if(mOrd.equals("0")){
                mOrd="1";
            }else{
                mOrd="0";
            }
        }
        mOrdTy=ordTy;

        //MOALog.w(TAG+" requestMoimProfile");
        Moa.moimMemberlistRequest(this,"1",mOrdTy, mOrd, mMainActivityHandler);
    }

    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_MEMBERLIST :    //모임 멤버 리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            try {   //gson파싱
                                VoMoimMemberList voMoimMemberList;
                                Gson gson = new Gson();
                                voMoimMemberList = gson.fromJson( data.body.toString() , VoMoimMemberList.class);

                                arMemberList=new ArrayList<>(voMoimMemberList.getParams()); //검색을 위한 원본 멤버리스트
                                setDate(voMoimMemberList.getParams());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     *
     * @param voBoardList
     */
    private void setDate(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> voBoardList) {
        Log.e(TAG,"setDate: voBoardList "+voBoardList.size());
        VoMoimMemberList.VoMoimMemberlistItem myItem;

//        if(mOrdTy.equals("0")){ //이름순정렬이면  //자신을 최상위로
//            for (VoMoimMemberList.VoMoimMemberlistItem item:voBoardList) {
//                if(item.getUserId().equals(MessengerInfo.getUserId(this))){
//                    myItem=item;
//                    voBoardList.remove(item);
//                    voBoardList.add(0,myItem);
//                    break;
//                }
//            }
//        }

        // 리더가 항상 최상위에 위치 - 180515 mwju
        for (VoMoimMemberList.VoMoimMemberlistItem item : voBoardList) {
            if("1".equals(item.getUsrLv())){
                myItem=item;
                voBoardList.remove(item);
                voBoardList.add(0,myItem);
                break;
            }
        }

        mMoimInfoRecyclerAdapter.setItem(voBoardList);
        mWrapAdapter.notifyDataSetChanged();
    }

    /**
     * 상단 레이어
     */
    public void addHeaderView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.header_moim_info, null);
        mWrapAdapter.addHeaderView(layout);

        setHeadViewOnClickEvent(layout);
    }

    /**
     * 상단 헤더 클릭이벤트
     */
    private void setHeadViewOnClickEvent(final LinearLayout layout) {

        //멤버초대하기
        layout.findViewById(R.id.ll_member_invite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMemberInveite();
            }
        });

        //가입신청자
        layout.findViewById(R.id.ll_member_join).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMemberApplicant();
            }
        });

        layout.findViewById(R.id.ll_sort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog_algin dialog_algin= new Dialog_algin(Activity_MoimMemberInfoList.this);
                dialog_algin.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e(TAG,"onClick "+view.getId());

                        int id =view.getId();

                        if(id==R.id.tv_option0){
                            ((TextView)layout.findViewById(R.id.tv_sort)).setText(getResources().getString(R.string.by_name));
                            MoimProfileRequest("0");
                        }else if(id==R.id.tv_option1){
                            ((TextView)layout.findViewById(R.id.tv_sort)).setText(getResources().getString(R.string.by_join));
                            MoimProfileRequest("1");
                        }else if(id==R.id.tv_option2) {
                            ((TextView) layout.findViewById(R.id.tv_sort)).setText(getResources().getString(R.string.by_level));
                            MoimProfileRequest("2");
                        }
                    }
                });
                dialog_algin.show();
            }
        });

        //상단 가입신청 화면 버튼
        setPermit(layout);
    }

    /**
     * 권한에 따른 가입신청자화면 버튼 설정
     * @param layout
     */
    private void setPermit(LinearLayout layout) {

        //Log.e(TAG,"getMymLv "+Util.getMymLv(this)+" getAcptTy "+Util.getAcptTy(this));

        if(Util.getPermissionLv(Util.getMymLv(this),Util.getAcptTy(this))){  //일반일경우 가입신청자 숨김.
            layout.findViewById(R.id.ll_member_join).setVisibility(View.VISIBLE);
        }else{
            layout.findViewById(R.id.ll_member_join).setVisibility(View.GONE);
        }

        if(Util.getPermissionLv(Util.getMymLv(this),Util.getIvtTy(this))){

        }else{
            layout.findViewById(R.id.ll_member_invite).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(Activity_MoimMemberInfoList.this,getString(R.string.member_invite_permit), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * 강제탈퇴후 리스트 새로고침.
     */
    public void refreshList() {
        mWrapAdapter.notifyDataSetChanged();
    }

}
