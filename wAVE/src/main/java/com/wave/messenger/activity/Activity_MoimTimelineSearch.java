package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.BoardListRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 4. 10..
 *
 * 종목명 검색 화면
 * 메인-모임 타임라인- 돋보기
 *
 */
public class Activity_MoimTimelineSearch extends BaseActivity {

    String TAG=this.getClass().getName();

    private BoardListRecyclerAdapter mMoimListRecyclerAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    SwipeRefreshLayout mSwipeRefreshLayout;

    //boolean mDeleteList=false;

    boolean mbRefresh = false; //새로고침 , 기존 삭제

    RecyclerView mRecyclerView;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_timeline_search);

        initView();

        setOnClickEvent();
    }


    private void initView() {
       /* findViewById(R.id.ll_findbysubject).setVisibility(View.GONE);
        findViewById(R.id.ll_recommend).setVisibility(View.GONE);
        ((EditText)findViewById(R.id.et_topbar_search)).setHint(getString(R.string.moim_search_edt_hint));*/

        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.title_find_moim));
        findViewById(R.id.tv_complete).setVisibility(View.GONE);
    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.tv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setRecyclerView();


                requestBoardList("");
            }
        });

    }



    private void setRecyclerView() {

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mMoimListRecyclerAdapter = new BoardListRecyclerAdapter(this, Constants.SET_ENTRY_TYPE.written_list);

        mRecyclerView.setAdapter(mMoimListRecyclerAdapter);

        //다음페이지 자동 로딩
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e(TAG, "onLoadMore offset: "+page+" "+totalItemsCount);
                loadNextDataFromApi();

            }
        };

        mRecyclerView.addOnScrollListener(scrollListener);

        mSwipeRefreshLayout=((SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Log.e(TAG,"SwipeRefreshLayout");
                refreshItems();
            }
        });

    }

    /**
     * 다음페이지 로딩
     * getMsgId
     */
    public void loadNextDataFromApi( ) {
        Log.e(TAG, "loadNextDataFromApi");
        requestBoardList(mMoimListRecyclerAdapter.getMsgId());
    }


    /**
     * 새로고침
     */
    public void refreshItems() {

        setRecyclerView();
        requestBoardList("");
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
        mbRefresh = false;
    }

    /**
     * 하단 모임 글 리스트를 호출한다
     */
    private void requestBoardList(String lastMsgid) {


        String keyword= ((EditText)findViewById(R.id.et_topbar_search)).getText().toString().trim();
        Log.e(TAG,"keyword: "+keyword);

        if(TextUtils.isEmpty(keyword)){
            Toast.makeText(this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
        }else{


            Moa.moimBoardListRequest(this
                    , SharedObject.getProperty_string(this, Constants.MOIM_USER_PROFILE_ID,null)
                    ,"0", SharedObject.getProperty_string(this, Constants.MOIM_ID,null),lastMsgid,keyword, mMainActivityHandler);
        }

        Util.hideKeyboard(Activity_MoimTimelineSearch.this,(EditText)findViewById(R.id.et_topbar_search));

    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);


                Util.appendLog(data.body.toString());

                String strResult;
                switch (data.ptc) {

                    //모임리스트
                    case PacketTypes.PTC_IMS_MOIM_BOARD_LIST:
                        Log.e(TAG,"result PTC_IMS_MOIM_BOARD_LIST ");

                        VoBoardList voBoardList;
                        Gson gson = new Gson();
                        //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                        voBoardList = gson.fromJson( data.body.toString() , VoBoardList.class);

                        Log.e(TAG,"voBoardList "+voBoardList.getResult()+" size: "+voBoardList.getParams().size());


                        setDate(voBoardList);

                        onItemsLoadComplete();

                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    private void setDate(VoBoardList voBoardList) {
        Log.e(TAG,"setDate mbRefresh: "+mbRefresh);
        mMoimListRecyclerAdapter.setItem(voBoardList, mbRefresh);
        mMoimListRecyclerAdapter.notifyDataSetChanged();

    }




}
