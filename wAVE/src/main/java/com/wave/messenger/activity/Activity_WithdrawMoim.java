package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;

import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * 모임 탈퇴 화면
 */
public class Activity_WithdrawMoim extends BaseActivity {

    String TAG=getClass().getSimpleName();

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_withdraw_moim);

        initView();
        setOnClickEvent();
    }

    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ((CheckBox)findViewById(R.id.check_whthdraw)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Log.e(TAG,"onCheckedChanged isChecked "+isChecked);

                findViewById(R.id.btn_confirm).setEnabled(isChecked);
                if(isChecked){
                    findViewById(R.id.btn_confirm).setBackgroundResource(R.color.bg_purple);
                }else{
                    findViewById(R.id.btn_confirm).setBackgroundResource(R.color.gray);
                }

            }
        });


        findViewById(R.id.btn_confirm).setEnabled(false);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 탈퇴처리
                //Toast.makeText(getApplicationContext(), "btn_confirm", Toast.LENGTH_SHORT).show();

                moimExitRequest();
            }
        });

    }

    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.withdraw_moim));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);


    }

    /**
     * 7.13 모임 탈퇴
     * 회원이 모임을 탈퇴한다
     */
    private void moimExitRequest() {
        Moa.moimExit(this, mMainActvtHandler);
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;

                switch (data.ptc) {
                    case 0x00080019 :    // 모임 탈퇴
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());
                            Toast.makeText(Activity_WithdrawMoim.this, "모임 탈퇴 완료", Toast.LENGTH_SHORT).show();
                            Fragment_Main.getInstance().FriendListRequest();
                            Fragment_Main.getInstance().setChatTab();
                            finish();

                            Activity_MoimInfo.activity.finish();
                            Activity_MoimTimeLineList.activity.finish();
                            Activity_MymoimList.activity.refreshItems();



                        }else if(strResult.equals(Constants.ERROR)){
                            Toast.makeText(Activity_WithdrawMoim.this, "모임탈퇴 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;


                    //0x00080019
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



}
