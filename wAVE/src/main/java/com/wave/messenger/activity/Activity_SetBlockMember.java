package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_BlockMember;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;


/**
 * 멤버탈퇴 차단설정 화면
 */
public class Activity_SetBlockMember extends BaseActivity {

    String TAG = getClass().getSimpleName();
    Fragment_BlockMember mFragment1_timeline;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_block_member);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        setRecyclerView();
        initView();
        setOnClickEvent();
    }

    private void setRecyclerView() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);


        mViewPager.setAdapter(mSectionsPagerAdapter);

        /*TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Log.e(TAG,"onPageSelected: "+position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });*/

    }

    private void setOnClickEvent() {
        findViewById(R.id.ll_tab1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
                /*findViewById(R.id.imgb_top1).setSelected(true);
                findViewById(R.id.imgb_top2).setSelected(false);*/
                findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
                findViewById(R.id.tab_bar2).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_SetBlockMember.this,R.color.bg_purple));
                ((TextView)findViewById(R.id.imgb_top2_txt)).setTextColor(Util.getColor(Activity_SetBlockMember.this,R.color.gray_9f));
            }
        });

        findViewById(R.id.ll_tab2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);

                findViewById(R.id.tab_bar1).setVisibility(View.INVISIBLE);
                findViewById(R.id.tab_bar2).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_SetBlockMember.this,R.color.gray_9f));
                ((TextView)findViewById(R.id.imgb_top2_txt)).setTextColor(Util.getColor(Activity_SetBlockMember.this,R.color.bg_purple));



            }
        });

    }

    private void initView() {
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.block_member));

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);


        ((TextView)findViewById(R.id.imgb_top1_txt)).setTextColor(Util.getColor(Activity_SetBlockMember.this,R.color.bg_purple));
        findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
        findViewById(R.id.tab_bar2).setVisibility(View.INVISIBLE);

    }

    public void refresh() {
        mFragment1_timeline.refresh();
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            mFragment1_timeline = Fragment_BlockMember.newInstance(position+"");
            return mFragment1_timeline;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "멤버 목록";
                case 1:
                    return "차단 목록";

            }
            return null;
        }
    }
}
