package com.wave.messenger.activity;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.utility.LogTrace;

import static com.wave.massenger.piggy.R.id.layout_formView;

@Deprecated
public class Activity_Stock extends Activity {
    static Activity_Stock instance = null;
    public static Activity_Stock getInstance() {
        return instance;
    }

    public static void clearInstance(Activity_Stock value) {
        if(value == instance) {
            instance = null;
        }
    }

    private View rootView;
    ImageView imageViewClose;
    View mtsView, mtsView2;
    int mtsWidth, mtsHeight, mDeviceScreenWidth, mDeviceScreenHeight;
    LinearLayout formView, ll_close;
    Display display;
    Animation startAnimation, closeAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        try {
            startAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right);
            closeAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_slide_out_right);
            instance = this;
            rootView = getLayoutInflater().from(this).inflate(R.layout.activity_slide_menu_main, null);
            formView = (LinearLayout) rootView.findViewById(layout_formView);
            formView.startAnimation(startAnimation);
            ll_close = (LinearLayout) rootView.findViewById(R.id.ll_close);

            mtsView = Fragment_Main.getInstance().mtsView;
            mtsView2 = Fragment_Main.getInstance().mtsView2;

            display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            mtsWidth = Fragment_Main.getInstance().mtsWidth;
            mDeviceScreenWidth = display.getWidth() - mtsWidth;
            mtsHeight = Fragment_Main.getInstance().mtsHeight;
            mDeviceScreenHeight = display.getHeight() - mtsHeight;

            formView.addView(mtsView, mtsWidth, mtsHeight);
            rootView.requestLayout();
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(rootView);

            String psOpenScreenLeft = getIntent().getStringExtra("psOpenScreenLeft");

            String arr[] ;
            arr = psOpenScreenLeft.split(";");
            String sItemCode = arr[0];
            String sScreenNo = arr[1];

            if (arr.length == 4) {                  //화면 번호 1000, 6000번을 호출할때
                String sCode = arr[2] + ";" +  arr[3];
                setDisplay(sScreenNo, sItemCode, sCode);
            } else {                               //그외 지수 화면으로 변경
                setDisplay(sScreenNo, "", "");
            }

        } catch (Exception e) {
            LogTrace.E("FragmentSlideMenu_Main ERROR : " + e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearInstance(instance);
        formView.startAnimation(closeAnimation);
        formView.removeAllViews();
    }

    private Activity mCallbacks;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void setDisplay(String screen, String itemCode, String psCode) {
    }

    public void setUp(View parent, int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = parent.findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ActionBar actionBar = getActionBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(true);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            actionBar.hide();
        }
    }

    public void setEvent() {
        ll_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            LogTrace.E("Touch");

            int x = (int)event.getX();
            int y = (int)event.getY();

            Bitmap bitmapScreen = Bitmap.createBitmap(mDeviceScreenWidth, mDeviceScreenHeight, Bitmap.Config.ARGB_8888);

            if(x < 0 || y < 0)
                return false;

            int ARGB = bitmapScreen.getPixel(x, y);

            int mColor = com.wave.messenger.util.Util.getColor(this, R.color.stock_color);

            if(Color.alpha(ARGB) == mColor) {
                finish();
            }
            return true;
       }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ActionBar getActionBar() {
        return this.getActionBar();
    }

    public void openMenu() {
        if(isDrawerOpen()) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        } else {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
    }
}
