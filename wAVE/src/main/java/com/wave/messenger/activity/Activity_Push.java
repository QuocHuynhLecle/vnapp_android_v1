package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.wave.messenger.share.HMMessengerShare;

import org.json.JSONObject;

public class Activity_Push extends FragmentActivity {

    static Activity_Push instaince = null;
    public static Activity_Push getInstance() {
        return instaince;
    }

    public static void clearInstance(Activity_Push value) {
        if(value.equals(instaince)) {
            instaince = null;
        }
    }

    private Bundle mBundle;
    private String pushData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            instaince = this;
            Intent mIntent = getIntent();
            mBundle = mIntent.getExtras();
            savePushMessage(mBundle);

            Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
            intent.putExtra("pushmsg", pushData);
            startActivity(intent);
        } catch(Exception e) {

        } finally {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        clearInstance(instaince);
    }

    private void savePushMessage(Bundle bundle) {
        String type = bundle.getString("type");
        String roomId = bundle.getString("roomId");
        String cId = bundle.getString("cId");
        String chatId = bundle.getString("chatId");
        String senderId = bundle.getString("senderId");
        String senderName = bundle.getString("senderName");
        String regDate = bundle.getString("regDate");
        String title = bundle.getString("title");
        String unread = bundle.getString("unread");
        String useNoti = bundle.getString("useNoti");

        JSONObject object = new JSONObject();

        try {
            object.put("type", type);
            object.put("roomId", roomId);
            object.put("cId", cId);
            object.put("chatId", chatId);
            object.put("senderId", senderId);
            object.put("senderName", senderName);
            object.put("regDate", regDate);
            object.put("title", title);
            object.put("unread", unread);
            object.put("useNoti", useNoti);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HMMessengerShare hMMessengerShare = HMMessengerShare.getInstance();
        hMMessengerShare.gcmMessage(object, this);

        pushData = object.toString();
    }
}
