package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

/**
 * Created by aveapp on 2017. 8. 7..
 */

public class Activity_Notice extends BaseActivity {

    private WebView webView;
    private WebSettings webSettings;
    String url;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_notice);

        ((RelativeLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        webView = (WebView) findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient());

        if ("notice".equals(getIntent().getStringExtra("type"))) {
            url = Constants.NOTICE_URL;
            ((TextView) findViewById(R.id.tv_noticetitle)).setText("공지사항");
        } else {
            url = Constants.HELP_URL;
            ((TextView) findViewById(R.id.tv_noticetitle)).setText("도움말");
        }
        webView.loadUrl(url);
        /**
         *  더보기 - 도움말
         */
//        webView.loadUrl("http://moa.aveapp.com:8680/candleman_help/NoticeView.jsp");
        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

//        http://moa.aveapp.com:8681/candleman_notice/NoticeView.jsp
    }
}
