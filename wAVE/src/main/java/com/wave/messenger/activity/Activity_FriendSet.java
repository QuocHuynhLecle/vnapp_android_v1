package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by aveapp on 2017. 8. 14..
 */

public class Activity_FriendSet extends BaseActivity implements View.OnClickListener {
    private TextView tv_refresh_date;
    private ImageView iv_refresh;
    private RelativeLayout ll_back;
    private LinearLayout ll_hide_set, ll_block_set;
    private String current_date;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_friendset);

        init();
    }

    private void init() {
        tv_refresh_date = (TextView) findViewById(R.id.tv_refresh_date);
        iv_refresh = (ImageView) findViewById(R.id.iv_refresh);
        ll_back = (RelativeLayout) findViewById(R.id.ll_back);
        ll_hide_set = (LinearLayout) findViewById(R.id.ll_hide_set);
        ll_block_set = (LinearLayout) findViewById(R.id.ll_block_set);

        iv_refresh.setOnClickListener(this);
        ll_back.setOnClickListener(this);
        ll_hide_set.setOnClickListener(this);
        ll_block_set.setOnClickListener(this);

        tv_refresh_date.setText(SharedObject.getProperty_string(Activity_FriendSet.this, Constants.refresh_date, ""));

        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat sdf = new SimpleDateFormat("MM월 dd일 a hh:mm");

        current_date = "최종 추가시간 " + sdf.format(date);
    }

    @Override
    public void onClick(View v) {

        int i1 = v.getId();
        if (i1 == R.id.iv_refresh) {
            Fragment_Main.getInstance().FriendListRequest();
            tv_refresh_date.setText(current_date);
            SharedObject.setProperty_string(Activity_FriendSet.this, Constants.refresh_date, current_date);

        } else if (i1 == R.id.ll_back) {
            finish();

        } else if (i1 == R.id.ll_hide_set) {
            Intent i = new Intent(Activity_FriendSet.this, Activity_HideFriend.class);
            i.putExtra("type", "0");
            startActivity(i);

        } else if (i1 == R.id.ll_block_set) {
            Intent intent = new Intent(Activity_FriendSet.this, Activity_HideFriend.class);
            intent.putExtra("type", "1");
            startActivity(intent);

        }
    }
}
