package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.SharedObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 10..
 */

public class Activity_TalkProfile_Set extends BaseActivity implements View.OnClickListener {

    private String path;
    private MOAData moaData;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_talkprofile_set);

        ((LinearLayout) findViewById(R.id.ll_change_photo)).setOnClickListener(this);

//        String date = moaData.body.getJson("params").getString("photoData");
//
//        LogTrace.E("photoData : " + date);
//
//        getProfile(date);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ll_change_photo) {
            Dialog_Camera.getProfileEditCameraDialog(Activity_TalkProfile_Set.this).show();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ErrorController.showMessage("onActivityResult, FragmentProfile started");
        Log.i("TEST", "requestCode : " + requestCode + "resultCode : " + resultCode + "data : " + data);

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            startCropImageActivity(data.getData());
        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {
            startCropImageActivity(data.getData());
        }

        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    //TODO
                    ((ImageView)findViewById(R.id.iv_profile)).setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //TODO 파일 업로드
                profileUpload(this, resultUri.getPath());



            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE "+resultCode);
            }

        }

    }

    public void onReceivePictureSuccess(Uri path) {

        LogTrace.E("crop path : " + path.toString());

        if (!getExternalCacheDir().exists())
            getExternalCacheDir().mkdirs();

        File temp = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));

        File destFile = new File(temp, time + ".jpg");
        this.path = destFile.getAbsolutePath();
        Uri destination = Uri.fromFile(destFile);
        //Crop.of(path, destination).asSquare().start(this);
    }


    public void profileUpload(final Context context, final String path) {

        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                // TODO Auto-generated method stub
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Constants.PROFILEUPLOAD_PROC);
                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
//                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, MessengerInfo.getUserId(Activity_TalkProfile_Set.this), "")));
//                    currentFileSize = 0;
                    file = new File(path);
//                    maxFileSize = (int) file.length();
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        LogTrace.E(jsonObject.getString("result"));

                        if (jsonObject.getString("result").equals("success")) {
                            onUploadComplete(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        onUploadComplete(false);
                    }
                } else {
                    onUploadComplete(false);
                }
            }
        }.execute();
    }

    public void onUploadComplete(boolean isSuccess) {
        if (isSuccess) {
            String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));

            try {
                PacketBody body = new PacketBody();
                JSONObject value = new JSONObject();
                value.put("userId", MessengerInfo.getUserId(this));
                value.put("photoType", "1");
                value.put("photoData", time);
                body.put("params", value);
                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_PHOTO, body);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, R.string.profile_image_fail, Toast.LENGTH_SHORT).show();
//            isChange = false;
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .start(this);
    }
    public void getProfile(String date) {
        final String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + MessengerInfo.getUserId(this) + "&date=" + date;

        LogTrace.E("photoUrl : " + url);

        MessengerInfo.setProfileImage(this, date);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(Activity_TalkProfile_Set.this).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_list_friend_profile).dontAnimate().into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        LogTrace.E("load success");

                        ((ImageView) findViewById(R.id.iv_profile)).setImageDrawable(drawable);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);

                        LogTrace.E("load fail");
                    }
                });
            }
        });

    }



}
