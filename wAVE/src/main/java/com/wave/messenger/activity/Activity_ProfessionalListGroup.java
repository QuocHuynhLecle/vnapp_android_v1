package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.ProfessionalListViewAdapter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMyprofessional;

import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_ProfessionalListGroup extends BaseActivity implements MOAEventListener, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {

    static public Activity_ProfessionalListGroup activity;

    String TAG=this.getClass().getName();
    ProfessionalListViewAdapter professionalListViewAdapter;
    private ExpandableListView expandableListView;
    private ArrayList<VoMyprofessional> childList = new ArrayList<>();
    private String grpId, grpNm;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_professionallist);

        activity=this;
        Intent mIntent = getIntent();
        grpId = mIntent.getStringExtra("groupId");
        grpNm = mIntent.getStringExtra("groupNm");
        initView();
        setOnClickEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"onResume");
//        ProListRequest();
    }

    @Override
    public void OnEvent(MOAData moaData) {

        LogTrace.E("MOADATA OnEvent");
        mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    private void initView() {
        ((TextView)findViewById(R.id.textViewTitle)).setText(MessengerInfo.getUserName(Activity_ProfessionalListGroup.this) + Constants.PROFESSIONAL_TITLE);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        ProListRequest(grpId);
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnChildClickListener(this);
    }

    /**
     리스트 설정
     */
    public void setListView(ArrayList<String> groupList, ArrayList<VoMyprofessional> items) {
        childList = items;
        professionalListViewAdapter = new ProfessionalListViewAdapter(this, groupList, items);
        expandableListView.setAdapter(professionalListViewAdapter);
        for (int i = 0; i < professionalListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        professionalListViewAdapter.notifyDataSetChanged();
    }

    public void setProListData(MOAData data) {
        LBJSONArray array = data.body.getArray("params");
        ArrayList<String> groupList = new ArrayList<>();
        ArrayList<String> groupitems = new ArrayList<>();
        ArrayList<VoMyprofessional> voProList = new ArrayList<>();
        ArrayList<VoMyprofessional> items = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            LBJSONObject jo = (LBJSONObject) array.get(i);
            VoMyprofessional item = VoMyprofessional.loadFromJsonServerResponse(jo);
            voProList.add(item);
        }

        //추천 전문가 집단
        VoMyprofessional mnData = new VoMyprofessional();
        ArrayList<VoMyprofessional> tempList = new ArrayList<>();

        for (int i = 0; i < voProList.size(); i++) {
            if (grpId.equals(voProList.get(i).getXprtGrpId())) {
                tempList.add(voProList.get(i));
            }
        }

        if (tempList.size() > 0) {
            mnData.setProList(tempList);
            mnData.setUserName(grpNm);
        }

        if (tempList.size() > 0)
            items.add(mnData);

        if (tempList.size() > 0)
            groupitems.add(grpNm);

        setListView(groupitems, items);
    }

    /**
     * 전문가 목록 요청
     */
    public void ProListRequest(String grpId) {
        try {
            if ("".equals(grpId)) grpId = "0";
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            value.put("xprtGrpId", grpId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_EXPERT_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void proChatRequest(String roomid) {
        Moa.chatRoomInfoRequest(this, roomid, mMainActivityHandler);
    }

    private VoChatList getRoomInfo(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("users");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }
        return info;
    }

    private void intentChat() {
        Intent intent = new Intent(this, Activity_ChatRoom.class);
        startActivity(intent);
    }

    //결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_EXPERT_LIST :    //전문가 리스트
                        MOALog.i("PTC_IMS_MOIM_EXPERT_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            LogTrace.E("params: " + data.body.get("result").toString());
                            setProListData(data);
                        }
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_INFO:
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            Statics.ROOMINFO=getRoomInfo(data);
                            intentChat();
                        }
                        break;
                    case PacketTypes.PTC_IMS_MOIM_APPLY:    //모임 가입
                        if(data.body.get("result").equals(Constants.SUCCESS)){

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("apply")) {
                                Toast.makeText(Activity_ProfessionalListGroup.this, "가입신청을 완료하였습니다.", Toast.LENGTH_SHORT).show();
                            } else {
                                //완료 표시
                            }

                        } else {

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("already_applied")) {
                                Toast.makeText(Activity_ProfessionalListGroup.this, "이미 가입신청함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_exist")) {
                                Toast.makeText(Activity_ProfessionalListGroup.this, "이미 가입함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_blocked")) {
                                Toast.makeText(Activity_ProfessionalListGroup.this, "차단됨", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("wrong_moim_password")) {
                                Toast.makeText(Activity_ProfessionalListGroup.this, "비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        VoMyprofessional item = childList.get(groupPosition).getProList().get(childPosition);
        switch (item.getmStt()) {
            case 0:
                Toast.makeText(Activity_ProfessionalListGroup.this, "가입 후 대화방 입장이 가능합니다", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                proChatRequest(item.getRmId());
                break;
            case 2:
                Toast.makeText(Activity_ProfessionalListGroup.this, "가입이 차단 되었습니다", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(Activity_ProfessionalListGroup.this, "가입대기 중 입니다", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }
}
