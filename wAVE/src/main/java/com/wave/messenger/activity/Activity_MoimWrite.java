package com.wave.messenger.activity;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.messenger.Emoticon.Emoticon_Activity;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.dialog.Dialog_WriteDelete;
import com.wave.messenger.dialog.Dialog_WriteSelect;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.util.AndroidLayoutResize;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.FileUtil;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMsgItem;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 글쓰기
 * 모임타임라인 - 글쓰기 버튼
 * Activity_MoimTimeLineList - Activity_TimeLineDetail
 * 글수정
 */
public class Activity_MoimWrite extends BaseActivity {

    String TAG = this.getClass().getName();
    String mAtch;   //첨부 데이터 파일
    HashMap<String, String> mimgHash = new HashMap<>();   //이미지의 getView값과 이미지패스를 저장
    HashMap<String, String> mFileHash = new HashMap<>();   //파일의 getView값과 파일패스를 저장
    HashMap<String, String> mISEmoticonHash = new HashMap<>();  //이모티콘인지 구분하기위한 값 저장.

    Constants.UPLOAD_TYPE UPLOAD_TYPE;  //이미지, 파일 구분

    public static Context context;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_write);
        context = this;

        //글수정 본문내용처리   공유,글수정,글쓰기 구
        String strWriteType = SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_WRITE);

        initView(strWriteType);
        setOnClickEvent(strWriteType);

        //Log.e(TAG,"onCreate strWriteType: "+strWriteType);


        if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_WRITE) || strWriteType.equals(Constants.MOIM_WRITE_TYPE_MAIN_WRITE)) { //모임 타임라인 글쓰기, 메인에서 모임선택해서 글쓰기.

            //저장된글 불러오기
            String savedContent = SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, "");
            if (TextUtils.isEmpty(savedContent)) {
                //아무것도 할필요없음. 그냥 글쓰기

                EditText edtxt = new EditText(Activity_MoimWrite.this);
                edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
                edtxt.setBackgroundColor(Color.TRANSPARENT);
                edtxt.setPadding(0, 10, 10, 10);
                edtxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                edtxt.setHint(getString(R.string.write_info));
                edtxt.setHintTextColor(Util.getColor(this, R.color.gray_6c));

                mllayout.addView(edtxt);

                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


            } else {
                setContent(savedContent);
                SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, "");
            }


        }else{ //MOIM_WRITE_TYPE_MODIFY 수정 , MOIM_WRITE_TYPE_SHARE 공유 일경우.
            setContent(SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.MOIM_MSG, ""));

            //메시지아이디를 수정
            SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.MOIM_MODIFY_MSGID, SharedObject.getProperty_string(context, Constants.MOIM_MSGID, "")  );  //메시지 키값
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        AndroidLayoutResize.assistActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if (findViewById(R.id.imgb_emoticon).isSelected()) {
            ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
            findViewById(R.id.imgb_emoticon).setSelected(false);
        } else {


            if(SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_WRITE).equals(Constants.MOIM_WRITE_TYPE_WRITE)){
                writeOutEvent();
            }else{
                setDialogModifyOut();
            }
        }
    }


    /**
     * 글수정 나가기 다이얼로그
     * 수정시 뒤로가기
     * 글 수정을 취소하시겠습니까? 아니오 / 예
     */
    private void setDialogModifyOut() {
        final Dialog_Text dialog = new Dialog_Text(Activity_MoimWrite.this
                , getString(R.string.moim_modify_out_text)
                , Constants.DIALOG_BUTTON_TYPE.modify_out);

        dialog.setCancleListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //아니요

            }
        });

        dialog.setListener(new View.OnClickListener() { //예 : 나가기
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * 글쓰기 나가기 이벤트
     * 내용이 없으면 화면 닫고,
     * 내용있으면 나가기 팝업을 띄운다.
     */
    private void writeOutEvent() {

        Util.hideKeyboard(Activity_MoimWrite.this,(EditText) findViewById(R.id.edt_write));

        if(isContentNull()){
            finish();
        }else{
            //내용이 없으면 나가시겠습니까 팝업
            setDialogWriteOut();
        }
    }

    /**
     * 나가기 팝업
     */
    private void setDialogWriteOut() {

        final Dialog_Text dialog = new Dialog_Text(Activity_MoimWrite.this
                , getString(R.string.moim_write_out_text)
                , Constants.DIALOG_BUTTON_TYPE.write_out);

        dialog.setCancleListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //나가기
                //현재 글 저장 pref
                String contentText=getJsonArrContent();
                SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, contentText);

                finish();
            }
        });

        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * 이모티콘을 넣는다.
     * @param emoticon
     */
    public void setEmoticon(String emoticon) {
        //this.emoticon_num = String.valueOf(emoticon);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), getDrawableResourceByName(emoticon));

        int nViewId=getRamdomId();
        addImageview(bm, "emoticon",nViewId);

        mimgHash.put(nViewId + "", emoticon);
        mISEmoticonHash.put(nViewId+"","emoticon");
        //emoticon_num=null;

    }



    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(this,str);
        String packageName = getPackageName();
        return getResources().getIdentifier(emoticon, "drawable", packageName);
    }


    private void setOnClickEvent(final String strWriteType) {
        //배경터치시 가상키보드 올림
        findViewById(R.id.scroll_view).setOnTouchListener(new View.OnTouchListener() {
           @Override
           public boolean onTouch(View view, MotionEvent motionEvent) {
                Util.showSoftKeyboard(Activity_MoimWrite.this, findViewById(R.id.edt_write));
            return false;
            }
        });



        ((EditText) findViewById(R.id.edt_write)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //2자이상 체크 이미지추가하며 조건 수정...
                /* if(count >2){
                     findViewById(R.id.imgb_right).setEnabled(true);
                }else{
                     findViewById(R.id.imgb_right).setEnabled(false);
                 }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ((EditText) findViewById(R.id.edt_write)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
            }
        });
        ((EditText) findViewById(R.id.edt_write)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                emoticon_gone();
            }
        });

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_WRITE).equals(Constants.MOIM_WRITE_TYPE_WRITE)){
                    writeOutEvent();
                }else{
                    setDialogModifyOut();
                }

            }
        });

        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isContentNull()){
                    Toast.makeText(Activity_MoimWrite.this, "입력된 내용이 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                emoticon_gone();
                Util.forceHideKeyboard(Activity_MoimWrite.this);


                if (strWriteType.equals(Constants.MOIM_WRITE_TYPE_SHARE)) {    //공유

                    //Toast.makeText(getApplicationContext(), "글공유 완료", Toast.LENGTH_SHORT).show();
                    openDialog_WriteSelect();

                }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_MODIFY)){  //수정하기

                    //글수정
                    Toast.makeText(getApplicationContext(), "글수정 완료", Toast.LENGTH_SHORT).show();

                    moimBoardUpdateRequest(getTTl(), getJsonArrContent(), getNotiValue());
                    SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, "");

                }else { //  MOIM_WRITE_TYPE_WRITE 글쓰기, MOIM_WRITE_TYPE_MAIN_WRITE 메인에서 모임선택후 글쓰기

                    openDialog_WriteSelect();
                }

               /* if (!SharedObject.getProperty_boolean(Activity_MoimWrite.this, Constants.MOIM_WRITE_TYPE, true)) {    //true:글쓰기 , false:글수정
                    //글수정
                    Toast.makeText(getApplicationContext(), "글수정 완료", Toast.LENGTH_SHORT).show();


                    //moimWriteRequest(lines[0], getJsonArrContent());
                    moimBoardUpdateRequest(getTTl(), getJsonArrContent(), getNotiValue());
                    SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, "");

                } else {    //글쓰기 완료버튼

                }*/
            }
        });

        findViewById(R.id.imgb_emoticon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ErrorController.showMessage("clicked Emotion");
                View view = Activity_MoimWrite.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) Activity_MoimWrite.this.getSystemService(Activity_MoimWrite.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                findViewById(R.id.imgb_emoticon).setSelected(!findViewById(R.id.imgb_emoticon).isSelected());
//                checkSendBtn();

                if (findViewById(R.id.imgb_emoticon).isSelected()) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.flEmoticonContainer, new Emoticon_Activity(0, Activity_MoimWrite.this));
                    fragmentTransaction.commit();
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.VISIBLE);


                } else {
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
                }

            }
        });

        //사진추가
        findViewById(R.id.imgb_picture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
                UPLOAD_TYPE = Constants.UPLOAD_TYPE.image;

                Toast.makeText(getApplicationContext(), "사진", Toast.LENGTH_SHORT).show();

                Util.hideKeyboard(Activity_MoimWrite.this, (EditText) findViewById(R.id.edt_write));

                Dialog_Camera.getMoimWriteCameraDialog(Activity_MoimWrite.this).show();
            }
        });

        //파일추가
        findViewById(R.id.imgb_attachment).setOnClickListener(new View.OnClickListener() {  //파일첨부
            @Override
            public void onClick(View v) {
                emoticon_gone();
                UPLOAD_TYPE = Constants.UPLOAD_TYPE.file;

                //Toast.makeText(getApplicationContext(), "파일 첨부", Toast.LENGTH_SHORT).show();
                onBrowse();
                Util.hideKeyboard(Activity_MoimWrite.this, (EditText) findViewById(R.id.edt_write));

            }
        });


        findViewById(R.id.imgb_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
                Toast.makeText(getApplicationContext(), "설정", Toast.LENGTH_SHORT).show();
            }
        });


        //공지 등록 버튼 -권한체크
        findViewById(R.id.imgb_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon_gone();

                //상단 초대 버튼
                if (Util.getPermissionLv(Util.getMymLv(Activity_MoimWrite.this), Util.getNotiTy(Activity_MoimWrite.this))) {

                    if (findViewById(R.id.imgb_notification).isSelected()) {
                        findViewById(R.id.imgb_notification).setSelected(false);

                    } else {
                        findViewById(R.id.imgb_notification).setSelected(true);
                        findViewById(R.id.imgb_emergency).setSelected(false);
                    }

                } else {
                    //layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "공지 등록권한이 없습니다", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //긴급공지
        findViewById(R.id.imgb_emergency).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon_gone();

                //상단 초대 버튼
                if (Util.getPermissionLv(Util.getMymLv(Activity_MoimWrite.this), Util.getNotiTy(Activity_MoimWrite.this))) {

                    if (findViewById(R.id.imgb_emergency).isSelected()) {
                        findViewById(R.id.imgb_emergency).setSelected(false);

                    } else {
                        findViewById(R.id.imgb_emergency).setSelected(true);
                        findViewById(R.id.imgb_notification).setSelected(false);

                    }

                } else {
                    //layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "긴급공지 등록 권한이 없습니다", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    /**
     * 글게시 항목 선택 팝업을 오픈
     */
    private void openDialog_WriteSelect() {
        //글게시 항목선택
        final Dialog_WriteSelect dialog_WriteSelect = new Dialog_WriteSelect(Activity_MoimWrite.this);
        dialog_WriteSelect.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();

                if(id==R.id.tv_cancle){
                    dialog_WriteSelect.dismiss();
                }else if(id==R.id.tv_confirm){
                    Log.e(TAG,"dialog_WriteSelect.getSelect(): "+dialog_WriteSelect.getSelect());

                    int nSelect=dialog_WriteSelect.getSelect();

                    //String strWrite = ((EditText) findViewById(R.id.edt_write)).getText().toString().trim();
                    //final String[] lines = strWrite.split("\n");

                    String strPrvTy="0";     //비공개 여부 0:공개, 1:비공개 종목대화방에 게시인 경우 0:공개로 설정

                    switch (nSelect){
                        case 0: //내모임에 게시
                            strPrvTy="0";
                            moimWriteRequest(getTTl(), getJsonArrContent(),strPrvTy,"","");
                            break;

                        case 1: //비공개로 내모임에서만 게시
                            strPrvTy="1";
                            moimWriteRequest(getTTl(), getJsonArrContent(),strPrvTy,"","");
                            break;
                    }

                    dialog_WriteSelect.dismiss();
                }

            }
        });
        dialog_WriteSelect.show();

    }




    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        //lvChatListView.requestFocus();
    }

    private void emoticon_gone() {
        findViewById(R.id.imgb_emoticon).setSelected(false);
        ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
    }

    /**
     * ttl 타이틀 값을 가져온다
     */
    private String getTTl() {
        //String[] lines = new String[0];

        String ttl = null;
        //String returnTitle = null;
        for (int i = 0; i < mllayout.getChildCount(); i++) {
            Log.e(TAG, "llLayout.getChildCount(), " + mllayout.getChildCount());

            if (mllayout.getChildAt(i) instanceof EditText) {
                ttl = ((EditText) mllayout.getChildAt(i)).getText().toString().trim();

                if (!TextUtils.isEmpty(ttl)) {

                    if(ttl.length()>85){
                        //String retTtl=ttl.substring(0,84);
                        return ttl.substring(0,84);
                    }else{
                        return ttl;
                    }

                }

            }
        }
        return ttl;
    }


    private void initView(String strWriteType) {

        if (strWriteType.equals(Constants.MOIM_WRITE_TYPE_SHARE)) {
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_share));

        }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_MODIFY)){
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_modify));

        }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_WRITE)){
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_write));

        }



        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
        findViewById(R.id.tv_topbar_name).setVisibility(View.GONE);
        findViewById(R.id.imgb_right).setVisibility(View.VISIBLE);

        //((ImageButton)findViewById(R.id.imgb_back_btn)).setBackgroundResource(R.drawable.selector_close);
        ((ImageButton) findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.ic_close_press);

        mllayout = (LinearLayout) findViewById(R.id.ll_write);


        Util.showSoftKeyboard(this, findViewById(R.id.edt_write));

        //((ImageButton)findViewById(R.id.imgb_back_btn)
        //((ImageButton)findViewById(R.id.imgb_right)
    }


    /**
     * 글수정
     * 텍스트와  이미지 넣기
     * 파일첨부 - 삭제 기능만.
     */
    private void setContent(String msg) {
        Log.e(TAG, "setContent: " + msg);

        LinearLayout llLayout = (LinearLayout) findViewById(R.id.ll_write);


        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(msg);

       //int  mAddImageCount = 0;
       int  mAddFileCount = 0;

        int nRandomId;

        for (VoMsgItem item : arMsgItem) {

            nRandomId=getRamdomId();

            switch (item.getType()) {
                case "0": //text
                    EditText edt_text = new EditText(Activity_MoimWrite.this);
                    edt_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    edt_text.setText(item.getData());
                    // edt_text.setTextColor(getResources().getColor(R.color.black));
                    //edt_text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                    edt_text.setTextSize(SharedObject.getProperty_int(Activity_MoimWrite.this, Constants.FONTSIZE, 16));
                    edt_text.setTextColor(Util.getColor(this, R.color.gray_6c));
                    edt_text.setBackgroundColor(Color.TRANSPARENT);
                    edt_text.setId(nRandomId);
                    llLayout.addView(edt_text);
                    break;

                case "1":  //image
                    final ImageView imgview = new ImageView(Activity_MoimWrite.this);
                    imgview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    //이미지 삭제
                    imgview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimWrite.this);
                            dialog_WriteDelete.show();

                            dialog_WriteDelete.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_WriteDelete.dismiss();
                                    int i = v.getId();
                                    if (i == R.id.tv_option0) {
                                        Toast.makeText(Activity_MoimWrite.this, "이미지 삭제", Toast.LENGTH_SHORT).show();
                                        mllayout.removeView(imgview);
                                    }
                                }
                            });
                        }
                    });

                    imgview.setId(nRandomId);


                    mimgHash.put(nRandomId + "", item.getData()+"");
                    mISEmoticonHash.put(nRandomId + "", "image");



                    Glide.with(Activity_MoimWrite.this).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData()).diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)
                            .dontAnimate()
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                   Bitmap bmap = ((GlideBitmapDrawable) resource).getBitmap();
                                    imgview.setLayoutParams(new LinearLayout.LayoutParams(bmap.getWidth(), bmap.getHeight()));
                                    imgview.setImageBitmap(bmap);
                                }
                            });

                    llLayout.addView(imgview);

                    //mAddImageCount++;
                    break;


                case "3": //파일첨부  //TODO 기능 없음
                    final View view = (View) getLayoutInflater().inflate(R.layout.inflate_fileupload, null);

                    TextView tv = (TextView) view.findViewById(R.id.tv_filename);
                    tv.setText(item.getData());

                    //파일 삭제 다이얼로그
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimWrite.this);
                            dialog_WriteDelete.show();

                            dialog_WriteDelete.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_WriteDelete.dismiss();
                                    int i = v.getId();
                                    if (i == R.id.tv_option0) {
                                        Toast.makeText(Activity_MoimWrite.this, " 삭제", Toast.LENGTH_SHORT).show();
                                        mllayout.removeView(view);
                                    }
                                }
                            });
                        }
                    });

                    mFileHash.put(mAddFileCount + "", item.getData()+"");
                    view.setId(mAddFileCount);
                    mAddFileCount++;
                    view.setPadding(0, 20, 0, 0);
                    llLayout.addView(view);

                    break;

                case "4":   //이모티콘
                    final ImageView imgvEmoticon = new ImageView(Activity_MoimWrite.this);
                    imgvEmoticon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    //이모티콘 삭제
                    imgvEmoticon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimWrite.this);
                            dialog_WriteDelete.show();

                            dialog_WriteDelete.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_WriteDelete.dismiss();
                                    int i = v.getId();
                                    if (i == R.id.tv_option0) {
                                        Toast.makeText(Activity_MoimWrite.this, "이모티콘 삭제", Toast.LENGTH_SHORT).show();
                                        mllayout.removeView(imgvEmoticon);
                                    }
                                }
                            });
                        }
                    });

                    Bitmap bm = BitmapFactory.decodeResource(getResources(), getDrawableResourceByName(item.getData()));
                    //emoticon.setImageResource(Integer.parseInt(item.getData()));

                    //mISEmoticonHash.
                    imgvEmoticon.setImageBitmap(bm);
                    imgvEmoticon.setId(nRandomId);

                    llLayout.addView(imgvEmoticon);

                    mimgHash.put(nRandomId + "", item.getData()+"");
                    mISEmoticonHash.put(nRandomId + "", "emoticon");  //이모티콘 구분


                   // mAddImageCount++;

                    break;


            }
        }

        //Log.e(TAG,"mllayout.getChildCount(): "+mllayout.getChildCount());

        if(mllayout.getChildAt(mllayout.getChildCount()-1) instanceof EditText){   //이모티콘다음 edittext없을때.
            //TODO 다음에 edittext 있을때
            //Log.e(TAG,"Edittext!!!");
        }else{
            //없으면 edittext 넣기
            final EditText edtxt = new EditText(Activity_MoimWrite.this);
            edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
            edtxt.setBackgroundColor(Color.TRANSPARENT);
            edtxt.setPadding(0, 10, 10, 10);
            edtxt.setFocusableInTouchMode(true);
            edtxt.setFocusable(true);
            edtxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
            //edtxt.setId(mAddImageCount); //TODO

            edtxt.setId(getRamdomId());

            edtxt.post(new Runnable(){
                @Override
                public void run(){
                    edtxt.requestFocus();
                } });

            mllayout.addView(edtxt,mllayout.getChildCount());
        }








        //TODO 마지막에 edtext없으면 넘어주기
        /*EditText edt_text = new EditText(Activity_MoimWrite.this);
        edt_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        //edt_text.setTextColor(getResources().getColor(R.color.black));
        edt_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        edt_text.setTextColor(Util.getColor(this, R.color.gray_6c));
        edt_text.setBackgroundColor(Color.TRANSPARENT);
        llLayout.addView(edt_text);*/


        //TODO 공지값 처리
        Log.e(TAG, "MOIM_NOTI : " + SharedObject.getProperty_string(this, Constants.MOIM_NOTI, "0"));
        switch (SharedObject.getProperty_string(this, Constants.MOIM_NOTI, "0")) {
            case "0":
                break;
            case "1":
                findViewById(R.id.imgb_notification).setSelected(true);
                break;
            case "2":
                findViewById(R.id.imgb_emergency).setSelected(true);
                break;

        }


    }


    /**
     * 모임리스트 글쓰기
     * PTC_IMS_MOIM_BOARD_WRITE
     *
     * @param
     */
    private void moimWriteRequest(String title, String msg,String prvTy, String stCd, String stNm) {
        MOALog.w(TAG + " moimWriteRequest" + title + " message: " + msg);

        SharedObject.setProperty_string(Activity_MoimWrite.this, Constants.WRITE_SAVE_CONTENT, ""); //저장글 값없애기.
        Moa.moimBoardWriteRequest(Activity_MoimWrite.this, title, msg, "", "", "", "", getNotiValue(),prvTy,stCd, stNm, mMainActvtHandler);
    }


    /**
     * 공지,긴급공지 설정값을 가져온다.
     */
    private String getNotiValue() {
        String noti = "0";
        if (findViewById(R.id.imgb_notification).isSelected()) {
            noti = "1";
        } else if (findViewById(R.id.imgb_emergency).isSelected()) {
            noti = "2";
        }
        return noti;
    }


    /**
     * 7.24 모임 글 수정
     */
    private void moimBoardUpdateRequest(String ttl, String msg, String noti) {
        MOALog.w(TAG + " moimBoardUpdateRequest" + ttl + " message: " + msg);
        Moa.moimBoardUpdateRequest(Activity_MoimWrite.this, ttl, msg, noti, mMainActvtHandler);
    }


    /**
     * 사진촬영후 또는  겔러리 이미지 선택후 크
     *
     * @param imageUri
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setInitialCropWindowPaddingRatio(0)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("TEST", "requestCode : " + requestCode + "resultCode : " + resultCode + "data : " + data);

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_MoimOpen", "onActivityResult CAMERA_REQUEST");
            Uri uri;
//            startCropImageActivity(data.getData());
            if (data != null) {
                uri = data.getData();
                LogTrace.E("data not null");
            } else {
                LogTrace.E("data null");
                uri = Uri.fromFile(FileUtil.image);
            }
            if (uri != null) {
//                onReceivePictureSuccess(uri);
                startCropImageActivity(uri);
            } else {
//                onReceivePictureSuccess(Uri.fromFile(FileUtil.image));
                startCropImageActivity(Uri.fromFile(FileUtil.image));
            }
        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {

            Log.e("Activity_MoimOpen", "onActivityResult GALLERY_REQUEST");

            startCropImageActivity(data.getData());

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();

                int nRandomId=getRamdomId();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    //((ImageView) findViewById(R.id.imgv_type)).setImageBitmap(bitmap);

                    //TODO

                    addImageview(bitmap, "",nRandomId);
                    //findViewById(R.id.imgv_type).setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //TODO 파일 업로드
                profileUpload(Activity_MoimWrite.this, resultUri.getPath(),nRandomId);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE " + resultCode);
            }

        }

        //파일 첨부
        switch (requestCode) {
            case Constants.ACTIVITY_CHOOSE_FILE:
                Uri uri = data.getData();
                //profileUpload(Activity_MoimWrite.this, resultUri.getPath());
                //String filePath = getRealPathFromURI(uri);


                Uri selectedImageUri = data.getData();

//                String filemanagerstring = selectedImageUri.getPath();


                String filepath = getRealPathFromURI(selectedImageUri);
                String filepath2 = getPath(this, selectedImageUri);
                Log.e(TAG, "FilePath3 : " + filepath);
                String selectedImagePath;
                //MEDIA GALLERY
                //selectedImagePath = getPath(selectedImageUri);
                //Toast.makeText(this.getApplicationContext(), selectedImagePath, Toast.LENGTH_SHORT).show();
                //change imageView1
                //imageView1.setImageURI(selectedImageUri);
                int nFileRandomid = getRamdomId();

                addFileView(filepath2);

                profileUpload(Activity_MoimWrite.this, filepath2, nFileRandomid);

                break;
        }
    }

    /**
     * 파일 선택
     */
    public void onBrowse() {

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, Constants.ACTIVITY_CHOOSE_FILE);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, Constants.ACTIVITY_CHOOSE_FILE);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }

    /*public String getPath(Uri uri) {

        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if(cursor == null){
            path = uri.getPath();
        }
        else{
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }*/

    /*public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }*/

//    /**
//     * UR에서 파일패스가져오기
//     *
//     * @param contentUri
//     * @return
//     */
//    public String getRealPathFromURI(Uri contentUri) {
//        String[] proj = {MediaStore.Images.Media.DATA};
//        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
//        if (cursor == null) return null;
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }

    // 파일경로 테스트
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


//테스트끝


    private String getRealPathFromURI(Uri uri) {
        String filePath = "";
        filePath = uri.getPath();
//        if (filePath.startsWith("/storage"))
//            return filePath;

        Log.i("TEST", "file_path1 : " + filePath);

        String wholeID = DocumentsContract.getDocumentId(uri);


        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        Log.e(TAG, "id = " + id);

        String[] column = {MediaStore.Files.FileColumns.DATA};


        String sel = MediaStore.Files.FileColumns.DATA + " LIKE '%" + id + "%'";

        Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"),
                column, sel, null, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        Log.i("TEST", "file_path2 : " + filePath);
        cursor.close();
        return filePath;
    }


    LinearLayout mllayout;
    //int mAddImageCount = 0;   //추가됨 이미지 카운트
    int mAddFileCount = 0;



    /**
     * 글쓰기화면에
     * 겔러리,사진촬영으로 받아온 이미지를 넣고 하단에 Edittext를 추가한다
     *
     * @param bitmap 이미지, 이모티콘 이미지
     * @param emoticon 명
     * 사진과 이모티콘 같이 사용
     * @param emoticon
     */
    private void addImageview(Bitmap bitmap, String emoticon, int viewId) {  //TODO
        //mllayout.setPadding(50,50,50,50);
        emoticon_gone();

        mllayout.setOrientation(LinearLayout.VERTICAL);
        mllayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        int padding = getResources().getDimensionPixelOffset(R.dimen.dp_15);

        final ImageView imgview = new ImageView(Activity_MoimWrite.this);
        imgview.setImageBitmap(bitmap);
        imgview.setLayoutParams(new LinearLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
        imgview.setPadding(0, 10, padding, 10);
        imgview.setScaleType(ImageView.ScaleType.FIT_XY);


        /*mAddImageCount = 0;
        for (int x = 0; x < mllayout.getChildCount(); x++) {
            if (mllayout.getChildAt(x) instanceof ImageView) {
                mAddImageCount++;
            }
        }*/


        //imgview.setId(mAddImageCount);  //카운트를 세어서 고유아이디 지정.
        imgview.setId(viewId);


        //선택한 뷰의 아이디를 가져와서 그다음뷰가 있는지 체크
        //있으면 그 아이디뷰가 몇번째인지 가져와서 그 순서에 이모티콘 넣기



        //활성화된 뷰의 아이디 가져오기
        View cView = getCurrentFocus();
        Log.e(TAG,"addImageview focusedView: "+cView.getId()+" cView.getId()+1 "+ (cView.getId()+1));


        //int nGetCurrentNextid = getCurrentFocus().getNextFocusDownId();
        //Log.e(TAG,"a "+nGetCurrentNextid);

        int layoutChildCount=0;
        for (int x = 0; x < mllayout.getChildCount(); x++) {
            if (mllayout.getChildAt(x).getId()==cView.getId() ) {
                layoutChildCount=x;
            }
        }

        //Log.e(TAG,"count x: " +layoutChildCount);

        //활설화된 뷰의 다음에 이미지 addView 시킨다.
        mllayout.addView(imgview, layoutChildCount+1 );//아이디 번호대로가 아닌 아이디를 가진 뷰의 다음에 붙이기

        //다음 뷰에 Edittext가 없으면 Edittext추가.

       //Log.e(TAG,"mllayout.getChildCount(): "+mllayout.getChildCount());
        if(mllayout.getChildAt(layoutChildCount+2) instanceof EditText){   //이모티콘다음 edittext없을때.
            //다음에 edittext 있을때
            //Log.e(TAG,"Edittext!!!");
        }else{
            //없으면 edittext 넣기

            final EditText edtxt = new EditText(Activity_MoimWrite.this);
            edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
            edtxt.setBackgroundColor(Color.TRANSPARENT);
            edtxt.setPadding(0, 10, padding, 10);
            edtxt.setFocusableInTouchMode(true);
            edtxt.setFocusable(true);
            edtxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            edtxt.setId(getRamdomId());

            edtxt.post(new Runnable(){
                @Override
                public void run(){
                    edtxt.requestFocus();
                } });

            mllayout.addView(edtxt,layoutChildCount+2);
        }


        //이미지 삭제 다이얼로그
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimWrite.this);
                dialog_WriteDelete.show();

                dialog_WriteDelete.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_WriteDelete.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {
                            Toast.makeText(Activity_MoimWrite.this, "삭제", Toast.LENGTH_SHORT).show();
                            mllayout.removeView(imgview);
                            mimgHash.remove(imgview.getId()+"");
                        }
                    }
                });
            }
        });

    }


    /**
     * 임의 랜덤값 아이디값으로 활용
     * @return
     */
    private int getRamdomId() {
        Random rnd = new Random();
        return  rnd.nextInt(10000);
    }


    /**
     * 파일첨부 뷰추가
     *
     * @param filepath
     */
    private void addFileView(String filepath) {

        mllayout.setOrientation(LinearLayout.VERTICAL);
        mllayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        int padding = getResources().getDimensionPixelOffset(R.dimen.dp_15);

        final View view = (View) getLayoutInflater().inflate(R.layout.inflate_fileupload, null);

        TextView tv = (TextView) view.findViewById(R.id.tv_filename);

        tv.setText(filepath);

        //이미지 삭제 다이얼로그
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimWrite.this);
                dialog_WriteDelete.show();

                dialog_WriteDelete.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_WriteDelete.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {
                            Toast.makeText(Activity_MoimWrite.this, "삭제", Toast.LENGTH_SHORT).show();

                            mllayout.removeView(view);

                        }
                    }
                });
            }
        });
        mAddFileCount = 0;
        for (int x = 0; x < mllayout.getChildCount(); x++) {
            if (mllayout.getChildAt(x) instanceof View) {
                mAddFileCount++;
            }
        }


        view.setId(mAddFileCount);
        mllayout.addView(view);

        EditText edtxt = new EditText(Activity_MoimWrite.this);
        edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
        edtxt.setBackgroundColor(Color.TRANSPARENT);

        edtxt.setPadding(padding, 10, padding, 10);
        edtxt.requestFocus();
        //edtxt.setBackgroundColor(Color.RED);

        edtxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

        mllayout.addView(edtxt);

    }

    private void scrolltoDown() {
        final ScrollView scrollview = ((ScrollView) findViewById(R.id.scroll_view));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }


    /**
     *
     * 화면내의 모든 Edittext 뷰의 값이 널인지 체크한다.
     * @return
     */
    private boolean isNullEdittextContent() {

        for (int i = 0; i < mllayout.getChildCount(); i++) {
            View view = mllayout.getChildAt(i);
            if (view instanceof EditText) {

                if (TextUtils.isEmpty(((EditText) view).getText().toString().trim())) {

                }else{
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 글쓰기 완료 버튼시
     * msg 배열 만드는부분
     * 화면내 모든 뷰를 가져와서 Json형식으로 만든다.
     *
     */
    private String getJsonArrContent() {
        //Log.e(TAG, "getWriteContent mllayout.getChildCount()" + mllayout.getChildCount());

        JSONArray jsonArray = new JSONArray();

        JSONObject jObject;
        for (int i = 0; i < mllayout.getChildCount(); i++) {
            View view = mllayout.getChildAt(i);

            jObject = new JSONObject();

            /*try {
                jObject.put("idx",i);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            if (view instanceof EditText) {
                Log.e(TAG, "getWriteContent EditText " + ((EditText) view).getText().toString());


                //if (!TextUtils.isEmpty(((EditText) view).getText().toString())) {  //빈칸도 그대로 집어넣기.
                    try {
                        jObject.put("idx", i);
                        jObject.put("type", "0");
                        jObject.put("data", ((EditText) view).getText().toString());
                        jsonArray.put(jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                //}

            } else if (view instanceof ImageView) {
                Log.e(TAG, "getWriteContent instanceof " + view.getId() + " " + mimgHash.get(view.getId()+"") + " "+ mISEmoticonHash.get(view.getId()+""));



                    try {
                        jObject.put("idx", i);

                        if( mISEmoticonHash.get(view.getId()+"")=="image"){ //이미지
                            jObject.put("type", "1");
                        }else{
                            jObject.put("type", "4");       //이모티콘
                        }

                        jObject.put("data", mimgHash.get(view.getId() + ""));
                        jsonArray.put(jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



            } else if (view instanceof View) {     //TODO 파일 첨부
                Log.e(TAG, "getWriteContent View");
                try {
                    jObject.put("idx", i);
                    jObject.put("type", "3");
                    jObject.put("data", mFileHash.get(view.getId() + ""));
                    jsonArray.put(jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }



        /*for (Map.Entry<String,String> entry : mimgHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e(TAG,"getWriteContent mimgHash key: "+key +" value: " +value );
        }*/


        //Log.e(TAG, "getWriteContent jsonArray  " + jsonArray.toString());

        return jsonArray.toString();
    }


    //글쓰기 결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);


                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_BOARD_WRITE:    //글쓰기
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "PTC_IMS_MOIM_BOARD_WRITE strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Util.hideKeyboard(Activity_MoimWrite.this, (EditText) findViewById(R.id.edt_write));

                            Toast.makeText(Activity_MoimWrite.this, "글쓰기 완료", Toast.LENGTH_SHORT).show();


                            String strWriteType = SharedObject.getProperty_string(Activity_MoimWrite.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_WRITE);

                            if (strWriteType.equals(Constants.MOIM_WRITE_TYPE_SHARE)) { //다른모임으로 공유.
                                Activity_SelectMoim.activity.finish();
                                finish();

                            }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_MODIFY)){    //글수정.
                                setResult(RESULT_OK);
                                finish();

                            }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_MAIN_WRITE)){ ////매인에서 모임선택 글쓰기
                                //SharedObject.setProperty_boolean(Activity_MoimWrite.this, Constants.MOIM_WRITE_MAIN, false);



                                //모임선택 닫고
                                Activity_SelectMoim.activity.finish();
                                //해당 모임 타임라인으로 이동
                                Intent intent = new Intent(Activity_MoimWrite.this, Activity_MoimTimeLineList.class);
                                startActivity(intent);

                                Fragment_MoimTab1_Timeline.instance.refreshItems(); //미리 새로고침
                                finish();

                            }else if(strWriteType.equals(Constants.MOIM_WRITE_TYPE_WRITE)){ //글쓰기
                                //모임타임라인 > 글쓰일경우
                                setResult(RESULT_OK);
                                finish();
                            }

                        } else {
                            Toast.makeText(Activity_MoimWrite.this, "글쓰기 실패", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        break;


                    case PacketTypes.PTC_IMS_MOIM_BOARD_UPDATE:    //모임글수정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "PTC_IMS_MOIM_BOARD_UPDATE strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Util.hideKeyboard(Activity_MoimWrite.this, (EditText) findViewById(R.id.edt_write));

                            Toast.makeText(Activity_MoimWrite.this, "글수정 완료", Toast.LENGTH_SHORT).show();

                            setResult(RESULT_OK);
                            finish();


                            if(Activity_TimeLineDetail.activity!=null){
                                Activity_TimeLineDetail.activity.refreshItems();
                            }

                            Fragment_MoimTab1_Timeline.instance.refreshItems();

                        } else {
                            Toast.makeText(Activity_MoimWrite.this, "글수정 실패", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 글내용이 널인지 체크.
     * @return
     */
    public boolean isContentNull(){
        Log.e(TAG,"isContentNull mimgHash.:"+ mimgHash.size() +" mFileHash: "+mFileHash.size());

        if(TextUtils.isEmpty( ((TextView)findViewById(R.id.edt_write)).getText().toString().trim())
                && mimgHash.size()==0
                && mFileHash.size()==0
                && isNullEdittextContent()
                ){
            return true;
        }else{
            return false;
        }
    }



    /**
     * 모임 파일 업로드
     *  @param context
     * @param path
     * @param nRandomId
     */
    public void profileUpload(final Context context, final String path, final int nRandomId) {

        //Log.e(TAG, "profileUpload path:" + path);

        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {

                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.MOIM_IMAGE_UPLOAD); //TODO

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    ///entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, "userId", "")));
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();
                    entity.addPart("type", new StringBody("1"));
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("profileUpload::::Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }


            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("result").equals("success")) {

                            Log.e(TAG, "profileUpload onPostExecute " + jsonObject.toString());

                            mAtch = jsonObject.toString();

                            try {

                                JSONObject jsonObj = new JSONObject(mAtch);
                                switch (UPLOAD_TYPE) {
                                    case image: //이미지 업로드

                                        //String thumb_file = jsonObj.getString("thumb_file");
                                        String saveFile = jsonObj.getString("save_file");

                                        mimgHash.put(nRandomId + "", saveFile);
                                        mISEmoticonHash.put(nRandomId + "", "image");  //이모티콘과 구분하기위한
                                        Log.e(TAG, "profileUpload onPostExecute nRandomId: " + nRandomId + " save_file: " + saveFile);

                                        break;

                                    case file:  //파일업로드
                                        String save_file = jsonObj.getString("save_file");
                                        mFileHash.put(mAddFileCount + "", save_file);

                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //onUploadComplete(false);
                    }
                } else {
                    Log.d(TAG, "profileUpload:::::Upload Image Failed!!!!!!!!");
                    //onUploadComplete(false);
                }
            }
        }.execute();
    }

}
