package com.wave.messenger.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 7. 26..
 */

public class Activity_Interference_Off extends BaseActivity implements View.OnClickListener {
    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interference_off);


        ((ToggleButton) findViewById(R.id.toggle_allow_interference)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {
                    ((LinearLayout)findViewById(R.id.ll_interference_start)).setVisibility(View.VISIBLE);
                    ((View)findViewById(R.id.line_interference_start)).setVisibility(View.VISIBLE);
                    ((LinearLayout)findViewById(R.id.ll_interference_end)).setVisibility(View.VISIBLE);
                    ((View)findViewById(R.id.line_interference_end)).setVisibility(View.VISIBLE);
                } else {
                    ((LinearLayout)findViewById(R.id.ll_interference_start)).setVisibility(View.GONE);
                    ((View)findViewById(R.id.line_interference_start)).setVisibility(View.GONE);
                    ((LinearLayout)findViewById(R.id.ll_interference_end)).setVisibility(View.GONE);
                    ((View)findViewById(R.id.line_interference_end)).setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
