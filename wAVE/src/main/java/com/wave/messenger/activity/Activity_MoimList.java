package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.MoimListRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimSearch;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 9..
 * 주제별 모임리스트
 * &
 * 모임글 검색
 * &
 * 새로시작한모임
 */
public class Activity_MoimList extends BaseActivity {


    String TAG = getClass().getSimpleName();
    MoimListRecyclerAdapter mMoimListRecyclerAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    int mnType;
    boolean mbSearch = false;     //검색 버튼이후 true; 리스트에 추가이후 false
    boolean mbResetList = false;  //setDate 리셋
    String mSearchKeyword;      //검색 키워드

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        setContentView(R.layout.activity_moim_list);

        Intent intent = getIntent();
        mnType = intent.getIntExtra("TYPE", 6);  //TYPE: 0~5 주제별리스트  6:모임글검색   기본: 이런모임은 어때요

        Log.e(this.getLocalClassName(), "mnType " + mnType);

        initView(mnType);
        setRecyclerView();
        setOnClickEvent(mnType);

        moimSearchRequest(mnType, "", "");

        //주제별 모임 찾기, 이런모임은 어때요 숨김처리
        findViewById(R.id.ll_findbysubject).setVisibility(View.GONE);
        findViewById(R.id.ll_recommend).setVisibility(View.GONE);
    }

    private void setOnClickEvent(int nType) {

        if (nType == 6) {
            findViewById(R.id.ll_top_search).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((EditText) findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search)).setHint("");
                }
            });

            ((EditText) findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        moimSearchBtn();
                        return true;
                    }
                    return false;
                }
            });

            findViewById(R.id.ll_top_search).findViewById(R.id.tv_search).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    moimSearchBtn();
                }
            });

            findViewById(R.id.ll_findbysubject).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Activity_MoimList.this, Activity_MoimFindBySubject.class);
                    intent.putExtra(Constants.MOIM_TYPE, Constants.SET_MOIM_LIST.search_by_subject);
                    startActivity(intent);
                }
            });

        } else {
            findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    /**
     * 검색버튼.
     */
    private void moimSearchBtn() {
        mSearchKeyword = ((EditText) findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search)).getText().toString().trim();
        if (TextUtils.isEmpty(mSearchKeyword)) {
            Toast.makeText(Activity_MoimList.this, "검색어를 입력하세요", Toast.LENGTH_SHORT).show();
        } else {

            Util.hideKeyboard(Activity_MoimList.this, ((EditText) findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search)));
            mbSearch = true;
            mbResetList = true;
            moimSearchRequest("");
            ((EditText) findViewById(R.id.ll_top_search).findViewById(R.id.et_topbar_search)).setText("");
        }
    }

    private void setRecyclerView() {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mMoimListRecyclerAdapter = new MoimListRecyclerAdapter(this);
        mRecyclerView.setAdapter(mMoimListRecyclerAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(mMoimListRecyclerAdapter.getLastMmId());
            }
        };

        mRecyclerView.addOnScrollListener(scrollListener);
    }

    /**
     * 다음페이지 로딩
     *
     * @param
     */
    public void loadNextDataFromApi(String mmid) {
        // Log.e(TAG, "loadNextDataFromApi mmid: "+mmid);
        if (mbSearch) {   //검색 다음페이지
            moimSearchRequest(mmid);
        } else {
            moimSearchRequest(mnType, "", mmid);
        }
    }

    /**
     * 리스트로 데이터 세팅
     *
     * @param params
     */
    private void setDate(ArrayList<VoMoimSearch.VoMoimSearchItem> params) {
        //Log.e(TAG, "setDate: "+mbResetList);
        mMoimListRecyclerAdapter.setItem(params, mbResetList);
        mMoimListRecyclerAdapter.notifyDataSetChanged();
        mbResetList = false;
    }

    /**
     * nType값이 6이면 모임글 검색화면
     * 7이면 새로시작한모임
     * 0~5이면 주제별 모임 리스트화면
     *
     * @param nType
     */
    private void initView(int nType) {

        if (nType == 6) {  //
            findViewById(R.id.ll_top_Bar).setVisibility(View.GONE);

            //상단 검색화면 visible
            //주제별모임찾기 visible
            //이런모임은 어때요 visible
        } else if (nType == 7) { //새로시작한 모임
            findViewById(R.id.ll_top_search).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.new_moim));

        } else {
            findViewById(R.id.ll_top_search).setVisibility(View.GONE);
            String[] listValue = getResources().getStringArray(R.array.subjects);
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(listValue[nType]);

            findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    /**
     * 모임검색-기본
     */
    private void moimSearchRequest(int bType, String srchWrd, String page) {
        //주제별 리스트일때 호출
        if (bType < 6) {
            Moa.moimSearchRequest(Activity_MoimList.this, "1", page, srchWrd, bType + 1 + "", mMainActvtHandler);
        } else if (bType == 6) {
            Moa.moimSearchRequest(Activity_MoimList.this, "2", page, srchWrd, bType + 1 + "", mMainActvtHandler);
        }
    }

    /**
     * 자동로딩
     * 모임검색-검색어 입력
     * md:0
     */
    private void moimSearchRequest(String mmId) {
        //Log.e(TAG,"moimSearchRequest :  "+mmId);
        Moa.moimSearchRequest(Activity_MoimList.this
                , "0"
                , mmId,
                mSearchKeyword
                , "", mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_SEARCH:    //
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            //Log.e(TAG,"params: "+data.body.get("params").toString());
                            VoMoimSearch voMoimSearch;
                            Gson gson = new Gson();
                            voMoimSearch = gson.fromJson(data.body.toString(), VoMoimSearch.class);

                            Log.e(TAG, "voMoimSearch size: " + voMoimSearch.getParams().size());
                            setDate(voMoimSearch.getParams());

                        } else {
                            //Toast.makeText(Activity_MoimList.this, "검색 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });
}
