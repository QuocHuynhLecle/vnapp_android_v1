package com.wave.messenger.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_JoinLimit;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.dialog.Dialog_professional;
import com.wave.messenger.dialog.Dialog_professionalAccept1;
import com.wave.messenger.dialog.Dialog_professionalAccept2;
import com.wave.messenger.util.AndroidLayoutResize;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.Moa;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * Created by yunsu on 2017. 3. 13..
 * 모임 이름,커버 설정 화면
 * 모임 이름,커버 변경 화면
 */
public class Activity_MoimCoverSet extends BaseActivity {

    String TAG = Activity_MoimCoverSet.class.getSimpleName();
    String type;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_cover_set);

        Intent intent = getIntent();
        type = intent.getStringExtra(Constants.SET_COVER);
        mMmTg = intent.getIntExtra(Constants.MOIM_TAG, 0) + "";

        //Log.e(TAG, "#mMmTg " + mMmTg);

        initView(type);
        setOnClickEvent(type);
    }

    private void initView(String type) {

        if (type.equals(Constants.NEW_COVER)) { //새로운 커버설정일경우
            ((TextView) findViewById(R.id.tv_moim_name_info)).setText(getResources().getString(R.string.new_cover_name_info));
            ((EditText) findViewById(R.id.edt_moin_name)).setHint(getResources().getString(R.string.input_group_name));
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getString(R.string.make_moim));

            setCoverBG(mMmTg);
            setBtn();

        } else if (type.equals(Constants.MODIFY_COVER)) { //커버 변경일경우

            ((TextView) findViewById(R.id.tv_moim_name_info)).setText(getResources().getString(R.string.modify_cover_name_info));
            findViewById(R.id.ll_type).setVisibility(View.GONE);
            ((EditText) findViewById(R.id.edt_moin_name)).setHint(getResources().getString(R.string.txt_input_modify_group_name));
            ((EditText) findViewById(R.id.edt_moin_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
            ((TextView)findViewById(R.id.ll_top_Bar).findViewById(R.id.tv_topbar_title)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));

            findViewById(R.id.imgv_thum_img).setVisibility(View.VISIBLE);
            findViewById(R.id.imgv_thum_icon).setVisibility(View.GONE);

            if(SharedObject.getProperty_string(this, Constants.imgTy, "0").equals("0")){
                ((ImageView) findViewById(R.id.imgv_thum_img)).setImageResource(R.drawable.meet_timeline_default_img);

            }else{

                mImgTy = SharedObject.getProperty_string(this, Constants.imgTy,"");
                mImgOrg = SharedObject.getProperty_string(this, Constants.imgOrg,"");
                mImgTmb = SharedObject.getProperty_string(this, Constants.imgTmb,"");

                Glide.with(this).load(Constants.MOIM_LIST_IMAGE_URL + SharedObject.getProperty_string(this, Constants.imgTmb, "").replace("thumb_", ""))
                        .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                        .dontAnimate().into((ImageView) findViewById(R.id.imgv_thum_img));

                //Log.e(TAG,"imgTmb "+SharedObject.getProperty_string(this, Constants.imgTmb, "").replace("thumb_", ""));
            }
        }
    }

    Uri fileUri;
    private void setOnClickEvent(final String type) {
        //완료
        findViewById(R.id.btn_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type.equals(Constants.NEW_COVER)) {
                    moimCreate();
                }else{
                    moimModify();
                }
            }
        });


        //하단 모임생성
        findViewById(R.id.tv_moim_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moimCreate();
            }
        });


        //사진찍기버튼
        findViewById(R.id.btn_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String fileName = System.currentTimeMillis()+".jpg";
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, fileName);
                fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                Dialog_Camera.getProfileEditCameraDialog2_2(Activity_MoimCoverSet.this,fileUri);
            }
        });

        //변경버튼 삭제됨
        /*findViewById(R.id.btn_modify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingDialog();
            }
        });*/

        //뒤로가기
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //인원제한 다이얼로그
        findViewById(R.id.btn_limit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinLimitDialog();
            }
        });
    }


    /**
     * 모임만들기
     * 설정 버튼을 세팅한다
     */
    private void setBtn() {


        //가입인원제한
        ((Button) findViewById(R.id.btn_limit)).setText("100명");//기본 100명


        //모임 글쓰기 방식 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
        ((Button) findViewById(R.id.btn_cover_option2_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0); //기본 모두가능
        findViewById(R.id.btn_cover_option2_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //리더만가능
                mWrtTy = "1";
                ((Button) findViewById(R.id.btn_cover_option2_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option2_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
            }
        });

        findViewById(R.id.btn_cover_option2_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  //모두가능
                mWrtTy = "0";
                ((Button) findViewById(R.id.btn_cover_option2_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option2_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
            }
        });

        //모임 공개여부
        ((Button) findViewById(R.id.btn_cover_option3_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0); //1: 공개(기본),

        findViewById(R.id.btn_cover_option3_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //공개
                mOpnTy = "1";
                ((Button) findViewById(R.id.btn_cover_option3_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option3_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
            }
        });

        findViewById(R.id.btn_cover_option3_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //비공개
                mOpnTy = "0";
                ((Button) findViewById(R.id.btn_cover_option3_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option3_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
            }
        });


        //멤버 가입 방식 / 자동가입기본
        ((Button) findViewById(R.id.btn_cover_option4_btn3)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);//기본 자동가입

        findViewById(R.id.btn_cover_option4_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //승인
                selectCover4("0");
                questioDialog();
            }
        });

        findViewById(R.id.btn_cover_option4_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //비밀번호 입력
                selectCover4("2");
                questioDialog();
            }
        });

        findViewById(R.id.btn_cover_option4_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //자동가입
                selectCover4("1");
            }
        });

        if (MessengerInfo.getUserType().equals("UT_ST") || MessengerInfo.getUserType().equals("UT_ST_HQ"))
            ((LinearLayout) findViewById(R.id.ll_stmm)).setVisibility(View.VISIBLE);

        //모임 타입 설정 - 상담모임, 종목 추천모임
        ((Button) findViewById(R.id.btn_cover_option5_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);

        findViewById(R.id.btn_cover_option5_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMmStTy = "0";
                ((Button) findViewById(R.id.btn_cover_option5_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
//                ((Button) findViewById(R.id.btn_cover_option5_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
            }
        });

//        findViewById(R.id.btn_cover_option5_btn2).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mMmStTy = "1";
//                ((Button) findViewById(R.id.btn_cover_option5_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
//                ((Button) findViewById(R.id.btn_cover_option5_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
//            }
//        });




        //리더타입 -
        // 정보교류및 친목 xprt=0
        // 전문가/매니저/애널리스트 xprt=1 , xprtTy 0:매니저, 1:애널리스트, 2:전문가
        ((Button) findViewById(R.id.btn_cover_option6_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
        findViewById(R.id.btn_cover_option6_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCover5("0");
            }
        });

        findViewById(R.id.btn_cover_option6_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCover5("1");
                professDialog();
            }
        });

        //직원모임 여부 - 일반모임, 직원모임
        // 일반모임 stMm=0, 직원전용모임 stMm=1
        ((Button) findViewById(R.id.btn_cover_option7_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
        findViewById(R.id.btn_cover_option7_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stMm = "0";
                ((Button) findViewById(R.id.btn_cover_option7_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option7_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
            }
        });

        findViewById(R.id.btn_cover_option7_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stMm = "1";
                ((Button) findViewById(R.id.btn_cover_option7_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option7_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
            }
        });
    }


    /**
     * 전문가 신청 다이얼로그
     * 3 라디오버튼 선택
     */
    private void professDialog() {

        final Dialog_professional dialog_professional = new Dialog_professional(this);
        dialog_professional.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();
                if(id==R.id.tv_cancle){
                    //정보교류및 친목 다시 선택하기
                    selectCover5("0");
                    dialog_professional.dismiss();
                }else if(id==R.id.tv_confirm){
                    selectCover5("1");

                    mXprtTy=dialog_professional.getSelect();
                    switch (mXprtTy){
                        case "0":
                            //메니져 소속지점및 부서 팝업
                            setAccept3Dialog("0");
                            break;

                        case "1":
                            //애널리스트 담당섹터 팝업
                            setAccept2Dialog("1");
                            break;

                        case "2":
                            //전문가 그룹 팝업
                            setAccept1Dialog();

                            break;
                    }
                    dialog_professional.dismiss();
                }
            }
        });
        dialog_professional.show();
    }



    /**
     * 1.전문가그룹선택  스피너 다이얼로그
     * 모임 전문가 그룹 리스트
     */
    private void setAccept1Dialog() {
        Log.e(TAG,"setAccept1Dialog");

        final Dialog_professionalAccept2 dialog_professionalAccept2 = new Dialog_professionalAccept2(this);
        dialog_professionalAccept2.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();
                if(id==R.id.tv_apply){

                    //신청사유

                    //edittex 가져오기
                    if(dialog_professionalAccept2.getEdtex()!=null){
                        //신청사유  값넣고
                        mXprtRsn=dialog_professionalAccept2.getEdtex();
                        mXprtGrpId=dialog_professionalAccept2.getSelect();

                        // 관리자검토 팝업
                        setReviewDialog();

                        dialog_professionalAccept2.dismiss();
                    }else{
                        Toast.makeText(Activity_MoimCoverSet.this, "간단한 전문가님 소개를 적어주세요.", Toast.LENGTH_SHORT).show();
                    }


                }else if(id==R.id.tv_cancle){
                    mXprtGrpId="";  //전문가 그룹 아이디(int)
                    mXprtTy="";     //전문가 타입(int)
                    mXprtRsn="";    //전문가 신청사유

                    //정보교류 및 친목 선택
                    dialog_professionalAccept2.dismiss();

                    //정보교류및 친목 다시 선택하기
                    selectCover5("0");
                }

            }
        });
        dialog_professionalAccept2.show();
    }


    /**
     * 0:메니져
     */
    private void setAccept3Dialog(String type) {
        final Dialog_professionalAccept1 dialog_professionalAccept1 = new Dialog_professionalAccept1(this,type);
        dialog_professionalAccept1.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();
                if(id==R.id.tv_apply){
                    //edittex 가져오기
                    if(dialog_professionalAccept1.getEdtex1()==null){
                        Toast.makeText(Activity_MoimCoverSet.this, "소속 지점 및 부서를 입력하세요.", Toast.LENGTH_SHORT).show();

                    }else if(dialog_professionalAccept1.getEdtex2()==null){
                        Toast.makeText(Activity_MoimCoverSet.this, "사번을 입력하세요", Toast.LENGTH_SHORT).show();
                    }else{
                        //
                        mXprtLoc = dialog_professionalAccept1.getEdtex1();
                        //사번
                        mXprtEmpNo = dialog_professionalAccept1.getEdtex2();

                        //Log.e(TAG,"mXprtLoc: "+mXprtLoc +" mXprtEmpNo: "+mXprtEmpNo);
                        setReviewDialog();

                        dialog_professionalAccept1.dismiss();
                    }
                }else if(id==R.id.tv_cancle){
                    mXprtLoc="";
                    mXprtEmpNo="";

                    //정보교류 및 친목 선택
                    dialog_professionalAccept1.dismiss();
                    //정보교류및 친목 다시 선택하기
                    selectCover5("0");
                }
            }
        });

        dialog_professionalAccept1.show();
    }

    /**
     * 2:애널리스트 다이얼로그
     */
    private void setAccept2Dialog(String type) {

        final Dialog_professionalAccept1 dialog_professionalAccept1 = new Dialog_professionalAccept1(this,type);
        dialog_professionalAccept1.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();
                if(id==R.id.tv_apply){
                    //신청
                        //edittex 가져오기
                        if(dialog_professionalAccept1.getEdtex1()==null){
                            Toast.makeText(Activity_MoimCoverSet.this, "담당섹터를 입력하세요.", Toast.LENGTH_SHORT).show();

                        }else if(dialog_professionalAccept1.getEdtex2()==null){
                            Toast.makeText(Activity_MoimCoverSet.this, "사번을 입력하세요", Toast.LENGTH_SHORT).show();
                        }else{
                            //담당
                            mXprtLoc = dialog_professionalAccept1.getEdtex1();
                            //사번
                            mXprtEmpNo = dialog_professionalAccept1.getEdtex2();

                            Log.e(TAG,"mXprtLoc: "+mXprtLoc +" mXprtEmpNo: "+mXprtEmpNo);
                            setReviewDialog();

                            dialog_professionalAccept1.dismiss();
                        }
                }else if(id==R.id.tv_cancle){
                    //정보교류 및 친목 선택
                    dialog_professionalAccept1.dismiss();

                    //정보교류및 친목 다시 선택하기
                    selectCover5("0");
                }
            }
        });

        dialog_professionalAccept1.show();
    }

    /**
     * 관리자검토 다이얼로그
     */
    private void setReviewDialog() {

        final Dialog_Text dialog = new Dialog_Text(Activity_MoimCoverSet.this
                , getString(R.string.dialog_review_info)
                , Constants.DIALOG_BUTTON_TYPE.confirm);

        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    String mMmStTy = "1"; //0:상담모임, 1:종목추천모임
    String mMmTg = "1";   //모임 종류

    String mImgTy = "0";  //모임 이미지 타입(int)  0:이미지 없음, 1:있음
    String mImgOrg;     //모임 이미지 원본
    String mImgTmb;     //모임 썸네일 원본
    String mIntr = "";       //모임 설명     TODO 모임설정에서 입력
    //String mSave_file;


    int mnMaxUsr = 1;     //가입 최대 사용자(int) 50, 100, 500 중에 설정, 기본 : 100
    String mWrtTy = "0";  //글쓰기 권한(int) 0:모든멤버(기본), 1:리더
    String mOpnTy = "1";  //공개 제한(int) 0:비공개, 1:공개(기본), //2:모임명만 공개(기본)삭제

    /*멤버가입방식*/
    String mEntTy = "1"; //멤버타입설정 0:리더승인 qstn입력 , 2:비밀번호 mmPw입력, 1:자동가입(기본)
    String mQstn = "";       //모임 가입질문  TODO entTy가 0일경우
    String mmPw = "";       //모임 가입비밀번호 TODO entTy가 2일경우

    /*리더타입*/
    String mXprt = "0";  //전문가 신청(int) 0:미신청, 1:신청  정보교류및 친목일때 0, 전문가/매니저/애널리스트 선택시
    String mXprtTy = ""; //전문가/매니저/애널리스트 선택시 0:매니저, 1:애널리스트, 2:전문가

    String mXprtRsn = "";   //전문가 그룹이 아닐 경우 신청사유 입력
    String mXprtGrpId ="";   //전문가 그룹 아이디(int)    전문가 타입이 전문가일 때 신청할 전문가 그룹 아이디 입력
    String mXprtEmpNo = ""; //사원번호 전문가 타입이 매니저, 애널리스트 일 경우 사원번호 입력
    String mXprtLoc ="";    //부서 혹은 담당섹터    전문가 타입이 매니저, 애널리스트 일 경우 부서 혹은 담당섹터 입력

    //*직원모임여부
    String stMm = "0";   //직원전용모임여부 0:일반모임, 1:직원전용모임



    /**
     * 모임생성
    */
    public void moimCreate() {
        Log.e(TAG, "moimCreate: " + MessengerInfo.getUserId(this));

        if (Util.isEmpty((EditText) findViewById(R.id.edt_moin_name))) {
            //그룹이름을 입력하세요
            Toast.makeText(Activity_MoimCoverSet.this, "그룹이름을 입력하세요", Toast.LENGTH_SHORT).show();
        } else {

            Moa.moimCreate(this, ((EditText) findViewById(R.id.edt_moin_name)).getText().toString().trim(),
                    mMmStTy, mMmTg, mXprt, mImgTy, mImgOrg, mImgTmb,
                    ((EditText)findViewById(R.id.et_moim_str)).getText().toString(), mQstn, mmPw,mnMaxUsr + "", mWrtTy,
                    mOpnTy, mEntTy,
                    mXprt, mXprtTy ,mXprtRsn, mXprtGrpId, mXprtEmpNo, mXprtLoc, stMm,
                    mMainActvtHandler);
        }
    }

    /**
     * 7.8 모임 설정
     * PTG : 0x00080000, PTC : 0x00080005
      PTG_IMS_MOIM, PTC_IMS_MOIM_CONFIG

     * 모임 커버이미지 모임명수정
     */
    public void moimModify() {

        Log.e(TAG,"moimModify");
        if (Util.isEmpty((EditText) findViewById(R.id.edt_moin_name))) {
            Toast.makeText(Activity_MoimCoverSet.this, "변경할 모임 이름을 입력하세요", Toast.LENGTH_SHORT).show();
        } else {

            Moa.moimModify(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null)
                    ,((EditText) findViewById(R.id.edt_moin_name)).getText().toString().trim()
                    , mImgTy, mImgOrg, mImgTmb,
                    mMainActvtHandler);
        }

    }


    //개설하기 응답 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                String strResult;
                boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_CREATE:    //개설
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + " mmId" + data.body.getJson("params").getString("mmId"));

                        SharedObject.setProperty_string(Activity_MoimCoverSet.this, Constants.MOIM_ID, data.body.getJson("params").getString("mmId"));

                        if (strResult.equals(Constants.SUCCESS)) {//일단 현재 창만 닫음
                            //finish();

                            if (Activity_MoimFindBySubject.activity != null) { // 개설선택 화면 닫기
                                Activity_MoimFindBySubject activity = Activity_MoimFindBySubject.activity;
                                activity.finish();
                            }

                            //내모임리스트 새로고침
                            Activity_MymoimList.activity.refreshItems();

                            Intent intent = new Intent(Activity_MoimCoverSet.this, Activity_MoimTimeLineList.class);
                            startActivity(intent);
                            finish();
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_CONFIG:    //수정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + " mmId" + data.body.getJson("params").getString("mmId"));

                        Toast.makeText(Activity_MoimCoverSet.this, getString(R.string.moim_cover_modify), Toast.LENGTH_SHORT).show();

                        if (strResult.equals(Constants.SUCCESS)) {// 일단 현재 창만 닫음

                            String mmNm = data.body.getJson("params").getString("mmNm");
                            SharedObject.setProperty_string(Activity_MoimCoverSet.this, Constants.mmNm, mmNm);

                            String imgTmb = data.body.getJson("params").getString("imgTmb");
                            SharedObject.setProperty_string(Activity_MoimCoverSet.this, Constants.imgTmb, imgTmb);

                            String imgTy = data.body.getJson("params").getString("imgTy");
                            SharedObject.setProperty_string(Activity_MoimCoverSet.this, Constants.imgTy, imgTy);

                            //내모임리스트 새로고침
                            if(Activity_MymoimList.activity!=null)
                            Activity_MymoimList.activity.refreshItems();

                            Activity_MoimTimeLineList.activity.refreshItems();

                            //모임정보
                            try{
                                Activity_MoimInfo.activity.initView();
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                            //모임설정관리 모임명수정
                            Activity_SetManage.activity.initView();

                            finish();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });


    /**
     * 모임 타입 설정 다이얼로그
     * 삭제됨
     *
     * @deprecated
     */
    /*private void SettingDialog() {
        //변경 팝업
        final Dialog_TypeSetting mDialog = new Dialog_TypeSetting(Activity_MoimCoverSet.this, Constants.SET_PUBLIC_DIALOG_TYPE.new_moim_cover, mOpnTy, mEntTy);

        //Constants.SET_PUBLIC_DIALOG_TYPE.new_moim_cover
        mDialog.show();
        //mDialog.setData(String.format(getString(R.string.dia_title_result), id, pw), getString(R.string.dia_sub_result));
        mDialog.setCancleListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setConfirmistener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOpnTy = mDialog.getSelect() + "";
                setPublicInfo(Integer.parseInt(mOpnTy));

                Toast.makeText(Activity_MoimCoverSet.this, "mOpnTy: " + mOpnTy + " mEnter_type: " + mEntTy + " mMmTg: " + mMmTg, Toast.LENGTH_SHORT).show();

                //
                mEntTy = mDialog.getToggle() + "";

                switch (mEntTy) {
                    case "0":
                        Toast.makeText(getApplicationContext(), "리더승인 가입신청받기", Toast.LENGTH_SHORT).show();
                        break;
                    case "1":
                        Toast.makeText(getApplicationContext(), "자동가입", Toast.LENGTH_SHORT).show();
                        break;
                }
                *//*if (mDialog.getToggle()) {
                    mEntTy = "1";

                } else {
                    mEntTy = "0";

                }*//*

                mDialog.dismiss();

            }
        });
    }*/


    /**
     * 모임가입 질문 & 비밀번호 입력 팝업
     *
     * 리더승인,비밀번호 입력 누르면..
     */
    private void questioDialog() {

        final Dialog_MoimJoinType dialog_MakeMoim = new Dialog_MoimJoinType(this, mEntTy);
        dialog_MakeMoim.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id=view.getId();
                if(id==R.id.tv_cancle){
                    //자동가입체크
                    dialog_MakeMoim.dismiss();
                    mQstn="";
                    mmPw="";
                    selectCover4("1");
                }else if(id==R.id.tv_confirm){
                    if(mEntTy.equals("0")){ //리더승인이면
                        mQstn=dialog_MakeMoim.getJoinTxt();   //가입질문

                    }else if(mEntTy.equals("2")){
                        mmPw=dialog_MakeMoim.getJoinTxt();   //비밀번호
                    }

                    //Log.e(TAG,"mQstn: "+mQstn +" mmPw: "+mmPw);
                    selectCover4(mEntTy);

                    Util.hideKeyboard(Activity_MoimCoverSet.this,((EditText)dialog_MakeMoim.findViewById(R.id.edt_input)));

                    dialog_MakeMoim.dismiss();
                }
            }
        });
        dialog_MakeMoim.show();
    }

    /**
     * 리더타입
     * @param i
     */
    private void selectCover5(String i) {

        mXprt=i;

        switch (i){
            case "0":   //정보교류및 친목
                ((Button) findViewById(R.id.btn_cover_option6_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option6_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                break;

            case "1":   //전문가매니저애널리스트
                ((Button) findViewById(R.id.btn_cover_option6_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option6_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                break;


        }
    }

    /**
     * 멤버 가입방식
     * @param i
     */
    private void selectCover4(String i) {
        mEntTy=i;

        switch (i){
            case "0":   //리더승인
                ((Button) findViewById(R.id.btn_cover_option4_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn3)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                break;

            case "2":   //비번입력
                ((Button) findViewById(R.id.btn_cover_option4_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn3)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                break;

            case "1":   //자동가입
                ((Button) findViewById(R.id.btn_cover_option4_btn1)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn2)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox, 0, 0, 0);
                ((Button) findViewById(R.id.btn_cover_option4_btn3)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_checkbox_press, 0, 0, 0);
                break;

        }
    }


    /**
     * 팝업 인원제한 팝업
     */
    private void joinLimitDialog() {
        //변경 팝업
        final Dialog_JoinLimit dialogJoinLimit = new Dialog_JoinLimit(Activity_MoimCoverSet.this, Constants.SET_PUBLIC_DIALOG_TYPE.new_moim_cover, mnMaxUsr);

        dialogJoinLimit.show();
        dialogJoinLimit.setConfirmistener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mnMaxUsr = dialogJoinLimit.getSelect();
                switch (mnMaxUsr) {
                    case 0:
                        ((Button) findViewById(R.id.btn_limit)).setText("50명");
                        break;
                    case 1:
                        ((Button) findViewById(R.id.btn_limit)).setText("100명");
                        break;
                    case 2:
                        ((Button) findViewById(R.id.btn_limit)).setText("500명");
                        break;
                    case 3:
                        ((Button) findViewById(R.id.btn_limit)).setText("1000명");
                        break;
                    case 4:
                        ((Button) findViewById(R.id.btn_limit)).setText("3000명");
                        break;
                    case 5:
                        ((Button) findViewById(R.id.btn_limit)).setText("5000명");
                        break;
                    case 6:
                        ((Button) findViewById(R.id.btn_limit)).setText("10000명");
                        break;
                }
                dialogJoinLimit.dismiss();

            }
        });
    }

    /**
     * 기본 이미지로 변경
     */
    public void setDefaultImage() {

        mImgTmb = "";
        mImgTy = "0";   //0:이미지 없음, 1:있음
        mImgOrg = "";

        if (type.equals(Constants.MODIFY_COVER)){
            findViewById(R.id.imgv_thum_img).setVisibility(View.VISIBLE);
            findViewById(R.id.imgv_thum_icon).setVisibility(View.GONE);
            ((ImageView) findViewById(R.id.imgv_thum_img)).setImageResource(R.drawable.meet_timeline_default_img);

        }else{
            setCoverBG(mMmTg);
            ((ImageView) findViewById(R.id.imgv_thum_img)).setImageBitmap(null);
        }
    }


    /**
     * 선택한 타입으로 박스 배경을 설정한다.
     * <p>
     * 1 : 주식    ic_stock
     * 2 : 국내파생   ic_mini_derivation
     * 3 : 해외파생   ic_mini_global_derivation
     * 4 : 해외주식   ic_mini_global_stock
     * 5 : 금융상품   ic_mini_bank_product
     * 6 : 기타      ic_mini_etc
     *
     * @param Moim_tag 모임 종류
     */
    private void setCoverBG(String Moim_tag) {

        //imgv_thum_icon
        findViewById(R.id.imgv_thum_icon).setVisibility(View.VISIBLE);
        findViewById(R.id.imgv_thum_img).setVisibility(View.GONE);

        Log.e(TAG, "setCoverBG: " + Moim_tag);
        switch (Integer.parseInt(Moim_tag)) {
            case 1:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_time_edu);
                break;
            case 2:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_personal);
                break;
//            case 1:
//                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_stock);
//                break;
//            case 2:
//                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_derivation);
//                break;
            case 3:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_global_derivation);
                break;
            case 4:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_global_stock);
                break;
            case 5:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_bank_product);
                break;
            case 6:
                ((ImageView) findViewById(R.id.imgv_thum_icon)).setBackgroundResource(R.drawable.ic_etc);
                break;
        }
    }


    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                //.setFixAspectRatio(true)
                .setInitialCropWindowPaddingRatio(0)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_MoimOpen", "onActivityResult CAMERA_REQUEST");

            //Bitmap bitmap = getPath(data.getData());
            //((ImageView)findViewById(R.id.imgv_profile)).setImageBitmap(bitmap);
            //onReceivePictureSuccess(data.getData());
            //XXX startCropImageActivity(data.getData());  값이 널
            startCropImageActivity(fileUri);


        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {

            Log.e("Activity_MoimOpen", "onActivityResult GALLERY_REQUEST");

            startCropImageActivity(data.getData());

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                try {
                    final Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

                    ((ImageView)findViewById(R.id.imgv_thum_img)).post(new Runnable() {
                        @Override
                        public void run() {

                            findViewById(R.id.imgv_thum_icon).setVisibility(View.GONE);
                            findViewById(R.id.imgv_thum_img).setVisibility(View.VISIBLE);
                            ((ImageView)findViewById(R.id.imgv_thum_img)).setImageBitmap(bitmap);
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

                // 파일 업로드
                profileUpload(this, resultUri.getPath());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE " + resultCode);
            }

        }
    }

    /**
     * 모임 파일 업로드
     *
     * @param context
     * @param path
     */
    public void profileUpload(final Context context, final String path) {
        Log.e(TAG, "profileUpload path:" + path);

        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.MOIM_PROFILE_IMAGE); //TODO

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {


                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("result").equals("success")) {

                            Log.e("profileUpload", "onPostExecute " + jsonObject.toString());
                            //onUploadComplete(true);

                            //save_file, thumb_file 값을 메시징서버로..
                            mImgTy = "1";
                            mImgTmb = jsonObject.getString("thumb_file");
                            //mImgOrg = jsonObject.getString("org_file");
                            mImgOrg = jsonObject.getString("save_file");

                            //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();

                            LoadingManager.with(context).hideLoadingDialog();
                        }
                    } catch (JSONException e) {
                        //onUploadComplete(false);
                    }
                } else {
                    //onUploadComplete(false);
                }
            }
        }.execute();
    }

    /**
     * 카메라 퍼미션 결과
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Activity_MoimOpen", "permission was granted");
                    // 카메라
                    Dialog_Camera.setCamera(Activity_MoimCoverSet.this);

                } else {//퍼미션 거부일때 처리
                    //카메라 퍼미션 거부 팝업
                    Dialog_Camera.showCameraPermissionDialog(Activity_MoimCoverSet.this);
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AndroidLayoutResize.assistActivity(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }
}
