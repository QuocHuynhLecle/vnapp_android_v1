package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.helper.UITopbarHelper;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.MTSItemMaster;
import com.wave.messenger.vo.VoItemCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * 500자 이상의 텍스트를 표시해주는 액티비티. <p/>
 * Intent content - 인텐트에 추가해서 보냄.
 */
public class Activity_LongTextView extends Activity {
    private static Activity_LongTextView instance=null;
    public static Activity_LongTextView getInstance(){
        return  instance;
    }
    private TextView tvContents;
    private UITopbarHelper llTopBar;

    /** 표시할 컨탠츠*/
    private String contents;
    private SpannableString mLinkableText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_longtextview);
        instance=this;
        initView();
        setEvent();
        getData();
        setInitialValue();
    }

    private void setInitialValue(){
        mLinkableText = new SpannableString(contents);
        //gatherLinksForText();
    }


    private void getData() {

        if(getIntent().hasExtra("chatId")){
            contents = getIntent().getStringExtra("contents");
            String chatId=getIntent().getStringExtra("chatId");
            String roomId=getIntent().getStringExtra("roomId");
            getMessage(roomId,chatId);
        }else{
            onBackPressed();
        }
    }
    public void onMessage(MOAData data){
        try {
            LBJSONObject object = data.body.getJson("params");
            String message=object.getString("message");
            mLinkableText = new SpannableString(message);
            //gatherLinksForText();
        }catch (Exception e){

        }




    }
    private void initView() {
        llTopBar = new UITopbarHelper((LinearLayout)findViewById(R.id.llTopBar));
        llTopBar.setText(getResources().getString(R.string.title_all));
        llTopBar.setGravity(Gravity.CENTER);
        llTopBar.setTextColor(getResources().getColor(R.color.bg_purple));
//        llTopBar.setCount(null);
        llTopBar.setHomeVisibility(View.VISIBLE);

        tvContents = (TextView) findViewById(R.id.tvContents);
    }

    private void setEvent() {
        llTopBar.setHomeBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    public void getMessage(String roomId, String chatId) {

        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("roomId",roomId);
            value.put("chatId", chatId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT,
                    PacketTypes.PTC_IMS_CHAT_GET_MSG, body);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    String tempText = "";
    private void gatherLinksForText() {
        //find(mLinkableText, MTSItemMaster.getInstance().getItemCodes());
    }

    static final String[] hanJosa1 = {"은", "는", "이", "가", "을", "를", "다", "의", "와", "과", "도"
            , "만", "에", "야", "여", "아", "면", "께"
    };

    static final String[] hanJosa2 = {"께서", "에서", "이다"
            , "까지", "부터", "마저", "에서", "에게", "이여", "시여", "보다"
            , "일까", "인가", "여서", "만큼", "니까"
    };

    static final String[] hanJosa3 = {"이니까", "이어서"};

    String tmpLowerText = "";

    public void find(Spannable s, ArrayList<VoItemCode> itemCodeList) {
        tempText = s.toString();
        tmpLowerText = tempText.toLowerCase();
        for (int i = 0; i < itemCodeList.size(); i++) {

            VoItemCode item = itemCodeList.get(i);
            String codeName = itemCodeList.get(i).getName().toLowerCase();

            boolean skipTxt = false;
            int start = tmpLowerText.indexOf(codeName);
            if (start > -1) { //종목명이 있을때

                int end = start + codeName.length();

                //바로앞이 문자가 오면안된다.
                if (start > 0) {
                    Character ch = tmpLowerText.charAt(start - 1);
                    if (Character.isLetter(ch)) {
                        skipTxt = true;
                    }
                }

                if (skipTxt == false && end + 1 <= tmpLowerText.length()) {
                    //뒤에 문자가 있는경우 특수문자(공백) 이거나 한글조사외에는 스킵한다.


                    Character ch = tmpLowerText.charAt(end);
                    if (Character.isLetter(ch)) {
                        //한글 조사 체크.
                        skipTxt = true;

                        String letter1 = String.valueOf(ch);
                        String letter2 = "";
                        String letter3 = "";
                        if (end + 2 <= tmpLowerText.length()) {
                            letter2 = tmpLowerText.substring(end, end + 2);
                        }
                        if (end + 3 <= tmpLowerText.length()) {
                            letter3 = tmpLowerText.substring(end, end + 3);
                        }

                        for (String value : hanJosa1) {
                            if (value.equals(letter1)) {
                                skipTxt = false;
                                break;
                            }
                        }

                        if (skipTxt == true && hanJosa2.equals("") == false) {
                            for (String value : hanJosa2) {
                                if (value.equals(letter2)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }

                        if (skipTxt == true && hanJosa3.equals("") == false) {
                            for (String value : hanJosa3) {
                                if (value.equals(letter3)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }

                    }
                }

                if (skipTxt == false) {
                    mLinkableText.setSpan(new InternalURLSpan(s.subSequence(start, end).toString()
                                    , item.getCode(), 2)
                            , start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(this, R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getName()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }

            if (skipTxt) {
                start = tempText.indexOf(item.getCode());
                if (start > -1) { //종목코드가 있을때
                    int end = start + item.getCode().length();
                    mLinkableText.setSpan(new InternalURLSpan(s.subSequence(start, end).toString()
                            , item.getCode(), 2), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(this, R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getCode()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }
        }

        addLinks(s);
    }


    public static final Pattern WEB_URL = Pattern.compile(
            "((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
                    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
                    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
                    + "((?:(?:[" + Patterns.GOOD_IRI_CHAR + "][" + Patterns.GOOD_IRI_CHAR + "\\-]{0,64}\\.)+"   // named host
                    + Patterns.TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL
                    + "|(?:(?:25[0-5]|2[0-4]" // or ip address
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
                    + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9])))"
                    + "(?:\\:\\d{1,5})?)" // plus option port number
                    + "(\\/(?:(?:[" + Patterns.GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
                    + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
                    + "(?:\\b|$)"); // and finally, a word boundary or end of
    // input.  This is to stop foo.sure from
    // matching as foo.su

    public void addLinks(Spannable spannable) {
        // TextView textView

        //Linkify.addLinks(spannable,
        //		WEB_URL, "http://");

        Linkify.addLinks(spannable,
                WEB_URL, "");

        URLSpan[] urlSpans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan urlSpan : urlSpans) {
            int start = spannable.getSpanStart(urlSpan);
            int end = spannable.getSpanEnd(urlSpan);
            mLinkableText.removeSpan(urlSpan);
            mLinkableText.setSpan(new InternalURLSpan(urlSpan.getURL()
                    , urlSpan.getURL(), 1), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        tvContents.setMovementMethod(LinkMovementMethod.getInstance());
        tvContents.setText(mLinkableText);


    }

    public String replaceText(String text) {
        String replaceText = "";
        for (int i = 0; i < text.length(); i++) {
            replaceText = replaceText + "-";
        }
        return replaceText;
    }

    private class InternalURLSpan extends ClickableSpan {
        private String clickedSpan;
        private String itemCode;
        int linkType;

        public InternalURLSpan(String clickedString, String itemCode, int linkType) {
            this.clickedSpan = clickedString;
            this.itemCode = itemCode;
            this.linkType = linkType;
        }

        @Override
        public void onClick(View textView) {
            String url = itemCode;
            switch (linkType) {
                case 1:
                    try {
                        if (url.contains("http://") || url.contains("https://")) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        } else if (url.contains(".com") || url.contains("www.")) {
                            String urlString = url;
                            if (urlString.contains(".com") && !urlString.contains("www.")) {
                                urlString = "http://www." + urlString;
                            } else if (!urlString.contains(".com") && urlString.contains("www.")) {
                                urlString = "http://" + urlString;
                            } else if (urlString.contains(".com") && urlString.contains("www.")) {
                                urlString = "http://" + urlString;
                            }
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                            startActivity(intent);
                        }
                    } catch (Exception e) {
                        Toast.makeText(Activity_LongTextView.this, "링크할 수 없는 url 입니다.", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
}
