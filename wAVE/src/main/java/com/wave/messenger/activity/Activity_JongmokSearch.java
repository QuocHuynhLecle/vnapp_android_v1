package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.GroupListRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoGroupList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_JongmokSearch extends Activity implements MOAEventListener {

    static public Activity_JongmokSearch activity;

    String TAG=this.getClass().getName();
    GroupListRecyclerAdapter groupListRecyclerAdapter;
    RecyclerView recyclerView;

    public String gId, grpNm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_grouplist);

        activity=this;

        initView();
        setOnClickEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG,"onResume");
    }

    private void initView() {
        ((TextView)findViewById(R.id.textViewTitle)).setText(getString(R.string.title_group));
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.requestFocus();
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.imgb_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatRoomSearch("하나금융지주");
            }
        });
    }

    /**
     리스트 설정
     */
    public void setRecyclerView(ArrayList<VoGroupList> groupLists) {

        //RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        groupListRecyclerAdapter = new GroupListRecyclerAdapter(this, groupLists);
        recyclerView.setAdapter(groupListRecyclerAdapter);
        groupListRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnEvent(MOAData moaData) {

        LogTrace.E("MOADATA OnEvent");
        mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    /**
     * 종목 대화방 검색
     */
    public void chatRoomSearch(String searchWord) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(this));
            value.put("searchWord", searchWord);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_INFO, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * 결과 처리 핸들러
     */
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_CHATROOM_SEARCH_ROOM :    //
                        MOALog.i("PTC_IMS_CHATROOM_SEARCH_ROOM :");
                        MOALog.i("result=" + data.body.get("result"));
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            LogTrace.E("종목대화방 params: "+data.body.get("params").toString());
                        }
                        break;
                    case PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM :    //
                        MOALog.i("PTC_IMS_CHATROOM_ENTER_ROOM :");
                        MOALog.i("result=" + data.body.get("result"));
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            LogTrace.E("종목대화방 params: "+data.body.get("params").toString());
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });
}
