package com.wave.messenger.activity;

import android.app.AlertDialog;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public interface IBaseActivity {
    public AlertDialog.Builder getAlert();
    public void addFragment(int layoutId, BaseFragment baseFragment, String name);
}
