package com.wave.messenger.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Emoticon.Emoticon_Activity;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.adapter.ReplyRecyclerAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_Stock;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.dialog.Dialog_TimeLineOption_2;
import com.wave.messenger.dialog.Dialog_share;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceChart;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceInterest;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceTotal;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.util.AndroidLayoutResize;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.MTSItemMaster;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.OpenGraph;
import com.wave.messenger.utility.OpenGraphData;
import com.wave.messenger.vo.VoBoardRead;
import com.wave.messenger.vo.VoItemCode;
import com.wave.messenger.vo.VoMsgItem;
import com.wave.messenger.vo.VoStockData;
import com.kevin.wraprecyclerview.WrapAdapter;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임 타임라인 상세화면
 * 모임 공지 상세화면
 */
public class Activity_TimeLineDetail extends BaseActivity {

    public static Activity_TimeLineDetail activity = null;
    String TAG = getClass().getSimpleName();
    String mAtch;   //첨부 데이터 파일

    private WrapAdapter<ReplyRecyclerAdapter> mWrapAdapter; //댓글 레이어와 상단 글내용을 붙이기위한 어댑터
    private ReplyRecyclerAdapter mReplyRecyclerAdapter;     //댓글 어댑터
    HashMap<String, String> mimgHash = new HashMap<>();   //이미지의 getView값과 이미지페스를 저장
    private Uri resultUri;  //파일업로드URI

    private String emoticon_num;    //이모티콘 넘버
    public static Context context;

    private LinearLayout layout;

    ArrayList pictureList;  //보이는 모든 이미지 리스트
    String strText = "";  //본문복사 글내

    SwipeRefreshLayout mSwipeRefreshLayout; //아래로 새로고침

    boolean bGoToReply = false; //댓글 최하단으로 이동 구분.

    Uri fileUri;

    private DrawerLayout layout_drawer;     //우측증권화면
    private View mFragmentContainerViewRight;
    private FrameLayout layout_slideMenu;


    public static Activity_TimeLineDetail getInstance() {
        return activity;
    }

    private FragmentSlideMenu_FinanceInterest fragmentSlideMenu_financeInterest;    //금융화면 프레그먼트.


    boolean bDraw = true;   //우측 드로우 레이어 true: 보이기 , 뒤로가기 처리.


    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AndroidLayoutResize.assistActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Util.hideKeyboard(Activity_TimeLineDetail.this, (EditText) findViewById(R.id.edt_reply));
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        setContentView(R.layout.activity_time_line_detail);

        activity = this;
        context = this;

        requestBoardRead("1");
        init();

        setPermit();

        DrawRight();
    }


    /**
     * 오른쪽 드로우 레이아웃 현재가 화면
     */
    private void DrawRight() {
        layout_slideMenu = (FrameLayout) findViewById(R.id.flRightNavContainer);
        layout_drawer = (DrawerLayout) findViewById(R.id.layout_drawer);
        mFragmentContainerViewRight = findViewById(R.id.flRightNavContainer);
        layout_drawer.setDrawerListener(drawerListener);
    }


    private FragmentSlideMenu_FinanceTotal fragmentSlideMenu_financeTotal;
    private FragmentSlideMenu_FinanceChart fragmentSlideMenu_financeChart;


    public void openMTS(String item) {
        switch (item) {
            case "0":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeTotal = new FragmentSlideMenu_FinanceTotal();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeTotal).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeTotal);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
            case "1":
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
//                        menuSize(0);
                        Fragment_Main.getInstance().fragmentSlideMenu_main.setUp(layout_drawer, R.id.flRightNavContainer, (DrawerLayout) findViewById(R.id.layout_drawer));
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, Fragment_Main.getInstance().fragmentSlideMenu_main).commit();
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                });
                break;
            case "2":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeChart = new FragmentSlideMenu_FinanceChart();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeChart).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeChart);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
            case "3":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeInterest = new FragmentSlideMenu_FinanceInterest();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeInterest).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeInterest);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
        }
    }


    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    public boolean isDrawerOpenRight() {
        return layout_drawer != null && layout_drawer.isDrawerOpen(mFragmentContainerViewRight);
    }

    public void openMenuRight() {
        if (isDrawerOpenRight()) {
            layout_drawer.closeDrawer(mFragmentContainerViewRight);
        } else {
            layout_drawer.openDrawer(mFragmentContainerViewRight);
        }
    }

    public void isopenMenuRight() {
        if (isDrawerOpenRight()) {
            layout_drawer.closeDrawer(mFragmentContainerViewRight);
        }
    }


    /**
     * 상단 모임 타이틀을 설정한다.
     *
     * @param mmNm
     */
    private void setMoimTitle(String mmNm) {
        ((TextView) findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.tv_moim_title)).setText(mmNm);


    }

    @Override
    public void onBackPressed() {
        if (bDraw && isDrawerOpenRight()) {   //우측 주식 현재가 화면닫기
            layout_drawer.closeDrawer(mFragmentContainerViewRight);
        } else {

            if (findViewById(R.id.btn_emoticon).isSelected()) {
                emoticon_gone();

                mimgHash.clear();
                emoticon_num = null;

            } else {
                super.onBackPressed();
            }
        }


    }

    public void setEmoticon(String emoticons) {

        mimgHash.clear();  //사진 초기화

        this.emoticon_num = String.valueOf(emoticons);
        ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_reply_image)).setImageResource(getDrawableResourceByName(emoticons));

    }

    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(this, str);
        String packageName = getPackageName();
        return getResources().getIdentifier(emoticon, "drawable", packageName);
    }

    /**
     * 권한에 따른 댓글바 표시
     */

    private void setPermit() {

        if (Util.getPermissionLv(Util.getMymLv(this), Util.getRpyTy(this))) {
            findViewById(R.id.ll_reply).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_reply).setVisibility(View.GONE);

            //recyclerview 패팅 0처리.
            findViewById(R.id.recyclerview).setPadding(0, 0, 0, 0);

        }

    }

    /**
     *
     */
    private void init() {

        //Log.e(TAG, "Constants.MOIM_REPLY: " + SharedObject.getProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_REPLY, false));

        //"댓글" 을 눌러 들어왔으면 최하단으로 스크롤 이동.
        if (SharedObject.getProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_REPLY, false)) {  //댓글
            Util.showSoftKeyboard(Activity_TimeLineDetail.this, findViewById(R.id.edt_reply));
            bGoToReply = true;

            SharedObject.setProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_REPLY, false);
        }

    }

    /**
     * 7.19 모임 글 조회
     */
    private void requestBoardRead(String upcnt) {
        //Log.i(TAG, "requestBoardRead");
        Moa.moimBoardReadRequest(Activity_TimeLineDetail.this, mMainActivityHandler,upcnt);
    }


    /**
     * 7.18 모임 글 쓰기 - 댓글쓰기
     * <p>
     * grpNo     최상위 글 번호(int)
     * prntNo     상위 글 번호(int)
     * bbsDph     상위 글 depth(int)
     * bbsOrd     상위 글 order(int)
     */
    private void moimWriteRequest(String message, String msg, String grpNo, String prntNo, String bbsDph, String bbsOrd) {
        Moa.moimBoardWriteRequest(Activity_TimeLineDetail.this, message, msg, grpNo, prntNo, bbsDph, bbsOrd, "0", "", "", "", mMainActivityHandler);
    }


    InterEmtcallback interEmtcallback;


    /**
     * 좋아요 결과  emtt 처리
     */
    public interface InterEmtcallback {
        public void setEmt(String emtt);
    }


    /**
     * 모임 글조회 결과처리
     */
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {


                    case PacketTypes.PTC_IMS_MOIM_BOARD_READ:   //7.19 모임 글 조회
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        Log.i("TEST", "moim_strResult: " + data.body);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoBoardRead voBoardRead;
                            Gson gson = new Gson();
                            //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                            voBoardRead = gson.fromJson(data.body.toString(), VoBoardRead.class);

                            //Log.e(TAG, "data.body.toString() " + data.body.toString());
                            //Log.e(TAG, "voBoardList " + voBoardRead.getResult() + " size: " + voBoardRead.getRpyLst().size());
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID, voBoardRead.getParams().getMmId());  //모임아이디

                            setRecyclerView(voBoardRead);

                            onItemsLoadComplete();

                            setMoimTitle(voBoardRead.getParams().getMmNm());

                        } else {

                            final Dialog_Text dialog = new Dialog_Text(Activity_TimeLineDetail.this, "해당 글은 삭제되었습니다.", Constants.DIALOG_BUTTON_TYPE.confirm);

                            dialog.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Log.e(TAG, "onClick" + v.getId());
                                    dialog.dismiss();
                                    finish();
                                }
                            });


                            dialog.show();
                        }

                        break;

                    case PacketTypes.PTC_IMS_MOIM_BOARD_WRITE:    // 댓글쓰기
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);


                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Util.hideKeyboard(Activity_TimeLineDetail.this, (EditText) findViewById(R.id.edt_reply));
                            Toast.makeText(Activity_TimeLineDetail.this, "댓글 완료", Toast.LENGTH_SHORT).show();

                            ((EditText) findViewById(R.id.edt_reply)).setText("");
                            ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.GONE);
                            mimgHash.clear();

                            ((EditText) findViewById(R.id.edt_reply)).setHint("댓글을 남겨주세요.");
                            ((ImageButton) findViewById(R.id.btn_image)).setVisibility(View.VISIBLE);
                            emoticon_gone();


                            bGoToReply = true;//댓글을 쓰면 새로고침후 하단으로 스크롤을 내린다.
                            requestBoardRead("0");

                            if (Activity_MoimTimeLineList.activity != null) {   //일단 새로고침.. 입력한 값만 바꿔야할듯
                                Activity_MoimTimeLineList.activity.refreshItems();
                            }
                            if (Fragment_MoimTab1_Timeline.instance != null) {
                                Fragment_MoimTab1_Timeline.instance.refreshItems();
                            }

                            //mLinearLayoutManager.scrollToPosition(mReplyRecyclerAdapter.getItemCount()-1);

                        } else {
                            Toast.makeText(Activity_TimeLineDetail.this, "댓글 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_BOARD_DELETE:    //글 삭제
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            //Toast.makeText(Activity_TimeLineDetail.this , "글 삭제 완료", Toast.LENGTH_SHORT).show();

                            //String msgId = data.body.getJson("params").getString("msgId");

                            if (Activity_MoimTimeLineList.activity != null) {
                                Activity_MoimTimeLineList.activity.refreshItems();
                            }

                            finish();


                        } else {
                            Toast.makeText(Activity_TimeLineDetail.this, "글삭제 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;



                    /*case PacketTypes.PTC_IMS_MOIM_BBS_BLOCK :    //7.29 모임 글 차단
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());
                            Toast.makeText(mContext, "차단 완료", Toast.LENGTH_SHORT).show();

                            //notifyDataSetChanged();
                            //fragment_MoimTab1_Timeline.moimSearchRequest("","");

                            //fragment_MoimTab1_Timeline.reFreshDate();//7.27 모임 타임라인 다시 호출

                        }else{
                            Toast.makeText(mContext, "차단 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;*/

                    case PacketTypes.PTC_IMS_MOIM_NOTICE:  //모임 공지사항 설정/해제
                        Log.e(TAG, "PTC_IMS_MOIM_NOTICE: ");

                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {

                            //String msgId = data.body.getJson("params").getString("msgId");

                            //Log.e(TAG, "params: " + data.body.get("params").toString());
                            // Log.e(TAG,"noti: "+data.body.get("noti").toString());

                            //공지 등록 결과 처리
                            if (Activity_MoimTimeLineList.activity != null) {
                                Activity_MoimTimeLineList.activity.refreshItems();
                            }

                            /*setNotiDate(data.body.getJson("params").getString("msgId").toString()
                                    ,data.body.getJson("params").getString("noti").toString());*/


                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_EMOTION:    //7.23 모임 글 감정표현
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());


                            String msgId = data.body.getJson("params").getString("msgId");
                            String emt = data.body.getJson("params").getString("emt");

                            Log.e(TAG, "params emt: " + emt);


                            LinearLayout layout = (LinearLayout) mWrapAdapter.getHeadersView().get(0).findViewById(R.id.ll_head_detail);

                            //ll_head_detail
                            int likeCount = Integer.parseInt(((TextView) layout.findViewById(R.id.tv_like_count)).getText().toString());

                            if (emt.equals("1")) { //좋아요 ->좋아요취소 처리

                                ((TextView) layout.findViewById(R.id.tv_like)).setText(getString(R.string.like_cancle));

                                ((TextView) layout.findViewById(R.id.tv_like_count)).setText((likeCount + 1) + "");

//                                Toast.makeText(Activity_TimeLineDetail.this, "좋아요 완료", Toast.LENGTH_SHORT).show();
                            } else { // 좋아요취소 ->좋아요

                                ((TextView) layout.findViewById(R.id.tv_like)).setText(getString(R.string.like));

                                ((TextView) layout.findViewById(R.id.tv_like_count)).setText((likeCount - 1) + "");

//                                Toast.makeText(Activity_TimeLineDetail.this, "좋아요 취소 완료", Toast.LENGTH_SHORT).show();
                            }

                            //내부저장된 emt에 넣기.
                            interEmtcallback.setEmt(emt);


                            //arvalues.get(mnLikeNum).setLkCnt((Integer.parseInt(arvalues.get(mnLikeNum).getLkCnt())+1)+"");
                            //notifyDataSetChanged();
                            /*if (mContext instanceof Activity_MoimTimeLineList) {
                                ((Activity_MoimTimeLineList) mContext).reFreshAdapter();
                            } else {
                                Fragment_MoimTab1_Timeline.fragment.reFreshAdapter();
                            }*/

                        } else {
                            Toast.makeText(context, "좋아요 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });


    private LinearLayoutManager mLinearLayoutManager;

    private void setRecyclerView(VoBoardRead voBoardRead) {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mReplyRecyclerAdapter = new ReplyRecyclerAdapter(this, voBoardRead.getRpyLst());

        mWrapAdapter = new WrapAdapter<>(mReplyRecyclerAdapter);
        mWrapAdapter.adjustSpanSize(recyclerView);
        recyclerView.setAdapter(mWrapAdapter);

        mReplyRecyclerAdapter.notifyDataSetChanged();

        mLinearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLinearLayoutManager);

        addHeaderView(voBoardRead);


        mSwipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Log.e(TAG, "SwipeRefreshLayout");
                refreshItems();
            }
        });


        //댓글 최하단으로 이동.
        Handler mHandler = new Handler();
        mHandler.postDelayed(mMyTask, 1000); // 1초후에 실행

    }


    Runnable mMyTask = new Runnable() {
        @Override
        public void run() { // 실제 동작
            if (bGoToReply) {
                mLinearLayoutManager.scrollToPosition(mReplyRecyclerAdapter.getItemCount());
                Log.e(TAG, "scrollToPosition");
                bGoToReply = false;
            }
        }
    };


    /**
     * 헤더 붙이기
     * 상단 글내용
     *
     * @param voBoardRead
     */
    private void addHeaderView(VoBoardRead voBoardRead) {
        LayoutInflater inflater = LayoutInflater.from(this);
        layout = (LinearLayout) inflater.inflate(R.layout.header_moim_timeline_detail, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layout.setLayoutParams(params);
        mWrapAdapter.addHeaderView(layout);

        initView(layout, voBoardRead);
        setEventListener(layout, voBoardRead);
        setContent(layout, voBoardRead.getParams().getMsg());
    }


    /**
     * 웹 프리뷰 정보를 가져온다.
     *
     * @param url
     * @param
     */
    public void addOGTypeMemo(String url, final View m_layoutRoot) {
        // 입력받은 url에 해당하는 html을 요청한다.
        com.android.volley.RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // html에서 OGTag들을 가져온다.
                        Document doc = Jsoup.parse(response);
                        Elements ogTags = doc.select("meta[property^=og:]");
                        if (ogTags.size() <= 0) {
                            return;
                        }
                        // 필요한 OGTag를 추려낸다
                        for (int i = 0; i < ogTags.size(); i++) {
                            Element tag = ogTags.get(i);

                            String text = tag.attr("property");
                            if ("og:url".equals(text)) {
                                //ret.setOgUrl(tag.attr("content"));
                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURL)).setText(tag.attr("content"));
                            } else if ("og:image".equals(text)) {
                                //ret.setOgImageUrl(tag.attr("content"));

                                Log.e(TAG, "og:image: " + tag.attr("content"));

                                Glide.with(context).load(tag.attr("content"))
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)
                                        .dontAnimate().into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));

                            } else if ("og:description".equals(text)) {
                                //ret.setOgDescription(tag.attr("content"));
                                Log.e(TAG, "og:description: " + tag.attr("content"));
                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURLContent)).setText(tag.attr("content"));
                            } else if ("og:title".equals(text)) {
                                //ret.setOgTitle(tag.attr("content"));
                                Log.e(TAG, "og:title: " + tag.attr("content"));
                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURLTitle)).setText(tag.attr("content"));
                            }
                        }

                        // 필요한 작업을 한다.
                        //doSomething(ret);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse");
                m_layoutRoot.setVisibility(View.GONE);
            }

        });

        queue.add(stringRequest);
    }

    /**
     * 본문에 텍스트와  이미지 넣기
     *
     * @param layout
     * @param msg
     */
    private void setContent(LinearLayout layout, String msg) {

        LayoutInflater inflater = (LayoutInflater) Activity_TimeLineDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout llLayout = (LinearLayout) layout.findViewById(R.id.ll_content);
        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(msg);
        pictureList = new ArrayList<>();

        for (final VoMsgItem item : arMsgItem) {

            switch (item.getType()) {
                case "0": //text
                    final TextView tv_text = new TextView(Activity_TimeLineDetail.this);
                    tv_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));

                    tv_text.setTextColor(Util.getColor(this, R.color.gray_29));
                    tv_text.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, getResources().getDisplayMetrics()), 1.0f);
                    tv_text.setAutoLinkMask(Linkify.WEB_URLS);
                    tv_text.setLinkTextColor(Util.getColor(this, R.color.blue));

                    tv_text.setText(item.getData().replace("&nbsp;", ""));

                    tv_text.setTextSize(SharedObject.getProperty_int(Activity_TimeLineDetail.this, Constants.FONTSIZE, 16));//글자사이즈

                    llLayout.addView(tv_text);

                    //TODO 종목
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                gatherLinksForText(tv_text, item.getData());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });




                    /*AsyncTask<Void, Void, Void> mtsDataLoad = new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            //tvMessage.setText(mLinkableText);
                            tv_text.setText(mLinkableText);
                        }


                        @Override
                        protected Void doInBackground(Void... params) {
                            try {
                                gatherLinksForText(tv_text);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            tv_text.setText(mLinkableText);
                        }
                    };

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        mtsDataLoad.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                    else mtsDataLoad.execute();*/


                    //TODO URL이미지 가져오기 테스트
                    if (Util.isURL(item.getData())) {


                        URLSpan spans[] = tv_text.getUrls();
                        for (URLSpan span : spans) {
                            final String getUrlText = span.getURL();
                            final View m_layoutRoot = (ViewGroup) inflater.inflate(R.layout.item_url_preview, null);

                            //기존 망고 바나나 프리브 이미지
                           /*
                           openGraph(getUrlText,m_layoutRoot);
                           llLayout.addView(m_layoutRoot);
                            */



                            /*new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        //TODO 프리뷰 안되는게 많음 네이버만 잘됨.
                                        addOGTypeMemo(getUrlText, m_layoutRoot);
                                        llLayout.addView(m_layoutRoot);
                                } catch (Exception e) {
                                    }
                                }
                            });*/


                            /*String imageUrl = null;
                            Document doc= null;
                            try {
                                doc = Jsoup.connect(getUrlText).get();
                                Elements elements=doc.select("meta");
                                for(Element e: elements){
                                    imageUrl = e.attr("content");

                                    //OR more specifically you can check meta property.
                                    if(e.attr("property").equalsIgnoreCase("og:image")){
                                        imageUrl = e.attr("content");
                                        break;
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Glide.with(this).load(imageUrl).into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));
                            llLayout.addView(m_layoutRoot);*/


                            //TODO 일부 웹 프리뷰 안되는...
                            /*
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    try {*/

                                        /*final LinkPreviewTask linkPreviewTask = new LinkPreviewTask(new LinkListener() {
                                            @Override
                                            public void onWebLoading() {

                                                Log.e(TAG, "onWebLoading: "+getUrlText);
                                                m_layoutRoot.setVisibility(View.GONE);
                                            }
                                            @Override
                                            public void onWebFinishedLoading() {
                                                m_layoutRoot.setVisibility(View.VISIBLE);
                                            }
                                            @Override
                                            public void onWebLoaded(Web web) {
                                                Log.e(TAG, "onWebLoaded: "+getUrlText);
                                            }

                                            @Override
                                            public void onWebError(String message) {
                                                m_layoutRoot.setVisibility(View.GONE);
                                            }
                                        }, getUrlText );
                                        linkPreviewTask.execute();

                                        try {
                                            Glide.with(context).load(linkPreviewTask.get().getImageURL())
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)
                                                    .dontAnimate().into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));

                                            ((TextView) m_layoutRoot.findViewById(R.id.textViewURLTitle)).setText(linkPreviewTask.get().getTitle());
                                            ((TextView) m_layoutRoot.findViewById(R.id.textViewURLContent)).setText(linkPreviewTask.get().getDescription());
                                            ((TextView) m_layoutRoot.findViewById(R.id.textViewURL)).setText(linkPreviewTask.get().getUrl());

                                            final String url =linkPreviewTask.get().getUrl();
                                            Log.e(TAG, "linkPreviewTask.get().getTitle(): "+linkPreviewTask.get().getTitle());
                                            Log.e(TAG, "linkPreviewTask.get().getDescription(): "+linkPreviewTask.get().getDescription());
                                            Log.e(TAG, "linkPreviewTask.get().getUrl(): "+linkPreviewTask.get().getUrl());

                                            m_layoutRoot.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                                    startActivity(browserIntent);

                                                }
                                            });

                                            llLayout.addView(m_layoutRoot);

                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } catch (ExecutionException e) {
                                            e.printStackTrace();
                                        }*/





                                    /*} catch (Exception e) {
                                    }
                                }
                            });*/




                            /*final View m_layoutRoot = (ViewGroup) inflater.inflate(R.layout.item_url_preview, null);

                            LinkPreviewTask linkPreviewTask = new LinkPreviewTask(new LinkListener() {
                                @Override
                                public void onWebLoading() {

                                    Log.e(TAG, "onWebLoading: "+getUrlText);
                                    m_layoutRoot.setVisibility(View.GONE);
                                }
                                @Override
                                public void onWebFinishedLoading() {
                                    m_layoutRoot.setVisibility(View.VISIBLE);
                                }
                                @Override
                                public void onWebLoaded(Web web) {
                                    Log.e(TAG, "onWebLoaded: "+getUrlText);
                                }

                                @Override
                                public void onWebError(String message) {

                                }
                            }, getUrlText );
                            linkPreviewTask.execute();

                            try {

                                Glide.with(context).load(linkPreviewTask.get().getImageURL())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.ic_default_my_profile)
                                        .dontAnimate().into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));

                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURLTitle)).setText(linkPreviewTask.get().getTitle());
                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURLContent)).setText(linkPreviewTask.get().getDescription());
                                ((TextView) m_layoutRoot.findViewById(R.id.textViewURL)).setText(linkPreviewTask.get().getUrl());

                                Log.e(TAG, "linkPreviewTask.get().getTitle(): "+linkPreviewTask.get().getTitle());
                                Log.e(TAG, "linkPreviewTask.get().getDescription(): "+linkPreviewTask.get().getDescription());
                                Log.e(TAG, "linkPreviewTask.get().getUrl(): "+linkPreviewTask.get().getUrl());


                                llLayout.addView(m_layoutRoot);

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }*/


                            /*new LinkPreviewTask(new LinkListener() {
                                @Override
                                public void onWebLoading() {

                                    m_layoutRoot.setVisibility(View.GONE);
                                }

                                @Override
                                public void onWebFinishedLoading() {
                                    m_layoutRoot.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onWebLoaded(Web web) {
                                    Log.e(TAG, "onWebLoaded : getImageURL" + web.getImageURL() + " getDescription: " + web.getDescription() + " getTitle: " + web.getTitle());

                                    //titlePreview.setText(web.getTitle());
                                    //descriptionPreview.setText(web.getDescription());

                                    Glide.with(context).load(web.getImageURL())
                                            .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.ic_default_my_profile)
                                            .dontAnimate().into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));

                                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURLTitle)).setText(web.getTitle());
                                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURLContent)).setText(web.getDescription());
                                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURL)).setText(web.getUrl());
                                    llLayout.addView(m_layoutRoot);
                                }

                                @Override
                                public void onWebError(String message) {
                                    m_layoutRoot.setVisibility(View.GONE);
                                }
                            }, getUrlText).execute(); //링크로 검색된 URL가져오기*/
                            //LinearLayout mLay = (LinearLayout)m_layoutRoot.findViewById(com.lib.shi.common.R.id.popup_layout);


                        }
                    }


                    if (!TextUtils.isEmpty(item.getData())) {
                        strText += item.getData() + getString(R.string.nextline);    //글 합치기
                    }

                    break;

                case "1":  //image
                    final ImageView imgview = new ImageView(Activity_TimeLineDetail.this);

//                    Glide.with(Activity_TimeLineDetail.this).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData()).diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .skipMemoryCache(true)
//                            .dontAnimate()
//                            .into(new SimpleTarget<GlideDrawable>() {
//                                @Override
//                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
//
//                                    Bitmap bmap = ((GlideBitmapDrawable) resource).getBitmap();
//                                    //imgview.setLayoutParams(new LinearLayout.LayoutParams(bmap.getWidth(), bmap.getHeight()));
//                                    imgview.setImageBitmap(bmap);
//
//                                }
//                            });

                    //Log.e(TAG, "Constants.MOIM_TIMELINE_LIST_IMAGE_URL: " + Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData().replace("thumb_", ""));

                    //Glide.with(Activity_TimeLineDetail.this).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData().replace("thumb_", ""))

                    //Glide.with(Activity_TimeLineDetail.this).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData())
                    //        .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).dontAnimate().into(imgview);

                    /*Glide.with(Activity_TimeLineDetail.this)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                    + item.getData().substring(0,11)+"thumb_"+item.getData().substring(11,item.getData().indexOf("."))+".png")

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)//.placeholder(R.drawable.free)
                            .dontAnimate().into(imgview);*/


                   /* if (item.getData().contains("thumb_")) { //thumb_이 이미 있는경우.
                        Glide.with(Activity_TimeLineDetail.this)
                                .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                        + item.getData().substring(0, item.getData().indexOf(".")) + ".png")

                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .skipMemoryCache(true)
                                .dontAnimate().into(imgview);

                    } else {
                        Glide.with(Activity_TimeLineDetail.this)
                                .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                        + item.getData().substring(0, 11) + "thumb_" + item.getData().substring(11, item.getData().indexOf(".")) + ".png")

                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .skipMemoryCache(true)
                                .dontAnimate().into(imgview);
                    }*/

                    Glide.with(Activity_TimeLineDetail.this)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                    + item.getData())

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)
                            .dontAnimate().into(imgview);

                    imgview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    imgview.setScaleType(ImageView.ScaleType.FIT_XY);
                    imgview.setAdjustViewBounds(true);
                    llLayout.addView(imgview);

//                    pictureList.add(new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData().replace("thumb_", "").replace(".png", ".jpg"), item.getIdx()));
                    pictureList.add(new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData(), item.getIdx()));
                    //미리보기
                    imgview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            AlbumPicture picture = null;
//                            picture = new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData().replace("thumb_", "").replace(".png", ".jpg"), item.getIdx());
                            picture = new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData(), item.getIdx());
                            Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_PreviewImage.class);
                            intent.putExtra("picture", picture);
                            intent.putExtra("pictureList", (Serializable) pictureList);
                            intent.putExtra("type", "moim");
                            startActivity(intent);
                        }
                    });
                    break;

                case "3":   //file
                    final View view = (View) getLayoutInflater().inflate(R.layout.inflate_fileupload, null);
                    TextView tv = (TextView) view.findViewById(R.id.tv_filename);
                    final String fileName = item.getData();

                    tv.setText(fileName);

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO File 보여지는 다운로드 파일명 오류.... 서버에서 파일명 수정해야함..
                            Toast.makeText(Activity_TimeLineDetail.this, fileName + " 파일다운로드..", Toast.LENGTH_SHORT).show();
                            //loadingBar.setVisibility(View.VISIBLE);

                            String Save_folder = "CandleManDownload";
                            String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"; //+ Save_folder;

                            dThread = new DownloadThread(Constants.MOIM_DOWNLOAD_URL + item.getData(),
                                    savePath + "candleman_download.txt"); //item.getData());
                            dThread.start();
                        }
                    });
                    view.setPadding(0, 20, 0, 0);

                    llLayout.addView(view);
                    break;

                case "4":   //이모티콘
                    ImageView emoticon = new ImageView(Activity_TimeLineDetail.this);
                    emoticon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));

                    emoticon.setImageResource(getDrawableResourceByName(item.getData()));

                    llLayout.addView(emoticon);
                    break;
            }
        }
    }


    /**
     * 본문 텍스트에 종목이 있는지 찾기
     *
     * @param tv_text
     * @param text
     */
    public void gatherLinksForText(TextView tv_text, String text) {
        //find(tv_text, text, MTSItemMaster.getInstance().getItemCodes());
    }


    static final String[] hanJosa1 = {"은", "는", "이", "가", "을", "를", "다", "의", "와", "과", "도"
            , "만", "에", "야", "여", "아", "면", "께"
    };

    static final String[] hanJosa2 = {"께서", "에서", "이다"
            , "까지", "부터", "마저", "에서", "에게", "이여", "시여", "보다"
            , "일까", "인가", "여서", "만큼", "니까"
    };

    static final String[] hanJosa3 = {"이니까", "이어서"};


    /**
     * 종목명을 찾아 종목 링크를 걸어준다.
     *
     * @param
     * @param itemCodeList
     */
    public void find(TextView tv_text, String text, ArrayList<VoItemCode> itemCodeList) {

        tv_text.setMovementMethod(LinkMovementMethod.getInstance());

        SpannableString mLinkableText = new SpannableString(text);

        String tempText = "";
        String tmpLowerText = "";


        tempText = mLinkableText.toString();
        tmpLowerText = tempText.toLowerCase();
        for (int i = 0; i < itemCodeList.size(); i++) {

            final VoItemCode item = itemCodeList.get(i);
            String codeName = itemCodeList.get(i).getName().toLowerCase();

            boolean skipTxt = false;
            int start = tmpLowerText.indexOf(codeName);
            if (start > -1) { //종목명이 있을때

                int end = start + codeName.length();

                //바로앞이 문자가 오면안된다.
                if (start > 0) {
                    Character ch = tmpLowerText.charAt(start - 1);
                    if (Character.isLetter(ch)) {
                        skipTxt = true;
                    }
                }

                if (skipTxt == false && end + 1 <= tmpLowerText.length()) {
                    //뒤에 문자가 있는경우 특수문자(공백) 이거나 한글조사외에는 스킵한다.


                    Character ch = tmpLowerText.charAt(end);
                    if (Character.isLetter(ch)) {
                        //한글 조사 체크.
                        skipTxt = true;

                        String letter1 = String.valueOf(ch);
                        String letter2 = "";
                        String letter3 = "";
                        if (end + 2 <= tmpLowerText.length()) {
                            letter2 = tmpLowerText.substring(end, end + 2);
                        }
                        if (end + 3 <= tmpLowerText.length()) {
                            letter3 = tmpLowerText.substring(end, end + 3);
                        }

                        for (String value : hanJosa1) {
                            if (value.equals(letter1)) {
                                skipTxt = false;
                                break;
                            }
                        }

                        if (skipTxt == true && "".equals(hanJosa2) == false) {
                            for (String value : hanJosa2) {
                                if (value.equals(letter2)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }

                        if (skipTxt == true && "".equals(hanJosa3) == false) {
                            for (String value : hanJosa3) {
                                if (value.equals(letter3)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (skipTxt == false) {
                    mLinkableText.setSpan(new InternalURLSpan(mLinkableText.subSequence(start, end).toString(), item.getCode(), 2, tv_text), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(context, R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getName()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }


            if (skipTxt) {
                start = tempText.indexOf(item.getCode());
                if (start > -1) { //종목코드가 있을때
                    int end = start + item.getCode().length();
                    mLinkableText.setSpan(new InternalURLSpan(mLinkableText.subSequence(start, end).toString(), item.getCode(), 2, tv_text), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(context, R.color.gray_52)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getCode()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }
        }
        tv_text.setText(mLinkableText);

    }

    public Handler mChatRoomHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            Log.e(TAG, "mChatRoomHandler" + msg.what);
            switch (msg.what) {
                case 0:
                    stockDialog();
                    break;
                case 1:
                    LogTrace.E("handleMessage : null");
                    break;
            }

            return false;
        }
    });


    public String replaceText(String text) {
        String replaceText = "";
        for (int i = 0; i < text.length(); i++) {
            replaceText = replaceText + "-";
        }
        return replaceText;
    }

    /**
     * 종목 링크 클릭
     */
    private class InternalURLSpan extends ClickableSpan {
        private String clickedSpan;
        private String itemCode;
        int linkType;
        TextView tv;

        public InternalURLSpan(String clickedString, String itemCode, int linkType, TextView textView) {

            this.clickedSpan = clickedString;
            this.itemCode = itemCode;
            this.linkType = linkType;
            this.tv = textView;
        }

        @Override
        public void onClick(View textView) {
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    public void stockDialog() {
        Log.e(TAG, "stockDialog");

//        VoStockData vostockData = MessengerInterface.getInstance().voStockData;

        String itemCode = voStockData.getM_ItemCode();
        String itemName = voStockData.getM_ItemName();
        String currentValue = voStockData.getM_CurrentValue();
        String compareValue = voStockData.getM_CompareValue();
        String compareMark = voStockData.getM_CompareMark();
        String compareRate = voStockData.getM_CompareRate();
        String amount = voStockData.getM_Amount();

        final Dialog_Stock dialogStock = new Dialog_Stock(Activity_TimeLineDetail.this);
        dialogStock.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogStock.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogStock.show();
        dialogStock.setData(itemCode, itemName, currentValue, compareValue, compareRate, compareMark, amount);
    }


    public VoStockData voStockData;

    /**
     * 이미지 프리뷰
     *
     * @param url
     * @param m_layoutRoot
     */
    private void openGraph(final String url, final View m_layoutRoot) {

        new AsyncTask<Object, Void, Object>() {
            @Override
            protected Object doInBackground(Object[] params) {
                OpenGraphData data = new OpenGraphData();
                try {
                    final OpenGraph openGraph = new OpenGraph.Builder(url)
//                            .logger(new OpenGraph.Logger() {
//                                @Override
//                                public void log(String tag, String msg) {
//                                    // print log
//                                }
//                            })
                            .build();

                    data = openGraph.getOpenGraph();
                } catch (Exception e) {

                }
                return data;
            }

            @Override
            protected void onPostExecute(Object data) {
                super.onPostExecute(data);
                OpenGraphData resultData = (OpenGraphData) data;

                if (resultData.getTitle() != null) {
                    //ErrorController.showMessage("[CellChatDefaultText] net link called by async : url Key : " + url);
                    //cachedWebLinkData.put(url, resultData);
                    //linearLayoutURLPreview.setVisibility(View.VISIBLE);
                    //linearLayoutTime.setVisibility(View.GONE);


                    //textViewURLTitle.setText(resultData.getTitle());
                    // textViewURLContent.setText(resultData.getDescription());
                    //textViewURL.setText(resultData.getDomain());
                    String imageUrl = resultData.getImage();


                    Glide.with(context).load(imageUrl)
                            .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.ic_default_my_profile)
                            .dontAnimate().into((ImageView) m_layoutRoot.findViewById(R.id.imageViewURLTitle));

                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURLTitle)).setText(resultData.getTitle());
                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURLContent)).setText(resultData.getDescription());
                    ((TextView) m_layoutRoot.findViewById(R.id.textViewURL)).setText(resultData.getUrl());

                    Log.e(TAG, "resultData.getTitle(): " + resultData.getTitle());
                    Log.e(TAG, "resultData.getDescription(): " + resultData.getDescription());
                    Log.e(TAG, "resultData.getUrl(): " + resultData.getUrl());


                } else {
                    ErrorController.showMessage("[URL CHEKC ] is null " + resultData.getTitle());
                }

            }
        }.execute();
    }
    /*private class OGTag {
        String OgUrl;
        String OgImageUrl;
        String OgDescription;
        String OgTitle;

        public String getOgUrl() {
            return OgUrl;
        }

        public void setOgUrl(String ogUrl) {
            OgUrl = ogUrl;
        }

        public String getOgImageUrl() {
            return OgImageUrl;
        }

        public void setOgImageUrl(String ogImageUrl) {
            OgImageUrl = ogImageUrl;
        }

        public String getOgDescription() {
            return OgDescription;
        }

        public void setOgDescription(String ogDescription) {
            OgDescription = ogDescription;
        }

        public String getOgTitle() {
            return OgTitle;
        }

        public void setOgTitle(String ogTitle) {
            OgTitle = ogTitle;
        }
    }*/

    /**
     * 미리보기 화면으로 이동.
     *
     * @param
     * @param data
     * @param idx
     */
    /*private void intentPreview(Context context, String data, String idx) {
        AlbumPicture picture = null;
        picture = new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + data, idx);
        Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_PreviewImage.class);
        intent.putExtra("picture", picture);
        intent.putExtra("pictureList", (Serializable) pictureList);
        intent.putExtra("type", "moim");
        startActivity(intent);

    }*/

    /*private SimpleTarget target = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
            // do something with the bitmap
            // for demonstration purposes, let's just set it to an ImageView
            //imageView1.setImageBitmap( bitmap );
        }
    };*/

    /**
     * 상단 화면
     *
     * @param layout
     * @param voBoardRead
     */
    private void initView(LinearLayout layout, final VoBoardRead voBoardRead) {
        //모임 이름
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(voBoardRead.getParams().getMmNm());

        //id
        ((TextView) layout.findViewById(R.id.tv_id)).setText(voBoardRead.getParams().getUsNm());

        //시간
        ((TextView) layout.findViewById(R.id.tv_time)).setText(voBoardRead.getParams().getRegDt());

        //읽음 카운트
        ((TextView) layout.findViewById(R.id.tv_read_counter)).setText(voBoardRead.getParams().getRdCnt());

        //컨텐츠
        //((TextView) layout.findViewById(R.id.tv_cotent)).setText(voBoardRead.getParams().getMsg());

        //좋아요 카운트
        ((TextView) layout.findViewById(R.id.tv_like_count)).setText(voBoardRead.getParams().getLkCnt());

        //상단 모임이름
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(voBoardRead.getParams().getMmNm());

        //댓글
        ((TextView) layout.findViewById(R.id.tv_reply_count)).setText(voBoardRead.getParams().getRpyCnt());

        //조회수
        ((TextView) layout.findViewById(R.id.tv_read_counter)).setText(voBoardRead.getParams().getRdCnt());


        //프뢸썸네일
        /*Util.setMoimProfileGlide(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null)
                , voBoardRead.getParams().getPfImg()
                , (ImageView) layout.findViewById(R.id.imgv_profile));*/


        Util.setMoimProfileGlide2(this, voBoardRead.getParams().getRegUsr(), (ImageView) layout.findViewById(R.id.imgv_profile));


        //상단 주식종류 이미지
        setTopMmtgImg(voBoardRead.getParams().getMmTg());

        //메인 타임라인인지 모임타임라인에서 오는건지 구분
        Log.e(TAG, "intentTimeLineDetail Constants.MOIM_MAIN_TIMELINE: " + SharedObject.getProperty_boolean(this, Constants.MOIM_MAIN_TIMELINE, false));
        if (SharedObject.getProperty_boolean(this, Constants.MOIM_MAIN_TIMELINE, false)) {
            findViewById(R.id.ll_to_moim).setVisibility(View.VISIBLE);

            ((TextView) findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.tv_move_to_moim)).setText(context.getResources().getString(R.string.move_to_moim)); //이모임으로 이동.


        } else {
            if (SharedObject.getProperty_boolean(this, Constants.MOIM_CHAT, true)) {
                findViewById(R.id.ll_to_moim).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.tv_move_to_moim)).setText(context.getResources().getString(R.string.move_to_moim)); //이모임으로 이동.

                SharedObject.setProperty_boolean(context, Constants.MOIM_CHAT, false);
            } else {
                findViewById(R.id.ll_to_moim).setVisibility(View.GONE);
            }
        }


        // 가로사이즈 + 패딩으로 추가.

       /* findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.tv_move_to_moim).measure(0, 0);
        findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.iv_drawlayout).measure(0, 0);

        int nWidthMoveToMoim = (findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.tv_move_to_moim)).getMeasuredWidth();
        int nWidthStock = (findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.iv_drawlayout)).getMeasuredWidth();

        (findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.ll_title)).setPadding(0, 0, (nWidthMoveToMoim + nWidthStock + 30), 0);*/

        interEmtcallback = new InterEmtcallback() {
            @Override
            public void setEmt(String emtt) {

                voBoardRead.getParams().setEmtStt(emtt);
                Log.e(TAG, "interEmtcallback emtt: " + emtt);
            }
        };

        //좋아요 ,취소표시
        if (voBoardRead.getParams().getEmtStt().equals("1")) {
            //좋아요 인상태
            ((TextView) layout.findViewById(R.id.tv_like)).setText(R.string.like_cancle);
        } else {
            //감정없음
            ((TextView) layout.findViewById(R.id.tv_like)).setText(R.string.like);
        }


        //옵션 팝업
        layout.findViewById(R.id.imgb_option).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //옵션 다이얼로그
                final Dialog_TimeLineOption_2 dialog_TimeLineOption = new Dialog_TimeLineOption_2(Activity_TimeLineDetail.this
                        , voBoardRead.getParams().getNoti()
                        , voBoardRead.getParams().getRegUsr()
                        , voBoardRead.getParams().getUsrLv()
                        , Constants.SET_ENTRY_TYPE.moim_timelineDetail);
                dialog_TimeLineOption.show();

                dialog_TimeLineOption.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_TimeLineOption.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {


                            if (voBoardRead.getParams().getNoti().equals("0")) {   //공지가 아닐경우 공지 등록
                                moimNotice(voBoardRead.getParams().getMsgId(), "1");
                                ///Toast.makeText(mContext, "공지글등록", Toast.LENGTH_SHORT).show();
                            } else {
                                moimNotice(voBoardRead.getParams().getMsgId(), "0"); //공지 해제
                                //Toast.makeText(mContext, "공지글 내리기", Toast.LENGTH_SHORT).show();
                            }
                        } else if (i == R.id.tv_option1) {   //본문복사
                            Toast.makeText(Activity_TimeLineDetail.this, "본문내용을 복사하였습니다.", Toast.LENGTH_SHORT).show();
                            Util.copyToClipBoard(Activity_TimeLineDetail.this, strText.trim());

                        } else if (i == R.id.tv_option2) {

                            Util.shareDialog(Activity_TimeLineDetail.this, voBoardRead.getParams().getMsgId(),
                                    voBoardRead.getParams().getMsg(),
                                    voBoardRead.getParams().getNoti(),
                                    voBoardRead.getParams().getMmNm(),
//                                    voBoardRead.getParams().getTtl(),     // ???
                                    strText.trim(),
                                    "");


                        } else if (i == R.id.tv_option3) {
                            //Toast.makeText(mContext, "북마크", Toast.LENGTH_SHORT).show();

                        } else if (i == R.id.tv_option4) {
                            //Toast.makeText(mContext, "신고하기", Toast.LENGTH_SHORT).show();

                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID, voBoardRead.getParams().getMmId());
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_MSGID, voBoardRead.getParams().getMsgId());

                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_REGUSER, voBoardRead.getParams().getRegUsr());//작성자
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_TTL, voBoardRead.getParams().getTtl());//첫줄

                            intentContentReport();

                        } else if (i == R.id.tv_option5) {
                            //Toast.makeText(mContext, "이 모임글 보지 않기", Toast.LENGTH_SHORT).show();
                            //SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID,arvalues.get(position).getMmId());

                            //dialogBlock( arvalues.get(position).getMmNm());

                            ///moimEmotionRequest("1");

                        } else if (i == R.id.tv_option6) {

                            //Toast.makeText(mContext, "글수정하기", Toast.LENGTH_SHORT).show();

                            //SharedObject.setProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_WRITE_TYPE, false);
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_MODIFY);
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID, voBoardRead.getParams().getMmId());      //모임아이디
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_MSGID, voBoardRead.getParams().getMsgId());  //메시지 키값
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_MSG, voBoardRead.getParams().getMsg());      //모임 메시지
                            SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_NOTI, voBoardRead.getParams().getNoti());    //모임 공지
                            intentmModify();

                        } else if (i == R.id.tv_option7) {
                            Toast.makeText(Activity_TimeLineDetail.this, "글삭제", Toast.LENGTH_SHORT).show();
                            moimBoardDeleteRequest(voBoardRead.getParams().getMsgId(), voBoardRead.getParams().getMmId());
                        }
                    }
                });


            }
        });

    }


    /**
     * 상단 주식종류 이미지
     *
     * @param mmtg
     */
    private void setTopMmtgImg(String mmtg) {
        switch (mmtg) {
            case "1":   //1 : 주식
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_stock);
                break;

            case "2":   //2 : 국내파생
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_derivation);
                break;

            case "3":    //3 : 해외파생
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_global_derivation);
                break;

            case "4":   //4 : 해외주식
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_global_stock);
                break;

            case "5":   //5 : 금융상품
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_bank_product);
                break;

            case "6":   //6 : 기타
                findViewById(R.id.imgv_mmtg).setBackgroundResource(R.drawable.ic_mid_etc);
                break;
        }
    }


    private void setEventListener(LinearLayout layout, final VoBoardRead voBoardRead) {

        //프로필
        layout.findViewById(R.id.imgv_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentmProfile(voBoardRead.getParams().getRegUsr());
            }
        });

        findViewById(R.id.btn_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
                //Dialog_Camera.getMoimWriteCameraDialog(Activity_TimeLineDetail.this).show();

                String fileName = System.currentTimeMillis() + ".jpg";
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, fileName);
                fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Dialog_Camera.getProfileEditCameraDialog2_2(Activity_TimeLineDetail.this, fileUri);
            }
        });

        findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.GONE);

                mimgHash.clear();
                emoticon_num = null;
            }
        });


        //좋아요
        layout.findViewById(R.id.ll_like).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Like");
                if (voBoardRead.getParams().getEmtStt().equals("1")) {
                    moimEmotionRequest(voBoardRead.getParams().getMmId(), voBoardRead.getParams().getMsgId(), "0");//좋아요 취소
                } else {
                    moimEmotionRequest(voBoardRead.getParams().getMmId(), voBoardRead.getParams().getMsgId(), "1");//좋아요
                }
            }
        });

        //뒤로가기
        findViewById(R.id.imgb_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //이모임으로 이동
        findViewById(R.id.ll_to_moim).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "move to moim");
                intentMoimList();

                finish();
            }
        });


        //보내기 버튼
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "move to btn_send");

                //TODO
                //이미지, 이모콘티 일경우
                if (findViewById(R.id.ll_reply_background).getVisibility() == View.VISIBLE) {

                    moimWriteRequest("",
                            getJsonArrContent(), voBoardRead.getParams().getGrpNo(),
                            voBoardRead.getParams().getGrpNo(),
                            "0",
                            "0"
                    );

                    emoticon_num = null;
                } else {  // 장문체크

                    String strMsg = ((EditText) findViewById(R.id.edt_reply)).getText().toString().trim();
                    String strTitle = null;
                    if (TextUtils.isEmpty(strMsg)) {
                        Toast.makeText(Activity_TimeLineDetail.this, "댓글을 입력하세요", Toast.LENGTH_SHORT).show();
                    } else {

                        if (strMsg.length() >= 85) { //타이틀은 85자 이하로, 이상일경우 응답 오류.
                            strTitle = strMsg.substring(0, 84);
                        } else {
                            strTitle = strMsg;
                        }

                        //일반 댓글일때
                        moimWriteRequest(strTitle,
                                getJsonArrContent(), voBoardRead.getParams().getGrpNo(),
                                voBoardRead.getParams().getGrpNo(),
                                "0",
                                "0"
                        );
                    }
                }


            }
        });

        //공유 주석처리.
        /*layout.findViewById(R.id.imgb_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*Util.intentShare(Activity_TimeLineDetail.this, "["+voBoardRead.getParams().getMmNm()+"]"+voBoardRead.getParams().getTtl()
                        +"http://naver.com"
                );*//*
                // 공유 구분
                dialogShare(voBoardRead);
            }
        });*/

        findViewById(R.id.recyclerview).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Util.hideKeyboard(Activity_TimeLineDetail.this, (EditText) findViewById(R.id.edt_reply));
                        break;
                    case MotionEvent.ACTION_UP:
                        Util.hideKeyboard(Activity_TimeLineDetail.this, (EditText) findViewById(R.id.edt_reply));
                        break;

                }
                return false;
            }
        });

        //이모티콘
        findViewById(R.id.btn_emoticon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("clicked Emotion");
                View view = Activity_TimeLineDetail.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) Activity_TimeLineDetail.this.getSystemService(Activity_TimeLineDetail.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                findViewById(R.id.btn_emoticon).setSelected(!findViewById(R.id.btn_emoticon).isSelected());
//                checkSendBtn();

                if (findViewById(R.id.btn_emoticon).isSelected()) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.flEmoticonContainer, new Emoticon_Activity(1, Activity_TimeLineDetail.this));
                    fragmentTransaction.commit();
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.VISIBLE);


                } else {
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
                }
            }
        });


    }

    /**
     * 이모티콘화면 숨기기
     */
    private void emoticon_gone() {
        findViewById(R.id.btn_emoticon).setSelected(false);
        ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.GONE);
    }

    /**
     * 7.23 모임 글 감정표현
     *
     * @param mmId
     * @param msgId
     * @param emt   감정표현(int) 0:기존감정표현취소, 1:좋아요, 2:싫어요
     */
    private void moimEmotionRequest(String mmId, String msgId, String emt) {
        Moa.moimEmotionRequest(Activity_TimeLineDetail.this, mmId, msgId, emt, mMainActivityHandler);
    }

    /**
     * 공유하기 팝업
     * 기능 없어짐.
     *
     * @param voBoardRead
     */
    private void dialogShare(final VoBoardRead voBoardRead) {

        final Dialog_share mDialog = new Dialog_share(this);
        mDialog.show();

        mDialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //다른모임으로 공유
                mDialog.dismiss();

                int id = v.getId();
                if (id == R.id.ll_option0) {
                    Util.intentSelectMoim(Activity_TimeLineDetail.this,
                            voBoardRead.getParams().getMsgId(),
                            voBoardRead.getParams().getMsg(),
                            voBoardRead.getParams().getNoti()
                    );
                }

                /*switch (v.getId()) {
                    case R.id.ll_option0:
                        Toast.makeText(Activity_TimeLineDetail.this, "다른 모임으로 공유", Toast.LENGTH_SHORT).show();


                        intentSelectMoim(voBoardRead);
                        //TODO intent SelectMoim
                        //프리퍼런스 옵션 저장

                        break;

                    case R.id.ll_option1:
                        Toast.makeText(Activity_TimeLineDetail.this, "Facebook", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.ll_option2:
                        Toast.makeText(Activity_TimeLineDetail.this, "kakao", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.ll_option3:
                        Toast.makeText(Activity_TimeLineDetail.this, "Url 복사하기", Toast.LENGTH_SHORT).show();
                        break;
                }*/
            }
        });
    }


    /**
     * 프로필화면
     *
     * @param
     * @param
     */
    public void intentmProfile(String regUsr) {

        Log.e(TAG, "intentmProfile regUsr:" + regUsr);
        SharedObject.setProperty_string(this, Constants.MOIM_USER_PROFILE_ID, regUsr);    //글등록자

        Intent intent = new Intent(this, Activity_MoimProfile.class);
        startActivity(intent);
    }

    /**
     * 공유할 모임 선택 화면
     *
     * @param
     */
    /*private void intentSelectMoim(VoBoardRead voBoardRead) {
        SharedObject.setProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_PUBLIC, true); //공유할 모임 선택화면 구분

        SharedObject.setProperty_boolean(Activity_TimeLineDetail.this, Constants.MOIM_WRITE_TYPE, false);
        //SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID, voBoardRead.getParams().getMmId());      //모임아이디
        SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_MSGID, voBoardRead.getParams().getMsgId());  //메시지 키값
        SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_MSG, voBoardRead.getParams().getMsg());      //모임 메시지
        SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_NOTI, voBoardRead.getParams().getNoti());    //모임 공지


        Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_SelectMoim.class);
        startActivity(intent);
    }*/


    /**
     * 모임 타임라인으로 이동.
     */
    private void intentMoimList() {
        Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_MoimTimeLineList.class);
        //intent.putExtra("TimeLineItem", item);
        startActivity(intent);
    }


    public void RemoveAdapter(int index, int size) {
        Log.e(TAG, "RemoveAdapter index: " + index + " size: " + size);

        mReplyRecyclerAdapter.notifyItemRemoved(index);
        mReplyRecyclerAdapter.notifyItemRangeChanged(index, size);
        mReplyRecyclerAdapter.notifyDataSetChanged();
        mWrapAdapter.notifyDataSetChanged();

        LinearLayout layout = (LinearLayout) mWrapAdapter.getHeadersView().get(0).findViewById(R.id.ll_head_detail);

        ((TextView) layout.findViewById(R.id.tv_reply_count)).setText(size + "");
    }

    /**
     * 모임 글 리스트 어댑터를 새로고침한다.
     */
    public void reFreshAdapter() {

        mReplyRecyclerAdapter.notifyDataSetChanged();
        mWrapAdapter.notifyDataSetChanged();
    }


    /**
     * 사진촬영후 또는  겔러리 이미지 선택후 크롭
     *
     * @param imageUri
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_MoimOpen", "onActivityResult CAMERA_REQUEST");

            //startCropImageActivity(data.getData());
            startCropImageActivity(fileUri);
        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {

            Log.e("Activity_MoimOpen", "onActivityResult GALLERY_REQUEST");

            startCropImageActivity(data.getData());

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

                    addImageview(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 파일 업로드
                profileUpload(Activity_TimeLineDetail.this, resultUri.getPath());


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE " + resultCode);
            }

        } else if (requestCode == Constants.RESULT_REPLY_MODIFY && resultCode == RESULT_OK) {   //댓글 수정이후 처리

            requestBoardRead("0");
        }
    }


    int mAddImageCount = 0;   //추가됨 이미지 카운트

    /**
     * 글쓰기화면에
     * 겔러리,사진촬영으로 받아온 이미지를 넣고 하단에 Edittext를 추가한다
     *
     * @param bitmap 글수정시 오류.. 기존 이미지에는 mAddImageCount 값이 없음
     *               <p>
     *               XXX이미지를 넣은후 간격.
     */
    private void addImageview(Bitmap bitmap) {
        ImageView imgview = new ImageView(Activity_TimeLineDetail.this);
        ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_reply_image)).setImageBitmap(bitmap);


    }


    public void profileUpload(final Context context, final String path) {
        // TODO Auto-generated method stub

        Log.e(TAG, "profileUpload path:" + path);

        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                // TODO Auto-generated method stub
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.MOIM_IMAGE_UPLOAD);

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    ///entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, "userId", "")));
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }


            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("result").equals("success")) {

                            Log.e(TAG, "profileUpload onPostExecute " + jsonObject.toString());


                            mAtch = jsonObject.toString();


                            try {
                                JSONObject jsonObj = new JSONObject(mAtch);
                                String thumb_file = jsonObj.getString("thumb_file");

                                mimgHash.put(mAddImageCount + "", thumb_file);
                                Log.e(TAG, "profileUpload onPostExecute mAddImageCount: " + mAddImageCount + " save_file: " + thumb_file);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        //onUploadComplete(false);
                    }
                } else {
                    //onUploadComplete(false);
                }
            }
        }.execute();
    }


    /**
     * 이미지, 댓글 보낼때
     * JSon형식으로 만든다.
     *
     * @return
     */
    private String getJsonArrContent() {


        JSONArray jsonArray = new JSONArray();
        JSONObject jObject;
        jObject = new JSONObject();


        //댓글 텍스트
        if (!TextUtils.isEmpty(((EditText) findViewById(R.id.edt_reply)).getText().toString())) {
            try {
                jObject.put("idx", "0");
                jObject.put("type", "0");
                jObject.put("data", ((EditText) findViewById(R.id.edt_reply)).getText().toString());
                jsonArray.put(jObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        //이미지
        if (!mimgHash.isEmpty()) {
            jObject = new JSONObject();
            try {
                jObject.put("idx", "1");
                jObject.put("type", "1");
                jObject.put("data", mimgHash.get(mAddImageCount + ""));
                //jObject.put("data",mimgHash.get(view.getId()));
                jsonArray.put(jObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //이모티콘
        if (emoticon_num != null) {
            jObject = new JSONObject();
            try {
                jObject.put("idx", "2");
                jObject.put("type", "4");
                jObject.put("data", emoticon_num);
                //jObject.put("data",mimgHash.get(view.getId()));
                jsonArray.put(jObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Log.e(TAG, "getWriteContent jsonArray  " + jsonArray.toString());

        return jsonArray.toString();
    }


    /**
     * 7.16 모임 공지사항 설정/해제
     * noti     공지 여부(int)     0:일반, 1:공지
     */
    private void moimNotice(String msgId, String noti) {
        Moa.moimNotice(Activity_TimeLineDetail.this, msgId, noti, mMainActivityHandler);
    }

    /**
     * 7.17 모임 글 삭제
     *
     * @param
     */
    private void moimBoardDeleteRequest(String msgId, String mmid) {
        Moa.moimBoardDelete(Activity_TimeLineDetail.this, msgId, mmid, mMainActivityHandler);
    }

    /**
     * 컨텐츠 신고하기
     */
    private void intentContentReport() {
        Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_MoimContentReport.class);
        startActivity(intent);
    }

    /**
     * 글쓰기화면
     * 글수정하기 intent
     */
    private void intentmModify() {

        Intent intent = new Intent(Activity_TimeLineDetail.this, Activity_MoimWrite.class);
        startActivity(intent);
        
        

       /* if(Activity_TimeLineDetail.this instanceof Activity_MoimTimeLineList){  //모임타임라인

            Log.e(TAG, "intentmModify MOIM_NOTI: "+SharedObject.getProperty_string(mContext, Constants.MOIM_NOTI,"") );
            ((Activity_MoimTimeLineList)mContext).startActivityForResult(intent, Constants.RESULT_MOIM_WRITE);
        }else{
            //Fragment_MoimTab1_Timeline.fragment.refreshItems();
            mContext.startActivity(intent); //메인화면
        }*/

    }

    //ProgressBar loadingBar;
    DownloadThread dThread;

    /**
     * 다운로드
     */
    class DownloadThread extends Thread {
        String ServerUrl;//=Constants.MOIM_TIMELINE_LIST_IMAGE_URL;
        String LocalPath;

        DownloadThread(String serverPath, String localPath) {

            Log.e(TAG, "DownloadThread serverPath:" + serverPath + " localPath: " + localPath);

            ServerUrl = serverPath;
            LocalPath = localPath;
        }

        @Override
        public void run() {
            URL imgurl;
            int Read;
            try {
                imgurl = new URL(ServerUrl);
                HttpURLConnection conn = (HttpURLConnection) imgurl
                        .openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                InputStream is = conn.getInputStream();
                File file = new File(LocalPath);
                FileOutputStream fos = new FileOutputStream(file);
                for (; ; ) {
                    Read = is.read(tmpByte);
                    if (Read <= 0) {
                        break;
                    }
                    fos.write(tmpByte, 0, Read);
                }
                is.close();
                fos.close();
                conn.disconnect();

            } catch (MalformedURLException e) {
                Log.e("ERROR1", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR2", e.getMessage());
                e.printStackTrace();
            }
            mAfterDown.sendEmptyMessage(0);
        }
    }

    Handler mAfterDown = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage " + msg);

            // TODO Auto-generated method stub
            //loadingBar.setVisibility(View.GONE);
            // 파일 다운로드 종료 후 다운받은 파일을 실행시킨다.
            //showDownloadFile();
        }

    };

    /**
     * 새로고침
     */
    public void refreshItems() {
        //MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");
        requestBoardRead("0");
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
    }


}
