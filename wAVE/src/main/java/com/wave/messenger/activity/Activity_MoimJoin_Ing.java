package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.wave.messenger.adapter.MoimJoinIng_Adapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimList;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 9..
 */

public class Activity_MoimJoin_Ing extends BaseActivity {

    String TAG = this.getClass().getSimpleName();

    private MoimJoinIng_Adapter moimJoinIng_adapter;
    private RecyclerView mRecyclerView;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moimjoin_ing);

        mRecyclerView = (RecyclerView) findViewById(R.id.lv_join_ing);

        ((RelativeLayout) findViewById(R.id.ll_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        moimListRequest();

    }

    public void moimListRequest() {
        Moa.moimListRequest(Activity_MoimJoin_Ing.this, "4", mMainActvtHandler);
    }


    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);
                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST:    //모임리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoMoimList voMoimList;
                            Gson gson = new Gson();
                            voMoimList = gson.fromJson(data.body.toString(), VoMoimList.class);
//                            setRecyclerView(voMoimHiddenList);
                            setMoimListDate(voMoimList.getParams());
                        }
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private void setMoimListDate(ArrayList<VoMoimList> params) {

        ArrayList<VoMoimList> arVoMoimList= new ArrayList<>();

        for (VoMoimList item : params) {
                arVoMoimList.add(item);
        }

        Collections.sort(arVoMoimList, myComparator);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        moimJoinIng_adapter = new MoimJoinIng_Adapter(Activity_MoimJoin_Ing.this, arVoMoimList);
        mRecyclerView.setAdapter(moimJoinIng_adapter);

    }

    /**
     * MmOrd 순서대로 상단정렬
     */
    public final static Comparator<VoMoimList> myComparator = new Comparator<VoMoimList>() {

        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(VoMoimList o1, VoMoimList o2) {

            return collator.compare(o2.getMmOrd(), o1.getMmOrd());
        }
    };
}
