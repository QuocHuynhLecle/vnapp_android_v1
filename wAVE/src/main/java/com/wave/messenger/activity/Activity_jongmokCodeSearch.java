package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wave.messenger.adapter.JongmokCodeSearchAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by aveapp on 2017. 9. 1..
 */

public class Activity_jongmokCodeSearch extends Activity {

    public EditText editTextFind;
    String TAG = this.getClass().getName();

    //private JongmokCodeSearchAdapter adapter;
    private ListView ListView;
    private String roomName;
    private LinearLayout ll_nosearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_jongmokcodesearch);

        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.textViewTitle)).setText("종목 채팅방");
        editTextFind = (EditText) findViewById(R.id.et_search);

        editTextFind.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        editTextFind.setInputType(InputType.TYPE_CLASS_TEXT);
        ListView = (ListView) findViewById(R.id.ListView);
        ll_nosearch = (LinearLayout) findViewById(R.id.ll_nosearch);

        ((ImageView) findViewById(R.id.imageButtonBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyboard(Activity_jongmokCodeSearch.this,editTextFind);
                finish();
            }
        });

        editTextFind.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (editTextFind.getText().toString().equals("")) {
                    ll_nosearch.setVisibility(View.VISIBLE);
                } else {
                    ll_nosearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM:
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Intent mIntent;
                            Statics.ROOMINFO = getRoomInfo(data, roomName);
                            mIntent = new Intent(Activity_jongmokCodeSearch.this, Activity_ChatRoom.class);
                            startActivity(mIntent);

                        } else {
//                            UUID uuid = UUID.randomUUID();
//                            String roomId = uuid.toString();
//                            PacketBody body = new PacketBody();
//                            JSONObject value = new JSONObject();
//                            value.put("userId", MessengerInfo.getUserId());
//                            value.put("roomId", roomId);
//                            value.put("roomType", "1");
//                            value.put("inviteUser", toInvite);
//
//                            body.put("params", value);
//                            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
//                                    PacketTypes.PTC_IMS_CHATROOM_CREATE, body);
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private VoChatList getRoomInfo(MOAData data, String roomName) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            info.setRoom_name(roomName);
            info.setRoom_type("8");

        }
        return info;
    }

}
