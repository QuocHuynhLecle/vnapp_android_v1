package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.wave.messenger.Emoticon.Emoticon_Activity;

import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.massenger.piggy.R;

import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardRead;
import com.wave.messenger.vo.VoMsgItem;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 3..
 */

public class Activity_Re_Reply extends BaseActivity {

    String TAG = getClass().getSimpleName();
    HashMap<String, String> mimgHash = new HashMap<>();   //이미지의 getView값과 이미지페스를 저장
    private String emoticon_num;
    public static Context context;

    private VoBoardRead.VoReadReply voReadReply;
    private boolean re_reply = false;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_reply);

        ((TextView) findViewById(R.id.tv_re_reply)).setVisibility(View.VISIBLE);
        findViewById(R.id.ll_detail_top_Bar).findViewById(R.id.ll_to_moim).setVisibility(View.GONE);

        context = this;

        //requestBoardRead();
        init();

        setPermit();

        Intent i = getIntent();

        voReadReply = (VoBoardRead.VoReadReply) i.getSerializableExtra("rereply");
        setRe_reply(voReadReply);
        initView(voReadReply);
        setEventListener(voReadReply);
        setContent(voReadReply.getMsg());
    }

    private void init() {

        findViewById(R.id.scroll_view).post(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) findViewById(R.id.scroll_view)).scrollTo(0, 0);
            }
        });

    }

    public void setEmoticon(String emoticon) {

        this.emoticon_num = String.valueOf(emoticon);
        ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_reply_image)).setImageResource(getDrawableResourceByName(emoticon));

    }
    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(this,str);
        String packageName = getPackageName();
        return getResources().getIdentifier(emoticon, "drawable", packageName);
    }
    /**
     * 대댓글 쓰기
     *
     * @param voReadReply
     */
    public void setRe_reply(VoBoardRead.VoReadReply voReadReply) {
        this.voReadReply = voReadReply;
        re_reply = true;
        Util.showSoftKeyboard(this, findViewById(R.id.edt_reply));
        ((EditText) findViewById(R.id.edt_reply)).setHint("덧글을 남겨주세요.");
        ((ImageButton) findViewById(R.id.btn_image)).setVisibility(View.GONE);
    }


    private void setPermit() {
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getRpyTy(this))) {
            findViewById(R.id.ll_reply).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_reply).setVisibility(View.GONE);
        }

    }


    private void requestBoardRead() {
        Moa.moimBoardReadReplyRequest(Activity_Re_Reply.this, mMainActivityHandler);
    }

    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_BOARD_READ:   //7.19 모임 글 조회
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoBoardRead.VoReadReply voBoardRead;
                            Gson gson = new Gson();

                            voBoardRead = gson.fromJson(data.body.toString(), VoBoardRead.VoReadReply.class);

                        }


                        break;

                    case PacketTypes.PTC_IMS_MOIM_BOARD_WRITE:    // 댓글쓰기
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);


                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Util.hideKeyboard(Activity_Re_Reply.this, (EditText) findViewById(R.id.edt_reply));
                            Toast.makeText(Activity_Re_Reply.this, "덧글 완료", Toast.LENGTH_SHORT).show();

                            ((EditText) findViewById(R.id.edt_reply)).setText("");
                            ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.GONE);
                            mimgHash.clear();
                            re_reply = false;
                            //((EditText) findViewById(R.id.edt_reply)).setHint("댓글을 남겨주세요.");
                            //((ImageButton) findViewById(R.id.btn_image)).setVisibility(View.VISIBLE);
                            //emoticon_gone();
                            //requestBoardRead();

                            Activity_TimeLineDetail.activity.refreshItems();
                            //re_reply = false;
                            finish();


                        } else {
                            Toast.makeText(Activity_Re_Reply.this, "덧글 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private void emoticon_gone() {
        findViewById(R.id.btn_emoticon).setSelected(false);
        ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
    }

    private void initView(VoBoardRead.VoReadReply voBoardRead) {

        //id
        ((TextView) findViewById(R.id.tv_id)).setText(voBoardRead.getUsNm());

        //시간
        ((TextView) findViewById(R.id.tv_time)).setText(voBoardRead.getRegDt());

        //읽음 카운트
        ((TextView) findViewById(R.id.tv_read_counter)).setText(voBoardRead.getRdCnt());

        //썸네일

        Util.setMoimProfileGlide(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null)
                , voBoardRead.getPfImg()
                , (ImageView) findViewById(R.id.imgv_profile));

        findViewById(R.id.imgv_mmtg).setVisibility(View.GONE);

        //메인 타임라인인지 모임타임라인에서 오는건지 구분
        /*Log.e(TAG, "intentTimeLineDetail Constants.MOIM_MAIN_TIMELINE: " + SharedObject.getProperty_boolean(this, Constants.MOIM_MAIN_TIMELINE, false));
        if (SharedObject.getProperty_boolean(this, Constants.MOIM_MAIN_TIMELINE, false)) {
            findViewById(R.id.ll_to_moim).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_to_moim).setVisibility(View.GONE);
        }*/

        //댓글달기 인지 구분

    }

    private void setContent(String msg) {

        LinearLayout llLayout = (LinearLayout) findViewById(R.id.ll_content);


        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(msg);

        for (final VoMsgItem item : arMsgItem) {

            switch (item.getType()) {
                case "0": //text
                    TextView tv_text = new TextView(Activity_Re_Reply.this);
                    tv_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    tv_text.setText(item.getData());
                    tv_text.setTextColor(Util.getColor(this, R.color.gray_29));
                    tv_text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
                    tv_text.setLineSpacing(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 2, getResources().getDisplayMetrics()), 1.0f);
                    llLayout.addView(tv_text);
                    break;

                case "1":  //image
                    ImageView imgview = new ImageView(Activity_Re_Reply.this);
                    imgview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    Glide.with(Activity_Re_Reply.this)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData()).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)
                            .dontAnimate().into(imgview);

                    llLayout.addView(imgview);
                    break;

                case "4":
                    ImageView emoticon = new ImageView(Activity_Re_Reply.this);
                    emoticon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    emoticon.setImageResource(Integer.parseInt(item.getData()));

                    llLayout.addView(emoticon);
                    break;
            }
        }


    }

    private void setEventListener(final VoBoardRead.VoReadReply voBoardRead) {
        findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LinearLayout) findViewById(R.id.ll_reply_background)).setVisibility(View.GONE);

                mimgHash.clear();
                emoticon_num = null;
            }
        });

        //뒤로가기
        findViewById(R.id.imgb_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "move to btn_send");


                String strMsg = ((EditText) findViewById(R.id.edt_reply)).getText().toString().trim();
                if (TextUtils.isEmpty(strMsg)) {
                    Toast.makeText(Activity_Re_Reply.this, "댓글을 입력하세요", Toast.LENGTH_SHORT).show();
                } else {

                    //댓글의 댓글일때
                    moimWriteRequest(strMsg,
                            getJsonArrContent(),
                            voReadReply.getGrpNo(),  //선택한 댓글의 msgNo
                            voReadReply.getMsgNo(), //선택한 댓글의 BbsDph
                            voReadReply.getBbsDph(),  //선택한 댓글의 BbsOrd
                            voReadReply.getBbsOrd()  //선택한 댓글의 BbsOrd

                    );


                }


            }
        });

        findViewById(R.id.btn_emoticon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("clicked Emotion");
                View view = Activity_Re_Reply.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) Activity_Re_Reply.this.getSystemService(Activity_Re_Reply.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                findViewById(R.id.btn_emoticon).setSelected(!findViewById(R.id.btn_emoticon).isSelected());

                if (findViewById(R.id.btn_emoticon).isSelected()) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.flEmoticonContainer, new Emoticon_Activity(2, Activity_Re_Reply.this));
                    fragmentTransaction.commit();
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.VISIBLE);


                } else {
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
                }
            }
        });


    }

    private void moimWriteRequest(String message, String msg, String grpNo, String prntNo, String bbsDph, String bbsOrd) {
        Moa.moimBoardWriteRequest(Activity_Re_Reply.this, message, msg, grpNo, prntNo, bbsDph, bbsOrd, "0", "","","",mMainActivityHandler);
    }

    private String getJsonArrContent() {


        JSONArray jsonArray = new JSONArray();

        JSONObject jObject;

        jObject = new JSONObject();

        if (!TextUtils.isEmpty(((EditText) findViewById(R.id.edt_reply)).getText().toString())) {
            try {
                jObject.put("idx", "0");
                jObject.put("type", "0");
                jObject.put("data", ((EditText) findViewById(R.id.edt_reply)).getText().toString());
                jsonArray.put(jObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (emoticon_num != null) {
            jObject = new JSONObject();
            try {
                jObject.put("idx", "2");
                jObject.put("type", "4");
                jObject.put("data", emoticon_num);
                //jObject.put("data",mimgHash.get(view.getId()));
                jsonArray.put(jObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.e(TAG, "getWriteContent jsonArray  " + jsonArray.toString());

        return jsonArray.toString();
    }
}
