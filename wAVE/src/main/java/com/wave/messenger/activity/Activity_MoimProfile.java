package com.wave.messenger.activity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoUserProfile;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAData;
import moa.android.api.MOALog;


/**
 * 모임 프로필 화면
 */
public class Activity_MoimProfile extends BaseActivity {

    String TAG = getClass().getSimpleName();

    static Activity_MoimProfile activity;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_profile);

        initView();
        setOnClickEvent();

        activity = this;

        moimUserProfileRequest();
    }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        /*findViewById(R.id.btn_set_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intentSetMoimProfile();


                FragmentManager manager = getSupportFragmentManager();
                Fragment_Main fragment = (Fragment_Main) manager.findFragmentById(R.id.main_content);
                fragment.setProfile();

                finish();
            }
        });*/

        //이프로필은 누가 볼수있나요
        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog_Text dialog = new Dialog_Text(Activity_MoimProfile.this, getString(R.string.profile_info), getString(R.string.profile_info_title), Constants.DIALOG_BUTTON_TYPE.confirm);
                dialog.setConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });



    }


    private void initView() {

        ((ImageButton) findViewById(R.id.imgb_right)).setImageResource(R.drawable.ic_question);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
        ((ImageButton) findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.ic_close_press); //뒤로가기 이미지


        //자신인지 회원인지구분
        if (Util.isMyAccount(this)) { //프로필설정
            findViewById(R.id.ll_set_profile).setVisibility(View.GONE);
            findViewById(R.id.ll_chat).setVisibility(View.GONE);
            //프로필 설정은 채팅 프로필 사용으로 주석처리
//            findViewById(R.id.imgb_right).setVisibility(View.VISIBLE);
        } else {                      //1:1대화 //다른사람프로필
            findViewById(R.id.ll_set_profile).setVisibility(View.GONE);
            findViewById(R.id.ll_chat).setVisibility(View.VISIBLE);
        }


    }


    /**
     * 7.30 모임 사용자 프로필
     * <p>
     * userIduserId 선택한아이디
     * mmIdmmId 모임아이디
     */
    public void moimUserProfileRequest() {

        //MOALog.w(TAG + " MoimUserinfoRequest: ");

        String userId = SharedObject.getProperty_string(Activity_MoimProfile.this, Constants.MOIM_USER_PROFILE_ID, null);
        String mmId = SharedObject.getProperty_string(Activity_MoimProfile.this, Constants.MOIM_ID, null);


        //Log.e(TAG, "moimUserProfileRequest userId: " + userId + " mmId:" + mmId);
        Moa.moimUserProfileRequest(Activity_MoimProfile.this, userId, mmId, mMainActivityHandler);
    }

    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case 0x00080033:    //7.30 모임 사용자 프로필
                        strResult = (String) data.body.get("result");
                        //Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            //Log.e(TAG, "params: " + data.body.get("params").toString());
                            //Log.e(TAG, "notiLst: " + data.body.get("notiLst").toString());

                            try {   //gson파싱
                                VoUserProfile voUserProfile;
                                Gson gson = new Gson();
                                voUserProfile = gson.fromJson(data.body.toString(), VoUserProfile.class);

                                setScreen(voUserProfile);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 유져 레벨 구분
     * 리더 공동리더 멤버
     */
    private void setUserLevel(int v1, int v2, int v3) {
        findViewById(R.id.ll_usr_level1).setVisibility(v1);
        findViewById(R.id.ll_usr_level2).setVisibility(v2);
        //findViewById(R.id.ll_usr_level3).setVisibility(v3); //멤버 표시는 없음
    }

    /**
     * 서버에서 받아온 정보를 화면에 뿌림
     *
     * @param
     */
    private void setScreen(final VoUserProfile voUserProfile) {


        //0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)
        switch (voUserProfile.getParams().getUsrLv()) {
            case "0":
                setUserLevel(View.GONE, View.GONE, View.VISIBLE);
                break;
            case "1":
                setUserLevel(View.VISIBLE, View.GONE, View.GONE);
                break;
            case "2":
                setUserLevel(View.GONE, View.VISIBLE, View.GONE);
                break;
        }


        //SnglTy 모임 멤버간 1:1채팅 가능여부(int) 0:불가, 1:가능(기본)
        //usrLv  사용자 레벨(int) 0:일반, 1:리더, 2:공동리더


        //TODO 대화권한 설정 부분 숨겨있어서 일단 다보이도록 처리.
        //Log.e(TAG,"voUserProfile.getParams().getSnglTy(): "+voUserProfile.getParams().getSnglTy());
            /*if(voUserProfile.getParams().getSnglTy().equals("0")) { //불가 snglTy대화권한
                findViewById(R.id.ll_chat).setVisibility(View.GONE);
            }else{
                findViewById(R.id.ll_chat).setVisibility(View.VISIBLE);
            }*/

        //이름
        ((TextView) findViewById(R.id.tv_user_id)).setText(voUserProfile.getParams().getUsNm());


        //회원 권한에 의한 표시  회원구분, 권한값구분
        //생년월일
        ((TextView) findViewById(R.id.tv_birthday)).setText(voUserProfile.getParams().getBrthDy());
        //전화번호   MessengerInfo.getUserPhoneNumber(this)
        //숨김처리
        //((TextView) findViewById(R.id.tv_phone)).setText(voUserProfile.getParams().getTel());

        //모임가입일
        ((TextView) findViewById(R.id.tv_regdt)).setText(voUserProfile.getParams().getRegDt());

        ((TextView) findViewById(R.id.tv_message)).setText(voUserProfile.getParams().getPfSbj());

        Log.e(TAG, "voUserProfile.getParams().getPfImg() " + voUserProfile.getParams().getPfImg());
        //모임 프로필 썸네일
        //Util.setMoimProfileGlide(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null), voUserProfile.getParams().getPfImg(), (ImageView) findViewById(R.id.imgv_profile));


        //설정에 수정된 프로필 이미지
        String friend_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + SharedObject.getProperty_string(this, Constants.MOIM_USER_PROFILE_ID, null);  // + "&date=" + voUserProfile.getParams().getPfImg();
        Log.e(TAG,"friend_url: "+friend_url);

        Glide.with(Activity_MoimProfile.this).load(friend_url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).placeholder(R.drawable.ic_default_profile)
                .dontAnimate().into((ImageView) findViewById(R.id.imgv_profile));



        //프로필설정
        findViewById(R.id.btn_set_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedObject.setProperty_string(Activity_MoimProfile.this, Constants.UsNm, voUserProfile.getParams().getUsNm());        //이름
                SharedObject.setProperty_string(Activity_MoimProfile.this, Constants.pfSbj, voUserProfile.getParams().getPfSbj());      //싱태메시지
                SharedObject.setProperty_string(Activity_MoimProfile.this, Constants.pfImg, voUserProfile.getParams().getPfImg());      //프로필이미지
                SharedObject.setProperty_string(Activity_MoimProfile.this, Constants.pfImgTy, voUserProfile.getParams().getPfImgTy());  //프로필 이미지 종류

                intentSetMoimProfile();
            }
        });

        findViewById(R.id.btn_written_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentWrittenTimelineList();
            }
        });

        findViewById(R.id.btn_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentChat(voUserProfile.getParams());
            }
        });
    }


    /**
     * 작성한 글보기 타임라인
     */
    private void intentWrittenTimelineList() {
        Intent intent = new Intent(Activity_MoimProfile.this, Activity_WrittenTimeLineList.class);
        startActivity(intent);

    }


    //모임설정 화면
    private void intentSetMoimProfile() {
        Intent intent = new Intent(Activity_MoimProfile.this, Activity_SetMoimProfile.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_SetMoimProfile", "onActivityResult CAMERA_REQUEST");

            if(data==null){
                Log.e(TAG,"onActivityResult data==null ");
            }else{
                Log.e(TAG,"onActivityResult data NOT null ");
                //startCropImageActivity(data.getData());
            }
        }
    }



    /**
     * 채팅 인탠트
     * @param params
     */
    private void intentChat(VoUserProfile.VoUserProfileItem params) {
        //TODO 채팅 연결
        Toast.makeText(this, "1:1대화", Toast.LENGTH_SHORT).show();

        String friendProfileId = SharedObject.getProperty_string(Activity_MoimProfile.this, Constants.MOIM_USER_PROFILE_ID, null);

        VoFriendList friend= new VoFriendList();
        friend.setUserId(friendProfileId);
        friend.setUserName(params.getUsNm());

        ChatListDbHelper listDb = LocalDB.getChatListDbHelper(Activity_MoimProfile.this);
        List<VoFriendList> friends = new ArrayList<>();
        VoChatList chatRoomInfo = new VoChatList();
        String roomId;

        UsersDbHelper userDb = LocalDB.getUsersDbHelper(Activity_MoimProfile.this);
        ArrayList<String> roomIdList = listDb.selectSingleRoom();
        roomId = userDb.existUserCaht(roomIdList, friendProfileId);
        chatRoomInfo.setRoom_type("0");

        VoFriendList my = new VoFriendList();
        my.setUserId(MessengerInfo.getUserId(Activity_MoimProfile.this));
        my.setUserName(MessengerInfo.getUserName(Activity_MoimProfile.this));
        my.setRealUserName(MessengerInfo.getRealUserName(Activity_MoimProfile.this));

        friends.add(my);
        friends.add(friend);

        chatRoomInfo.setFriends(friends, Activity_MoimProfile.this);

        if (!TextUtils.isEmpty(roomId)) {
            chatRoomInfo.setRoomId(roomId);
        }
        Statics.ROOMINFO = chatRoomInfo;

        BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

        Intent mIntent = new Intent(Activity_MoimProfile.this, Activity_ChatRoom.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(mIntent);

        finish();
    }


}
