package com.wave.messenger.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.wave.massenger.piggy.R;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public abstract class BaseActivity extends FragmentActivity implements IBaseActivity {


    static ArrayList<BaseActivity> openedActivityList;
    public static boolean isLocked = false;

    Object tag = null;
    static BaseActivity topActivity = null;
    static boolean isTop = false;

    public static boolean isTop() {
        boolean result = false;
        try {
            if (isTop && topActivity != null) {
                result = topActivity.getWindow().getDecorView().getRootView().isShown();
            }
        } catch (Exception e) {

        }

        return result;
    }

    public BaseActivity() {
        super();
    }

    public void sendMessage(int nMsg, int nArg1, int nArg2) {
    }

    public void sendMessage(int nMsg, int nArg1, int nArg2, Object obj) {
    }

    public static void setTop(boolean isTop) {
        BaseActivity.isTop = isTop;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    abstract public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2);

    public void clearBackStack() {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (BaseActivity.openedActivityList == null) {
                BaseActivity.openedActivityList = new ArrayList<BaseActivity>();
            }
            BaseActivity.openedActivityList.add(this);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isTop = true;
        topActivity = this;

        ActivityHelper.setTop(true);
        ActivityHelper.setTopActivity(this);

        if (this.getClass().equals(Activity_CandleMain.class)) {
            isTop = false;
            ActivityHelper.setTop(false);
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        BaseActivity.openedActivityList.remove(this);
        if (this.getClass().equals(Activity_CandleMain.class)) {
            isTop = false;
        }
    }

    public Activity getActivity() {
        return this;
    }

    public AlertDialog.Builder getAlert() {
        int version = Integer.parseInt((Build.VERSION.RELEASE).substring(0, 1));
        AlertDialog.Builder alert = null;
        if (version >= 3) {
            alert = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        alert.setTitle(R.string.app_name);
        return alert;
    }

    @Override
    public void addFragment(int layoutId, BaseFragment baseFragment, String name) {
        clearBackStack();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(layoutId, baseFragment, name);
        ft.addToBackStack(name);
        ft.commit();
    }
}
