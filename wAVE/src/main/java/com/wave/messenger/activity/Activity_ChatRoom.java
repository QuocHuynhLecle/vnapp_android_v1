package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.h1comz.ccast.CCastClient;
import com.h1comz.ccast.CCastListener;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Emoticon.Emoticon_Activity;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.ChatRoomAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.db.UsersReadDbHelper;
import com.wave.messenger.db.greendao.model.ChatEntity;
import com.wave.messenger.db.greendao.model.ChatModel;
import com.wave.messenger.db.greendao.model.LuckyBagModel;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.dialog.Dialog_Signal;
import com.wave.messenger.dialog.Dialog_Stock;
import com.wave.messenger.fragment.ChatRoom.Fragment_OpenNavigation;
import com.wave.messenger.fragment.ChatRoom.Fragment_RightNavigation;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceChart;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceCurrent;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceInterest;
import com.wave.messenger.fragment.FragmentSlideMenu_FinanceTotal;
import com.wave.messenger.fragment.FragmentSlideMenu_Potfolio;
import com.wave.messenger.fragment.FragmentSlideMenu_StockSearch;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.fragment.SlideMenu.FragmentSlideMenu_MTS;
import com.wave.messenger.fragment.SlideMenu.FragmentSlideMenu_Main;
import com.wave.messenger.helper.DrawerLayoutEventHelper;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.helper.UIChatTopbarHelper;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.mvp.ChatRoom.ChatRoomListener;
import com.wave.messenger.mvp.ChatRoom.IChatView;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.sdk.Util.ImageUploadData;
import com.wave.messenger.share.HMMessengerShare;
import com.wave.messenger.util.AbstractAdapter.TextWatcherAbstract;
import com.wave.messenger.util.AndroidLayoutResize;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.FileUtil;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoChatRoom;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoPortfolioList;
import com.wave.messenger.vo.VoStockData;
import com.wave.messenger.vo.VoUsers;
import com.squareup.otto.Subscribe;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by Ryan on 2017. 2. 7..
 */

public class Activity_ChatRoom extends FragmentActivity implements IChatView, MOAEventListener, ChatRoomListener, CCastListener {

    private static final String TAG = Activity_ChatRoom.class.getSimpleName();

    static Activity_ChatRoom instance = null;

    public Activity_ChatRoom() {
    }

    public static Activity_ChatRoom getInstance() {
        return instance;
    }

    public static void clearInstance(Activity_ChatRoom value) {
        if (value == instance) {
            instance = null;
        }
    }

    public static Context aveContext;

    public static Activity_ChatRoom activity_chatRoom;

    public static final String RUN_KEY_MODE = "RUNMODE";
    public static final String RUN_KEY_SCREEN_NAME = "RUNSCREENNAME";
    public static final String RUN_KEY_SCREEN = "RUNSCREEN";
    public static final String RUN_KEY_OPENDATA = "RUNOPENDATA";
    public static final String RUN_MODE_INTRIMPORT = "RUNINTRIMPORT";

    public static final String RUN_KEY_SENDERID = "RUNSENDERID";
    public static final String RUN_KEY_GROUPNAME = "RUNGROUPNAME";
    public static final String RUN_KEY_GROUPITEM = "RUNGROUPITEM";

    public static final String RUN_KEY_APPTYPE = "RUNAPPTYPE";
    public static final String RUN_KEY_ITEMCODE = "RUNITEMCODE";
    public static final String RUN_KEY_ITEMCODETYPE = "RUNITEMCODETYPE";

    public static final int RUN_MODE_NONE = 0;
    public static final int RUN_MODE_OPENSCREEN = 1;
    public static final int RUN_MODE_OPENWEB = 2;
    public static final int RUN_MODE_EX_APP = 3;

    private String mUserID;
    private String itemCode;
    private String itemCodeName;
    private String anMarketType;

    private Fragment_RightNavigation fragment_rightNavigation;
    private Fragment_OpenNavigation fragment_openNavigation;
    private FragmentSlideMenu_StockSearch fragmentSlideMenu_stockSearch;
    private FragmentSlideMenu_Potfolio fragmentSlideMenu_potfolio;
    private FragmentSlideMenu_FinanceTotal fragmentSlideMenu_financeTotal;
    private FragmentSlideMenu_FinanceCurrent fragmentSlideMenu_financeCurrent;
    private FragmentSlideMenu_FinanceChart fragmentSlideMenu_financeChart;
    private FragmentSlideMenu_FinanceInterest fragmentSlideMenu_financeInterest;

    private FragmentSlideMenu_Main fragmentSlideMenu_Main;
    private FragmentSlideMenu_MTS fragmentSlideMenu_Mts;

    private View mFragmentContainerViewRight;
    private FrameLayout layout_slideMenu;
    private FrameLayout layout_slideMTSMenu;
    public ChatRoomAdapter adapter;
    public ChatPresenter chatPresenter;
    public VoChatList roomInfo;
    public VoChatData messagedata;
    Handler handler = new Handler();

    private DrawerLayout layout_drawer;
    private UIChatTopbarHelper topBar;
    private ListView lvChatListView;
    //private ImageView ivChatSendMore, ivChatEmoticon, ivChatSendBtn;
    private ImageView ivChatSendMore, ivChatEmoticon, ivAudio, ivChatVoice;
    private Button ivChatSendBtn;
    private EditText etChatInput;
    private LinearLayout ll_notice, ll_noticebutton, linearLayoutMoreMenu, ll_audioOnOff, ll_audio;
    private TextView tv_notice, tv_close, tv_delete, tv_staffmsg;
    private ImageButton bt_moreshow;
    private Button ivPicture, ivCamera, ivFile; //, ivJongmok, ivSignal;
    private LinearLayout linearLayoutInputCahtting;

    // EmoticonPreview
    private RelativeLayout rlEmoticonPreview;
    private ImageView tvCancelEmoticonView;
    private ImageView ivEmoticon;
    public ArrayList<VoChatData> chatDataList;

    // NonMember Channel
    private ImageView ivChatShake, ivChatSound, ivChatDelete, iv_staff;

    private boolean newCreate = false;
    private String message = "";
    private String path = "";
    private String selectEmoticon = "";
    private boolean checkSignal = false;
    private VoFriendList friend;
    private String point = "";
    private boolean type = false;
    private boolean isNotMemnberChatFrist = false;
    private LinearLayout linearLayoutTop;
    private boolean noChatData = false;
    private boolean listLastCheck = false;
    //페이징 처리를 위한 변수
    private List<List<VoChatData>> pageList;
    private int currentPage = 0;
    private String enableWriteMsg;
    private Toast shakeToast;
    private View view;
    //이모티콘 변수
    private String emoticon_num;

    //대화내용 검색
    private EditText editTextSearchInput;
    boolean visibleLastItem = false;
    ChatDataDbHelper db = null;
    public String currentSearchWord = "";
    int searshedIndex = -1;
    long searchedDate = -1;
    boolean isSearchStart = true;
    private ImageView imageViewSearchUp, imageViewSearchDown, imageViewDeleteKeyword;
    private LinearLayout linearLayoutSearch;

    private boolean checkPortfolio = false;
    private boolean checkRateShare = false;
    public TextView tv_contact;
    private int count;
    private int version;

    private FrameLayout flEmoticonContainer;
    private RelativeLayout ll_reply_background;
    private boolean emoticonViewFlag;

    private String pushIntent;

    public interface PreViewListener {
        void setPreViewHeight(int px);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            version = 1;
        } else {
            version = 0;
        }
//        LogTrace.E("onCreate  showLoadingDialog");
        //LoadingManager.with(this).showLoadingDialog();

        isopenMenuRight();
        aveContext = this;
        instance = this;
        view = getLayoutInflater().from(this).inflate(R.layout.fragment_chatroom, null);
        setContentView(view);
//        LoadingManager.with(Activity_ChatRoom.this).showLoadingDialog();

        BusProvider.getInstance().register(this);
        mUserID = MessengerInfo.getUserId(this);
        chatPresenter = new ChatPresenter(this);
        adapter = new ChatRoomAdapter(this);

        adapter.setProfileListener(profileListener);
        adapter.setPreViewListener(preViewListener);

        initView();
        setEvent();
        CCastClient.getInstance().setCCastListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogTrace.E("onResume");
        Statics.setRoomOnStop(false);

        MOAClient.getInstance().addEventListener(this);
        BusProvider.getInstance().post(new FragmentEventHelper("setOnChatRoomListener", this, null));
//        BusProvider.getInstance().post(new FragmentEventHelper("GetCurrentChatList", null, null));
        Fragment_Main.getInstance().GetCurrentChatList();
        Fragment_Main.getInstance().chatListUpdate();

        Uri uri = getIntent().getData();
        if (uri != null) {
            if (getIntent().getAction().equals(Intent.ACTION_VIEW)) {

                final String mmid = uri.getQueryParameter("l");
                LogTrace.E("mmid: " + mmid);
                SharedObject.setProperty_string(this, Constants.MOIM_ID, mmid);

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (mmid != null) {
                            SharedObject.setProperty_string(Activity_ChatRoom.this, Constants.MOIM_ID, mmid);
                            startActivity(new Intent(Activity_ChatRoom.this, Activity_MoimTimeLineList.class));
                        }
                    }
                });

            }
        }

//        if (getIntent().getStringExtra("pushData") != null) {
//            pushIntent = getIntent().getStringExtra("pushData");
//        }

        AndroidLayoutResize.assistActivity(this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setData(intent);
        //모임 초대하기
        Uri uri = getIntent().getData();
        if (uri != null) {
            if (getIntent().getAction().equals(Intent.ACTION_VIEW)) {

                final String mmid = uri.getQueryParameter("l");
                LogTrace.E("mmid: " + mmid);
                SharedObject.setProperty_string(this, Constants.MOIM_ID, mmid);

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (mmid != null) {
                            SharedObject.setProperty_string(Activity_ChatRoom.this, Constants.MOIM_ID, mmid);
                            startActivity(new Intent(Activity_ChatRoom.this, Activity_MoimTimeLineList.class));
                        }
                    }
                });

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogTrace.E("onStop");
        BusProvider.getInstance().post(new FragmentEventHelper("setOnChatRoomListener", null, null));
//        BusProvider.getInstance().post(new FragmentEventHelper("GetCurrentChatList", null, null));
        Fragment_Main.getInstance().GetCurrentChatList();
//        Fragment_Main.getInstance().setChatTab();
        MOAClient.getInstance().removeEventListener(this);

        Statics.setRoomOnStop(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearInstance(instance);
        LogTrace.E("chatroom_onDestroy");

        BusProvider.getInstance().post(new FragmentEventHelper("setMainTotalCount", null, null));
        BusProvider.getInstance().unregister(this);
        if (isCCastClientStarted())
            stopCCastClient();
        CCastClient.getInstance().setCCastListener(null);
    }

    private void initView() {
        layout_drawer = (DrawerLayout) findViewById(R.id.layout_drawer);
        layout_slideMenu = (FrameLayout) findViewById(R.id.flRightNavContainer);
        ViewGroup.LayoutParams params = layout_slideMenu.getLayoutParams();
        layout_slideMenu.setLayoutParams(params);

        topBar = new UIChatTopbarHelper((LinearLayout) findViewById(R.id.includeChatBar));
        lvChatListView = (ListView) findViewById(R.id.lvChatListView);
        ivChatSendMore = (ImageView) findViewById(R.id.ivChatSendMore);
        ivChatEmoticon = (ImageView) findViewById(R.id.ivChatEmoticon);
//        ivChatShake = (ImageView) findViewById(R.id.ivChatShake);
//        ivChatSound = (ImageView) findViewById(R.id.ivChatSound);
//        ivChatDelete = (ImageView) findViewById(R.id.ivChatDelete);
//        iv_staff = (ImageView) findViewById(R.id.iv_staff);
        ivChatSendBtn = (Button) findViewById(R.id.ivChatSendBtn);
        etChatInput = (EditText) findViewById(R.id.etChatInput);
        linearLayoutTop = (LinearLayout) findViewById(R.id.linearLayoutTop);
//        rlEmoticonPreview = (RelativeLayout) findViewById(R.id.rlEmoticonPreview);
//        tvCancelEmoticonView = (ImageView) findViewById(R.id.tvCancelEmoticonView);
        ivEmoticon = (ImageView) findViewById(R.id.ivEmoticon);
        ll_notice = (LinearLayout) findViewById(R.id.ll_notice);
        tv_notice = (TextView) findViewById(R.id.tv_notice);
        bt_moreshow = (ImageButton) findViewById(R.id.bt_moreshow);
        ll_noticebutton = (LinearLayout) findViewById(R.id.ll_noticebutton);
        tv_delete = (TextView) findViewById(R.id.tv_delete);
        tv_close = (TextView) findViewById(R.id.tv_close);
        tv_staffmsg = (TextView) findViewById(R.id.tv_staffmsg);
        linearLayoutMoreMenu = (LinearLayout) findViewById(R.id.linearLayoutMoreMenu);
        ivPicture = (Button) findViewById(R.id.ivPicture);
        ivCamera = (Button) findViewById(R.id.ivCamera);
        ivFile = (Button) findViewById(R.id.ivFile);
        // ivJongmok = (Button) findViewById(R.id.ivJongmok); //종목삭제
        //ivSignal = (Button) findViewById(R.id.ivSignal); //시그널삭제
        ivAudio = (ImageView) findViewById(R.id.ivAudio);
        ll_audioOnOff = (LinearLayout) findViewById(R.id.ll_audioOnOff);
        ll_audio = (LinearLayout) findViewById(R.id.ll_audio);
        linearLayoutInputCahtting = (LinearLayout) findViewById(R.id.linearLayoutInputCahtting);
        ivChatVoice = (ImageView) findViewById(R.id.ivChatVoice);
        editTextSearchInput = (EditText) findViewById(R.id.editTextSearchInput);
        imageViewSearchUp = (ImageView) findViewById(R.id.imageViewSearchUp);
        imageViewSearchDown = (ImageView) findViewById(R.id.imageViewSearchDown);
        imageViewDeleteKeyword = (ImageView) findViewById(R.id.imageViewDeleteKeyword);
        linearLayoutSearch = (LinearLayout) findViewById(R.id.linearLayoutSearch);
        tv_contact = (TextView) findViewById(R.id.tv_contact);

        mFragmentContainerViewRight = findViewById(R.id.flRightNavContainer);
        etChatInput.setImeOptions(EditorInfo.IME_ACTION_DONE);

        flEmoticonContainer = (FrameLayout) findViewById(R.id.flEmoticonContainer);
        ll_reply_background = (RelativeLayout) findViewById(R.id.ll_reply_background);
        setData(getIntent());
    }

    private void setData(Intent intent) {
        // LogTrace.E("setData : " + Statics.ROOMINFO.getEnableWriteMsg());
        //LogTrace.E("setData  showLoadingDialog");
        // LoadingManager.with(this).showLoadingDialog();
        roomInfo = Statics.ROOMINFO;

        if (roomInfo != null) {
            Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
        }

        chatDataList = new ArrayList<>();

        if (!TextUtils.isEmpty(roomInfo.getRoomId())) {

            LogTrace.E("setData    if (!TextUtils.isEmpty(roomInfo.getRoomId())) { setChatData");
            setChatData();  //테스트 주석처리
            chatPresenter.chatRoomInfo(this, roomInfo.getRoomId());
            chatPresenter.chatReadData(this, roomInfo.getRoomId());
        } else {
            displayChatUpdate(true);
        }

        Bundle bundle = new Bundle();


        LogTrace.E("룸타입 체크 = " + roomInfo.getRoom_type());
        //나와의 대화방
        if ("4".equals(roomInfo.getRoom_type())) {
            topBar.setText(MessengerInfo.getUserName(this));
            topBar.setCountGONE();

            tv_staffmsg.setVisibility(View.GONE);

            //bottomIcon(View.GONE, View.GONE);
            fragment_rightNavigation = new Fragment_RightNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_rightNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_rightNavigation).commit();
        }
        //2: 오픈 대화방 3:공용 오픈 대화방
        else if ("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type())) {
            topBar.setText(roomInfo.getRoom_name());
            topBar.setCountGONE();

            enableWriteMsg = roomInfo.getEnableWriteMsg();

            LogTrace.E("chat info : " + enableWriteMsg + " / " + roomInfo.getStaff());

            if ("1".equals(roomInfo.getStaff())) {
                bottomIcon(View.VISIBLE, View.GONE);

                LogTrace.E("init enable : " + enableWriteMsg);

                if ("1".equals(enableWriteMsg)) {
                    ivChatSound.setSelected(false);
                } else {
                    ivChatSound.setSelected(true);
                }
            } else {
                bottomIcon(View.GONE, View.VISIBLE);

                if ("1".equals(enableWriteMsg)) {
                    onlyStaff(true);
                } else {
                    onlyStaff(false);
                }
            }

            fragment_openNavigation = new Fragment_OpenNavigation();

            if ("1".equals(roomInfo.getStaff())) {
                bundle.putBoolean("isStaff", true);
            } else {
                bundle.putBoolean("isStaff", false);
            }
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_openNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_openNavigation).commit();
        }
        //1:1 대화방
        else if ("0".equals(roomInfo.getRoom_type())) {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
            topBar.setText(getChatRoomTitle());

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
                    topBar.setCount(String.valueOf(roomInfo.getFriends().size()));
                } else {
                    topBar.setCountGONE();
                }
            }

            //bottomIcon(View.GONE, View.GONE);

            //tv_staffmsg.setVisibility(View.GONE);
            fragment_rightNavigation = new Fragment_RightNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_rightNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_rightNavigation).commit();
        }
        //시스템 대화방
        else if ("5".equals(roomInfo.getRoom_type())) {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
            topBar.setText(getChatRoomTitle());
            topBar.setVisibleMenu(false);
            linearLayoutInputCahtting.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
                    topBar.setCount(String.valueOf(roomInfo.getFriends().size() - 1));
                } else {
                    topBar.setCountGONE();
                }
            }
            fragment_openNavigation = new Fragment_OpenNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_openNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_openNavigation).commit();
        }
        //그룹 쪽지방
        else if ("6".equals(roomInfo.getRoom_type())) {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
            topBar.setText(roomInfo.getRoom_name());

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
                    topBar.setCount(String.valueOf(roomInfo.getFriends().size() - 1));
                } else {
                    topBar.setCountGONE();
                }
            }


            fragment_rightNavigation = new Fragment_RightNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_rightNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_rightNavigation).commit();
        }
        //모임 대화방
        else if ("7".equals(roomInfo.getRoom_type())) {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
//            ((LinearLayout) findViewById(R.id.ll_manage_notice)).setVisibility(View.VISIBLE);
            topBar.setText(getChatRoomTitle());

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
                    topBar.setCount(String.valueOf(roomInfo.getFriends().size() - 1));
                } else {
                    topBar.setCountGONE();
                }
            }
            //방송
            if (!roomInfo.getRoomStatus().equals("") && roomInfo.loadFromRoomStatusJson("value").equals("1")) {
                ll_audio.setVisibility(View.VISIBLE);
                ivAudio.setImageResource(R.drawable.ic_speak_on);
                startCCastClient();
            }
            fragment_openNavigation = new Fragment_OpenNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_openNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_openNavigation).commit();
        }
        //종목 대화방
        else if ("8".equals(roomInfo.getRoom_type())) {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
            topBar.setText(getChatRoomTitle());

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
//                    topBar.setCount(roomInfo.getUsercount());
                    setContactCount();
//                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
//                    topBar.setCount(String.valueOf(roomInfo.getFriends().size() -1));
                    setContactCount();
//                } else {
                    topBar.setCountGONE();
                }
            }
            fragment_openNavigation = new Fragment_OpenNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_openNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_openNavigation).commit();
//            fragment_openNavigation.setUserCount("참여자 목록 (" + roomInfo.getUsercount() + ")");
        } else {
            LogTrace.E("Activity_ChatRoom : " + roomInfo.getRoom_type());
            topBar.setText(getChatRoomTitle());

            if (!TextUtils.isEmpty(roomInfo.getUsercount())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            } else {
                if (roomInfo.getFriends().size() > 2) {
                    topBar.setCount(String.valueOf(roomInfo.getFriends().size()));
                } else {
                    topBar.setCountGONE();
                }
            }
            fragment_rightNavigation = new Fragment_RightNavigation();
            bundle.putSerializable("roomInfo", roomInfo);
            fragment_rightNavigation.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_rightNavigation).commit();
        }

        if (intent.hasExtra("noticeId")) {
            openNotice(intent.getStringExtra("noticeId"));
        }

//        layout_drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        if (adapter.getCount() > 50) {
        lvChatListView.setStackFromBottom(true);
//        }
        lvChatListView.setAdapter(adapter);

    }

    private void setEvent() {
        // 페이징 처리를 위한 리스너.
        // 리스트의 탑을 만나면 다음 페이지를 뿌려준다.'

        etChatInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flEmoticonContainer.getVisibility() != View.GONE) {
                    flEmoticonContainer.setVisibility(View.GONE);
                }
                if (linearLayoutMoreMenu.getVisibility() == View.VISIBLE) {
                    linearLayoutMoreMenu.setVisibility(View.GONE);
                }
                lvChatListView.smoothScrollToPosition(adapter.getCount());
                lvChatListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            }
        });

        etChatInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (flEmoticonContainer.getVisibility() != View.GONE) {
                        flEmoticonContainer.setVisibility(View.GONE);
                    }
                    lvChatListView.smoothScrollToPosition(adapter.getCount());
                    lvChatListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                }
            }
        });

//        lvChatListView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && listLastCheck) {
//                    Log.i("TEST", "리스트뷰 바닥");
//                }
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                listLastCheck = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
//                if (firstVisibleItem == 0 && view.getChildAt(0) != null && view.getChildAt(0).getTop() == 0) {
//                    Log.i("TEST", "리스트뷰 상단");
////                    count += 30;
//////                    setChatData();
////                    loadNextDataFromApi();
//                }
//            }
//        });


        lvChatListView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                View mView = lvChatListView.getChildAt(0);

                if (mView == null) {
                    return false;
                }

                int top = mView.getTop();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        // see if it top is at Zero, and first visible position is at 0
                        if (top == 0 && lvChatListView.getFirstVisiblePosition() == 0) {
                            if (pageList != null && pageList.size() > 0 && pageList.size() > currentPage + 1) {
                                currentPage++;
                                final List<VoChatData> pastList = pageList.get(currentPage);
                                chatDataList.addAll(0, pastList);
                                adapter.setChatListData(chatDataList);
                                adapter.notifyDataSetChanged();
                                ErrorController.showMessage("[ChatPresenter] fragChatRoom currentPage : " + currentPage + ", Total List Size : " + pageList.size());
                                new Thread(new Runnable() {
                                    public void run() {
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                lvChatListView.setSelection(pastList.size());
                                            }
                                        }, 500);
                                    }
                                }).start();
                            }
                        }
                        break;

                    case MotionEvent.ACTION_DOWN:
                        if (this != null) {
                            View view = getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }

                            if (flEmoticonContainer.getVisibility() != View.GONE) {
                                flEmoticonContainer.setVisibility(View.GONE);
                            }
                        }
                        break;
                }

                return false;
            }
        });

        //메뉴버튼
        topBar.setButtonMenuClickListener(new View.OnClickListener() {// Menu Btn
            // Click ->
            // navigation
            @Override
            public void onClick(View v) {
                if (!"5".equals(roomInfo.getRoom_type())) {
//                    menuSize(1);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(etChatInput.getWindowToken(), 0);
//                    layout_drawer.openDrawer(Gravity.RIGHT);
                    openMenuRight();
                }
            }
        });

       /* topBar.setButtonMTSClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                menuSize(0);
                if (roomInfo.getRoom_type().equals("8")) {
                    mitemCode = ItemMaster.getRealSearchCode(StringUtil.setJongmok(roomInfo.getRoom_name()), false);
                    Fragment_Main.getInstance().fragmentSlideMenu_main.setDisplay(aveContext, "1000", roomInfo.getRoomId().replace("STC_", ""));
                } else {
                    Fragment_Main.getInstance().fragmentSlideMenu_main.setDisplay(aveContext, "1000", "");
                }
                Fragment_Main.getInstance().fragmentSlideMenu_main.setUp(view, R.id.flRightNavContainer, (DrawerLayout) view.findViewById(R.id.layout_drawer));
                getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, Fragment_Main.getInstance().fragmentSlideMenu_main).commit();
//                layout_drawer.openDrawer(Gravity.RIGHT);
                openMenuRight();
            }
        });*/

        topBar.setButtonSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linearLayoutSearch.getVisibility() == View.VISIBLE) {
                    linearLayoutSearch.setVisibility(View.GONE);
                    if (searshedIndex != -1) {
                        chatDataList.get(searshedIndex).setSearched(false);
                    }
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    flEmoticonContainer.setVisibility(View.GONE);
                    linearLayoutSearch.setVisibility(View.VISIBLE);
                }

            }
        });

        layout_drawer.setDrawerListener(drawerListener);

        linearLayoutTop.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                View view = getCurrentFocus();

                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }

                if (checkEmoticon()) {
                    selectEmoticon = "";
//                    hidePreview(View.GONE);
                    return true;
                }

                if (linearLayoutMoreMenu.getVisibility() == View.VISIBLE) {
                    linearLayoutMoreMenu.setVisibility(View.GONE);
                    return true;
                }

                return false;
            }
        });

        topBar.setButtonBackClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //이모티콘
        ivChatEmoticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("clicked Emoticon");
                View view = getCurrentFocus();

                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

//                if (((FrameLayout) findViewById(R.id.flEmoticonContainer)).getVisibility() == View.GONE) {
                if (!emoticonViewFlag) {
                    emoticonViewFlag = true;
                    flEmoticonContainer.setVisibility(View.VISIBLE);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.flEmoticonContainer, new Emoticon_Activity(3, Activity_ChatRoom.this));
                    fragmentTransaction.commit();
                } else {
                    emoticonViewFlag = false;
                    flEmoticonContainer.setVisibility(View.GONE);
                }
            }
        });

        //더보기 메뉴
        ivChatSendMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linearLayoutMoreMenu.getVisibility() == View.GONE) {
                    linearLayoutMoreMenu.setVisibility(View.VISIBLE);
                    if (flEmoticonContainer.getVisibility() != View.GONE) {
                        flEmoticonContainer.setVisibility(View.GONE);
                    }
                } else {
                    linearLayoutMoreMenu.setVisibility(View.GONE);
                }
            }
        });

        etChatInput.addTextChangedListener(new TextWatcherAbstract() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    ivChatSendBtn.setSelected(false);
                } else {
                    ivChatSendBtn.setSelected(true);
                }
            }
        });

        ivChatSendBtn.setOnClickListener(new View.OnClickListener() {// send btn
            @Override
            public void onClick(View v) {
                if (emoticon_num != null) {
                    setChatMessage("(이모티콘)", "1");
                    selectEmoticon = "";
//                    return;
                }

                if (etChatInput.getText().toString().length() > 0) {
                    LogTrace.E("발송 룸타입 : " + roomInfo.getRoom_type());
                    setChatMessage(etChatInput.getText().toString(), "0");
                    etChatInput.setText("");
                }
            }
        });

//        tvCancelEmoticonView.setOnClickListener(new View.rlEmoticonPreview

        bt_moreshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bt_moreshow.isSelected()) {
                    tv_notice.setMaxLines(2);
                    ll_noticebutton.setVisibility(View.GONE);
                    bt_moreshow.setSelected(false);
                } else {
                    tv_notice.setMaxLines(4);
                    ll_noticebutton.setVisibility(View.VISIBLE);
                    bt_moreshow.setSelected(true);
                }
            }
        });

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_notice.setVisibility(View.GONE);
                tv_notice.setMaxLines(2);
                ll_noticebutton.setVisibility(View.GONE);
                bt_moreshow.setSelected(false);
            }
        });

        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_noticebutton.setVisibility(View.GONE);
                bt_moreshow.setSelected(false);
                tv_notice.setMaxLines(2);
            }
        });

        ivPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    Intent intent = new Intent(Activity_ChatRoom.this, Activity_Gallery.class);
                    intent.putExtra("maxCount", 1);
                    startActivityForResult(intent, Constants.GALLERY_REQUEST);
                    linearLayoutMoreMenu.setVisibility(View.GONE);
                } else {
                    Dialog_Common.showCameraPermissionDialog(Activity_ChatRoom.this);
                }
            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = FileUtil.createImageFile();
                    if (f != null) {
                        LogTrace.E("파일 생성 완료");
                    }
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                    linearLayoutMoreMenu.setVisibility(View.GONE);
                } else {
                    Dialog_Common.showCameraPermissionDialog(Activity_ChatRoom.this);
                }
            }
        });

        ivFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

/*
        ivJongmok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jongmokSearch();
                linearLayoutMoreMenu.setVisibility(View.GONE);
            }
        });

        ivSignal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMoreMenu.setVisibility(View.GONE);
                signalShare();
            }
        });*/

        ll_audioOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCCastClientStarted()) {
                    ivAudio.setImageResource(R.drawable.ic_speak_on);
                    startCCastClient();
                } else {
                    ivAudio.setImageResource(R.drawable.ic_speak_off);
                    stopCCastClient();
                }
            }
        });

        editTextSearchInput
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        // TODO Auto-generated method stub
                        try {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                if (editTextSearchInput.getText().toString().length() < 2) {
                                    Toast.makeText(Activity_ChatRoom.this, "2글자 이상 입력해주세요.", Toast.LENGTH_SHORT).show();
                                } else {
                                    PageSearch();
                                    search(v, "asc", true);
                                }
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        return false;
                    }
                });

        editTextSearchInput.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
//                    setListScroll();
//                    if (isEmoticonOpened) {
//                        // if(isBottomOpened || isEmoticonOpened) {
//                        // isBottomOpened = false;
//                        // linearLayoutBottom.setVisibility(View.GONE);
//                        isEmoticonOpened = false;
//                        linearLayoutEmoticon.setVisibility(View.GONE);
//                    }
                } catch (Exception e) {
                    // TODO: handle exception

                }
            }
        });

        editTextSearchInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.toString().equals("")) {
                    imageViewDeleteKeyword.setVisibility(View.GONE);
//                    lvChatListView.postDelayed(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            // TODO Auto-generated method stub
//                            lvChatListView.setSelection(chatDataList.size() - 1);
//                        }
//                    }, 10);
                } else {
                    imageViewDeleteKeyword.setVisibility(View.VISIBLE);
                }
            }
        });

        imageViewDeleteKeyword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (searshedIndex != -1) {
                    chatDataList.get(searshedIndex).setSearched(false);
                }
                adapter.notifyDataSetChanged();
                editTextSearchInput.setText("");
//                try {
//                    if(searshedIndex != -1) {
//                        chatDataList.get(searshedIndex).setSearched(false);
//                    }
//                    editTextSearchInput.setText("");
//                } catch (Exception e) {
//                    // TODO: handle exception
//
//                }
            }
        });


        imageViewSearchUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!editTextSearchInput.getText().toString().equals("")) {
                    try {
                        PageSearch();
                        search(v, "desc", true);
                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                } else {
                    Toast.makeText(Activity_ChatRoom.this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                    editTextSearchInput.requestFocus();
                }
            }
        });

        imageViewSearchDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!editTextSearchInput.getText().toString().equals("")) {

                    try {
                        search(v, "desc", false);
                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                } else {
                    Toast.makeText(Activity_ChatRoom.this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                    editTextSearchInput.requestFocus();
                }
            }
        });


        ivChatVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFindVoice();
            }
        });

        findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_reply_background.setVisibility(View.GONE);
                emoticon_num = null;
            }
        });

//        findViewById(R.id.ll_notice_close).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((LinearLayout) findViewById(R.id.ll_manage_notice)).setVisibility(View.GONE);
//            }
//        });

    }// end of setEvent

    public void setEmoticon(String emoticon) {
        this.emoticon_num = String.valueOf(emoticon);

        ll_reply_background.setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_reply_image)).setImageResource(getDrawableResourceByName(emoticon));
    }

    private void emoticon_gone() {
//        findViewById(R.id.btn_emoticon).setSelected(false);
        ll_reply_background.setVisibility(View.GONE);
        flEmoticonContainer.setVisibility(View.GONE);
    }

    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(this, str);
        String packageName = getPackageName();
        return getResources().getIdentifier(emoticon, "drawable", packageName);
    }

    @Subscribe
    public void BusEvent(DrawerLayoutEventHelper event) {
        if ("openMTS".equals(event.getEvent())) {
            openMTS(event.getItem());
        } else if ("openStockSearch".equals(event.getEvent())) {
            openStockSearch();
        } else if ("openPotfolio".equals(event.getEvent())) {
//            openPotfolio();
        } else if ("isDrawerOpenRight".equals(event.getEvent())) {
//            isDrawerOpenRight();
            openMenuRight();
        } else if ("isDrawerCloseRight".equals(event.getEvent())) {
//            isDrawerCloseRight();
            openMenuRight();
        } else if ("openMenuRight".equals(event.getEvent())) {
            openMenuRight();
        }
    }

    public Handler mChatRoomHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            MOAData data = (MOAData) msg.obj;
            String strResult;

            switch (msg.what) {
                case 0:
                    //stockDialog();
                    break;
                case 1:
                    LogTrace.E("handleMessage : null");
                    break;

                case PacketTypes.PTC_IMS_CHATROOM_INFO:
                    strResult = (String) data.body.get("result");
                    if (strResult.equals(Constants.SUCCESS)) {
                        try {
                            getRoomInfo(data);
                            setMainTotalCount();
//                            adapter.notifyDataSetChanged();
//                            updateRoomUI(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }

            return false;
        }
    });


    public void setMainTotalCount() {

//        ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
//        int totalCount = db.chatRoomNoReadTotalCount();
        // 채팅 미확인 카운트 조회
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", mUserID);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_TOTAL_UNREAD, body);

            LogTrace.E("afterLogin PTC_IMS_CHATROOM_TOTAL_UNREAD" + getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        setChatCount(totalCount);
//        setBadge(totalCount + moimCount);
    }

    //시그널 공유
    final String[] signalitemPv = new String[1];
    final String[] signalitemCv = new String[1];
    String signalitemCode;
    String signalitemSg;
    String signalitemCodeName;
    final String[] signalitemTp = new String[1];

    String portfolioAttachment;

    //포트폴리오 전체공유
    public void stockPortfolioAll(String sRoomId, ArrayList<VoPortfolioList> portfolioLists) {
        LogTrace.E(portfolioLists.get(0).getM_ItemName() + "");

        if (sRoomId != "" && sRoomId.equals(roomInfo.getRoomId())) {
            // -룸아이디;종목코드1`종목명1`편입가1`매입비중1`손익률1,종목코드2`종목명2`편입가2`매입비중2`손익률2

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < portfolioLists.size(); i++) {
                JSONObject object = new JSONObject();
                try {
                    object.put("stcd", portfolioLists.get(i).getM_ItemCode());
                    object.put("stnm", portfolioLists.get(i).getM_ItemName());
                    object.put("stav", portfolioLists.get(i).getM_Purchase());
                    object.put("bywt", portfolioLists.get(i).getM_PurchasePs());
                    object.put("strt", portfolioLists.get(i).getM_ProfitPs());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                jsonArray.put(object);
            }

            JSONObject object = new JSONObject();
            try {
                object.put("cuser_type", MessengerInfo.getUserType());
                object.put("crisk_type", MessengerInfo.getRiskType());
                object.put("data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String portfolioAttachment = object.toString();
            LogTrace.E("포트폴리오 전체공유 : " + portfolioAttachment);
            this.portfolioAttachment = portfolioAttachment;
            checkPortfolio = true;

            long now = System.currentTimeMillis();
            Date date = new Date(now);
            SimpleDateFormat sdf = new SimpleDateFormat("MM.dd");
            String getDate = sdf.format(date);
            String message = getDate + " 포트폴리오 전 종목을 공유합니다";
            setChatMessage(message, "15");

        } else {
            //
        }
    }

    //포트폴리오 신규,변경,제외
    public void stockPortfolio(String psSharePortfolio) {
        LogTrace.E(psSharePortfolio);


        String sArr[] = psSharePortfolio.split("`");
        String sRoomId, sItemType, sItemCode, sItemName, sPurchasePs, sPurchase, sPurchasePsChg = null, sProfitPs;


        sRoomId = sArr[0];   //룸아이디
        sItemType = sArr[1];   //신규, 변경, 제외

        if (sItemType.equals("new") || sItemType.equals("del")) {
            sItemCode = sArr[2];   //종목코드
            sItemName = sArr[3];   //종목명
            sPurchase = sArr[4];   //편입가
            sPurchasePs = sArr[5];   //기존 매입비중
            sProfitPs = sArr[6];   //손익률
        } else {
            sItemCode = sArr[2];   //종목코드
            sItemName = sArr[3];   //종목명
            sPurchasePs = sArr[4];   //기존 매입비중
            sPurchase = sArr[5];   //편입가
            sPurchasePsChg = sArr[6];   //변경 매입비중
            sProfitPs = sArr[7];   //손익률
        }

        checkPortfolio = true;
//        String itemName;
//        String itemType = sItemType;

//        IStructItemCode m_ItemInfo = ItemMaster.getCodeItem(sItemCode);
//        itemName = m_ItemInfo.getDivName();
        switch (sItemType) {
            case "new":
                sItemType = "" + sItemName + " 비중 " + sPurchasePs + "% 신규 편입하였습니다";
                break;
            case "change":
                sItemType = "" + sItemName + " 비중 " + sPurchasePs + "% 에서 " + sPurchasePsChg + "% 로 변경 되었습니다";
                break;
            case "del":
                sItemType = "" + sItemName + " 비중 " + sPurchasePs + "% 제외 하였습니다";
//                sItemType = ""+sItemName+"를 제외 하였습니다";
                break;
        }

        String message = sItemType;
        setChatMessage(message, "14");
    }

    /*public void stockDialog() {
        VoStockData vostockData = MessengerInterface.getInstance().voStockData;

        String itemCode = vostockData.getM_ItemCode();
        String itemName = vostockData.getM_ItemName();
        String currentValue = vostockData.getM_CurrentValue();
        String compareValue = vostockData.getM_CompareValue();
        String compareMark = vostockData.getM_CompareMark();
        String compareRate = vostockData.getM_CompareRate();
        String amount = vostockData.getM_Amount();

        final Dialog_Stock dialogStock = new Dialog_Stock(Activity_ChatRoom.this);
        dialogStock.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogStock.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialogStock.show();
        dialogStock.setData(itemCode, itemName, currentValue, compareValue, compareRate, compareMark, amount);
    }*/

    public boolean isDrawerOpenRight() {
        return layout_drawer != null && layout_drawer.isDrawerOpen(mFragmentContainerViewRight);
    }

    //우측 메뉴버튼 열기
    public void openMenuRight() {
        if (isDrawerOpenRight()) {
            layout_drawer.closeDrawer(mFragmentContainerViewRight);
        } else {
            layout_drawer.openDrawer(mFragmentContainerViewRight);
        }
    }

    public void isopenMenuRight() {
        if (isDrawerOpenRight()) {
            layout_drawer.closeDrawer(mFragmentContainerViewRight);
        }
    }

    public void openMTS(String item) {
        switch (item) {
            case "0":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeTotal = new FragmentSlideMenu_FinanceTotal();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeTotal).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeTotal);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
            case "1":
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
//                        menuSize(0);
                        Fragment_Main.getInstance().fragmentSlideMenu_main.setUp(view, R.id.flRightNavContainer, (DrawerLayout) view.findViewById(R.id.layout_drawer));
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, Fragment_Main.getInstance().fragmentSlideMenu_main).commit();
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                });
                break;
            case "2":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeChart = new FragmentSlideMenu_FinanceChart();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeChart).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeChart);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
            case "3":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentSlideMenu_financeInterest = new FragmentSlideMenu_FinanceInterest();
                        getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_financeInterest).commit();
//                        ViewManager.getViewManager().replaceFragment(getInstance(), R.id.flRightNavContainer, fragmentSlideMenu_financeInterest);
//                        layout_drawer.openDrawer(Gravity.RIGHT);
                        openMenuRight();
                    }
                }, 500);
                break;
        }
    }

    public void openStockSearch() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fragmentSlideMenu_stockSearch = new FragmentSlideMenu_StockSearch();
                getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragmentSlideMenu_stockSearch).commit();
//                layout_drawer.openDrawer(Gravity.RIGHT);
                openMenuRight();
            }
        }, 500);
    }

    public void openPotfolio(final String psUserInfo) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment_Main.getInstance().fragmentSlideMenu_main.setDisplay(aveContext, "6910", psUserInfo);
                openMTS("1");
            }
        }, 500);
    }

    public void isDrawerCloseRight() {
        layout_drawer.closeDrawer(Gravity.RIGHT);
    }

    DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerClosed(View drawerView) {
//            setData(getIntent());
            if ("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type()) || "7".equals(roomInfo.getRoom_type()) || "8".equals(roomInfo.getRoom_type())) {
                fragment_openNavigation = new Fragment_OpenNavigation();
                bundle.putSerializable("roomInfo", roomInfo);
                fragment_openNavigation.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_openNavigation).commit();
            } else {
                fragment_rightNavigation = new Fragment_RightNavigation();
                bundle.putSerializable("roomInfo", roomInfo);
                fragment_rightNavigation.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.flRightNavContainer, fragment_rightNavigation).commit();
            }
        }

        @Override
        public void onDrawerOpened(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private void bottomIcon(int visibleStaff, int visibleNormal) {
//        ivChatShake.setVisibility(visibleStaff);
//        ivChatSound.setVisibility(visibleStaff);
//        ivChatDelete.setVisibility(visibleStaff);
//        iv_staff.setVisibility(visibleNormal);
    }

    private void onlyStaff(final boolean isOnly) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isOnly) {
                    tv_staffmsg.setText(getString(R.string.open_enable));
                    tv_staffmsg.setVisibility(View.VISIBLE);
                } else {
                    if (iv_staff.isSelected()) {
                        tv_staffmsg.setText(getString(R.string.open_staff));
                    } else {
                        tv_staffmsg.setVisibility(View.GONE);
                        tv_staffmsg.setText("");
                    }
                }

                ivChatSound.setSelected(!isOnly);

                if ("0".equals(roomInfo.getStaff())) {
                    LogTrace.E("not staff : " + isOnly);
                    ivChatSendMore.setEnabled(isOnly);
                    ivChatEmoticon.setEnabled(isOnly);
                    etChatInput.setEnabled(isOnly);
                    ivChatSendBtn.setEnabled(isOnly);
                }
            }
        });
    }

    private void openNotice(String chatId) {
        String notice = "";

        for (int i = 0; i < chatDataList.size(); i++) {
            if (chatDataList.get(i).getChatId().equals(chatId)) {
                notice = chatDataList.get(i).getMessage();
            }
        }

        if (!"".equals(notice)) {
            tv_notice.setText(notice);
            ll_notice.setVisibility(View.VISIBLE);
        }
    }

    public void setScrolldown(final int pix) {
        lvChatListView.postDelayed(new Runnable() {
            @Override
            public void run() {
                lvChatListView.smoothScrollBy(pix, 100);
            }
        }, 0);

    }

    public void setChatData() {
        LogTrace.E("setChatData DataAsyncTask");

//        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(this);
       /* DataAsyncTask asyncTask = new DataAsyncTask();
        asyncTask.execute();*/

        ///TODO asyncTask 에서 이전으로 수정
        db = LocalDB.getChatDataDbHelper(Activity_ChatRoom.this);
        List<VoChatData> list = db.getChatData(roomInfo.getRoomId());

        if (list.size() == 0) {
            noChatData = true;
        }

        if (chatDataList.size() != 0) {
            chatDataList.clear();
        }

        for (VoChatData item : list) {
            item.setRoomType(roomInfo.getRoom_type());
        }

        //New Logic for paging
        pageList = new ArrayList<>();
        chatPresenter.setPageList(pageList, list);

        //가장 최근의 채팅부터 가져온다.
        if (pageList.size() != 0) {
            List<VoChatData> latestFiftyChats = pageList.get(0);

            for (VoChatData item : latestFiftyChats) {
                addChat(item);
            }

            currentPage = 0;
        }
        adapter.setChatListData(chatDataList);

        displayChatUpdate(true);
        lvChatListView.setSelection(adapter.getCount());


        //TODO  20180323뭔지..
      /*  db = LocalDB.getChatDataDbHelper(Activity_ChatRoom.this);
        List<VoChatData> list = db.getChatData(roomInfo.getRoomId());

        if (list.size() == 0) {
            noChatData = true;
        }

        if (chatDataList.size() != 0) {
            chatDataList.clear();
        }

        for (VoChatData item : list) {
            item.setRoomType(roomInfo.getRoom_type());
        }

        //New Logic for paging
        pageList = new ArrayList<>();
        chatPresenter.setPageList(pageList, list);

        //가장 최근의 채팅부터 가져온다.
        if (pageList.size() != 0) {
            List<VoChatData> latestFiftyChats = pageList.get(0);

            for (VoChatData item : latestFiftyChats) {
                addChat(item);
            }

            currentPage = 0;
        }
        adapter.setChatListData(chatDataList);

           displayChatUpdate(true);
          lvChatListView.setSelection(adapter.getCount());*/


    }

    public class DataAsyncTask extends android.os.AsyncTask<String, Integer, String> {

        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//                        lvChatListView.setVisibility(View.INVISIBLE);
            LogTrace.E("DataAsyncTask onPreExecute");
            LoadingManager.with(Activity_ChatRoom.this).showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... params) {


            db = LocalDB.getChatDataDbHelper(Activity_ChatRoom.this);
            List<VoChatData> list = db.getChatData(roomInfo.getRoomId());

            if (list.size() == 0) {
                noChatData = true;
            }

            if (chatDataList.size() != 0) {
                chatDataList.clear();
            }

            for (VoChatData item : list) {
                item.setRoomType(roomInfo.getRoom_type());
            }

            //New Logic for paging
            pageList = new ArrayList<>();
            chatPresenter.setPageList(pageList, list);

            //가장 최근의 채팅부터 가져온다.
            if (pageList.size() != 0) {
                List<VoChatData> latestFiftyChats = pageList.get(0);

                for (VoChatData item : latestFiftyChats) {
                    addChat(item);
                }

                currentPage = 0;
            }
            adapter.setChatListData(chatDataList);

//            displayChatUpdate(true);
//            lvChatListView.setSelection(adapter.getCount());

            result = String.valueOf(adapter.getCount());
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            if(!s.equals("0")) {
            adapter.notifyDataSetChanged();
            lvChatListView.setSelection(adapter.getCount());
            // lvChatListView.setVisibility(View.VISIBLE);

            LogTrace.E("DataAsyncTask onPostExecute");
            LoadingManager.with(Activity_ChatRoom.this).hideLoadingDialog();
//            }
        }
    }

    public void setStaffChatData() {
        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(this);
        List<VoChatData> list = db.getStaffChatData(roomInfo.getRoomId());

        if (list.size() == 0) {
            noChatData = true;
        }

        if (chatDataList.size() != 0) {
            chatDataList.clear();
        }

        //New Logic for paging
        pageList = new ArrayList<>();
        chatPresenter.setPageList(pageList, list);

        //가장 최근의 채팅부터 가져온다.
        if (pageList.size() != 0) {
            List<VoChatData> latestFiftyChats = pageList.get(0);

            for (VoChatData item : latestFiftyChats) {
                addChat(item);
            }

            currentPage = 0;
        } else {
            addChat(new VoChatData());
        }

        displayChatUpdate(true);
        lvChatListView.setSelection(adapter.getCount());
    }

    public void addChat(VoChatData newData) {
        boolean exist = false;

        for (VoChatData item : chatDataList) {
            if (item.getChatId().equals(newData.getChatId())) {
                exist = true;
                break;
            }
        }

        if (!exist) {
            //if(!"1".equals(newData.getStaffMsg())) {
            LogTrace.E("addChat Room_type : " + roomInfo.getRoom_type());
            newData.setRoomType(roomInfo.getRoom_type());
            chatDataList.add(newData);
            //}
        }
    }

    private void setChatMessage(String msg, String msgType) {
        UUID uuid = UUID.randomUUID();
        message = msg;
        messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(Activity_ChatRoom.this));
        messagedata.setRoomType(roomInfo.getRoom_type());
        if (roomInfo.getRoom_name() != "")
            messagedata.setRoomName(roomInfo.getRoom_name());
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setMessageType(msgType);
        messagedata.setOwnerId(MessengerInfo.getUserId(Activity_ChatRoom.this));

        if ("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type())) {
            messagedata.setUserName(roomInfo.getRoomSubject());
            messagedata.setSenderName(roomInfo.getRoomSubject());
        } else if ("6".equals(roomInfo.getRoom_type())) {
            messagedata.setRoomName(roomInfo.getRoom_name());
            messagedata.setgId(roomInfo.getGid());
        } else {
            messagedata.setRoomName(roomInfo.getRoom_name());
            messagedata.setUserName(MessengerInfo.getUserName(Activity_ChatRoom.this));
            messagedata.setSenderName(MessengerInfo.getUserName(Activity_ChatRoom.this));
        }

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        if (emoticon_num == null) {
            messagedata.setMessage(message);
        }
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA).format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);

        //유저타입, 투자권유정보
        JSONObject object = new JSONObject();

        try {
            object.put("cuser_type", MessengerInfo.getUserType());
            object.put("crisk_type", MessengerInfo.getRiskType());
            if (emoticon_num != null) {   //이모티콘
                object.put("code", emoticon_num);

                emoticon_num = null;
            }
            if (checkSignal) {   //시그널공유
                object.put("stnm", signalitemCodeName);
                object.put("stcd", signalitemCode);
                object.put("stsg", signalitemSg);
                object.put("stpv", signalitemPv[0]);
                object.put("stcv", signalitemCv[0]);
                object.put("sttp", signalitemTp[0]);

                checkSignal = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (checkPortfolio) {
            messagedata.setAttachment(portfolioAttachment);
            checkPortfolio = false;
            portfolioAttachment = "";
        } else if (checkRateShare) {
            messagedata.setAttachment(rateShareAttachment);
            checkRateShare = false;
            rateShareAttachment = "";
        } else {
            messagedata.setAttachment(object.toString());
        }


        if (TextUtils.isEmpty(roomInfo.getRoomId())) {
            if ("1".equals(roomInfo.getRoom_type())) {
                isNotMemnberChatFrist = true;
                inviteFriend(roomInfo.getRoomId(), roomInfo.getFriends());
                ErrorController.showMessage("newFriends", roomInfo.getFriends().toString());
            } else {
                messagedata.setRoomId("");
                for (VoFriendList item : roomInfo.getFriends()) {
                    if (!item.getUserId().equals(MessengerInfo.getUserId(this))) {
                        if (!"6".equals(roomInfo.getRoom_type()))
                            messagedata.setInviteUser(item.getUserId());
                        messagedata.setSenderId(item.getUserId());
                    }
                }
                messagedata.setRoomCreate("1");
                chatPresenter.sendMessage(this, messagedata);
            }
        } else {
            for (VoFriendList item : roomInfo.getFriends()) {
                if (!item.getUserId().equals(MessengerInfo.getUserId(this))) {
                    messagedata.setSenderId(item.getUserId());
                }
            }

            messagedata.setRoomCreate("0");
            messagedata.setRoomId(roomInfo.getRoomId());
            chatPresenter.sendMessage(this, messagedata);

            if ("1".equals(messagedata.getRoomType()) && isNotMemnberChatFrist) {
                isNotMemnberChatFrist = false;
            }
        }

        setChatDataSend(messagedata);
//        rlEmoticonPreview.setVisibility(View.GONE);
        ll_reply_background.setVisibility(View.GONE);
        ivChatSendBtn.setSelected(false);
    }

    String rateShareAttachment;

    public void rateShareMessageSend(String rateShareAttachment) {
        this.rateShareAttachment = rateShareAttachment;
        checkRateShare = true;

        String message = "수익률 공유";
        setChatMessage(message, "13");
    }

    public void setChatDataSend(VoChatData item) {
        addChat(item);

        ArrayList<VoChatData> list = new ArrayList<>();
        item.setRetryCnt(0);
        item.setStatus("1");
        list.add(item);
        boolean readcount;
        if (roomInfo.getRoom_type().equals("7") || roomInfo.getRoom_type().equals("8")) {
            readcount = false;
        } else {
            readcount = true;
        }
        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(this);
        db.checkChatData(list, readcount);

//        setChatData();

        displayChatUpdate(true);
        lvChatListView.setSelection(adapter.getCount());
    }

    private boolean checkEmoticon() {
        if (selectEmoticon == null || "".equals(selectEmoticon))
            return false;
        else
            return true;
    }

//    private void hidePreview(int visibility) {
//        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
//            getSupportFragmentManager().popBackStack();
//        }
//        rlEmoticonPreview.setVisibility(visibility);
//    }

    public void inviteFriend(final String roomId, final List<VoFriendList> list) {
        chatPresenter.inviteFriend(this, roomId, list);
    }

    public void inviteFriend(final String roomId, final List<VoFriendList> list, VoChatData voChatData) {
        if (voChatData != null) {
            messagedata = voChatData;
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                chatPresenter.inviteFriend(Activity_ChatRoom.this, roomId, list);
            }
        });
    }

    public String getChatRoomTitle() {
        if (TextUtils.isEmpty(roomInfo.getRoom_name())) {
            if (roomInfo.getFriends().size() == 1) {
                return roomInfo.getFriends().get(0).getUserName();
            } else if (roomInfo.getFriends().size() == 2) {
                return roomInfo.getFriends().get(1).getUserName();
            } else if (roomInfo.getFriends().size() == 3) {
                return roomInfo.getFriends().get(1).getUserName() + ", " + roomInfo.getFriends().get(2).getUserName();
            } else if (roomInfo.getFriends().size() > 3) {
                return roomInfo.getFriends().get(1).getUserName() + ", " + roomInfo.getFriends().get(2).getUserName() + "...";
            } else {
                return "";
            }
        } else {
            return roomInfo.getRoom_name();
        }
    }

    public void removeChat(String chatId) {
        for (int i = 0; i < chatDataList.size(); i++) {
            if (chatDataList.get(i).getChatId().equals(chatId)) {
                chatDataList.remove(i);
                break;
            }
        }

        displayChatUpdate(false);
    }

    public void displayChatUpdate(boolean isScroll) {
        adapter.setChatListData(chatDataList);
        adapter.notifyDataSetChanged();

        if (isScroll) {
//            lvChatListView.postDelayed(new Runnable() {
//                @Override
//                public void run() {
            lvChatListView.smoothScrollToPosition(adapter.getCount());
//                }
//            }, 500);
        }

    }

    public String getPathFromUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToNext();
        String path = cursor.getString(cursor.getColumnIndex("_data"));
        cursor.close();
        return path;
    }

    private void getRoomInfo(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("users");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }

        roomInfo = info;
    }

    private void updateRoomUI(MOAData data) {
        LogTrace.E("updateRoomUI" + roomInfo.getRoom_type());

        if (newCreate) {
            getRoomInfo(data);
            adapter.notifyDataSetChanged();
            VoChatList currentChatData = roomInfo;
            currentChatData.setRoomId(roomInfo.getRoomId());

            if (!"2".equals(roomInfo.getRoom_type()) && !"3".equals(roomInfo.getRoom_type())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            }

//            fragment_rightNavigation.setListView();
            newCreate = false;
        } else {
            getRoomInfo(data);
            adapter.notifyDataSetChanged();

            try {
                if (!"2".equals(roomInfo.getRoom_type()) && !"3".equals(roomInfo.getRoom_type())) {
                    if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                        topBar.setCount(roomInfo.getUsercount());
                        Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                    } else {
                        topBar.setCountGONE();
                    }
                }

                recRead();

//                if (!TextUtils.isEmpty(pushIntent)) {
//                    Log.d(TAG, "RoomInfo : " + roomInfo.toString());
//                    Statics.ROOMINFO.setLast_read_cid(roomInfo.getLast_read_cid());
//                    Statics.ROOMINFO.setUnread(roomInfo.getUnread());
//                    Fragment_Main.getInstance().GetCurrentChatList();
//                    pushIntent = null;
//                }

//                fragment_rightNavigation.setListView();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if ("5".equals(roomInfo.getRoom_type())) {
                chatPresenter.getMessage(this, roomInfo.getRoomId(), roomInfo.getLast_read_cid(), roomInfo.getUnread());
            } else {

                LogTrace.E("##noChatData roomInfo.getUnread(): "+roomInfo.getUnread());

                //chatPresenter.getMessage(this, roomInfo.getRoomId(), "", roomInfo.getUnread()); //0508 leeyunsu

                if(roomInfo.getUnread().equals("0") ){ //미확인 메시지 없을경우.  0508 leeyunsu
                    if(noChatData){     //채팅리스트 없을경우, 삭제후 설치해서 디비에 없을경우.
                        LogTrace.E("##noChatData");
                        chatPresenter.getMessage(this, roomInfo.getRoomId(), "", roomInfo.getUnread());
                    }
                }else{ //미확인 메시지 있을경우
                    if(noChatData){  //미확인 카운트 있는데, 채팅리스트없으면 ""로 호출. (전체호출)
                        chatPresenter.getMessage(this, roomInfo.getRoomId(), "", roomInfo.getUnread());
                    }else{                      //기존 대화있으면 getLast_read_cid로  호출. (lastcid 이후 신규 데이터만 보냄)
                        chatPresenter.getMessage(this, roomInfo.getRoomId(), roomInfo.getLast_read_cid(), roomInfo.getUnread());
                    }
                }

                /*if (!noChatData) {
                    LogTrace.E("if (!noChatData) {");
                       //chatPresenter.getMessage(this, roomInfo.getRoomId(), roomInfo.getLast_read_cid());
                } else {
                    LogTrace.E("ichatPresenter.getMessage(this, roomInfo.getRoomId(), \"\", roomInfo.getUnread());");
                    chatPresenter.getMessage(this, roomInfo.getRoomId(), "", roomInfo.getUnread());
                }*/
            }
        }
    }

    private void setChatDataMsg(final VoChatData item) {
        if (!TextUtils.isEmpty(roomInfo.getRoomId())) {
            addChat(item);
            chatPresenter.setRead(this, roomInfo.getRoomId(), item.getCid(), item.getChatId());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    displayChatUpdate(listLastCheck);
                    LogTrace.E("리스트 마지막 : " + listLastCheck);
                }
            });
        }
    }

    private void exitChatRoom(boolean result) {
        if (result) {
            ChatDataDbHelper dataDb = LocalDB.getChatDataDbHelper(this);
            ChatListDbHelper listDb = LocalDB.getChatListDbHelper(this);
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(this);
            UsersReadDbHelper readDb = LocalDB.getUsersReadDbHelper(this);

            if ("3".equals(roomInfo.getRoom_type())) {
                listDb.updateEnter(roomInfo.getRoomId(), "0");
                listDb.updateNoti(roomInfo.getRoomId(), "1");
                listDb.updateStaff(roomInfo.getRoomId(), "0");
                listDb.updateRoomSubject(roomInfo.getRoomId(), "");
            } else {
                listDb.exitChatRoom(roomInfo.getRoomId());
            }

            dataDb.exitChatData(roomInfo.getRoomId());
            userDb.deleteUsers(roomInfo.getRoomId());
            readDb.deleteUsers(roomInfo.getRoomId());
            Statics.ROOMINFO = null;
            finish();
        }
    }

    private void kickRoom() {
//        nfragment_main.mFragmentChatRmList.kickUpdate(roomInfo.getRoom_name());
        exitChatRoom(true);
    }

    private void updateStaff(final VoUsers user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (user.getUserId().equals(MessengerInfo.getUserId(Activity_ChatRoom.this))) {
                    fragment_openNavigation.updateView(user.getStaff());

                    if ("1".equals(user.getStaff())) {
                        bottomIcon(View.VISIBLE, View.GONE);

                    } else {
                        bottomIcon(View.GONE, View.VISIBLE);

                        if ("1".equals(enableWriteMsg)) {
                            onlyStaff(true);

                        } else {
                            onlyStaff(false);
                            hideKeyboard();

                        }
                    }

                } else {
                    fragment_openNavigation.setListView(version);
                }
            }
        });
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

//        lvChatListView.requestFocus();
    }

    private void getToastView(String count, String max) {
        final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 110, getResources().getDisplayMetrics());

        final View v = View.inflate(this, R.layout.toast_shake, null);

        TextView tv_count = (TextView) v.findViewById(R.id.tv_count);
        TextView tv_maxcount = (TextView) v.findViewById(R.id.tv_maxcount);

        tv_count.setText(getString(R.string.shake_count, count));
        tv_maxcount.setText(getString(R.string.shake_maxcount, max));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (shakeToast == null) {
                    shakeToast = new Toast(Activity_ChatRoom.this);
                    LogTrace.E("toast after make");
                }

                shakeToast.setGravity(Gravity.BOTTOM, 0, margin);
                shakeToast.setDuration(Toast.LENGTH_SHORT);
                shakeToast.setView(v);
                shakeToast.show();
            }
        });
    }

    private void deleteChat(String roomId, String chatId) {
        LocalDB.getChatDataDbHelper(this).delChatData(roomId, chatId);

        if (roomId.equals(roomInfo.getRoomId())) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LogTrace.E("deleteChat setChatData");
                    setChatData();
                }
            });
        }
    }

    Bundle bundle = new Bundle();

    View.OnClickListener profileListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!"8".equals(roomInfo.getRoom_type())) {
                String ownerId = (String) view.getTag();
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(Activity_ChatRoom.this);
                VoFriendList item2 = db.getBuddy(ownerId);

                bundle = new Bundle();

                bundle.putBoolean("isEdit", false);

                if ("0".equals(roomInfo.getRoom_type()))
                    bundle.putBoolean("isShow", true);
                else
                    bundle.putBoolean("isShow", false);

                if (item2 != null) {
                    bundle.putSerializable("friendInfo", item2);
                    bundle.putBoolean("isNew", false);

                } else {
                    item2 = LocalDB.getUsersDbHelper(Activity_ChatRoom.this).getUserInfo(roomInfo.getRoomId(), ownerId);

                    bundle.putSerializable("friendInfo", item2);
                    bundle.putBoolean("isNew", true);

                }

                Intent mIntent = new Intent(Activity_ChatRoom.this, Activity_Profile.class);
                mIntent.putExtras(bundle);
                startActivity(mIntent);
            }
        }
    };

    PreViewListener preViewListener = new PreViewListener() {
        @Override
        public void setPreViewHeight(int px) {
            setScrolldown(px);
        }
    };

    public void startFindVoice() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "음성 인식");
        startActivityForResult(intent, Constants.RESULT_SPEECH);
    }

    @Override
    public void onSavePictureSuccess(String path) {
        File temp = getCacheDir();
        String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));
        File destFile = new File(temp, time + ".jpg");

        this.path = destFile.getAbsolutePath();

        if ("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type()))
            new ImageUploadData().chatFileUpload(this, path, roomInfo.getRoomId(), roomInfo.getRoomSubject());
        else
            new ImageUploadData().chatFileUpload(this, path, roomInfo.getRoomId(), MessengerInfo.getUserName(this));
    }

    @Override
    public void onBackPressed() {
        if (layout_drawer.isDrawerOpen(Gravity.RIGHT)) {
            layout_drawer.closeDrawer(Gravity.RIGHT);
        } else if (emoticonViewFlag) {
            if (ll_reply_background.getVisibility() == View.VISIBLE) {
//                rlEmoticonPreview.setVisibility(View.GONE);
                ll_reply_background.setVisibility(View.GONE);
//                hidePreview(View.GONE);
                selectEmoticon = "";
            } else {
                emoticonViewFlag = false;
                flEmoticonContainer.setVisibility(View.GONE);
            }
        } else {
//            if (checkEmoticon()) {
            if (ll_reply_background.getVisibility() == View.VISIBLE) {
//                rlEmoticonPreview.setVisibility(View.GONE);
                ll_reply_background.setVisibility(View.GONE);
//                hidePreview(View.GONE);
                selectEmoticon = "";
            } else {
                try {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                        getSupportFragmentManager().popBackStack();
                    else {
//                        BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));
//                        Fragment_Main.getInstance().chatListUpdate();
//Fragment_Main.getInstance().GetCurrentChatList();
                        finish();
                        Statics.ROOMINFO = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            layout_drawer.closeDrawer(Gravity.RIGHT);

            Uri uri;

            if (data != null) {
                uri = data.getData();
                LogTrace.E("data not null");
            } else {
                LogTrace.E("data null");
                uri = Uri.fromFile(FileUtil.image);
            }

            if (uri != null) {
                String path = uri.getPath();
                if (uri.getAuthority() != null && uri.getAuthority().length() > 0)
                    path = getPathFromUri(uri);

                LogTrace.E("camera path : " + path);

                chatPresenter.saveFileAsScaledImage(this, path, 2);
            } else {
                chatPresenter.saveFileAsScaledImage(this, FileUtil.image.getPath(), 2);
            }
        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            List<GalleryDataModel> items = (List<GalleryDataModel>) data.getSerializableExtra("item");
            for (GalleryDataModel gdm : items) { // send picture
                chatPresenter.saveFileAsScaledImage(this, gdm.getFullPath(), 2);
            }
        } else if (requestCode == Constants.INVITE_FRIEND_CHAT_REQ && resultCode == Constants.RESULT_INVITE_ONLY_MEMBERS) {
            //회원만 초대할 경우
            layout_drawer.closeDrawer(Gravity.RIGHT);
            List<VoFriendList> newFriends = new ArrayList<>();
            if ("0".equals(roomInfo.getRoom_type())) {
                newFriends.addAll(roomInfo.getFriends());
                newFriends.addAll((List<VoFriendList>) data.getSerializableExtra("toInvite"));
                VoChatList currentChatData = new VoChatList();
                currentChatData.setRoom_type("1");
                currentChatData.setRoomId("");
                List<VoChatData> data1 = Collections.emptyList();
                currentChatData.getFriends().addAll(newFriends);
                roomInfo = currentChatData;
                topBar.setText(getChatRoomTitle());
                chatDataList = new ArrayList<>();

                adapter.setChatListData(data1);
                adapter.notifyDataSetChanged();

                newCreate = true;
            } else {
                newFriends.addAll((List<VoFriendList>) data.getSerializableExtra("toInvite"));
                roomInfo.getFriends().addAll(newFriends);
                if (!TextUtils.isEmpty(roomInfo.getRoomId())) {
                    adapter.notifyDataSetChanged();
                    inviteFriend(roomInfo.getRoomId(), newFriends);
                    ErrorController.showMessage("newFriends", newFriends.toString());
                }
            }
            List<VoFriendList> nonMemberFriends = new ArrayList<>();

            for (VoFriendList item : newFriends) {
                if (!item.isMember()) {
                    nonMemberFriends.add(item);
                }
            }
            newFriends.clear();
        }

        /*else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            if("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type()))
                new ImageUploadData().chatFileUpload(this, path, roomInfo.getRoomId(), roomInfo.getRoomSubject());
            else
                new ImageUploadData().chatFileUpload(this, path, roomInfo.getRoomId(), MessengerInfo.getUserName(this));

            lvChatListView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lvChatListView.smoothScrollToPosition(adapter.getCount());
                }
            }, 500);

        }*/

        /*else if (requestCode == Constants.INVITE_FRIEND_CHAT_REQ && resultCode == Constants.RESULT_INVITE_EVERYONE) {
            //비회원까지 초대
            layout_drawer.closeDrawer(Gravity.RIGHT);
            List<VoFriendList> newFriends = (List<VoFriendList>) data.getSerializableExtra("toInvite");
            Intent intent = new Intent(this, Activity_SendOtherSelector.class);
            intent.putExtra("toInvite", (Serializable) newFriends);
            startActivityForResult(intent, Constants.INVITE_FRIEND_CHAT_REQ_NON_MEMBER);
        } */

        else if (requestCode == Constants.INVITE_FRIEND_CHAT_REQ_NON_MEMBER && resultCode == Constants.RESULT_INVITE_EVERYONE_CHANNEL_SELECTED) {
            List<VoFriendList> newFriends = (List<VoFriendList>) data.getSerializableExtra("toInvite");
            //비회원 초대 완료
            if (newFriends != null) {
                //친구를 해당 방의 친구 목록에 추가.
                roomInfo.getFriends().addAll(newFriends);
                addChat(chatPresenter.makeNewInviteMessage(newFriends));
                adapter.notifyDataSetChanged();
                displayChatUpdate(true);
                inviteFriend(roomInfo.getRoomId(), newFriends);
            }
        } else if (requestCode == Constants.REQUEST_CHAT_ALBUM && resultCode == Constants.RESULT_CHAT_ALBUM) {
            String delItem = data.getStringExtra("delItem");
            removeChat(delItem);
        } else if (requestCode == Constants.REQUEST_CHAT_STAFF && resultCode == Constants.RESULT_CHAT_STAFF) {
            updateStaff((VoUsers) data.getSerializableExtra("staff"));
        } else if (resultCode == RESULT_OK && requestCode == Constants.RESULT_SPEECH) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String speechResult = result.get(0);
            LogTrace.E(speechResult);
            etChatInput.setText(speechResult);
            etChatInput.setSelection(speechResult.length());
        }
    }//end of onActivityResult.

    @Override
    public void onEmoticonSelected(int resid) {
        rlEmoticonPreview.setVisibility(View.VISIBLE);
        ivEmoticon.setImageResource(resid);
        selectEmoticon = getResources().getResourceEntryName(resid);
    }

    @Override
    public void receiveMsg(String roomId, VoChatData data) {
        Log.d("", "receiveMsg" + " / data : " + data.toString());
        if (data.getRoomId().equals(roomInfo.getRoomId())) {
            if (data.getOwnerId().equals(MessengerInfo.getUserId(this))) {
                if (!adapter.existChatmsg(data.getChatId())) {
                    setChatDataMsg(data);
                } else {
                    chatPresenter.setRead(this, roomId, data.getCid(), data.getChatId());
                    //채팅 리스트 메모리 업데이트.
                    adapter.updateCid(data.getChatId(), data.getCid(), data.getAttachment(), "100");
                    //어댑터 갱신.
                    adapter.notifyDataSetChanged();
                }
            } else {
                setChatDataMsg(data);
            }

            if ("1".equals(data.getChatType()) || "2".equals(data.getChatType())) {
                chatPresenter.chatRoomInfo(this, roomId);
                if ("1".equals(data.getChatType())) {
                    isNotMemnberChatFrist = true;
                }
            }
        }
    }

    @Override
    public void sendMsg(MOAData data) {
        try {
            String chatId = data.body.getString("chatId");

            if (!TextUtils.isEmpty(chatId)) {
                if ("success".equals(data.body.getString("roomCreate"))) {
                    roomInfo.setRoomId(data.body.getString("roomId"));
                    ChatDataDbHelper chatDataDb = LocalDB.getChatDataDbHelper(this);
                    ArrayList<VoChatData> list = new ArrayList<>();

                    if (messagedata == null) {
                        List<VoChatData> chatList = chatDataDb.getChatData(roomInfo.getRoomId());

                        if (chatList != null) {
                            for (VoChatData voChatData : chatList) {
                                if (chatId.equals(voChatData.getChatId())) {
                                    messagedata = voChatData;
                                    break;
                                }
                            }
                        }
                    }
                    boolean readcount;
                    if (roomInfo.getRoom_type().equals("7") || roomInfo.getRoom_type().equals("8")) {
                        readcount = false;
                    } else {
                        readcount = true;
                    }
                    messagedata.setRoomId(roomInfo.getRoomId());
                    messagedata.setStatus("100");
                    list.add(messagedata);
                    chatDataDb.checkChatData(list, readcount);
                    chatPresenter.chatRoomInfo(this, roomInfo.getRoomId());
                    chatPresenter.getMessage(this, roomInfo.getRoomId(), "0", roomInfo.getUnread());
                    chatPresenter.chatReadData(this, roomInfo.getRoomId());
                } else {
                    roomInfo.setRoomId(data.body.getString("roomId"));
                    ChatDataDbHelper chatDataDb = LocalDB.getChatDataDbHelper(this);
                    ArrayList<VoChatData> list = new ArrayList<>();

                    if (messagedata == null) {
                        List<VoChatData> chatList = chatDataDb.getChatData(roomInfo.getRoomId());

                        if (chatList != null) {
                            for (VoChatData voChatData : chatList) {
                                if (chatId.equals(voChatData.getChatId())) {
                                    messagedata = voChatData;
                                    break;
                                }
                            }
                        }
                    }
                    boolean readcount;
                    if (roomInfo.getRoom_type().equals("7") || roomInfo.getRoom_type().equals("8")) {
                        readcount = false;
                    } else {
                        readcount = true;
                    }
                    messagedata.setRoomId(roomInfo.getRoomId());
                    messagedata.setStatus("100");
                    list.add(messagedata);
                    chatDataDb.checkChatData(list, readcount);
                    chatPresenter.chatRoomInfo(this, roomInfo.getRoomId());
                    chatPresenter.getMessage(this, roomInfo.getRoomId(), "0", roomInfo.getUnread());
                    chatPresenter.chatReadData(this, roomInfo.getRoomId());
                }

                String status = Constants.CODE_ERROR;

                if (Constants.MOA_RESULT_OK.equals(data.body.getString("result"))) {
                    status = Constants.CODE_SUCCESS;
                }

                if (Constants.CODE_ERROR.equals(status)) {
                    ChatDataDbHelper chatDataDb = LocalDB.getChatDataDbHelper(this);
                    chatDataDb.updateFailMsg(chatId);
                    adapter.updateStatus(chatId, status);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String chatId = data.body.getString("chatId");

            if (!TextUtils.isEmpty(chatId)) {
                ChatEntity entity = ChatModel.getChatEntity(this, chatId);


                if (entity != null) {
                    if (entity.getStatus() == 1) {
                        if ("success".equals(data.body.getString("result"))) {
                            entity.setStatus(100);
                        } else {
                            entity.setStatus(400);
                        }

                        if (ChatModel.update(this, entity)) {
                            displayChatUpdate(true);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exitRoom(boolean result, MOAData data) {
        exitChatRoom(result);
    }

    @Override
    public void createRoom(MOAData data) {
        LBJSONObject jo = data.body.getJson("params");
        roomInfo.setRoomId(jo.getString("roomId"));
        messagedata.setRoomId(roomInfo.getRoomId());
        chatPresenter.chatRoomInfo(this, roomInfo.getRoomId());
        chatPresenter.sendMessage(this, messagedata);

        if (isNotMemnberChatFrist) {
            isNotMemnberChatFrist = false;
        }
    }

    @Override
    public void chatList(MOAData data) {
        //TODO
        LogTrace.E("@@Activity_ChatRoom chatList ");
        Log.d(TAG, "DATA : " + data.toString());

        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(this);
        LBJSONArray array = data.body.getArray("params");

        if (array != null && !array.isEmpty()) {
            ErrorController.writeLog("array Not empty");
            Log.d(TAG, "Array : " + array);
            if (db.addChatData(array, roomInfo.getRoomId(), roomInfo.getRoom_type())) {
                LogTrace.E("addChatData Success");
                ErrorController.writeLog("addChatData Success");

                ChatListDbHelper db1 = LocalDB.getChatListDbHelper(this);
                VoChatRoom voChatRoom = db1.getChatRoom(roomInfo.getRoomId());
                String roomType = "0";

                if (voChatRoom != null) {
                    roomType = voChatRoom.getRoomType();
                }

                String myId = MessengerInfo.getUserId(this);
                String toId = null;

                // 내가 보낸 사람 검색 로직, 서버에서 주질 않아 forloop를 어쩔수 없이 돌려야 한다.
                try {
                    for (Object o : array) {
                        LBJSONObject jo = (LBJSONObject) o;
                        String ownerId = jo.getString("ownerId");

                        if (!myId.equals(ownerId)) {
                            toId = ownerId;
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Object o : array) {
                    LBJSONObject jo = (LBJSONObject) o;

                    String chatId = jo.getString("chatId");
                    String senderId = jo.getString("ownerId");
                    String messageType = jo.getString("messageType");
                    String regDate = jo.getString("reg_date");

                    if (myId.equals(senderId)) {
                        VoChatData item = Statics.loadFromJsonChatList(jo);
                        ChatEntity chatEntity = ChatModel.convertChatEntity(item);

                        if (chatEntity != null && !TextUtils.isEmpty(toId)) {
                            chatEntity.setOwnerId(myId);
                            chatEntity.setSenderId(toId);

                            if (!TextUtils.isEmpty(chatEntity.getChatId()) && !TextUtils.isEmpty(chatEntity.getOwnerId()) && !TextUtils.isEmpty(chatEntity.getSenderId())) {
                                ChatModel.insert(this, chatEntity);
                            }
                        }
                    }

                    Date date;

                    try {
                        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA).parse(regDate);
                    } catch (ParseException e) {
                        date = new Date();
                    }

                    LuckyBagModel.checkLuckyBag(this, chatId, senderId, messageType, roomType, date);
                }
            }
        }

        LogTrace.E("chatList setChatData");
        setChatData();
        VoChatData item = db.getRead(roomInfo.getRoomId());
        if (item != null) {
            chatPresenter.setRead(this, roomInfo.getRoomId(), item.getCid(), item.getChatId());
        }
    }

    @Override
    public void recRead() {
        if ("2".equals(roomInfo.getRoom_type()) || "3".equals(roomInfo.getRoom_type())) {
            return;
        }

        LogTrace.E("unread count update");

        adapter.setUsersData();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onChatListResult(boolean result) {
    }

    @Override
    public void onChatListUpdate(String roomId) {
        Log.d(TAG, "onChatListUpdate");
        if (roomId.equals(MessengerInfo.getUserId(this))) {
            ChatListDbHelper db = LocalDB.getChatListDbHelper(this);
            roomInfo = db.getChatData(roomId);

            if (!"2".equals(roomInfo.getRoom_type()) && !"3".equals(roomInfo.getRoom_type())) {
                if (Integer.parseInt(roomInfo.getUsercount()) > 2) {
                    topBar.setCount(roomInfo.getUsercount());
                    Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
                } else {
                    topBar.setCountGONE();
                }
            }

            fragment_rightNavigation.setListView();
            recRead();
        }
    }

    @Override
    public void onChatRoomInfo(MOAData data) {
        String roomId = data.body.getString("roomId");
        if (roomId.equals(roomInfo.getRoomId())) {
//            if (!TextUtils.isEmpty(pushIntent)) {
//                Fragment_Main.getInstance().getRoomInfo(data);
//                return;
//            }
            updateRoomUI(data);
        }
    }

    @Override
    public void onChatReadMy(MOAData data) {
    }

    @Override
    public void OnEvent(MOAData moaData) {
        final String result = moaData.body.getString("result");
        LBJSONObject jObject = moaData.body.getJson("params");
        LogTrace.E("대화방 패킷" + moaData.body.getString("result"));

        if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_WRITE_CONFIG) {
            if (Constants.MOA_RESULT_OK.equals(result)) {
                if ("0".equals(jObject.getString("enableWriteMsg"))) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Activity_ChatRoom.this, "잡담금지 상태 입니다", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Activity_ChatRoom.this, "잡담금지가 해제 되었습니다", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } else {
                Toast.makeText(this, "일시적인 장애로 변경되지 않았습니다", Toast.LENGTH_SHORT).show();
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_RECV_WRITE_CONFIG) {
            LogTrace.E("잡담금지 수신 : " + jObject.toString());
            enableWriteMsg = jObject.getString("enableWriteMsg");
            LocalDB.getChatListDbHelper(this).updateEnableMsg(roomInfo.getRoomId(), enableWriteMsg);

            LogTrace.E("chat room write able : " + enableWriteMsg);

            if ("1".equals(enableWriteMsg)) {
                onlyStaff(true);
            } else {
                onlyStaff(false);
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_USEROUT) {
            if (Constants.MOA_RESULT_OK.equals(result)) {
                jObject = moaData.body.getJson("params");
                LocalDB.getUsersDbHelper(this).deleteUser(jObject.getString("roomId"), jObject.getString("userOutId"));
                fragment_openNavigation.kickResult();
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_RECV_USEROUT) {
            LogTrace.E("PTC_IMS_CHATROOM_RECV_USEROUT");
            kickRoom();
            refreshMoimTImeline();


        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_RECV_STAFF_CONFIG) {
            LogTrace.E("chatroom update staff");
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(this);
            VoUsers user = new VoUsers();

            user.setRoomId(jObject.getString("roomId"));
            user.setUserId(jObject.getString("staffUserId"));
            user.setStaff(jObject.getString("staff"));

            userDb.updateStaff(user);

            updateStaff(user);

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_RECV_UPDATEINFO) {
            String roomId = jObject.getString("roomId");
            LBJSONArray jArray = moaData.body.getArray("users");
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(this);
            ArrayList<VoUsers> userList = new ArrayList<>();

            for (Object o : jArray) {
                jObject = (LBJSONObject) o;

                VoUsers user = new VoUsers();

                user.setRoomId(roomId);
                user.setUserId(jObject.getString("userId"));
                user.setUserName(jObject.getString("userName"));
                user.setProfile_image(jObject.getString("profile_image"));
                user.setProfile_image_type(jObject.getString("profile_image_type"));
                user.setProfile_subject(jObject.getString("profile_subject"));
                user.setStaff(jObject.getString("staff"));

                userList.add(user);
            }

            userDb.addUsersList(userList, roomId);

            if (roomId.equals(roomInfo.getRoomId())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if ("0".equals(roomInfo.getRoom_type()) || "1".equals(roomInfo.getRoom_type()) || "4".equals(roomInfo.getRoom_type()))
                            fragment_rightNavigation.setListView();
                        else
                            fragment_openNavigation.setListView(version);
                    }
                });
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHAT_ACTION) {
            String action = jObject.getString("action");
            LogTrace.E("shake result : " + jObject.getString("action").toString());

            if ("action_count_zero".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Activity_ChatRoom.this, "흔들기 횟수가 초과되었습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if ("0".equals(action)) {
                String maxCount = jObject.getString("maxActionCount");
                String count = jObject.getString("actionCount");

                LogTrace.E("action is shake : " + count + " / " + maxCount);

                getToastView(count, maxCount);
            } else if ("voice".equals(action)) {
                final String value = jObject.getString("value");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (value.equals("0")) {
                            if (isCCastClientStarted())
                                stopCCastClient();
                            ivAudio.setImageResource(R.drawable.ic_speak_off);
                            ll_audioOnOff.setVisibility(View.GONE);
                        } else {
                            ll_audioOnOff.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_ROOMCLEAR) {

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHATROOM_RECV_ROOMCLEAR) {
            LogTrace.E("clean result : " + jObject.toString());

            String roomId = jObject.getString("roomId");
            String cId = jObject.getString("cId");

            LocalDB.getChatDataDbHelper(this).cleanChatData(roomId, cId);

            if (roomId.equals(roomInfo.getRoomId())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogTrace.E("OnEvent PTC_IMS_CHATROOM_RECV_ROOMCLEAR  setChatData");
                        setChatData();
                    }
                });
            }

//            LogTrace.E("clean packet");
//            WAVEMainActivity.staticMainActivity.chatListReload();


        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHAT_CHECK_RECALLMSG) {
            LogTrace.E("회수 : " + result);

            if (Constants.MOA_RESULT_OK.equals(result)) {
                Toast.makeText(this, "회수 성공", Toast.LENGTH_SHORT).show();
            } else if (Constants.MOA_RESULT_ERROR.equals(result)) {
                Toast.makeText(this, "회수 실패", Toast.LENGTH_SHORT).show();
            } else if ("chat does not exist".equals(result)) {
                Toast.makeText(this, "이미 회수 된 메세지 입니다.", Toast.LENGTH_SHORT).show();
                deleteChat(jObject.getString("roomId"), jObject.getString("chatId"));
            }

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHAT_RECALLMSG) {
            LogTrace.E("회수 : " + jObject.toString());
            deleteChat(jObject.getString("roomId"), jObject.getString("chatId"));

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHAT_REPORTMSG) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Constants.MOA_RESULT_OK.equals(result)) {
                        Toast.makeText(Activity_ChatRoom.this, "신고 되었습니다.", Toast.LENGTH_SHORT).show();
                    } else if (Constants.MOA_RESULT_ERROR.equals(result)) {
                        Toast.makeText(Activity_ChatRoom.this, "일시적인 오류입니다.", Toast.LENGTH_SHORT).show();
                    } else if ("already_reported".equals(result)) {
                        Toast.makeText(Activity_ChatRoom.this, "이미 신고 되었습니다.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else if (moaData.ptc == PacketTypes.PTC_IMS_CHAT_BLINDMSG) {
            String roomId = jObject.getString("roomId");
            String chatId = jObject.getString("chatId");

            LocalDB.getChatDataDbHelper(this).updateBlind(roomId, chatId);

            if (roomId.equals(roomInfo.getRoomId())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogTrace.E("OnEvent PTC_IMS_CHAT_BLINDMSG  setChatData");
                        setChatData();
                    }
                });
            }

        }
    }

    /**
     * 방장에 의해 모임 강제탈퇴후 모임타임라인 화면 처리.
     * 20180322 leeyunsu
     */
    private void refreshMoimTImeline() {
        if (Activity_MoimTimeLineList.activity != null) {
            Activity_MoimTimeLineList.activity.MoimProfileRequest();
        }
    }

    public void audioOnOff(String value) {
        if (value.equals("On")) {
            startCCastClient();
            ll_audio.setVisibility(View.VISIBLE);
            ivAudio.setImageResource(R.drawable.ic_speak_on);
        } else if (value.equals("Off")) {
            stopCCastClient();
            ll_audio.setVisibility(View.GONE);
            ivAudio.setImageResource(R.drawable.ic_speak_off);
        }
    }

    //음성방송
    private void updateConnectionStatus(Boolean isConnected) {
        if (isConnected) {
            LogTrace.E("CCastClient Connected");
        } else {
            LogTrace.E("CCastClient Disconnected");
        }
    }

    private void updateVolume(final int volume) {
//        LogTrace.E("VolumeChanged : " + volume);
    }

    private Boolean isCCastClientStarted() {
        return CCastClient.getInstance().isStarted;
    }

    private void startCCastClient() {
        String ruid = roomInfo.getRoomId(); //ruid
        String uuid = UUID.randomUUID().toString();  //uuid
        CCastClient.getInstance().start(this, Constants.AUDIO_PROC, Constants.AUDIO_PORT, ruid, uuid);
    }

    public void stopCCastClient() {
        CCastClient.getInstance().stop(this);
    }

    @Override
    public void onClientConnectChanged(Boolean isConnected) {
        updateConnectionStatus(isConnected);
    }

    @Override
    public void onVolumeChanged(int volume) {
        updateVolume(volume);
    }

    @Override
    public void onStartSvc() {
        CCastClient.wasStarted = true;
        Toast.makeText(this, "음성방송듣기 시작", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStopSvc() {
        CCastClient.wasStarted = false;
        Toast.makeText(this, "음성방송듣기 종료", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int error) {
        ErrorController.showMessage("[CCastClient] Audio Error : " + error);
    }

    public void setListScroll() {
        if (visibleLastItem) {
            lvChatListView.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    lvChatListView.setSelection(chatDataList.size() - 1);
                }
            }, 10);
        }
    }

    public void search(View v, String orderType, boolean isUp) {

        String searchWord = editTextSearchInput.getText().toString();
        long chat_room_seq = Long.parseLong(chatDataList.get(0).getChatData_seq());
        long searchDate = db.getSearchFirstDate(chat_room_seq, searchWord,
                orderType);

        db.getSearchChatCount(editTextSearchInput.getText().toString());

        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date to = null;

        if (!currentSearchWord.equals(searchWord)) {
            currentSearchWord = searchWord;
            searshedIndex = -1;
            searchedDate = -1;
            isSearchStart = true;
        }

        if (searshedIndex == -1) {
            searshedIndex = chatDataList.size() - 1;
            try {
                to = transFormat.parse(chatDataList.get(searshedIndex).getReg_date());
                searchedDate = to.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
//            lvChatListView.setSelection(searshedIndex);
        }

        if (isSearchStart) {
            isSearchStart = false;
            for (int i = searshedIndex; i >= 0; i--) {
                try {
                    to = transFormat.parse(chatDataList.get(i).getReg_date());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (searchedDate >= to.getTime()
                        && (chatDataList.get(i).getMessage().toLowerCase()
                        .contains(searchWord.toLowerCase()) || (chatDataList
                        .get(i).getImagePath() != null
                        && chatDataList.get(i).getImagePath() != null && chatDataList
                        .get(i).getImagePath()
                        .toLowerCase()
                        .contains(searchWord.toLowerCase())))) {
                    // if(searchedDate > items.get(i).getRegDate() &&
                    // (items.get(i).getContents().contains(searchWord))) {
                    searshedIndex = i;
                    searchedDate = to.getTime();
                    for (int j = 0; j < chatDataList.size(); j++) {
                        if (j != searshedIndex) {
                            chatDataList.get(j).setSearched(false);
                        } else {
                            chatDataList.get(j).setSearched(true);
                        }
                    }
                    adapter.notifyDataSetChanged();

                    lvChatListView.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            lvChatListView.setSelection(searshedIndex);
                        }
                    }, 100);
                    return;
                }
            }
        } else {
            if (isUp) {
                for (int i = searshedIndex; i >= 0; i--) {


                    try {
                        to = transFormat.parse(chatDataList.get(i).getReg_date());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (searchedDate > to.getTime()
                            && (chatDataList.get(i).getMessage().toLowerCase()
                            .contains(searchWord.toLowerCase()) || (chatDataList
                            .get(i).getImagePath() != null
                            && chatDataList.get(i).getImagePath() != null && chatDataList
                            .get(i).getImagePath()
                            .toLowerCase()
                            .contains(searchWord.toLowerCase())))) {
                        // if(searchedDate > items.get(i).getRegDate() &&
                        // (items.get(i).getContents().contains(searchWord))) {
                        searshedIndex = i;

                        searchedDate = to.getTime();
                        for (int j = 0; j < chatDataList.size(); j++) {
                            if (j != searshedIndex) {
                                chatDataList.get(j).setSearched(false);
                            } else {
                                chatDataList.get(j).setSearched(true);
                            }
                        }
                        adapter.notifyDataSetChanged();

                        lvChatListView.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                lvChatListView.setSelection(searshedIndex);
                            }
                        }, 100);


                        return;
                    }
                }

                if (searchDate != -1) {
                    db.getChatMsgListAsDate(Fragment_Main.getInstance()
                            .getActivity(), chatDataList, chat_room_seq, searchDate);
                    // adapter.notifyDataSetChanged();
                    // if(isSearchOpened) {
                    // isSearchOpened = false;
                    // linearLayoutSearch.setVisibility(View.GONE);
                    // hideSoftKeyboad(v);
                    // }
                    for (int i = searshedIndex; i >= 0; i--) {
                        try {
                            to = transFormat.parse(chatDataList.get(i).getReg_date());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (searchedDate > to.getTime()
                                && (chatDataList.get(i).getMessage().toLowerCase()
                                .contains(searchWord.toLowerCase()) || (chatDataList
                                .get(i).getImagePath() != null
                                && chatDataList.get(i).getImagePath() != null && chatDataList
                                .get(i).getImagePath()
                                .toLowerCase()
                                .contains(searchWord.toLowerCase())))) {
                            // if(searchedDate > items.get(i).getRegDate() &&
                            // (items.get(i).getContents().contains(searchWord))) {
                            searshedIndex = i;
                            searchedDate = to.getTime();
                            for (int j = 0; j < chatDataList.size(); j++) {
                                if (j != searshedIndex) {
                                    chatDataList.get(j).setSearched(false);
                                } else {
                                    chatDataList.get(j).setSearched(true);
                                }
                            }
                            adapter.notifyDataSetChanged();

                            lvChatListView.postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    lvChatListView.setSelection(searshedIndex);
                                }
                            }, 100);

                            return;
                        }
                    }
                }
            } else {
                for (int i = searshedIndex; i < chatDataList.size(); i++) {
                    try {
                        to = transFormat.parse(chatDataList.get(i).getReg_date());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (searchedDate < to.getTime()
                            && (chatDataList.get(i).getMessage().toLowerCase()
                            .contains(searchWord.toLowerCase()) || (chatDataList
                            .get(i).getImagePath() != null
                            && chatDataList.get(i).getImagePath() != null && chatDataList
                            .get(i).getImagePath()
                            .toLowerCase()
                            .contains(searchWord.toLowerCase())))) {
                        // if(searchedDate < items.get(i).getRegDate() &&
                        // (items.get(i).getContents().contains(searchWord))) {
                        searshedIndex = i;
                        searchedDate = to.getTime();
                        for (int j = 0; j < chatDataList.size(); j++) {
                            if (j != searshedIndex) {
                                chatDataList.get(j).setSearched(false);
                            } else {
                                chatDataList.get(j).setSearched(true);
                            }
                        }
                        adapter.notifyDataSetChanged();

                        lvChatListView.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                lvChatListView.setSelection(searshedIndex);
                            }
                        }, 100);
                        return;
                    }
                }
            }
        }

    }

    public void PageSearch() {
        int currentSearchCount = 0;
        int allSearchCount = db.getSearchChatCount(editTextSearchInput.getText().toString());


        for (int i = 0; i < chatDataList.size(); i++) {
            if (chatDataList.get(i).getMessage().contains(editTextSearchInput.getText().toString())) {
                int count = StringUtils.countMatches(chatDataList.get(i).getMessage(), editTextSearchInput.getText().toString());
                currentSearchCount += count;

            }
        }
        if (currentSearchCount < allSearchCount) {

            currentPage++;
            final List<VoChatData> pastList = pageList.get(currentPage);
            chatDataList.addAll(0, pastList);
            adapter.setChatListData(chatDataList);
            adapter.notifyDataSetChanged();

        }

    }

    public void setContactCount() {

        roomInfo = Statics.ROOMINFO;
        tv_contact.setVisibility(View.VISIBLE);
        tv_contact.setText("접속자 (" + roomInfo.getUsercount() + "명)");
        Statics.ROOMINFO.setUsercount(roomInfo.getUsercount());
    }
}