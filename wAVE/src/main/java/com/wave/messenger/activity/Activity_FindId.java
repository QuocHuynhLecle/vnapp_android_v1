package com.wave.messenger.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.DeviceInfo;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;

import org.json.JSONObject;
import org.json.simple.JSONArray;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by Joo on 2018. 5. 29.
 */

public class Activity_FindId extends AppCompatActivity implements MOAEventListener {

    private static final String TAG = Activity_FindId.class.getSimpleName();

    private EditText edit_name;
    private EditText edit_tel;
    private Button btn_find_id;
    private TextView text_result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_id);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.titleFindId));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MOAClient.getInstance().addEventListener(this);
        initView();
        setOnClickEvent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MOAClient.getInstance().removeEventListener(this);
    }

    private void initView() {
        edit_name = findViewById(R.id.et_name);
        edit_tel = findViewById(R.id.et_tel);
        btn_find_id = findViewById(R.id.bt_findId);
        text_result = findViewById(R.id.tv_result);
    }

    private void setOnClickEvent() {
        btn_find_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempSocketOpen();
            }
        });
    }

    private void tempSocketOpen() {
        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);

        MOAClient.getInstance().setProperties("userId", "test01");
        MOAClient.getInstance().setProperties("userPassword", "12345");
        MOAClient.getInstance().setProperties("tel", "");
        MOAClient.getInstance().setProperties("userName", "");
        MOAClient.getInstance().setProperties("candleUserType", "");
        MOAClient.getInstance().setProperties("riskType", "");
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken", MessengerInfo.getUserTocken(getBaseContext()));
        MOAClient.getInstance().setProperties("branchName", MessengerInfo.getBranchName());
        MOAClient.getInstance().setProperties("login_ver", "1");
        MOAClient.getInstance().setProperties("agree", "1"); // 약관 동의 값을 최초 1회만 보내면됩니다
        MOAClient.getInstance().login();
    }

    private void requestFindId() {

        String userName = "" + edit_name.getText().toString();
        String tel = "" + edit_tel.getText().toString();

        if (TextUtils.isEmpty(userName)) {
            Toast.makeText(this, "이름을 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(tel)) {
            Toast.makeText(this, "전화번호를 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            PacketBody body = new PacketBody();
            body.putParams("userName", userName);
            body.putParams("tel", tel);

//            body.putParams("userName", "테스트7");
//            body.putParams("tel", "01000000007");

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, 0x00030020, body);    // 아이디 찾기
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnEvent(MOAData moaData) {
        mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            try {
                final MOAData data = (MOAData) msg.obj;
                 Log.e(TAG, "data body :" + data.body.toString());

                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_LOGIN_LOGIN:
                        requestFindId();
                        break;

                    case 0x00030020:
                        String result = (String) data.body.get("result");
                        switch (result) {
                            case "success":
//                                long count = (long) data.body.get("count");
                                JSONArray params = data.body.getArray("params");
                                String paramsJsonStr = params.toJSONString();
                                org.json.JSONArray jsonParams = new org.json.JSONArray(paramsJsonStr); // JSONArray converting

                                int paramsCnt = jsonParams.length();
                                ArrayList<String> userIds = new ArrayList<>(); // 사용자 아이디 리스트

                                for (int i = 0; i < paramsCnt; i++) {
                                    JSONObject jsonObject = jsonParams.getJSONObject(i); //i번째 Json데이터를 가져옴
                                    userIds.add(i, jsonObject.getString("userId"));  //userId 값을 list에 저장
                                    Log.i(TAG, "JSON Object = " + jsonObject);
                                    Log.i(TAG, "UserIdList = " + userIds.toString().replace("[", "").replace("]", ""));
                                }

                                text_result.setVisibility(View.VISIBLE);
                                text_result.setText(userIds.toString().replace("[", "").replace("]", ""));
                                MOAClient.getInstance().logout();   // 로그아웃 호출하여 소켓 연결 해제
                                break;

                            case "error":
                                String reason = (String) data.body.get("reason");
                                switch (reason) {
                                    case "username_empty":
                                        Toast.makeText(Activity_FindId.this, "이름을 확인해주세요.", Toast.LENGTH_SHORT).show();
                                        break;

                                    case "tel_empty":
                                        Toast.makeText(Activity_FindId.this, "전화번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                                        break;

                                    case "user_not_found":
                                        Toast.makeText(Activity_FindId.this, "사용자를 찾을 수 없습니다.", Toast.LENGTH_SHORT).show();
                                        break;

                                    default:
                                        Toast.makeText(Activity_FindId.this, reason, Toast.LENGTH_SHORT).show();
                                        break;
                                }
                                MOAClient.getInstance().logout();   // 로그아웃 호출하여 소켓 연결 해제
                                break;

                            default:
                                break;
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });
}
