package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.adapter.GalleryTopAdapter;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.helper.UITopbarHelper;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.mvp.Gallery.GalleryPresenter;
import com.wave.messenger.share.HMMessengerShare;
import com.wave.messenger.util.Constants;

/**
 * Params - int maxCount <br/>
 * @author geniu
 *
 */
public class Activity_Gallery extends Activity {
	
	private UITopbarHelper llTopBar;
	private ListView lvTitleList;
	
	private GalleryPresenter galleryPresenter;
	private GalleryTopAdapter adapter;
	private GalleryDataModel item = null;
	
	private int maxCount = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//화면 풀사이즈
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Window w = getWindow(); // in Activity's onCreate() for instance
			w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
			w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		}
		setContentView(R.layout.fragment_gallery);
		
		this.galleryPresenter = new GalleryPresenter();
		getData();
		initView();
		setEvent();
		setListView();
	}
	
	private void getData() {
		Bundle bundle = getIntent().getExtras();
		
		if(bundle.containsKey("maxCount")){
			maxCount = bundle.getInt("maxCount");
		}else{ //no parameter, finish.
			finish();
		}
	}

	private void initView() {

		llTopBar = new UITopbarHelper((LinearLayout)findViewById(R.id.llTopBar));
		lvTitleList = (ListView)findViewById(R.id.lvTitleList);

//		llTopBar.setText(getResources().getString(R.string.title_gallery));
		llTopBar.setText(getResources().getString(R.string.title_gallery));
		llTopBar.setGravity(Gravity.CENTER);
		llTopBar.setTextColor(getResources().getColor(R.color.black));
//		llTopBar.setCount(null);
//		llTopBar.setSearchFriendClickListener(null);
		llTopBar.setHomeVisibility(View.VISIBLE);
	}

	private void setEvent() {
		llTopBar.setHomeBtnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		lvTitleList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    item = (GalleryDataModel) adapter.getItem(position);
                    Intent intent = new Intent(Activity_Gallery.this, Activity_GalleryDetail.class);
                    intent.putExtra("maxCount", maxCount);
                    intent.putExtra("item", item);
                    startActivityForResult(intent, Constants.GALLERY_REQUEST);
                    overridePendingTransition(R.anim.anim_slide_in_right, R.anim.no_change);
                } else {
                    Dialog_Common.showCameraPermissionDialog(Activity_Gallery.this);
                }
			}
		});
	}

	private void setListView() {
		adapter =  new GalleryTopAdapter(galleryPresenter.getTitles(this), this);
		lvTitleList.setAdapter(adapter);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK){
			//Selection Finished. Propagate data back to the caller
			setResult(RESULT_OK, data);
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
	}
}
