package com.wave.messenger.activity;


import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Emoticon.Emoticon_Activity;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.Emoticon.SoftKeyboard;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_WriteDelete;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMsgItem;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 *
 * 모임타임라인 - 댓글 수정화면
 *
 * Activity_MoimTimeLineList - Activity_TimeLineDetail
 *
 */
public class Activity_MoimReplyModify extends BaseActivity {

    String TAG = this.getClass().getName();
    String mAtch;   //첨부 데이터 파일
    HashMap<String, String> mimgHash = new HashMap<>();   //이미지의 getView값과 이미지페스를 저장
    HashMap<String, String> mFileHash = new HashMap<>();   //파일의 getView값과 파일페스를 저장

    Constants.UPLOAD_TYPE UPLOAD_TYPE;

    //String noti="0";   //공지 여부(int) 0:일반, 1:공지 2:긴급공지

    private SoftKeyboard softKeyboard;
    private LinearLayout ll_write_all;
    private String emoticon_num;
    public static Context context;
    private String activityName;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_write_modify);
        initView();
        setOnClickEvent();

        context = this;
        //글수정 본문내용처리
        //if (!SharedObject.getProperty_boolean(Activity_MoimReplyModify.this, Constants.MOIM_WRITE_TYPE, true)) {    //true:글쓰기 , false:글수정
            findViewById(R.id.edt_write).setVisibility(View.GONE); //첫줄 edtext숨김
            setContent(SharedObject.getProperty_string(Activity_MoimReplyModify.this, Constants.MOIM_MSG, ""));
        //}

        ll_write_all = (LinearLayout) findViewById(R.id.ll_write_all);

    }


    public void setEmoticon(String emoticon) {
        this.emoticon_num = String.valueOf(emoticon);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), getDrawableResourceByName(emoticon));
        addImageview(bm);
    }

    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(this,str);
        String packageName = getPackageName();
        return getResources().getIdentifier(emoticon, "drawable", packageName);
    }


    private void setOnClickEvent() {

        ((EditText) findViewById(R.id.edt_write)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //TODO 2자이상 체크 이미지추가하며 조건 수정...
                /* if(count >2){
                     findViewById(R.id.imgb_right).setEnabled(true);
                }else{
                     findViewById(R.id.imgb_right).setEnabled(false);
                 }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ((EditText) findViewById(R.id.edt_write)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
            }
        });
        ((EditText) findViewById(R.id.edt_write)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                emoticon_gone();
            }
        });

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emoticon_gone();

                moimBoardUpdateRequest(getTTl(), getJsonArrContent(), "0");

                /*if (!SharedObject.getProperty_boolean(Activity_MoimWriteModify.this, Constants.MOIM_WRITE_TYPE, true)) {    //true:글쓰기 , false:글수정
                    //글수정
                    Toast.makeText(getApplicationContext(), "글수정 완료", Toast.LENGTH_SHORT).show();


                    //moimWriteRequest(lines[0], getJsonArrContent());



                } else {*/    //글쓰기 완료버튼


                   /* String strWrite = ((EditText) findViewById(R.id.edt_write)).getText().toString().trim();
                    String[] lines = strWrite.split("\n");

                    moimWriteRequest(lines[0], getJsonArrContent());*/


                    //글게시 항목선택
                    /*final Dialog_WriteSelect dialog_WriteSelect = new Dialog_WriteSelect(Activity_MoimWriteModify.this);

                    dialog_WriteSelect.setListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            switch (view.getId()){
                                case R.id.tv_cancle:
                                    dialog_WriteSelect.dismiss();
                                    break;

                                case R.id.tv_confirm:   //완료
                                   Log.e(TAG,"dialog_WriteSelect.getSelect(): "+dialog_WriteSelect.getSelect());


                                    //dialog_WriteSelect.getSelect()    //TODO 파라미터 전달 예정. 프로토콜 작업중..

                                    String strWrite = ((EditText) findViewById(R.id.edt_write)).getText().toString().trim();
                                    String[] lines = strWrite.split("\n");

                                    moimWriteRequest(lines[0], getJsonArrContent());

                                    dialog_WriteSelect.dismiss();
                                    break;
                            }
                        }
                    });
                    dialog_WriteSelect.show();*/


               // }
            }
        });

        findViewById(R.id.imgb_emoticon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ErrorController.showMessage("clicked Emotion");
                View view = Activity_MoimReplyModify.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) Activity_MoimReplyModify.this.getSystemService(Activity_MoimReplyModify.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                findViewById(R.id.imgb_emoticon).setSelected(!findViewById(R.id.imgb_emoticon).isSelected());
//                checkSendBtn();

                if (findViewById(R.id.imgb_emoticon).isSelected()) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.flEmoticonContainer, new Emoticon_Activity(0, Activity_MoimReplyModify.this));
                    fragmentTransaction.commit();
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.VISIBLE);


                } else {
                    ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
                }

            }
        });

        findViewById(R.id.imgb_picture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
                UPLOAD_TYPE = Constants.UPLOAD_TYPE.image;

                Toast.makeText(getApplicationContext(), "사진", Toast.LENGTH_SHORT).show();

                Util.hideKeyboard(Activity_MoimReplyModify.this, (EditText) findViewById(R.id.edt_write));

//                if(Util.CheckPermission(Activity_MoimWrite.this)){
//                    Dialog_Camera.setCamera(Activity_MoimWrite.this);
//
//                } else {
//                    //Dialog_Camera.showCameraPermissionDialog(Activity_MoimWrite.this);
//                }


                Dialog_Camera.getMoimWriteCameraDialog(Activity_MoimReplyModify.this).show();
            }
        });

        findViewById(R.id.imgb_attachment).setOnClickListener(new View.OnClickListener() {  //파일첨부
            @Override
            public void onClick(View v) {
                emoticon_gone();
                UPLOAD_TYPE = Constants.UPLOAD_TYPE.file;

                //Toast.makeText(getApplicationContext(), "파일 첨부", Toast.LENGTH_SHORT).show();
                onBrowse();
                Util.hideKeyboard(Activity_MoimReplyModify.this, (EditText) findViewById(R.id.edt_write));

            }
        });



        findViewById(R.id.imgb_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emoticon_gone();
                Toast.makeText(getApplicationContext(), "설정", Toast.LENGTH_SHORT).show();
            }
        });


        //공지
        /*findViewById(R.id.imgb_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon_gone();

                //상단 초대 버튼
                if (Util.getPermissionLv(Util.getMymLv(Activity_MoimWriteModify.this), Util.getNotiTy(Activity_MoimWriteModify.this))) {

                    if (findViewById(R.id.imgb_notification).isSelected()) {
                        findViewById(R.id.imgb_notification).setSelected(false);

                    } else {
                        findViewById(R.id.imgb_notification).setSelected(true);
                        findViewById(R.id.imgb_emergency).setSelected(false);
                    }

                } else {
                    //layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "공지 등록권한이 없습니다", Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        //긴급공지
        /*findViewById(R.id.imgb_emergency).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon_gone();

                //상단 초대 버튼
                if (Util.getPermissionLv(Util.getMymLv(Activity_MoimWriteModify.this), Util.getNotiTy(Activity_MoimWriteModify.this))) {

                    if (findViewById(R.id.imgb_emergency).isSelected()) {
                        findViewById(R.id.imgb_emergency).setSelected(false);

                    } else {
                        findViewById(R.id.imgb_emergency).setSelected(true);
                        findViewById(R.id.imgb_notification).setSelected(false);

                    }

                } else {
                    //layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "긴급공지 등록 권한이 없습니다", Toast.LENGTH_SHORT).show();
                }

            }
        });*/


    }

    private void emoticon_gone() {
        findViewById(R.id.imgb_emoticon).setSelected(false);
        ((FrameLayout) findViewById(R.id.flEmoticonContainer)).setVisibility(View.GONE);
    }

    /**
     * ttl 타이틀 값을 가져온다
     */
    private String getTTl() {
        String[] lines = new String[0];

        //mllayout
        //LinearLayout llLayout = (LinearLayout)findViewById(R.id.ll_write);
        String ttl;
        String returnTitle = null;
        for (int i = 1; i < mllayout.getChildCount(); i++) {
            Log.e(TAG, "llLayout.getChildCount(), " + mllayout.getChildCount());

            if (mllayout.getChildAt(i) instanceof EditText) {
                ttl = ((EditText) mllayout.getChildAt(i)).getText().toString().trim();
                Log.e(TAG, "llLayout ttl " + ttl);

                lines = ttl.split("\n");

                for (int t = 0; t < lines.length; t++) {

                    if (!TextUtils.isEmpty(lines[t])) {
                        Log.e(TAG, "lines[t] :" + lines[t]);
                        return lines[t];
                    }


                }


                Log.e(TAG, "llLayout lines " + lines);
                break;
            }
        }

        return returnTitle;
    }


    private void initView() {
        /*if (SharedObject.getProperty_boolean(Activity_MoimWriteModify.this, Constants.MOIM_WRITE_TYPE, true)) {    //true:글쓰기 , false:글수정
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_write));
        } else {
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_modify));
        }*/

        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_modify));


        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
        findViewById(R.id.tv_topbar_name).setVisibility(View.GONE);
        findViewById(R.id.imgb_right).setVisibility(View.VISIBLE);

        //((ImageButton)findViewById(R.id.imgb_back_btn)).setBackgroundResource(R.drawable.selector_close);
        ((ImageButton) findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.ic_close_press);

        mllayout = (LinearLayout) findViewById(R.id.ll_write);


        Util.showSoftKeyboard(this, findViewById(R.id.edt_write));

        //((ImageButton)findViewById(R.id.imgb_back_btn)
        //((ImageButton)findViewById(R.id.imgb_right)
    }


    /**
     * 글수정
     * 텍스트와  이미지 넣기
     * <p>
     * 파일첨부 - 삭제 기능만.
     */
    private void setContent(String msg) {
        Log.e(TAG, "setContent: " + msg);

        LinearLayout llLayout = (LinearLayout) findViewById(R.id.ll_write);


        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(msg);

        mAddImageCount = 0;
        mAddFileCount = 0;

        for (VoMsgItem item : arMsgItem) {

            switch (item.getType()) {
                case "0": //text
                    EditText edt_text = new EditText(Activity_MoimReplyModify.this);
                    edt_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    edt_text.setText(item.getData());
                    // edt_text.setTextColor(getResources().getColor(R.color.black));
                    edt_text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
                    edt_text.setTextColor(Util.getColor(this, R.color.gray_6c));
                    edt_text.setBackgroundColor(Color.TRANSPARENT);

                    llLayout.addView(edt_text);
                    break;

                case "1":  //image
                    final ImageView imgview = new ImageView(Activity_MoimReplyModify.this);
                    imgview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    //이미지 삭제
                    imgview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimReplyModify.this);
                            dialog_WriteDelete.show();

                            dialog_WriteDelete.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_WriteDelete.dismiss();

                                    int i = v.getId();

                                    if (i == R.id.tv_option0) {
                                        Toast.makeText(Activity_MoimReplyModify.this, "삭제", Toast.LENGTH_SHORT).show();

                                        mllayout.removeView(imgview);

                                    }
                                }
                            });
                        }
                    });


                    mimgHash.put(mAddImageCount + "", item.getData());

                    imgview.setId(mAddImageCount);

                    Glide.with(Activity_MoimReplyModify.this).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData()).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                            .dontAnimate().into(imgview);

                    llLayout.addView(imgview);

                    mAddImageCount++;
                    break;


                case "3": //파일첨부  //TODO
                    final View view = (View) getLayoutInflater().inflate(R.layout.inflate_fileupload, null);

                    TextView tv = (TextView) view.findViewById(R.id.tv_filename);
                    tv.setText(item.getData());

                    //이미지 삭제 다이얼로그
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimReplyModify.this);
                            dialog_WriteDelete.show();

                            dialog_WriteDelete.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog_WriteDelete.dismiss();

                                    int i = v.getId();

                                    if (i == R.id.tv_option0) {
                                        Toast.makeText(Activity_MoimReplyModify.this, "삭제", Toast.LENGTH_SHORT).show();

                                        mllayout.removeView(view);

                                    }
                                }
                            });
                        }
                    });

                    mFileHash.put(mAddFileCount + "", item.getData());
                    view.setId(mAddFileCount);

                    mAddFileCount++;

                    view.setPadding(0, 20, 0, 0);
                    llLayout.addView(view);

                    break;

                case "4":
                    ImageView emoticon = new ImageView(Activity_MoimReplyModify.this);
                    emoticon.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));


                    emoticon.setImageResource(Integer.parseInt(item.getData()));

                    llLayout.addView(emoticon);
                    break;


            }
        }

        EditText edt_text = new EditText(Activity_MoimReplyModify.this);
        edt_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        //edt_text.setTextColor(getResources().getColor(R.color.black));
        edt_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        edt_text.setTextColor(Util.getColor(this, R.color.gray_6c));
        edt_text.setBackgroundColor(Color.TRANSPARENT);
        llLayout.addView(edt_text);


        //공지값 처리
        /*Log.e(TAG, "MOIM_NOTI : " + SharedObject.getProperty_string(this, Constants.MOIM_NOTI, "0"));
        switch (SharedObject.getProperty_string(this, Constants.MOIM_NOTI, "0")) {
            case "0":
                break;
            case "1":
                findViewById(R.id.imgb_notification).setSelected(true);
                break;
            case "2":
                findViewById(R.id.imgb_emergency).setSelected(true);
                break;

        }*/


    }


    /**
     * 모임리스트 글쓰기
     * PTC_IMS_MOIM_BOARD_WRITE
     *
     * @param
     */
    /*private void moimWriteRequest(String title, String msg) {
        MOALog.w(TAG + " moimWriteRequest" + title + " message: " + msg);

        Moa.moimBoardWriteRequest(Activity_MoimWriteModify.this, title, msg, "", "", "", "", getNotiValue(), mMainActvtHandler);
    }*/


    /**
     * 공지,긴급공지 설정값을 가져온다.
     */
    /*private String getNotiValue() {
        String noti = "0";
        if (findViewById(R.id.imgb_notification).isSelected()) {
            noti = "1";
        } else if (findViewById(R.id.imgb_emergency).isSelected()) {
            noti = "2";
        }
        return noti;
    }*/


    /**
     * 7.24 모임 글 수정
     */
    private void moimBoardUpdateRequest(String ttl, String msg, String noti) {
        MOALog.w(TAG + " moimBoardUpdateRequest" + ttl + " message: " + msg);
        Moa.moimBoardUpdateRequest(Activity_MoimReplyModify.this, ttl, msg, noti, mMainActvtHandler);
    }


    /**
     * 사진촬영후 또는  겔러리 이미지 선택후 크
     *
     * @param imageUri
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_MoimOpen", "onActivityResult CAMERA_REQUEST");

            startCropImageActivity(data.getData());
        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {

            Log.e("Activity_MoimOpen", "onActivityResult GALLERY_REQUEST");

            startCropImageActivity(data.getData());

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    //((ImageView) findViewById(R.id.imgv_type)).setImageBitmap(bitmap);

                    //TODO
                    addImageview(bitmap);
                    //findViewById(R.id.imgv_type).setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //TODO 파일 업로드
                profileUpload(Activity_MoimReplyModify.this, resultUri.getPath());


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE " + resultCode);
            }

        }

        //파일 첨부
        switch (requestCode) {
            case Constants.ACTIVITY_CHOOSE_FILE:
                Uri uri = data.getData();
                //profileUpload(Activity_MoimWrite.this, resultUri.getPath());
                //String filePath = getRealPathFromURI(uri);


                Uri selectedImageUri = data.getData();

//                String filemanagerstring = selectedImageUri.getPath();


                String filepath = getRealPathFromURI(selectedImageUri);
                String filepath2 = getPath(this, selectedImageUri);
                Log.e(TAG, "FilePath3 : " + filepath);
                String selectedImagePath;
                //MEDIA GALLERY
                //selectedImagePath = getPath(selectedImageUri);
                //Toast.makeText(this.getApplicationContext(), selectedImagePath, Toast.LENGTH_SHORT).show();
                //change imageView1
                //imageView1.setImageURI(selectedImageUri);

                addFileView(filepath2);

                profileUpload(Activity_MoimReplyModify.this, filepath2);

                break;
        }
    }


    /**
     * 파일 선택
     */
    public void onBrowse() {

        boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            Intent intent = new Intent();
            intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, Constants.ACTIVITY_CHOOSE_FILE);
        } else {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, Constants.ACTIVITY_CHOOSE_FILE);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            //HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            //THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }



    // 파일경로 테스트
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


//테스트끝


    private String getRealPathFromURI(Uri uri) {
        String filePath = "";
        filePath = uri.getPath();
//        if (filePath.startsWith("/storage"))
//            return filePath;

        Log.i("TEST", "file_path1 : " + filePath);

        String wholeID = DocumentsContract.getDocumentId(uri);


        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        Log.e(TAG, "id = " + id);

        String[] column = {MediaStore.Files.FileColumns.DATA};


        String sel = MediaStore.Files.FileColumns.DATA + " LIKE '%" + id + "%'";

        Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"),
                column, sel, null, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        Log.i("TEST", "file_path2 : " + filePath);
        cursor.close();
        return filePath;
    }


    LinearLayout mllayout;
    int mAddImageCount = 0;   //추가됨 이미지 카운트
    int mAddFileCount = 0;

    /**
     * 글쓰기화면에
     * 겔러리,사진촬영으로 받아온 이미지를 넣고 하단에 Edittext를 추가한다
     *
     * @param bitmap 글수정시 오류.. 기존 이미지에는 mAddImageCount 값이 없음
     *               <p>
     *               XXX이미지를 넣은후 간격.
     */
    private void addImageview(Bitmap bitmap) {
        //mllayout.setPadding(50,50,50,50);

        mllayout.setOrientation(LinearLayout.VERTICAL);
        mllayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        int padding = getResources().getDimensionPixelOffset(R.dimen.dp_15);

        final ImageView imgview = new ImageView(Activity_MoimReplyModify.this);
        imgview.setImageBitmap(bitmap);
        /*imgview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));*/

        imgview.setLayoutParams(new LinearLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));

        imgview.setPadding(padding, 10, padding, 10);
        //imgview.setPadding(50,50,50,50);
        //imgview.setBackgroundColor(Color.RED);
        imgview.setScaleType(ImageView.ScaleType.FIT_CENTER);


        //이미지 삭제 다이얼로그
        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimReplyModify.this);
                dialog_WriteDelete.show();

                dialog_WriteDelete.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_WriteDelete.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {
                            Toast.makeText(Activity_MoimReplyModify.this, "삭제", Toast.LENGTH_SHORT).show();

                            mllayout.removeView(imgview);

                        }
                    }
                });
            }
        });
        mAddImageCount = 0;
        for (int x = 0; x < mllayout.getChildCount(); x++) {
            if (mllayout.getChildAt(x) instanceof ImageView) {
                mAddImageCount++;
            }
        }

        imgview.setId(mAddImageCount);
        /*try {
            JSONObject jsonObj = new JSONObject(mAtch);
           String org_file= jsonObj.getString("org_file");

            mimgHash.put(count+"",org_file);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        mllayout.addView(imgview);

        EditText edtxt = new EditText(Activity_MoimReplyModify.this);
        edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
        //edtxt.setPadding(0,0,0,0);
        edtxt.setBackgroundColor(Color.TRANSPARENT);


        edtxt.setPadding(padding, 10, padding, 10);
        edtxt.requestFocus();
        //edtxt.setBackgroundColor(Color.RED);

        edtxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);

        mllayout.addView(edtxt);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


        scrolltoDown();

    }


    /**
     * 파일첨부 뷰추가
     *
     * @param filepath
     */
    private void addFileView(String filepath) {
        //mllayout.setPadding(50,50,50,50);

        mllayout.setOrientation(LinearLayout.VERTICAL);
        mllayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        int padding = getResources().getDimensionPixelOffset(R.dimen.dp_15);

        //TODO
        final View view = (View) getLayoutInflater().inflate(R.layout.inflate_fileupload, null);

        TextView tv = (TextView) view.findViewById(R.id.tv_filename);

        tv.setText(filepath);

        //이미지 삭제 다이얼로그
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_WriteDelete dialog_WriteDelete = new Dialog_WriteDelete(Activity_MoimReplyModify.this);
                dialog_WriteDelete.show();

                dialog_WriteDelete.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_WriteDelete.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {
                            Toast.makeText(Activity_MoimReplyModify.this, "삭제", Toast.LENGTH_SHORT).show();

                            mllayout.removeView(view);

                        }
                    }
                });
            }
        });
        mAddFileCount = 0;
        for (int x = 0; x < mllayout.getChildCount(); x++) {
            if (mllayout.getChildAt(x) instanceof View) {
                mAddFileCount++;
            }
        }


        view.setId(mAddFileCount);
        mllayout.addView(view);

        EditText edtxt = new EditText(Activity_MoimReplyModify.this);
        edtxt.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        edtxt.setTextColor(Util.getColor(this, R.color.gray_6c));
        //edtxt.setPadding(0,0,0,0);
        edtxt.setBackgroundColor(Color.TRANSPARENT);


        edtxt.setPadding(padding, 10, padding, 10);
        edtxt.requestFocus();
        //edtxt.setBackgroundColor(Color.RED);

        edtxt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

        mllayout.addView(edtxt);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        scrolltoDown();
    }

    private void scrolltoDown() {
        final ScrollView scrollview = ((ScrollView) findViewById(R.id.scroll_view));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }


    /**
     * 글쓰기 완료 버튼시
     * msg 배열 만드는부분
     */
    private String getJsonArrContent() {
        Log.e(TAG, "getWriteContent mllayout.getChildCount()" + mllayout.getChildCount());

        JSONArray jsonArray = new JSONArray();

        JSONObject jObject;
        for (int i = 0; i < mllayout.getChildCount(); i++) {
            View view = mllayout.getChildAt(i);

            jObject = new JSONObject();

            /*try {
                jObject.put("idx",i);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            if (view instanceof EditText) {
                Log.e(TAG, "getWriteContent EditText" + ((EditText) view).getText().toString());


                if (!TextUtils.isEmpty(((EditText) view).getText().toString())) {
                    try {
                        jObject.put("idx", i);
                        jObject.put("type", "0");
                        jObject.put("data", ((EditText) view).getText().toString());
                        jsonArray.put(jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } else if (view instanceof ImageView) {
                Log.e(TAG, "getWriteContent ImageView getId " + view.getId() + " " + mimgHash.get(view.getId() + ""));
                if (emoticon_num != null) {
                    try {
                        jObject.put("idx", i);
                        jObject.put("type", "4");
                        jObject.put("data", emoticon_num);
                        //jObject.put("data",mimgHash.get(view.getId()));
                        jsonArray.put(jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    try {
                        jObject.put("idx", i);
                        jObject.put("type", "1");
                        jObject.put("data", mimgHash.get(view.getId() + ""));
                        //jObject.put("data",mimgHash.get(view.getId()));
                        jsonArray.put(jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } else if (view instanceof View) {     //TODO 파일 첨부
                Log.e(TAG, "getWriteContent View");
                try {
                    jObject.put("idx", i);
                    jObject.put("type", "3");
                    jObject.put("data", mFileHash.get(view.getId() + ""));
                    jsonArray.put(jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }



        /*for (Map.Entry<String,String> entry : mimgHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e(TAG,"getWriteContent mimgHash key: "+key +" value: " +value );
        }*/


        Log.e(TAG, "getWriteContent jsonArray  " + jsonArray.toString());

        return jsonArray.toString();
    }


    //댓글수정 결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);


                String strResult;
                switch (data.ptc) {

                    /*case PacketTypes.PTC_IMS_MOIM_BOARD_WRITE:    //글쓰기
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "PTC_IMS_MOIM_BOARD_WRITE strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Util.hideKeyboard(Activity_MoimWriteModify.this, (EditText) findViewById(R.id.edt_write));

                            Toast.makeText(Activity_MoimWriteModify.this, "글쓰기 완료", Toast.LENGTH_SHORT).show();


                            if (SharedObject.getProperty_boolean(Activity_MoimWriteModify.this, Constants.MOIM_WRITE_MAIN, false)) {  //메인타임라인 > 모인선택 > 글쓰기 인경우


                                //모임선택 닫고
                                Activity_SelectMoim.activity.finish();
                                //해당 모임 타임라인으로 이동
                                //Activity_MoimTimeLineList
                                Intent intent = new Intent(Activity_MoimWriteModify.this, Activity_MoimTimeLineList.class);
                                startActivity(intent);

                                Fragment_MoimTab1_Timeline.fragment.reFreshDate(); //미리 새로고침
                                finish();

                            } else {

                                SharedObject.setProperty_boolean(Activity_MoimWriteModify.this, Constants.MOIM_WRITE_MAIN, false);

                                //모임타임라인 > 글쓰일경우
                                setResult(RESULT_OK);
                                finish();

                            }

                        } else {
                            Toast.makeText(Activity_MoimWriteModify.this, "글쓰기 실패", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        break;*/


                    case PacketTypes.PTC_IMS_MOIM_BOARD_UPDATE:    //모임글수정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "PTC_IMS_MOIM_BOARD_UPDATE strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            Util.hideKeyboard(Activity_MoimReplyModify.this, (EditText) findViewById(R.id.edt_write));

                            Toast.makeText(Activity_MoimReplyModify.this, "글수정 완료", Toast.LENGTH_SHORT).show();

                            setResult(RESULT_OK);
                            finish();

                        } else {
                            Toast.makeText(Activity_MoimReplyModify.this, "글수정 실패", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 모임 파일 업로드
     *
     * @param context
     * @param path
     */
    public void profileUpload(final Context context, final String path) {
        // TODO Auto-generated method stub

        Log.e(TAG, "profileUpload path:" + path);

        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                // TODO Auto-generated method stub
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.MOIM_IMAGE_UPLOAD); //TODO

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    ///entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, "userId", "")));
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }


            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("result").equals("success")) {

                            Log.e(TAG, "profileUpload onPostExecute " + jsonObject.toString());


                            mAtch = jsonObject.toString();


                            try {


                                JSONObject jsonObj = new JSONObject(mAtch);
                                switch (UPLOAD_TYPE) {
                                    case image:


                                        String thumb_file = jsonObj.getString("thumb_file");
                                        mimgHash.put(mAddImageCount + "", thumb_file);
                                        Log.e(TAG, "profileUpload onPostExecute mAddImageCount: " + mAddImageCount + " save_file: " + thumb_file);

                                        break;

                                    case file:
                                        String save_file = jsonObj.getString("save_file");
                                        mFileHash.put(mAddFileCount + "", save_file);

                                        break;

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            //onUploadComplete(true);

                            //mImgTy="1";
                            //mImgTmb=jsonObject.getString("thumb_file");
                            //mImgOrg=jsonObject.getString("org_file");
                        }
                    } catch (JSONException e) {
                        //onUploadComplete(false);
                    }
                } else {
                    //onUploadComplete(false);
                }
            }
        }.execute();
    }


}
