package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;

/**
 * Created by aveapp on 2017. 8. 8..
 */

public class Activity_TextSizeSet extends BaseActivity {

    private CheckBox cb_normal, cb_large, cb_verylarge;
    private RelativeLayout ll_back;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_textsizeset);

        cb_normal = (CheckBox) findViewById(R.id.cb_normal);
        cb_large = (CheckBox) findViewById(R.id.cb_large);
        cb_verylarge = (CheckBox) findViewById(R.id.cb_verylarge);
        ll_back = (RelativeLayout) findViewById(R.id.ll_back);

        if(SharedObject.getProperty_string(Activity_TextSizeSet.this, "fontstyle", "").equals("normal")){
            setcheck(cb_normal);

        }else if(SharedObject.getProperty_string(Activity_TextSizeSet.this, "fontstyle", "").equals("large")){
            setcheck(cb_large);
        }else if(SharedObject.getProperty_string(Activity_TextSizeSet.this, "fontstyle", "").equals("verylarge")){
            setcheck(cb_verylarge);
        }


        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cb_normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcheck(cb_normal);
//                setTextSize(15);
                SharedObject.setProperty_int(Activity_TextSizeSet.this, Constants.FONTSIZE, 16);
                SharedObject.setProperty_string(Activity_TextSizeSet.this, "fontstyle", "normal");
            }
        });

        cb_large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcheck(cb_large);
//                setTextSize(20);
                SharedObject.setProperty_int(Activity_TextSizeSet.this, Constants.FONTSIZE, 20);
                SharedObject.setProperty_string(Activity_TextSizeSet.this, "fontstyle", "large");
            }
        });

        cb_verylarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setcheck(cb_verylarge);
//                setTextSize(24);
                SharedObject.setProperty_int(Activity_TextSizeSet.this, Constants.FONTSIZE, 24);
                SharedObject.setProperty_string(Activity_TextSizeSet.this, "fontstyle", "verylarge");
            }
        });

    }

    private void setcheck(CheckBox cb) {
        cb_normal.setChecked(false);
        cb_large.setChecked(false);
        cb_verylarge.setChecked(false);
        cb.setChecked(true);
    }
}
