package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.NoticeRecyclerAdapter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.vo.VoBoardList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 공지사항
 *
 * 모임타임라인 - [공지사항]
 */
public class Activity_MoimNotice extends BaseActivity {

    String TAG=getClass().getSimpleName();
    NoticeRecyclerAdapter mNoticeRecyclerAdapter;
    public boolean mDeleteList=false;
    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_notice);
        initView();
        setOnClickEvent();
        setRecyclerView();

        //requestBoardRead();
    }

    /**
     * 7.22 모임 공지사항 수신
     */
    /*private void MoimNoticeRecvRequest() {
        MOALog.w(TAG+" requestMoimProfile");
        Moa.moimNoticeRecvRequest(Activity_MoimNotice.this,  mMainActivityHandler);
    }*/


    /**
     * 7.15 모임 글 리스트
     * 공지사항프로토콜이 없어서 모임글로 정보 요청
     */
    private void requestBoardList(String page) {
        //TODO
        Moa.moimBoardListRequest(Activity_MoimNotice.this,
                MessengerInfo.getUserId(this)
                ,"1", SharedObject.getProperty_string(this, Constants.MOIM_ID,null),"","",mMainActivityHandler);
    }


    /**
     * 모임 글조회 결과처리
     */
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_BOARD_LIST :   //모임리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoBoardList voBoardList;
                            Gson gson = new Gson();
                            //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                            voBoardList = gson.fromJson( data.body.toString() , VoBoardList.class);

                            setDate(voBoardList);
                            //setRecyclerView(voBoardRead);


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     *
     * @param voBoardList
     */
    private void setDate(VoBoardList voBoardList) {

        mNoticeRecyclerAdapter.setItem(voBoardList.getParams(),mDeleteList);
        mNoticeRecyclerAdapter.notifyDataSetChanged();
        mDeleteList=false;  //
    }

    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.notice));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm,null));
        //findViewById(R.id.tv_topbar_name).setVisibility(View.GONE);
        //((ImageButton)findViewById(R.id.imgb_back_btn)

        findViewById(R.id.view_top_line).setVisibility(View.GONE);

    }


    private void setRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(this, 2);
        //recyclerView.setLayoutManager(recyclerViewLayoutManager);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);



        mNoticeRecyclerAdapter = new NoticeRecyclerAdapter(this);
        recyclerView.setAdapter(mNoticeRecyclerAdapter);

        requestBoardList("0");

    }


    /**
     * 공지 새로고침
     */
    public void refreshItem() {
        requestBoardList("0");
    }
}
