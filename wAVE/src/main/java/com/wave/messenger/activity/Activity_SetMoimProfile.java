package com.wave.messenger.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.SharedObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * 이모임 프로필 설정 화면
 */
public class Activity_SetMoimProfile extends Activity{ //extends BaseActivity {

    String TAG = getClass().getSimpleName();

    String mPhotoType = "0";   //이미지 종류 0: 없음, 1:사진데이터 사용  TODO 이번페이지에서 사진 주소값 가져오기
    String mPhotoData;   //사진 파일 URL


    /*@Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_moim_profile);

        initView();
        setOnClickEvent();


        //TODO 사진 수정 버튼 숨김
        findViewById(R.id.btn_camera).setVisibility(View.GONE);
    }


    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.profileset));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, null));

        ((TextView) findViewById(R.id.tv_topbar_title)).setTextColor(Util.getColor(this, R.color.white));
        ((TextView) findViewById(R.id.tv_topbar_name)).setTextColor(Util.getColor(this, R.color.white));

        //findViewById(R.id.imgb_back_btn)
        findViewById(R.id.rl_bg).setBackgroundColor(Color.parseColor("#00000000"));
        findViewById(R.id.view_top_line).setVisibility(View.GONE);


        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        findViewById(R.id.imgb_right).setVisibility(View.GONE);


        //이름
        ((EditText) findViewById(R.id.edt_name)).setText(
                SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.UsNm, ""));

        //TODO 싱태메시지 값없음
        ((EditText) findViewById(R.id.edt_status_msg)).setText(
                SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.pfSbj, ""));


        mPhotoType = SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.pfImgTy, "");    //프로필 이미지 종류
        mPhotoData = SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.pfImg, "");      //프로필 상태메시지


        //프로필이미지
       /* Util.setGlideProfile(Activity_SetMoimProfile.this
                , com.hanawm.massenger.util.SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.MOIM_USER_PROFILE_ID, null)
                , (ImageView) findViewById(R.id.imgv_profile));*/

        //모임 프로필 썸네일
        /*Util.setMoimProfileGlide(this, SharedObject.getProperty_string(this, Constants.MOIM_ID, null)
                , com.hanawm.massenger.util.SharedObject.getProperty_string(this, Constants.pfImg,null)
                ,(ImageView) findViewById(R.id.imgv_profile));*/


        String profile_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + com.wave.messenger.util.SharedObject.getProperty_string(this, Constants.MOIM_USER_PROFILE_ID, null);  // + "&date=" + voUserProfile.getParams().getPfImg();


        Glide.with(this).load(profile_url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).placeholder(R.drawable.ic_default_profile)
                .dontAnimate().into((ImageView) findViewById(R.id.imgv_profile));

    }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog_Text dialog = new Dialog_Text(Activity_SetMoimProfile.this, getString(R.string.profile_info), getString(R.string.profile_info_title), Constants.DIALOG_BUTTON_TYPE.confirm);
                dialog.setConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });


        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        //확인
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String userName = ((EditText) findViewById(R.id.edt_name)).getText().toString().trim();

                if (TextUtils.isEmpty(userName)) {
                    Toast.makeText(Activity_SetMoimProfile.this, "이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                String statusMsg = ((EditText) findViewById(R.id.edt_status_msg)).getText().toString().trim();

                requestProfileUpdate(userName, statusMsg);


            }
        });


        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //사진등록 다이얼로그
        findViewById(R.id.btn_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dialog_Camera.getProfileEditCameraDialog(Activity_SetMoimProfile.this).show();

                //카메라 null 오류
                //Dialog_Camera.getProfileEditCameraDialog2(Activity_SetMoimProfile.this);

                //Dialog_Camera.getProfileEditCameraDialog3(Activity_SetMoimProfile.this).show();


                String fileName = System.currentTimeMillis()+".jpg";
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, fileName);
                fileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                Dialog_Camera.getProfileEditCameraDialog2_2(Activity_SetMoimProfile.this,fileUri);

                /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, Constants.CAMERA_REQUEST);*/

            }
        });
    }

    Uri fileUri;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_SetMoimProfile", "onActivityResult CAMERA_REQUEST");

            /*if(data==null){
                Log.e(TAG,"onActivityResult data==null ");
            }else{
                startCropImageActivity(data.getData());
            }*/

            if(data==null){
                Log.e(TAG,"onActivityResult data==null ");
                startCropImageActivity(fileUri);
            }else{
                Log.e(TAG,"onActivityResult data NOT null ");
                startCropImageActivity(data.getData());
            }


        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {
            Log.e("Activity_MoimOpen", "onActivityResult GALLERY_REQUEST");

            //onReceivePictureSuccess(data.getData());
            startCropImageActivity(data.getData());

        } /*else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            //크롭된 사진을 받음. (앨범, 카메라 공통)

            Uri uri = Crop.getOutput(data);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                ((ImageView)findViewById(R.id.imgv_profile)).setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //TODO 파일 업로드
            //profileUpload(this, uri.getPath(), "camera");

            profileUpload();
        }*/ else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    //TODO

                    //findViewById(R.id.imgv_profile).setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));  겉으로 적용


                    //((ImageView)findViewById(R.id.imgv_profile)).setImageDrawable(new BitmapDrawable(getResources(), bitmap));

                    //Drawable drawable = new BitmapDrawable(getResources(),bitmap);

                    //((ImageView)findViewById(R.id.imgv_profile)).setImageResource(drawable);


                    // setprofileImageview(bitmap);
                    //findViewById(R.id.imgv_type).setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));


                    //((ImageView)findViewById(R.id.imgv_profile)).setBackground(new BitmapDrawable(getResources(), bitmap)); //겉에만 적용됨

                    ((ImageView) findViewById(R.id.imgv_profile)).setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //TODO 파일 업로드
                profileUpload(this, resultUri.getPath());


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Activity_MoimOpen", "onActivityResult CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE " + resultCode);
            }

        }
    }

    /**
     * 업로드후 프로필 이미지 교체
     *
     * @param bitmap
     */
    private void setprofileImageview(Bitmap bitmap) {
        Log.e("Activity_MoimOpen", "setprofileImageview ");
        ((ImageView) findViewById(R.id.imgv_profile)).setImageBitmap(bitmap);
    }


    /**
     * 카메라 퍼미션 결과
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Activity_MoimOpen", "permission was granted");
                    // 카메라
                    Dialog_Camera.setCamera(Activity_SetMoimProfile.this);
                    //

                } else {//퍼미션 거부일때 처리
                    //카메라 퍼미션 거부 팝업
                    Dialog_Camera.showCameraPermissionDialog(Activity_SetMoimProfile.this);
                }
                return;
            }
        }
    }


    /**
     * 기본이미지
     * <p>
     * from Dialog_Camera
     */
    public void setDefaultImage() {
        //String userId = MessengerInfo.getUserId(this);
        ((ImageView) findViewById(R.id.imgv_profile)).setImageURI(null);
        ((ImageView) findViewById(R.id.imgv_profile)).setImageResource(R.drawable.ic_default_my_profile);

        mPhotoType = "0";
        //deleteUserPicture(userId);
    }


    /**
     * 7.31 모임 사용자 프로필 수정
     * 이름,상태메시지
     */
    private void requestProfileUpdate(String usNm, String pfSbj) {
        Moa.moimUserProfileUpdateRequest(Activity_SetMoimProfile.this, usNm, mPhotoData, mPhotoType, pfSbj, mMainActivityHandler);
    }

    /**
     * 프로필 수정 결과처리
     */
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case 0x00080034:   //7.31 모임 사용자 프로필 수정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {




                            //{"result":"success","thumb_file":"thumb_979c4982c2594912b1dbb5716daf8ad0.png","thumb_height":"196","thumb_width":"340","org_file":"cropped305389230.jpg","save_file":"979c4982c2594912b1dbb5716daf8ad0.png","type":"image"}

                            final String pfImg = data.body.getJson("params").getString("pfImg");
                            SharedObject.setProperty_string(Activity_SetMoimProfile.this, Constants.pfImg, pfImg);


                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Util.setMoimProfileGlide(Activity_SetMoimProfile.this, SharedObject.getProperty_string(Activity_SetMoimProfile.this, Constants.MOIM_ID, null)
                                                , pfImg
                                                ,(ImageView) findViewById(R.id.imgv_profile));

                                    } catch (Exception e) {
                                    }
                                }
                            });






                            Activity_MoimProfile.activity.moimUserProfileRequest();



                            try
                            {
                                if (Activity_MoimInfo.activity != null) {
                                    Activity_MoimInfo.activity.resetThumnail();
                                }
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }





                            if (Activity_MoimTimeLineList.activity != null) {
                                Activity_MoimTimeLineList.activity.mDeleteList=true;
                                Activity_MoimTimeLineList.activity.refreshItems();
                            }

                            if (Fragment_MoimTab1_Timeline.instance != null) {
                                Fragment_MoimTab1_Timeline.instance.refreshItems();
                            }


                            finish();
                            //화면 새로고침
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 사진촬영후 또는  겔러리 이미지 선택후 크
     *
     * @param imageUri
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAutoZoomEnabled(false)
                .setFixAspectRatio(true)
                .start(this);
    }

    /**
     * 모임 파일 업로드
     *
     * @param context
     * @param path
     */
    public void profileUpload(final Context context, final String path) {
        // TODO Auto-generated method stub

        Log.e(TAG, "profileUpload path:" + path);

        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //TODO 로딩 안보임
                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                // TODO Auto-generated method stub
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.PROFILE_IMAGE_UPLOAD_URL); //TODO

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    ///entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, "userId", "")));
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();

                    //SharedObject.getProperty_string(context, Constants.MOIM_ID,null)
                    //entity.addPart("roomid", new StringBody(MessengerInfo.getUserId(context)));
                    entity.addPart("moimid", new StringBody(SharedObject.getProperty_string(context, Constants.MOIM_ID, null)));
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }


            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("result").equals("success")) {

                            Log.e(TAG, "profileUpload onPostExecute " + jsonObject.toString());


                            mPhotoType = "1";
                            mPhotoData = jsonObject.getString("save_file");   //526c79cdeb94463188aa8ce038136db8.png" 형식 전달

                            /*mAtch=jsonObject.toString();


                            try {
                                JSONObject jsonObj = new JSONObject(mAtch);
                                String thumb_file= jsonObj.getString("thumb_file");

                                mimgHash.put(mAddImageCount+"",thumb_file);
                                Log.e(TAG,"profileUpload onPostExecute mAddImageCount: "+mAddImageCount+" save_file: "+thumb_file);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/


                            //onUploadComplete(true);

                            //mImgTy="1";
                            //mImgTmb=jsonObject.getString("thumb_file");
                            //mImgOrg=jsonObject.getString("org_file");
                        }
                    } catch (JSONException e) {
                        //onUploadComplete(false);
                    }
                } else {
                    //onUploadComplete(false);
                }
            }
        }.execute();
    }
}
