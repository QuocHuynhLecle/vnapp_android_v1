package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.DeliveryChatListAdapter;
import com.wave.messenger.adapter.FriendInviteListViewAdapter;
import com.wave.messenger.adapter.InviteHorizontaListAdapter;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.mvp.ChatList.ChatListListener;
import com.wave.messenger.mvp.ChatList.ChatListPresenter;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.mvp.Main.IInviteView;
import com.wave.messenger.mvp.Main.InvitePresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.DialogCallback;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import moa.android.api.MOAData;

/**
 * Created by user on 2016-09-18.
 */
public class Activity_Delivery extends Activity implements IInviteView, ChatListListener, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener, DialogCallback, InviteHorizontaListAdapter.OnXBtnClickedListener {


    public static Activity_Delivery instance = null;
    public static Activity_Delivery getInstance(){
        return instance;
    }

    private RelativeLayout relativeLayoutFriend, relativeLayoutChat;
    private LinearLayout ll_selectorFriend, ll_selectorChat;
    private TextView tv_friend, tv_chat;
    private ImageView imageViewFriend, imageViewChat;
    private ImageButton imageButtonBack;
    private TextView textViewInvite;
    private TextView textViewCount;
    private ImageView ivSearchIcon;
    private static EditText editTextFind;
    private ImageView ivDeleteText;
    private static boolean isSearchingChat = false;
    private static ArrayList<String> searchGroupList;
    private static ArrayList<VoFriendList> searchChildList;
    private ExpandableListView expandableListView;
    private FriendInviteListViewAdapter friendListViewAdapter;
    private ListView listViewChat;
    private DeliveryChatListAdapter adapter;
    //이미 있는 목록에 있는 친구
    private VoChatData voChatData;
    //상단 리스트
    private InviteHorizontaListAdapter inviteHorizontaListAdapter;
    private static boolean isSearching = false;
    //Horizontal recyclerview
    private RecyclerView rvFriendToAdd;
    // ListView Components
    private ArrayList<String> groupList;
    private ArrayList<VoFriendList> childList;
    private VoChatList selected;
    // presenter
    private InvitePresenter mainPresenter;
    private List<VoFriendList> selectedFriendList = new ArrayList<>();
    private LinearLayout linearLayoutFriend;
    private LinearLayout linearLayoutChat;
    private ChatListPresenter chatListPresenter;
    public static boolean ok=false;
    public VoChatData messagedata;
    private CURRENT mCurrent;
    private enum CURRENT {FRIEND, CHAT}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_delivery);
        instance=this;
        getData();
        initView();
        setEvent();
        setListView();
        mainPresenter = new InvitePresenter(this);
        this.mainPresenter.getFriendList(this);
        MessengerInfo.setMode(this, "");
        LogTrace.E("Delivery : " + MessengerInfo.getUserName(this) + " / " + MessengerInfo.getUserId(this));
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        voChatData = new VoChatData();
        voChatData.setMessageType(bundle.getString("messageType"));
        voChatData.setMessage(bundle.getString("message"));
        voChatData.setAttachment(bundle.getString("attachment"));
    }

    private void initView() {
        Toast.makeText(Activity_Delivery.this, "SSAM메신저로 수익률을 공유하시겠습니까?", Toast.LENGTH_SHORT).show();
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);
        textViewInvite = (TextView) findViewById(R.id.textViewInvite);
        textViewCount = (TextView) findViewById(R.id.textViewCount);
        relativeLayoutFriend = (RelativeLayout) findViewById(R.id.relativeLayoutFriend);
        ll_selectorFriend = (LinearLayout) findViewById(R.id.ll_selectorFriend);
//        imageViewFriend = (ImageView) findViewById(R.id.imageButtonFriend);
        tv_friend = (TextView) findViewById(R.id.tv_friend);
        relativeLayoutChat = (RelativeLayout) findViewById(R.id.relativeLayoutChat);
        ll_selectorChat = (LinearLayout) findViewById(R.id.ll_selectorChat);
//        imageViewChat = (ImageView) findViewById(R.id.imageButtonChat);
        tv_chat = (TextView) findViewById(R.id.tv_chat);
        linearLayoutFriend = (LinearLayout) findViewById(R.id.linearLayoutFriend);
        linearLayoutChat = (LinearLayout) findViewById(R.id.linearLayoutChat);
        setTabTextColor(R.color.bg_purple, R.color.gray_9f);
        findViewById(R.id.imageButtonFriend).setSelected(true);
        findViewById(R.id.imageButtonChat).setSelected(false);
        setTabVisible(View.VISIBLE, View.INVISIBLE);
        setTabSelected(true, false);
        mCurrent = CURRENT.FRIEND;
        initViewFriend();
        initViewChat();
    }

    private void initViewFriend() {
        ivSearchIcon = (ImageView) findViewById(R.id.ivSearchIcon);
        ivDeleteText = (ImageView) findViewById(R.id.ivDeleteText);
        editTextFind = (EditText) findViewById(R.id.editTextFind);

        linearLayoutChat = (LinearLayout) findViewById(R.id.linearLayoutChat);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        rvFriendToAdd = (RecyclerView) findViewById(R.id.rvFriendToAdd);
        rvFriendToAdd.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //상단 리스트뷰는 미리 정의해둠
        inviteHorizontaListAdapter = new InviteHorizontaListAdapter(this, this);
        rvFriendToAdd.setAdapter(inviteHorizontaListAdapter);
    }

    private void initViewChat() {
        listViewChat = (ListView) findViewById(R.id.listViewChat);
    }

    private void setEvent() {
        // init listener
        chatListPresenter = new ChatListPresenter(this);

        relativeLayoutFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTabTextColor(R.color.bg_purple, R.color.gray_9f);
                setTabVisible(View.VISIBLE, View.INVISIBLE);
                setTabSelected(true, false);
                mCurrent = CURRENT.FRIEND;

                linearLayoutFriend.setVisibility(View.VISIBLE);
                linearLayoutChat.setVisibility(View.GONE);
                textViewInvite.setVisibility(View.INVISIBLE);
                findViewById(R.id.imageButtonFriend).setSelected(true);
                findViewById(R.id.imageButtonChat).setSelected(false);

                List<VoChatList> list = adapter.getList();

                for (int i = 0; i < adapter.getCount(); i++) {
                    list.get(i).setSelected(false);
                }

                adapter.notifyDataSetChanged();
                selected = new VoChatList();
            }


        });

        relativeLayoutChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTabTextColor(R.color.gray_9f, R.color.bg_purple);
                setTabVisible(View.INVISIBLE, View.VISIBLE);
                setTabSelected(false, true);
                mCurrent = CURRENT.CHAT;

                linearLayoutFriend.setVisibility(View.GONE);
                linearLayoutChat.setVisibility(View.VISIBLE);
                textViewInvite.setVisibility(View.INVISIBLE);
                findViewById(R.id.imageButtonFriend).setSelected(false);
                findViewById(R.id.imageButtonChat).setSelected(true);
                selectedFriendList.clear();

                mainPresenter.getFriendList(Activity_Delivery.this);
                initViewFriend();

                textViewCount.setText("");
            }
        });

        editTextFind.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mCurrent == CURRENT.FRIEND)
                    searchFriend(s);
                else if(mCurrent == CURRENT.CHAT)
                    searchChat(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        expandableListView.setOnChildClickListener(this);
        expandableListView.setOnGroupClickListener(this);

        ivDeleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextFind.setText("");
            }
        });

        textViewInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ErrorController.showMessage("onClick");
               if(mCurrent == CURRENT.FRIEND) {
                   if (selectedFriendList.size() > 0 && mainPresenter.validateInvite(selectedFriendList)) {
                       //전부 회원인 경우
                       if(selectedFriendList.size() > 1) {
                           Dialog_Common.getDeliveryDialog(Activity_Delivery.this, selectedFriendList, voChatData).show();
                       } else {
                           List<VoFriendList> friends = new ArrayList<>();
                           VoFriendList my = new VoFriendList();
                           my.setUserId(MessengerInfo.getUserId(Activity_Delivery.this));
                           my.setUserName(MessengerInfo.getUserName(Activity_Delivery.this));
                           my.setRealUserName(MessengerInfo.getRealUserName(Activity_Delivery.this));
                           friends.add(my);

                           VoChatList chatRoomInfo = new VoChatList();

                           ChatListDbHelper listDb = LocalDB.getChatListDbHelper(Activity_Delivery.this);
                           UsersDbHelper userDb = LocalDB.getUsersDbHelper(Activity_Delivery.this);
                           String roomId;

                           if(selectedFriendList.get(0).getUserId().equals(my.getUserId())) {
                               chatRoomInfo.setRoom_type("4");
                               roomId = listDb.selectMyRoom();
                           } else {
                               friends.add(selectedFriendList.get(0));
                               chatRoomInfo.setRoom_type("0");
                               ArrayList<String> roomIdList = listDb.selectSingleRoom();
                               roomId = userDb.existUserCaht(roomIdList, selectedFriendList.get(0).getUserId());
                           }

                           List<VoChatData> chats = new ArrayList<>();

                           chatRoomInfo.setFriends(friends, Activity_Delivery.this);
                           chatRoomInfo.setChats(chats);

                           if(!"".equals(roomId)) {
                               chatRoomInfo.setRoomId(roomId);
                               sendMessageFriend(roomId, "0", chatRoomInfo.getRoom_type(), "");
                           } else {
                               ok=true;
                               chatRoomInfo.setRoomId(roomId);
                               UUID uuid = UUID.randomUUID();
                               messagedata = new VoChatData();
                               messagedata.setUserId(MessengerInfo.getUserId(Activity_Delivery.this));
                               messagedata.setRoomType(chatRoomInfo.getRoom_type());
                               messagedata.setChatId(uuid.toString());
                               messagedata.setChatType("0");
                               messagedata.setMessageType(voChatData.getMessageType());
                               messagedata.setOwnerId(MessengerInfo.getUserId(Activity_Delivery.this));
                               String message= voChatData.getMessage();

                               if (message.length() < 50) {
                                   messagedata.setTitle(message);
                               } else {
                                   messagedata.setTitle(message.substring(0, 50));
                               }

                               messagedata.setMessage(message);
                               messagedata.setAttachment(voChatData.getAttachment());
                               String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
                               messagedata.setReg_date(time);
                               messagedata.setRoomCreate("1");
                               messagedata.setInviteUser(selectedFriendList.get(0).getUserId());
                               LogTrace.E("invite : " + selectedFriendList.get(0).getUserId());
                               sendMessageFriend("", "1", chatRoomInfo.getRoom_type(), selectedFriendList.get(0).getUserId());
                           }

                           Statics.ROOMINFO = chatRoomInfo;
                           LogTrace.E("current is Friend");
                           Intent mIntent = new Intent(Activity_Delivery.this, Activity_ChatRoom.class);
                           mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                           startActivity(mIntent);
                           finish();
                       }

                   }
               } else {
                   sendMessage();
                   Statics.ROOMINFO = selected;
                   Intent mIntent = new Intent(Activity_Delivery.this, Activity_ChatRoom.class);
                   mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                   startActivity(mIntent);
                   finish();
               }
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // go back to chat room
                ErrorController.showMessage("Click Home");
                finish();
            }
        });

        listViewChat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<VoChatList> list = adapter.getList();
                textViewInvite.setVisibility(View.VISIBLE);
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (i == position) {
                        selected = list.get(i);
                        list.get(i).setSelected(true);
                    } else {

                        list.get(i).setSelected(false);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void searchFriend(CharSequence s) {
        if (s.length() != 0) {
            ivDeleteText.setVisibility(View.VISIBLE);
            isSearching = true;

            mainPresenter.searchFriendList(s + "", searchText(childList));
        } else {
            ivSearchIcon.setVisibility(View.VISIBLE);
            ivDeleteText.setVisibility(View.GONE);
            isSearching = false;
            searchGroupList = null;
            searchChildList = null;

            mainPresenter.getFriendList(Activity_Delivery.this);

            try {
                View cView = getCurrentFocus();
                if (cView != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(cView.getWindowToken(), 0);
                }
                expandableListView.requestFocus();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void searchChat(CharSequence s) {
        if (s.length() != 0) {
            ivDeleteText.setVisibility(View.VISIBLE);
//            displaySearchResults(chatListPresenter.search(s.toString(), this));
        } else {
            adapter.resetSearch();
            ivDeleteText.setVisibility(View.GONE);

            View cView = getCurrentFocus();
            if (cView != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(cView.getWindowToken(), 0);
            }

            listViewChat.requestFocus();
        }
    }

    public void displaySearchResults(List<String> roomIds) {
        adapter.showPartialSearchedList(roomIds);
        adapter.notifyDataSetChanged();
    }

    private void sendMessage() {
        UUID uuid = UUID.randomUUID();
        VoChatData messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(this));

        if("2".equals(selected.getRoom_type()) || "3".equals(selected.getRoom_type()))
            messagedata.setUserName(selected.getRoomSubject());
        else
            messagedata.setUserName(MessengerInfo.getUserName(this));

        messagedata.setRoomType(selected.getRoom_type());
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setMessageType(voChatData.getMessageType());
        messagedata.setOwnerId(MessengerInfo.getUserId(this));
        String message= voChatData.getMessage();

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        messagedata.setAttachment(voChatData.getAttachment());
        messagedata.setMessage(message);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);
        messagedata.setRoomCreate("0");
        messagedata.setRoomId(selected.getRoomId());
        ChatPresenter chatPresenter= new ChatPresenter(null);
        chatPresenter.sendMessage(this, messagedata);
    }

    private void sendMessageFriend(String roomId, String roomCreate, String roomType, String inviteUser) {
        UUID uuid = UUID.randomUUID();
        messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(this));
        messagedata.setUserName(MessengerInfo.getUserName(this));
        messagedata.setRoomType(roomType);
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setInviteUser(inviteUser);
        messagedata.setMessageType(voChatData.getMessageType());
        messagedata.setOwnerId(MessengerInfo.getUserId(this));
        String message = voChatData.getMessage();

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        messagedata.setMessage(message);
        messagedata.setAttachment(voChatData.getAttachment());
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);
        messagedata.setRoomCreate(roomCreate);

        if("1".equals(roomCreate)) {
            messagedata.setRoomId(roomId);
        } else {
            messagedata.setRoomId(roomId);
        }

        ChatPresenter chatPresenter = new ChatPresenter(null);
        chatPresenter.sendMessage(this, messagedata);
    }

    public void setTabTextColor(int friend, int chat) {
        tv_friend.setTextColor(getResources().getColor(friend));
        tv_chat.setTextColor(getResources().getColor(chat));
    }

    public void setTabVisible(int friend, int chat) {
        ll_selectorFriend.setVisibility(friend);
        ll_selectorChat.setVisibility(chat);
    }

    public void setTabSelected(boolean friend, boolean chat) {
//        imageViewFriend.setSelected(friend);
//        imageViewChat.setSelected(chat);
    }

    @Override
    public void showFriendList(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
        this.groupList = groupList;
        this.childList = friendList;
        friendListViewAdapter = new FriendInviteListViewAdapter(this, this.groupList, this.childList, mainPresenter, true);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
    }

    @Override
    public void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
        searchChildList = friendList;
        searchGroupList = groupList;
        friendListViewAdapter = new FriendInviteListViewAdapter(this, groupList, friendList, mainPresenter, true);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void receiveMsg(String roomId, VoChatData data) {

    }

    @Override
    public void sendMsg(MOAData data) {

    }

    @Override
    public void exitRoom(boolean result, MOAData data) {

    }

    @Override
    public void createRoom(MOAData data) {

    }

    @Override
    public void chatList(MOAData data) {

    }

    @Override
    public void recRead() {

    }

    @Override
    public void onChatListResult(boolean result) {

    }

    @Override
    public void onChatListUpdate(String roomId) {

    }

    @Override
    public void onChatRoomInfo(MOAData data) {

    }

    @Override
    public void onChatReadMy(MOAData data) {

    }

    @Override
    public void showSearchList(ArrayList<String> groupList, ArrayList<VoChatList> chatList) {

    }

    @Override
    public void onHideSuccess(VoFriendList hiddenFriend) {
    }

    @Override
    public void onBlockSuccess(VoFriendList blockedFriend) {
    }

    // ListView Item click.
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        VoFriendList item;

        if (!isSearching) {
            item = childList.get(groupPosition).getList().get(childPosition);
        }else{
            item = searchChildList.get(groupPosition).getList().get(childPosition);
        }

        if (item.isSelected()) {
            inviteHorizontaListAdapter.remove(item);
            selectedFriendList.remove(item);
            item.setSelected(false);
        } else {
            inviteHorizontaListAdapter.add(item);
            selectedFriendList.add(item);
            item.setSelected(true);
        }

        if (selectedFriendList.size() > 0) {
            textViewCount.setText("(" + String.valueOf(selectedFriendList.size()) + ")");
            textViewInvite.setVisibility(View.VISIBLE);
        } else {
            textViewCount.setText("");
            textViewInvite.setVisibility(View.INVISIBLE);
        }

        friendListViewAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {// from FriendListViewAdapter
        return true;
    }



    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onConfirm(Object... params) {
        // Dialog callback
        List<VoFriendList> result = (List<VoFriendList>) params[0];
        Intent intent = new Intent();
        intent.putExtra("toInvite", (Serializable) result);
        setResult(Constants.RESULT_INVITE_EVERYONE, intent);
        finish();
    }

    @Override
    public void onDecline(Object... params) {
        // Dialog callback - 비회원은 초대하지 않음.
        List<VoFriendList> data = (List<VoFriendList>) params[0];
        List<VoFriendList> result = mainPresenter.getMemberList(data);

        if (result.size() != 0) {
            // 초대할 사람 있음.
            Intent intent = new Intent();
            intent.putExtra("toInvite", (Serializable) result);
            setResult(Constants.RESULT_INVITE_ONLY_MEMBERS, intent);
            finish();
        }
    }// end of onDecline.

    //상단 리스트뷰 클릭 리스너
    @Override
    public void onXClicked(VoFriendList friend, int position) {
        inviteHorizontaListAdapter.remove(friend);
        selectedFriendList.remove(friend);
        friend.setSelected(false);

        if (selectedFriendList.size() > 0) {
            textViewCount.setText("(" + String.valueOf(selectedFriendList.size()) + ")");
            textViewInvite.setVisibility(View.VISIBLE);
        } else {
            textViewCount.setText("");
            textViewInvite.setVisibility(View.INVISIBLE);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    private ArrayList<VoFriendList> searchText(ArrayList<VoFriendList> childList) {
        for (VoFriendList item : childList) {
            if (Constants.LISTTYPE_MYFRIEND.equals(item.getUserName())) {
                return item.getList();
            }
        }
        ErrorController.showMessage("ffmain - searchText() - return null");
        return null;
    }


    public void setListView() {
        ChatListDbHelper db = LocalDB.getChatListDbHelper(this);
        adapter = new DeliveryChatListAdapter(db.getDeliveryChatList(), this);
        listViewChat.setAdapter(adapter);
    }
}
