package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.wave.messenger.adapter.BoardListRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardList;
import com.wave.messenger.vo.VoMoimProfile;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 6. 16..
 * 모임 -> 프로필 -> 작성한글보기
 * 타임라인 리스트
 *
 * BoardListRecyclerAdapter 같이사용
 */
public class Activity_WrittenTimeLineList extends BaseActivity {

    public static Activity_WrittenTimeLineList activity=null;

    String TAG=this.getClass().getName();

    private BoardListRecyclerAdapter mMoimListRecyclerAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;


    SwipeRefreshLayout mSwipeRefreshLayout;


    int mNpage=0;   //페이지번호
    boolean mDeleteList=false;  //true: 기존 리스트 삭제후 다시 불러옴 false: 기존 리스트에 다음페이지 추가


    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    //String mStrMoimid;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_written_timeline_list);
        activity=this;
        initView();

        setRecyclerView();

        requestBoardList();

    }

    /**
     * 상단 타이틀
     */
    private void initView() {
        //findViewById(R.id.rl_bg).setBackgroundColor(getResources().getColor(R.color.transparent));
        findViewById(R.id.imgb_search).setVisibility(View.GONE);
        findViewById(R.id.imgb_write).setVisibility(View.GONE);

        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.my_write));

        findViewById(R.id.ll_top_Bar).setVisibility(View.VISIBLE);

        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * RecyclerView 설정.
     */
    private void setRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        mMoimListRecyclerAdapter = new BoardListRecyclerAdapter(this, Constants.SET_ENTRY_TYPE.written_list);
        // MoimRecyclerAdapter moimRecyclerAdapter = new MoimRecyclerAdapter(this,voMoimSearch.getParams());

        //mWrapAdapter = new WrapAdapter<>(mMoimListRecyclerAdapter);
        //mWrapAdapter.adjustSpanSize(recyclerView);
        recyclerView.setAdapter(mMoimListRecyclerAdapter);

        //다음페이지 자동 로딩
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e(TAG, "onLoadMore offset: "+page+" "+totalItemsCount);

                mNpage=page-1;
                loadNextDataFromApi(mNpage);
            }
        };



        mSwipeRefreshLayout=((SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Log.e(TAG,"SwipeRefreshLayout");
                refreshItems();
            }
        });

    }



    /**
     * 페이지 로딩
     * @param offset
     */
    public void loadNextDataFromApi(int offset) {

        Log.e("TAG", "loadNextDataFromApi offset: "+offset);
        //requestBoardList(offset+"");
    }

    /**
     * 새로고침
     */
    public void refreshItems() {
        MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");
        //MoimProfileRequest();
        //requestBoardList("0");

        //onItemsLoadComplete();
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
    }



    /**
     * 하단 모임 글 리스트를 호출한다
     *  지정된 모임에서만 작성된 글리스트
     */
    private void requestBoardList() {
        Moa.moimBoardListRequest(Activity_WrittenTimeLineList.this
                , SharedObject.getProperty_string(this, Constants.MOIM_USER_PROFILE_ID,null)
                ,"4", SharedObject.getProperty_string(this, Constants.MOIM_ID,null),"", "",mMainActivityHandler);
    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);


                Util.appendLog(data.body.toString());

                String strResult;
                switch (data.ptc) {

                    //모임리스트
                   case PacketTypes.PTC_IMS_MOIM_BOARD_LIST:
                       Log.e(TAG,"result PTC_IMS_MOIM_BOARD_LIST ");

                       VoBoardList voBoardList;
                       Gson gson = new Gson();
                       //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                       voBoardList = gson.fromJson( data.body.toString() , VoBoardList.class);

                       Log.e(TAG,"voBoardList "+voBoardList.getResult()+" size: "+voBoardList.getParams().size());
                       //setRecyclerView(voBoardList);
                       setDate(voBoardList);

                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



    /**
     *
     * 모임 리스트를 제외한 화면을 세팅한다
     * @param voMoimProfile 모임 프로필
     * @param layout 헤더화면
     */
    private void setMoimProfileScreen(VoMoimProfile voMoimProfile, LinearLayout layout) {

        ((TextView)findViewById(R.id.tv_topbar_title)).setText(voMoimProfile.getParams().getMmNm());


        Log.e(TAG,"setMoimProfileScreen getImg_thumb: "+voMoimProfile.getParams().getImgTmb()+" voMoimProfile.getParam().getImg_type(): "+voMoimProfile.getParams().getImgTy());
        ((TextView)layout.findViewById(R.id.tv_moim_name)).setText(voMoimProfile.getParams().getMmNm()); //모임이름
        ((TextView)layout.findViewById(R.id.tv_user_cnt)).setText(voMoimProfile.getParams().getUsrCnt());  //멤버
        ((TextView)layout.findViewById(R.id.tv_notice_seq)).setText(voMoimProfile.getParams().getNotiCnt());  //공지카운트


        if(voMoimProfile.getParams().getImgTy().equals("1")){ //모임이미지 있음

            ViewGroup.LayoutParams params = findViewById(R.id.flexible_image).getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            findViewById(R.id.flexible_image).setLayoutParams(params);

            //상단이미지
            Glide.with(this).load(Constants.MOIM_LIST_IMAGE_URL +voMoimProfile.getParams().getImgTmb().replace("thumb_","")).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into((ImageView)findViewById(R.id.flexible_image));

        }else {

            switch (voMoimProfile.getParams().getMmTg()) {
                case "1":   //1 : 주식
                    ((ImageView)findViewById(R.id.flexible_image)).setImageResource(R.drawable.ic_stock);
                    break;

                case "2":   //2 : 국내파생
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_derivation);
                    break;

                case "3":    //3 : 해외파생
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_global_derivation);
                    break;

                case "4":   //4 : 해외주식
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_global_stock);
                    break;

                case "5":   //5 : 금융상품
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_bank_product);
                    break;

                case "6":   //6 : 기타
                    findViewById(R.id.flexible_image).setBackgroundResource(R.drawable.ic_etc);
                    break;
            }
        }


        //공지사항
        /*for (int i = 0; i <voMoimProfile.getNotiLst().size(); i++) {
            Log.e(TAG,"voMoimProfile.getNoticeList() "+ voMoimProfile.getNotiLst().get(i).getTtl());
        }*/

        int nNotiCount=Integer.parseInt(voMoimProfile.getParams().getNotiCnt());

        if(nNotiCount==0){
            layout.findViewById(R.id.ll_notice).setVisibility(View.GONE);
            layout.findViewById(R.id.ll_notice_content).setVisibility(View.GONE);
        }else if(nNotiCount==1){
            layout.findViewById(R.id.ll_notice).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.tv_notice_content1).setVisibility(View.VISIBLE);
            ((TextView)layout.findViewById(R.id.tv_notice_content1)).setText(voMoimProfile.getNotiLst().get(0).getTtl());
            layout.findViewById(R.id.tv_notice_content2).setVisibility(View.GONE);

        }else if(nNotiCount>1){
            layout.findViewById(R.id.ll_notice).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.tv_notice_content1).setVisibility(View.VISIBLE);
            ((TextView)layout.findViewById(R.id.tv_notice_content1)).setText(voMoimProfile.getNotiLst().get(0).getTtl());
            layout.findViewById(R.id.tv_notice_content2).setVisibility(View.VISIBLE);
            ((TextView)layout.findViewById(R.id.tv_notice_content2)).setText(voMoimProfile.getNotiLst().get(1).getTtl());
        }


        //회원일때와 아닐때 화면 내상태값으로 구분 0:가입안됨, 1:가입됨, 2:차단, 3:가입대기
        if(voMoimProfile.getParams().getmStt().equals("0")) { //0 이면 가입안됨

            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);       //글쓰기버튼 숨김
            findViewById(R.id.ll_join).setVisibility(View.VISIBLE);      //밴드가입하기버튼 보임
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);  //하단메뉴 숨김
            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);      //초대버튼

            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김

        }else if(voMoimProfile.getParams().getmStt().equals("1")){  //1: 가입된상태

            layout.findViewById(R.id.btn_write).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);
            findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.ll_invite).setVisibility(View.VISIBLE);
            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.VISIBLE);     //탑메뉴 글쓰기 TODO 설정 구분 필요

            //글쓰기 권한 구분
            //setWrtTy(voMoimProfile.getParams().getWrtTy());

        }else if(voMoimProfile.getParams().getmStt().equals("2")){  //가입차단

            findViewById(R.id.ll_join_applied).setVisibility(View.VISIBLE); //가입신청중
            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);
            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김

        }else if(voMoimProfile.getParams().getmStt().equals("3")){  //가입신청 승인 전
            findViewById(R.id.ll_join_applied).setVisibility(View.VISIBLE); //가입신청중
            layout.findViewById(R.id.btn_write).setVisibility(View.GONE);
            findViewById(R.id.ll_join).setVisibility(View.GONE);
            findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);

            layout.findViewById(R.id.ll_invite).setVisibility(View.GONE);
            findViewById(R.id.imgb_search).setVisibility(View.GONE);    //검색숨김
            findViewById(R.id.imgb_write).setVisibility(View.GONE);     //탑메뉴 글쓰기 숨김
        }



        //joinTest();//TODO 가입 테스트



        //setMargins(findViewById(R.id.recyclerview),0,findViewById(R.id.header).getHeight(),0,0);
    }

    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     *
     */
    private void setDate(VoBoardList voBoardList) {
        mMoimListRecyclerAdapter.setItem(voBoardList, mDeleteList);
        mMoimListRecyclerAdapter.notifyDataSetChanged();
        mDeleteList=false;  //
    }





    /**
     * 상단타이틀 호출 완료후 이벤트 처리
     * @param layout
     */
    private void setOnClickEvent(LinearLayout layout) {
        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





        //검색화면
        findViewById(R.id.imgb_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.intentActivity(Activity_WrittenTimeLineList.this, Activity_MoimTimelineSearch.class);
            }
        });

        //모임 가입하기 화면
        findViewById(R.id.ll_join).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showDialogJoin();
            }
        });



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==Constants.RESULT_MOIM_WRITE)
        {
            mDeleteList=true;
            //requestBoardList(mNpage+"");
        }
    }



    /**
     * 모임정보 화면으로 intent
     */
    private void intentSet() {
        Intent intent = new Intent(Activity_WrittenTimeLineList.this, Activity_MoimInfo.class);
        //intent.putExtra("TimeLineItem", item);
        startActivity(intent);
    }








    /**
     * 삭제처리 이후.
     * 어뎁터에서 데이터를 삭제한다
     *
     * WrapAdapter<BoardListRecyclerAdapter> -BoardListRecyclerAdapter
     *
     * @param index
     * @param size
     */
    public void RemoveAdapter(int index,int size){
        Log.e(TAG,"RemoveAdapter index: "+ index +" size: "+size);

        mMoimListRecyclerAdapter.notifyItemRemoved(index);
        mMoimListRecyclerAdapter.notifyItemRangeChanged(index, size);
        mMoimListRecyclerAdapter.notifyDataSetChanged();

    }



    /**
     * 모임 글 리스트 어댑터를 새로고침한다.
     */
    public void reFreshAdapter(){
        Log.e(TAG,"reFreshAdapter ");

        //mMoimListRecyclerAdapter.notifyItemRemoved(index);
        //mMoimListRecyclerAdapter.notifyItemRangeChanged(index, size);
        mMoimListRecyclerAdapter.notifyDataSetChanged();

    }



}
