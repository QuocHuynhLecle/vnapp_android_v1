package com.wave.messenger.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.fragment.Fragment_ChatTab;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.LocalContactData;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.DeviceInfo;
import com.wave.messenger.utility.LogTrace;

import org.json.JSONArray;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

public class Activity_CandleMain extends AppCompatActivity implements MOACommonListener, MOAEventListener {

    public static Activity_CandleMain staticMainActivity;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    String TAG="Activity_CandleMain";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candle_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


       // staticMainActivity =this;
       /* mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        setOnClickEvent();

        findViewById(R.id.imgb_top1).setSelected(true);

        initMOACleint();*/

        /*FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.fragment_main, new Fragment_Main());
        fragmentTransaction.commit();*/

        initMOACleint();
    }



    /**
     * MOACleint 임의 로그인 처리
     */
    private void initMOACleint() {

        //TODO 임시 테스트 인트로에 들어감
        //String TestID="test0001"; //리더
        //String TestID="test0002";  //가입test

        String TestID="ave000";  //가입test
        //String Password="123456";
        String Password="11223344@";
        MessengerInfo.setUserId(this, TestID);
        MessengerInfo.setUserPw(this, Password);


        MOAClient.getInstance().setAddressInfo(Constants.ADDRESS);

        MOAClient.getInstance().setProperties("userId", MessengerInfo.getUserId(this));
        MOAClient.getInstance().setProperties("userPassword", MessengerInfo.getUserPw(this));
        MOAClient.getInstance().setProperties("tel", MessengerInfo.getUserPhoneNumber(this));
        MOAClient.getInstance().setProperties("userName", MessengerInfo.getUserName(this));
        MOAClient.getInstance().setProperties("staff", MessengerInfo.getStaff(this));
        MOAClient.getInstance().setProperties("osinfo", DeviceInfo.getOsType() + "," + DeviceInfo.getOsVersion());
        MOAClient.getInstance().setProperties("deviceType", "android");
        MOAClient.getInstance().setProperties("deviceToken",MessengerInfo.getUserTocken(this));
        MOAClient.getInstance().login();

        MOAClient.getInstance().addCommonListener(this);
        MOAClient.getInstance().addEventListener(this);


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.fragment_main, new Fragment_Main());
        fragmentTransaction.commit();

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");

        //MOAClient를 해제한다.
        try {
            MOAClient.getInstance().logout();
            MOAClient.getInstance().removeCommonListener(this);
            MOAClient.getInstance().removeEventListener(this);

            //GreenDaoManager.getInstance().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /*private void setOnClickEvent() {
        Log.e(TAG,"setOnClickEvent");



        findViewById(R.id.imgb_top1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
                allBtnSelectedFalse();
                findViewById(R.id.imgb_top1).setSelected(true);

            }
        });
        findViewById(R.id.imgb_top2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);

                allBtnSelectedFalse();
                findViewById(R.id.imgb_top2).setSelected(true);
            }
        });
        findViewById(R.id.imgb_top3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);

                allBtnSelectedFalse();
                findViewById(R.id.imgb_top3).setSelected(true);
            }
        });
        findViewById(R.id.imgb_top4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(3);

                allBtnSelectedFalse();
                findViewById(R.id.imgb_top4).setSelected(true);
            }
        });
    }*/

    /**
     * 메인 상단 모든 선택 해제
     */
    private void allBtnSelectedFalse() {

        findViewById(R.id.imgb_top1).setSelected(false);
        findViewById(R.id.imgb_top2).setSelected(false);
        findViewById(R.id.imgb_top3).setSelected(false);
        findViewById(R.id.imgb_top4).setSelected(false);
    }

    private void contactData() {
        LocalDB.getPhoneDbHelper(this).updateFlagAll();

        LocalContactData.getContactList(this);

        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(this));

            if(SharedObject.getProperty_boolean(this, "firstLogin", true)) {
                body.put("first", "1");
            } else {
                body.put("first", "0");
            }

            body.put("params", LocalContactData.loadContactData(this, SharedObject.getProperty_boolean(this, "firstLogin", true)));

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_UPLOAD, body);

        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedObject.setProperty_boolean(this, "firstLogin", false);
    }

    private void deleteContact() {
        ArrayList<String> deleteList = LocalDB.getPhoneDbHelper(this).getDeleteList();

        JSONArray array = new JSONArray();

        for(String s : deleteList) {
            array.put(s);
        }

        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(this));
            body.put("params", array);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_DELETE, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFriends() {
        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(this));
            body.put("mode", "");

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_LIST, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMOALogin(MOAData moaData) {
//        LogTrace.E("onMOALogin : " + moaData);
//
//        MOAClient.getInstance().removeCommonListener(this);
//        BUSMOAData.getInstantce().setmMOAData(moaData);
//        LogTrace.E("#login success : " + moaData.body.toString());

        //contactData();
    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {

    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {

    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {

    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {
        LogTrace.E("Activity_CandleMain onMOAError: "+e);
    }

    @Override
    public void OnEvent(MOAData moaData) {
        MOALog.e("OnEvent handleMessage : "+moaData.body.toString());
        mMainActvtHandler.sendMessage(mMainActvtHandler.obtainMessage(moaData.ptc, moaData));
    }


    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    /*case PacketTypes.PTC_IMS_MOIM_LIST :    //모임리스트
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)) {
                            //디비저장
                            //MoimDbHelper db1 = CandleLocalDB.getMoimDbHelper(Activity_CandleMain.this);
                            MoimDbHelper db1 = LocalDB.getMoimDbHelper(Activity_CandleMain.this);
                            db1.addMoimList(data, Activity_CandleMain.this);

                        }


                        break;*/

                    case PacketTypes.PTC_IMS_ADDRESS_UPLOAD :
                        Toast.makeText(Activity_CandleMain.this, data.body.getString("result"), Toast.LENGTH_SHORT).show();

                        if("success".equals(data.body.getString("result"))) {
                            if(LocalDB.getPhoneDbHelper(Activity_CandleMain.this).checkDelete() > 0) {
                                deleteContact();
                            } else {
                                getFriends();
                            }
                        }

                        break;

                    case PacketTypes.PTC_IMS_ADDRESS_DELETE :
                        if("success".equals(data.body.getString("result"))) {
                            LocalDB.getPhoneDbHelper(Activity_CandleMain.this).delete();

                            getFriends();
                        }

                        break;

                    case PacketTypes.PTC_IMS_ADDRESS_LIST :
                        Toast.makeText(Activity_CandleMain.this, data.body.getString("result"), Toast.LENGTH_SHORT).show();

                        //위에서 보낸 요청의 콜백
                        MOALog.e("PTC_IMS_ADDRESS_LIST :");
                        MOALog.e("result=" + data.body.toString());

//                        setMode("2");
//
//                        if (mFragmentFriendList != null)
//                            mFragmentFriendList.onUpdateFriend();

                        if ("success".equals(data.body.getString("result"))) {
                            BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(Activity_CandleMain.this);
                            db1.addBuddys(data, Activity_CandleMain.this);
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        String TAG="SectionsPagerAdapter";
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Log.e(TAG, "getItem: "+position);

            switch (position) {

                case 0:
                   /* Fragment_MoimTab fragment_Tab = new Fragment_MoimTab();
                    return fragment_Tab;*/
                    Fragment_MoimTab1_Timeline timeline1 = new Fragment_MoimTab1_Timeline();
                    return timeline1;
                case 1:
                    Fragment_ChatTab fragment_ChatTab= new Fragment_ChatTab();
                    return fragment_ChatTab;
                case 2:
                    Fragment_MoimTab1_Timeline fragment_Test3 = new Fragment_MoimTab1_Timeline();
                    return fragment_Test3;
                case 3:
                    Fragment_MoimTab1_Timeline fragment_Test4 = new Fragment_MoimTab1_Timeline();
                    return fragment_Test4;
                default:
                    Fragment_MoimTab1_Timeline fragment_Test0= new Fragment_MoimTab1_Timeline();
                    return fragment_Test0;

            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
