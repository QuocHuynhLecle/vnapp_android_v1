package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;


/**
 * Created by aveapp on 2017. 7. 25..
 */

public class Activity_Start_Page extends BaseActivity implements View.OnClickListener {
    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_start_page);

        init();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.ll_feed_start_page) {
            setreset();
            ((ImageButton) findViewById(R.id.iv_feed_start_page)).setSelected(true);

        } else if (i == R.id.ll_finance_start_page) {
            setreset();
            ((ImageButton) findViewById(R.id.iv_finance_start_page)).setSelected(true);

        } else if (i == R.id.ll_chat_start_page) {
            setreset();
            ((ImageButton) findViewById(R.id.iv_chat_start_page)).setSelected(true);

        } else if (i == R.id.ll_cashman_start_page) {
            setreset();
            ((ImageButton) findViewById(R.id.iv_cashman_start_page)).setSelected(true);

        } else if (i == R.id.iv_back) {
            finish();

        }
    }

    private void init() {
        ((LinearLayout) findViewById(R.id.ll_feed_start_page)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.ll_finance_start_page)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.ll_chat_start_page)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.ll_cashman_start_page)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.iv_back)).setOnClickListener(this);
    }

    private void setreset() {
        ((ImageButton) findViewById(R.id.iv_feed_start_page)).setSelected(false);
        ((ImageButton) findViewById(R.id.iv_finance_start_page)).setSelected(false);
        ((ImageButton) findViewById(R.id.iv_chat_start_page)).setSelected(false);
        ((ImageButton) findViewById(R.id.iv_cashman_start_page)).setSelected(false);

    }
}
