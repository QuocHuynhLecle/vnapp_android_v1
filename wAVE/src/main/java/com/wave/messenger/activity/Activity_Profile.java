package com.wave.messenger.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ImageCrop.Crop;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.GroupDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_Camera;
import com.wave.messenger.dialog.Dialog_GroupAdd;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.mvp.Profile.DataModel.ThumbnailBitmap;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.FileUtil;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoGroupList;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;
import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

import static com.wave.messenger.Info.MessengerInfo.getUserType;

/**
 * 프로필
 * Created by aveapp on 2017-02-06.
 */

public class Activity_Profile extends BaseActivity implements MOAEventListener {
    private VoFriendList friend;
    String TAG = getClass().getSimpleName();
    private TextView tv_profile_name, tv_profile_message, textViewHidden, textViewDel, textViewFaverite, tv_phonenum, tv_risk_type;
    private ImageView tv_profile_xbtn, iv_profile_image, iv_profile_upper_icon1, iv_profile_upper_icon2, iv_profile_upper_icon3, bt_add, bt_chat, iv_bgImage;

    //업로드할 파일 경로. 편의상 필드로 저장.
    private String path, type;
    private boolean isEdit = false, isShow = false, isChange = false, isNew = false, isBgImage = false;
    private Bundle mBundle;
    private String url;
    private LinearLayout ll_change_photo, ll_change_statemessage, ll_talkProfile_Set, ll_talkProfile, ll_chat, ll_chat_me, ll_change_bg;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.fragment_profileself);

        url = Constants.CHATDAWN_PROC + "?type=2&userid=" + MessengerInfo.getUserId(this) + "&date=" + com.wave.messenger.util.SharedObject.getProperty_string(this, Constants.pfImg, "");
        init();
        mBundle = getIntent().getExtras();
        if (mBundle.isEmpty()) {
            VoFriendList myData = new VoFriendList();

            myData.setUserName(MessengerInfo.getUserName(this));
            myData.setPhone(MessengerInfo.getUserPhoneNumber(this));
            myData.setUserId(MessengerInfo.getUserId(this));
            myData.setBuddyType("0");
            myData.setProfileImage(MessengerInfo.getProfileImage(this));

            friend = myData;

            initView_Setting();
        } else {
            friend = (VoFriendList) mBundle.getSerializable("friendInfo");
            isEdit = mBundle.getBoolean("isEdit", false);
            isShow = mBundle.getBoolean("isShow", false);
            isNew = mBundle.getBoolean("isNew", false);

            initTalkView();

        }

        MOAClient.getInstance().addEventListener(this);


    }

    private void init() {
        tv_profile_name = (TextView) findViewById(R.id.tv_profile_name);
        tv_phonenum = (TextView) findViewById(R.id.tv_phonenum);
        tv_profile_message = (TextView) findViewById(R.id.tv_profile_message);
        iv_profile_image = (ImageView) findViewById(R.id.iv_profile_image);
        tv_profile_xbtn = (ImageView) findViewById(R.id.tv_profile_xbtn);
        bt_add = (ImageView) findViewById(R.id.bt_add);
        bt_chat = (ImageView) findViewById(R.id.bt_chat);
        ll_chat = (LinearLayout) findViewById(R.id.ll_chat);
        ll_change_statemessage = (LinearLayout) findViewById(R.id.ll_change_statemessage);
        ll_chat_me = (LinearLayout) findViewById(R.id.ll_chat_me);
        ll_change_photo = (LinearLayout) findViewById(R.id.ll_change_photo);
        ll_talkProfile_Set = (LinearLayout) findViewById(R.id.ll_talkProfile_Set);
        ll_talkProfile = (LinearLayout) findViewById(R.id.ll_talkProfile);
        tv_risk_type = (TextView) findViewById(R.id.tv_risk_type);
        ll_change_bg = (LinearLayout) findViewById(R.id.ll_change_bg);
        iv_bgImage = (ImageView) findViewById(R.id.iv_bgImage);
    }

    private void initTalkView() {

        ll_talkProfile.setVisibility(View.VISIBLE);
        ll_talkProfile_Set.setVisibility(View.GONE);
        tv_phonenum.setVisibility(View.VISIBLE);
        ll_change_bg.setVisibility(View.GONE);
        if (isShow) {
            if (!friend.getUserId().equals(MessengerInfo.getUserId(this))) {
//                bt_chat.setVisibility(View.GONE);
//                ll_chat.setVisibility(View.GONE);
            }
        }


        if (friend != null) {
            String userType = "";
            if ("UT_AD".equals(getUserType()) || "UT_ST".equals(getUserType()) || "UT_ST_RE".equals(getUserType()) || "UT_ST_CO".equals(getUserType()) || "UT_ST_HQ".equals(getUserType())) {
                if ("UT_ME_JU".equals(friend.getCuser_type()))
                    userType = "(준)";
            }
            tv_profile_name.setText(userType + friend.getUserName());
        }
        String friend_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + friend.getUserId();
        Glide.with(Activity_Profile.this).load(friend_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile).dontAnimate().into(iv_profile_image);

        String friend_bg_url = Constants.CHATDAWN_PROC + "?type=3&userid=bg_" + friend.getUserId();
        Glide.with(Activity_Profile.this).load(friend_bg_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.talk_profile_default_image).dontAnimate().into(iv_bgImage);

        final String phoneNum = friend.getPhone();

        tv_profile_message.setText(friend.getProfile_subject());

        if (friend.getPhone().equals("")) {
            tv_phonenum.setVisibility(View.INVISIBLE);
        } else {
            tv_phonenum.setVisibility(View.VISIBLE);
        }
        tv_phonenum.setText(friend.getPhone());
        final String phonenum = friend.getPhone().replaceAll("-", "");
        tv_phonenum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tv_phonenum.getText().toString() != null) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + phonenum));
                    startActivity(intent);
                }

            }
        });


        //친구 숨김
        ((TextView) findViewById(R.id.tv_hide)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Moa.hideList(Activity_Profile.this, phoneNum.replaceAll("-", ""), 1, mActivityHandler);
            }
        });
        if (isEdit) {
            //textViewDel.setVisibility(View.GONE);
            //textViewHidden.setVisibility(View.GONE);
            //textViewFaverite.setVisibility(View.GONE);
            //iv_profile_upper_icon1.setVisibility(View.GONE);
            //iv_profile_upper_icon2.setVisibility(View.GONE);
            //iv_profile_upper_icon3.setImageResource(R.drawable.ic_list_friend_profile);
        } else {
            //iv_profile_upper_icon3.setImageResource(R.drawable.ic_list_friend_profile);

            /*if("1".equals(friend.getBuddyHidden())) {
                //숨겨진 유저의 경우
                iv_profile_upper_icon1.setSelected(true);
            }else{
                iv_profile_upper_icon1.setSelected(false);
            }

            if("1".equals(friend.getBuddyBlocked())) {
                //블록된 유저의 경우
                iv_profile_upper_icon2.setSelected(true);
            }else{
                iv_profile_upper_icon2.setSelected(false);
            }*/

//            if("1".equals(friend.getBuddyFavorite())) {
//                //숨겨진 유저의 경우
//                iv_profile_upper_icon3.setSelected(true);
//            }else{
//                iv_profile_upper_icon3.setSelected(false);
//            }

            if (isNew) {
                //iv_profile_upper_icon3.setVisibility(View.GONE);
//                textViewFaverite.setVisibility(View.GONE);

                bt_add.setVisibility(View.VISIBLE);
            }
        }

        if ("UT_AD".equals(getUserType()) || "UT_ST".equals(getUserType()) || "UT_ST_RE".equals(getUserType()) || "UT_ST_CO".equals(getUserType()) || "UT_ST_HQ".equals(getUserType())) {
            tv_risk_type.setVisibility(View.VISIBLE);
            String risk_type = "";
            if (friend.getRisk_type() == 0)
                risk_type = "(투자권유 불가)";
            else if (friend.getRisk_type() == 1)
                risk_type = "(투자권유 가능)";
            tv_risk_type.setText(risk_type);
        }

        setEvent();
    }

    //톡프로필 설정
    private void initView_Setting() {


        ll_talkProfile_Set.setVisibility(View.VISIBLE);
        ll_talkProfile.setVisibility(View.GONE);
        tv_phonenum.setVisibility(View.INVISIBLE);


        ll_chat_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChatListDbHelper listDb = LocalDB.getChatListDbHelper(Activity_Profile.this);
                List<VoFriendList> friends = new ArrayList<>();
                VoChatList chatRoomInfo = new VoChatList();
                String roomId;

                roomId = listDb.selectMyRoom();
                chatRoomInfo.setRoom_type("4");
                friends.add(friend);

                chatRoomInfo.setFriends(friends, Activity_Profile.this);

                if (!TextUtils.isEmpty(roomId)) {
                    chatRoomInfo.setRoomId(roomId);
                }
                Statics.ROOMINFO = chatRoomInfo;

                BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

                Intent mIntent = new Intent(Activity_Profile.this, Activity_ChatRoom.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(mIntent);

                finish();
            }
        });

        ll_change_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Camera.getProfileEditCameraDialog3(Activity_Profile.this).show();
            }
        });

        ll_change_statemessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity_Profile.this, Activity_StateMessageChange.class));
            }
        });

        ll_change_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isBgImage = true;
                Dialog_Camera.getbgProfileEditCameraDialog(Activity_Profile.this).show();
            }
        });

        getProfile(SharedObject.getProperty_string(Activity_Profile.this, Constants.pfImg, ""));
        setEvent();
    }

    //결과 처리 핸들러
    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_ADDRESS_HIDDEN:    //
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());



                            /*for (int i = 0; i < voMoimSearch.getParams().size(); i++) {
                                mArVoMoimSearchItem.add(voMoimSearch.getParams().get(i));
                            }*/

                            //TODO
//                            setDate(voHiddenList.getParams());
                            //setRecyclerView(voMoimSearch);


                        } else {


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    @Override
    protected void onRestart() {
        super.onRestart();

        if (mBundle.isEmpty()) {
            getProfile(com.wave.messenger.util.SharedObject.getProperty_string(Activity_Profile.this, Constants.pfImg, ""));
            tv_profile_message.setText(com.wave.messenger.util.SharedObject.getProperty_string(Activity_Profile.this, Constants.STATE_MESSAGE, ""));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBundle.isEmpty()) {
            String userType = "";
            if ("UT_AD".equals(getUserType()) || "UT_ST".equals(getUserType()) || "UT_ST_RE".equals(getUserType()) || "UT_ST_CO".equals(getUserType()) || "UT_ST_HQ".equals(getUserType())) {
                if ("UT_ME_JU".equals(friend.getUserType()))
                    userType = "(준)";
            }
            tv_profile_name.setText(userType + MessengerInfo.getUserName(getBaseContext()));
            tv_profile_message.setText(com.wave.messenger.util.SharedObject.getProperty_string(Activity_Profile.this, Constants.STATE_MESSAGE, ""));
        }
    }

    private void setEvent() {
        tv_profile_xbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<VoGroupList> voGroupLists = new GroupDbHelper(Activity_Profile.this).getGroupList();
                Dialog_GroupAdd addGroup = new Dialog_GroupAdd(Activity_Profile.this);
                addGroup.show();
                addGroup.setRadioAdd(voGroupLists, friend.getBid(), friend.getUserId());
            }
        });

        bt_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatListDbHelper listDb = LocalDB.getChatListDbHelper(Activity_Profile.this);
                List<VoFriendList> friends = new ArrayList<>();
                VoChatList chatRoomInfo = new VoChatList();
                String roomId;

                if (isEdit) {
                    roomId = listDb.selectMyRoom();
                    chatRoomInfo.setRoom_type("4");
                    friends.add(friend);
                } else {
                    UsersDbHelper userDb = LocalDB.getUsersDbHelper(Activity_Profile.this);
                    ArrayList<String> roomIdList = listDb.selectSingleRoom();
                    roomId = userDb.existUserCaht(roomIdList, friend.getUserId());
                    chatRoomInfo.setRoom_type("0");
                    VoFriendList my = new VoFriendList();
                    my.setUserId(MessengerInfo.getUserId(Activity_Profile.this));
                    my.setUserName(MessengerInfo.getUserName(Activity_Profile.this));
                    my.setRealUserName(MessengerInfo.getRealUserName(Activity_Profile.this));
                    friends.add(my);
                    friends.add(friend);
                }

                chatRoomInfo.setFriends(friends, Activity_Profile.this);

                if (!TextUtils.isEmpty(roomId)) {
                    chatRoomInfo.setRoomId(roomId);
                }
                Statics.ROOMINFO = chatRoomInfo;

                BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

                Intent mIntent = new Intent(Activity_Profile.this, Activity_ChatRoom.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(mIntent);

                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isChange)
            setResult(Constants.RESULT_CHANGE_PROFILE);
        else
            setResult(RESULT_CANCELED);

        MOAClient.getInstance().removeEventListener(this);

        finish();
    }

    public void hideFriend(Context context, VoFriendList friendToHide, String mode) {
        /*try {
            if(!friendToHide.isMember()){
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
                db.hideBuddy(friendToHide.getUserId());
                return;
            }

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", friendToHide.getUserId());
            value.put("mode", mode);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);

            //db.hideBuddy(friendToHide.getUserId());
            if(mode.equals("1")) //숨기기
                db.hideBuddy(friendToHide.getUserId());
            else                // 숨기기취소 0
                db.unHideBuddy(friendToHide.getUserId(),null);


        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            ErrorController.showMessage("hide : " +friendToHide.getUserName());
        }*/
    }

    public void blockFriend(Context context, VoFriendList friendToBlock, String mode) {
        //Callback to : view.onBlockSuccess, onBlockFailed
        /*try {
            if(!friendToBlock.isMember()){
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
                db.blockBuddy(friendToBlock.getUserId());
                return;
            }

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", friendToBlock.getUserId());
            value.put("mode", mode);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_BLOCK, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);

            //db.blockBuddy(friendToBlock.getUserId());
            if(mode.equals("1")) //차단
                db.blockBuddy(friendToBlock.getUserId());
            else                //차단취소
                db.unblockBuddy(friendToBlock.getUserId(),null);


        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            ErrorController.showMessage("block : " +friendToBlock.getUserName());
        }*/
    }

    public void booMarkFriend(Context context, VoFriendList friendToBookMark, String mode) {
        /*try {
            if(!friendToBookMark.isMember()){
                ErrorController.showMessage("[ProfileState_Friend] bookmarkFriend : friend is not a member");
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
                db.favoriteBuddy(friendToBookMark.getUserId());
                return;
            }

            ErrorController.showMessage("[ProfileState_Friend] bookmarkFriend : friend is a member - name : " + friendToBookMark.getUserId());
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", friendToBookMark.getUserId());
            value.put("mode", mode);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_FAVORITE, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
            ErrorController.showMessage("[Framgnet_main] bookmark - id from server : " + friendToBookMark.getUserId());
            //db.favoriteBuddy(friendToBookMark.getUserId());
            if(mode.equals("1")) //즐겨찾기
                db.favoriteBuddy(friendToBookMark.getUserId());
            else                //즐겨찾기취소
                db.unfavoriteBuddy(friendToBookMark.getUserId());

        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            ErrorController.showMessage("[ProfilePresenter] add favorite : " +friendToBookMark.getUserName());
        }*/
    }

    public void setDefaultImage(String type) {
        String userId = null;
        if ("profile".equals(type)) {
            userId = MessengerInfo.getUserId(this);
            iv_profile_image.setImageURI(null);
            iv_profile_image.setImageResource(R.drawable.ic_default_profile);
        } else if ("bg".equals(type)) {
            userId = "bg_" + MessengerInfo.getUserId(this);
            iv_bgImage.setImageURI(null);
            iv_bgImage.setImageResource(R.drawable.talk_profile_default_image);
        }

        deleteUserPicture(userId);

        //Inform Bitmap changed
        ThumbnailBitmap bm = Statics.getThumbnailMap().get(userId);

        if (bm != null) {
            bm.setThumbnail(null);
            bm.setChanged(true);
        }

        ThumbnailBitmap bt3 = Statics.getMyProfilePic();

        if (bt3 != null) {
            bt3.setThumbnail(null);
            bt3.setChanged(true);
        }

        ThumbnailBitmap bm2 = Statics.getProfileImageCache().get(userId);

        if (bm2 != null) {
            bm2.setThumbnail(null);
            bm2.setChanged(true);
        }

        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", userId);
            value.put("photoType", "0");
            value.put("photoData", "");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_PHOTO, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // From Dialog_Common.getPictureDialog
    @SuppressWarnings("unchecked")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ErrorController.showMessage("onActivityResult, FragmentProfile started");

        if (requestCode == Constants.CAMERA_REQUEST && resultCode == RESULT_OK) {
            Uri uri;

            if (data != null) {
                uri = data.getData();
                LogTrace.E("data not null");
            } else {
                LogTrace.E("data null");
                uri = Uri.fromFile(FileUtil.image);
            }

            if (uri != null) {
                onReceivePictureSuccess(uri);
            } else {
                onReceivePictureSuccess(Uri.fromFile(FileUtil.image));
            }

        } else if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {
//            List<GalleryDataModel> items = (List<GalleryDataModel>) data.getSerializableExtra("item");
//
//            for (GalleryDataModel gdm : items)
//                ErrorController.showMessage("profile : " + gdm.getFullPath());
//
//            if (items != null) {
////                path = items.get(0).getFullPath();
////                profileUpload(this, path, "gallery");
////                Glide.with(Activity_Profile.this).load(path).diskCacheStrategy(DiskCacheStrategy.NONE).into(iv_profile_image);
////                startCropImageActivity();
//                onReceivePictureSuccess(data.getData());
//
//            }
            startCropImageActivity(data.getData());
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) { //갤러리 크롭
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
//                    Glide.with(Activity_Profile.this).load(resultUri.getPath()).diskCacheStrategy(DiskCacheStrategy.NONE).into(iv_profile_image);
                    //TODO
//                        addImageview(bitmap, "");
                    //findViewById(R.id.imgv_type).setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //TODO 파일 업로드
                profileUpload(this, resultUri.getPath(), "gallery");


            }
        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            //크롭된 사진을 받음. (앨범, 카메라 공통)
            ErrorController.showError("Fragment_Profile", "OnActivityResult", null);

            Uri uri = Crop.getOutput(data);

            LogTrace.E("crop complete : " + uri.getPath());

            //파일 업로드
            profileUpload(this, uri.getPath(), "camera");
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .setAutoZoomEnabled(false)
            .setInitialCropWindowPaddingRatio(0)
            .start(this);
    }

    public void onReceivePictureSuccess(Uri path) {

//        LogTrace.E("crop path : " + path.toString());

        if (!getExternalCacheDir().exists())
            getExternalCacheDir().mkdirs();

        File temp = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));

        File destFile = new File(temp, time + ".jpg");
        this.path = destFile.getAbsolutePath();
        Uri destination = Uri.fromFile(destFile);
        Crop.of(path, destination).asSquare().start(this);
    }

    public void onUploadComplete(boolean isSuccess) {
        if (isSuccess) {
            String time = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA).format(new Date(System.currentTimeMillis()));

            Toast.makeText(this, R.string.profile_image_success, Toast.LENGTH_SHORT).show();

            try {
                PacketBody body = new PacketBody();
                JSONObject value = new JSONObject();
                value.put("userId", MessengerInfo.getUserId(this));
                value.put("photoType", "1");
                value.put("photoData", time);
                body.put("params", value);
                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_PHOTO, body);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, R.string.profile_image_fail, Toast.LENGTH_SHORT).show();
            isChange = false;
        }

    }

    public void getProfile(String date) {
        final String url = Constants.CHATDAWN_PROC + "?type=2&userid=" + MessengerInfo.getUserId(this);
        final String bgurl = Constants.CHATDAWN_PROC + "?type=2&userid=bg_" + MessengerInfo.getUserId(this);

        LogTrace.E("photoUrl : " + url);

        MessengerInfo.setProfileImage(this, date);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(Activity_Profile.this).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile).dontAnimate().into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        LogTrace.E("load success");

                        iv_profile_image.setImageDrawable(drawable);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);

                        LogTrace.E("load fail");
                    }
                });
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(Activity_Profile.this).load(bgurl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.talk_profile_default_image).dontAnimate().into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable drawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        LogTrace.E("load success");

                        iv_bgImage.setImageDrawable(drawable);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);

                        LogTrace.E("load fail");
                    }
                });
            }
        });

        String userType = "";
        if ("UT_AD".equals(getUserType()) || "UT_ST".equals(getUserType()) || "UT_ST_RE".equals(getUserType()) || "UT_ST_CO".equals(getUserType()) || "UT_ST_HQ".equals(getUserType())) {
            if ("UT_ME_JU".equals(friend.getUserType()))
                userType = "(준)";
        }

        tv_profile_name.setText(userType + MessengerInfo.getUserName(getBaseContext()));
        tv_profile_message.setText(com.wave.messenger.util.SharedObject.getProperty_string(Activity_Profile.this, Constants.STATE_MESSAGE, ""));

        isChange = true;

    }

    public void profileUpload(final Context context, final String path, final String type) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, String>() {
            int maxFileSize = 0;
            int currentFileSize = 0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                LoadingManager.with(context).showLoadingDialog();
            }

            @Override
            protected String doInBackground(Void... params) {
                // TODO Auto-generated method stub
                File file;
                String result = "";
                String boundary = "---------------------------This is the boundary";

                try {
                    HttpClient httpclient = new DefaultHttpClient();

                    HttpPost httppost = new HttpPost(Constants.PROFILEUPLOAD_PROC);

                    CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            try {
                                currentFileSize = (int) num;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    if (isBgImage) {
                        entity.addPart("type", new StringBody("bg"));
                        isBgImage = false;
                    }
                    entity.addPart("userId", new StringBody(MessengerInfo.getUserIdKey()));
                    currentFileSize = 0;
                    file = new File(path);
                    maxFileSize = (int) file.length();
                    entity.addPart("file", new FileBody(file));
                    httppost.setEntity(entity);
                    httppost.setHeader("Accept-Charset", "UTF-8");
                    httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
                    HttpResponse response = httpclient.execute(httppost);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();
                    while ((sResponse = reader.readLine()) != null) {
                        s = s.append(sResponse);
                    }
                    result = s.toString();
                    ErrorController.showMessage("Result : " + result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                LoadingManager.with(context).hideLoadingDialog();

                if (result != null && result.length() > 0) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        LogTrace.E(jsonObject.getString("result"));

                        if (jsonObject.getString("result").equals("success")) {
                            onUploadComplete(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        onUploadComplete(false);
                    }
                } else {
                    onUploadComplete(false);
                }
            }
        }.execute();
    }

    public void deleteUserPicture(final String userId) {
        new AsyncTask<Void, Void, Void>() {
            private String address = "";

            @Override
            protected Void doInBackground(Void... params) {
                address = Constants.DEL_URL + userId;
                ErrorController.showMessage("[ProfileModel] deleteUserPicture : " + address);
                try {
                    URL url_obj = new URL(address);
                    HttpURLConnection con = (HttpURLConnection) url_obj.openConnection();
                    con.setRequestMethod("GET");              // default is GET
                    con.setDoInput(true);                            // default is true
                    con.setDoOutput(true);                          //default is false
                    InputStream in = con.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    String result = "";
                    while (data != -1) {
                        char current = (char) data;
                        data = isw.read();
                        result += current;
                    }
                    ErrorController.showMessage("[ProfileModel] result : " + result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                com.wave.messenger.util.SharedObject.setProperty_string(Activity_Profile.this, Constants.pfImg, "");
            }
        }.execute();

    }

    @Override
    public void OnEvent(MOAData moaData) {
        int ptc = moaData.ptc;

        if (ptc == PacketTypes.PTC_IMS_USER_CHANGE_PHOTO) {
            String date = moaData.body.getJson("params").getString("photoData");
            com.wave.messenger.util.SharedObject.setProperty_string(this, Constants.pfImg, date);
            LogTrace.E("photoData : " + date);


            getProfile(date);
        }
    }
}
