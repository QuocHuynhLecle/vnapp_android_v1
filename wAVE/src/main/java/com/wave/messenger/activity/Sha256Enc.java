package com.wave.messenger.activity;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public class Sha256Enc {
    public static String getHeshKey(String input) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(input.getBytes());
            byte byteData[] = md.digest();
            result = Base64.encodeToString(byteData, Base64.NO_WRAP);

        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block

        }

        return result;
    }
}
