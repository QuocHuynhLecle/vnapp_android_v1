package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.dialog.Dialog_Moim_introduce;
import com.wave.messenger.dialog.Dialog_TypeSetting;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimSet;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임 설정 관리 화면
 */
public class Activity_SetManage extends BaseActivity {

    String TAG = getClass().getSimpleName();
    Constants.SET_CONFIG_TYPE SET_CONFIG_TYPE;

    static Activity_SetManage activity;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fragment_Main.getInstance().FriendListRequest();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_manage);

        initView();
        setOnClickEvent();
        activity = this;
        setPermit();
    }

    /**
     * 권한에 따른 화면 숨김
     */
    private void setPermit() {
        //커버설정
        //리더가 아니면 숨기기
        //설정이 리더나 공동리더 == 내가 공동리더이면 보이기
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getCovTy(this))) {

            if (Util.getMymLv(this).equals("0")) {    //멤버에겐 숨기기
                findViewById(R.id.ll_member_gone).setVisibility(View.GONE);
            }

        } else {  //나 공동리더인데  권한 리더 일때, 멤버일때
            findViewById(R.id.ll_moim_cover_set).setVisibility(View.GONE);
            findViewById(R.id.ll_line).setVisibility(View.GONE);
        }

        //탈퇴권환 설정
        if (Util.getPermissionLv(Util.getMymLv(this), Util.getFrcTy(this))) {

            switch (Util.getMymLv(this)) {

                case "0":   //멤버
                    break;

                case "1":   // 리더
                    break;

                case "2":   // 공동리더
                    findViewById(R.id.ll_member_gone).setVisibility(View.GONE);
                    findViewById(R.id.ll_member_manage_title).setVisibility(View.VISIBLE);
                    findViewById(R.id.ll_line).setVisibility(View.GONE);
                    findViewById(R.id.ll_member_manage_1).setVisibility(View.GONE);
                    findViewById(R.id.ll_block_member).setVisibility(View.VISIBLE);
                    findViewById(R.id.ll_member_manage_2).setVisibility(View.GONE);
                    break;
            }

        } else {  //나 공동리더인데  권한 리더 일때
            findViewById(R.id.ll_member_gone).setVisibility(View.GONE);
            findViewById(R.id.ll_member_manage_title).setVisibility(View.GONE);
            findViewById(R.id.ll_member_manage_1).setVisibility(View.GONE);
            findViewById(R.id.ll_block_member).setVisibility(View.GONE);
            findViewById(R.id.ll_member_manage_2).setVisibility(View.GONE);
        }
    }


    public void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moin_set_manage));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));
        ((TextView) findViewById(R.id.tv_topbar_name)).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        findViewById(R.id.view_top_line).setVisibility(View.GONE);
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.tv_moin_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));

        //프로필 "나"의 프로필이 아니라 모임프로필
        if (SharedObject.getProperty_string(this, Constants.imgTy, "0").equals("0")) {
            ((ImageView) findViewById(R.id.imgv_profile)).setImageResource(R.drawable.meet_timeline_default_img);
        } else {
            Glide.with(this).load(Constants.MOIM_LIST_IMAGE_URL + SharedObject.getProperty_string(this, Constants.imgTmb, "")).
                    diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.meet_timeline_default_img)
                    .dontAnimate().into((ImageView) findViewById(R.id.imgv_profile));
        }


        //모임공개설정
        setPublicTxt(SharedObject.getProperty_string(this, Constants.opnTy, ""));

        //최대멤버수
        setMaxNum(Integer.parseInt(SharedObject.getProperty_string(this, Constants.maxUsr, "")));

        //가입방법
        setJoinMathod(SharedObject.getProperty_string(this, Constants.entTy, ""));

        checkIntroText();

        //가입제한 체크
        setEntTy(SharedObject.getProperty_string(this, Constants.entTy, ""));

        //종목상담여부 초기화
        setConTy(SharedObject.getProperty_string(this, Constants.csltTy, ""));
        //종목리딩여부
        setReTy(SharedObject.getProperty_string(this, Constants.ldngTy, ""));


        //전문가이면 종목상담, 종목리딩. 보이
        if (SharedObject.getProperty_string(this, Constants.xprt, "").equals("1")) {
            findViewById(R.id.ll_consulting).setVisibility(View.VISIBLE);
            findViewById(R.id.view_consulting).setVisibility(View.VISIBLE);
            findViewById(R.id.ll_reading).setVisibility(View.VISIBLE);
            findViewById(R.id.view_reading).setVisibility(View.VISIBLE);
        }
    }

    /**
     * 가입제한 표시
     * 0:리더승인, 1:비밀번호 입력, 2:자동가입
     */
    private void setEntTy(String entTy) {

        switch (entTy) {
            case "0":
//                ((TextView) findViewById(R.id.tv_entTy)).setText(getResources().getString(R.string.signup_info));
//                ((ToggleButton) findViewById(R.id.toggle_entTy)).setChecked(false);
                break;

            case "1":
//                ((TextView) findViewById(R.id.tv_entTy)).setText(getResources().getString(R.string.signup_info2));
//                ((ToggleButton) findViewById(R.id.toggle_entTy)).setChecked(true);
                break;
        }
    }

    /**
     * 종목상담 표시
     * 0:OFF, 1:ON
     */
    private void setConTy(String conTy) {

        switch (conTy) {
            case "0":
                ((TextView) findViewById(R.id.tv_consulting)).setText("OFF");
                ((ToggleButton) findViewById(R.id.toggle_consulting)).setChecked(false);
                break;
            case "1":
                ((TextView) findViewById(R.id.tv_consulting)).setText("ON");
                ((ToggleButton) findViewById(R.id.toggle_consulting)).setChecked(true);
                break;
        }
    }

    /**
     * 종목리딩 표시
     * 0:OFF, 1:ON
     */
    private void setReTy(String reTy) {

        switch (reTy) {
            case "0":
                ((TextView) findViewById(R.id.tv_reading)).setText("OFF");
                ((ToggleButton) findViewById(R.id.toggle_reading)).setChecked(false);
                break;
            case "1":
                ((TextView) findViewById(R.id.tv_reading)).setText("ON");
                ((ToggleButton) findViewById(R.id.toggle_reading)).setChecked(true);
                break;
        }
    }

    /**
     * 모임설명글이 있는지 체크
     */
    private void checkIntroText() {

        Log.e(TAG, "checkIntroText: " + SharedObject.getProperty_string(this, Constants.intr, ""));

        if (TextUtils.isEmpty(SharedObject.getProperty_string(this, Constants.intr, ""))) {
            ((TextView) findViewById(R.id.tv_introduce_set)).setText(getResources().getString(R.string.not_set));
        } else {
            ((TextView) findViewById(R.id.tv_introduce_set)).setText(getResources().getString(R.string.set_complete));
        }
    }

    private void selectCheckBtn(boolean b1, boolean b2, boolean b3) {
        findViewById(R.id.btn_leader).setSelected(b1);
        findViewById(R.id.btn_password).setSelected(b2);
        findViewById(R.id.btn_auto).setSelected(b3);
    }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //커버설정화면
        findViewById(R.id.ll_moim_cover_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_MoimCoverSet.class);
                intent.putExtra(Constants.SET_COVER, Constants.MODIFY_COVER);
                startActivity(intent);
            }
        });

        //모임공개설정
        findViewById(R.id.ll_set_moim_public).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settTypeDialog();
            }
        });

        //모임 소개말
        findViewById(R.id.ll_moim_introduce).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialogMoimIntroduct();
            }
        });

        //멤버수 설정
        findViewById(R.id.ll_max_member).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_SetMaxMember.class);
                startActivityForResult(intent, Constants.MAX_MEMBER_REQUEST);
            }
        });

        //가입신청받기
        ((ToggleButton) findViewById(R.id.toggle_entTy)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                VoMoimSet voMoimSet = new VoMoimSet();
                SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.entTy;
                if (isChecked) {
                    voMoimSet.setEntTy("1");

                } else {
                    voMoimSet.setEntTy("0");
                }
                MoimConfigRequest(voMoimSet);
            }
        });

        //종목상담
        ((ToggleButton) findViewById(R.id.toggle_consulting)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VoMoimSet voMoimSet = new VoMoimSet();
                SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.csltTy;
                if (isChecked) {
                    voMoimSet.setCsltTy("1");

                } else {
                    //setConTy("0");
                    voMoimSet.setCsltTy("0");
                }
                MoimConfigRequest(voMoimSet);

            }
        });

        //종목리딩
        ((ToggleButton) findViewById(R.id.toggle_reading)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VoMoimSet voMoimSet = new VoMoimSet();
                SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.ldngTy;
                if (isChecked) {
                    voMoimSet.setLdngTy("1");

                } else {
                    //setConTy("0");
                    voMoimSet.setLdngTy("0");
                }
                MoimConfigRequest(voMoimSet);

            }
        });


        //멤버들의 권한 설정
        findViewById(R.id.ll_set_member_permission).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_SetPermission.class);
                startActivity(intent);
            }
        });

        //공동리더 관리
        findViewById(R.id.ll_leader_manage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_SetManage.this, Activity_SetLeaderManage.class);
                startActivity(mIntent);
            }
        });

        //멤버탈퇴 차단목록
        findViewById(R.id.ll_block_member).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(Activity_SetManage.this, Activity_SetBlockMember.class);
                startActivity(mIntent);
            }
        });

        //리더위임
        findViewById(R.id.ll_mandate_leader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_SetAddLeader.class);
                intent.putExtra(Constants.INTENT_TYPE, Constants.MANDATE_LEADER);
                startActivity(intent);
            }
        });

        //채팅 설정 및 메뉴 우선순위 결정
        findViewById(R.id.ll_set_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_SetChat.class);
                startActivity(intent);
            }
        });


        //가입신청받기-리더승인
        findViewById(R.id.ll_leader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("1");
            }
        });

        //가입신청받기-리더승인
        findViewById(R.id.btn_leader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("1");
            }
        });

        //가입신청받기-비밀번호
        findViewById(R.id.ll_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("2");
            }
        });

        findViewById(R.id.btn_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("2");
            }
        });


        //가입신청받기-자동가입
        findViewById(R.id.ll_auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("3");
            }
        });

        findViewById(R.id.btn_auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setJoinType("3");
            }
        });

        //파일 보관설정 [삭제]
        /*findViewById(R.id.ll_keep_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_SetManage.this, Activity_SetKeepFile.class);
                startActivity(intent);

            }
        });*/

        /*
        findViewById(R.id.ll_stats).setOnClickListener(new View.OnClickListener() {   //모임통계 웹뷰로 제작 예정
            @Override
            public void onClick(View v) {
                //Intent mIntent = new Intent(Activity_MoimInfo.this, Activity_MoimFindBySubject.class);
                //startActivity(mIntent);
            }
        });*/
    }

    /**
     * 가입신청 받기
     * 1 리더승인
     * 2 비밀번호입력
     * 3 자동가입
     *
     */
    private void setJoinType(String type) {
        switch (type){
            case "1":
                final Dialog_MoimJoinType dialog_moimJoinType = new Dialog_MoimJoinType(Activity_SetManage.this, "0");
                dialog_moimJoinType.show();
                dialog_moimJoinType.setCancleListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_moimJoinType.dismiss();
                    }
                });
                dialog_moimJoinType.setConfirmistener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCheckBtn(true, false, false);
                        Log.i("TEST", "content : " + dialog_moimJoinType.getJoinTxt());
                        VoMoimSet voMoimSet = new VoMoimSet();
                        voMoimSet.setEntTy("0");
                        voMoimSet.setQstn(dialog_moimJoinType.getJoinTxt());
                        SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.entTy;
                        MoimConfigRequest(voMoimSet);
                        dialog_moimJoinType.dismiss();
                    }
                });
                break;

            case "2":
                final Dialog_MoimJoinType dialog_moimJoinType2 = new Dialog_MoimJoinType(Activity_SetManage.this, "1");
                dialog_moimJoinType2.show();
                dialog_moimJoinType2.setCancleListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_moimJoinType2.dismiss();
                    }
                });
                dialog_moimJoinType2.setConfirmistener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectCheckBtn(false, true, false);
                        VoMoimSet voMoimSet = new VoMoimSet();
                        voMoimSet.setEntTy("2");
                        voMoimSet.setMmPw(dialog_moimJoinType2.getJoinTxt());
                        SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.entTy;
                        MoimConfigRequest(voMoimSet);
                        dialog_moimJoinType2.dismiss();
                    }
                });
                break;

            case "3":
                selectCheckBtn(false, false, true);
                VoMoimSet voMoimSet = new VoMoimSet();
                voMoimSet.setEntTy("1");
                SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.entTy;
                MoimConfigRequest(voMoimSet);
                break;
        }
    }


    //최대인원 선택후
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.MAX_MEMBER_REQUEST && resultCode == RESULT_OK) {
            int nResult = data.getIntExtra("result", -1);
            Log.e(TAG, "nResult " + nResult);
            setMaxNum(nResult);

            VoMoimSet voMoimSet = new VoMoimSet();
            voMoimSet.setMaxUsr(nResult + "");
            SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.maxUsr;

            MoimConfigRequest(voMoimSet);
        }
    }


    /**
     * 최대인원설정
     *
     * @param nResult
     */
    private void setMaxNum(int nResult) {
        if (nResult == Constants.MAX_MEMBER_50) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit1));
        } else if (nResult == Constants.MAX_MEMBER_100) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit2));
        } else if (nResult == Constants.MAX_MEMBER_500) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit3));
        } else if (nResult == Constants.MAX_MEMBER_1000) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit4));
        } else if (nResult == Constants.MAX_MEMBER_3000) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit5));
        } else if (nResult == Constants.MAX_MEMBER_5000) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit6));
        } else if (nResult == Constants.MAX_MEMBER_10000) {
            ((TextView) findViewById(R.id.tv_max_member)).setText(getResources().getString(R.string.member_limit7));
        }
    }

    /**
     * 가입방법 설정 리더,패스워드,자동
     * @param nResult
     */
    private void setJoinMathod(String nResult) {
        if (nResult.equals("0")) {
            selectCheckBtn(true, false, false);
        } else if (nResult.equals("2")) {
            selectCheckBtn(false, true, false);
        } else if (nResult.equals("1")) {
            selectCheckBtn(false, false, true);

        }
    }

    /**
     * 모임 소개말 설정
     */
    private void setDialogMoimIntroduct() {
        //변경 팝업
        final Dialog_Moim_introduce mDialog = new Dialog_Moim_introduce(Activity_SetManage.this, SharedObject.getProperty_string(this, Constants.intr, ""));

        mDialog.show();
        mDialog.setConfirmistener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //입력체크 미설정> 설정됨

                if (mDialog.isEmpty()) {

                    ((TextView) findViewById(R.id.tv_introduce_set)).setText(getResources().getString(R.string.not_set));
                    VoMoimSet voMoimSet = new VoMoimSet();
                    voMoimSet.setIntr(" ");
                    SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.intr;
                    MoimConfigRequest(voMoimSet);

                    Activity_MoimTimeLineList.activity.setMoimIntrChange("");

                } else {

                    VoMoimSet voMoimSet = new VoMoimSet();
                    voMoimSet.setIntr(mDialog.getIntroTxt());
                    SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.intr;
                    MoimConfigRequest(voMoimSet);

                    ((TextView) findViewById(R.id.tv_introduce_set)).setText(getResources().getString(R.string.set_complete));

                    Activity_MoimTimeLineList.activity.setMoimIntrChange(mDialog.getIntroTxt());
                }
                mDialog.dismiss();
            }
        });
    }


    /**
     * 모임 공개 설정 다이얼로그
     * <p>
     * 공개설정 제목만공개 삭제됨
     */
    private void settTypeDialog() {

        //변경 팝업
        final Dialog_TypeSetting mDialog = new Dialog_TypeSetting(Activity_SetManage.this, Constants.SET_PUBLIC_DIALOG_TYPE.set_public_manage,
                SharedObject.getProperty_string(this, Constants.opnTy, ""),   //공개설정
                SharedObject.getProperty_string(this, Constants.entTy, "")    //가입설정
        );

        mDialog.show();
        mDialog.setCancleListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setConfirmistener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Log.e(TAG, " mDialog MoimSetRequest getSelect: " + mDialog.getSelect() + " getToggle " + mDialog.getToggle() + "");
                VoMoimSet voMoimSet = new VoMoimSet();
                voMoimSet.setOpnTy(mDialog.getSelect() + "");
                voMoimSet.setEntTy(mDialog.getToggle() + "");
                SET_CONFIG_TYPE = Constants.SET_CONFIG_TYPE.opnTy;
                MoimConfigRequest(voMoimSet);

                setPublicTxt(mDialog.getSelect() + "");

                mDialog.dismiss();
            }
        });
    }

    /**
     * 모임 공개설정 텍스트
     */
    private void setPublicTxt(String s) {
        switch (s) {
            case "0":
                ((TextView) findViewById(R.id.tv_set_public)).setText(getResources().getString(R.string.private_moim));
                break;
            case "1":
                ((TextView) findViewById(R.id.tv_set_public)).setText(getResources().getString(R.string.public_moim));
                break;
        }
    }


    /**
     * 모임설정
     * 공개 제한,가입 제한
     */
    private void MoimConfigRequest(VoMoimSet voMoimSet) {

        Moa.moimConfig(this, voMoimSet, mMainActivityHandler);
    }


    // 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());

                String strResult;
                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_CONFIG:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + " SET_CONFIG_TYPE: " + SET_CONFIG_TYPE);
                        if (strResult.equals(Constants.SUCCESS)) {

                            if (SET_CONFIG_TYPE == Constants.SET_CONFIG_TYPE.opnTy) {   //공개타입설정, 가입제한
                                String opnTy = data.body.getJson("params").getString("opnTy");
                                String entTy = data.body.getJson("params").getString("entTy");

                                SharedObject.setProperty_string(Activity_SetManage.this, Constants.opnTy, opnTy);
                                SharedObject.setProperty_string(Activity_SetManage.this, Constants.entTy, entTy);

                                setPublicTxt(opnTy);

                            } else if (SET_CONFIG_TYPE == Constants.SET_CONFIG_TYPE.maxUsr) {    //최대인원

                                String maxUsr = data.body.getJson("params").getString("maxUsr");
                                SharedObject.setProperty_string(Activity_SetManage.this, Constants.opnTy, maxUsr);

                            } else if (SET_CONFIG_TYPE == Constants.SET_CONFIG_TYPE.intr) {      //모임소개

                                String intr = data.body.getJson("params").getString("intr");
                                SharedObject.setProperty_string(Activity_SetManage.this, Constants.intr, intr);

                            } else if (SET_CONFIG_TYPE == Constants.SET_CONFIG_TYPE.entTy) {     //가입신청받기
                                String entTy = data.body.getJson("params").getString("entTy");
                                SharedObject.setProperty_string(Activity_SetManage.this, Constants.entTy, entTy);

                                //setEntTy(entTy);
                            }
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

}
