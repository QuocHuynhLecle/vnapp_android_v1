package com.wave.messenger.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * 파일보관설정 화면 [삭제]
 * @deprecated
 */
public class Activity_SetKeepFile extends AppCompatActivity {

    private static final String TAG = "Activity_SetKeepFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_keep_file);

        initView();
        setOnClickEvent();
    }

    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.set_keep_file));
        ((TextView) findViewById(R.id.tv_topbar_name)).setText("주식하는 사람들");
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);
    }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.imgb_5m_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgb_5m_1).setSelected(true);
                findViewById(R.id.imgb_5m_2).setSelected(false);
            }
        });
        findViewById(R.id.imgb_5m_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgb_5m_1).setSelected(false);
                findViewById(R.id.imgb_5m_2).setSelected(true);


            }
        });
        findViewById(R.id.imgb_10m_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgb_10m_1).setSelected(true);
                findViewById(R.id.imgb_10m_2).setSelected(false);

            }
        });
        findViewById(R.id.imgb_10m_2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.imgb_10m_1).setSelected(false);
                findViewById(R.id.imgb_10m_2).setSelected(true);

            }
        });




    }

    /**
     * 대화내용 서버보관기간 다이얼로그
     */
    /*private void showMessageKeepPeriodDialog() {
        final Dialog_MessageKeepPeriod dialog = new Dialog_MessageKeepPeriod(Activity_SetKeepFile.this);
        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case 0:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.no_keep));
                        break;
                    case 1:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.day30));
                        break;
                    case 2:
                        ((TextView)findViewById(R.id.tv_keep_server)).setText(getString(R.string.one_year));
                        break;
                }

                //Toast.makeText(getApplicationContext(),""+v.getId(), Toast.LENGTH_SHORT).show();
                //TODO 서버통신 완료후처리

                dialog.dismiss();
            }
        });
        dialog.show();
    }*/



}
