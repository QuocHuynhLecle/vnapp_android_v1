package com.wave.messenger.activity;

import android.support.v4.app.FragmentActivity;

/**
 * Created by aveapp on 2017. 8. 20..
 */

public class ActivityHelper {
    static boolean isTop = false;
    static FragmentActivity topActivity = null;
    public static boolean isTop() {
        boolean result = false;
        try {
            if(isTop && topActivity != null) {
                result = topActivity.getWindow().getDecorView().getRootView().isShown();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return result;
        //return isTop;
    }
    public static FragmentActivity getTopActivity() {
        return topActivity;
    }
    public static void setTopActivity(FragmentActivity topActivity) {
        ActivityHelper.topActivity = topActivity;
    }
    public static void setTop(boolean isTop) {
        ActivityHelper.isTop = isTop;
    }
}
