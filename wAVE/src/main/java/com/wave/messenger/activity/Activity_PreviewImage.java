package com.wave.messenger.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.helper.UITopbarHelper;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.widget.CustomViewPager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

public class Activity_PreviewImage extends BaseActivity {

    private ImageView ivPicture;
    private UITopbarHelper llTopBar;
    private AlbumPicture picture;
    private ImageView ivDown, ivDel;
    private CustomViewPager vpImageList;
    private List<AlbumPicture> pictureList;
    private LinearLayout linearLayoutLoading;
    private boolean hana = false;

    private List<PhotoViewAttacher> mAttacherList = new ArrayList<>();

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_previewimage);
        initView();
        setEvent();
        getData();
    }

    private void getData() {

        //모임에서 올경우 휴지통레이어 숨김
        if (getIntent().hasExtra("type")) {
            String type=getIntent().getStringExtra("type");

            if(type.equals("moim")){
                findViewById(R.id.llAction).setVisibility(View.GONE);
                findViewById(R.id.llAction_moim).setVisibility(View.VISIBLE);

            }
            Log.e("Activity_PreviewImage","getData type:"+type);
        }


        if (getIntent().hasExtra("picture")) {
            picture = (AlbumPicture) getIntent().getSerializableExtra("picture");
        }

        if (getIntent().hasExtra("pictureList")) {
            pictureList = (List<AlbumPicture>) getIntent().getSerializableExtra("pictureList");
            VPAdapter adapter = new VPAdapter(pictureList, getLayoutInflater());
            vpImageList.setAdapter(adapter);
            vpImageList.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    redrawViewPager();
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            int index = 0;

            for (AlbumPicture pic : pictureList) {
                if (pic.getUrlPath().equals(picture.getUrlPath())) {
                    index = pictureList.indexOf(pic);
                    break;
                }
            }
            ErrorController.showMessage("[Activity_PreviewImage] Current index : " + index);
            vpImageList.setCurrentItem(index);
        } else {
            ErrorController.showMessage("[Activity_PreviewImage] no pictureList");

        }


    }

    private void setEvent() {

        llTopBar.setHomeBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ivDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumPicture pic = pictureList.get(vpImageList.getCurrentItem());
                ChatDataDbHelper db = new ChatDataDbHelper(Activity_PreviewImage.this);
                db.delChatData(pic.getChatId());
                Intent mIntent = new Intent();
                mIntent.putExtra("delItem", pic.getChatId());
                setResult(Constants.RESULT_CHAT_ALBUM, mIntent);
//                Fragment_ChatRoom.getInstance().removeChat(pic.getChatId());
                if (Activity_ChatAlbum.getInstance() != null) {
                    Activity_ChatAlbum.getInstance().getData();
                }
                finish();
            }
        });

        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picture != null) {
                    AlbumPicture item = pictureList.get(vpImageList.getCurrentItem());

                    if (item != null && item.isExistImage()) {
                        new ImageDownloader().execute(pictureList.get(vpImageList.getCurrentItem()).getUrlPath());
                    } else {
                        Toast.makeText(Activity_PreviewImage.this, R.string.image_not_exist_caption, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //모임에서 올경우 다운로드 이벤트.
        findViewById(R.id.ivDown2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picture != null) {
                    AlbumPicture item = pictureList.get(vpImageList.getCurrentItem());

                    if (item != null && item.isExistImage()) {
                        new ImageDownloader().execute(pictureList.get(vpImageList.getCurrentItem()).getUrlPath());
                    } else {
                        Toast.makeText(Activity_PreviewImage.this, R.string.image_not_exist_caption, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void initView() {
        vpImageList = (CustomViewPager) findViewById(R.id.vpImageList);
        ivPicture = (ImageView) findViewById(R.id.ivPicture);
        llTopBar = new UITopbarHelper((LinearLayout) findViewById(R.id.llTopBar));
        llTopBar.setText("자세히 보기");
        llTopBar.setHomeVisibility(View.VISIBLE);
        llTopBar.setMenuIconVisibility(View.GONE);
//        llTopBar.setSearchVisibility(View.GONE);

        ivDel = (ImageView) findViewById(R.id.ivDel);
        ivDown = (ImageView) findViewById(R.id.ivDown);
        linearLayoutLoading = (LinearLayout) findViewById(R.id.linearLayoutLoading);
    }

    private void redrawViewPager() {
        try {
            for (PhotoViewAttacher attacher : mAttacherList) {
                attacher.update();
            }
        } catch (Exception ignored) {
        }
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {


        public ImageDownloader() {

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = params[0];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");

                File file = null;
                try {
                    Date now = new Date();
                    android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

                    File path = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES);
                    file = new File(path, now + ".jpg");

                    FileOutputStream out = new FileOutputStream(file);

                    image.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(Uri.fromFile(file));
                    sendBroadcast(mediaScanIntent);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[Activity_ChatAlbum] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                Toast.makeText(Activity_PreviewImage.this, "이미지가 저장되었습니다.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Activity_PreviewImage.this, "이미지 저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private class VPAdapter extends PagerAdapter {

        private List<AlbumPicture> picList;
        private LayoutInflater inflater;

        public VPAdapter(List<AlbumPicture> pictureList, LayoutInflater layoutInflater) {
            super();
            this.picList = pictureList;
            this.inflater = layoutInflater;
            ErrorController.showMessage("[Activity_PreviewImage] picList size : " + picList.size());
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = inflater.inflate(R.layout.cell_imagepreview2, null);
            final TextView captionText = (TextView) view.findViewById(R.id.txt_caption);
            final ImageView ivImagePreview = (ImageView) view.findViewById(R.id.ivImagePreview);

            final AlbumPicture item = picList.get(position);

            try {
                if ("1".equals(item.getType())) {
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inSampleSize = 2;
                    Bitmap bm = BitmapFactory.decodeFile(item.getUrlPath(), opts);
                    if (bm.getWidth() > bm.getHeight()) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    }
                    ivImagePreview.setImageBitmap(bm);
                } else {
                    Glide.with(Activity_PreviewImage.this).load(item.getUrlPath()).asBitmap().format(DecodeFormat.PREFER_ARGB_8888).asIs().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                            if (bitmap != null) {
                                ivImagePreview.setImageBitmap(bitmap);
                                item.setExistImage(true);

                                PhotoViewAttacher attacher = new PhotoViewAttacher(ivImagePreview);
                                attacher.update();
                                mAttacherList.add(attacher);
                            } else {
                                ivImagePreview.setImageResource(R.drawable.img_picture_default_android);
                                captionText.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            ivImagePreview.setImageResource(R.drawable.img_picture_default_android);
                            captionText.setVisibility(View.VISIBLE);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return picList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}
