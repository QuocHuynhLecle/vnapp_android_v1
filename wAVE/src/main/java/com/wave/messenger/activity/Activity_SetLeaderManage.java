package com.wave.messenger.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.LeaderManageRecyclerViewAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;
import java.util.Comparator;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 모임설정관리 - 공동리더관리 화면
 */
public class Activity_SetLeaderManage extends BaseActivity {

    String TAG = getClass().getSimpleName();

    static public Activity_SetLeaderManage activity;
    LeaderManageRecyclerViewAdapter mLeaderManageRecyclerViewAdapter;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_leader_manage);

        activity =this;
        initView();
        setOnClickEvent();
        setRecyclerView();

    }


    /**
     * 상단 타이틀설정
     */
    private void initView() {
        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.leader_manage));
        findViewById(R.id.tv_topbar_name).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
    }


    private void setOnClickEvent() {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ((TextView)findViewById(R.id.tv_member_permission)).setPaintFlags(((TextView)findViewById(R.id.tv_member_permission)).getPaintFlags()|Paint.UNDERLINE_TEXT_FLAG);
        findViewById(R.id.tv_member_permission).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentPermission();
            }
        });


        findViewById(R.id.btn_add_leader).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentAddLeader();
            }
        });

        //Activity_AddLeader

    }



    private void setRecyclerView() {
        RecyclerView mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Activity_SetLeaderManage.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mLeaderManageRecyclerViewAdapter = new LeaderManageRecyclerViewAdapter(Activity_SetLeaderManage.this);
        mRecyclerView.setAdapter(mLeaderManageRecyclerViewAdapter);


        MoimMemberListRequest();
    }

    /**
     * 7.9 모임 멤버 리스트
     *
     */
    private void MoimMemberListRequest() {
        MOALog.w(TAG + " requestMoimProfile");
        Moa.moimMemberlistRequest(this, "1", "1","0",mMainActivityHandler);
    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_MEMBERLIST:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());


                            try {   //gson파싱
                                VoMoimMemberList voMoimMemberList;
                                Gson gson = new Gson();
                                //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                                voMoimMemberList = gson.fromJson(data.body.toString(), VoMoimMemberList.class);
                                setDate(voMoimMemberList.getParams());


                                ((TextView)findViewById(R.id.tv_leader_count)).setText("("+ mLeaderManageRecyclerViewAdapter.getItemCount()+")");
                                //arMemberList=new ArrayList<>(voMoimMemberList.getParams()); //검색을 위한 원본 멤버리스트

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    private void setDate(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        mLeaderManageRecyclerViewAdapter.setItem(params);
        mLeaderManageRecyclerViewAdapter.notifyDataSetChanged();
    }



    public static class NameAscCompare implements Comparator<VoMoimMemberList.VoMoimMemberlistItem> {
        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(VoMoimMemberList.VoMoimMemberlistItem lhs, VoMoimMemberList.VoMoimMemberlistItem rhs) {
            return lhs.getUsNm().compareTo(rhs.getUsNm());
        }
    }

    /**
     * 멤버들의 권한설정 바로가기
     */
    private void intentPermission() {
        Intent intent = new Intent(Activity_SetLeaderManage.this, Activity_SetPermission.class);
        //intent.putExtra("TimeLineItem", item);
        startActivity(intent);
    }


    /**
     * 공동리더 추가
     * 리더위임화면과 공통사용
     */
    private void intentAddLeader() {
        Intent intent = new Intent(Activity_SetLeaderManage.this, Activity_SetAddLeader.class);
        intent.putExtra(Constants.INTENT_TYPE,Constants.ADD_LEADER);
        startActivity(intent);
    }


}
