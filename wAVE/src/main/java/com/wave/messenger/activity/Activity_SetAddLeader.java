package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.AddLeaderRecycleAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_AddLeader;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 공동리더관리 - 공동리더 추가  화면
 * 공동리더관리 - 리더 위임  화면
 */
public class Activity_SetAddLeader extends BaseActivity {

    String TAG = getClass().getSimpleName();

    String sType;
    private AddLeaderRecycleAdapter mAddLeaderRecycleAdapter;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_add_leader);

        Intent intent = getIntent();
        sType = intent.getStringExtra(Constants.INTENT_TYPE);
        //Constants.ADD_LEADER 공동리더추가
        //

        initView(sType);

        setEdtSearch();
        setOnClickEvent(sType);
        setRecyclerView(sType);

    }


    /**
     * 검색 기능
     */
    private void setEdtSearch() {


        //검색
        ((EditText)findViewById(R.id.et_topbar_search)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                Log.e(TAG,"onTextChanged: "+i+" "+i1+" "+i2);

                if(i2==0){
                    findViewById(R.id.imgb_close).setVisibility(View.GONE);
                }else{
                    findViewById(R.id.imgb_close).setVisibility(View.VISIBLE);
                }

                String strText=((EditText)findViewById(R.id.et_topbar_search)).getText().toString().trim();
                if(TextUtils.isEmpty(strText)){
                    //다시 보이기 처리
                    setOriginalList();

                }else{      //검색  , 상단 메뉴 숨김처리
                    setSearchList(strText);
                    //setheadVisibility(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        //edt 지우기
        findViewById(R.id.imgb_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setheadVisibility(false);
                ((EditText)findViewById(R.id.et_topbar_search)).setText("");
                findViewById(R.id.imgb_close).setVisibility(View.GONE);
            }
        });

    }


    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arMemberList;  //원본 멤버리스트
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arSearchMemberList;    //검색된 멤버리스트


    /**
     * 검색_원래 리스트로 되돌리기
     */
    private void setOriginalList() {
        setDate(arMemberList);
        //mMoimInfoRecyclerAdapter.setItem(arMemberList);
        //mWrapAdapter.notifyDataSetChanged();
    }

    /**
     * 검색_입력한 텍스트로 리스트 재구성
     * @param strText
     */
    private void setSearchList(String strText) {
        arSearchMemberList= new ArrayList<>();
        //arMemberList = mMoimInfoRecyclerAdapter.getMemberlistItem();

        for (VoMoimMemberList.VoMoimMemberlistItem item : arMemberList) {
            if (item.getUsNm().contains(strText)) {
                arSearchMemberList.add(item);
            }
        }
        mAddLeaderRecycleAdapter.setItem(arSearchMemberList);
        mAddLeaderRecycleAdapter.notifyDataSetChanged();
    }



    private void initView(String sType) {

        if (sType.equals(Constants.ADD_LEADER)) { //공동리더 추가
            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.leader_manage));


            findViewById(R.id.tv_complete).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_complete)).setText(getString(R.string.add));
            findViewById(R.id.imgb_right).setVisibility(View.GONE); //추가 텍스




        } else if (sType.equals(Constants.MANDATE_LEADER)) { // 리더위임

            ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.leader_mandate));
            findViewById(R.id.imgb_right).setVisibility(View.VISIBLE); //완료 이미지 텍스트로해야할듯
        }

        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명
        findViewById(R.id.view_top_line).setVisibility(View.GONE);

    }





    private void setOnClickEvent(final String sType) {
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //추가 공동리더 추가.
        findViewById(R.id.tv_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //선택된 모든 멤버를 공동리더로 등록
                if (mAddLeaderRecycleAdapter.getSelectedLeaderItem() == null) {
                    Toast.makeText(getApplicationContext(), "공동리더 인원을 선택하세요. ", Toast.LENGTH_SHORT).show();
                }else{
                    //Log.e(TAG, "SelectedLeaderList " + mAddLeaderRecycleAdapter.getSelectedList());

                    //TODO 팝업 없이?
                    moimUserLeverRequest("2",mAddLeaderRecycleAdapter.getSelectedList());
                }
            }
        });


        //완료 버튼 리더위임
        findViewById(R.id.imgb_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mAddLeaderRecycleAdapter.getSelectedLeaderItem() == null) {
                    Toast.makeText(getApplicationContext(), "위임할 회원을 선택하세요. ", Toast.LENGTH_SHORT).show();

                } else {
                    //체크된 리더만   취소,확인 내용택스트
                    final Dialog_Text dialog = new Dialog_Text(Activity_SetAddLeader.this, getResources().getString(R.string.leader_mandate_info), Constants.DIALOG_BUTTON_TYPE.cancle_confirm);
                    //dialog.setData(getResources().getString(R.string.leader_remove_dialog_info));
                    dialog.setListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.e(TAG, "SelectedLeaderList " + mAddLeaderRecycleAdapter.getSelectedLeaderItem());

                            moimUserLeverRequest("1",mAddLeaderRecycleAdapter.getSelectedLeaderItem());

                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }


                /*if (sType.equals(Constants.ADD_LEADER)) {
                    //Toast.makeText(getApplicationContext(), mAddLeaderRecycleAdapter.getSelectedList().size(), Toast.LENGTH_SHORT).show();

                } else if (sType.equals(Constants.MANDATE_LEADER)) {

                    if (mAddLeaderRecycleAdapter.getSelectedLeaderItem() == null) {
                        Toast.makeText(getApplicationContext(), "위임할 회원을 선택하세요. ", Toast.LENGTH_SHORT).show();

                    } else {
                        //체크된 리더만   취소,확인 내용택스트
                        final Dialog_Text dialog = new Dialog_Text(Activity_SetAddLeader.this, getResources().getString(R.string.leader_mandate_info), Constants.DIALOG_BUTTON_TYPE.cancle_confirm);
                        //dialog.setData(getResources().getString(R.string.leader_remove_dialog_info));
                        dialog.setListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.e(TAG, "SelectedLeaderList " + mAddLeaderRecycleAdapter.getSelectedLeaderItem());

                                moimDeleteRequest("1",mAddLeaderRecycleAdapter.getSelectedLeaderItem());

                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                }*/

            }
        });



    }

    private void setRecyclerView(String sType) {
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Activity_SetAddLeader.this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mAddLeaderRecycleAdapter = new AddLeaderRecycleAdapter(Activity_SetAddLeader.this, sType);
        mRecyclerView.setAdapter(mAddLeaderRecycleAdapter);

        MoimProfileRequest();

    }


    /**
     * 어댑터에 붙인다
     *
     * @param voBoardList
     */
    private void setDate(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> voBoardList) {
        mAddLeaderRecycleAdapter.setItem(voBoardList);
        mAddLeaderRecycleAdapter.notifyDataSetChanged();
    }


    /**
     * 멤버들의 권한설정 바로가기
     */
    private void intentPermission() {
        Intent intent = new Intent(Activity_SetAddLeader.this, Activity_SetPermission.class);
        startActivity(intent);
    }

    /**
     * 7.9 모임 멤버 리스트
     *
     */
    private void MoimProfileRequest() {
        MOALog.w(TAG + " requestMoimProfile");
        Moa.moimMemberlistRequest(this, "1","1","0", mMainActivityHandler);
    }

    /**
     * 7.21 모임 멤버레벨 설정
     * <p>
     * usrLv     사용자 레벨(int)     0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)
     * trgtUsr     설정할 사용자 아이디     여러명일 경우 콤마텍스트로 보냄
     */
    private void moimUserLeverRequest(String usrLv, String trgtUsr) {
        Moa.moimUserLeverRequest(this, usrLv, trgtUsr, mMainActivityHandler);
    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_MEMBERLIST:   //멤버리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            try {   //gson파싱
                                VoMoimMemberList voMoimMemberList;
                                Gson gson = new Gson();
                                //voMoimProfile = gson.fromJson( data.body.get("params").toString() , VoMoimProfile.class);

                                voMoimMemberList = gson.fromJson(data.body.toString(), VoMoimMemberList.class);

                                setDate(voMoimMemberList.getParams());

                                arMemberList=new ArrayList<>(voMoimMemberList.getParams()); //검색을 위한 원본 멤버리스트

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_USER_LEVEL:   //7.21 모임 멤버레벨 설정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());


                            //리더위임과 공동리더 구분

                            if(sType.equals(Constants.ADD_LEADER)){
                                reaultAddLeader();
                                //공동리더 완료이후 처리




                            }else{
                                try {   //son파싱
                                    //리더위임 결과처리
                                    //나의 사용자레벨 처리
                                    //사용자 레벨 저장
                                    SharedObject.setProperty_string(Activity_SetAddLeader.this, Constants.mLv, "0");  //내 사용자 레벨(int) 0:일반, 1:리더, 2:공동리더  //TODO 내가 멤버로? 공동리더로?

                                    Activity_SetManage.activity.finish();   //설정관리화면 닫기
                                    Activity_MoimInfo.activity.finish();    //모임정보화면 닫기
                                    finish(); //현재화면닫기

                                    //모임 타임라인 새로고침
                                    if(Activity_MoimTimeLineList.activity!=null){
                                        Activity_MoimTimeLineList.activity.bMoimProfile=false;
                                        Activity_MoimTimeLineList.activity.refreshItems();
                                    }

                                    Toast.makeText(Activity_SetAddLeader.this, "리더위임을 완료하였습니다.", Toast.LENGTH_SHORT).show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 공동리더 완료처리
     */
    private void reaultAddLeader() {
        final Dialog_AddLeader dialog = new Dialog_AddLeader(Activity_SetAddLeader.this );
        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentPermission();
                dialog.dismiss();

                Activity_SetLeaderManage.activity.finish();
                finish();
            }
        });

        dialog.setLaterListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Activity_SetLeaderManage.activity.finish();
                finish();
            }
        });
        dialog.show();

    }


}
