package com.wave.messenger.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;

/**
 * 모임설정관리 - 모임 멤버수 설정 화면
 */
public class Activity_SetMaxMember extends BaseActivity {

    private static final String TAG = "Activity_SetMaxMember";

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_set_max_member);

        initView();
        setOnClickEvent();


        setMaxUser();


    }


    /**
     * 체크박스 선택.
     */
    private void setSelect(boolean b0, boolean b1, boolean b2, boolean b3, boolean b4, boolean b5, boolean b6) {

        findViewById(R.id.img_item1).setSelected(b0);
        findViewById(R.id.img_item2).setSelected(b1);
        findViewById(R.id.img_item3).setSelected(b2);
        findViewById(R.id.img_item4).setSelected(b3);
        findViewById(R.id.img_item5).setSelected(b4);
        findViewById(R.id.img_item6).setSelected(b5);
        findViewById(R.id.img_item7).setSelected(b6);
    }

    /**
     * 최대사용자값 화면에 세팅
     */
    private void setMaxUser() {
        //값 가져와서 화면 체크버튼
        String maxUser = SharedObject.getProperty_string(this, Constants.maxUsr, "100");

        switch (maxUser){
            case "50":
                setSelect(true,false,false,false,false,false,false);
                /*findViewById(R.id.imgb_item1).setBackgroundResource(R.drawable.selector_check);
                findViewById(R.id.imgb_item2).setBackgroundResource(R.drawable.ic_checkbox);
                findViewById(R.id.imgb_item3).setBackgroundResource(R.drawable.ic_checkbox);
                findViewById(R.id.imgb_item3).setBackgroundResource(R.drawable.ic_checkbox);*/
                break;

            case "100":
                setSelect(false,true,false,false,false,false,false);
                break;
            case "500":
                setSelect(false,false,true,false,false,false,false);
                break;
            case "1000":
                setSelect(false,false,false,true,false,false,false);
                break;
            case "3000":
                setSelect(false,false,false,false,true,false,false);
                break;
            case "5000":
                setSelect(false,false,false,false,false,true,false);
                break;
            case "10000":
                setSelect(false,false,false,false,false,false,true);
                break;
        }
    }

    private void initView() {
        //((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.set_permission));
        //((TextView)findViewById(R.id.tv_topbar_title2)).setText(SharedObject.getProperty_string(this,Constants.mmNm,""));

        ((TextView) findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_set_max_member)); //상단 타이틀 이름
        ((TextView) findViewById(R.id.tv_topbar_name)).setText(SharedObject.getProperty_string(this, Constants.mmNm, ""));  //모임명

        findViewById(R.id.view_top_line).setVisibility(View.GONE);

        }


    private void setOnClickEvent() {

        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.ll_select1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_50);
            }
        });

        findViewById(R.id.ll_select2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_100);
            }
        });

        findViewById(R.id.ll_select3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_500);
            }
        });

        findViewById(R.id.ll_select4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_1000);
            }
        });

        findViewById(R.id.ll_select5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_3000);
            }
        });

        findViewById(R.id.ll_select6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_5000);
            }
        });

        findViewById(R.id.ll_select7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_10000);
            }
        });


        /*findViewById(R.id.imgb_item1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_50);
            }
        });
        findViewById(R.id.imgb_item2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_100);
            }
        });
        findViewById(R.id.imgb_item3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResultMaxMember(Constants.MAX_MEMBER_500);
            }
        });*/
    }


    /**
     * @param result
     */
    private void setResultMaxMember(int result) {


        SharedObject.setProperty_string(this, Constants.maxUsr, result+"");

        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",result);
        setResult(RESULT_OK,returnIntent);
        finish();
    }


}
