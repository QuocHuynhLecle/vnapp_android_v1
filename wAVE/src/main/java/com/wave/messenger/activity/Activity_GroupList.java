package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.adapter.GroupListRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.GroupDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_GroupEdit;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoGroupList;

import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017-07-14.
 */
public class Activity_GroupList extends Activity implements MOAEventListener {

    static public Activity_GroupList activity;

    String TAG=this.getClass().getName();
    GroupListRecyclerAdapter groupListRecyclerAdapter;
    RecyclerView recyclerView;

    public String gId, grpNm;

    public static Activity_GroupList getInstance() {
        return activity;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //화면 풀사이즈
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_grouplist);

        activity=this;

        initView();
        setOnClickEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG,"onResume");
        //GroupListRequest();
    }

    private void initView() {
        ((TextView)findViewById(R.id.textViewTitle)).setText(getString(R.string.title_group));
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        GroupListRequest();
    }

    /**
     * 이벤트 리스너설정
     */
    private void setOnClickEvent() {
        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.imgb_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_GroupEdit addGroup = new Dialog_GroupEdit(Activity_GroupList.this);
                addGroup.show();
                addGroup.setTv_title(0);
                addGroup.setOkListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!"".equals(addGroup.getGroupName())) {
                            GroupAddRequest(addGroup.getGroupName());
                            addGroup.dismiss();
                        } else {
                            Toast.makeText(Activity_GroupList.this, "그룹명을 입력해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    /**
     리스트 설정
     */
    public void setRecyclerView(ArrayList<VoGroupList> groupLists) {

        //RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        groupListRecyclerAdapter = new GroupListRecyclerAdapter(this, groupLists);
        recyclerView.setAdapter(groupListRecyclerAdapter);
        groupListRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnEvent(MOAData moaData) {

        LogTrace.E("MOADATA OnEvent");
        mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
    }

    public void setGroupListData() {
        ArrayList<VoGroupList> voGroupLists;
        voGroupLists = new GroupDbHelper(Activity_GroupList.this).getGroupList();

        setRecyclerView(voGroupLists);
    }

    /**
     * 그룹 등록 요청
     */
    public void GroupAddRequest(String grpNm) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            value.put("grpNm", grpNm);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_ADD, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 그룹 목록 요청
     */
    public void GroupListRequest() {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 그룹 수정 요청
     */
    public void GroupEditRequest(String gId, String grpNm) {
        try {
            this.gId = gId;
            this.grpNm = grpNm;
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            value.put("gId", gId);
            value.put("grpNm", grpNm);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_UPDATE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 그룹 삭제 요청
     */
    public void GroupDeleteRequest(String gId) {
        try {
            this.gId = gId;
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getApplicationContext()));
            value.put("gId", gId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_DELETE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    //그룹결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_GROUP_LIST :    //그룹 리스트
                        MOALog.i("PTC_IMS_GROUP_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());
                            GroupDbHelper db1 = LocalDB.getGroupDbHelper(getApplicationContext());
                            db1.addGroupList(getApplicationContext(), data, true);

                            setGroupListData();
                            //GroupListRequest();
                        }
                        break;
                    case PacketTypes.PTC_IMS_GROUP_ADD :
                        MOALog.i("PTC_IMS_GROUP_ADD :");
                        MOALog.i("result=" + data.body.get("result"));
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            hideKeyboard();
                            GroupListRequest();
                            Toast.makeText(Activity_GroupList.this, "등록 되었습니다", Toast.LENGTH_SHORT).show();
                            BusProvider.getInstance().post(new FragmentEventHelper("FriendListRequest", null, null));
                        } else if (data.body.get("result").equals("error")) {
                            hideKeyboard();
                            Toast.makeText(Activity_GroupList.this, "이미 등록된 그룹명 입니다", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case PacketTypes.PTC_IMS_GROUP_UPDATE :
                        MOALog.i("PTC_IMS_GROUP_UPDATE :");
                        MOALog.i("result=" + data.body.get("result"));
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            hideKeyboard();
                            GroupDbHelper db1 = LocalDB.getGroupDbHelper(getApplicationContext());
                            db1.updateGroupList(gId, grpNm);
                            GroupListRequest();
                            Toast.makeText(Activity_GroupList.this, "수정 되었습니다", Toast.LENGTH_SHORT).show();
                            BusProvider.getInstance().post(new FragmentEventHelper("FriendListRequest", null, null));
                        } else if (data.body.get("result").equals("error")) {
                            hideKeyboard();
                            Toast.makeText(Activity_GroupList.this, "중복된 그룹명 입니다", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case PacketTypes.PTC_IMS_GROUP_DELETE :
                        MOALog.i("PTC_IMS_GROUP_DELETE :");
                        MOALog.i("result=" + data.body.get("result"));
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            hideKeyboard();
                            GroupDbHelper db1 = LocalDB.getGroupDbHelper(getApplicationContext());
                            db1.deleteGroupList(gId);
                            GroupListRequest();
                            Toast.makeText(Activity_GroupList.this, "삭제 되었습니다", Toast.LENGTH_SHORT).show();
                            BusProvider.getInstance().post(new FragmentEventHelper("FriendListRequest", null, null));
                        } else if (data.body.get("result").equals("error")) {
                            hideKeyboard();
                            Toast.makeText(Activity_GroupList.this, "삭제 오류", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });
}
