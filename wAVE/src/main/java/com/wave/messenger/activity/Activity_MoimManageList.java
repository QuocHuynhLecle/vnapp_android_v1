package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.IonManageEventListener;
import com.wave.messenger.adapter.MoimManagePixRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.helper.OnStartDragListener;
import com.wave.messenger.rvjoiner.JoinableAdapter;
import com.wave.messenger.rvjoiner.JoinableLayout;
import com.wave.messenger.rvjoiner.RvJoiner;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimHiddenList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * 모임 관리 화면
 * 2017.07.17
 */
public class Activity_MoimManageList extends BaseActivity implements OnStartDragListener {

    String TAG=this.getClass().getSimpleName();

    private static final int FIX_TITLE_TYPE = 21;       //상단 고정
    private static final int NO_FIX_TITLE_TYPE = 22;    //고정안함

    private RvJoiner rvJoiner = new RvJoiner();
    private JoinableLayout JoinableLayoutTitle1;


    boolean bRefresh=false; //모임관리 수정시 false, 모임리스트 새로고침


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bRefresh)
        Activity_MymoimList.activity.refreshItems();
    }

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_moim_manage);

        initView();
        setEventListener();

        moimListRequest();

    }

    private void initView() {
        ((ImageView)findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.selector_close);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.moim_manage));
    }

    private void setEventListener() {


        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {   //드래그가 시작될때 호출
        Log.e(this.getClass().getName(),"onStartDrag");
        ///mItemTouchHelper.startDrag(viewHolder);

    }


    private void setRecyclerView(final VoMoimHiddenList params) {
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(this));

        final ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arVoMoim1;
        final ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arVoMoim2;

        arVoMoim1=params.getPixList(); //상단고정리스트
        arVoMoim2=params.getNoPixList(); //고정안함 리스트

        //상단 해제->고정 모임버튼 리스너
        IonManageEventListener onManageEventListener1 = new IonManageEventListener() {
            @Override
            public void onClearEvent(int idx) {
                //TODO 해제 프로토콜
                ///moimHiddenRequest(arVoMoim1.get(idx).getMmId(), "0");

                moimOrderRequest(arVoMoim1.get(idx).getMmId(), "");

                arVoMoim2.add(arVoMoim1.get(idx));
                arVoMoim1.remove(idx);

                //if(arVoMoim1.size()==0)JoinableLayoutTitle1.visibleEmptyView(Activity_InvisibleMoimManageList.this); //사이즈 0 이면 비어있음표시

                rvJoiner.getAdapter().notifyDataSetChanged();
            }
        };


        //상단 상단에 고정 어댑터
        MoimManagePixRecyclerAdapter invisibleAdapter1 = new MoimManagePixRecyclerAdapter(this, arVoMoim1,onManageEventListener1, Constants.LIST_TYPE.nopix);

        //고정->해제 모임버튼 리스너
        IonManageEventListener onManageEventListener2 = new IonManageEventListener() {
            @Override
            public void onClearEvent(int idx) {

                if(arVoMoim2.size()<1){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.limit), Toast.LENGTH_SHORT).show();
                    return;
                }

               //TODO 고정 프로토콜  moimHiddenRequest(arVoMoim2.get(idx).getMmId(), "1");
                moimOrderRequest(arVoMoim2.get(idx).getMmId(), "0");

                arVoMoim1.add(arVoMoim2.get(idx));
                arVoMoim2.remove(idx);

                //if(arVoMoim1.size()>0)JoinableLayoutTitle1.inVisibleEmptyView(); //사이즈1 이상이면 비어있음 숨김

                rvJoiner.getAdapter().notifyDataSetChanged();
            }
        };


        //하단 고정안함 어댑터
        MoimManagePixRecyclerAdapter visibleAdapter2 = new MoimManagePixRecyclerAdapter(this, arVoMoim2,onManageEventListener2, Constants.LIST_TYPE.pix);

        //title1
        //JoinableLayoutTitle1= new JoinableLayout(R.layout.item_moim_manage_invisible_title, TITLE_TYPE_VISIVLE,this, arVoMoim1.size()==0);
        JoinableLayoutTitle1= new JoinableLayout(R.layout.item_moim_manage_pix_title, FIX_TITLE_TYPE);
        rvJoiner.add(JoinableLayoutTitle1);
        rvJoiner.add(new JoinableAdapter(invisibleAdapter1));

        //title2
        rvJoiner.add(new JoinableLayout(R.layout.item_moim_manage_nopix_title, NO_FIX_TITLE_TYPE));
        rvJoiner.add(new JoinableAdapter(visibleAdapter2));

        rv.setAdapter(rvJoiner.getAdapter());
    }


    /**
     * 7.1 모임 리스트
     */
    private void moimListRequest() {
        Moa.moimListRequest(this,"0",mMainActvtHandler);
    }

    /**
     * 7.37 모임 관리(상단고정)
     */
    private void moimOrderRequest(String mmmId,String mmOrd) {
        Moa.moimOrderRequest(this, mmmId,  mmOrd, mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);
                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST :    //모임리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)) {

                            VoMoimHiddenList voMoimHiddenList;
                            Gson gson = new Gson();
                            voMoimHiddenList = gson.fromJson( data.body.toString() , VoMoimHiddenList.class);

                            Log.e(TAG,"voMoimHiddenList size: "+voMoimHiddenList.getParams().size());

                            setRecyclerView(voMoimHiddenList);
                        }
                        break;


                    case 0x00080040 :    //모임 관리(상단고정)
                        strResult = (String) data.body.get("result");
                        if(strResult.equals(Constants.SUCCESS)) {
                            //TODO 응답오류. 응답 오류이후 이동 처리

                            bRefresh=true;

                        }

                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });






}
