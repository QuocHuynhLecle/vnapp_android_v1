package com.wave.messenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.messenger.adapter.ChatRoomAlbumAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * List<String> url_path = getIntent().getStringArrayListExtra("URL_PATH"); 이렇게 받을 예정임.
 */
public class Activity_ChatAlbum extends Activity implements ChatRoomAlbumAdapter.OnListClickListener, ChatRoomAlbumAdapter.OnListLongClickListener {

    private static Activity_ChatAlbum instance = null;

    public static Activity_ChatAlbum getInstance() {
        return instance;
    }

    private RecyclerView rvAlbum;
    private TextView tvCount, tvSelect;
    private ImageView ivBack;

    private ChatRoomAlbumAdapter adapter;

    private AtomicInteger aInt;
    private List<AlbumPicture> pictureList;
    //하단바
    private LinearLayout llAction;
    private ImageView ivDown, ivDel;

    private List<AlbumPicture> selectedList = new ArrayList<>();

    private boolean isSelectMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatalbum);
        instance = this;
        initView();
        setEvent();
        getData();
    }

    private void initView() {
        tvCount = (TextView) findViewById(R.id.tvCount);
        tvSelect = (TextView) findViewById(R.id.tvSelect);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        llAction = (LinearLayout) findViewById(R.id.llAction);
        ivDown = (ImageView) findViewById(R.id.ivDown);
        ivDel = (ImageView) findViewById(R.id.ivDel);

        //리스트뷰 설정 - 그리드뷰
        rvAlbum = (RecyclerView) findViewById(R.id.rvAlbum);
        rvAlbum.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void setEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//RESULT_CANCLED로 액티비티 종료
                onBackPressed();
            }
        });

        ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (AlbumPicture selected : selectedList) {
                    new ImageDownloader(selected).execute(selected.getUrlPath());
                }
            }
        });

        ivDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO : 사진을 삭제 처리한다.
                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(Activity_ChatAlbum.this);
                List<AlbumPicture> list = adapter.getSelectedData();
                for (AlbumPicture item : list) {
                    db.delChatData(item.getChatId());
                    Intent mIntent = new Intent();
                    mIntent.putExtra("delItem", item.getChatId());
                    setResult(Constants.RESULT_CHAT_ALBUM, mIntent);
//                    Fragment_ChatRoom.getInstance().removeChat(item.getChatId());
                }
                getData();

            }
        });

        tvSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelectMode = !isSelectMode;
                setMode(isSelectMode);
            }
        });

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void setMode(boolean mode) {
        if(mode) {
            adapter.setBGFrame(true);
            tvCount.setText(String.valueOf(selectedList.size()));
            tvCount.setVisibility(View.VISIBLE);
            tvSelect.setText("선택 해제");
        } else {
            selectedList.clear();
            adapter.setUnSelected();
            adapter.setBGFrame(false);
            tvCount.setVisibility(View.INVISIBLE);
            tvSelect.setText("선택");
            llAction.setVisibility(View.GONE);
        }
    }

    private void doAction(boolean mode, AlbumPicture picture, int position) {
        if(mode) {
            if(!selectedList.contains(picture)) {
                selectedList.add(picture);
            }else{
                selectedList.remove(picture);
            }
            adapter.setItemSelected(position);
        } else {
            Intent intent = new Intent(this, Activity_PreviewImage.class);
            intent.putExtra("picture", picture);
            intent.putExtra("pictureList", (Serializable) pictureList);
            startActivity(intent);
        }
    }

    public void getData() {
        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(this);
        ArrayList<VoChatData> list = db.getImage(Statics.ROOMINFO.getRoomId());

        if (list.size() > 0) {
            pictureList = new ArrayList<>();
            String url = "";
            for (VoChatData item : list) {
                try {
                    JSONObject object = new JSONObject(item.getAttachment());
                    String path = object.getString("save_file");
                    url = Constants.URL_IMAGE_BASE + path;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                pictureList.add(new AlbumPicture(url, item.getChatId()));
            }

            adapter = new ChatRoomAlbumAdapter(pictureList, this, this, this);
            rvAlbum.setAdapter(adapter);
            aInt = new AtomicInteger(0);
            setMode(isSelectMode);
        } else {
            Toast.makeText(this, "사진이 없습니다.", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @Override
    public void onClickList(AlbumPicture picture, int position) {
        doAction(isSelectMode, picture, position);
        if(isSelectMode) {
            tvCount.setText(String.valueOf(selectedList.size()));
            if (selectedList != null && selectedList.size() > 0) {
                llAction.setVisibility(View.VISIBLE);
            } else {
                llAction.setVisibility(View.GONE);
            }
        } else {
            llAction.setVisibility(View.GONE);
        }
    }//end of onClickList

    @Override
    public void onLongClick() {
        if(!isSelectMode) {
            isSelectMode = true;
            setMode(isSelectMode);
        }
    }//end of onLongClick.


    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private AlbumPicture selected;

        public ImageDownloader(AlbumPicture selected) {
            this.selected = selected;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = params[0];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");

                File file;
                try {
                    Date now = new Date();
                    android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

                    File path = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES);
                    file = new File(path, now + "" + aInt.incrementAndGet() + ".jpg");

                    FileOutputStream out = new FileOutputStream(file);

                    image.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(Uri.fromFile(file));
                    sendBroadcast(mediaScanIntent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[Activity_ChatAlbum] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                Toast.makeText(Activity_ChatAlbum.this, "이미지가 저장되었습니다.", Toast.LENGTH_SHORT).show();
                selected.setSelected(false);
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(Activity_ChatAlbum.this, "이미지 저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
