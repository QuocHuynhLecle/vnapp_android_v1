package com.wave.messenger.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_PhoneFriend;
import com.wave.messenger.fragment.Fragment_RecommendFriend;
import com.wave.messenger.fragment.dummy.TestInviteItem;
import com.wave.messenger.util.Constants;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 9. 22..
 */

public class Activity_FriendRecommend extends BaseActivity {
    private LinearLayout ll_back, ivFriendplus;
    private boolean isRecommend = true;
    private TextView tv_title,tv_ok, tv_friend;
    private View.OnClickListener mOnClickListener;
    private Fragment_PhoneFriend fragment_phonefriend;
    private Fragment_RecommendFriend fragment_recommendFriend;
    private ImageView iv_friend;
    private EditText et_search;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_friendrecommend);

        ll_back = (LinearLayout) findViewById(R.id.ll_back);
        ivFriendplus = (LinearLayout) findViewById(R.id.ivFriendplus);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_ok = (TextView) findViewById(R.id.tv_ok);
        iv_friend = (ImageView) findViewById(R.id.iv_friend);
//        tv_friend = (TextView) findViewById(R.id.tv_friend);
        et_search = (EditText) findViewById(R.id.et_search);

        switchFragment();

        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivFriendplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               switchFragment();
            }
        });

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment_phonefriend.getSelectecPhoneNumList().size() < 1) {
                    Toast.makeText(Activity_FriendRecommend.this, "연락처를 선택하세요", Toast.LENGTH_SHORT).show();
                } else {
                    inviteLoop();
                }
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String text = et_search.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    //리스트
                    if(isRecommend) {
                        fragment_phonefriend.setRecyclerView();
                    }else{
                        fragment_recommendFriend.setRecyclerView();
                    }

                } else {//검색
                    if(isRecommend) {
                        fragment_phonefriend.filter(text);
                    }else {
                        fragment_recommendFriend.filter(text);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void switchFragment() {

        FragmentManager fm = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            fm = getSupportFragmentManager();
        }


        if (isRecommend) {
            et_search.setText("");
            tv_title.setText("친구 추가");
            tv_ok.setVisibility(View.GONE);
            iv_friend.setBackgroundResource(R.drawable.ic_invite_friends);
//            tv_friend.setText("친구초대");
            fragment_recommendFriend = new Fragment_RecommendFriend();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, fragment_recommendFriend);
            fragmentTransaction.commit();
        } else {
            et_search.setText("");
            tv_title.setText("친구 초대");
            tv_ok.setVisibility(View.VISIBLE);
            iv_friend.setBackgroundResource(R.drawable.ic_recommend_friends);
//            tv_friend.setText("추천친구");
            fragment_phonefriend = new Fragment_PhoneFriend();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout, fragment_phonefriend);
            fragmentTransaction.commit();
        }
        isRecommend = (isRecommend) ? false : true;



    }

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    /**
     * 문자 메시지 선택만큼 반복.
     */

    private void inviteLoop() {
        String PhoneNum = null;
        String msg = null;
        ArrayList<String> phone = new ArrayList();

        for (TestInviteItem item : fragment_phonefriend.getSelectecPhoneNumList()) {


            //Util.getMsg
            msg =  getResources().getString(R.string.invite_app) + Constants.CHAT_INVITE_URL;

            PhoneNum = item.getPhone();
            phone.add(item.getPhone().toString());

        }

        String toNumbers = "";
        for ( String s :  phone)
        {
            toNumbers = toNumbers + s + ";";
        }

        sendToSms(toNumbers, msg);
    }

    /**
     * 문자를 보낸다.
     * @param number
     * @param msg
     */
    private void sendToSms(String number, String msg) {

        String strNumber=number.replace("[","");
        strNumber=strNumber.replace("]","");
        strNumber=strNumber.replace("-","");

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("smsto:"+number));
            intent.putExtra("sms_body", msg);
            intent.putExtra("exit_on_sent", true);
            startActivityForResult(intent, 111);

        } catch (Exception ex) {

        }
    }


}
