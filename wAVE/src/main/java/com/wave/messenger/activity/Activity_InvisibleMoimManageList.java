package com.wave.messenger.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.messenger.adapter.IonManageEventListener;
import com.wave.messenger.adapter.MoimManagePixRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.rvjoiner.JoinableAdapter;
import com.wave.messenger.rvjoiner.JoinableLayout;
import com.wave.messenger.rvjoiner.RvJoiner;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimHiddenList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * 숨김 모임 관리 화면
 */
public class Activity_InvisibleMoimManageList extends BaseActivity {

    String TAG=this.getClass().getSimpleName();

    private static final int TITLE_TYPE_VISIVLE = 21;
    private static final int TITLE_TYPE_INVISIVLE = 22;

    //private RvJoiner rvJoiner = new RvJoiner(true);  //XXX
    private RvJoiner rvJoiner = new RvJoiner();
    private JoinableLayout JoinableLayoutTitle1;

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        setContentView(R.layout.activity_invisible_moim_manage);

        initView();
        setEventListener();

        moimListRequest();

        //setRecyclerView(voMoimHiddenList);




    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Activity_MymoimList.activity.refreshItems();

    }

    /**
     * 모임리스트를 호출한다.
     */
    private void moimListRequest() {
        MOALog.w("Fragment_MoimTab2_MyMoimList moimListRequest" );
        Moa.moimListRequest(this,"0",mMainActvtHandler);
    }

    /**
     *7.25 모임 숨김/해제
     */
    private void moimHiddenRequest(String mmid, String hdn) {
        Moa.moimHiddenRequest(this, mmid, hdn,  mMainActvtHandler);
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST :    //모임리스트
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)) {

                            VoMoimHiddenList voMoimHiddenList;
                            Gson gson = new Gson();
                            voMoimHiddenList = gson.fromJson( data.body.toString() , VoMoimHiddenList.class);

                            Log.e(TAG,"voMoimHiddenList size: "+voMoimHiddenList.getParams().size());
                            setRecyclerView(deleteMmTy(voMoimHiddenList));
                        }


                        break;

                    case PacketTypes.PTC_IMS_MOIM_HIDDEN :    //모임 숨김/해제


                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 추천모임은 지워준다
     * @param voMoimHiddenList
     * @return
     */
    private VoMoimHiddenList deleteMmTy(VoMoimHiddenList voMoimHiddenList) {


        VoMoimHiddenList list= new VoMoimHiddenList();

        ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arVoMoimHiddenList= new ArrayList<>();

        for (VoMoimHiddenList.VoMoimHiddenListItem item:voMoimHiddenList.getParams()){
            if(item.getMmTy().equals("0")){
                arVoMoimHiddenList.add(item);
            }
       }

        list.setParams(arVoMoimHiddenList);

        return voMoimHiddenList;
    }


    private void initView() {

        ((ImageView)findViewById(R.id.imgb_back_btn)).setImageResource(R.drawable.selector_close);
        findViewById(R.id.view_top_line).setVisibility(View.GONE);

        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getResources().getString(R.string.invisible_moim_manage));
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.imgb_right).setVisibility(View.GONE);
    }


    private void setEventListener() {


        findViewById(R.id.ll_top_Bar).findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setRecyclerView(final VoMoimHiddenList params) {
        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(this));

        final ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arVoMoim1;
        final ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arVoMoim2;

        arVoMoim1=params.getHiddenList();   //숨김
        arVoMoim2=params.getNormalList();   //일반


        //숨김->일반 꺼내기 모임버튼 리스너
        IonManageEventListener onManageEventListener1 = new IonManageEventListener() {
            @Override
            public void onClearEvent(int idx) {

                moimHiddenRequest(arVoMoim1.get(idx).getMmId(), "0");

                arVoMoim2.add(arVoMoim1.get(idx));
                arVoMoim1.remove(idx);

                //if(arVoMoim1.size()==0)JoinableLayoutTitle1.visibleEmptyView(Activity_InvisibleMoimManageList.this); //사이즈 0 이면 비어있음표시




                rvJoiner.getAdapter().notifyDataSetChanged();
            }
        };


        //숨김모임 어댑터
        MoimManagePixRecyclerAdapter invisibleAdapter1 = new MoimManagePixRecyclerAdapter(this, arVoMoim1,onManageEventListener1, Constants.LIST_TYPE.invisible);
        //MoimManagePixRecyclerAdapter invisibleAdapter1 = new MoimManagePixRecyclerAdapter(this, arVoMoim1,onManageEventListener1, Constants.LIST_TYPE.invisible);


        //recyclerView.setHasFixedSize(true);
        //recyclerView.setAdapter(adapter);
        //recyclerView.setLayoutManager(new LinearLayoutManager(Activity_InvisibleMoimManageList.this));



        //일반->숨김 숨기기 모임버튼 리스너
        IonManageEventListener onManageEventListener2 = new IonManageEventListener() {
            @Override
            public void onClearEvent(int idx) {

                if(arVoMoim2.size()<1){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.limit), Toast.LENGTH_SHORT).show();
                    return;
                }

                moimHiddenRequest(arVoMoim2.get(idx).getMmId(), "1");
                arVoMoim1.add(arVoMoim2.get(idx));
                arVoMoim2.remove(idx);

                //if(arVoMoim1.size()>0)JoinableLayoutTitle1.inVisibleEmptyView(); //사이즈1 이상이면 비어있음 숨김

                rvJoiner.getAdapter().notifyDataSetChanged();
            }
        };


        //노출중인 모임 어댑터
        MoimManagePixRecyclerAdapter visibleAdapter2 = new MoimManagePixRecyclerAdapter(this, arVoMoim2,onManageEventListener2, Constants.LIST_TYPE.visible);

        //title1
        //JoinableLayoutTitle1= new JoinableLayout(R.layout.item_moim_manage_invisible_title, TITLE_TYPE_VISIVLE,this, arVoMoim1.size()==0);
        JoinableLayoutTitle1= new JoinableLayout(R.layout.item_moim_manage_invisible_title, TITLE_TYPE_VISIVLE);
        rvJoiner.add(JoinableLayoutTitle1);
       rvJoiner.add(new JoinableAdapter(invisibleAdapter1));

        //title2
        rvJoiner.add(new JoinableLayout(R.layout.item_moim_manage_visible_title, TITLE_TYPE_INVISIVLE));
        rvJoiner.add(new JoinableAdapter(visibleAdapter2));

        rv.setAdapter(rvJoiner.getAdapter());



    }


    private void checkItem1List() {
    }






}
