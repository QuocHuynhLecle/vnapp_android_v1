package com.wave.messenger.activity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wave.messenger.adapter.MoimRecyclerAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimSearch;
import com.kevin.wraprecyclerview.WrapAdapter;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 9..
 * 모임찾기 액티비티
 * 모임글 검색으로 이름변경.
 * [모임, 종목 코멘트 찾기]
 *
 */
public class Activity_MoimFind extends BaseActivity {

    String TAG=getClass().getSimpleName();

    private WrapAdapter<MoimRecyclerAdapter> mWrapAdapter;  //상단 주제별 모임 찾기 뷰와 하단 모임리스트 합치기위한 어댑터.
    MoimRecyclerAdapter mMoimRecyclerAdapter;   //모임 리스트
    private EndlessRecyclerViewScrollListener scrollListener;   //가장마지막 스크롤 다음페이지로딩
    SwipeRefreshLayout mSwipeRefreshLayout; //드래그 다운 새로고침
    boolean mbRefresh=false;    //새로고침 구분

    @Override
    public void OnReceiveMsgFromFragment(int msgId, Object param1, Object param2) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * AveApp(20171123)
         * StatusBar 색상
         */
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        setContentView(R.layout.activity_moim_find);

        initView();
        setRecyclerView();

        mSwipeRefreshLayout=((SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        moimSearchRequest("2",""+"","","");
    }

    /**
     * 새로고침
     */
    void refreshItems() {
        MOALog.w("refreshItems");

        mbRefresh=true;
        setRecyclerView();
        moimSearchRequest("2","","","");
    }

    /**
     * 새로고침 완료후 호출됨.
     */
    void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
        mbRefresh = false;
    }


    private void setRecyclerView() {

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        //RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(this, 1);
        //recyclerView.setLayoutManager(recyclerViewLayoutManager);
        mMoimRecyclerAdapter = new MoimRecyclerAdapter(this);

        mWrapAdapter = new WrapAdapter<>(mMoimRecyclerAdapter);
        mWrapAdapter.adjustSpanSize(recyclerView);
        recyclerView.setAdapter(mWrapAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        RecyclerView.LayoutManager recyclerViewLayoutManager= gridLayoutManager;
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                loadNextDataFromApi(mMoimRecyclerAdapter.getLastMmId());
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

        addHeaderView();
    }

    /**
     * 페이지 로딩
     * @param mmid
     */
    public void loadNextDataFromApi(String mmid) {
        Log.e("TAG", "loadNextDataFromApi mmid: "+mmid);
        moimSearchRequest("2",mmid+"","","");
    }

    /**
     * 서버에서 받아온 값을 어댑더에 세팅한다.
     * @param params
     */
    private void setAdapterItem(ArrayList<VoMoimSearch.VoMoimSearchItem> params) {
        mMoimRecyclerAdapter.setItem(params,mbRefresh);
        mMoimRecyclerAdapter.notifyDataSetChanged();
        mWrapAdapter.notifyDataSetChanged();

        onItemsLoadComplete();
    }

    LinearLayout mHeaderLayout;

    /**
     * 헤더뷰를 붙인다
     */
    private void addHeaderView() {
        mHeaderLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.header_moim_find, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mHeaderLayout.setLayoutParams(params);
        mWrapAdapter.addHeaderView(mHeaderLayout);
        setOnClickEvent(mHeaderLayout);
    }


    /**
     * 검색 날짜
     * @param strtDt
     * @param endDt
     */
    private void setDate(String strtDt, String endDt) {
        Log.e(TAG,"setDate: "+strtDt +" "+endDt);
         if(null!=endDt){
             ((TextView)mHeaderLayout.findViewById(R.id.tv_date)).setText(endDt+" ~ "+strtDt);
         }
    }

    /**
     * 타이틀뷰세팅
     */
    private void initView() {
        ((TextView)findViewById(R.id.tv_topbar_title)).setText(getString(R.string.title_find_moim));
        findViewById(R.id.tv_complete).setVisibility(View.GONE);
    }


    /**
     * 헤더 이벤트리스너
     * @param layout
     */
    private void setOnClickEvent(LinearLayout layout) {

//        findViewById(R.id.view_top_line).setVisibility(View.GONE);

        //종목
        layout.findViewById(R.id.ll_search_by_subjects).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_MoimFind.this, Activity_MoimFindBySubject.class);
                intent.putExtra(Constants.MOIM_TYPE, Constants.SET_MOIM_LIST.search_by_subject);
                startActivity(intent);
            }
        });

        //뒤로가기
        findViewById(R.id.imgb_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayList<LinearLayout> arImgBtn= new ArrayList<>();
        for(int i=0; i<2; i++) {
            {
                String buttonID = "imgb_subject" + (i + 1);
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                arImgBtn.add((LinearLayout) layout.findViewById(resID));
                final int finalI = i;
                arImgBtn.get(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intentMoimList(finalI);
                    }
                });
            }
        }

        //모임명 및 종목코드로 검색해보세요
        findViewById(R.id.ll_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentMoimList(6);
            }
        });
    }

    /**
     * nType: 6   모임명 종목코드찾기화면
     * nType: 0~5 주제별 리스트화면
     * @param nType
     */
    private void intentMoimList(int nType) {
        Intent intent = new Intent(Activity_MoimFind.this, Activity_MoimList.class);
        intent.putExtra("TYPE",nType);
        startActivity(intent);
    }

    /**
     * 모임검색
     * @param md
     * @param mmId
     * @param srchWrd
     * @param mmTg
     */
    private void moimSearchRequest(String md,String mmId, String srchWrd ,String mmTg) {
        Log.e(TAG," moimWriteRequest" +md+" srchWrd: "+srchWrd+" "+mmTg);
        Moa.moimSearchRequest(Activity_MoimFind.this, md, mmId, srchWrd, mmTg, mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_SEARCH :    //7.5 모임 검색
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){

                            VoMoimSearch voMoimSearch;
                            Gson gson = new Gson();
                            voMoimSearch = gson.fromJson( data.body.toString() , VoMoimSearch.class);

                            //Log.e(TAG,"voMoimSearch size: "+voMoimSearch.getParams().size());
                            setAdapterItem(voMoimSearch.getParams());
                            setDate(voMoimSearch.getStrtDt(), voMoimSearch.getEndDt());

                        }else{
                            //Toast.makeText(Activity_MoimFind.this, "새로시작한모임 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

}
