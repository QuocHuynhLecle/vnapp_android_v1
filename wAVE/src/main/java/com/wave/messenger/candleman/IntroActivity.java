package com.wave.messenger.candleman;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_CandleMain;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.SharedObject;

public class IntroActivity extends AppCompatActivity {

    String TAG = "IntroActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);


            showLoginDialog();

//        showLoginDialog();
        //intentMain();
    }


    /**
     * 임의아이디로 로그인하기위한 다이얼로그
     */
    protected void showLoginDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_login, null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setView(dialogLayout);

        //dialog.setView(inflater.inflate(R.layout.dialog_login, null))
        dialog.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Dialog f = (Dialog) dialog;
                        EditText uName;
                        uName = (EditText) f.findViewById(R.id.edt_id);
                        Log.i(TAG, uName.getText().toString() + " " + uName.getText().toString());

                        MessengerInfo.setUserId(IntroActivity.this, uName.getText().toString());
                        SharedObject.setProperty_string(IntroActivity.this, "LOGIN", uName.getText().toString());


                        String name=((EditText)f.findViewById(R.id.edt_name)).getText().toString().trim();
                        MessengerInfo.setUsername(IntroActivity.this, name);
                        SharedObject.setProperty_string(IntroActivity.this, "NAME", name);

                        String phone=((EditText)f.findViewById(R.id.edt_phone)).getText().toString().trim();
                        MessengerInfo.setUserPhoneNumber(IntroActivity.this, phone);
                        SharedObject.setProperty_string(IntroActivity.this, "PHONE", phone);


                        Log.e("showLoginDialog","setProperty_string: "+uName.getText().toString()+" "+name+" "+phone);
                        intentMain();

                    }
                });
                /*.setNegativeButton(R.string.cancel,  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });*/

        ((EditText)dialogLayout.findViewById(R.id.edt_id)).setText(SharedObject.getProperty_string(IntroActivity.this,"LOGIN","test0001"));
        Log.e("showLoginDialog","LOGIN: "+SharedObject.getProperty_string(IntroActivity.this,"LOGIN","test0001"));

        ((EditText)dialogLayout.findViewById(R.id.edt_name)).setText(SharedObject.getProperty_string(IntroActivity.this,"NAME","에이브"));

        //((EditText)dialogLayout.findViewById(R.id.edt_phone)).setText(Util.getPhoneNum(this));
        ((EditText)dialogLayout.findViewById(R.id.edt_phone)).setText("01012345678");


                //SharedObject.getProperty_string(IntroActivity.this,"PHONE","012-345-6789"));





        AlertDialog alert = dialog.create();
        alert.show();

        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(Color.WHITE);
        pbutton.setTextColor(Color.BLACK);
    }


    private void intentMain() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroActivity.this, Activity_CandleMain.class);
                //if(mbIsPush) intent.putExtra(CommParams.PUSHMSG, msPush);
                startActivity(intent);
                //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                finish();
            }
        }, 500);
    }

}
