package com.wave.messenger.candleman;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.wave.messenger.Info.DeviceInfo;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.Info.UserInfo;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.utility.PushProc;
import com.wave.messenger.vo.HanaMember;
import com.wave.messenger.vo.VoFriendList;
import java.util.ArrayList;

import moa.android.api.MOAClient;

/**
 * Created by aveapp on 2017-06-09.
 */

@SuppressLint("NewApi")
public class MessengerInterface {
    private static final String TAG = "MessengerInterface";
    private static MessengerInterface messengerInterface = null;
    public static MessengerInterface getInstance() {
        if(messengerInterface == null) {
            messengerInterface = new MessengerInterface();
        }
        return messengerInterface;
    }
    public static ArrayList<VoFriendList> voFriendLists;
    public static ArrayList<HanaMember> customerBuddyList;

    /**
     * AveApp 메신저 로그인
     * @param user : 로그인 정보            //SSAM 로그인 정보를 담아서 호출
    MessengerInfo
    String userName = "";
    String userId = "";
    String password = "";
    String pushKey = "";
    String phoneNumber = "";
    String userType = "";           //유저타입 입니다. UT_AD <관리자>, UT_ST <직원>, UT_ST_RE <종목추천직원>,  UT_ST_CO <상담직원>, UT_ME <회원>, UT_ME_JU <준회원>
    int riskType = 0;               //투자위험도 입니다. 종목 추천을 받는 회원은 1 받지않는 회원은 0 입니다.
    String	empNo = "";			    //직원인경우 사번, 고객인경우 "";
    String offYN = "";
    */
    public void HanaToMessengerLogin(Context mContext, UserInfo user)
    {
		/*
	     로그인 이후, Messanger에 본 메쏘드를 호출하여 필요한 데이터를 전달
	     <필요한 데이터 - userInfo 오브젝트에 담아주시면 됩니다.>
	     1. 유저 아이디
	     2. 비밀번호 (암호화)
	     3. push key
	     4. 전화번호
	     5. 유저 타입 :NSString
	        UT_AD <관리자>, UT_ST <직원>, UT_ST_RE <종목추천직원>,  UT_ST_CO <상담직원>, UT_ME <회원>, UT_ME_JU <준회원>
	     6. 이름
	     */

        MessengerInfo.setUserId(mContext, user.getUserId());
        MessengerInfo.setUserPw(mContext, user.getPassword());
        MessengerInfo.setUserTocken(mContext, user.getPushKey());
        String phoneNumber = user.getPhoneNumber();
        if(phoneNumber == null || phoneNumber.equals("")) {
            phoneNumber = DeviceInfo.getPhoneNumber(mContext);
        }
        MessengerInfo.setUserPhoneNumber(mContext, phoneNumber);
        MessengerInfo.setUsername(mContext, user.getUserName());
        MessengerInfo.setStaff(mContext, "");
        MessengerInfo.setFirstContactUploaded(mContext, true);
        MessengerInfo.setFirstLogin(mContext, false);
        MessengerInfo.setAutoLogin(mContext, false);
        MessengerInfo.setUserType(user.getUserType());
        MessengerInfo.setRiskType(user.getRiskType());
        MessengerInfo.setEmpNo(user.getEmpNo());
        MessengerInfo.setOffYN(user.getOffYN());
        MessengerInfo.setBranchCode(user.getBranchCode());
        MessengerInfo.setBranchName(user.getBranchName());
        MessengerInfo.setAgree(mContext, user.isAgreeYN());  //약관동의 여부
    }

    public void HanaToMessengerGCMOnMessage(Context context, Intent intent) {
        String power;
        PowerManager mPowerManager;
        try {
                mPowerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                if(mPowerManager.isScreenOn()) {
                    power = "1";
                } else {
                    power = "0";
                }

                PushProc pushProc = new PushProc();
                pushProc.onReceivePushMsg(context, intent, power);
//            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
