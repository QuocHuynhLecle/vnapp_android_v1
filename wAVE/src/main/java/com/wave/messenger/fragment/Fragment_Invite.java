package com.wave.messenger.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.wave.messenger.adapter.InviteRecycleAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.dummy.TestInviteItem;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.StringProcesser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * 멤버초대 프레그먼트
 */
public class Fragment_Invite extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "Fragment_Invite";

    private String mParam1;

    //private OnFragmentInteractionListener mListener;
    private View.OnClickListener onRightClick;
    private InviteRecycleAdapter inviteRecycleAdapter;
    private EditText et_topbar_search;
    private RecyclerView mRecyclerView;
    private ArrayList<TestInviteItem> mFriendBeanList;  //원래 리스트
    private ArrayList<TestInviteItem> mSearchFriendBeanList; //검색 리스트


    SwipeRefreshLayout mSwipeRefreshLayout;

    public static Fragment_Invite newInstance() {
        Fragment_Invite fragment = new Fragment_Invite();

        //Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_invite, container, false);

        et_topbar_search = (EditText) rootView.findViewById(R.id.et_topbar_search);

        //삭제버튼
        et_topbar_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                rootView.findViewById(R.id.iv_delete).setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count == 0) {
                    rootView.findViewById(R.id.iv_delete).setVisibility(View.GONE);
                } else {
                    rootView.findViewById(R.id.iv_delete).setVisibility(View.VISIBLE);
                }

                String text = et_topbar_search.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    //리스트
                    setRecyclerView(rootView);

                    Log.e(TAG,"onTextChanged isEmpty");
                    //TODO
                    //검색연락처 리스트->연락처리스트
                    //
                } else {//검색
                    filter(text);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //엔터눌렀을 때 검색 ???   불필요
        /*et_topbar_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    Toast.makeText(getActivity(), "search", Toast.LENGTH_SHORT).show();
                    return true;
                }

                return false;
            }
        });*/



        mFriendBeanList = new ArrayList<>();
        mFriendBeanList = getPhoneNumber();



        setOnClickEvent(rootView);

        setRecyclerView(rootView);
        /*if (Util.CheckPhenumPermission(getActivity())) {
            setRecyclerView(rootView);
        }*/
        return rootView;
    }




    /**
     * 검색
     *
     * @param charText
     */
    public void filter(String charText) {
        Log.e("TEST filter charText", charText);

        mSearchFriendBeanList = new ArrayList<>();

        for (TestInviteItem potion : mFriendBeanList) { //문자 검색
            if (potion.getName().contains(charText)) {
                    mSearchFriendBeanList.add(potion);
            }
        }

        if(mSearchFriendBeanList.size()==0){
            for (TestInviteItem potion : mFriendBeanList) { //초성검색
                if (StringProcesser.matchString(potion.getName(),charText)) {
                    mSearchFriendBeanList.add(potion);
                }
            }

        }

       // if(mSearchFriendBeanList.size()==0){
            for (TestInviteItem potion : mFriendBeanList) { //초성검색
                if (StringProcesser.matchString(potion.getPhone(),charText)) {
                    mSearchFriendBeanList.add(potion);
                }
            }

        //}

        //inviteRecycleAdapter.setItem(mSearchFriendBeanList);
        inviteRecycleAdapter.setItem(mSearchFriendBeanList);
        inviteRecycleAdapter.notifyDataSetChanged();

    }


    private void setOnClickEvent(final View rootView) {

        rootView.findViewById(R.id.tv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "search", Toast.LENGTH_SHORT).show();

            }
        });
        rootView.findViewById(R.id.iv_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_topbar_search.setText("");
            }
        });
    }



    private void setRecyclerView(View rootView) {
        Log.e(TAG, "setRecyclerView");
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        //inviteRecycleAdapter = new InviteRecycleAdapter(getActivity());
        //mRecyclerView.setAdapter(inviteRecycleAdapter);

        inviteRecycleAdapter = new InviteRecycleAdapter(getActivity());
        mRecyclerView.setAdapter(inviteRecycleAdapter);
        inviteRecycleAdapter.setItem(mFriendBeanList);
        inviteRecycleAdapter.notifyDataSetChanged();

    }


    public void onButtonPressed(Uri uri) {
        /*if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    /*public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/


    public ArrayList<TestInviteItem> getPhoneNumber() {

        ArrayList<TestInviteItem> arTestInviteItem = new ArrayList<>();


        //String [] arrPhoneProjection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

        TestInviteItem testInviteItem;
        Cursor cursor = null;

        //ContactsContract.Contacts.HAS_PHONE_NUMBER+"=1"
        String[] nameProjection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER
        };


        try {
            cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            int contactIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
            Log.e(TAG, "contactId : " + contactIdIdx + " nameIdx : " + nameIdx + " phoneNumberIdx: " + phoneNumberIdx);
            cursor.moveToFirst();


            do {
                String idContact = cursor.getString(contactIdIdx);
                String name = cursor.getString(nameIdx);
                String phoneNumber = cursor.getString(phoneNumberIdx);
                String photo = cursor.getString(photoIdIdx);
                //Log.e("Fragment_Invite",name+" "+phoneNumber +" "+photo);

                String phoneNum0 = String.valueOf(phoneNumber).substring(0, 3);

                if (phoneNum0.contains("01")) {
                    Collections.sort(arTestInviteItem, new NameAscCompare());
                    testInviteItem = new TestInviteItem(idContact, name, phoneNumber, photo, false);
                    arTestInviteItem.add(testInviteItem);
                }


            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return arTestInviteItem;
    }

    private static class NameAscCompare implements Comparator<TestInviteItem> {

        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(TestInviteItem lhs, TestInviteItem rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Activity_MoimOpen", "permission was granted");

                    setRecyclerView(rootView);

                } else {//퍼미션 거부일때 처리

                    showNumberPermissionDialog(getActivity());
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * 주소록 허가 팝업
     *
     * @param activity
     */
    public static void showNumberPermissionDialog(Activity activity) {
        if (activity != null) {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is not null");
            Log.e(activity.getLocalClassName(), "showCameraPermissionDialog");
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.camera_permission_title));
            builder.setMessage(activity.getString(R.string.read_contacts_permission_message));
            builder.setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog ab = builder.create();

            if (ab != null) {
                ab.show();
            }
        } else {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is null");
        }
    }

    //private OnItemSelectedListener onItemSelectedListener;


    public ArrayList<TestInviteItem> getSelectecPhoneNumList() {
        //onItemSelectedListener.onRssItemSelected(inviteRecycleAdapter.getSelectedList());
        return inviteRecycleAdapter.getSelectedList();
    }

    /*public interface OnItemSelectedListener {
        public void onRssItemSelected(ArrayList<TestInviteItem> items);
    }*/

}
