package com.wave.messenger.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wave.massenger.piggy.R;
import com.wave.messenger.activity.Activity_MoimFind;
import com.wave.messenger.activity.Activity_MymoimList;
import com.wave.messenger.adapter.BoardListRecyclerAdapter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EndlessRecyclerViewScrollListener;
import com.wave.messenger.util.HideShowScrollListener;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 메인 타임라인
 */
public class Fragment_MoimTab1_Timeline extends Fragment {

    String TAG = "Fragment_Tab1_Timeline";
    private OnFragmentInteractionListener mListener;
    private BoardListRecyclerAdapter mRecyclerViewAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    boolean mbRefresh = false; //새로고침 , 추가페이지 구분

    SwipeRefreshLayout mSwipeRefreshLayout;

    //public static Fragment_MoimTab1_Timeline fragment;
    public static Fragment_MoimTab1_Timeline instance = null;
    FrameLayout flTimeline;
    LinearLayout llTimelineNull;    //타임라인 0일때 화면

    public boolean isTimelineEnd=true;

    public static Fragment_MoimTab1_Timeline getInstance() {
        //return instance;
        if(instance!=null){
            return instance;
        }else{
            return  instance =new Fragment_MoimTab1_Timeline();
        }
    }
    public Fragment_MoimTab1_Timeline() {
        //Log.e(TAG, "Fragment_Tab1_Timeline");
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Log.e(TAG, "onCreateView");

        View rootView = inflater.inflate(R.layout.fragment_moim_tab1_timeline, container, false);

        flTimeline = (FrameLayout) rootView.findViewById(R.id.fl_timeline);
        llTimelineNull = (LinearLayout) rootView.findViewById(R.id.ll_timeline_null);

        setRecyclerView(rootView);
        setEventListener(rootView);

        mSwipeRefreshLayout = ((SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        return rootView;
    }

    /**
     * 새로고침
     */
    public void refreshItems() {
        //Log.e(TAG, "refreshItems");
        //MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");

        mbRefresh = true;
        moimSearchRequest("", "");

        //알림 카운터 새로고침.
        Fragment_Main.getInstance().chatroomReadCountRequest();
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
        mbRefresh = false;
    }

    private void setEventListener(View rootView) {
        //상단 내모임으로 가기
        rootView.findViewById(R.id.ll_mymoim).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentMymoim();
            }
        });

        //모임찾기
        rootView.findViewById(R.id.ll_find_moim).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), Activity_MoimFind.class);
                startActivity(mIntent);
            }
        });

        //새로운모임 찾기
        rootView.findViewById(R.id.btn_null_timelinelist_find).setOnClickListener(new View.OnClickListener() { //새로운 모임 찾기
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), Activity_MoimFind.class);
                startActivity(mIntent);
            }
        });

       /* rootView.findViewById(R.id.btn_floating).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_Main.getInstance().mViewPager.setCurrentItem(0);
            }
        });*/
    }

    /**
     * 7.27 모임 타임라인
     *
     * @param mmId
     * @param msgId
     */
    public void moimSearchRequest(String mmId, String msgId) {
        Moa.moimTimeLineRequest(getActivity(), mmId, msgId, mMainActvtHandler);
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("Fragment_MoimTab1_Timeline PTC_IMS_TIMELINE handleMessage : " + data.body.toString());
                //Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_TIMELINE:    //모임 타임라인
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG, "PTC_IMS_MOIM_TIMELINE strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoBoardList voBoardList = null;
                            Gson gson = new Gson();
                            voBoardList = gson.fromJson(data.body.toString(), VoBoardList.class);

                            //리스트에 글이 없을때의 처리(20171216 - 오재득 추천모임이 추가로인해 주석 처리
                            /*if (voBoardList == null || voBoardList.getParams() == null || voBoardList.getParams().size() == 0) {
                                flTimeline.setVisibility(View.GONE);
                                llTimelineNull.setVisibility(View.VISIBLE);
                            } else {
                                flTimeline.setVisibility(View.VISIBLE);
                                llTimelineNull.setVisibility(View.GONE);

                                setDate(voBoardList);
                            }*/

                            flTimeline.setVisibility(View.VISIBLE);
                            llTimelineNull.setVisibility(View.GONE);
                            setDate(voBoardList);

                            onItemsLoadComplete();

                            //TODO 타임라인 호출이후 리스트 호출
                            //Fragment_Main.getInstance(). afterLoginRequest();
//                            Fragment_Main.getInstance().setTimelineView();
                        } else {
                            Toast.makeText(getActivity(), "타임라인 호출 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });


    /**
     * @param voBoardList mbRefresh true이면 새로고침
     */
    private void setDate(VoBoardList voBoardList) {
        mRecyclerViewAdapter.setItem(voBoardList, mbRefresh);  // mSwipeRefreshLayout.isRefreshing() mbRefresh
        mRecyclerViewAdapter.notifyDataSetChanged();
        mbRefresh = false;
    }

    boolean firstTime=true;
    private void setRecyclerView(View rootView) {
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerViewAdapter = new BoardListRecyclerAdapter(getActivity(), Constants.SET_ENTRY_TYPE.main_tab);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(mRecyclerViewAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi();
            }
        };


        /*Log.e("timeline1","RecyclerView start: "+getCurrentTime() );
        ViewTreeObserver vto = recyclerView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (recyclerView.getChildCount() > 0 && firstTime ){
                    firstTime=false;
                    Log.e("timeline1","RecyclerView end: "+getCurrentTime() );
                }
            }
        });*/
        recyclerView.addOnScrollListener(scrollListener);
        setBottomMenu(rootView, recyclerView);
        //moimSearchRequest("", "");

        RequestAsyncTask asyncTask = new RequestAsyncTask();
        asyncTask.execute();
    }

    public class RequestAsyncTask extends android.os.AsyncTask<String, Integer, String> {
        String result;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LoadingManager.with(getActivity()).showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            moimSearchRequest("", "");
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LoadingManager.with(getActivity()).hideLoadingDialog();
        }
    }

    /**
     * 현재시간갖오기
     * @return
     */
    private String getCurrentTime() {
        DateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd hhmmss");
        dateFormatter.setLenient(false);
        Date today = new Date();
        String s = dateFormatter.format(today);
        return s;
    }

    /**
     * 페이지 로딩
     */
    public void loadNextDataFromApi() {
        moimSearchRequest(mRecyclerViewAdapter.getLastMmId(), mRecyclerViewAdapter.getMsgId());
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * 상,하단 베너 영역
     *
     * @param rootView
     * @param recyclerView
     */
    private void setBottomMenu(final View rootView, RecyclerView recyclerView) {
        //하단 버튼
        recyclerView.addOnScrollListener(new HideShowScrollListener() {
            @Override
            public void onHide() {
                //Log.e(TAG, "onHide");
                if (Constants.BOTTOM_ADVERTISING) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                        rootView.findViewById(R.id.ll_bottom_menu).animate().translationY(rootView.findViewById(R.id.ll_bottom_menu).getHeight());

                    } else {
                        rootView.findViewById(R.id.ll_bottom_menu).setVisibility(View.GONE);
                    }
                }

                //상단 숨김
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    rootView.findViewById(R.id.ll_top_menu).animate().translationY(-rootView.findViewById(R.id.ll_top_menu).getHeight());
                } else {
                    rootView.findViewById(R.id.ll_top_menu).setVisibility(View.GONE);
                }
            }

            @Override
            public void onShow() {
                //Log.e(TAG, "onShow");
                if (Constants.BOTTOM_ADVERTISING) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                        rootView.findViewById(R.id.ll_bottom_menu).animate().translationY(0);
                    } else {
                        rootView.findViewById(R.id.ll_bottom_menu).setVisibility(View.VISIBLE);
                    }
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    rootView.findViewById(R.id.ll_top_menu).animate().translationY(0);
                } else {
                    rootView.findViewById(R.id.ll_top_menu).setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * 삭제처리 이후.
     * 어뎁터에서 데이터를 삭제한다
     * <p>
     * WrapAdapter<BoardListRecyclerAdapter> -BoardListRecyclerAdapter
     *
     * @param index
     * @param size
     */
    public void RemoveAdapter(int index, int size) {
        Log.e(TAG, "RemoveAdapter index: " + index + " size: " + size);

        mRecyclerViewAdapter.notifyItemRemoved(index);
        mRecyclerViewAdapter.notifyItemRangeChanged(index, size);
        mRecyclerViewAdapter.notifyDataSetChanged();
    }

    /**
     * 모임 글 리스트 어댑터를 새로고침한다.
     */
    public void reFreshAdapter() {
        mRecyclerViewAdapter.notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    /**
     * 내 모임으로 가기
     */
    private void intentMymoim() {
        Intent mIntent = new Intent(getActivity(), Activity_MymoimList.class);
        startActivity(mIntent);
    }
}
