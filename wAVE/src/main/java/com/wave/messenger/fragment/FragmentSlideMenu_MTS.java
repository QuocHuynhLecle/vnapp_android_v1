package com.wave.messenger.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;


/**
 * FragmentSlideMenu_MTS
 */
public class FragmentSlideMenu_MTS extends Fragment {

    LinearLayout rootView;
    static FragmentSlideMenu_MTS instance = null;
    public static FragmentSlideMenu_MTS getInstance() {
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //1 XML을 잡는다.
        rootView = (LinearLayout)inflater.inflate(R.layout.fragment_slide_menu_mts, container, false);
        //2 XML 안의ID 를 FORMVIEW넣는다.
        LinearLayout formView = (LinearLayout) rootView.findViewById(R.id.layout_formView);
        //3 만든곳에 form을 넣는다.

        rootView.requestLayout();

        return rootView;
    }

    /**
     * StatsusBar 크기계산
     * @return
     */
    public int getStatusBarHeight(){

        int statusHeight = 0;
        int screenSizeType = (getContext().getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK);

        if(screenSizeType != Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            int resourceId = getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");

            if (resourceId > 0) {
                statusHeight = getContext().getResources().getDimensionPixelSize(resourceId);
            }
        }

        return statusHeight;
    }
}
