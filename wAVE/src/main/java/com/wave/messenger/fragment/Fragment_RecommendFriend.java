package com.wave.messenger.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.adapter.FriendRecommendAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.vo.VoFriendList;

import android.support.v4.app.Fragment;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by aveapp on 2017. 9. 22..
 */

public class Fragment_RecommendFriend extends Fragment {
    private RecyclerView rv_friendrecommend;
    private FriendRecommendAdapter friendRecommendAdapter;
    public static Fragment_RecommendFriend fragment;
    private ArrayList<VoFriendList> voFriendListArrayList;
    private ArrayList<VoFriendList> mSearchFriendBeanList; //검색
    private TextView tv_title;
    private LinearLayout ll_nosearch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_invitefriend, container, false);

        fragment = this;

        rv_friendrecommend = (RecyclerView) v.findViewById(R.id.rv_friendrecommend);
        tv_title = (TextView) v.findViewById(R.id.tv_title);
        ll_nosearch = (LinearLayout) v.findViewById(R.id.ll_nosearch);

        tv_title.setText("추천친구");

        setRecyclerView();
        return v;
    }

    public void setRecyclerView(){
        Moa.RecommendFriendList(getActivity(), mMainActivityHandler);
    }

    private void setFriendList(ArrayList<VoFriendList> params) {

        if(params.size() == 0){
            ll_nosearch.setVisibility(View.VISIBLE);
            rv_friendrecommend.setVisibility(View.GONE);
        }else {
            ll_nosearch.setVisibility(View.GONE);
            rv_friendrecommend.setVisibility(View.VISIBLE);

            voFriendListArrayList = new ArrayList<>();

            for (VoFriendList item : params) {
                voFriendListArrayList.add(item);
            }
            rv_friendrecommend.setLayoutManager(new LinearLayoutManager(getActivity()));
            friendRecommendAdapter = new FriendRecommendAdapter(getActivity(), voFriendListArrayList);
            rv_friendrecommend.setAdapter(friendRecommendAdapter);
        }
    }

    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());


                String strResult;
                switch (data.ptc) {

                    case 0x00020017:
                        strResult = (String) data.body.get("result");
                        if (strResult.equals(Constants.SUCCESS)) {

                            try {
                                LBJSONArray array = data.body.getArray("params");

                                ArrayList<VoFriendList> buddys = new ArrayList<VoFriendList>();
                                for (int i = 0; i < array.size(); i++) {
                                    LBJSONObject jo = (LBJSONObject) array.get(i);

                                    VoFriendList item = VoFriendList.loadFromJson(jo);
                                    buddys.add(item);
                                }

                                setFriendList(buddys);
                            } catch (Exception e) {
                                // TODO: handle exception
                            }
                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    public void RemoveAdapter(int index, int size) {
        Log.e("TEST", "RemoveAdapter index: " + index + " size: " + size);

        friendRecommendAdapter.notifyItemRemoved(index);
        friendRecommendAdapter.notifyItemRangeChanged(index, size);
        friendRecommendAdapter.notifyDataSetChanged();

        if(size == 0){
            ll_nosearch.setVisibility(View.VISIBLE);
            rv_friendrecommend.setVisibility(View.GONE);
        }
    }

    /**
     * 검색
     *
     * @param charText
     */
    public void filter(String charText) {
        Log.e("TEST filter charText", charText);

        mSearchFriendBeanList = new ArrayList<>();

        for (VoFriendList potion : voFriendListArrayList) { //문자 검색
            if (potion.getUserName().contains(charText)) {
                mSearchFriendBeanList.add(potion);
            }
        }

        if(mSearchFriendBeanList.size()==0){
            for (VoFriendList potion : voFriendListArrayList) { //초성검색
                if (StringProcesser.matchString(potion.getUserName(),charText)) {
                    mSearchFriendBeanList.add(potion);
                }
            }

        }

        for (VoFriendList potion : voFriendListArrayList) { //초성검색
            if (StringProcesser.matchString(potion.getPhone(),charText)) {
                mSearchFriendBeanList.add(potion);
            }
        }

        //}

        friendRecommendAdapter.setItem(mSearchFriendBeanList);
        friendRecommendAdapter.notifyDataSetChanged();

    }
}
