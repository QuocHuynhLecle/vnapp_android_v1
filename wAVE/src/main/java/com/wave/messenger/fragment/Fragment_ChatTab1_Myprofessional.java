package com.wave.messenger.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.vo.VoMyprofessional;

import java.util.ArrayList;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;


/**
 * 나의 전문가 프레그먼트 화면
 */
public class Fragment_ChatTab1_Myprofessional extends Fragment {

    private SectionedRecyclerViewAdapter sectionAdapter;

    onEventListener onEventListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myprofessional, container, false);

        sectionAdapter = new SectionedRecyclerViewAdapter();


       /* for(char alphabet = 'A'; alphabet <= 'Z';alphabet++) {
            List<String> contacts = getContactsWithLetter(alphabet);

            if (contacts.size() > 0) {
                sectionAdapter.addSection(new ContactsSection(String.valueOf(alphabet), contacts));
            }
        }*/

        /*final ArrayList<VoMyprofessional> arVoMyprofessional1 = new ArrayList<>();
        arVoMyprofessional1.add(new VoMyprofessional("id","상돌이 매니저1","https://tse1.mm.bing.net/th?id=OIP.4WWbTEjldQQdfg0wY_348wCWCW&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,true));
        arVoMyprofessional1.add(new VoMyprofessional("id","상돌이 매니저2","https://tse1.mm.bing.net/th?id=OIP.4WWbTEjldQQdfg0wY_348wCWCW&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,true));
        arVoMyprofessional1.add(new VoMyprofessional("id","상돌이 매니저3","https://tse1.mm.bing.net/th?id=OIP.4WWbTEjldQQdfg0wY_348wCWCW&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,true));
        sectionAdapter.addSection(new ContactsSection(getString(R.string.myprofessional), arVoMyprofessional1));*/

        onEventListener= new onEventListener() {
            @Override
            public void onAddEvent(VoMyprofessional item, int position) {
//                item.setMylist(true);
//                arVoMyprofessional1.add(item);
//                sectionAdapter.notifyItemRemoved(position);
//                sectionAdapter.notifyDataSetChanged();

            }

            @Override
            public void onDeleteEvent(VoMyprofessional item, int position) {
                sectionAdapter.notifyItemRemoved(position);
                sectionAdapter.notifyDataSetChanged();
            }
        };

        /*//test  TODO 타이블 부분이 고정인지 유동적인건지?
        ArrayList<VoMyprofessional> arVoMyprofessional2 = new ArrayList<>();
        arVoMyprofessional2.add(new VoMyprofessional("id","캔들맨 매니저1","https://tse4.mm.bing.net/th?id=OIP.BJM2jXVJ_nSHbeXvPZVP_QCACA&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,false));
        arVoMyprofessional2.add(new VoMyprofessional("id","캔들맨 매니저2","https://tse4.mm.bing.net/th?id=OIP.BJM2jXVJ_nSHbeXvPZVP_QCACA&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,false));
        arVoMyprofessional2.add(new VoMyprofessional("id","캔들맨 매니저3","https://tse4.mm.bing.net/th?id=OIP.BJM2jXVJ_nSHbeXvPZVP_QCACA&pid=15.1&P=0&w=300&h=300","1","2",true,true,true,false));
        sectionAdapter.addSection(new ContactsSection(getString(R.string.recommend_manager), arVoMyprofessional2));

        ArrayList<VoMyprofessional> arVoMyprofessional3 = new ArrayList<>();
        arVoMyprofessional3.add(new VoMyprofessional("id","이정기 애널리스트1","http://www.veryicon.com/icon/png/System/nDroid/android%20market%202.png","1","2",true,true,true,false));
        arVoMyprofessional3.add(new VoMyprofessional("id","이정기 애널리스트2","http://www.veryicon.com/icon/png/System/nDroid/android%20market%202.png","1","2",true,true,true,false));
        arVoMyprofessional3.add(new VoMyprofessional("id","이정기 애널리스트3","http://www.veryicon.com/icon/png/System/nDroid/android%20market%202.png","1","2",true,true,true,false));
        sectionAdapter.addSection(new ContactsSection(getString(R.string.recommend_analyst), arVoMyprofessional3));

        ArrayList<VoMyprofessional> arVoMyprofessional4 = new ArrayList<>();
        arVoMyprofessional4.add(new VoMyprofessional("id","김준연 주식전문가1","https://www.android.com/static/2016/img/share/andy-lg.png","1","2",true,true,true,false));
        arVoMyprofessional4.add(new VoMyprofessional("id","김준연 주식전문가2","https://www.android.com/static/2016/img/share/andy-lg.png","1","2",true,true,true,false));
        arVoMyprofessional4.add(new VoMyprofessional("id","김준연 주식전문가3","https://www.android.com/static/2016/img/share/andy-lg.png","1","2",true,true,true,false));
        sectionAdapter.addSection(new ContactsSection(getString(R.string.recommend_professional), arVoMyprofessional4));


        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(sectionAdapter);*/

        return view;
    }

    /*@Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity.getSupportActionBar() != null)
                activity.getSupportActionBar().setTitle(R.string.nav_example1);
        }
    }*/

    /*private List<String> getContactsWithLetter(char letter) {
        List<String> contacts = new ArrayList<>();

        for (String contact : getResources().getStringArray(R.array.names)) {
            if (contact.charAt(0) == letter) {
                contacts.add(contact);
            }
        }

        return contacts;
    }*/

    class ContactsSection extends StatelessSection {

        String title;
        ArrayList<VoMyprofessional> list;

        public ContactsSection(String title, ArrayList<VoMyprofessional> list) {
            super(R.layout.header_myprofessional, R.layout.item_myprofessional);
            this.title = title;
            this.list = list;
        }

        @Override
        public int getContentItemsTotal() {
            return list.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        public void removeAt(int position) {

            list.remove(position);
            //notifyItemRemoved(position);
            //notifyItemRangeChanged(position, arvalues.size());
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;

            /*Glide.with(getActivity()).load(list.get(position).getProfileImg()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_profile_big_default_android).dontAnimate().into(itemHolder.imgvProfile);


            itemHolder.tvName.setText(list.get(position).getName());*/

            //itemHolder.imgvType
            //itemHolder.tvProfessionalType;

            itemHolder.tvCounseling.setText(getString(R.string.on));
            itemHolder.tvLeading.setText(getString(R.string.off));

            if(position == list.size() - 1) {
                itemHolder.ll_divider.setVisibility(View.GONE);
            }

            itemHolder.imgb_chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), "chat"+itemHolder.getAdapterPosition()  ,Toast.LENGTH_SHORT).show();

                }
            });

            itemHolder.imgb_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    /*if(list.get(position).isMylist()){//나의전문가 리스트
                        Toast.makeText(getActivity(), "Delete"+itemHolder.getAdapterPosition() ,Toast.LENGTH_SHORT).show();
                        //서버처리
                        //나의 리스트에서 삭제
                        //다시 서버에서 리스트 불러오거나, 어디서 왔던 리스트인지 구분해서 원래 자리로 되돌리기\
                        onEventListener.onDeleteEvent(list.get(position),position);
                        removeAt(position);
                    }else{
                        Toast.makeText(getActivity(), "Add"+itemHolder.getAdapterPosition()+" ItemViewType(): "+itemHolder.getItemViewType(),Toast.LENGTH_SHORT).show();
                        //서버처리
                        //삭제하고 나의 전문가 리스트에 추가.


                        onEventListener.onAddEvent(list.get(position),position);
                        removeAt(position);
                    }*/


                }
            });


            //itemHolder.imgItem.setImageResource(name.hashCode() % 2 == 0 ? R.drawable.ic_face_black_48dp : R.drawable.ic_tag_faces_black_48dp);


            itemHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), String.format("Clicked on position #%s of Section %s", sectionAdapter.getPositionInSection(itemHolder.getAdapterPosition()), title), Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            headerHolder.tvTitle.setText(title);
        }
    }


    /**
     * 헤더뷰
     */
    class HeaderViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;

        public HeaderViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        }
    }


    /**
     * 아이템 뷰
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;

        private final ImageView imgvProfile;
        private final TextView tvName;

        private final ImageView imgvType;
        private final TextView tvProfessionalType;

        private final TextView tvCounseling;
        private final TextView tvLeading;
        private final ImageButton imgb_chat, imgb_add;

        private final LinearLayout ll_divider;

        public ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imgvProfile= (ImageView) view.findViewById(R.id.imgv_profile);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvProfessionalType = (TextView) view.findViewById(R.id.tv_professional_type);
            imgvType= (ImageView) view.findViewById(R.id.imgv_type);
            tvCounseling= (TextView) view.findViewById(R.id.tv_counseling);
            tvLeading= (TextView) view.findViewById(R.id.tv_leading);

            imgb_chat = (ImageButton) view.findViewById(R.id.imgb_chat);
            imgb_add = (ImageButton) view.findViewById(R.id.imgb_add);

            ll_divider = (LinearLayout) view.findViewById(R.id.ll_divider);
        }
    }

    public interface  onEventListener {
        void onAddEvent(VoMyprofessional item, int position);
        void onDeleteEvent(VoMyprofessional item, int position);
    }
}
