package com.wave.messenger.fragment;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.wave.messenger.utility.IFragmentBackPressedListener;
import com.wave.messenger.utility.LogTrace;

public class NMFragmentManager {

    public static NMFragmentManager instance = null;

    public static NMFragmentManager getInstance() {
        if (instance == null) {
            instance = new NMFragmentManager();
        }
        return instance;
    }

    FragmentActivity mainActivity = null;
    private static Fragment currentFragment;

    static boolean isclick = true;
    boolean isBack = false;
    int resourceId = 0;


    public void setMainActivity(FragmentActivity activity, int resourceId) {
        this.mainActivity = activity;
        this.resourceId = resourceId;
    }

    public void setMainActivity(FragmentActivity activity) {
        this.mainActivity = activity;
    }


    public void replaceFragment(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                currentFragment = fragment;
                FragmentManager manager = mainActivity.getSupportFragmentManager();
                manager.beginTransaction()
                        .replace(R.id.main_frag_container, fragment)
                        .commitAllowingStateLoss();
                manager.executePendingTransactions();
            } catch (Exception e) {
                e.printStackTrace();
            }
            isclick = true;
        }*/
    }

    public void addFragmentUpAnim(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                currentFragment = fragment;
                FragmentManager manager = mainActivity.getSupportFragmentManager();
                manager.beginTransaction().setCustomAnimations(R.anim.slide_up_info, R.anim.no_change, R.anim.no_change, R.anim.slide_down_info)
                        .add(R.id.main_frag_container, fragment, fragment.getTag())
                        .addToBackStack(fragment.getTag()).commitAllowingStateLoss();
                manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        isclick = true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            isclick = true;
        }*/
    }

    public void nowFragment(Fragment fragment) {
        currentFragment = fragment;
    }

    public void addFragmentRightAnim(Fragment fragment) {
        /*if (isclick) {
            isclick = false;

            try {
                currentFragment = fragment;
                //FragmentManager manager = mainActivity.getSupportFragmentManager();
                FragmentManager manager = WAVEMainActivity.staticMainActivity.getSupportFragmentManager();

                manager.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                        R.anim.slide_in_left, R.anim.slide_out_right)
                        //.add(R.id.main_frag_container, fragment, rfragment.getTag())
                        .add(R.id.fl_main, fragment, fragment.getTag())


                        .addToBackStack(fragment.getTag()).commitAllowingStateLoss();
                manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        isclick = true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }


    public void addFragmentDownAnim(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                currentFragment = fragment;
                FragmentManager manager = WAVEMainActivity.staticMainActivity.getSupportFragmentManager();
                manager.beginTransaction().setCustomAnimations(R.anim.no_change, R.anim.slide_down_info, R.anim.slide_up_info, R.anim.no_change)
                        .add(R.id.fl_main, fragment, fragment.getTag())
                        .addToBackStack(fragment.getTag()).commitAllowingStateLoss();
                manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        isclick = true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }


    public void addFragment(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                currentFragment = fragment;
                FragmentManager manager = mainActivity.getSupportFragmentManager();
                manager.beginTransaction()
                        .add(R.id.main_frag_container, fragment, fragment.getTag())
                        .addToBackStack(fragment.getTag()).commitAllowingStateLoss();
                manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        isclick = true;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }

    public Fragment getBackFragment() {
        FragmentManager.BackStackEntry backEntry = mainActivity.getSupportFragmentManager().getBackStackEntryAt(mainActivity.getSupportFragmentManager().getBackStackEntryCount() - 1);
        String str = backEntry.getName();
        Fragment fragment = mainActivity.getSupportFragmentManager().findFragmentByTag(str);
        return fragment;
    }

    public void removeFragment(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                //FragmentManager manager = mainActivity.getSupportFragmentManager();
                FragmentManager manager = WAVEMainActivity.staticMainActivity.getSupportFragmentManager();
                manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
            } catch (Exception e) {
                mainActivity.finish();
            }
            ErrorController.showMessage("NMFragmentManager.removeFragment");
            isclick = true;
        }*/
    }

    public void removeSubFragment(Fragment fragment) {
        /*if (isclick) {
            isclick = false;
            try {
                FragmentManager manager = mainActivity.getSupportFragmentManager();
                manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
            } catch (Exception e) {
                mainActivity.finish();
            }
            ErrorController.showMessage("NMFragmentManager.removeSubFragment");
            isclick = true;
        }*/
    }

    public void removeMainFragment(Fragment fragment) {
        FragmentManager manager = mainActivity.getSupportFragmentManager();
        manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        fragment.onDestroy();
    }

    public void onBackPressed() {
        LogTrace.E("currentFragment");
        try {
            if (currentFragment != null && currentFragment.isAdded()) {
                ((IFragmentBackPressedListener) currentFragment).onBackPressed();
                LogTrace.E("currentFragment not null");
            } else {
                LogTrace.E("currentFragment null");
                if(isBack) {
                    instance = null;
                    mainActivity.finish();
                } else {
                    LogTrace.E("isBack false");

                    isBack = true;

                    Toast.makeText(mainActivity, "뒤로가기를 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isBack = false;
                        }
                    }, 1000);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}