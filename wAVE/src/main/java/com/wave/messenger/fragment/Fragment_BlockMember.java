package com.wave.messenger.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.wave.messenger.adapter.BlockMemberRecycleAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * 멤버차단탈퇴차단목록 프레그먼트
 */
public class Fragment_BlockMember extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "Fragment_BlockMember";

    private String mParam1;

    private OnFragmentInteractionListener mListener;

    /**
     * 0 맴버목록 : 강제탈퇴
     * 1 차단목록 : 차단해제
     *
     * @param param1
     * @return
     */
    public static Fragment_BlockMember newInstance(String param1) {
        Fragment_BlockMember fragment = new Fragment_BlockMember();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }


    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arMemberList;  //원본 멤버리스트
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arSearchMemberList;    //검색된 멤버리스트


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_set_block_member, container, false);
        setRecyclerView(rootView);
        setOnClickEvent(rootView);
        setEdtSearch(rootView);

        return rootView;
    }


    /**
     * 검색 기능
     *
     * @param rootView
     */
    private void setEdtSearch(final View rootView) {

        //검색
        ((EditText) rootView.findViewById(R.id.et_topbar_search)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                Log.e(TAG, "onTextChanged: " + i + " " + i1 + " " + i2);

                if (i2 == 0) {
                    rootView.findViewById(R.id.imgb_close).setVisibility(View.GONE);
                } else {
                    rootView.findViewById(R.id.imgb_close).setVisibility(View.VISIBLE);
                }

                String strText = ((EditText) rootView.findViewById(R.id.et_topbar_search)).getText().toString().trim();
                if (TextUtils.isEmpty(strText)) {
                    //다시 보이기 처리
                    setOriginalList();

                } else {      //검색  , 상단 메뉴 숨김처리
                    setSearchList(strText);
                    //setheadVisibility(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        //edt 지우기
        rootView.findViewById(R.id.imgb_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //setheadVisibility(false);
                ((EditText) rootView.findViewById(R.id.et_topbar_search)).setText("");
                rootView.findViewById(R.id.imgb_close).setVisibility(View.GONE);
            }
        });

    }

    /**
     * 검색_원래 리스트로 되돌리기
     */
    private void setOriginalList() {
        setDate(arMemberList);
        //mMoimInfoRecyclerAdapter.setItem(arMemberList);
        //mWrapAdapter.notifyDataSetChanged();
    }

    /**
     * 검색_입력한 텍스트로 리스트 재구성
     *
     * @param strText
     */
    private void setSearchList(String strText) {
        arSearchMemberList = new ArrayList<>();
        //arMemberList = mMoimInfoRecyclerAdapter.getMemberlistItem();

        for (VoMoimMemberList.VoMoimMemberlistItem item : arMemberList) {
            if (item.getUsNm().contains(strText)) {
                arSearchMemberList.add(item);
            }
        }
        mBlockAdapter.setItem(arSearchMemberList);
        mBlockAdapter.notifyDataSetChanged();
    }


    private void setOnClickEvent(View rootView) {

        /*rootView.findViewById(R.id.tv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO  검색
                Toast.makeText(getActivity(), "search", Toast.LENGTH_SHORT).show();

           }
        });*/


    }

    /*private void intentMoimManage() {
        Intent mIntent = new Intent(getActivity(), Activity_MoimManageList.class);
        startActivity(mIntent);
    }*/


    BlockMemberRecycleAdapter mBlockAdapter;


    private void setRecyclerView(View rootView) {
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        mBlockAdapter = new BlockMemberRecycleAdapter(getActivity(), mParam1);
        mRecyclerView.setAdapter(mBlockAdapter);

        MoimProfileRequest();

    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * 탈퇴차단이후 새로고침
     */
    public void refresh() {
        Log.e(TAG,"refresh");
        MoimProfileRequest();
    }




    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    /**
     * 7.9 모임 멤버 리스트
     */
    private void MoimProfileRequest() {
        MOALog.w(TAG + " requestMoimProfile");

        if (mParam1.equals("0")) {
            Moa.moimMemberlistRequest(getActivity(), "1", "1","0",mMainActivityHandler);
        } else {
            Moa.moimMemberlistRequest(getActivity(), "2", "1","0",mMainActivityHandler);
        }

    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_MEMBERLIST:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());


                            try {   //gson파싱
                                VoMoimMemberList voMoimMemberList;
                                Gson gson = new Gson();

                                voMoimMemberList = gson.fromJson(data.body.toString(), VoMoimMemberList.class);

                                setDate(voMoimMemberList.getParams());

                                arMemberList = new ArrayList<>(voMoimMemberList.getParams()); //검색을 위한 원본 멤버리스트

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }


    });

    private void setDate(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        mBlockAdapter.setItem(params);
        mBlockAdapter.notifyDataSetChanged();
    }
}
