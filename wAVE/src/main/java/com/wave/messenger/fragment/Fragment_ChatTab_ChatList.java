package com.wave.messenger.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_JongmokList;
import com.wave.messenger.activity.Activity_OpenProfile;
import com.wave.messenger.adapter.ChatListExpandableAdapter;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.db.UsersReadDbHelper;
import com.wave.messenger.dialog.Dialog_Kick;
import com.wave.messenger.helper.ContactUploadHelper;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.mvp.ChatList.ChatListListener;
import com.wave.messenger.mvp.ChatList.ChatListPresenter;
import com.wave.messenger.mvp.ChatRoom.ChatRoomListener;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONObject;

import static com.wave.massenger.piggy.R.id.et_search;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class Fragment_ChatTab_ChatList extends Fragment implements TextWatcher, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener, ChatListListener, ChatRoomListener {

    private ImageView iv_chatList_event_fold, iv_search;
    private TextView tv_chatList_event_contents;
    private ListView lv_fragment_chat_list;
    private ExpandableListView expandableListView;

    //전체 선택 아이콘
    private ImageView ivSelection;
    private TextView imageViewFind;
    // Top Layouts
    private LinearLayout llSearchLayout, llEditLayout;
    private LinearLayout[] topLayouts;
    // 검색
    public EditText editTextFind;

//    private ChatListViewAdapter adapter;
    private ChatListExpandableAdapter adapter;
    private ImageView ivDeleteText;
    /**
     * Flag for folding the event layout
     */
    private boolean isEventFolded = false;
    private boolean isClick = true;
    /**
     * Selected chat rooms - for edit mode
     */
    private List<Integer> selectedPositions = new ArrayList<>();
    private ChatListPresenter chatListPresenter;

    public boolean isChatListEnd=true;

    private ArrayList<VoChatList> childList = new ArrayList<>();
    static Fragment_ChatTab_ChatList instance = null;

    public static Fragment_ChatTab_ChatList getInstance() {
        if(instance!=null){
            return instance;
        }else{
            return  instance =new Fragment_ChatTab_ChatList();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat_tab3_chatlist, container, false);

        chatListPresenter = new ChatListPresenter(this);

        instance = this;

        if(isChatListEnd){
            ChatListAsyncTask asyncTask = new ChatListAsyncTask();
            asyncTask.execute();
        }


        initView(v);
        setListner();

        return v;
    }
    public class ChatListAsyncTask extends android.os.AsyncTask<String, Integer, String> {

        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            setChatTab();
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            isChatListEnd=false;
        }
    }

    private void initView(View v) {
        editTextFind = (EditText) v.findViewById(et_search);
        // 커서 자동 포커스(커서 깜빡임)
        try {
            Field cursor = TextView.class.getDeclaredField("mCursorDrawableRes");
            cursor.setAccessible(true);
            cursor.set(editTextFind, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        expandableListView = (ExpandableListView) v.findViewById(R.id.expandableListView);

        setListView();
    }
    /**
     * 채팅데이터삭제
     * 채팅방리스트요청
     * 대화리스트가져오기
     * 하이퍼링크 데이터요청
     */
    public void setChatTab() {
       LocalDB.getDbHelper(getContext()).deleteTotalChatData();   //채팅 데이터 삭제
        //채팅방 리스트요청


        ContactUploadHelper.requestChatList(new ContactUploadHelper.OnCompleteListener() {
            @Override
            public void onComplete(boolean result) {
                if (result){
                    isChatListEnd=false;
                    LogTrace.E("chat list complete");
                }

            }
        }, getContext());

        //대화리스트
       // GetCurrentChatList();
    }
    public void GetCurrentChatList() {

        try {
         /*   if (chatRoomListener == null) {
                LogTrace.E("chatRoomListener null");
                return;
            }*/

            if (Statics.ROOMINFO == null) {
                LogTrace.E("Statics.ROOMINFO null");
                return;
            }

            String roomId = Statics.ROOMINFO.getRoomId();
            LogTrace.E("roomId : " + roomId);

            if ("".equals(roomId))
                return;

            String lastReadCid = Statics.ROOMINFO.getLast_read_cid();
            String roomType = Statics.ROOMINFO.getRoom_type();

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getContext()));
            value.put("roomId", roomId);
            value.put("room_type", roomType);
            value.put("lastcid", lastReadCid);
            value.put("mode", "2");
            value.put("covl", Statics.ROOMINFO.getUnread());
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_LIST, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void setListner() {
        editTextFind.addTextChangedListener(this);
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnChildClickListener(this);
        BusProvider.getInstance().post(new FragmentEventHelper("setOnChatListListener",null, this));

        editTextFind.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                chatListPresenter.searchChatList(editTextFind.getText().toString(), searchText(childList));
                return false;
            }
        });

        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final VoChatList item = (VoChatList) parent.getAdapter().getItem(position);

                if(!"0".equals(item.getEntered())) {
//                    if ("7".equals(item.getRoom_type())) {
//                        Toast.makeText(getContext(), "모임대화방은 나가기가 불가능 합니다", Toast.LENGTH_SHORT).show();
//                    } else {
//                        getAlert().setTitle("채팅방 나가기")
//                                .setMessage("채팅방을 나가게 되면 모든 대화내용이 삭제되며,삭제된 대화내용은 복구되지 않습니다.")
//                                .setPositiveButton("삭제하기", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        exitChatRoom(item.getRoomId());
//                                    }
//                                })
//                                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.cancel();
//                                    }
//                                }).show();
//                    }
                    exitChatRoomDialog(item.getRoomId());
                }
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        setUpdate();
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        editTextFind.clearFocus();
    }

    public void setListView() {
        ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
        ArrayList<String> groupList = new ArrayList<>();
        groupList.add(Constants.CHATLISTTITLE_JONGMOK);
        groupList.add(Constants.CHATLISTTYPE_MYCHAT);
        childList = db.getChatList();
        adapter = new ChatListExpandableAdapter(groupList, db.getChatList(), getContext());
        expandableListView.setAdapter(adapter);

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
    }

    public void setUpdate() {
        
        ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
        ArrayList<String> groupList = new ArrayList<>();
        groupList.add(Constants.CHATLISTTITLE_JONGMOK);
        groupList.add(Constants.CHATLISTTYPE_MYCHAT);
        childList = db.getChatList();
//        adapter = new ChatListExpandableAdapter(groupList, db.getChatList(), getContext());
        adapter.setGroupList(groupList);
        adapter.setChatList(childList);
//        expandableListView.setAdapter(adapter);

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        adapter.notifyDataSetChanged();
    }

    public void kickUpdate(final String roomName) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Dialog_Kick dialog_kick = new Dialog_Kick(getContext());
                dialog_kick.show();
                dialog_kick.setData(roomName);

                ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
                adapter.setChatList(db.getChatList());
//                lv_fragment_chat_list.setAdapter(adapter);
                expandableListView.setAdapter(adapter);

                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                searchText();
            }
        }, 100);
    }

    private ArrayList<VoChatList> searchText(ArrayList<VoChatList> childList) {
        for (VoChatList item : childList) {
            if (Constants.CHATLISTTYPE_MYCHAT.equals(item.getRoom_name())) {
                return item.getList();
            }
        }

        ErrorController.showMessage("ffmain - searchText() - return null");

        return null;
    }

    private void searchText() {

        if (editTextFind.getText().toString().length() != 0) {
            String test = editTextFind.getText().toString() + "";
            chatListPresenter.searchChatList(test, searchText(childList));
        } else {
            adapter.resetSearch();
            setListView();
        }
    }

    @Override
    public void onChatListResult(boolean result) {
        ErrorController.showMessage("## Fragment_ChatTab_ChatList onChatListResult setListView()");
       // setListView(); //TODO  계속 호출되서 느린느낌.  일단 마지막에 한번 호출은 되야함. leeyunsu

        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
                ArrayList<String> groupList = new ArrayList<>();
                groupList.add(Constants.CHATLISTTITLE_JONGMOK);
                groupList.add(Constants.CHATLISTTYPE_MYCHAT);
                childList = db.getChatList();
                adapter = new ChatListExpandableAdapter(groupList, db.getChatList(), getContext());
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                expandableListView.setAdapter(adapter);

                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }
            }
        }.execute(null, null, null);
    }

    @Override
    public void chatList(MOAData data) {
    }

    @Override
    public void onChatReadMy(MOAData data) {
        LogTrace.E("onChatReadMy");
        setUpdate();
    }

    @Override
    public void showSearchList(ArrayList<String> groupList, ArrayList<VoChatList> chatList) {
        adapter = new ChatListExpandableAdapter(groupList, chatList, getContext());
        expandableListView.setAdapter(adapter);

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void exitRoom(boolean result, MOAData data) {
        exitChatRoom(result, data);
    }

    @Override
    public void createRoom(MOAData data) {
    }

    @Override
    public void receiveMsg(String roomId, VoChatData data) {
        ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());

        if (db.existChatRoom(roomId)) {
            LogTrace.E("receiveMsg");
            setUpdate();
        }
    }

    @Override
    public void recRead() {
    }

    @Override
    public void sendMsg(MOAData data) {
    }

    @Override
    public void onChatRoomInfo(MOAData data) {
        LogTrace.E("onChatRoomInfo");
        setUpdate();
    }

    @Override
    public void onChatListUpdate(String roomId) {
        LogTrace.E("onChatListUpdate");
        setUpdate();
    }

//    public AlertDialog.Builder getAlert(final String str) {
    public void exitChatRoomDialog(final String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("채팅방 나가기")
                .setMessage("채팅방을 나가게 되면 모든 대화내용이 삭제되며,삭제된 대화내용은 복구되지 않습니다.")
                .setPositiveButton("삭제하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitChatRoom(str);
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        // 예/아니오 버튼
        Button nBtn = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nBtn.setTextColor(Color.WHITE);
        Button pBtn = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pBtn.setTextColor(Color.WHITE);

//        AlertDialog.Builder builder;
//        builder = new AlertDialog.Builder(getContext());
//        return builder;
    }

    public void exitChatRoom(String roomId) {
        try {
            ErrorController.showMessage("[FragFragChatList] exitChatRoom check RoomId: " + roomId);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getContext()));
            value.put("roomId", roomId);
            value.put("exitUserId", MessengerInfo.getUserId(getContext()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_EXIT, body);
            setUpdate();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void exitChatRoom(boolean result, MOAData data) {
        LBJSONObject go = data.body.getJson("params");
        String roomId = go.getString("roomId");

        LogTrace.E("exit : " + go.toString());

        if (result) {
            ChatDataDbHelper dataDb = LocalDB.getChatDataDbHelper(getContext());
            ChatListDbHelper listDb = LocalDB.getChatListDbHelper(getContext());
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(getContext());
            UsersReadDbHelper readDb = LocalDB.getUsersReadDbHelper(getContext());

            VoChatList item = listDb.getChatData(roomId);

            if("3".equals(item.getRoom_type())) {
                listDb.updateEnter(item.getRoomId(), "0");
                listDb.updateRoomSubject(item.getRoomId(), "");
                listDb.updateNoti(item.getRoomId(), "1");
                listDb.updateStaff(item.getRoomId(), "0");
            } else {
                LogTrace.E("exit room : " + item.getRoomId());
                listDb.exitChatRoom(item.getRoomId());
            }

            dataDb.exitChatData(roomId);
            userDb.deleteUsers(roomId);
            readDb.deleteUsers(roomId);

            setUpdate();
            BusProvider.getInstance().post(new FragmentEventHelper("setMainTotalCount", null, null));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            BusProvider.getInstance().post(new FragmentEventHelper("setOnChatListListener", null, null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (isClick) {
            isClick = false;
            VoChatList voChatList = (VoChatList) adapter.getChild(groupPosition, childPosition);

            LogTrace.E("chat room : " + voChatList.getRoom_name() + " / roomType : " + voChatList.getRoom_type());

            Intent mIntent;

            if("3".equals(voChatList.getRoom_type()) || "2".equals(voChatList.getRoom_type())) {
                LogTrace.E("chat list : " + voChatList.getRoomSubject());
                if("1".equals(voChatList.getEntered()) && !"".equals(voChatList.getRoomSubject())) {
                    Statics.ROOMINFO = voChatList;
                    mIntent = new Intent(getActivity(), Activity_ChatRoom.class);
                } else {
                    mIntent = new Intent(getActivity(), Activity_OpenProfile.class);
                    mIntent.putExtra("roomLink", voChatList.getRoomShortId());
                    mIntent.putExtra("roomThumb", voChatList.getRoom_profile_image());
                    mIntent.putExtra("roomTitle", voChatList.getRoom_name());
                    mIntent.putExtra("roomType", voChatList.getRoom_type());
                    mIntent.putExtra("enable", voChatList.getEnableWriteMsg());
                    mIntent.putExtra("roomComment", voChatList.getRoomComment());
                    mIntent.putExtra("roomId", voChatList.getRoomId());
                    mIntent.putExtra("maxUser", voChatList.getMaxUser());
                    mIntent.putExtra("isStaff", voChatList.getStaff());
                    mIntent.putExtra("ownerId", voChatList.getOwnerId());
                    mIntent.putExtra("isEnter", voChatList.getEntered());
                    mIntent.putExtra("roomSubject", "");
                }
            } else {
                Statics.ROOMINFO = voChatList;
                //VoChatList voChatList
                mIntent = new Intent(getContext(), Activity_ChatRoom.class);
            }
            startActivity(mIntent);
            isClick = true;

        }
        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        ChatListExpandableAdapter adapter = (ChatListExpandableAdapter) parent.getExpandableListAdapter();

        if (adapter.getGroup(groupPosition).equals(Constants.CHATLISTTITLE_JONGMOK)) {
            Intent mIntent = new Intent(getActivity(), Activity_JongmokList.class);
            startActivity(mIntent);
        }
        return false;
    }
}
