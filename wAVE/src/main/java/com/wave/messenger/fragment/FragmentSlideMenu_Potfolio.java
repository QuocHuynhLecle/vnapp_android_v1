package com.wave.messenger.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentSlideMenu_Potfolio.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentSlideMenu_Potfolio#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSlideMenu_Potfolio extends Fragment {
    static FragmentSlideMenu_Potfolio instance = null;
    public static FragmentSlideMenu_Potfolio getInstance() {
        return instance;
    }

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private int userType;
    LinearLayout rootView;

    public FragmentSlideMenu_Potfolio() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSlideMenu_MTS.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSlideMenu_Potfolio newInstance(String param1, String param2) {
        FragmentSlideMenu_Potfolio fragment = new FragmentSlideMenu_Potfolio();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public void userType(int userType) {
        this.userType = userType;
    }

    //JYJO(20171012) - 포트폴리오 추가 진행
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String portpolioFileName = null;
        switch (userType) {
            case 0:  //리더
                portpolioFileName = "M6910M00";
                break;
            case 1:  //유저
                portpolioFileName = "M6910M00";
                break;
        }
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
