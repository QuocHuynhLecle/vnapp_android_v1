package com.wave.messenger.fragment.dummy;

public class Item_LeaderProfile implements Comparable<Item_LeaderProfile> {

    private String mFriend;
    private boolean isSelected;
    private boolean isLeader;

    /****
     *
     * @param friend
     * @param isSelected
     */
    public Item_LeaderProfile(String friend, boolean isSelected, boolean isLeader) {
        this.mFriend = friend;
        this.isSelected = isSelected;
        this.isLeader=isLeader;
    }

    public String getFriend() {
        return mFriend;
    }

    public void setFriend(String Friend) {
        this.mFriend = Friend;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public void setLeader(boolean leader) {
        isLeader = leader;
    }

    @Override
    public String toString() {
        return mFriend.toString();
    }

    @Override
    public int compareTo(Item_LeaderProfile another) {
        return this.mFriend.compareTo(another.getFriend());
    }
}
