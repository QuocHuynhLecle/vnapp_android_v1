package com.wave.messenger.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wave.messenger.adapter.InviteRecycleAdapter;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.dummy.TestInviteItem;
import com.wave.messenger.utility.StringProcesser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by aveapp on 2017. 9. 22..
 */

public class Fragment_PhoneFriend extends Fragment {
    private RecyclerView rv_friendrecommend;
    private InviteRecycleAdapter inviteRecycleAdapter;
    private ArrayList<TestInviteItem> mFriendBeanList;  //원래 리스트
    private ArrayList<TestInviteItem> mSearchFriendBeanList; //검색 리스트
    Context context;
    private TextView tv_title;

    public static Fragment_PhoneFriend newInstance() {
        Fragment_PhoneFriend fragment = new Fragment_PhoneFriend();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_invitefriend, container, false);
        rv_friendrecommend = (RecyclerView) v.findViewById(R.id.rv_friendrecommend);

        tv_title = (TextView) v.findViewById(R.id.tv_title);
        tv_title.setText("내 휴대폰 친구");

        setRecyclerView();

        return v;
    }


    public ArrayList<TestInviteItem> getPhoneNumber() {

        ArrayList<TestInviteItem> arTestInviteItem = new ArrayList<>();


        //String [] arrPhoneProjection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

        TestInviteItem testInviteItem;
        Cursor cursor = null;

        //ContactsContract.Contacts.HAS_PHONE_NUMBER+"=1"
        String[] nameProjection = new String[]{
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER
        };


        try {
            cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            int contactIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
            int nameIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int photoIdIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
            cursor.moveToFirst();


            do {
                String idContact = cursor.getString(contactIdIdx);
                String name = cursor.getString(nameIdx);
                String phoneNumber = cursor.getString(phoneNumberIdx);
                String photo = cursor.getString(photoIdIdx);
                //Log.e("Fragment_Invite",name+" "+phoneNumber +" "+photo);

                String phoneNum0 = String.valueOf(phoneNumber).substring(0, 3);

                if (phoneNum0.contains("01")) {
                    Collections.sort(arTestInviteItem, new NameAscCompare());
                    testInviteItem = new TestInviteItem(idContact, name, phoneNumber, photo, false);
                    arTestInviteItem.add(testInviteItem);
                }


            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return arTestInviteItem;
    }

    private static class NameAscCompare implements Comparator<TestInviteItem> {

        /**
         * 오름차순(ASC)
         */
        @Override
        public int compare(TestInviteItem lhs, TestInviteItem rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }
    }

    public void setRecyclerView() {

        mFriendBeanList = new ArrayList<>();
        mFriendBeanList = getPhoneNumber();

        // HashMap에 전화번호를 key로 데이터를 담는다 (Key값 중복 안되므로 전화번호 같으면 데이터 덮어씌워짐)
        HashMap<String, TestInviteItem> hashMap = new HashMap<>();
        for (TestInviteItem item : mFriendBeanList) {
            hashMap.put(item.getPhone().replace("-", ""), item);
        }

        // HashMap.size() <= mFriendBeanList.size() -> tempFriendBeanList.size() = mFriendBeanList.size()
        // mFriendBeanList 크기만큼 임시 ArrayList에 저장
        ArrayList<TestInviteItem> tempFriendBeanList = new ArrayList<>();
        for (TestInviteItem item : mFriendBeanList) {
            tempFriendBeanList.add(hashMap.get(item.getPhone().replace("-", "")));
        }

        // 임시 ArrayList 중복 제거 후 새 ArrayList에 저장
        HashSet hashSet = new HashSet(tempFriendBeanList);
        ArrayList<TestInviteItem> addrFriendList = new ArrayList<>(hashSet);

        // 이름 순 정렬
        MemberListCompare memberListCompare = new MemberListCompare();
        Collections.sort(addrFriendList, memberListCompare);

        Log.d(Fragment_PhoneFriend.class.getSimpleName(), "중복제거 후 mFriendBeanList = " + addrFriendList.toString());

        rv_friendrecommend.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_friendrecommend.setLayoutManager(mLayoutManager);

        inviteRecycleAdapter = new InviteRecycleAdapter(getActivity());
        rv_friendrecommend.setAdapter(inviteRecycleAdapter);
        inviteRecycleAdapter.setItem(addrFriendList);
//        inviteRecycleAdapter.setItem(mFriendBeanList);
        inviteRecycleAdapter.notifyDataSetChanged();

    }

    // 리스트 이름순 정렬
    private class MemberListCompare implements Comparator<TestInviteItem>, Serializable {
        @Override
        public int compare(TestInviteItem arg0, TestInviteItem arg1) {
            return arg0.getName().compareTo(arg1.getName());
        }
    }

    public ArrayList<TestInviteItem> getSelectecPhoneNumList() {
        return inviteRecycleAdapter.getSelectedList();
    }

    /**
     * 검색
     *
     * @param charText
     */
    public void filter(String charText) {

        mSearchFriendBeanList = new ArrayList<>();

        for (TestInviteItem potion : mFriendBeanList) { //문자 검색
            if (potion.getName().contains(charText)) {
                mSearchFriendBeanList.add(potion);
            }
        }

        if (mSearchFriendBeanList.size() == 0) {
            for (TestInviteItem potion : mFriendBeanList) { //초성검색
                if (StringProcesser.matchString(potion.getName(), charText)) {
                    mSearchFriendBeanList.add(potion);
                }
            }

        }

        // if(mSearchFriendBeanList.size()==0){
        for (TestInviteItem potion : mFriendBeanList) { //초성검색
            if (StringProcesser.matchString(potion.getPhone(), charText)) {
                mSearchFriendBeanList.add(potion);
            }
        }

        //}

        //inviteRecycleAdapter.setItem(mSearchFriendBeanList);
        inviteRecycleAdapter.setItem(mSearchFriendBeanList);
        inviteRecycleAdapter.notifyDataSetChanged();

    }
}
