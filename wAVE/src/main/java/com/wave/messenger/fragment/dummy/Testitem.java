package com.wave.messenger.fragment.dummy;

import java.io.Serializable;

/**
 * Created by yunsu on 2017. 3. 8..
 */
public class Testitem implements Serializable {
    String Text1;
    String Text2;
    String sTime;
    String sContents;
    String sImgUrl;


    public Testitem(String text1, String text2, String sTime, String sContents, String sImgUrl) {
        Text1 = text1;
        Text2 = text2;
        this.sTime = sTime;
        this.sContents = sContents;
        this.sImgUrl = sImgUrl;
    }

    public String getsImgUrl() {
        return sImgUrl;
    }

    public void setsImgUrl(String sImgUrl) {
        this.sImgUrl = sImgUrl;
    }

    public String getsTime() {
        return sTime;
    }

    public void setsTime(String sTime) {
        this.sTime = sTime;
    }

    public String getsContents() {
        return sContents;
    }

    public void setsContents(String sContents) {
        this.sContents = sContents;
    }

    public String getText1() {
        return Text1;
    }

    public void setText1(String text1) {
        Text1 = text1;
    }

    public String getText2() {
        return Text2;
    }

    public void setText2(String text2) {
        Text2 = text2;
    }



}
