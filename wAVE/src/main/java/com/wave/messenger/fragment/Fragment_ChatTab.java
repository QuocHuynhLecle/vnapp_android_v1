package com.wave.messenger.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.wave.massenger.piggy.R;


/**
 * 탭 프레그먼트
 * 타임라인,마이모임
 */
public class Fragment_ChatTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TabPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private OnFragmentInteractionListener mListener;

    private ImageButton ib_master, ib_friend, ib_chat;

    String TAG="Fragment_Tab";

    public Fragment_ChatTab() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Fragment_ChatTab newInstance(String param1, String param2) {
        Fragment_ChatTab fragment = new Fragment_ChatTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.e(TAG,"onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_chat_tab, container, false);

        InitView(rootView);

        return rootView;
    }

    private void InitView(final View rootView) {
        Log.e(TAG,"InitView");

        //mSectionsPagerAdapter = new TabPagerAdapter(getActivity().getSupportFragmentManager(),rootView);
        mSectionsPagerAdapter = new TabPagerAdapter(getChildFragmentManager(), rootView);


        mViewPager = (ViewPager) rootView.findViewById(R.id.container1);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        //mViewPager.addOnPageChangeListener(mPageChangeListener);

        mViewPager.setOnPageChangeListener(mPageChangeListener);
        ib_master = (ImageButton) rootView.findViewById(R.id.imgv_tab1);
        ib_friend = (ImageButton) rootView.findViewById(R.id.imgv_tab2);
        ib_chat = (ImageButton) rootView.findViewById(R.id.imgv_tab3);

        setTabSelected(true, false, false);

        ib_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"tab1");
                setTabSelected(true, false, false);
                mViewPager.setCurrentItem(0);
            }
        });

        ib_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"tab2");
                setTabSelected(false, true, false);
                mViewPager.setCurrentItem(1);
            }
        });

        ib_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"tab3");
                setTabSelected(false, false, true);
                mViewPager.setCurrentItem(2);
            }
        });

    }

    private void setTabSelected(boolean a, boolean b, boolean c) {
        ib_master.setSelected(a);
        ib_friend.setSelected(b);
        ib_chat.setSelected(c);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
           // throw new RuntimeException(context.toString()
            //        + " must implement OnFragmentInteractionListener");
        }
    }*/


    public class TabPagerAdapter extends FragmentPagerAdapter {
        View rootView;
        String TAG="TabPagerAdapter";
        public TabPagerAdapter(FragmentManager fm, View v) {
            super(fm);
            rootView=v;
        }

        @Override
        public Fragment getItem(int position) {

            Log.e(TAG, "getItem: "+position);

            switch (position) {

                case 0:
                    Fragment_ChatTab_ChatList fragment_ChatTab1_Myprofessional = new Fragment_ChatTab_ChatList();
                    return fragment_ChatTab1_Myprofessional;

                case 1:
                    Fragment_ChatTab_ChatList fragment_ChatTab2_FriendList = new Fragment_ChatTab_ChatList();
                    return fragment_ChatTab2_FriendList;

                case 2:
                    Fragment_ChatTab_ChatList fragment_ChatTab3_ChatList = new Fragment_ChatTab_ChatList();
                    return fragment_ChatTab3_ChatList;

                default:
                    Fragment_ChatTab_ChatList fragment_ChatTab1_Myprofessional_default = new Fragment_ChatTab_ChatList();
                    return fragment_ChatTab1_Myprofessional_default;

            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";

            }
            return null;
        }
    }

    ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    setTabSelected(true, false, false);
                    break;
                case 1:
                    setTabSelected(false, true, false);
                    break;
                case 2:
                    setTabSelected(false, false, true);
                    break;
                default:
                    setTabSelected(true, false, false);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
