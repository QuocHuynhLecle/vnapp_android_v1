package com.wave.messenger.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChangePassword;
import com.wave.messenger.activity.Activity_FriendSet;
import com.wave.messenger.activity.Activity_LockScreen_OnOff;
import com.wave.messenger.activity.Activity_MyPage;
import com.wave.messenger.activity.Activity_Notice;
import com.wave.messenger.activity.Activity_ProgramInfo;
import com.wave.messenger.activity.Activity_Set_Alarm;
import com.wave.messenger.activity.Activity_TextSizeSet;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.sdk.Util.AlarmReceiver;
import com.wave.messenger.sdk.Util.TranService;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.SharedUserInfo;
import com.wave.messenger.utility.Badge;
import com.wave.messenger.utility.Moa;

import java.util.UUID;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * 메인 설정
 */
public class Fragment_MoimTab4_Setup extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    String TAG = "Fragment_Tab1_Timeline";

    private ImageView imgv_profile;
    private View rootView;

    public Fragment_MoimTab4_Setup() {
        Log.e(TAG, "Fragment_Tab1_Timeline");
    }


    public static Fragment_MoimTab4_Setup newInstance(String param1, String param2) {
        Fragment_MoimTab4_Setup fragment = new Fragment_MoimTab4_Setup();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");

//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_moim_tab4_setup, container, false);
        Moa.UserInfo(getActivity(), mActivityhandler);
        init(rootView);
        setEventListener(rootView);

        return rootView;
    }

    private void init(final View rootView) {

        final String url = Constants.CHATDAWN_PROC + "?type=2&userid=" + MessengerInfo.getUserId(getActivity()) + "&date=" + SharedObject.getProperty_string(getActivity(), Constants.pfImg, "");

        ((TextView) rootView.findViewById(R.id.tv_name)).setText(MessengerInfo.getUserName(getActivity().getApplicationContext()));
        ((TextView) rootView.findViewById(R.id.tv_phonenum)).setText(SharedObject.getProperty_string(getActivity(), Constants.tel, ""));
        imgv_profile = (ImageView) rootView.findViewById(R.id.imgv_profile);

        Glide.with(getActivity()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imgv_profile);
    }

    @Override
    public void onResume() {
        super.onResume();
        init(rootView);
    }

    private Handler mActivityhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);
                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_USER_USERINFO:
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult + data.body.toString());

                        if (strResult.equals(Constants.SUCCESS)) {

                            SharedObject.setProperty_string(getActivity(), Constants.tel, data.body.getJson("params").getString("tel"));
                            SharedObject.setProperty_string(getActivity(), Constants.UsNm, data.body.getJson("params").getString("userName"));
                            SharedObject.setProperty_string(getActivity(), Constants.pfImg, data.body.getJson("params").getString("profile_image"));
                            SharedObject.setProperty_string(getActivity(), Constants.STATE_MESSAGE, data.body.getJson("params").getString("profile_subject"));
                            //((TextView) rootView.findViewById(R.id.tv_name)).setText(data.body.getJson("params").getString("userName"));
                        }
                        break;
                    case 0x00010002:
                        strResult = (String) data.body.get("result");
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());


//                            ChatListDbHelper db = LocalDB.getChatListDbHelper(getActivity());
//                            db.dropAllTable();
//                            BuddyDbHelper bdb = LocalDB.getBuddyDbHelper(getActivity());
//                            bdb.dropAllTable();
                            MessengerInfo.clear(getActivity());

                            try {
                                TranService.releaseService(getActivity());
                                AlarmReceiver.stopAlarmReceiver(getActivity());
                            } catch (Exception e) {
                                // TODO: handle exception
                            }

                            getActivity().moveTaskToBack(true);
                            getActivity().finish();

                            Intent reStart = getContext().getPackageManager().getLaunchIntentForPackage(getContext().getPackageName());
                            reStart.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(reStart);
                        } else {
                            getActivity().moveTaskToBack(true);
                            getActivity().finish();
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                        break;

                    case 0x00030007:        // 회원 탈퇴
                        strResult = (String) data.body.get("result");
                        if (strResult.equals(Constants.SUCCESS)) {
                            SharedUserInfo.getInstance(getContext()).clearInfo();
                            MessengerInfo.setLockPassowrd(getActivity(), "");
                            LocalDB.resetDB();
                            new Badge(getContext()).setBadge(0);

                            MessengerInfo.clear(getActivity());

                            try {
                                TranService.releaseService(getActivity());
                                AlarmReceiver.stopAlarmReceiver(getActivity());
                            } catch (Exception e) {
                                Log.e(TAG, e.getLocalizedMessage());
                            }

                            getActivity().moveTaskToBack(true);
                            getActivity().finish();

                            Intent reStart = getContext().getPackageManager().getLaunchIntentForPackage(getContext().getPackageName());
                            reStart.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(reStart);
                        } else {
                            String reason = (String) data.body.get("reason");
                            if ("moim_leader".equals(reason)) {
                                Toast.makeText(getContext(), "모임리더..", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    });

    private void setEventListener(View rootView) {

        //텍스트 설정
        rootView.findViewById(R.id.ll_textset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_TextSizeSet.class));
            }
        });
        //도움말
        rootView.findViewById(R.id.ll_qna).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getContext(), Activity_Notice.class);
                mIntent.putExtra("type", "help");
                startActivity(mIntent);
            }
        });
        //프로그램 정보
        rootView.findViewById(R.id.ll_program_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_ProgramInfo.class));
            }
        });
        //문의하기
        rootView.findViewById(R.id.ll_question).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri uri_email = Uri.parse("mailto:" + "prime@t-ime.com");
                    Intent it_email = new Intent(Intent.ACTION_SENDTO, uri_email);
                    it_email.putExtra(Intent.EXTRA_SUBJECT, "[" + MessengerInfo.getUserId(getActivity()) + "]님의 문의메일");
                    startActivity(it_email);
                } catch (Exception e) {
                    // TODO: handle exception

                }
            }
        });
        //공지사항
        rootView.findViewById(R.id.ll_notice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getContext(), Activity_Notice.class);
                mIntent.putExtra("type", "notice");
                startActivity(mIntent);
            }
        });
        //화면잠금
        rootView.findViewById(R.id.ll_lockscreen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_LockScreen_OnOff.class));
            }
        });
        //알람설정
        rootView.findViewById(R.id.ll_alarm_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_Set_Alarm.class));
            }
        });
        //마이페이지
        rootView.findViewById(R.id.ll_mypage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_MyPage.class));
            }
        });
        //시작설정
//        rootView.findViewById(R.id.ll_start_page).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), Activity_Start_Page.class));
//            }
//        });
        //친구관리
        rootView.findViewById(R.id.ll_friend_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Activity_FriendSet.class));
            }
        });

        // 비밀번호 변경
        rootView.findViewById(R.id.ll_change_passwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Activity_ChangePassword.class));
            }
        });

        //로그아웃
        rootView.findViewById(R.id.ll_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog_Text dialog_text = new Dialog_Text(getActivity(), getActivity().getResources().getString(R.string.logout), Constants.DIALOG_BUTTON_TYPE.logout);
                dialog_text.show();

                dialog_text.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_text.dismiss();
                        SharedUserInfo.getInstance(getContext()).clearInfo();
                        MessengerInfo.setLockPassowrd(getActivity(), "");
                        LocalDB.resetDB();
                        new Badge(getContext()).setBadge(0);
                        Moa.logout(getContext(), mActivityhandler);
                    }
                });
            }
        });

        // 회원탈퇴
        rootView.findViewById(R.id.ll_leave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog_Text dialog_text = new Dialog_Text(getActivity(), getActivity().getResources().getString(R.string.leaveMember), Constants.DIALOG_BUTTON_TYPE.leave_member);
                dialog_text.show();

                dialog_text.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_text.dismiss();
                        Moa.leaveMember(getContext(), mActivityhandler);
                    }
                });
            }
        });
    }
}
