package com.wave.messenger.fragment.dummy;

/**
 * Created by apple on 2017. 3. 23..
 */

public class TestLeaderItem {
    String name;
    boolean isLeader;

    public TestLeaderItem(String name, boolean isLeader) {
        this.name = name;
        this.isLeader = isLeader;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public void setLeader(boolean leader) {
        isLeader = leader;
    }
}
