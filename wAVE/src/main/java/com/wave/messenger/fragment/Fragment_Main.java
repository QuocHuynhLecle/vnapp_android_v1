package com.wave.messenger.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.ActivityLockCheck;
import com.wave.messenger.activity.Activity_AlramList;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_Delivery;
import com.wave.messenger.activity.Activity_Delivery2;
import com.wave.messenger.activity.Activity_LongTextView;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_SelectMoim;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.messenger.activity.Activity_intro;
import com.wave.messenger.activity.HomeWatcher;
import com.wave.messenger.activity.MlTimer;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.GroupDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.db.UsersReadDbHelper;
import com.wave.messenger.dialog.Dialog_TermsHtml;
import com.wave.messenger.fragment.ChatRoom.Fragment_RightNavigation;
import com.wave.messenger.fragment.SlideMenu.FragmentSlideMenu_Main;
import com.wave.messenger.helper.ContactUploadHelper;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.mvp.ChatList.ChatListListener;
import com.wave.messenger.mvp.ChatList.ChatListPresenter;
import com.wave.messenger.mvp.ChatRoom.ChatRoomListener;
import com.wave.messenger.mvp.Main.IMainView;
import com.wave.messenger.mvp.Main.MainPresenter;
import com.wave.messenger.sdk.Util.AlarmReceiver;
import com.wave.messenger.sdk.Util.ImageUploadData;
import com.wave.messenger.sdk.Util.TranService;
import com.wave.messenger.share.HMMessengerShare;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.LocalContactData;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.SharedUserInfo;
import com.wave.messenger.utility.BUSMOAData;
import com.wave.messenger.utility.Badge;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.GreenDaoManager;
import com.wave.messenger.utility.LoadingManager;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.HanaMember;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoChatRoom;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoLinkData;
import com.wave.messenger.vo.VoUserRead;
import com.wave.messenger.vo.VoUsers;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.MOACommonListener;
import moa.android.api.MOAConsts;
import moa.android.api.MOAData;
import moa.android.api.MOAEventListener;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

import static com.wave.massenger.piggy.R.id.tv_msg_unread_count;

/**
 * Created by aveapp on 2017-06-07.
 */

public class Fragment_Main extends Fragment implements MOACommonListener, MOAEventListener, IMainView, ChatListListener {

    static Fragment_Main instance = null;

    public static Fragment_Main getInstance() {
        return instance;
    }

    public static void clearInstance(Fragment_Main value) {
        if (value == instance) {
            instance = null;
        }
    }

    public static Context mFragment_Main_Context;
    public static Fragment_Main fragmentMain;


    public enum CURRENT_FRAG {FRND, CHAT, MORE}

    public CURRENT_FRAG mCURRENT = CURRENT_FRAG.FRND;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static ViewPager mViewPager;
    private LinearLayout appbar_layout;
    // private int menuPosition;
    private final static String TAG = "Fragment_Main";
    View view = null;

    private String mUserID;


    private ChatDataDbHelper mChatDataDb;
    private ChatListDbHelper mChatListDb;
    private UsersReadDbHelper mUsersReadDb;
    private UsersDbHelper mUsersDb;
    private HMMessengerShare mHMMessengerShare;

    public static ArrayList<VoLinkData> mVoLinkDataList;

    //Fragments
    // public Fragment_MoimTab1_Timeline mFragment_MoimTab1_Timeline;
    //public Fragment_ChatTab_FriendList mFragmentFriendList;
    //public Fragment_ChatTab_ChatList mFragmentChatRmList;
    public FragmentSlideMenu_Main fragmentSlideMenu_main;
    public View mtsView;
    public View mtsView2;
    public int mtsWidth, mtsHeight;

    //Controllsers
    public MainPresenter mMainPresenter;
    private ChatListListener chatListListener;
    private ChatRoomListener chatRoomListener;

    private TextView tv_top_category, tv_main_top_sub_category;

    public boolean isLocked = true;
    public boolean isHomePressed = false;

    public HomeWatcher homeWatcher;

    MlTimer mlTimer = null;
    private View mFragmentContainerViewRight;
    private DrawerLayout layout_drawer;
    private Fragment_RightNavigation fragment_rightNavigation;

    private int moimCount;

    public static Fragment_Main newInstance(String userId, String userName, String password, String userIdKey, boolean isUseDefaultProfileImg) {
        instance = new Fragment_Main();
        Bundle args = new Bundle();
        args.putString("userId", userId);
        args.putString("userName", userName);
        args.putString("userPassword", password);
        args.putString("userIdKey", userIdKey);
        args.putBoolean("isUseDefaultProfileImg", isUseDefaultProfileImg);
        instance.setArguments(args);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        //TODO 인트로 뷰페이져 설명
        //처음실행인지 판단위한 pref 필
        if (com.wave.messenger.util.Util.isFirstStart(getActivity())) {
            mUserID = MessengerInfo.getUserId(getContext());
            LocalDB.DB_NAME = mUserID + ".db";
            LocalDB.resetDB();
            startActivity(new Intent(getActivity(), Activity_intro.class));
        }

        instance = this;
        view = inflater.inflate(R.layout.nfragment_main, container, false);

        BusProvider.getInstance().register(this);

        setStart();

        // Set data from app projects
        String userName = getArguments().getString("userName");
        String userId = getArguments().getString("userId");
        String userPassword = getArguments().getString("userPassword");
        String userIdKey = getArguments().getString("userIdKey");
        boolean isUseDefaultProfileImg = getArguments().getBoolean("isUseDefaultProfileImg", false);
        if (isUseDefaultProfileImg) {
            SharedObject.setProperty_string(getActivity(), Constants.pfImg, "");
        }
        if (null != userIdKey && !userIdKey.isEmpty()) {
            MessengerInfo.setUserIdKey(userIdKey);
        }
        if (!"".equalsIgnoreCase(userName)) {
            MessengerInfo.setUsername(getActivity(), getArguments().getString("userName"));
        }
        if (!"".equalsIgnoreCase(userId)) {
            MessengerInfo.setUserId(getActivity(), getArguments().getString("userId"));
        }
        if (!"".equalsIgnoreCase(userPassword)) {
            MessengerInfo.setUserPw(getActivity(), getArguments().getString("userPassword"));
        }
        return view;
    }

    public void setStart() {

        Log.d(TAG, "setStart()");

        appbar_layout = (LinearLayout) view.findViewById(R.id.appbar_layout);

        mFragment_Main_Context = getContext();
        mUserID = MessengerInfo.getUserId(getContext());
        LogTrace.E(TAG + "onCreate");

        LocalDB.DB_NAME = mUserID + ".db";


        mMainPresenter = new MainPresenter(this);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.ncontainer);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(0);

        setOnClickEvent();
        view.findViewById(R.id.imgb_top1).setSelected(true);
        view.findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);

        tv_top_category = (TextView) view.findViewById(R.id.tv_main_top_category);
        tv_main_top_sub_category = (TextView) view.findViewById(R.id.tv_main_top_sub_category);

        initDB();
        initMain();

        fragmentSlideMenu_main = new FragmentSlideMenu_Main();

        if (Fragment_Main.getInstance() != null && Fragment_Main.getInstance().isLocked && !"".equals(MessengerInfo.getLockPassword(Fragment_Main.getInstance().getActivity()))) {
            Fragment_Main.getInstance().isLocked = false;
            Intent intent = new Intent(getActivity(), ActivityLockCheck.class);
            intent.putExtra("relaseMode", false);
            getActivity().startActivity(intent);
        } else {
            MessengerInfo.setLockPassowrd(getActivity(), "");
        }
    }

    private void initDB() {

        mChatDataDb = LocalDB.getChatDataDbHelper(getContext());
        mChatListDb = LocalDB.getChatListDbHelper(getContext());
        mUsersReadDb = LocalDB.getUsersReadDbHelper(getContext());
        mUsersDb = LocalDB.getUsersDbHelper(getContext());
    }

    private void initMain() {
        Log.d(TAG, "initMain()");
        MOAClient.getInstance().addCommonListener(this);
        MOAClient.getInstance().addEventListener(this);
        contactData();
        afterLogin(BUSMOAData.getInstantce().getmMOAData());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Log.e(TAG, "onDestroy");
        BusProvider.getInstance().unregister(this);

        //MOAClient를 해제한다.
        try {
            MOAClient.getInstance().logout();
            MOAClient.getInstance().removeCommonListener(this);
            MOAClient.getInstance().removeEventListener(this);

            GreenDaoManager.getInstance().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        //Log.e(TAG, "setOnClickEvent");

        view.findViewById(R.id.imgb_top1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
                allBtnSelectedFalse();
                view.findViewById(R.id.imgb_top1).setSelected(true);
                allTabBarSelectedFalse();
                view.findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
                tv_top_category.setText(getString(R.string.titleTimeLine));
                tv_main_top_sub_category.setText("");
                view.findViewById(R.id.imgb_make).setVisibility(View.VISIBLE);

            }
        });
        view.findViewById(R.id.imgb_top2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
                allBtnSelectedFalse();
                view.findViewById(R.id.imgb_top2).setSelected(true);
                allTabBarSelectedFalse();
                view.findViewById(R.id.tab_bar2).setVisibility(View.VISIBLE);
                tv_top_category.setText(getString(R.string.titleFriend));
                tv_main_top_sub_category.setText("");
                view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
            }
        });
        view.findViewById(R.id.imgb_top3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(2);
                allBtnSelectedFalse();
                view.findViewById(R.id.imgb_top3).setSelected(true);
                allTabBarSelectedFalse();
                view.findViewById(R.id.tab_bar3).setVisibility(View.VISIBLE);
                tv_top_category.setText(getString(R.string.titleChat));
                tv_main_top_sub_category.setText("");
                view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
            }
        });
        view.findViewById(R.id.imgb_top4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(3);
                allBtnSelectedFalse();
                view.findViewById(R.id.imgb_top4).setSelected(true);
                allTabBarSelectedFalse();
                view.findViewById(R.id.tab_bar4).setVisibility(View.VISIBLE);
                tv_top_category.setText(getString(R.string.titleSettings));
                tv_main_top_sub_category.setText("");
                view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
            }
        });

        //새로운 알림 화면
        view.findViewById(R.id.imgb_new_alram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (moimCount > 0) {
                    Intent mIntent = new Intent(getActivity(), Activity_AlramList.class);
                    startActivity(mIntent);
                } else {
                    Toast.makeText(getContext(), getString(R.string.toastNoNewAlarm), Toast.LENGTH_SHORT).show();
                }

            }
        });

        //모임만들기에서 수정됨
        //모임글쓰기 모임선택화면으로 이동한다
        view.findViewById(R.id.imgb_make).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모임개설
                SharedObject.setProperty_string(getActivity(), Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_MAIN_WRITE);
                Intent intent = new Intent(getActivity(), Activity_SelectMoim.class);
                startActivity(intent);
                //getActivity().overridePendingTransition(R.anim.anim_slide_in_bottom, R.anim.anim_slide_out_bottom);

            }
        });
//        mViewPager.setPageTransformer();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               /* if (position == 0) {
                    menuChange(true);
                } else {
                    menuChange(false);
                }*/
                switch (position) {
                    case 0:
                        //menuChange(true);
                        menuChange(false);
                        break;
                    case 1:
                        menuChange(false);
                        break;
                    case 2:
                        menuChange(false);
                        break;
                    case 3:
                        menuChange(false);
                        break;
                    case 4:
                        menuChange(false);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {
               /* if (position == 0) {
                    //menuChange(true);
                    //mViewPager.setCurrentItem(1);  //TODO 0번째 증권 차트 막아둠.
                    mViewPager.setCurrentItem(1,false);
                } else {
                    menuChange(false);
                }*/
                switch (position) {
                    /*case 0:
                        //menuChange(true);
                        break;*/
                    case 0:
                        menuChange(false);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top1).setSelected(true);
                        allBtnSelectedFalse();
                        allTabBarSelectedFalse();
                        view.findViewById(R.id.imgb_top1).setSelected(true);
                        view.findViewById(R.id.tab_bar1).setVisibility(View.VISIBLE);
                        tv_top_category.setText(getString(R.string.titleTimeLine));
                        tv_main_top_sub_category.setText("");
                        view.findViewById(R.id.imgb_make).setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        menuChange(false);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top2).setSelected(true);
                        allBtnSelectedFalse();
                        allTabBarSelectedFalse();
                        view.findViewById(R.id.imgb_top2).setSelected(true);
                        tv_top_category.setText(getString(R.string.titleFriend));
                        tv_main_top_sub_category.setText("");
                        view.findViewById(R.id.tab_bar2).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
                        break;
                    case 2:
                        menuChange(false);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top3).setSelected(true);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top3).setSelected(true);
                        allTabBarSelectedFalse();
                        tv_top_category.setText(getString(R.string.titleChat));
                        tv_main_top_sub_category.setText("");
                        view.findViewById(R.id.tab_bar3).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
                        break;
                    case 3:
                        menuChange(false);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top4).setSelected(true);
                        allBtnSelectedFalse();
                        view.findViewById(R.id.imgb_top4).setSelected(true);
                        allTabBarSelectedFalse();
                        tv_top_category.setText(getString(R.string.titleSettings));
                        tv_main_top_sub_category.setText("");
                        view.findViewById(R.id.tab_bar4).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.imgb_make).setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    /**
     * 메인 상단 모든 선택 해제
     */
    private void allBtnSelectedFalse() {

        view.findViewById(R.id.imgb_top1).setSelected(false);
        view.findViewById(R.id.imgb_top2).setSelected(false);
        view.findViewById(R.id.imgb_top3).setSelected(false);
        view.findViewById(R.id.imgb_top4).setSelected(false);
    }

    private void allTabBarSelectedFalse() {
        view.findViewById(R.id.tab_bar1).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.tab_bar2).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.tab_bar3).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.tab_bar4).setVisibility(View.INVISIBLE);
    }

    private void contactData() {
        LocalDB.getPhoneDbHelper(getContext()).updateFlagAll();

        LocalContactData.getContactList(getContext());

        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(getContext()));

            if (SharedObject.getProperty_boolean(getContext(), "firstLogin", true)) {
                body.put("first", "1");
            } else {
                body.put("first", "0");
            }

            body.put("params", LocalContactData.loadContactData(getContext(), SharedObject.getProperty_boolean(getContext(), "firstLogin", true)));

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_UPLOAD, body);

        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedObject.setProperty_boolean(getContext(), "firstLogin", false);
    }

    public void customercontactData(int reqType, ArrayList<HanaMember> buddyList) {

        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(getContext()));
            body.put("type", reqType);
            if (SharedObject.getProperty_boolean(getContext(), "firstLogin", true)) {
                body.put("first", "1");
            } else {
                body.put("first", "0");
            }

            body.put("params", HanaMember.loadCustomerContactData(buddyList));

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, 0x00020016, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteContact() {
        ArrayList<String> deleteList = LocalDB.getPhoneDbHelper(getContext()).getDeleteList();

        JSONArray array = new JSONArray();

        for (String s : deleteList) {
            array.put(s);
        }

        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(getContext()));
            body.put("params", array);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_DELETE, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFriends() {
        try {
            PacketBody body = new PacketBody();
            body.put("userId", MessengerInfo.getUserId(getContext()));
            body.put("mode", "");

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_LIST, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void afterLogin(final MOAData data) {
        LBJSONObject params = data.body.getJson("params");
        ErrorController.showMessage("[onMOALogin] result : " + data.body.toString());
        LogTrace.E("[afterLogin] result : " + data.body.toString());
        MOALog.e("userName=" + params.get("userName"));

        //TODO result 에서 약관 동의 판단 해서 로그아웃 처리.
        String result = data.body.get("result").toString();

        if (result.equals("success")) {
            //내 정보를 내부 DB에 저장
            MessengerInfo.setUserId(getContext(), (String) params.get("userId"));
            MessengerInfo.setUsername(getContext(), (String) params.get("userName"));
            MessengerInfo.setRealUserName(getContext(), (String) params.get("realUserName"));
            MessengerInfo.setUserPhoneNumber(getContext(), (String) params.get("tel"));
            MessengerInfo.setClearBuddy(getContext(), (String) params.get("clear_buddy"));
            MessengerInfo.setProfileImage(getContext(), (String) params.get("profile_image"));

            ErrorController.showMessage("Frag_main : LOGIN User Phone : " + MessengerInfo.getUserPhoneNumber(getContext()));

            try {
                PacketBody body = new PacketBody();
                JSONObject value = new JSONObject();
                value.put("userId", mUserID);
                body.put("params", value);
                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_SYSTEM, PacketTypes.PTC_IMS_SYSTEM_NORECV_DATA, body);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if ("5".equals(MessengerInfo.getMode(getContext())) || "6".equals(MessengerInfo.getMode(getContext()))) {
                setMode("5");
            }

            //TODO    leeyunsu
         /*   GroupListRequest();
            FriendListRequest();
            setChatTab();
            SetDataRequest();*/
            //afterLoginRequest();

            //setChatTab();
            chatroomReadCountRequest();

            if (MessengerInfo.getPushData(getContext()) != null) {
                final String pushData = MessengerInfo.getPushData(getContext());
                String stuPush = SharedObject.getProperty_string(getActivity(), "moim_push", "");
                Log.d(TAG, "pushData : " + pushData + " / stuPush : " + stuPush);
                if (!"".equals(pushData) && "pushCheck".equals(stuPush)) {
                    LoadingManager.with(getContext()).showLoadingDialog();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            SharedObject.setProperty_string(getActivity(), "moim_push", "");
                            JSONObject object = null;
                            try {
                                object = new JSONObject(pushData);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String chatId = null;
                            String roomId = null;
                            String senderId = null;

                            try {
                                chatId = object.getString("chatId");
                                roomId = object.getString("roomId");
                                senderId = object.getString("senderId");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            LogTrace.E("setPushData chatId: " + chatId + " roomId: " + roomId + " senderId: " + senderId);

                            try {
                                VoChatList chatRoomInfo = LocalDB.getChatListDbHelper(getContext()).getChatData(roomId);
                                List<VoFriendList> friends = new ArrayList<>();
                                List<VoChatData> chats = new ArrayList<>();

                                List<VoUsers> fList = LocalDB.getUsersDbHelper(getContext()).getUserList(roomId);

                                for (VoUsers user : fList) {
                                    VoFriendList friend = new VoFriendList();
                                    friend.setUserId(user.getUserId());
                                    friend.setUserName(user.getUserName());

                                    friends.add(friend);
                                }

                                chatRoomInfo.setFriends(friends, getActivity());
                                chatRoomInfo.setChats(chats);

                                Statics.ROOMINFO = chatRoomInfo;

                                roomId = Statics.ROOMINFO.getRoomId();
                                LogTrace.E("roomId : " + roomId);

                                if ("".equals(roomId))
                                    return;

                                PacketBody body = new PacketBody();
                                JSONObject value = new JSONObject();
                                value.put("userId", mUserID);
                                value.put("roomId", roomId);
                                value.put("room_type", Statics.ROOMINFO.getRoom_type());
                                value.put("lastcid", "");

                                Log.d(TAG, "value = " + value.toString());
                                body.put("params", value);
                                MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_LIST, body);

                                LogTrace.E("push intent");
                                Intent mIntent = new Intent(getActivity(), Activity_ChatRoom.class);
//                                mIntent.putExtra("pushData", MessengerInfo.getPushData(getContext()));
                                mIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(mIntent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            LoadingManager.with(getContext()).hideLoadingDialog();
                        }
                    }, 1000);
                }
            }
        }
    }

    /**
     * 로그인이후 서버 요청 모음
     * 20180330 leeyunsu
     */
    /*public void afterLoginRequest() {
        MainRequestAsyncTask asyncTask = new MainRequestAsyncTask();
        asyncTask.execute();
        LogTrace.E("Fragment_Main  afterLoginRequest");

    }*/

    /**
     * 로그인이후
     * 읽은카운터
     * 그릅리스트 , 친구리스트
     * 탭정보, 사용자 데이터 요청 등을 한꺼번에 AsyncTask로 처리
     * 20180330 leeyunsu
     */
    /*public class MainRequestAsyncTask extends android.os.AsyncTask<String, Integer, String> {

        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            chatroomReadCountRequest();
           // GroupListRequest(); //그룹리스트  Activity_GroupList
            //FriendListRequest(); //친구리스트
            //setChatTab(); //채팅리스트,채팅내용.
            //SetDataRequest();   //개인정보  Fragment_MoimTab4_Setup
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }*/

    /**
     * 그룹 목록 요청
     */
    public void GroupListRequest() {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getContext()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_LIST, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 친구 목록 요청
     */
    public void FriendListRequest() {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", mUserID);
            value.put("mode", ""); //모든 친구
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_LIST, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetDataRequest() {
        Moa.UserInfo(getActivity(), mMainActvtHandler);
    }

    /**
     * 채팅데이터삭제
     * 채팅방리스트요청
     * 대화리스트가져오기
     * 하이퍼링크 데이터요청
     */
    public void setChatTab() {
        LocalDB.getDbHelper(getContext()).deleteTotalChatData();   //채팅 데이터 삭제
//        LocalDB.getBuddyDbHelper(getContext())


        //채팅방 리스트요청
        ContactUploadHelper.requestChatList(new ContactUploadHelper.OnCompleteListener() {
            @Override
            public void onComplete(boolean result) {
                if (result)
                    LogTrace.E("chat list complete");
            }
        }, getContext());
        //대화리스트
        GetCurrentChatList();
        //대화하이퍼링크 데이터요청
        //MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_LINK, PacketTypes.PTC_IMS_LINK_DATA_LIST, new PacketBody());
    }

    /**
     * 친구데이터삭제
     * 친구리스트요청
     */
    public void setFriendTab() {
        LogTrace.E("friend db data delete");
        LocalDB.getDbHelper(getContext()).deleteFriendData();   //친구 데이터 삭제

//        SharedObject.setProperty_boolean(getContext(), "firstLogin", true);
        //친구리스트
        contactData();
    }

    @Subscribe
    public void BusEvent(FragmentEventHelper event) {
        if ("GetCurrentChatList".equals(event.getEvent())) {
            GetCurrentChatList();
        } else if ("setOnChatRoomListener".equals(event.getEvent())) {
            setOnChatRoomListener(event.getRoomListener());
        } else if ("setMainTotalCount".equals(event.getEvent())) {
            setMainTotalCount();
        } else if ("chatListUpdate".equals(event.getEvent())) {
            chatListUpdate();
        } else if ("setOnChatListListener".equals(event.getEvent())) {
            setOnChatListListener(event.getListListener());
        } else if ("FriendListRequest".equals(event.getEvent())) {
            FriendListRequest();
        }
    }

    public void GetCurrentChatList() {
        Log.d(TAG, "GetCurrentChatList");
        try {
            if (chatRoomListener == null) {
                LogTrace.E("chatRoomListener null");
                return;
            }

            if (Statics.ROOMINFO == null) {
                LogTrace.E("Statics.ROOMINFO null");
                return;
            }

            String roomId = Statics.ROOMINFO.getRoomId();
            LogTrace.E("roomId : " + roomId);

            if ("".equals(roomId))
                return;

            String lastReadCid = Statics.ROOMINFO.getLast_read_cid();
            String roomType = Statics.ROOMINFO.getRoom_type();

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", mUserID);
            value.put("roomId", roomId);
            value.put("room_type", roomType);
            value.put("lastcid", lastReadCid);
            value.put("mode", "2");
            value.put("covl", Statics.ROOMINFO.getUnread());

            Log.d(TAG, "value = " + value.toString());
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, PacketTypes.PTC_IMS_CHAT_LIST, body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMode(String value) {

        if ("1".equals(value)) {
            if ("1".equals(MessengerInfo.getMode(getContext()))) {
                setShareData(MessengerInfo.getData(getContext()));
                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setPushData(getContext(), "");
            }
        } else if ("2".equals(value)) {
            if ("2".equals(MessengerInfo.getMode(getContext()))) {
                Intent intent = new Intent(getContext(), Activity_Delivery2.class);
                intent.putExtra("type", "2");
                startActivity(intent);
                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setPushData(getContext(), "");
            } else if ("3".equals(MessengerInfo.getMode(getContext()))) {
                Intent intent = new Intent(getContext(), Activity_Delivery2.class);
                intent.putExtra("type", "3");
                startActivity(intent);
                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setPushData(getContext(), "");
            }
        } else if ("4".equals(value)) {
            if ("4".equals(MessengerInfo.getMode(getContext()))) {
                LogTrace.E("setMode : " + value);
//                setPushData(MessengerInfo.getPushData(getContext()));
                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setPushData(getContext(), "");
            }
        } else if ("5".equals(value)) {
            LogTrace.E("get Share");
            if ("5".equals(MessengerInfo.getMode(getContext()))) {
                LogTrace.E("getMode 5");
                setShareData(MessengerInfo.getData(getContext()));

                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setData(getContext(), "");
            } else if ("6".equals(MessengerInfo.getMode(getContext()))) {
                LogTrace.E("getMode 6");
                setShareData(MessengerInfo.getData(getContext()));

                MessengerInfo.setMode(getContext(), "");
                MessengerInfo.setData(getContext(), "");
            }
        }/* else if ("7".equals(value)) {
            Intent mIntent = new Intent(this, Activity_OpenProfile.class);
            mIntent.putExtra("shortId", MessengerInfo.getData(this));
            startActivity(mIntent);

            MessengerInfo.setMode(WAVEMainActivity.this, "");
            MessengerInfo.setData(WAVEMainActivity.this, "");
        }*/
    }

    public void getRoomInfo(MOAData data) {
        Log.d(TAG, "getRoomInfo");
        String result = data.body.getString("result");
        String roomId = data.body.getString("roomId");

        if ("success".equals(result)) {
            ArrayList<VoChatList> chatList = new ArrayList<>();
            ArrayList<VoUsers> userList = new ArrayList<>();
            ArrayList<VoUserRead> userReadList = new ArrayList<>();
            LBJSONObject parmas = data.body.getJson("params");

            LogTrace.E("room info : " + parmas.toString());

            VoChatList info = VoChatList.loadFromJson(parmas);
            info.setRoomId(roomId);

            chatList.add(info);
            LBJSONArray users = data.body.getArray("users");
            LogTrace.E("room user info : " + users.toString());

            for (int i = 0; i < users.size(); i++) {
                LBJSONObject userdata = (LBJSONObject) users.get(i);
                VoUsers item = VoUsers.loadFromJsonUsers(userdata);
                item.setRoomId(roomId);
                userList.add(item);
            }

            LBJSONArray userReadData = data.body.getArray("readdata");

            for (int i = 0; i < userReadData.size(); i++) {
                LBJSONObject userdata = (LBJSONObject) userReadData.get(i);
                VoUserRead item = VoUserRead.loadFromJson(userdata);
                item.setRoomId(roomId);
                userReadList.add(item);
            }

            mChatListDb.checkChatList(chatList);
            mUsersReadDb.addUsersList(userReadList, roomId);
            mUsersDb.addUsersList(userList, roomId);

            if (chatRoomListener != null) {
                chatRoomListener.onChatListUpdate(roomId);
            }
            if (chatListListener != null) {
                chatListListener.onChatListUpdate(roomId);
            }

            //모임 대화방 방송중 방송메뉴 Visible
            if (info.getRoom_type().equals("7")) {
                if (!info.getRoomStatus().equals("") && info.loadFromRoomStatusJson("value").equals("1")) {
                    Activity_ChatRoom.getInstance().audioOnOff("On");
//                    FriendListRequest();
                }
            }
        }
    }

    private void uptNotification(MOAData data) {

        LogTrace.E("main useNoti update");
        LBJSONObject item = data.body.getJson("params");
        String roomId = item.getString("roomId");
        String useNoti = item.getString("useNoti");
        LogTrace.E("main useNoti update : " + useNoti);

        if (Statics.ROOMINFO != null) {
            Statics.ROOMINFO.setUseNoti(useNoti);
        }

        mChatListDb.updateNoti(roomId, useNoti);
    }

    private void useNotification(MOAData data) {

        if ("success".equals(data.body.getString("result"))) {

            LBJSONObject noti = data.body.getJson("params");
            String useNoti = noti.getString("useNoti");

            if ("1".equals(useNoti))
                MessengerInfo.setReceiveAlarm(getContext(), true);
            else
                MessengerInfo.setReceiveAlarm(getContext(), false);
        }
    }

    private void chatListUser(final MOAData data) {
        ErrorController.showMessage("## chatListUser");

        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                String roomId = data.body.getString("roomId");
                LBJSONArray users = data.body.getArray("params");
                ArrayList<VoUsers> userList = new ArrayList<>();

                ErrorController.showMessage("## chatListUser doInBackground roomId: " + roomId + " users.size(): " + users.size());

                for (int i = 0; i < users.size(); i++) {
                    LBJSONObject userdata = (LBJSONObject) users.get(i);
                    VoUsers item = VoUsers.loadFromJsonUsers(userdata);
                    item.setRoomId(roomId);
                    userList.add(item);
                }
                mUsersDb.addUsersList(userList, roomId);

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                ErrorController.showMessage("## chatListUser onPostExecute");

                if (chatListListener != null) {
                    chatListListener.onChatListResult(true);
                }
            }
        }.execute(null, null, null);

    }

    private void setShareData(String data) {

        JSONObject object;

        try {

            object = new JSONObject(data);

            LogTrace.E("share data : " + object.toString());

            String msgType = object.getString("msgType");
            String msg = object.getString("msg");
            String url = object.getString("url");

            LogTrace.E("msg type : " + msgType);

            Bundle bundle = new Bundle();

            if ("text/plain".equals(msgType)) {

                Intent intent = new Intent(getContext(), Activity_Delivery.class);

                bundle.putString("messageType", "0");
                bundle.putString("message", msg);
                bundle.putString("attachment", "");

                intent.putExtras(bundle);

                startActivity(intent);
            } else if (msgType.contains("image/")) {
                LogTrace.E("image share");
                Uri uri = Uri.parse(url);
                new ImageUploadData().chatFileUploadReceive(getContext(), uri);
            }

        } catch (Exception e) {
            LogTrace.E("share exception");
            e.printStackTrace();
        }
    }

    /**
     * 푸시 데이터 받아서 처리한다.
     * type: moim  새글, 댓글 => 모임 상세글조회 화면으로 이동.
     * type: chat 채팅화면으로 이동.
     *
     * @param pushData
     */
    public void setPushData(String pushData) {
        Log.e(TAG, "setPushData");

        String moimId = null;
        String msgId = null;
        String type = null;

//        String data = intent.getStringExtra("pushmsg");
//        if(data==null){
//            return;
//        }
        String data = pushData;
        if (data == "") {
            return;
        }

        JSONObject object = null;
        JSONObject object2;
        JSONObject object3;
        try {
            object = new JSONObject(data);
            type = object.getString("type");
            LogTrace.E("setPushData type: " + type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*SharedObject.setProperty_string(getActivity(), Constants.MOIM_ID, moimId);
        // 모임타임라인으로 가기는 할필요없음.
        Intent intentDetail2 = new Intent(getActivity(), Activity_MoimTimeLineList.class);
        getActivity().startActivity(intentDetail2);*/

        //글조회 화면으로가기 새글,댓글
        if ("moim".equals(type)) {
            try {
                String attachment = object.getString("attachment");
                object2 = new JSONObject(attachment);
                object3 = new JSONObject(object2.getString("link_data"));
                moimId = object3.getString("moimId");
                msgId = object3.getString("msgId");

                LogTrace.E("setPushData attachment: " + attachment.toString());
                LogTrace.E("setPushData moimId: " + moimId + " msgId: " + msgId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SharedObject.setProperty_string(getActivity(), Constants.MOIM_ID, moimId);

            if (msgId != null) {//글상세화면
                SharedObject.setProperty_string(getActivity(), Constants.MOIM_MSGID, msgId);
                SharedObject.setProperty_boolean(getActivity(), Constants.MOIM_REPLY, false);

                Intent intentDetail = new Intent(getActivity(), Activity_TimeLineDetail.class);
                getActivity().startActivity(intentDetail);

            } else {  //타임라인
                Intent intentDetail = new Intent(getActivity(), Activity_MoimTimeLineList.class);
                getActivity().startActivity(intentDetail);
            }
        } else if ("chat".equals(type)) {  //채팅으로 보내는 부분

            String chatId = null;
            String roomId = null;
            String senderId = null;

            try {
                chatId = object.getString("chatId");
                roomId = object.getString("roomId");
                senderId = object.getString("senderId");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            LogTrace.E("setPushData chatId: " + chatId + " roomId: " + roomId + " senderId: " + senderId);

            try {
                VoChatList chatRoomInfo = LocalDB.getChatListDbHelper(getContext()).getChatData(roomId);
                List<VoFriendList> friends = new ArrayList<>();
                List<VoChatData> chats = new ArrayList<>();

                List<VoUsers> fList = LocalDB.getUsersDbHelper(getContext()).getUserList(roomId);

                for (VoUsers user : fList) {
                    VoFriendList friend = new VoFriendList();
                    friend.setUserId(user.getUserId());
                    friend.setUserName(user.getUserName());

                    friends.add(friend);
                }

                chatRoomInfo.setFriends(friends, getActivity());
                chatRoomInfo.setChats(chats);

                Statics.ROOMINFO = chatRoomInfo;

                LogTrace.E("push intent");
                Fragment_Main.getInstance().GetCurrentChatList();

                Intent mIntent = new Intent(getActivity(), Activity_ChatRoom.class);
//                mIntent.putExtra("pushData", MessengerInfo.getPushData(getContext()));
                mIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                startActivity(mIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else { //가입
            {
                try {

                    String attachment = object.getString("attachment");
                    object2 = new JSONObject(attachment);
                    object3 = new JSONObject(object2.getString("link_data"));
                    moimId = object3.getString("moimId");

                    LogTrace.E("setPushData attachment: " + attachment.toString());
                    LogTrace.E("setPushData moimId: " + moimId + " msgId: " + msgId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                SharedObject.setProperty_string(getActivity(), Constants.MOIM_ID, moimId);

                Intent intentDetail = new Intent(getActivity(), Activity_MoimTimeLineList.class);
                getActivity().startActivity(intentDetail);

            }

        }

        MessengerInfo.setPushData(getContext(), "");
        MessengerInfo.setPushStart(getContext(), "");
    }

    public void setChatCount(int count) {
        if (count > 0) {
            ((TextView) view.findViewById(tv_msg_unread_count)).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(tv_msg_unread_count)).setText(String.valueOf(count));
        } else
            ((TextView) view.findViewById(tv_msg_unread_count)).setVisibility(View.GONE);
    }

    public void setMainTotalCount() {

//        ChatListDbHelper db = LocalDB.getChatListDbHelper(getContext());
//        int totalCount = db.chatRoomNoReadTotalCount();
        // 채팅 미확인 카운트 조회
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", mUserID);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_TOTAL_UNREAD, body);

            LogTrace.E("setMainTotalCount, afterLogin PTC_IMS_CHATROOM_TOTAL_UNREAD " + getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        setChatCount(totalCount);
//        setBadge(totalCount + moimCount);
    }

    private void setBadge(int count) {
        new Badge(getContext()).setBadge(count);
    }

    public void setFriendCount(String friendCount) {
        //ll_frag_main_topbar.setCount(friendCount);
    }

    private void chatRoomUpdate(MOAData data) {

        String roomId = data.body.getString("roomId");
        ArrayList<VoChatList> chatList = new ArrayList<>();
        ArrayList<VoUsers> userList = new ArrayList<>();
        ArrayList<VoUserRead> userReadList = new ArrayList<>();
        LBJSONObject parmas = data.body.getJson("params");

        VoChatList info = VoChatList.loadFromJson(parmas);
        info.setRoomId(roomId);

        chatList.add(info);

        LBJSONArray users = data.body.getArray("users");

        for (int i = 0; i < users.size(); i++) {
            LBJSONObject userdata = (LBJSONObject) users.get(i);
            VoUsers item = VoUsers.loadFromJsonUsers(userdata);
            item.setRoomId(roomId);
            userList.add(item);
        }

        LBJSONArray userReadData = data.body.getArray("readdata");

        for (int i = 0; i < userReadData.size(); i++) {
            LBJSONObject userdata = (LBJSONObject) userReadData.get(i);
            VoUserRead item = VoUserRead.loadFromJson(userdata);
            item.setRoomId(roomId);
            userReadList.add(item);
        }

        mChatListDb.checkChatList(chatList);
        mUsersReadDb.addUsersList(userReadList, roomId);
        mUsersDb.addUsersList(userList, roomId);

        if (chatRoomListener != null) {
            chatRoomListener.onChatListUpdate(roomId);
        }
        if (chatListListener != null) {
            LogTrace.E("chatRoomUpdate");
            chatListListener.onChatListUpdate(roomId);
        }
    }

    private void chatMyUnread(MOAData data) {

        LBJSONObject jo1 = data.body.getJson("params");
        String roomId = jo1.getString("roomId");
        String unCount = jo1.getString("unread_count");
        int unread = Integer.valueOf(jo1.getString("total_unread_count"));

        setBadge(unread);
        mChatListDb.updateUnread(roomId, unCount);

        if (chatListListener != null)
            chatListListener.onChatReadMy(data);

        if (chatRoomListener != null)
            chatRoomListener.onChatReadMy(data);
    }

    public void chatListUpdate() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                mWAVEMainPager.setCurrentItem(1);
//                setFragmentChatRmList();
                mViewPager.setCurrentItem(2);
            }
        }, 500);
    }

    private void chatReceive(MOAData data) {

        ErrorController.writeLog("chatReceive Start");

        String roomId = null;
        VoChatData item = null;

        try {
            roomId = data.body.getString("roomId");
            String chatId = data.body.getString("chatId");
            String messageType = data.body.getString("messageType");
            String chatType = data.body.getString("chatType");
            String senderId = data.body.getString("senderId");
            String regDate = data.body.getString("regDate");

            boolean isSuccess = true;
            String roomType = "0";
            VoChatList chatRoomInfo = LocalDB.getChatListDbHelper(getContext()).getChatData(roomId);
            item = mChatDataDb.addChatDataRecive(data, roomId, chatRoomInfo.getRoom_type());


            if (item == null) {

                ErrorController.writeLog("item == null");
                isSuccess = false;
            } else {

                VoChatList chatListData = new VoChatList();
                chatListData.setRoomId(roomId);
                chatListData.setTitle(item.getTitle());
                chatListData.setLast_msg_date(item.getReg_date());

                if (!senderId.equals(MessengerInfo.getUserId(getContext()))) {
                    chatListData.setUnread(String.valueOf(mChatListDb.chatRoomNoReadCount(roomId) + 1));
                } else {
                    chatListData.setUnread(String.valueOf(mChatListDb.chatRoomNoReadCount(roomId)));
                }

                if ("2".equals(chatType)) {

                    String name = "";

                    try {
                        JSONObject object = new JSONObject(item.getAttachment());
                        JSONArray array = object.getJSONArray("user");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jo = array.getJSONObject(0);
                            name = jo.getString("userId") + ", ";
                        }

                        name = name.substring(0, name.length() - 2);
                        mUsersDb.deleteUser(roomId, name);
                        mUsersReadDb.deleteUser(roomId, name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if ("4".equals(chatType)) {
                    if ("55".equals(messageType)) {
                        JSONObject attachment = new JSONObject(data.body.getString("attachment"));
                        String code = attachment.getString("code");
                        String value = attachment.getString("value");

                        if ("voice".equals(code)) {
                            switch (value) {
                                case "0":
                                    Activity_ChatRoom.getInstance().audioOnOff("Off");
                                    break;
                                case "1":
                                    Activity_ChatRoom.getInstance().audioOnOff("On");
                                    break;
                            }
                        }
                    }
                }

                VoChatRoom voChatRoom = mChatListDb.getChatRoom(roomId);

                if (voChatRoom != null) {

                    LogTrace.E("chat receive room not null");

                    if (voChatRoom.isHidden())
                        mChatListDb.updateHidden(roomId, "0");

                    if ("0".equals(chatType) || "4".equals(chatType))
                        mChatListDb.updateMsg(chatListData);


                    setMainTotalCount();
                    roomType = voChatRoom.getRoomType();
                } else {
                    LogTrace.E("chat receive room null");
                    ChatListPresenter chatListPresenter = new ChatListPresenter(this);
                    chatListPresenter.chatRoomInfo(getContext(), roomId, "");
                }
            }

            ErrorController.writeLog("messageType::" + messageType, "isSuccess::" + isSuccess);


            /*if (isSuccess) {

                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA).parse(regDate);

                if (LuckyBagModel.checkLuckyBag(getContext(), chatId, senderId, messageType, roomType, date)) {

                    if (chatRoomListener != null) {
                        chatRoomListener.receiveMsg(roomId, item);
                        ErrorController.writeLog("chatRoomListener refresh");
                    }
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (chatRoomListener != null && !TextUtils.isEmpty(roomId) && item != null) {

                chatRoomListener.receiveMsg(roomId, item);
                ErrorController.writeLog("finally", "chatRoomListener refresh");
            }

            if (chatListListener != null && !TextUtils.isEmpty(roomId)) {

                chatListListener.receiveMsg(roomId, null);
                ErrorController.writeLog("finally", "chatListListener refresh");
            }
        }
    }

    private void linkData(MOAData data) {
        LogTrace.E("링크 데이터 : " + data);
        mVoLinkDataList = new ArrayList<>();
        LBJSONArray users = data.body.getArray("params");

        for (int i = 0; i < users.size(); i++) {
            LBJSONObject item = (LBJSONObject) users.get(i);
            VoLinkData linkData = new VoLinkData();
            linkData.setLinkid(item.getString("linkid"));
            linkData.setLink_tag(item.getString("link_tag"));
            linkData.setLink_type(item.getString("link_type"));
            linkData.setLink_action(item.getString("link_action"));
            mVoLinkDataList.add(linkData);
        }

        setMode("4");
    }

    @Override
    public void showFriendList(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
    }

    @Override
    public void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
    }

    @Override
    public void onHideSuccess(VoFriendList hiddenFriend) {
        //LogTrace.E("WAVEMainActivity onHideSuccess(VoFriendList hiddenFriend)");
    }

    @Override
    public void onBlockSuccess(VoFriendList blockedFriend) {
        //LogTrace.E("WAVEMainActivity onBlockSuccess(VoFriendList hiddenFriend)");
    }

    @Override
    public void receiveMsg(String roomId, VoChatData data) {

    }

    @Override
    public void sendMsg(MOAData data) {

    }

    @Override
    public void exitRoom(boolean result, MOAData data) {

    }

    @Override
    public void createRoom(MOAData data) {

    }

    @Override
    public void chatList(MOAData data) {

    }

    @Override
    public void recRead() {

    }

    @Override
    public void onChatListResult(boolean result) {

    }

    @Override
    public void onChatListUpdate(String roomId) {

    }

    @Override
    public void onChatRoomInfo(MOAData data) {

    }

    @Override
    public void onChatReadMy(MOAData data) {

    }

    @Override
    public void showSearchList(ArrayList<String> groupList, ArrayList<VoChatList> chatList) {

    }

    @Override
    public void onMOALogin(MOAData moaData) {
//        MOAClient.getInstance().removeCommonListener(this);
//        BUSMOAData.getInstantce().setmMOAData(moaData);
//        LogTrace.E("#login success : " + moaData.body.toString());
//
//        if (!"".equals(moaData.body.getJson("params").getString("userIdKey"))) {
//            MessengerInfo.setUserIdKey(moaData.body.getJson("params").getString("userIdKey"));
//        }
//        int reqType;
//        if ("".equals(MessengerInfo.getEmpNo()))
//            reqType = 0;
//        else
//            reqType = 1;

//        //고객리스트, 담당직원 RequestBuddyList 호출
//        MessengerInterface.getInstance().MessengerToHanaRequestBuddyList(reqType);
//
//        String appVersion = com.time.prime.util.Util.getAppVersion(getContext());
//
////        if (appVersion == 330)
////            setFriendTab();
////        else
        contactData();
        afterLogin(BUSMOAData.getInstantce().getmMOAData());
    }

    @Override
    public void onMOALoginError(int i, MOAData moaData) {

    }

    @Override
    public void onMOALogout(int i, MOAData moaData) {
        LogTrace.E("arg0 : " + i + " / " + moaData.body.toString());

        if (i == MOAConsts.LOGOUT_KICKOFF) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    LogTrace.E("doubleLogin");
                    Toast.makeText(getContext(), "다른 기기에서 로그인되었습니다.", Toast.LENGTH_SHORT).show();

                    LocalDB.resetDB();
                    new Badge(getContext()).setBadge(0);
                }
            });
        }
    }

    @Override
    public void onMOAPing() {

    }

    @Override
    public void onMOARegister(MOAData moaData) {

    }

    @Override
    public void onMOAAgreeInfo(MOAData moaData) {

    }

    @Override
    public void onMOAError(Exception e) {
        LogTrace.E(" Fragment_Main onMOAError: " + e);
    }

    @Override
    public void OnEvent(MOAData moaData) {
        //MOALog.e("Fragment_Main OnEvent handleMessage : "+moaData.body.toString());
        mMainActvtHandler.sendMessage(mMainActvtHandler.obtainMessage(moaData.ptc, moaData));
    }


    /**
     * 4.35 알림방 카운트 요청
     * PTG : 0x00040000, PTC : 0x00040039
     * PTG_IMS_CHATROOM, PTC_IMS_CHATROOM_ALARM_COUNT
     */
    public void chatroomReadCountRequest() {
        Moa.chatroomAlarmCount(getActivity(), mMainActvtHandler);
    }

    /**
     * 알림 카운터
     */
    public void chatroomReadCountSubtraction() {
        LogTrace.E("chatroomReadCountSubtraction");

        int readcount = Integer.parseInt(((TextView) view.findViewById(R.id.tv_unread_count)).getText().toString()) - 1;
        if (readcount == 0) {
            view.findViewById(R.id.tv_unread_count).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.tv_unread_count).setVisibility(View.VISIBLE);
            ((TextView) view.findViewById(R.id.tv_unread_count)).setText(readcount + "");
        }
    }

    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
//                SharedUserInfo sharedUserInfo = SharedUserInfo.getInstance(getContext());
                boolean result = false;
                final MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                // Log.e(TAG, "data.ptc  " + data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_LOGIN_LOGIN:  //TODO   로그인시  "no_agree" 체크 강제로그아웃약관처리
                        MOALog.e("@@PTC_IMS_LOGIN_LOGIN ");
                        MOALog.e("result=" + data.body.toString());


                        String agree = data.body.get("result").toString();
                        // if(true){   //test팝업 띄우기
                        if ("no_agree".equals(agree)) {   //팝업 띄우기
                            try {
                                //MOALog.e("@@no_agree ");

                                MOAClient.getInstance().logout();
                                final Dialog_TermsHtml dialogDefault = new Dialog_TermsHtml(getContext());
                                dialogDefault.setCancelEvent(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //Log.e(TAG,"Dialog_TermsHtml Cancel");
                                        dialogDefault.cancel();
                                        //로그아웃처리
                                        MessengerInfo.clear(getActivity());
                                        //Util.getMainActivity().startLogout(false, true);
                                        try {
                                            TranService.releaseService(getActivity());
                                            AlarmReceiver.stopAlarmReceiver(getActivity());
                                        } catch (Exception e) {
                                        }
                                        PendingIntent i = PendingIntent.getActivity(getActivity().getApplicationContext(), 0, new Intent(getActivity().getIntent()), getActivity().getIntent().getFlags());
                                        AlarmManager am2 = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                                        am2.set(AlarmManager.RTC, System.currentTimeMillis(), i);

                                        getActivity().moveTaskToBack(true);
                                        getActivity().finish();
                                        android.os.Process.killProcess(android.os.Process.myPid());

                                    }
                                });
                                dialogDefault.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        break;

                    case 0x00040039: //4.35 알림방 카운트 요청

                        MOALog.e("PTC_IMS_ADDRESS_LIST :");
                        MOALog.e("result=" + data.body.toString());

                        //알림 카운트 수정
                        String unreadCount = data.body.getJson("params").getString("unread");
                        LogTrace.E("alramCount :" + unreadCount);
//                        sharedUserInfo.setAlarmCount(Integer.parseInt(unreadCount));

                        if ("0".equals(unreadCount)) {
                            view.findViewById(R.id.tv_unread_count).setVisibility(View.GONE);
                        } else {
                            view.findViewById(R.id.tv_unread_count).setVisibility(View.VISIBLE);
                            ((TextView) view.findViewById(R.id.tv_unread_count)).setText(unreadCount);
                        }

                        moimCount = Integer.parseInt(unreadCount);
                        // 채팅 미확인 카운트 조회
                        try {

                            PacketBody body = new PacketBody();
                            JSONObject value = new JSONObject();
                            value.put("userId", mUserID);
                            body.put("params", value);
                            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_TOTAL_UNREAD, body);

                            LogTrace.E("afterLogin PTC_IMS_CHATROOM_TOTAL_UNREAD" + getClass());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //((TextView)view.findViewById(R.id.tv_unread_count)).setText(unreadCount);
                        break;
                    //주소록 리스트
                    case 0x00020016:
                        MOALog.e("PTC_IMS_ADDRESS_CUSTOMER_UPLOAD :");
                        MOALog.e("result=" + data.body.toString());
                        if ("success".equals(data.body.get("result"))) {
                            FriendListRequest();
                        }
                        break;
                    //주소록 리스트
                    case PacketTypes.PTC_IMS_ADDRESS_LIST:
                        //위에서 보낸 요청의 콜백
                        Log.e(TAG, "PTC_IMS_ADDRESS_LIST :");
                        Log.e(TAG, "result=" + data.body.toString());

                        BuddyDbHelper db1 = LocalDB.getBuddyDbHelper(getContext());
                        db1.addBuddys(data, getContext());
//                        db1.get

                        mMainPresenter.getGroupList(data);
                        setMode("2");

                        //if (mFragmentFriendList != null)
                        Fragment_ChatTab_FriendList.getInstance().onUpdateFriend();
                        break;

                    //주소록 동기화
                    case PacketTypes.PTC_IMS_ADDRESS_UPLOAD:
                        //내 주소록 이름:전번 업로드함
                        Log.i(TAG, "PTC_IMS_ADDRESS_UPLOAD :");
                        Log.i(TAG, "result=" + data.body.get("result"));
                        ErrorController.showMessage("matching result : " + data.body.toString());

                        //친구 목록 요청
                        if ("success".equals(data.body.get("result"))) {

                            MessengerInfo.setFirstContactUploaded(getContext(), false);

                            // 서버로부터 성공 메시지 받으면 주소록 직렬화
                            LocalContactData.serializeContactList(getContext());
                            ErrorController.showMessage("hoya", "[Fragment_Main.PTC_IMS_ADDRESS_LIST]");

                            PacketBody body = new PacketBody();
                            JSONObject value = new JSONObject();
                            value.put("userId", mUserID);
                            value.put("mode", ""); //모든 친구
                            body.put("params", value);
                            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_LIST, body);
                            LogTrace.E("Jsonobject value : " + value);

                        } else {//실패 시 자동 시도
                            ContactUploadHelper.requestUpload(getActivity(), false, null);
                        }

                        break;

                    case PacketTypes.PTC_IMS_ADDRESS_DELETE:
                        if ("success".equals(data.body.getString("result"))) {
                            LocalDB.getPhoneDbHelper(getContext()).delete();

                            //getFriends();
                        }

                        break;

                    //대화방 목록 요청
                    case PacketTypes.PTC_IMS_CHATROOM_LIST:
                        Log.i(TAG, "PTC_IMS_CHATROOM_LIST :");
                        Log.i(TAG, "chat result=" + data.body.get("result"));

                        LogTrace.E("result : " + data.body.getString("params"));

                        mChatListDb.addChatList(data);
                        Log.d(TAG, "addChatList in DB");

                        //TODO
                        if ("success".equals(data.body.get("result"))) {
                            new ChatRoomListAsyncTask().execute(data.body.get("result").toString()).get();
                            result = true;
                        } else {
                            result = false;
                        }

                        // asyncTask.execute(data.body.get("result").toString());

                      /*  if ("success".equals(data.body.get("result"))) {
                            result = true;

                            ArrayList<VoChatList> list = mChatListDb.getChatList();

                            for (int j = 0; j < list.size(); j++) {
                                for (int i = 0; i < list.get(j).getList().size(); i++) {
                                    if (!list.get(j).getList().get(i).getRoom_type().equals("2") || !list.get(j).getRoom_type().equals("3")) {
                                        mMainPresenter.userList(getActivity(), mUserID, list.get(j).getList().get(i).getRoomId());
                                    }
                                }
                            }
                        } else {

                            result = false;
                        }*/



                        /*if (chatRoomListener != null)
                            chatRoomListener.onChatListResult(result);

                        if (chatListListener != null)
                            chatListListener.onChatListResult(result);*/

                        /**
                         * Push 처리
                         */
                        //TODO 푸시로 들어갔을때와 일반적인 로그인을때의 구분.
                        final String pushData = MessengerInfo.getPushData(getContext());
                        String stuPush = SharedObject.getProperty_string(getActivity(), "moim_push", "");
                        Log.d(TAG, "pushData : " + pushData + " / stuPush : " + stuPush);
                        if (!"".equals(pushData) && "pushCheck".equals(stuPush)) {
                            LoadingManager.with(getContext()).showLoadingDialog();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    setPushData(pushData);
                                    SharedObject.setProperty_string(getActivity(), "moim_push", "");
                                    LoadingManager.with(getContext()).hideLoadingDialog();
                                }
                            }, 1000);
                        }

                        break;
                    //대화방 개설
                    case PacketTypes.PTC_IMS_CHATROOM_CREATE:
                        MOALog.i("PTC_IMS_CHATROOM_CREATE :");
                        MOALog.i("result=" + data.body.get("result"));

                        if (chatRoomListener != null)
                            chatRoomListener.createRoom(data);

                        if (chatListListener != null)
                            chatListListener.createRoom(data);

                        break;

                    //대화방 초대
                    case PacketTypes.PTC_IMS_CHATROOM_INVITE:

                        MOALog.i("PTC_IMS_CHATROOM_INVITE :");
                        MOALog.i("result=" + data.body.get("result"));

                        /**
                         * 기존 종목대화방 유저리스트 받아 오지 않았을때 팝업 처리 주석
                         */
                        /*String invite_success = (String) data.body.get("result");
                        String existUser = (String) data.body.getJson("params").getString("existUser");
                        String blockedUser = (String) data.body.getJson("params").getString("blockedUser");

                        String text = null;

                        if(existUser != "" && blockedUser != ""){
                            text = existUser + "님은 이미 대화에 참여중입니다.\n" + blockedUser + "님은 차단된 사용자입니다." ;
                        }else if(existUser != "" && blockedUser == ""){
                            text = existUser + "님은 이미 대화에 참여중입니다.";
                        }else if(existUser == "" && blockedUser != ""){
                            text =  blockedUser + "님은 차단된 사용자입니다.";
                        }

                        if ("success".equals(invite_success)) {
                            if (existUser != "" || blockedUser != "") {
                                final Dialog_Text dialog = new Dialog_Text(Activity_ChatRoom.getInstance(), text, Constants.DIALOG_BUTTON_TYPE.chat_invite);
                                dialog.setConfirmListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }

                        }*/

                        break;

                    //대화방 퇴장
                    case PacketTypes.PTC_IMS_CHATROOM_EXIT:

                        MOALog.i("PTC_IMS_CHATROOM_EXIT :");
                        MOALog.i("result=" + data.body.get("result"));
                        LogTrace.E("result : " + data.body.get("result"));

                        if ("success".equals(data.body.get("result")))
                            result = true;
                        else
                            result = false;

                        if (chatRoomListener != null)
                            chatRoomListener.exitRoom(result, data);

                        if (chatListListener != null)
                            chatListListener.exitRoom(result, data);

                        break;

                    //대화방 제목 변경
                    case PacketTypes.PTC_IMS_CHATROOM_CHANGE_TITLE:

                        MOALog.i("PTC_IMS_CHATROOM_CHANGE_TITLE :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    //대화방 알림 변경
                    case PacketTypes.PTC_IMS_CHATROOM_NOTI_CONFIG:

                        MOALog.i("PTC_IMS_CHATROOM_NOTI_CONFIG :");
                        MOALog.i("result=" + data.body.get("result"));
                        LogTrace.E("result = " + data.body.get("result"));
                        LogTrace.E("result all = " + data.body.toString());
                        uptNotification(data);
                        break;

                    //대화방 미확인 카운트 조회
                    case PacketTypes.PTC_IMS_CHATROOM_READ_COUNT:

                        MOALog.e("@@PTC_IMS_CHATROOM_READ_COUNT :");
                        MOALog.e("@@result=" + data.body.get("result"));

                        //TODO
                        //알림 카운트 수정
//                        String alramCount = data.body.getJson("params").getString("count");
//                        int moimAlramCount = Integer.parseInt(alramCount);
//                        ((TextView)view.findViewById(tv_msg_unread_count)).setText(moimAlramCount - moimCount);

                        break;

                    //대화방 사용자 리스트 조회
                    case PacketTypes.PTC_IMS_CHATROOM_USERLIST:
                        MOALog.i("PTC_IMS_CHATROOM_USERLIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        LogTrace.E("PacketTypes.PTC_IMS_CHATROOM_USERLIST chatListUser");

                        chatListUser(data);

                        break;

                    //대화방 정보 요청
                    case PacketTypes.PTC_IMS_CHATROOM_INFO:

                        MOALog.i("PTC_IMS_CHATROOM_INFO :");
                        MOALog.i("result=" + data.body.get("result"));

                        LogTrace.E("PTC_IMS_CHATROOM_INFO");

                        getRoomInfo(data);
                        setMainTotalCount();

                        if (chatRoomListener != null)
                            chatRoomListener.onChatRoomInfo(data);

                        if (chatListListener != null)
                            chatListListener.onChatRoomInfo(data);

                        break;

                    //대화방 정보 업데이트 수신
                    case PacketTypes.PTC_IMS_CHATROOM_RECV_UPDATEINFO:

                        MOALog.i("PTC_IMS_CHATROOM_RECV_UPDATEINFO :");
                        LogTrace.E("PTC_IMS_CHATROOM_RECV_UPDATEINFO : " + data.body.toString());

                        chatRoomUpdate(data);
                        setMainTotalCount();
                        break;

                    // 전체 미확인 카운트 ??
                    // 대화방 미확인 카운트
                    case PacketTypes.PTC_IMS_CHATROOM_TOTAL_UNREAD:

                        //안읽은 채팅수 표기
                        Log.d(TAG, "PTC_IMS_CHATROOM_TOTAL_UNREAD :");
                        Log.d(TAG, "result=" + data.body.get("result"));
                        ErrorController.showMessage("[Fragment_Main] handler-unread : " + data.body.toString());
                        String success = (String) data.body.get("result");

                        if ("success".equals(success)) {

                            int count = data.body.getJson("params").getInt("count");

                            Log.d(TAG, "count = " + count);
//                            setChatCount(count - moimCount);
//                            setBadge(count);
                            setChatCount(count);
//                            setBadge(count + sharedUserInfo.getAlaramCount());
                            setBadge(count + moimCount);
                        }

                        break;

                    //대화 리스트 요청
                    case PacketTypes.PTC_IMS_CHAT_LIST:

                        Log.d(TAG, "PTC_IMS_CHAT_LIST :");
                        Log.d(TAG, "result=" + data.body.get("result"));
                        ErrorController.showMessage("[Fragment_Main] PTC_IMS_CHAT_LIST : " + data.body.toString());

                        try {
                            ErrorController.writeLog("PTC_IMS_CHAT_LIST", data.body.toString());
                            Log.d("PTC_IMS_CHAT_LIST", data.body.toString());
                        } catch (Exception ignored) {
                            ErrorController.writeLog("PTC_IMS_CHAT_LIST ERROR", ignored.getMessage());
                        }

                        if (chatRoomListener != null)
                            chatRoomListener.chatList(data);

                        if (chatListListener != null)
                            chatListListener.chatList(data);


                        break;

                    //대화 메세지 발송
                    case PacketTypes.PTC_IMS_CHAT_SEND_MSG:

                        MOALog.i("PTC_IMS_CHAT_SEND_MSG :");
                        MOALog.i("result=" + data.body.get("result"));
                        LogTrace.E("send result : " + data.body.get("result"));

                        if (chatListListener != null)
                            chatListListener.sendMsg(data);

                        if (chatRoomListener != null)
                            chatRoomListener.sendMsg(data);

                        break;

                    //대화 메시지 수신
                    case PacketTypes.PTC_IMS_CHAT_RECV_MSG:

                        MOALog.i("PTC_IMS_CHAT_RECV_READ :");
                        MOALog.i("result=" + data.body.toString());
                        ErrorController.showMessage("[Frag_Main] PTC_IMS_CHAT_RECV_MSG " + data.body.toString());

                        try {
                            ErrorController.writeLog("PTC_IMS_CHAT_RECV_MSG", data.body.toString());
                        } catch (Exception ignored) {
                            ErrorController.writeLog("PTC_IMS_CHAT_RECV_MSG ERROR", ignored.getMessage());
                        }

                        chatReceive(data);

                        break;

                    //대화내용 조회
                    case PacketTypes.PTC_IMS_CHAT_GET_MSG:

                        MOALog.i("PTC_IMS_CHAT_SEND_MSG :");
                        MOALog.i("result=" + data.body.get("result"));
                        Activity_LongTextView.getInstance().onMessage(data);
                        break;

                    //대화 읽음 데이터
                    case PacketTypes.PTC_IMS_CHAT_READ_DATA:

                        MOALog.i("PTC_IMS_CHAT_READ_DATA :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    case PacketTypes.PTC_IMS_CHAT_RECV_READ:

                        MOALog.i("PTC_IMS_CHAT_READ_DATA :");
                        MOALog.i("result=" + data.body.get("result"));

                        mUsersReadDb.addRevUsers(data);

                        if (chatRoomListener != null)
                            chatRoomListener.recRead();

                        if (chatListListener != null)
                            chatListListener.recRead();

                        break;

                    //대화 읽음 처리
                    case PacketTypes.PTC_IMS_CHAT_READ:

                        MOALog.i("PTC_IMS_CHAT_READ :");
                        MOALog.i("result=" + data.body.get("result"));
                        chatMyUnread(data);
                        break;

                    //개인정보 요청
                    case PacketTypes.PTC_IMS_USER_USERINFO:

                        MOALog.i("PTC_IMS_USER_USERINFO :");
                        MOALog.i("result=" + data.body.get("result"));
                        String strResult = (String) data.body.get("result");
                        LogTrace.E("strResult: " + strResult);

                        if (Constants.SUCCESS.equals(strResult)) {

                            SharedObject.setProperty_string(getActivity(), Constants.tel, data.body.getJson("params").getString("tel"));
                            SharedObject.setProperty_string(getActivity(), Constants.UsNm, data.body.getJson("params").getString("userName"));
                            SharedObject.setProperty_string(getActivity(), Constants.pfImg, data.body.getJson("params").getString("profile_image"));
                            SharedObject.setProperty_string(getActivity(), Constants.STATE_MESSAGE, data.body.getJson("params").getString("profile_subject"));

                        }
                        break;

                    //대화명 변경
                    case PacketTypes.PTC_IMS_USER_CHANGE_TITLE:

                        MOALog.i("PTC_IMS_USER_CHANGE_TITLE :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    //프로필 사진 변경
                    case PacketTypes.PTC_IMS_USER_CHANGE_PHOTO:

                        MOALog.i("PTC_IMS_USER_CHANGE_PHOTO :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    case PacketTypes.PTC_IMS_USER_CHANGE_USERNAME:

                        MOALog.i("PTC_IMS_USER_CHANGE_USERNAME :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    case PacketTypes.PTC_IMS_USER_NOTI_CONFIG:
                        MOALog.i("PTC_IMS_USER_NOTI_CONFIG :");
                        MOALog.i("result=" + data.body.get("result"));
                        useNotification(data);
                        break;

                    case PacketTypes.PTC_IMS_SYSTEM_CONFIG:

                        MOALog.i("PTC_IMS_SYSTEM_CONFIG :");
                        MOALog.i("result=" + data.body.get("result"));
                        break;

                    case PacketTypes.PTC_IMS_LINK_DATA_LIST:

                        MOALog.i("PTC_IMS_LINK_DATA_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        //linkData(data);
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_RECV_ROOMCLEAR:

                        String roomId = data.body.getJson("params").getString("roomId");
                        String cId = data.body.getJson("params").getString("cId");

                        LogTrace.E("clean msg roomId : " + roomId + " / cId : " + cId);

                        LocalDB.getChatDataDbHelper(getContext()).cleanChatData(roomId, cId);
                        break;

                    case PacketTypes.PTC_IMS_CHAT_BLINDMSG:
                        // 블라인드 수신
                        String blindRoom = data.body.getJson("params").getString("roomId");
                        String blindChatId = data.body.getJson("params").getString("chatId");

                        LocalDB.getChatDataDbHelper(getContext()).updateBlind(blindRoom, blindChatId);
                        break;

                    case PacketTypes.PTC_IMS_CHAT_RECALLMSG:
                        // 회수 수신
                        String recallRoom = data.body.getJson("params").getString("roomId");
                        String recallChatId = data.body.getJson("params").getString("chatId");

                        LocalDB.getChatDataDbHelper(getContext()).delChatData(recallRoom, recallChatId);
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_RECV_USEROUT:
                        // 강제퇴장
                        String kickRoom = data.body.getJson("params").getString("roomId");

                        if (chatRoomListener == null) {
                            Fragment_ChatTab_ChatList.getInstance().kickUpdate(LocalDB.getChatListDbHelper(getContext()).getRoomName(kickRoom));
                            Fragment_ChatTab_ChatList.getInstance().exitChatRoom(true, data);
                        }
                        Toast.makeText(getContext(), "강제 퇴장 되었습니다", Toast.LENGTH_SHORT).show();

                        break;

                    case PacketTypes.PTC_IMS_GROUP_LIST:    //그룹 리스트
                        MOALog.i("PTC_IMS_GROUP_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        if (data.body.get("result").equals(Constants.SUCCESS)) {
                            LogTrace.E(TAG + "params: " + data.body.get("params").toString());
                            GroupDbHelper groupDbHelper = LocalDB.getGroupDbHelper(getContext());
                            groupDbHelper.addGroupList(getContext(), data, true);
                        }
                        break;

                    case PacketTypes.PTC_IMS_LOGIN_LOGOUT:
                        MOALog.i("PTC_IMS_LOGIN_LOGOUT :");
                        MOALog.i("result=" + data.body.get("result"));
                        LogTrace.E("PTC_IMS_LOGIN_LOGOUT : " + data.body.toString());

                        LogTrace.E("doubleLogin");
//                        Toast.makeText(getContext(), "다른 기기에서 로그인되어 로그인화면으로 이동합니다!", Toast.LENGTH_SHORT).show();
                        LogTrace.E(TAG + "params: " + data.body.get("params").toString());

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

//                            ChatListDbHelper db = LocalDB.getChatListDbHelper(getActivity());
//                            db.dropAllTable();
//                            BuddyDbHelper bdb = LocalDB.getBuddyDbHelper(getActivity());
//                            bdb.dropAllTable();
                                MessengerInfo.clear(getActivity());

                                try {
                                    TranService.releaseService(getActivity());
                                    AlarmReceiver.stopAlarmReceiver(getActivity());
                                } catch (Exception e) {
                                    // TODO: handle exception
                                }

                                Fragment_Main am = Fragment_Main.getInstance();
                                if (am != null) {
                                    //Util.getMainActivity().startLogout(false, true);
                                    //am.getActivity().finish();
                                }

                                SharedUserInfo sharedUserInfo = SharedUserInfo.getInstance(getContext());
                                sharedUserInfo.clearInfo();

//                                PendingIntent i = PendingIntent.getActivity(getActivity().getApplicationContext(), 0, new Intent(getActivity().getIntent()), getActivity().getIntent().getFlags());
//                                AlarmManager am2 = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
//                                am2.set(AlarmManager.RTC, System.currentTimeMillis(), i);

                                getActivity().moveTaskToBack(true);
                                getActivity().finish();
                                android.os.Process.killProcess(android.os.Process.myPid());

                            }
                        }, 2500);
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 쳇룸이후 호출
     */
    public class ChatRoomListAsyncTask extends android.os.AsyncTask<String, String, String> {

        String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            LogTrace.E("PTC_IMS_CHATROOM_LIST ChatRoomListAsyncTask  doInBackground: " + params[0]);

            if ("success".equals(params[0])) {
                result = "success";

                ArrayList<VoChatList> list = mChatListDb.getChatList();

                for (int j = 0; j < list.size(); j++) {
                    for (int i = 0; i < list.get(j).getList().size(); i++) {

                        LogTrace.E("PTC_IMS_CHATROOM_LIST ChatRoomListAsyncTask   ist.get(" + j + ").getList().get(" + i + ") getRoom_name: " + list.get(j).getList().get(i).getRoom_name());

                        if (!list.get(j).getList().get(i).getRoom_type().equals("2") || !list.get(j).getRoom_type().equals("3")) {
                            //TODO
                            mMainPresenter.userList(getActivity(), mUserID, list.get(j).getList().get(i).getRoomId());
                        }
                    }
                }

                setMode("1");
            } else {
                result = "false";
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (chatRoomListener != null)
                chatRoomListener.onChatListResult(true);

            if (chatListListener != null)
                chatListListener.onChatListResult(true);

        }
    }


    public void setOnChatListListener(ChatListListener listener) {
        this.chatListListener = listener;
    }

    public void setOnChatRoomListener(ChatRoomListener listener) {
        this.chatRoomListener = listener;
    }

    public void menuChange(final boolean value) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (value) {
                    appbar_layout.setVisibility(View.GONE);
                } else {
                    appbar_layout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        try {
            pauseTimer();

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        setMainTotalCount();
        try {

            resumeTimer();

            if (isHomePressed) {
                isHomePressed = false;


                AlarmReceiver.startAlarmReceiver(getActivity());
                TranService.startService(getActivity());

                if (Fragment_Main.getInstance() != null && Fragment_Main.getInstance().isLocked && !"".equals(MessengerInfo.getLockPassword(Fragment_Main.getInstance().getActivity()))) {
                    Fragment_Main.getInstance().isLocked = false;
                    Intent intent = new Intent(getActivity(), ActivityLockCheck.class);
                    intent.putExtra("relaseMode", false);
                    getActivity().startActivity(intent);
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    void pauseTimer() {
        try {
            if (mlTimer != null) {
                mlTimer.closeTimer();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    void resumeTimer() {
        if (mlTimer != null) {
            mlTimer.openTimer();
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        String TAG = "SectionsPagerAdapter";

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            LogTrace.E(TAG + "getItem: " + position);

            switch (position) {

                // case 0:
                //FragmentSlideMenu_MTS fragmentSlideMenuMts = new FragmentSlideMenu_MTS();
                //return null;  //fragmentSlideMenuMts;
            /*    mFragmentChatRmList = new Fragment_ChatTab_ChatList();
                return mFragmentChatRmList;*/
                //  Fragment_MoimTab4_Setup fragment_MoimTab4_Setup0 = new Fragment_MoimTab4_Setup();
                // return fragment_MoimTab4_Setup0;

                case 0:
                    // mFragment_MoimTab1_Timeline = new Fragment_MoimTab1_Timeline();
                    return Fragment_MoimTab1_Timeline.getInstance();

                case 1:
                    //mFragmentFriendList = new Fragment_ChatTab_FriendList();
                    return Fragment_ChatTab_FriendList.getInstance();
                case 2:
                    //mFragmentChatRmList = new Fragment_ChatTab_ChatList();
                    return Fragment_ChatTab_ChatList.getInstance();
                case 3:
                    Fragment_MoimTab4_Setup fragment_MoimTab4_Setup = new Fragment_MoimTab4_Setup();
                    return fragment_MoimTab4_Setup;
                default:
                    Fragment_MoimTab1_Timeline timeline0 = new Fragment_MoimTab1_Timeline();
                    return timeline0;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
            //return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
                case 3:
                    return "SECTION 4";
                case 4:
                    return "SECTION 5";
            }
            return null;
        }
    }
}
