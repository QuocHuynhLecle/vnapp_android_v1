package com.wave.messenger.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.SharedObject;

import java.util.List;


/**
 * 멤버초대  다른방법으로초대 프레그먼트
 */
public class  Fragment_Invite_other extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "Fragment_Invite_other";

    private String mParam1;

    private OnFragmentInteractionListener mListener;

    public static Fragment_Invite_other newInstance(String  param1) {
        Fragment_Invite_other fragment = new Fragment_Invite_other();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_invite_other, container, false);
        setOnClickEvent(rootView);

        return rootView;
    }


    private void setOnClickEvent(View rootView) {

        rootView.findViewById(R.id.ll_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg  = SharedObject.getProperty_string(getActivity(), Constants.mmNm, "") +" "+ getResources().getString(R.string.invite_sms_info)
                        + Constants.MOIM_INVITE_URL+ com.wave.messenger.util.SharedObject.getProperty_string(getActivity(), Constants.mmShrtId, "")
                        + "\nFrom " + MessengerInfo.getUserName(getActivity());
                Util.intentShare(getActivity(), msg);


            }
        });


        rootView.findViewById(R.id.ll_kakao).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "ll_kakao", Toast.LENGTH_SHORT).show();
                sharekakao();

            }
        });


        rootView.findViewById(R.id.ll_facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), "ll_line", Toast.LENGTH_SHORT).show();
                shareFacebook();

            }
        });
        rootView.findViewById(R.id.ll_band).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "ll_band", Toast.LENGTH_SHORT).show();

           }
        });


        rootView.findViewById(R.id.ll_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "ll_sms", Toast.LENGTH_SHORT).show();

            }
        });

        rootView.findViewById(R.id.ll_mail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "ll_mail", Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * 페이스북으로 공유하기
     */
    private void shareFacebook() {
        String msg  = SharedObject.getProperty_string(getActivity(), Constants.mmNm, "") +" "+ getResources().getString(R.string.invite_sms_info)
                + Constants.MOIM_INVITE_URL+ com.wave.messenger.util.SharedObject.getProperty_string(getActivity(), Constants.mmShrtId, "")
                + "\nFrom " + MessengerInfo.getUserName(getActivity());

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        //intent.putExtra(Intent.EXTRA_SUBJECT, "타이틀");
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        PackageManager packManager = getActivity().getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList) {
            if(resolveInfo.activityInfo.packageName.startsWith("com.facebook.katana")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if(resolved) {
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "페이스북 앱이 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 카카오톡으로 공유하기
     */
    private void sharekakao() {

        String msg  = SharedObject.getProperty_string(getActivity(), Constants.mmNm, "") +" "+getResources().getString(R.string.invite_sms_info)
                + Constants.MOIM_INVITE_URL+ com.wave.messenger.util.SharedObject.getProperty_string(getActivity(), Constants.mmShrtId, "")
                + "\nFrom " + MessengerInfo.getUserName(getActivity());

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        //intent.putExtra(Intent.EXTRA_SUBJECT, "타이틀");
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        PackageManager packManager = getActivity().getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList) {
            if(resolveInfo.activityInfo.packageName.startsWith("com.kakao.talk")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if(resolved) {
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "카카오톡 앱이 없습니다.", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * 트위터 공유하기.
     */
    public void shareTwiter(){
        String msg  = SharedObject.getProperty_string(getActivity(), Constants.mmNm, "") + getResources().getString(R.string.invite_sms_info)
                + Constants.MOIM_INVITE_URL+ com.wave.messenger.util.SharedObject.getProperty_string(getActivity(), Constants.mmShrtId, "")
                + "\nFrom " + MessengerInfo.getUserName(getActivity());

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.setPackage("com.twitter.android");
        //intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        try {
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "트위터 앱이 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
