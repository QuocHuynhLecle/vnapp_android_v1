package com.wave.messenger.fragment.SlideMenu;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;

public class FragmentSlideMenu_MTS extends Fragment {
	
	static FragmentSlideMenu_MTS instance = null;
	public static FragmentSlideMenu_MTS getInstance() {
		return instance;
	}
	
	public static void clearInstance(FragmentSlideMenu_MTS value) {
		if(value == instance) {
			instance = null;
		}
	}
	
//	FragmentMain mlFragmentMain;
	
	private LinearLayout rootView;
	LinearLayout linearLayoutExit, linearLayoutInterest, linearLayoutCurrent, linearLayoutChart, linearLayoutInformation;
	ImageView imageViewInterest, imageViewCurrent, imageViewChart, imageViewInformation;
	TextView textViewInterest, textViewCurrent, textViewChart, textViewInformation;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {              // 네비게이터에 리스트뷰를 셋한다
    	try {
    		
    		instance = this;
    		
//    		mlFragmentMain = MlFragmentMain.getInstance();
    		
    		rootView = (LinearLayout) inflater.inflate(R.layout.fragmentslide_menu_mts, container, false);

//    		LibMisslee.setSoftwareLayerType(rootView);
    		
    		
    		initView(rootView);
			setEvent();
    	} catch (Exception e) {
			// TODO: handle exception
    		
		}
    	return rootView;
	} 
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		clearInstance(instance);
	}

	/*@Override
	public void OnReceiveMsgFromActivity(int msgId, Object param1, Object param2) {
		// TODO Auto-generated method stub
		
	}*/
	
	public void initView(View view) {
		linearLayoutExit = (LinearLayout)rootView.findViewById(R.id.linearLayoutExit);
		linearLayoutInterest = (LinearLayout)rootView.findViewById(R.id.linearLayoutInterest);
		linearLayoutCurrent = (LinearLayout)rootView.findViewById(R.id.linearLayoutCurrent);
		linearLayoutChart = (LinearLayout)rootView.findViewById(R.id.linearLayoutChart);
		linearLayoutInformation = (LinearLayout)rootView.findViewById(R.id.linearLayoutInformation);
	}
	
	public void setEvent() {
		
		linearLayoutExit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
//					activityChat.showContent();
					openMenu();
				} catch (Exception e) {
					// TODO: handle exception
					
				}
			}
		});
		
		linearLayoutInterest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			}
		});
		
		linearLayoutCurrent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			}
		});
		
		linearLayoutChart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			}
		});

		linearLayoutInformation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			}
		});
	}
	
	/*
	 * Navigation Drawer Settings. (inside Fragment)
	 */
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }
	
	public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

	public void setUp(View parent, int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = parent.findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
//        mDrawerLayout.setScrimColor(0x200000);
        
        
        
        ActionBar actionBar = getActionBar();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			actionBar.setHomeButtonEnabled(true);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar.hide();
		}

		mDrawerToggle = new ActionBarDrawerToggle(
        		/*MlFragmentMain.getInstance().getActivity(),*/                    /* host Activity */
        		Activity_ChatRoom.activity_chatRoom,
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.icon,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            
            @Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					if (slideOffset == 0
                            && getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD) {
                        // drawer closed
                    } else if (slideOffset != 0
                            && getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
                        // started opening
                        /*if(MlFragmentMain.getInstance() != null) {
                            MessengerInterface.hideSoftKeyboard(MlFragmentMain.getInstance().getActivity());
                        }*/
                    }
				}
				super.onDrawerSlide(drawerView, slideOffset);
			}
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
//
//        mDrawerToggle = new MlActionBarDrawerToggle(
//        		MlFragmentMain.getInstance().getActivity(),                    /* host Activity */
//                mDrawerLayout,                    /* DrawerLayout object */
//                R.drawable.icon,             /* nav drawer image to replace 'Up' caret */
//                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
//                R.string.navigation_drawer_close,  /* "close drawer" description for accessibility */
//                MlFragmentSlideMenu_MTS.this,
//                "mts"
//        ) {
//            
//            @Override
//			public void onDrawerSlide(View drawerView, float slideOffset) {
//				if (slideOffset == 0
//						&& getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_STANDARD) {
//					// drawer closed
//					getActionBar().setNavigationMode(
//							ActionBar.NAVIGATION_MODE_TABS);
//					MlFragmentMain.getInstance().getActivity().invalidateOptionsMenu();
//	                Toast.makeText(MlFragmentMain.getInstance().getActivity(), "Close " + getTag(), Toast.LENGTH_SHORT).show();
//				} else if (slideOffset != 0
//						&& getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
//					// started opening
//					getActionBar().setNavigationMode(
//							ActionBar.NAVIGATION_MODE_STANDARD);
//					MlFragmentMain.getInstance().getActivity().invalidateOptionsMenu();
//	                Toast.makeText(MlFragmentMain.getInstance().getActivity(), "Open " + getTag(), Toast.LENGTH_SHORT).show();
//				}
//				super.onDrawerSlide(drawerView, slideOffset);
//			}
//        };
//
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
    }

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private ActionBar getActionBar() {
//        return Fragment_Main.getInstance().getActivity().getActionBar();
		return Activity_ChatRoom.activity_chatRoom.getActionBar();
	}

    public void openMenu() {
    	if(isDrawerOpen()) {
    		mDrawerLayout.closeDrawer(mFragmentContainerView);
    	} else {
    		mDrawerLayout.openDrawer(mFragmentContainerView);
    		
//    		if(MlFragmentMain.getInstance() != null) {
//    			MessengerInterface.hideSoftKeyboard(MlFragmentMain.getInstance().getActivity());
//    		}
    	}
    }
    public void closeMenu() {
    	if(isDrawerOpen()) {	
    		mDrawerLayout.closeDrawer(mFragmentContainerView);    		
    	}
    }  
}
