package com.wave.messenger.fragment.ChatRoom;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_Gallery;
import com.wave.messenger.activity.Activity_InviteFriend;
import com.wave.messenger.activity.Activity_Profile;
import com.wave.messenger.adapter.ChatListUserAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.share.HMMessengerShare;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.FileUtil;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoUsers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;



public class Fragment_RightNavigation extends Fragment {

    private ImageView ivalarm, ivCamera, ivGallery, ivExit;
    private Button ivJongMokSearch, ivShareMTS, ivJongMokLink, ivCurStock, ivImpInterrest, ivChart, ivMTS, ivPotfolio;
    private LinearLayout llViewRoomPictureGallery, llAddNewUser, ll_profitshare, ll_jongmoksearch, ll_signalshare, ll_portfolio, ll_camera, ll_picture, ll_download;
    private TextView tv_userlist;
    private ListView lvUserList;
    private ChatListUserAdapter adapter;
    private VoChatList roomInfo;
    private MessengerInterface messengerInterface;

    private List<VoFriendList> friendList = Collections.emptyList();
    private View mFragmentContainerView;

    Uri fileUri;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rightnavigation, container, false);

        roomInfo = (VoChatList) getArguments().getSerializable("roomInfo");
        initView(view);
        setEvent();
        setListView();

        return view;
    }

    private void initView(View view) {
        ivalarm = (ImageView) view.findViewById(R.id.ivalarm);
        ivExit = (ImageView) view.findViewById(R.id.ivExit);
        llAddNewUser = (LinearLayout) view.findViewById(R.id.llAddNewUser);
        //ll_profitshare = (LinearLayout) view.findViewById(R.id.ll_profitshare);
       // ll_jongmoksearch = (LinearLayout) view.findViewById(R.id.ll_jongmoksearch);
       // ll_signalshare = (LinearLayout) view.findViewById(R.id.ll_signalshare);
//		ll_portfolio = (LinearLayout) view.findViewById(R.id.ll_portfolio);
        ll_camera = (LinearLayout) view.findViewById(R.id.ll_camera);
        ll_picture = (LinearLayout) view.findViewById(R.id.ll_picture);
        tv_userlist = (TextView) view.findViewById(R.id.tv_userlist);
        lvUserList = (ListView) view.findViewById(R.id.lvUserList);
        ll_download = (LinearLayout) view.findViewById(R.id.ll_download);

        if ("4".equals(roomInfo.getRoom_type()) || "5".equals(roomInfo.getRoom_type())) {
            llAddNewUser.setVisibility(View.GONE);
        }

        if ("1".equals(Statics.ROOMINFO.getUseNoti()) || TextUtils.isEmpty(Statics.ROOMINFO.getUseNoti())) {
            ivalarm.setSelected(false);
        } else {
            ivalarm.setSelected(true);
        }

        lvUserList.setFocusable(false);
    }

    private void setEvent() {

        messengerInterface = new MessengerInterface();
        /**
         * 수익률공유
         */
       /* ll_profitshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "준비중입니다!", Toast.LENGTH_SHORT).show();

//                MessengerInterface.getInstance().onOpenMTS(Activity_ChatRoom.getInstance(), 1, "5000", "");

//                String mag = "현재 대화방을 닫고 \nMTS 앱으로 이동 하시겠습니까?";
                String mag = "MTS의 수익률 자랑하기 화면으로 \n이동하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessengerInterface.getInstance().onOpenMTS(Activity_ChatRoom.getInstance(), 1, "5000", "");
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.setCancelEvent(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.show();
            }
        });*/
        /**
         * 종목검색
         */
      /*  ll_jongmoksearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_ChatRoom.getInstance().jongmokSearch();
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });*/

        /**
         * 시그널공유
         */
        /*ll_signalshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_ChatRoom.getInstance().signalShare();
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });*/

        /**
         * 포트폴리오
         */
        /*ll_portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View v) {
			    Activity_ChatRoom.getInstance().openMenuRight();

			}
		});*/

        ivalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivalarm.setSelected(!ivalarm.isSelected());
                String roomId = roomInfo.getRoomId();
                if (ivalarm.isSelected()) {
                    setAlarm("0", roomId);
                } else {
                    setAlarm("1", roomId);
                }
            }
        });

        ll_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = FileUtil.createImageFile();
                    if (f != null) {
                        LogTrace.E("파일 생성 완료");
                    }
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    getActivity().startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                    Activity_ChatRoom.getInstance().openMenuRight();
                } else {
                    Dialog_Common.showCameraPermissionDialog(getActivity());
                }
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        ll_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    Intent intent = new Intent(getActivity(), Activity_Gallery.class);
                    intent.putExtra("maxCount", 1);
                    getActivity().startActivityForResult(intent, Constants.GALLERY_REQUEST);
                    Activity_ChatRoom.getInstance().openMenuRight();
                } else {
                    Dialog_Common.showCameraPermissionDialog(getActivity());
                }
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        llAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (friendList == null) {
                    friendList = new ArrayList<>();
                }

                Intent intent = new Intent(getActivity(), Activity_InviteFriend.class);
                intent.putExtra("presentFriends", (Serializable) friendList);
                getActivity().startActivityForResult(intent, Constants.INVITE_FRIEND_CHAT_REQ);
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        ivExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO : Add Delete Chat Room Logic.
                getAlert().setTitle("채팅방 나가기")
                        .setMessage("채팅방을 나가게 되면 모든 대화내용이 삭제되며,삭제된 대화내용은 복구되지 않습니다.")
                        .setNeutralButton("삭제하기", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                exitChatRoom(roomInfo.getRoomId());
                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();

            }
        });

		/*llViewRoomPictureGallery.setOnClickListener(new OnClickListener() {
            @Override
			public void onClick(View v) {
				//TODO : Add PICTURE GALLERY LOGIC
				Intent mIntent = new Intent(getActivity(), Activity_ChatAlbum.class);
				mIntent.putExtra("roomInfo", roomInfo);
				getActivity().startActivityForResult(mIntent, Constants.REQUEST_CHAT_ALBUM);
			}
		});*/

        lvUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                VoFriendList item = (VoFriendList) parent.getAdapter().getItem(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable("friendInfo", item);

                if (item.getUserId().equals(MessengerInfo.getUserId(getActivity())))
                    bundle.putBoolean("isEdit", true);
                else
                    bundle.putBoolean("isEdit", false);

                if ("0".equals(roomInfo.getRoom_type()))
                    bundle.putBoolean("isShow", true);
                else
                    bundle.putBoolean("isShow", false);

                Intent mIntent = new Intent(getActivity(), Activity_Profile.class);
                mIntent.putExtras(bundle);
                startActivityForResult(mIntent, Constants.REQUEST_CHANGE_PROFILE);
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });
        ll_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = new File("/sdcard/time_prime");
                if (!path.isDirectory()) {
                    path.mkdirs();
                }

                File savefile = new File(path.getAbsolutePath() + "/피기맘 대화내용" + ".txt");
                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getActivity());
                List<VoChatData> list = db.getChatData(roomInfo.getRoomId());

                fileUri = Uri.fromFile(savefile);

                try {
                    if (!savefile.exists())
                        savefile.createNewFile();

                    BufferedWriter reader = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(savefile), "euc-kr"));

                    for (int i = 0; i < list.size(); i++) {
                        String date = "[" + list.get(i).getReg_date().toString() + "] ";
                        String name = list.get(i).getSenderName().toString() + " : ";
                        String msg = "";
                        if (list.get(i).getTitle().equals("(이모티콘)") || list.get(i).getTitle().equals("(사진)"))
                            msg = list.get(i).getTitle().toString();
                        else
                            msg = list.get(i).getMessage().toString();
                        String textfile = date + name + msg;

                        reader.write(textfile);
                        reader.write("\r\n");
                    }

                    reader.flush();
                    reader.close();

                    sendEmailOrMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });


    }//end of setEvent

    protected void sendEmailOrMessage() {
        Uri uri_email = Uri.parse("mailto:");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri_email);
//        i.addCategory(Intent.CATEGORY_DEFAULT);
//        i.setType("plan/text");
        i.putExtra(Intent.EXTRA_STREAM, fileUri);
        i.putExtra(Intent.EXTRA_SUBJECT, "피기맘 대화내용");
        i.putExtra(Intent.EXTRA_TEXT, "피기맘 대화내용입니다.");

        getActivity().startActivity(i);
    }

    public void setListView() {

        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if (TextUtils.isEmpty(roomInfo.getRoomId())) {
                    friendList = roomInfo.getFriends();
                    adapter = new ChatListUserAdapter(friendList);
                    lvUserList.setAdapter(adapter);
                    setListViewHeightBasedOnChildren(lvUserList);

                } else {
                    UsersDbHelper db = LocalDB.getUsersDbHelper(getActivity());
                    ArrayList<VoUsers> list = db.getUserList(roomInfo.getRoomId());
                    friendList = new ArrayList<>();

                    for (int i = 0; i < list.size(); i++) {
                        VoFriendList item = new VoFriendList();
                        item.setUserId(list.get(i).getUserId());
                        item.setUserName(list.get(i).getUserName());
                        item.setPhone(list.get(i).getTel());

                        friendList.add(item);
                    }

                    adapter = new ChatListUserAdapter(friendList);

                    lvUserList.setAdapter(adapter);
                    setListViewHeightBasedOnChildren(lvUserList);
//                    Activity_ChatRoom.getInstance().roomInfo.setFriends(friendList, getContext());

                }
                tv_userlist.setText(getString(R.string.open_usercount, friendList.size()));
                return false;
            }
        });

        try {
            handler.sendMessage(handler.obtainMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void setUpView(View parent, int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = parent.findViewById(fragmentId);
    }

    public void exitChatRoom(String roomId) {
        try {
            ErrorController.showMessage("[FragFragRightNavigation] exitChatRoom check RoomId: " + roomId);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getActivity()));
            value.put("roomId", roomId);
            value.put("exitUserId", MessengerInfo.getUserId(getActivity()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_EXIT, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setAlarm(String alarm, String roomId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getActivity()));
            value.put("roomId", roomId);
            value.put("deviceType", "android");
            value.put("useNoti", alarm);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_NOTI_CONFIG, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AlertDialog.Builder getAlert() {
        int version = Integer.parseInt((Build.VERSION.RELEASE).substring(0, 1));
        AlertDialog.Builder alert;
        if (version >= 3) {
            alert = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        return alert;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CHANGE_PROFILE && resultCode == Constants.RESULT_CHANGE_PROFILE) {
            adapter.notifyDataSetChanged();
        }
    }
}
