package com.wave.messenger.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wave.messenger.activity.Activity_InvisibleMoimManageList;
import com.wave.messenger.activity.Activity_MoimManageList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.MoimDbHelper;
import com.wave.messenger.dialog.Dialog_MyMoim_Edit;
import com.wave.messenger.dialog.Dialog_ViewType;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.InterfaceMymoimListener;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 *@deprecated
 */
public class Fragment_MoimTab2_MyMoimList extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "Fragment_Tag2";
    boolean mbRequest=true;

    private String mParam1;
    private String mParam2;

    //public  static  Fragment_MoimTab2_MyMoimList Fragment;
    //private OnFragmentInteractionListener mListener;
    private RecyclerView mRecyclerView;

    public Fragment_MoimTab2_MyMoimList() {
        Log.e(TAG,"Fragment_Tab1_Timeline");
    }


    public static Fragment_MoimTab2_MyMoimList newInstance(String param1, String param2) {
        Fragment_MoimTab2_MyMoimList fragment = new Fragment_MoimTab2_MyMoimList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MOALog.w("Fragment_MoimTab2_MyMoimList onCreate");

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        //Fragment=this;
        //moimListRequest();
    }



    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MOALog.w("Fragment_MoimTab2_MyMoimList onCreateView" );

        View rootView = inflater.inflate(R.layout.fragment_moim_tab2_mymoimlist, container, false);

        mSwipeRefreshLayout=((SwipeRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MOALog.w("Fragment_MoimTab2_MyMoimList onRefresh");
                // Refresh items
                refreshItems();
            }
        });


        if(Util.isNetworkConnected(getActivity())){
            setRecyclerView(rootView);
            moimListRequest();
        }else{
            setRecyclerView(rootView);
        }



        setOnClickEvent(rootView);

        return rootView;
    }


    /**
     * 새로고침
     */
    public void refreshItems() {
        MOALog.w("Fragment_MoimTab2_MyMoimList refreshItems");
        moimListRequest();

        // Load complete
        onItemsLoadComplete();
    }

    /**
     * 새로고침 완료후
     */
    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        mSwipeRefreshLayout.setRefreshing(false);
    }



    /**
     * 모임리스트를 호출한다.
     */
    private void moimListRequest() {
        MOALog.w("Fragment_MoimTab2_MyMoimList moimListRequest" );
        Moa.moimListRequest(getActivity(),"0",mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_LIST :    //모임리스트
                        String strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            //디비저장
                            MoimDbHelper db1 = LocalDB.getMoimDbHelper(getActivity());
                            db1.addMoimList(data, getActivity());

                            setMoimListDate();
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private void setOnClickEvent(View rootView) {

        //편집버튼
        rootView.findViewById(R.id.btn_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                InterfaceMymoimListener  InterfaceMymoimListener=new InterfaceMymoimListener() {
                    @Override
                    public void onSelectItem(int n) {
                        Log.e("setOnClickEvent","onSelectItem "+n );
                        switch (n){ //뷰타입선택
                            case 0:
                                dialogViewType();
                                break;

                            case 1: //모임관리
                                intentMoimManage();
                                break;

                            case 2: //숨긴 모임관리
                                intentInvisivleMoimManage();
                                break;
                        }
                    }
                };
                //Dialog_MyMoim_Edit.getProfileEditCameraDialog(getActivity(),InterfaceMymoimListener).show();

                final Dialog_MyMoim_Edit mDialog = new Dialog_MyMoim_Edit(getActivity(),InterfaceMymoimListener);
                mDialog.show();
            }
        });
    }

    private void intentMoimManage() {
        Intent mIntent = new Intent(getActivity(), Activity_MoimManageList.class);
        startActivity(mIntent);
    }

    private void intentInvisivleMoimManage() {
        Intent mIntent = new Intent(getActivity(), Activity_InvisibleMoimManageList.class);

        //Intent mIntent = new Intent(getActivity(), TestMainActivity.class);

        startActivity(mIntent);
    }

    /**
     * 뷰타입설정대로 리스트를 설정한다
     */
    public  void setListType(){
        Log.e(getActivity().getLocalClassName(),"setListType");

    }


    /**
     * 뷰타입 팝업
     */
    private void dialogViewType() {
        OnSelectViewTypeInterface onSelectViewTypeInterface = new OnSelectViewTypeInterface() {
            @Override
            public void onSelectView() {    //뷰타입선택 큰카드형 작은카드형 목록형 설정

                setGridLayout(SharedObject.getProperty_int(getActivity(), Constants.PREF_VIEW_TYPE,1));
                setMoimListDate();


            }
        };
        Dialog_ViewType dialog= new Dialog_ViewType(getActivity(),onSelectViewTypeInterface);
        dialog.show();
    }

    /**
     * 리싸이클 뷰
     * 2칸 3칸 1칸 설정한다
     * @param property_int
     */
    private void setGridLayout(int property_int) {

        Log.e(TAG,"#setGridLayout "+property_int);
        switch (property_int){
            case 1:
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));    //2칸
                break;
            case 2:
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));    //3칸
                break;
            case 3:
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));    //1칸
                break;
        }


    }

    public interface OnSelectViewTypeInterface {
        void onSelectView();
    }

    private void setRecyclerView(View rootView) {
        MOALog.w("Fragment_MoimTab2_MyMoimList setRecyclerView" );

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.itemsRecyclerView);
        //RecyclerView.LayoutManager recyclerViewLayoutManager = new GridLayoutManager(getActivity(), 2);
        //mRecyclerView.setLayoutManager(recyclerViewLayoutManager);

        setGridLayout(SharedObject.getProperty_int(getActivity(), Constants.PREF_VIEW_TYPE,1));

        /*ArrayList<VoMoim> arVoMoim = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            voMoim= new VoMoim("단타를 사랑하는 사람들"+i,i);
            arVoMoim.add(voMoim);
        }
        voMoim=new VoMoim("+", 0);
        arVoMoim.add(voMoim);*/

        setMoimListDate();
    }

    /**
     * 디비에서 모임리스트를 가져와서
     * 어댑터세팅
     */
    private void setMoimListDate() {
        ArrayList<VoMoimList> arVoMoimList;
        arVoMoimList = new MoimDbHelper(getActivity()).getContactMoimListInDB(getActivity());

        arVoMoimList.add(new VoMoimList());//추가 버튼 위한 빈값

        /*for (int i = 0; i < arVoMoimList.size(); i++) {
            Log.e(TAG,"setMoimListDate arVoMoimList: "+arVoMoimList.get(i).getMmNm()+" "+arVoMoimList.get(i).getMmTg());
        }*/



        //MyMoimRecyclerAdapter myMoimRecyclerAdapter = new MyMoimRecyclerAdapter(getActivity(),arVoMoimList, SharedObject.getProperty_int(Activity_MymoimList.this, Constants.PREF_VIEW_TYPE, 1));
        //mRecyclerView.setAdapter(myMoimRecyclerAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onStart() {
        super.onStart();

        MOALog.w("Fragment_MoimTab2_MyMoimList onStart" );

    }

    @Override
    public void onResume() {
        super.onResume();
        MOALog.w("Fragment_MoimTab2_MyMoimList onResume" );
        if(mbRequest){
            moimListRequest();
            mbRequest=false;
        }

    }

    /*@Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


}
