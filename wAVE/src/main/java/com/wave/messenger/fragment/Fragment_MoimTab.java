package com.wave.messenger.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wave.messenger.activity.Activity_MoimFind;
import com.wave.messenger.activity.Activity_MoimFindBySubject;
import com.wave.messenger.activity.Activity_MymoimList;
import com.wave.massenger.piggy.R;


/**
 * 탭 프레그먼트
 * 타임라인,마이모임
 *
 *
 *
 */
public class Fragment_MoimTab extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private TabPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private OnFragmentInteractionListener mListener;

    String TAG="Fragment_Tab";

    public Fragment_MoimTab() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Fragment_MoimTab newInstance(String param1, String param2) {
        Fragment_MoimTab fragment = new Fragment_MoimTab();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"onCreate");


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_moim_tab, container, false);
        InitView(rootView);
        return rootView;
    }

    private void InitView(final View rootView) {
        //mSectionsPagerAdapter = new TabPagerAdapter(getActivity().getSupportFragmentManager(),rootView);
        mSectionsPagerAdapter = new TabPagerAdapter(getChildFragmentManager(),rootView);
        mViewPager = (ViewPager) rootView.findViewById(R.id.container1);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        (rootView.findViewById(R.id.imgv_tab1)).setSelected(true);
                        (rootView.findViewById(R.id.imgv_tab2)).setSelected(false);

                        break;
                    case 1:
                        (rootView.findViewById(R.id.imgv_tab1)).setSelected(false);
                        (rootView.findViewById(R.id.imgv_tab2)).setSelected(true);

                        break;

                    default:
                        (rootView.findViewById(R.id.imgv_tab1)).setSelected(true);
                        (rootView.findViewById(R.id.imgv_tab2)).setSelected(false);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*(rootView.findViewById(R.id.imgv_tab1)).setSelected(true);
        (rootView.findViewById(R.id.imgv_tab2)).setSelected(false);


        rootView.findViewById(R.id.imgv_tab1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"tab1");
                mViewPager.setCurrentItem(0);


                (rootView.findViewById(R.id.imgv_tab1)).setSelected(true);
                (rootView.findViewById(R.id.imgv_tab2)).setSelected(false);

            }
        });*/


        /*rootView.findViewById(R.id.imgv_tab2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"tab2");
                mViewPager.setCurrentItem(1);

                (rootView.findViewById(R.id.imgv_tab1)).setSelected(false);
                (rootView.findViewById(R.id.imgv_tab2)).setSelected(true);
            }
        });*/


        /*rootView.findViewById(R.id.imgb_moim_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모임찾RL

                Intent mIntent = new Intent(getActivity(), Activity_MoimFind.class);
                startActivity(mIntent);
            }
        });*/

        /*rootView.findViewById(R.id.imgb_moim_make).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모임개설

                Intent intent = new Intent(getActivity(), Activity_MoimFindBySubject.class);
                intent.putExtra("TYPE",1);
                startActivity(intent);
            }
        });*/


        //상단 내모임으로 가기
        rootView.findViewById(R.id.ll_mymoim).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentMymoim();
            }
        });

        //검색화면
        rootView.findViewById(R.id.imgb_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(getActivity(), Activity_MoimFind.class);
                startActivity(mIntent);

            }
        });

        //모임만들기
        rootView.findViewById(R.id.imgb_make).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모임개설
                Intent intent = new Intent(getActivity(), Activity_MoimFindBySubject.class);
                intent.putExtra("TYPE",1);
                startActivity(intent);
            }
        });

    }

    /**
     * 내 모임으로 가기
     */
    private void intentMymoim() {
        Intent mIntent = new Intent(getActivity(), Activity_MymoimList.class);
        startActivity(mIntent);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
           // throw new RuntimeException(context.toString()
            //        + " must implement OnFragmentInteractionListener");
        }
    }*/


    public class TabPagerAdapter extends FragmentPagerAdapter {
        View rootView;
        String TAG="TabPagerAdapter";
        public TabPagerAdapter(FragmentManager fm, View v) {
            super(fm);
            rootView=v;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    Fragment_MoimTab1_Timeline fragment1_timeline = new Fragment_MoimTab1_Timeline();
                    return fragment1_timeline;


                /*case 1:
                    Fragment_MoimTab2_MyMoimList fragment_MyMoimList = new Fragment_MoimTab2_MyMoimList();
                    return fragment_MyMoimList;*/

                default:
                    Fragment_MoimTab1_Timeline fragment1 = new Fragment_MoimTab1_Timeline();
                    return fragment1;
            }
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";

            }
            return null;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
