package com.wave.messenger.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_CustomerList;
import com.wave.messenger.activity.Activity_Floating;
import com.wave.messenger.activity.Activity_FriendRecommend;
import com.wave.messenger.activity.Activity_GroupList;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_ProfessionalList;
import com.wave.messenger.activity.Activity_Profile;
import com.wave.messenger.adapter.FriendListViewAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Friend;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.dialog.Dialog_Moim_Friend_Join;
import com.wave.messenger.dialog.Dialog_Moim_Join;
import com.wave.messenger.mvp.Main.IMainView;
import com.wave.messenger.mvp.Main.MainPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

import static com.wave.massenger.piggy.R.id.ivDelete;

/**
 * Created by aveapp on 2017. 4. 14..
 */

public class Fragment_ChatTab_FriendList extends Fragment implements IMainView, TextWatcher, ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener, ExpandableListView.OnItemLongClickListener {
    static Fragment_ChatTab_FriendList instance = null;

    public static Fragment_ChatTab_FriendList getInstance() {
        if(instance!=null){
            return instance;
        }else{
            return  instance =new Fragment_ChatTab_FriendList();
        }

    }

    public static void clearInstance(Fragment_ChatTab_FriendList value) {
        if (value == instance) {
            instance = null;
        }
    }
    public static Fragment_ChatTab_FriendList fragmentChatTabFriendList;
    public EditText et_search;
    public ImageView iv_search, iv_delete;
    private ImageButton btn_floating;
    private LinearLayout ll_search;
    private LinearLayout ivFriendRecommend;

    private ExpandableListView expandableListView;
    private FriendListViewAdapter friendListViewAdapter;

    private Context context;

    // ListView Components
    private ArrayList<VoFriendList> childList = new ArrayList<>();
    private VoFriendList friend;
    private static ArrayList<String> searchGroupList = new ArrayList<>();
    private static ArrayList<VoFriendList> searchChildList = new ArrayList<>();

    public boolean mIsSearchingFriends = false;
    public String clickItem = "";

    // presenter
    private MainPresenter mMainPresenter;
    public String msFriendCount;
    String TAG = getClass().getSimpleName();

    public String roomName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat_tab2_friendlist, container, false);
        context = getActivity();
        instance = this;
        initView(v);
        setEvent();
        mMainPresenter = new MainPresenter(this);
        //db에서 친구리스트 가져오기
        mMainPresenter.getFriendList(getContext());

        return v;
    }

    @Override
    public void onStop() {
        super.onStop();
        et_search.clearFocus();
    }

    private void initView(View v) {
        et_search = (EditText) v.findViewById(R.id.et_search);
        iv_search = (ImageView) v.findViewById(R.id.iv_search);
        iv_delete = (ImageView) v.findViewById(ivDelete);
        btn_floating = (ImageButton) v.findViewById(R.id.btn_floating);
        expandableListView = (ExpandableListView) v.findViewById(R.id.expandableListView);
        ll_search = (LinearLayout) v.findViewById(R.id.ll_search);
        ivFriendRecommend = (LinearLayout) v.findViewById(R.id.ivFriendRecommend);

        et_search.setImeOptions(EditorInfo.IME_ACTION_DONE);
        // 커서 자동 포커스(커서 깜빡임)
        try {
            Field cursor = TextView.class.getDeclaredField("mCursorDrawableRes");
            cursor.setAccessible(true);
            cursor.set(et_search, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEvent() {

        et_search.addTextChangedListener(this);
        expandableListView.setOnGroupClickListener(this);
        expandableListView.setOnChildClickListener(this);
        expandableListView.setOnItemLongClickListener(this);

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                mIsSearchingFriends = true;
                mMainPresenter.searchFriendList(et_search.getText().toString(), searchText(childList));
                return false;
            }
        });

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_search.setText("");
            }
        });

        btn_floating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), Activity_Floating.class);
                startActivityForResult(mIntent, Constants.REQUEST_ADD_GROUP);
            }
        });

        ivFriendRecommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Activity_FriendRecommend.class));
            }
        });
    }

    /**
     * 친구리스트 업데이트
     * 친구목록,친구차단,숨김,즐겨찾기
     */
    public void onUpdateFriend() {
        try {
            mMainPresenter.getFriendList(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void addMemberFriends(String userId, String userName, String tel) {
        try {
            mMainPresenter.addMemberFriend(userId, userName, tel, getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addNonMemberFriend(String userId, String userName, String tel) {
        try {
            mMainPresenter.addNonMemberFriend(userId, userName, tel, getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<VoFriendList> searchText(ArrayList<VoFriendList> childList) {
        for (VoFriendList item : childList) {
            if (Constants.LISTTYPE_MYFRIEND.equals(item.getUserName())) {
                return item.getList();
            }
        }

        ErrorController.showMessage("ffmain - searchText() - return null");

        return null;
    }

    private void searchText() {

        if (et_search.getText().toString().length() != 0) {
            iv_delete.setVisibility(View.VISIBLE);
            mIsSearchingFriends = true;
            mMainPresenter.searchFriendList(et_search.getText().toString() + "", searchText(childList));
        } else {
            iv_delete.setVisibility(View.GONE);
            ErrorController.showMessage("[Frag_Frag_main] onTextChanged()");
            mMainPresenter.getFriendList(getContext());
            mIsSearchingFriends = false;
            searchGroupList = null;
            searchChildList = null;
        }
    }

    public AlertDialog.Builder getAlert() {
        AlertDialog.Builder alert;
        alert = new AlertDialog.Builder(getContext());
        return alert;
    }

    /**
     * 그룹 추가 이벤트
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_ADD_GROUP) {
            if (resultCode == Constants.RESULT_ADD_GROUP) {
                Intent mIntent = new Intent(getActivity(), Activity_GroupList.class);
                startActivity(mIntent);
            }
        }
    }

    /**
     * 친구 목록 화면에 뿌림
     */
    @Override
    public void showFriendList(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
        if (count != null) {
            msFriendCount = count;
            //TODO WAVEMainActivity.staticMainActivity.setFragmentFriendList();
            //WAVEMainActivity.staticMainActivity.setFriendCount(count);
        }
        childList = friendList;

        friendListViewAdapter = new FriendListViewAdapter(getActivity(), groupList, friendList, mMainPresenter, false);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    /**
     * 친구 목록 화면에 뿌림 - 검색
     */
    @Override
    public void showFriendList2(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, String count) {
        ErrorController.showMessage("Frag_Frag_main, showFriendList2 : friendList size : " + friendList.size() + ", innerList : " + friendList.get(0).getList().size());

        searchChildList = friendList;
        searchGroupList = groupList;

        friendListViewAdapter = new FriendListViewAdapter(getActivity(), groupList, friendList, mMainPresenter, false);
        expandableListView.setAdapter(friendListViewAdapter);

        for (int i = 0; i < friendListViewAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        friendListViewAdapter.notifyDataSetChanged();
    }

    //친구 숨김/해제
    @Override
    public void onHideSuccess(VoFriendList hiddenFriend) {// from FriendListViewAdapter
        LogTrace.E("onHideSuccess");
        friendListViewAdapter.removeChild(hiddenFriend);
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getActivity());
        db.hideBuddy(hiddenFriend.getUserId());
    }

    //친구 차단/해제
    @Override
    public void onBlockSuccess(VoFriendList blockedFriend) {// from FriendListViewAdapter
        LogTrace.E("Fragment_FriendList onBlockSuccess(VoFriendList blockedFriend");
        friendListViewAdapter.removeChild(blockedFriend);
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getActivity());
        db.blockBuddy(blockedFriend.getUserId());
    }

    /**
     * 친구리스트 아이템 클릭 이벤트
     */
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, final int groupPosition, final int childPosition, long id) {
        final Bundle bundle = new Bundle();
        try {

            if (!mIsSearchingFriends) {//검색 중이 아닐때
                final VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
                bundle.putSerializable("friendInfo", item);

                if (item.getUserId().equals(MessengerInfo.getUserId(getActivity()))) {
                    bundle.putBoolean("isEdit", true);
                }
                else {
                    bundle.putBoolean("isEdit", false);
                }

                bundle.putBoolean("isShow", false);

            } else {//검색 중일 때

                VoFriendList item = searchChildList.get(groupPosition).getList().get(childPosition);
                bundle.putSerializable("friendInfo", item);
                bundle.putBoolean("isEdit", false);
                bundle.putBoolean("isShow", false);
                Intent mIntent = new Intent(getActivity(), Activity_Profile.class);
                mIntent.putExtras(bundle);
                startActivityForResult(mIntent, Constants.REQUEST_CHANGE_PROFILE);

            }

            if (childList.get(groupPosition).getUserName().equals(Constants.LISTTYPE_NEWFRIEND)) {

                BuddyDbHelper db = LocalDB.getBuddyDbHelper(getActivity());
                db.setNotFriendAnymore(getActivity(), childList.get(groupPosition).getList().get(childPosition));
            }

            View view = getActivity().getCurrentFocus();

            if (view != null) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            if (childList.get(groupPosition).getUserName().equals(Constants.LISTTYPE_MYPROFILE) && !mIsSearchingFriends) {
                Bundle mybundle = new Bundle();
                Intent myintent = new Intent(getActivity(), Activity_Profile.class);
                myintent.putExtras(mybundle);
                startActivity(myintent);
            } else if (childList.get(groupPosition).getUserName().equals(Constants.LISTTYPE_MYPROFESSIONALFRIEND) || childList.get(groupPosition).getUserName().equals(Constants.LISTTYPE_PROFESSIONALFRIEND)) {
                final VoFriendList item = childList.get(groupPosition).getList().get(childPosition);

                switch (item.getmStt()) {
                    case 0:
//                        Toast.makeText(getContext(), "가입 후 대화방 입장이 가능합니다", Toast.LENGTH_SHORT).show();
                        final Dialog_Moim_Join dialog_moim_join = new Dialog_Moim_Join(getActivity(), item.getMmNm(), item.getIntr(), item.getImgTmb());
                        dialog_moim_join.setListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int i = v.getId();
                                if (i == R.id.bt_cancel) {
                                    dialog_moim_join.dismiss();
                                } else if (i == R.id.bt_ok) {
                                    showDialogJoin(item);
                                    dialog_moim_join.dismiss();
                                } else if(i == R.id.ll_close){
                                    dialog_moim_join.dismiss();
                                }
                            }
                        });
                        dialog_moim_join.show();
                        break;
                    case 1:


                        final Dialog_Moim_Friend_Join dialog_moim_friend_join = new Dialog_Moim_Friend_Join(getActivity(), item.getMmNm(), item.getIntr(), item.getImgTmb());
                        dialog_moim_friend_join.setListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int i = v.getId();
                                if (i == R.id.bt_cancel) {
                                    //모임으로 이동
                                    SharedObject.setProperty_string(getActivity(), Constants.MOIM_ID, item.getMmId());
                                    startActivity(new Intent(getActivity(), Activity_MoimTimeLineList.class));

                                    dialog_moim_friend_join.dismiss();
                                } else if (i == R.id.bt_ok) {
                                    //대화방으로 이동
//                                    String dbroomName = LocalDB.getChatListDbHelper(getContext()).getRoomName(item.getRmId());
//                                    if (item.getMmNm().equals(dbroomName)) {
//                                        proChatRequest(item.getRmId());
//                                    } else {
//                                        roomName = item.getMmNm();
//                                        Moa.opentalkenter(getContext(), item.getRmId(), item.getMmNm(), "7", mMainActivityHandler);
//                                    }

                                    if (LocalDB.getChatListDbHelper(getContext()).existChatRoom(item.getRmId())) {
                                        proChatRequest(item.getRmId());
                                    } else {
                                        roomName = item.getMmNm();
                                        Moa.opentalkenter(getContext(), item.getRmId(), item.getMmNm(), "7", mMainActivityHandler);
                                    }
//                                    Fragment_Main.getInstance().chatListUpdate();

                                    dialog_moim_friend_join.dismiss();
                                } else if(i == R.id.ll_close){
                                    dialog_moim_friend_join.dismiss();
                                }
                            }
                        });
                        dialog_moim_friend_join.show();



                        break;
                    case 2:
                        Toast.makeText(getContext(), "가입이 차단 되었습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(getContext(), "가입대기 중 입니다", Toast.LENGTH_SHORT).show();
                        break;
                }
            } else if (!mIsSearchingFriends) {
                Intent mIntent = new Intent(getActivity(), Activity_Profile.class);
                mIntent.putExtras(bundle);
                startActivity(mIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return false;
    }

    public void proChatRequest(String roomid) {
        Moa.chatRoomInfoRequest(getContext(), roomid, mMainActivityHandler);
    }

    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());


                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_CHATROOM_INFO:
                        strResult = (String) data.body.get("result");
                        if (strResult.equals(Constants.SUCCESS)) {

                            try {
                                Statics.ROOMINFO = getRoomInfo(data);
                                intentChat();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_APPLY:    //모임 가입
                        if (data.body.get("result").equals(Constants.SUCCESS)) {

                            String reason = (String) data.body.get("reason");
                            Fragment_Main.getInstance().FriendListRequest();
                            if (reason.equals("apply")) {
                                Toast.makeText(getActivity(), "가입신청을 완료하였습니다.", Toast.LENGTH_SHORT).show();
                                Fragment_Main.getInstance().FriendListRequest();
                            } else {
                                //완료 표시
                            }

                        } else {

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("already_applied")) {
                                Toast.makeText(getActivity(), "이미 가입신청함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_exist")) {
                                Toast.makeText(getActivity(), "이미 가입함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_blocked")) {
                                Toast.makeText(getActivity(), "차단됨", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("wrong_moim_password")) {
                                Toast.makeText(getActivity(), "비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        break;
                    case 0x00060008:
                        if (data.body.get("result").equals(Constants.SUCCESS)) {

                            Toast.makeText(getActivity(), "삭제하였습니다.", Toast.LENGTH_SHORT).show();
                            Fragment_Main.getInstance().FriendListRequest();

                        }
                        break;

                    case PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM:
                        String strResult2 = (String) data.body.get("result");

                        if (strResult2.equals(Constants.SUCCESS)) {

                            Intent mIntent;
                            Statics.ROOMINFO = getRoomInfo(data, roomName);
                            mIntent = new Intent(getContext(), Activity_ChatRoom.class);
                            startActivity(mIntent);

                        } else {

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    private VoChatList getRoomInfo(MOAData data) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            LBJSONArray j1 = data.body.getArray("users");
            ArrayList<VoFriendList> list = new ArrayList<>();

            for (int i = 0; i < j1.size(); i++) {
                LBJSONObject jo2 = (LBJSONObject) j1.get(i);
                VoFriendList item = VoFriendList.loadFromJsonChatUsers(jo2);
                list.add(item);
            }

            info.getFriends().addAll(list);
        }
        return info;
    }

    private VoChatList getRoomInfo(MOAData data, String roomName) {
        String result = data.body.getString("result");
        VoChatList info = new VoChatList();

        if ("success".equals(result)) {
            LBJSONObject jo = data.body.getJson("params");
            info = VoChatList.loadFromJson(jo);
            info.setRoom_name(roomName);
            info.setRoom_type("7");

        }
        return info;
    }

    private void intentChat() {
        Intent intent = new Intent(getContext(), Activity_ChatRoom.class);
        startActivity(intent);
    }

    /**
     * 친구리스트 헤더뷰 클릭 이벤트
     */
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

        FriendListViewAdapter adapter = (FriendListViewAdapter) parent.getExpandableListAdapter();
        VoFriendList chekItem = childList.get(groupPosition);
        ArrayList<VoFriendList> voFriendList = adapter.getGroupChildList(groupPosition);
        ArrayList<String> userList = new ArrayList<>();
        if (chekItem.getUserName().equals(Constants.LISTTYPE_PROFESSIONALFRIEND)) {
            Intent mIntent = new Intent(getActivity(), Activity_ProfessionalList.class);
            startActivity(mIntent);
        } else if (chekItem.getUserName().equals(Constants.LISTTYPE_MYFRIEND)) {
            for (int i = 0; i < voFriendList.size(); i++) {
                userList.add(voFriendList.get(i).getUserId());
            }
        } else if (chekItem.getUserName().equals(Constants.LISTTYPE_CUSTOMER)) {
            MessengerInterface.voFriendLists = voFriendList;
            Intent mIntent = new Intent(getActivity(), Activity_CustomerList.class);
//            mIntent.putExtra("customerList", voFriendList);
            startActivity(mIntent);
        } else {
            if (!chekItem.getUserName().equals(Constants.LISTTYPE_PROFESSIONALFRIEND) && !chekItem.getUserName().equals(Constants.LISTTYPE_MYPROFESSIONALFRIEND)) {
                if (chekItem.getList().size() > 0) {
                    for (int i = 0; i < voFriendList.size(); i++) {
                        userList.add(voFriendList.get(i).getUserId());
                    }

                    VoChatList chatRoomInfo = new VoChatList();
                    chatRoomInfo.setRoom_name(chekItem.getUserName());
                    chatRoomInfo.setRoom_type("6");
                    chatRoomInfo.setGid(chekItem.getgId());
                    List<VoFriendList> friends = new ArrayList<>();
                    List<VoChatData> chats = new ArrayList<>();
                    VoFriendList my = new VoFriendList();
                    my.setUserId(MessengerInfo.getUserId(context));
                    my.setUserName(MessengerInfo.getUserName(context));
                    my.setRealUserName(MessengerInfo.getRealUserName(context));
                    friends.add(my);

                    String roomId;
                    chatRoomInfo.setFriends(friends, context);
                    chatRoomInfo.setChats(chats);
                    if (chekItem.getRmId() != null) {
                        roomId = chekItem.getRmId();
                        chatRoomInfo.setRoomId(roomId);
                    }

                    Statics.ROOMINFO = chatRoomInfo;

                    Intent mIntent = new Intent(context, Activity_ChatRoom.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(mIntent);
                }
            }
        }

        return true;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, final View view, int position, long id) {
        int itemType = ExpandableListView.getPackedPositionType(id);
        boolean retVal = true;

        if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            final int childPosition = ExpandableListView.getPackedPositionChild(id);
            final int groupPosition = ExpandableListView.getPackedPositionGroup(id);

            final VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
            final VoFriendList chekItem = childList.get(groupPosition);

            if (chekItem.getUserName().equals(Constants.LISTTYPE_PROFESSIONALFRIEND)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_BOOKMARK)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_MYPROFILE)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_NEWFRIEND)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_RECOMMENDFRIEND)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_MYPROFESSIONALFRIEND)) {
            } else if (chekItem.getUserName().equals(Constants.LISTTYPE_MYFRIEND)) {
                final Dialog_Friend dialog_friend = new Dialog_Friend(context);
                dialog_friend.show();

                dialog_friend.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_friend.dismiss();

                        int i = v.getId();
                        if (i == R.id.tv_option0) {
                            hideFriend(context, item, "1");
                        } else if (i == R.id.tv_option1) {
                            blockFriend(context, item, "1");
                        }
                    }
                });

            } else {
                getAlert().setTitle("SSAM")
                        .setMessage("그룹에서 해제 하시겠습니까?")
                        .setPositiveButton("해제", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(getContext(), item.getUserName() + "삭제", Toast.LENGTH_SHORT).show();
                                Moa.GroupDelete(getActivity(),Integer.parseInt(chekItem.getgId()), Integer.parseInt(item.getBid()), Integer.parseInt(item.getUserId()), mMainActivityHandler);

                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
            }


//            getAlert().setTitle("SSAM")
//                    .setMessage("그룹에서 해제 하시겠습니까?")
//                    .setPositiveButton("해제", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Toast.makeText(getContext(), item.getUserName() + "삭제", Toast.LENGTH_SHORT).show();
//
//                        }
//                    })
//                    .setNegativeButton("취소", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    }).show();
            return retVal;

        } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
            int groupPosition = ExpandableListView.getPackedPositionGroup(id);

            return retVal;

        } else {
            return false;
        }
    }


    public void hideFriend(Context context, VoFriendList friendToHide, String mode) {
        try {
            if (!friendToHide.isMember()) {
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
                db.hideBuddy(friendToHide.getUserId());
                return;
            }

            String phonenum = friendToHide.getPhone().replaceAll("-", "");

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", phonenum);
//            value.put("users", friendToHide.getUserId());
            value.put("mode", mode);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);

            //db.hideBuddy(friendToHide.getUserId());
//            if(mode.equals("1")) //숨기기
            db.hideBuddy(friendToHide.getUserId());
//            else                // 숨기기취소 0
//                db.unHideBuddy(friendToHide.getUserId(),null);

            Fragment_Main.getInstance().FriendListRequest();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            ErrorController.showMessage("hide : " + friendToHide.getUserName());
        }
    }

    public void blockFriend(Context context, VoFriendList friendToBlock, String mode) {
        //Callback to : view.onBlockSuccess, onBlockFailed
        try {
            if (!friendToBlock.isMember()) {
                BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);
                db.blockBuddy(friendToBlock.getUserId());
                return;
            }
            String phonenum = friendToBlock.getPhone().replaceAll("-", "");
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", phonenum);
//            value.put("users", friendToBlock.getUserId());
            value.put("mode", mode);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS,
                    PacketTypes.PTC_IMS_ADDRESS_BLOCK, body);
            BuddyDbHelper db = LocalDB.getBuddyDbHelper(context);

            //db.blockBuddy(friendToBlock.getUserId());
//            if(mode.equals("1")) //차단
            db.blockBuddy(friendToBlock.getUserId());
//            else                //차단취소
//                db.unblockBuddy(friendToBlock.getUserId(),null);
            Fragment_Main.getInstance().FriendListRequest();

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            ErrorController.showMessage("block : " + friendToBlock.getUserName());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    /**
     * 친구리스트 TextEdit 텍스트 변경 이벤트 처리
     */
    @Override
    public void afterTextChanged(Editable s) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                searchText();
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!mIsSearchingFriends) {
//            Fragment_Main.getInstance().FriendListRequest();
//        }
    }

    /**
     * 모임 가입하기 다이얼로그
     * 현재 질문가입형
     * <p>
     * 0:승인, 1:자동가입(기본), 2:비밀번호
     */
    private void showDialogJoin(final VoFriendList param) {
        switch (param.getEntTy()) {
            case 1:  //팝업 필요없이 바로 가입
                Moa.moimApplyProfessional(getActivity(), "", "", mMainActivityHandler, param.getMmId());
                break;
            case 0:  //승인 질문입력 팝업
            case 2: //비밀번호 팝업
                //TODO
                final Dialog_MoimJoinType dialog_MakeMoim = new Dialog_MoimJoinType(getActivity(), param); //param.getQstn() getEntTy
                dialog_MakeMoim.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int id = view.getId();

                        if (id == R.id.tv_cancle) {
                            dialog_MakeMoim.dismiss();
                        } else if (id == R.id.tv_confirm) {
                            dialog_MakeMoim.getJoinTxt();   //비밀번호

                            String answ = null;   //모임 가입질문
                            String mmPw = null;  //모임 가입비밀번호

                            if (param.getEntTy() == 0) { //리더승인이면
                                answ = dialog_MakeMoim.getJoinTxt();   //가입질문

                            } else if (param.getEntTy() == 2) {
                                mmPw = dialog_MakeMoim.getJoinTxt();   //비밀번호
                            }

                            Util.hideKeyboard(getActivity(), ((EditText) dialog_MakeMoim.findViewById(R.id.edt_input)));
                            dialog_MakeMoim.dismiss();

                            Moa.moimApplyProfessional(getActivity(), answ, mmPw, mMainActivityHandler, param.getMmId());

                        }

                    }
                });
                dialog_MakeMoim.show();
                break;
        }
    }
}
