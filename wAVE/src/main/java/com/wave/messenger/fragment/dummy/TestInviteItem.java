package com.wave.messenger.fragment.dummy;

/**
 * Created by apple on 2017. 3. 23..
 */

public class TestInviteItem  implements Comparable<Item_LeaderProfile>{

    String idContact;
    String name;
    String Phone;
    String photo;
    boolean isSelected;

    public TestInviteItem(String idContact, String name, String phone, String photo, boolean isSelected) {
        this.idContact = idContact;
        this.name = name;
        Phone = phone;
        this.photo = photo;
        this.isSelected = isSelected;
    }


    public String getIdContact() {
        return idContact;
    }

    public void setIdContact(String idContact) {
        this.idContact = idContact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "TestInviteItem{" +
                "idContact='" + idContact + '\'' +
                ", name='" + name + '\'' +
                ", Phone='" + Phone + '\'' +
                ", photo='" + photo + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }

    @Override
    public int compareTo(Item_LeaderProfile another) {
        return this.name.compareTo(another.getFriend());
    }


}
