package com.wave.messenger.fragment.ChatRoom;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_InviteFriend;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.adapter.OpenUserAdapter;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoUsers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class Fragment_OpenNavigation extends Fragment {

    private ImageView iv_share, iv_roomprofile, iv_exit, iv_alarm, iv_profile, iv_jongmok;
    private TextView tv_roomname, tv_roomcomment, tv_userlist, tv_moimmove;
    private Button bt_kick, bt_staff;
    private RecyclerView rv_userlist;
    private OpenUserAdapter adapter;
    private VoChatList roomInfo;
    private LinearLayout  ll_userlist, ll_download, llAddNewUser;  //ll_profitshare, ll_jongmoksearch, ll_signalshare,ll_portfolio,
    private View ul_hideview;
    private boolean isStaff;
    private ScrollView scrollView;

    private int count;

    private Intent mIntent;
    Uri fileUri;
    private List<VoFriendList> friendList;
    private int version;

    public interface KickListener {
        void kick(String userId, String userName);
    }

    private KickListener kickListener = new KickListener() {
        @Override
        public void kick(String userId, String userName) {
            kickUser(userId, userName);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_opennavigation, container, false);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            version = 1;
        }else{
            version = 0;
        }

        roomInfo = (VoChatList) getArguments().getSerializable("roomInfo");
        isStaff = getArguments().getBoolean("isStaff");
        //mIntent = new Intent(getActivity(), Activity_UserManage.class);
        friendList = new ArrayList<>();
        init(v);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressLint("NewApi")
    private void init(View v) {
//        iv_share = (ImageView) v.findViewById(R.id.iv_share);
//        iv_roomprofile = (ImageView) v.findViewById(R.id.iv_roomprofile);
        tv_roomname = (TextView) v.findViewById(R.id.tv_roomname);
//        tv_roomcomment = (TextView) v.findViewById(R.id.tv_roomcomment);
        bt_kick = (Button) v.findViewById(R.id.bt_kick);
        bt_staff = (Button) v.findViewById(R.id.bt_staff);
        rv_userlist = (RecyclerView) v.findViewById(R.id.rv_userlist);
        tv_userlist = (TextView) v.findViewById(R.id.tv_userlist);
        iv_exit = (ImageView) v.findViewById(R.id.iv_exit);
        iv_alarm = (ImageView) v.findViewById(R.id.iv_alarm);
        tv_moimmove = (TextView) v.findViewById(R.id.tv_moimmove);
        //증권메뉴
        //ll_profitshare = (LinearLayout) v.findViewById(R.id.ll_profitshare);
        //ll_jongmoksearch = (LinearLayout) v.findViewById(R.id.ll_jongmoksearch);
        //ll_signalshare = (LinearLayout) v.findViewById(R.id.ll_signalshare);
        ll_download = (LinearLayout) v.findViewById(R.id.ll_download);
        //ll_portfolio = (LinearLayout) v.findViewById(R.id.ll_portfolio);
        iv_profile = (ImageView) v.findViewById(R.id.iv_profile);
        iv_jongmok = (ImageView) v.findViewById(R.id.iv_jongmok);
        ll_userlist = (LinearLayout) v.findViewById(R.id.ll_userlist);
        ul_hideview = (View) v.findViewById(R.id.ul_hideview);
        llAddNewUser = (LinearLayout) v.findViewById(R.id.llAddNewUser);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);

        if (roomInfo.getRoom_type().equals("8")) {
            String itemCode = roomInfo.getRoomId();
            String url = Constants.CHATDAWN_PROC + "?type=9&serverfile=" + itemCode.substring(4, 10);
            Glide.with(this).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_default_profile_candle_img).into(iv_jongmok);
            iv_profile.setVisibility(View.GONE);
            iv_jongmok.setVisibility(View.VISIBLE);
            tv_roomname.setText(roomInfo.getRoom_name());
            tv_moimmove.setVisibility(View.GONE);
            llAddNewUser.setVisibility(View.VISIBLE);
            //ll_portfolio.setVisibility(View.GONE);
        } else {
            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + roomInfo.getOwnerId();
            Glide.with(this).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_master_default_profile).into(iv_profile);
            iv_profile.setVisibility(View.VISIBLE);
            iv_jongmok.setVisibility(View.GONE);
            tv_roomname.setText(roomInfo.getRoom_name());
            tv_moimmove.setVisibility(View.VISIBLE);
            llAddNewUser.setVisibility(View.GONE);
        }
//        tv_roomcomment.setText(roomInfo.getRoomComment());

        if (roomInfo.getOwnerId().equals(MessengerInfo.getUserId(getActivity()))) {
            bt_staff.setVisibility(View.VISIBLE);
        }

        if (isStaff) {
            bt_kick.setVisibility(View.VISIBLE);
            tv_userlist.setVisibility(View.GONE);
            rv_userlist.setVisibility(View.VISIBLE);
        } else {
            bt_kick.setVisibility(View.GONE);
            rv_userlist.setVisibility(View.VISIBLE);
            tv_userlist.setVisibility(View.VISIBLE);
        }

        rv_userlist.setLayoutManager(new LinearLayoutManager(getActivity()));



        adapter = new OpenUserAdapter(getActivity());
        rv_userlist.setAdapter(adapter);

        if (roomInfo.getRoom_type().equals("7")) {
            rv_userlist.setVisibility(View.VISIBLE);
            iv_exit.setVisibility(View.VISIBLE);
            ll_userlist.setVisibility(View.VISIBLE);
        } else if (roomInfo.getRoom_type().equals("8")) {
            rv_userlist.setVisibility(View.VISIBLE);
            iv_exit.setVisibility(View.VISIBLE);
            ll_userlist.setVisibility(View.VISIBLE);
        } else {
            rv_userlist.setVisibility(View.VISIBLE);
            iv_exit.setVisibility(View.VISIBLE);
            ll_userlist.setVisibility(View.VISIBLE);
        }
        setEvent();
        setListView(version);

        if(version == 1) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

                    if (diff == 0) {
                        count += 10;
                        loadNextDataFromApi(count);
                    }
                }
            });
        }
        rv_userlist.setFocusable(false);
    }

    private void setEvent() {
//        iv_share.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shareLink();
//            }
//        });

        if ("1".equals(Statics.ROOMINFO.getUseNoti()) || TextUtils.isEmpty(Statics.ROOMINFO.getUseNoti())) {
            iv_alarm.setSelected(false);
        } else {
            iv_alarm.setSelected(true);
        }

       /* ll_profitshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "준비중입니다!", Toast.LENGTH_SHORT).show();
//                MessengerInterface.getInstance().onOpenMTS(Activity_ChatRoom.getInstance(), 1, "5000", "");
                String mag = "MTS의 수익률 자랑하기 화면으로 \n이동하시겠습니까?";

                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MessengerInterface.getInstance().onOpenMTS(Activity_ChatRoom.getInstance(), 1, "5000", "");
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.setCancelEvent(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.show();
            }
        });

        ll_jongmoksearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_ChatRoom.getInstance().jongmokSearch();
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        ll_signalshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_ChatRoom.getInstance().signalShare();
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        ll_portfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = MessengerInfo.getUserId(getContext());
                String roomNum = roomInfo.getRoomId();
                if (MessengerInfo.getUserId(getContext()).equals(roomInfo.getOwnerId())) {
                    MessengerInterface.getInstance().openScreenPotfolio(0, userId, roomNum);
                } else {
                    MessengerInterface.getInstance().openScreenPotfolio(1, roomInfo.getOwnerId(), roomNum);
                }
            }
        });*/

        bt_kick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIntent.putExtra("title", getString(R.string.kick_user));
                mIntent.putExtra("kick", true);
                getActivity().startActivity(mIntent);
            }
        });

        bt_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIntent.putExtra("title", getString(R.string.staff));
                mIntent.putExtra("kick", false);
                getActivity().startActivityForResult(mIntent, Constants.REQUEST_CHAT_STAFF);
            }
        });

        iv_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlert().setTitle("채팅방 나가기")
                        .setMessage("채팅방을 나가게 되면 모든 대화내용이 삭제되며,삭제된 대화내용은 복구되지 않습니다.")
                        .setNeutralButton("삭제하기", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if ("3".equals(roomInfo.getRoom_type())) {
                                    exitChatRoom(roomInfo.getRoomId());
                                } else {
                                    exitChatRoom(roomInfo.getRoomId());
                                }
                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();
            }
        });

        iv_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_alarm.setSelected(!iv_alarm.isSelected());
                String roomId = roomInfo.getRoomId();
                if (iv_alarm.isSelected()) {
                    setAlarm("0", roomId);
                } else {
                    setAlarm("1", roomId);
                }
            }
        });
        tv_moimmove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String moimId = roomInfo.getMmid();
                SharedObject.setProperty_string(getContext(), Constants.MOIM_ID, moimId);  //모임아이디 저장

                //내모임으로 이동.
                Intent intent = new Intent(getContext(), Activity_MoimTimeLineList.class);
                startActivity(intent);

            }
        });

        ll_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File path = new File("/sdcard/time_prime");
                if (!path.isDirectory()) {
                    path.mkdirs();
                }

                File savefile = new File(path.getAbsolutePath() + "/피기맘 대화내용" + ".txt");
                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getActivity());
                List<VoChatData> list = db.getChatData(roomInfo.getRoomId());

                fileUri = Uri.fromFile(savefile);

                try {
                    if (!savefile.exists())
                        savefile.createNewFile();

                    BufferedWriter reader = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(savefile), "euc-kr"));

                    for (int i = 0; i < list.size(); i++) {
                        String date = "[" + list.get(i).getReg_date().toString() + "] ";
                        String name = list.get(i).getSenderName().toString() + " : ";
                        String msg = "";
                        if (list.get(i).getTitle().equals("(이모티콘)") || list.get(i).getTitle().equals("(사진)"))
                            msg = list.get(i).getTitle().toString();
                        else
                            msg = list.get(i).getMessage().toString();
                        String textfile = date + name + msg;

                        reader.write(textfile);
                        reader.write("\r\n");
                    }

                    reader.flush();
                    reader.close();

                    sendEmailOrMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });

        llAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (friendList == null) {
                    friendList = new ArrayList<>();
                }

                Intent intent = new Intent(getActivity(), Activity_InviteFriend.class);
                getActivity().startActivityForResult(intent, Constants.INVITE_FRIEND_CHAT_REQ);
                Activity_ChatRoom.getInstance().openMenuRight();
            }
        });


    }

    protected void sendEmailOrMessage() {
        Uri uri_email = Uri.parse("mailto:");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri_email);
        i.putExtra(Intent.EXTRA_STREAM, fileUri);
        i.putExtra(Intent.EXTRA_SUBJECT, "피기맘 대화내용");
        i.putExtra(Intent.EXTRA_TEXT, "피기맘 대화내용입니다.");

        getActivity().startActivity(i);
    }

    /**
     * 페이지 로딩
     *
     * @param
     */
    public void loadNextDataFromApi(int count) {

        UsersDbHelper db = LocalDB.getUsersDbHelper(getActivity());
        ArrayList<VoUsers> list = db.getUserListLimit(roomInfo.getRoomId(), count);

//        friendList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            VoFriendList item = new VoFriendList();
            item.setUserId(list.get(i).getUserId());
            item.setUserName(list.get(i).getUserName());
            item.setPhone(list.get(i).getTel());
            item.setRealUserName(list.get(i).getRealUserName());
            item.setStaff(list.get(i).getStaff());
            friendList.add(item);
        }

        adapter.setList(friendList, kickListener);
        adapter.notifyDataSetChanged();

//        Statics.ROOMINFO.setFriends(friendList, getActivity());

//        tv_userlist.setText(getString(R.string.open_usercount, roomInfo.getUsercount()));
    }

    public void setListView(int version) {
        if (TextUtils.isEmpty(roomInfo.getRoomId())) {
            friendList = roomInfo.getFriends();
            adapter.setList(friendList, kickListener);

        } else {
            UsersDbHelper db = LocalDB.getUsersDbHelper(getActivity());
            ArrayList<VoUsers> list;
            if(version == 1) {
                 list = db.getUserListLimit(roomInfo.getRoomId(), 0);
            }else {
                list = db.getUserList(roomInfo.getRoomId());
            }
//            friendList = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                VoFriendList item = new VoFriendList();
                item.setUserId(list.get(i).getUserId());
                item.setUserName(list.get(i).getUserName());
                item.setPhone(list.get(i).getTel());
                item.setRealUserName(list.get(i).getRealUserName());
                item.setStaff(list.get(i).getStaff());
                friendList.add(item);
            }

            adapter.setList(friendList, kickListener);
            adapter.notifyDataSetChanged();
//            Statics.ROOMINFO.setFriends(friendList, getActivity());
        }

//        tv_userlist.setText(getString(R.string.open_usercount, Integer.parseInt(Statics.ROOMINFO.getUsercount())));
        tv_userlist.setText("참여자 목록");

    }

    public void updateView(String staff) {
        LogTrace.E("update staff : " + staff);
        if ("1".equals(staff)) {
            bt_kick.setVisibility(View.VISIBLE);
            tv_userlist.setVisibility(View.GONE);
            rv_userlist.setVisibility(View.VISIBLE);
        } else {
            bt_kick.setVisibility(View.GONE);
            tv_userlist.setVisibility(View.VISIBLE);
            rv_userlist.setVisibility(View.GONE);
        }

        setListView(version);
    }

    public void kickResult() {
        LogTrace.E("kickResult");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                setListView();
            }
        });
    }

    private void kickUser(String userId, String userName) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("roomId", roomInfo.getRoomId());
            value.put("userOutId", userId);
            value.put("roomUserName", userName);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_USEROUT, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void shareLink() {
        //Dialog_Common.getShareDialog(getActivity(), Constants.OPEN_LINK + roomInfo.getRoomShortId()).show();

        /*Bundle bundle = new Bundle();
        bundle.putString("messageType", "0");
        bundle.putString("message", Constants.OPEN_LINK + roomInfo.getRoomShortId());
        bundle.putString("attachment", "");
        Intent intent = new Intent(getActivity(), Activity_Delivery.class);
        intent.putExtras(bundle);
        startActivity(intent);*/
    }

    public void exitChatRoom(String roomId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getActivity()));
            value.put("roomId", roomId);
            value.put("exitUserId", MessengerInfo.getUserId(getActivity()));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_EXIT, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAlarm(String alarm, String roomId) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(getActivity()));
            value.put("roomId", roomId);
            value.put("deviceType", "android");
            value.put("useNoti", alarm);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_NOTI_CONFIG, body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AlertDialog.Builder getAlert() {
        int version = Integer.parseInt((Build.VERSION.RELEASE).substring(0, 1));
        AlertDialog.Builder alert;
        if (version >= 3) {
            alert = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        } else {
            alert = new AlertDialog.Builder(getActivity());
        }
        return alert;
    }
}
