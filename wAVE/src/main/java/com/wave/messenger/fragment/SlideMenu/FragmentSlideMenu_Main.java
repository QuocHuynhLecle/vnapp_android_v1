package com.wave.messenger.fragment.SlideMenu;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.utility.LogTrace;

public class FragmentSlideMenu_Main extends Fragment {
	private static final String TAG = "FragmentSlideMenu_Main";
	static FragmentSlideMenu_Main instance = null;
	public static FragmentSlideMenu_Main getInstance() {
		return instance;
	}
	
	public static void clearInstance(FragmentSlideMenu_Main value) {
		if(value == instance) {
			instance = null;
		}
	}
	
	private LinearLayout rootView;
	ImageView imageViewClose;
	View mtsView, mtsView2;
	int mtsWidth, mtsHeight;
	LinearLayout formView;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {              // 네비게이터에 리스트뷰를 셋한다
    	try {
    		
    		instance = this;
    		
//    		mlFragmentMain = Fragment_Main.getInstance();
    		
    		rootView = (LinearLayout) inflater.inflate(R.layout.fragment_slide_menu_main, container, false);
    		
    		formView = (LinearLayout) rootView.findViewById(R.id.layout_formView);
    		
    		initView(rootView);
//			setEvent();

			//JYJO(20170621) - 231 -> 280 변경
			//loadScreen("M9999M00", "");
			mtsView = Fragment_Main.getInstance().mtsView;
			mtsView2 = Fragment_Main.getInstance().mtsView2;

			mtsWidth = Fragment_Main.getInstance().mtsWidth;
			mtsHeight = Fragment_Main.getInstance().mtsHeight;

			formView.addView(mtsView, mtsWidth, mtsHeight);

			rootView.requestLayout();
    	} catch (Exception e) {
			// TODO: handle exception
			LogTrace.E("FragmentSlideMenu_Main ERROR : " + e);

		}
    	return rootView;
	}

	public void mtsData(View mtsView, int width, int height) {

	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		clearInstance(instance);
		formView.removeAllViews();
	}
	
	public void initView(View view) {
		imageViewClose = (ImageView)rootView.findViewById(R.id.imageViewClose);
//		setDisplay();
	}
	
	public void setEvent() {
		
		imageViewClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					openMenu();
				} catch (Exception e) {
					// TODO: handle exception
					
				}
			}
		});
	}

	private Activity mCallbacks;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }
	
	public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

	public void setDisplay(Context mContext, String screen, String itemCode) {
	}

	public void setUp(View parent, int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = parent.findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
    }
    
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private ActionBar getActionBar() {
		return Fragment_Main.getInstance().getActivity().getActionBar();
    }

	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (Activity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public void openMenu() {
    	if(isDrawerOpen()) {
    		mDrawerLayout.closeDrawer(mFragmentContainerView);
    	} else {
    		mDrawerLayout.openDrawer(mFragmentContainerView);
    	}
    }
	
}
