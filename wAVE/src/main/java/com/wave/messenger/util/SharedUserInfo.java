package com.wave.messenger.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Joo on 2018. 4. 20.
 */

public class SharedUserInfo {
    public static final String TAG = SharedUserInfo.class.getSimpleName();
    private static SharedUserInfo sharedUserInfo;         // 싱글톤
    private Context context;
    private SharedPreferences userInfo;             // 사용자 정보
    private SharedPreferences.Editor editor;        // SharedPreferences 수정자
//    private String secretKey;

    public static SharedUserInfo getInstance(Context context) {
        if (sharedUserInfo == null) {
            sharedUserInfo = new SharedUserInfo(context);
        }
        return sharedUserInfo;
    }

    // 최초 생성
    private SharedUserInfo(Context context) {
        this.context = context;
        userInfo = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    }

    public String getRegId() {
        if (userInfo.getString("RegId", null) != null) {
            return userInfo.getString("RegId", null);
        }
        return "";
    }

    public void setRegId(String value) {
        editor = userInfo.edit();
        editor.putString("RegId", value);
        editor.apply();
    }


    public String getUserId() {
        if (userInfo.getString("UserId", null) != null) {
            return userInfo.getString("UserId", null);
        }
        return "";
    }

    public void setUserId(String value) {
        editor = userInfo.edit();
        editor.putString("UserId", value);
        editor.apply();
    }

    public String getUserPassWd() {
        if (userInfo.getString("PassWd", null) != null) {
            return userInfo.getString("PassWd", null);
        }
        return "";
    }

    public void setUserPassWd(String value) {
        editor = userInfo.edit();
        editor.putString("PassWd", value);
        editor.apply();
    }

    public int getAutoLoginFlag() {
        int autoLogin = userInfo.getInt("AutoLogin", -1);
        if (autoLogin == 1) {
            return 1;
        }
        return 0;
    }

    public void setAutoLoginFlag(int value) {
        editor = userInfo.edit();
        editor.putInt("AutoLogin", value);
        editor.apply();
    }

    public int getSaveIdFlag() {
        int saveId = userInfo.getInt("SaveId", -1);
        if (saveId == 1) {
            return 1;
        }
        return 0;
    }

    public void setSaveIdFlag(int value) {
        editor = userInfo.edit();
        editor.putInt("SaveId", value);
        editor.apply();
    }

    public int getAlaramCount() {
//        int alarmCount = userInfo.getInt("AlarmCount", -1);
//        if (alarmCount != -1) {
//            return alarmCount;
//        }
        return userInfo.getInt("AlarmCount", -1);
    }
    public void setAlarmCount(int value) {
        editor = userInfo.edit();
        editor.putInt("AlarmCount", value);
        editor.apply();
    }

    public void clearInfo() {
        editor.clear();
        editor.commit();
    }

}
