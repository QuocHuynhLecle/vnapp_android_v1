package com.wave.messenger.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;

import com.wave.messenger.db.LocalDB;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoContact;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aveapp on 2017. 5. 10..
 */

public class LocalContactData {
    private static ConcurrentHashMap<String, VoContact> sContactMap = new ConcurrentHashMap<>();

    public static String getMatchingNum(String phoneNum) {
        String result = "";

        try {
            for (int i = phoneNum.length() - 1; i > -1; i--) {
                char ch = phoneNum.charAt(i);

                if (Character.isDigit(ch)) {
                    result = ch + result;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return result;
    }

    static String StringReplace(String str) {
        String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
        str = str.replaceAll(match, " ");
        return str;
    }

    /**
     * 휴대폰 번호인지 체크
     *
     * @param phoneNumber 휴대폰 번호
     * @return 휴대폰 번호(010, 011, 016, 017, 019)이면 true
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return false;
        }

        phoneNumber = phoneNumber.replace("-", "").replace(".", "");

        String regex = "^(01[0|1|6|7|8|9])(\\d{3}|\\d{4})(\\d{4})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(phoneNumber);

        return match.find();
    }

    /**
     * 주소록 총 개수 조회
     *
     * @param context Context
     * @return 개수
     */
    public static int getContactCount(Context context) {
        int result = 0;

        Cursor cursor = null;

        try {
            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            ContentResolver resolver = context.getContentResolver();

            if (resolver != null) {
                cursor = resolver.query(uri, null, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getCount();
                }
            }
        } catch (Exception ignored) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return result;
    }

    public static List<VoContact> getContactList(Context context) {
        Cursor cursor = null;

        try {
            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.NUMBER,        // 연락처
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME}; // 연락처 이름.

            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    + " COLLATE LOCALIZED ASC";

            ContentResolver resolver = context.getContentResolver();

            if (resolver != null) {
                cursor = resolver.query(uri, projection, null, null, sortOrder);

                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String number = cursor.getString(0);

                        if (!TextUtils.isEmpty(number)) {
                            String name = StringReplace(cursor.getString(1));

                            LocalDB.getPhoneDbHelper(context).checkList(number, name);
                        }

                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception ignored) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    // 수동 친구 추가는 모든 주소록 동기화, 자동 친구 추가는 기존 저장된 주소록에서 바뀐 부분만 동기화
    public static JSONArray loadContactData(Context context, boolean allSync) {
        JSONArray jArray = new JSONArray();

        HashMap<String, String> hashMap;

        try {
            // 이전에 전송한 주소록을 파일로 직렬화 한 뒤 주소록에서 변경한 파일만 업로드 하도록 변경
            // 단 친구 추가에서 호출할 경우는 모두 동기화 시킨다.
            if (allSync) {
                LogTrace.E("first login");
                hashMap = LocalDB.getPhoneDbHelper(context).selectAll();
            } else {
                LogTrace.E("not first login");
                hashMap = LocalDB.getPhoneDbHelper(context).select();
            }

            // 중복 전화번호를 제거하기 위한 Set
            Set<String> phoneNumberSet = new HashSet<>();

            for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                String number = entry.getKey();
                String name = entry.getValue();

                if (!TextUtils.isEmpty(name) && isPhoneNumber(number)) {
                    try {
                        // 중복 제거
                        if (!phoneNumberSet.contains(number)) {

                            JSONObject jo = new JSONObject();
                            jo.put("addrPhone", number);
                            jo.put("addrName", name);

                            jArray.put(jo);

                            phoneNumberSet.add(number);
                        }
                    } catch (Exception ignored) {
                    }
                }
            }

        } catch (Exception ignored) {
        }

        LogTrace.E("jArray : " + jArray.length());

        return jArray;
    }

    // 직렬화 로직
    public static void serializeContactList(Context context) {
        String savePath = context.getCacheDir().getAbsolutePath() + File.separator + "contactList";

        try {
            FileOutputStream fos = new FileOutputStream(savePath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(sContactMap);
            oos.close();
        } catch (IOException ignored) {
        } finally {
            clear();
        }
    }

    // 역직렬화 로직
    public static Object deserializeContactList(Context context) {
        Object result = null;

        String savePath = context.getCacheDir().getAbsolutePath() + File.separator + "contactList";

        try {
            FileInputStream fis = new FileInputStream(savePath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            result = ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException ignored) {
        }

        return result;
    }

    // 메모리 초기화
    public static void clear() {
        if (sContactMap != null) {
            sContactMap.clear();
            sContactMap = null;
        }
    }
}
