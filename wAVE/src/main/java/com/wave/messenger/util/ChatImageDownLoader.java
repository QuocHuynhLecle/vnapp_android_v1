package com.wave.messenger.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by user on 2016-09-01.
 */
public class ChatImageDownLoader extends AsyncTask<String, Void, Bitmap> {
    private String address = Constants.CHATDAWN_PROC;
    @Override
    protected Bitmap doInBackground(String... params) {

        try {
            String fullAddress = address + "type=" + params[0] + "&serverfile=" + params[1] + "&savefile=" + params[2];
            //ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

            URL url = new URL(fullAddress);
            URLConnection conn = url.openConnection();
            conn.connect();
            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
            Bitmap image = BitmapFactory.decodeStream(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
    }
}

