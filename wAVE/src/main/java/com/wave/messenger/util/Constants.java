package com.wave.messenger.util;

import moa.android.api.MOALog;

public class Constants {

    /**
     * SERVER_MODE로 서버 설정
     * true : 운영계, false : 개발계
     */
    public static boolean SERVER_MODE = false;
    public static final boolean DEBUG_MODE = true;

    static {
        //서버 주소 설정 및 디버그모드 설정
        setAddressInfo(SERVER_MODE, DEBUG_MODE);
    }

    public static String C2DM_SENDER_ID_MESSENGER;
    public static String ADDRESS;
    public static String IMG_DOWN_ADDRESS;
    public static String AUDIO_PROC;
    public static int AUDIO_PORT;
    public static String NOTICE_URL;
    public static String HELP_URL;
    public static String MOIM_INVITE_URL;
    public static String CHAT_INVITE_URL;

    public static void setAddressInfo(boolean isRealServer, boolean isDebugMode) {
        MOALog.MOA_DEBUG = isDebugMode;
        Constants.SERVER_MODE = isRealServer;

        if (isRealServer) {
            C2DM_SENDER_ID_MESSENGER = "121842800270";
            ADDRESS = "moa.aveapp.com:8601:ssl";
            IMG_DOWN_ADDRESS = "moa.aveapp.com:8680";
            AUDIO_PROC = "1.234.83.36";
            AUDIO_PORT = 9090;
            NOTICE_URL = "http://moa.aveapp.com:8681/candleman_notice/NoticeView.jsp";
            HELP_URL = "http://moa.aveapp.com:8680/candleman_help/NoticeView.jsp";
            MOIM_INVITE_URL = "http://moa.aveapp.com:8681/i/";
            CHAT_INVITE_URL = "http://moa.aveapp.com:8681/ssam_invite";
        } else {
            C2DM_SENDER_ID_MESSENGER = "121842800270";
            ADDRESS = "ims1.wave.aveapp.com:8101:ssl";
            IMG_DOWN_ADDRESS = "file1.wave.aveapp.com:8007";
            AUDIO_PROC = "1.234.83.36";
            AUDIO_PORT = 9090;
            NOTICE_URL = "http://file1.wave.aveapp.com:8007/notice/NoticeView.jsp";
            HELP_URL = "http://file1.wave.aveapp.com:8007/help/NoticeView.jsp\n";
            MOIM_INVITE_URL = "http://open.wave.aveapp.com:8007/i/";
            CHAT_INVITE_URL = "http://moa.aveapp.com:8681/ssam_invite";
        }
    }
    public static final String PACKAGE_NAME = "com.time.prime";
    public static final String TIMEPRIME_ACTIVITY = "com.lib.shi.messenger.HMMessengerActivity";

    public final static String LISTTYPE_MYPROFILE = "내 프로필";
    public final static String LISTTYPE_RECOMMENDFRIEND = "하나멤버스 추천 친구";
    public final static String LISTTYPE_MYPROFESSIONALFRIEND = "나의 스페셜 모임";
    public final static String LISTTYPE_PROFESSIONALFRIEND = "님을 위한 스페셜 모임";
    public final static String LISTTYPE_BOOKMARK = "즐겨찾기";
    public final static String LISTTYPE_MYFRIEND = "내 친구";
    public final static String LISTTYPE_NEWFRIEND = "새로운 친구";
    public final static String LISTTYPE_MYMANAGER = "담당직원(하나금융투자 관리 직원)";
    public final static String LISTTYPE_MYCUSTOMER = "내 관리손님";
    public final static String LISTTYPE_CUSTOMER = "내 미관리손님";

    public final static String CHATLISTTITLE_JONGMOK = "종목채팅방";
    public final static String CHATLISTTYPE_JONGMOK = "진행중인 종목 채팅 TOP50";
    public final static String CHATLISTTYPE_JONGMOK_ING = "내가 참여중인 종목채팅방";
    public final static String CHATLISTTYPE_MYCHAT = "내 채팅방";

    public final static String PROFESSIONAL_TITLE = "님을 위한 스페셜 모임";
    public final static String PROFESSIONAL_STOCK = "프렌드";
    public final static String PROFESSIONAL_ANALYST = "애널리스트";
    public final static String PROFESSIONAL_MANAGER = "매니저";
    public final static String PROFESSIONAL_LIST_MYPROFESSIONAL = "나의 프렌드 / 애널리스트";
    public final static String PROFESSIONAL_LIST_MANAGER = "매니저";
    public final static String PROFESSIONAL_LIST_ANALYST = "추천 애널리스트";
    public final static String PROFESSIONAL_LIST_PROFESSIONAL = "플러스 매니저/프렌드";
    public final static String PROFESSIONAL_LIST_PROFESSIONAL_GROUP = "그룹";
    public final static String PROFESSIONAL_LIST_PROFESSIONAL_NORMAL = "일반";

    public final static String PROFESSIONAL_MOIM_STOCK = "국내주식";
    public final static String PROFESSIONAL_MOIM_DERIVATION = "국내파생";
    public final static String PROFESSIONAL_MOIM_GLOBALDERIVATION = "해외파생";
    public final static String PROFESSIONAL_MOIM_GLOBALSTOCK = "해외주식";
    public final static String PROFESSIONAL_MOIM_BANKPRODUCT = "금융상품";
    public final static String PROFESSIONAL_MOIM_ETC = "기타모임";

    public static String CODE_HANA_MEMBERS = "하나멤버스";
    public static String CODE_ACCOUNT1 = "계좌개설";
    public static String CODE_ACCOUNT2 = "비대면계좌";
    public static String CODE_TISSUEGENE = "티슈진";
    public static String CODE_TISSUEGENE_NUM = "950160";
    public static String CODE_ETC001 = "레드존";
    public static String CODE_ETC002 = "투자의달인";
    public static String CODE_ETC003 = "RG";
    public static String CODE_ETC004 = "스톡봇";
    public static String CODE_ETC005 = "파봇";
    public static String CODE_ETC006 = "모닝스타";
    public static String CODE_ETC007 = "로보스탁";
    public static String CODE_ETC008 = "멘토스";

    public final static String DIALOG_TITLE_ADD = "그룹 추가";
    public final static String DIALOG_TITLE_EDIT = "그룹 수정";
    public final static String DIALOG_TITLE_DELETE = "그룹 삭제";

    public final static String ROOMTYPE_ONEANDONE = "0";
    public final static String ROOMTYPE_ONEANDN = "0";
    public final static String ROOMTYPE_OPEN = "0";
    public final static String ROOMTYPE_SHAREOPEN = "0";

    public enum FRAGMENT_MAIN_CALLBACK_SENDER {list, chat}

    public static String ADDRESS_INFO = "";
    public static String PROFILEUPLOAD_PROC = "http://" + IMG_DOWN_ADDRESS + "/file/api/profileupload_proc.jsp";
    public static String CHATUPLOAD_PROC = "http://" + IMG_DOWN_ADDRESS + "/file/api/fileupload_proc.jsp";
    public static String CHATDAWN_PROC = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp";
    public static String URL_IMAGE_BASE = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=1&serverfile=";

    public static String DEL_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/profiledelete_proc.jsp?userid=";
    public static String LUCKY_BAG_URL = "";

    //모임 커버 업로드
    public static String MOIM_PROFILE_IMAGE = "http://" + IMG_DOWN_ADDRESS + "/file/api/roomprofileupload_proc.jsp";
    //모임 프로필 다운로드 - 상단대표이미지
    public static String MOIM_LIST_IMAGE_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=7&serverfile=";

    //파일 다운로드
    public static String MOIM_DOWNLOAD_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=1&serverfile=";


    //모임 프로필 업로드URL TODO
    public static String PROFILE_IMAGE_UPLOAD_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/moimuserprofileupload_proc.jsp";

    //모임 프로필 다운로드URL -썸네일
    public static String PROFILE_IMAGE_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=3&userid=";


    // 모임 사용자 프로필 다운로드 시 파라메터 설정
    public static String MOIM_PROFILE_IMAGE_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=8&moimid=";


    //글쓰기 이미지 업로드
    public static String MOIM_IMAGE_UPLOAD = "http://" + IMG_DOWN_ADDRESS + "/file/api/fileupload_proc.jsp";
    //모임 타임라인 다운로드시 사용 -이미지
    public static String MOIM_TIMELINE_LIST_IMAGE_URL = "http://" + IMG_DOWN_ADDRESS + "/file/api/down_proc.jsp?type=1&serverfile=";

    //모임 초대링크
    //public static String MOIM_INVITE_LINK = "http://moa.aveapp.com:8681/i/";


    public final static String MOA_RESULT_OK = "success";
    public final static String MOA_RESULT_ERROR = "error";

    public final static String CODE_SUCCESS = "100";
    public final static String CODE_ERROR = "400";

    /**
     * Request Code for the Camera intent..
     */
    public static final int CAMERA_REQUEST = 102;

    public static final int GALLERY_REQUEST = 103;

    public static final int INVITE_FRIEND_CHAT_REQ = 104;
    public static final int INVITE_FRIEND_CHAT_REQ_NON_MEMBER = 108;

    public static final int RESULT_INVITE_ONLY_MEMBERS = 105;
    public static final int RESULT_INVITE_EVERYONE = 106;
    public static final int RESULT_INVITE_EVERYONE_CHANNEL_SELECTED = 107;

    /**
     * Request Code for Add Friend -> non member
     */
    public static final int REQUEST_INVITE_NONMEMBER_FRIEND = 109;
    public static final int RESULT_INVITE_NONMEMBER_FRIEND = 110;

    public static final int RESULT_CHANGE_PROFILE = 111;
    public static final int REQUEST_CHANGE_PROFILE = 1111;

    public static final int RESULT_CHAT_ALBUM = 112;
    public static final int REQUEST_CHAT_ALBUM = 1112;

    public static final int RESULT_CHAT_STAFF = 113;
    public static final int REQUEST_CHAT_STAFF = 1113;

    public static final int REQUEST_ADD_GROUP = 114;
    public static final int RESULT_ADD_GROUP = 1114;
    public static final int RESULT_ADD_FRIEND = 11114;

    /**
     * Request permission
     */
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 2001;
    public static final int MY_PERMISSIONS_READ_EXTERNAL_STORAGE = 2002;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2003;
    public static final int MAX_MEMBER_REQUEST = 203;

    public static final int MAX_MEMBER_50 = 50;
    public static final int MAX_MEMBER_100 = 100;
    public static final int MAX_MEMBER_500 = 500;
    public static final int MAX_MEMBER_1000 = 1000;
    public static final int MAX_MEMBER_3000 = 3000;
    public static final int MAX_MEMBER_5000 = 5000;
    public static final int MAX_MEMBER_10000 = 10000;

    public static final int RESULT_MOIM_WRITE = 207;
    public static final int RESULT_REPLY_MODIFY = 208;
    public static final int ACTIVITY_CHOOSE_FILE = 301;  //파일첨부

    /**
     * Request permission
     */
    public static final int RESULT_SPEECH = 120;

    //뷰타입 설정
    public static final String PREF_VIEW_TYPE = "viewtype";

    public enum LIST_TYPE {pix, nopix, visible, invisible}

    public static final String MOIM_TYPE = "moim_type";
    public static final String MOIM_TAG = "moim_tag";
    public static final String SET_COVER = "setcover";
    public static final String NEW_COVER = "new_cover";
    public static final String MODIFY_COVER = "modify_cover";

    public static final String INTENT_TYPE = "type";
    //리더추가 리더위임
    public static final String ADD_LEADER = "add_leader";            //공동리더 추가
    public static final String MANDATE_LEADER = "mandate_leader";    //리더위임


    public enum SET_MOIM_LIST {search_by_subject, moim_create}  //주제별 모임 찾기, 모임 개설 구분

    public enum SET_PUBLIC_DIALOG_TYPE {new_moim_cover, set_public_manage} //공개설정 다이얼로그타입

    public enum SET_PERMISSION_DIALOG_TYPE {all_member, leader}  //모든멤버, 리더와 공동리더 타입

    public enum DIALOG_BUTTON_TYPE {confirm, ok_cancle, moim_delete, cancle_confirm, add_leader, friend_block, friend_delete, logout, write_out, modify_out, chat_invite,alram_read, leave_member}  //확인 버튼 투버튼 구분 , 모임 삭제팝업 ,, cancle_confirm : 취소/확인  add_leader:공동리더 추가 alram_read: 알림화면 모두읽음처리

    public enum UPLOAD_TYPE {image, file}   //글쓰기 이미지, 파일첨부 구분

    /* 모임정보-내설정-알림설정 */
    //public static final String SET_NOTIFY_PUSH = "set_notify_push";
    //public static final String SET_NOTIFY_WRITING = "set_notify_writing";
    // public static final String SET_NOTIFY_REPLY = "set_notify_reply";
    public static final String SET_NOTIFY_CHAT = "set_notify_chat";

    /* 모임정보-내설정 */
    public static final String SET_RECIVE_CHAT = "set_recive_chat"; //채팅 바로받기
    public static final String SET_ALLOW_INVITE = "set_allow_invite"; //초대허용

    public static final String SUCCESS = "success"; //
    public static final String ERROR = "error"; //
    public static final String MOIM_PROFILE = "moim_profile"; //

    /* 모임 타임라인 가입하기  */
    public static final String MOIM_ID = "moimId"; //모임아이디  pref저장
    public static final String MOIM_CHATROOMID = "moim_chatRoomId"; //모임대화방 아이디  pref저장
    public static final String MOIM_MSG = "moim_msg"; //모임메시지  pref저장
    public static final String MOIM_NOTI = "moim_noti"; //모임공지  pref저장
    public static final String MOIM_MSGID = "moim_msgId"; //메시지키값  pref저장
    public static final String MOIM_MODIFY_MSGID = "moim_modify_msgId"; //글수정 메시지키값  pref저장

    public static final String MOIM_RE_ID = "moimReId";
    public static final String MOIM_RE_MSGID = "moim_msgReId";

    public static final String MOIM_ENTER_TYPE = "enter_type"; //모임 가입제한  pref저장

    /* 글쓰기 기능 구분 */
    public static final String MOIM_WRITE_TYPE = "moim_write_type"; //글쓰기, 수정하기 ,공유하기 구분 pref저장 PROPERTY
    public static final String MOIM_WRITE_TYPE_WRITE = "type_write"; //글쓰기
    public static final String MOIM_WRITE_TYPE_MAIN_WRITE = "type_main_write"; //매인에서 모임선택 글쓰기
    public static final String MOIM_WRITE_TYPE_MODIFY = "type_modify"; //수정하기
    public static final String MOIM_WRITE_TYPE_SHARE = "type_share";   //공유하기


    public static final String MOIM_REPLY = "reply";    //댓글달기 모드인지 구분 pref저장
    public static final String MOIM_REGUSER = "reguser";    //작성자 pref저장
    public static final String MOIM_TTL = "ttl";    //첫줄 pref저장
    public static final String MOIM_SETTING = "setting";

    /*챗리스트 룸아이디 저장*/
    public static final String CHAT_ROOM_ID = "roomid"; //알림리스트 SSAM
    public static final String CHAT_UNREAD_COUNT = "unread_counter"; //읽지않은 알림 카운터

    /* 프로필화면 Pref */
    public static final String MOIM_USER_PROFILE_ID = "user_profile_id";    //profile_id pref저장 선택한 프로필의 아이디


    public static final String imgOrg = "imgOrg";    //모임 이미지 원본
    public static final String imgTmb = "imgTmb";   //모임 타이틀이미지
    public static final String mmNm = "mmNm";         //모임명
    public static final String mmTg = "mmTg";         //모임종류
    public static final String imgTy = "imgTy";         //모임이미지 없음,있음
    public static final String mmShrtId = "mmShrtId";


    /*모임설정관리*/
    public static final String opnTy = "opnTy";       //모임공개설정  opnTy       공개 제한(int)                0:비공개, 1:공개, 2:모임명만 공개(기본),
    public static final String entTy = "entTy";       //가입신청받기  entTy       가입 제한(int)                0:리더승인, 1:비밀번호 입력, 2:자동가입
    //public static final String conTy = "conTy";       //종목상담     conTy        0:OFF, 1:ON
    //public static final String reTy = "reTy";         //종목리딩      reTy        0:OFF, 1:ON
    /*전문가 관련 설정 저장*/
    public static final String csltTy="csltTy";     //csltTy    종목상담여부(int) 0:OFF, 1:ON
    public static final String ldngTy="ldngTy";     //ldngTy    종목리딩여부(int) 0:OFF, 1:ON



    public static final String maxUsr = "maxUsr";     //최대멤버수   maxUsr       가입 최대 사용자(int)        50, 100, 500 중에 설정, 기본 : 100

    public static final String intr = "intr";         //모임 설명

    /*멤버들의 권한 설정*/
    public static final String covTy = "covTy";       //모임 이름 및 커버 설정 권한   0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
    public static final String acptTy = "acptTy";     //멤버 가입신청 수락 권한      0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
    public static final String ivtTy = "ivtTy";       //멤버 초대 권한             0:모든멤버(기본), 1:리더, 2:리더와 공동리더
    public static final String notiTy = "notiTy";     //공지글 등록 권한(int)     0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
    public static final String wrtTy = "wrtTy";       //글쓰기 권한(int)          0:모든멤버(기본), 1:리더, 2:리더와 공동리더
    public static final String abmTy = "abmTy";       //앨범 만들기 권한(int)     0:모든멤버(기본), 1:리더, 2:리더와 공동리더
    public static final String rpyTy = "rpyTy";       //댓글 쓰기 권한(int)     0:모든멤버(기본), 1:리더, 2:리더와 공동리더
    public static final String delTy = "delTy";       //다른 멤버의 게시물, 댓글 삭제 권한(int)     0:모든멤버, 1:리더(기본), 2:리더와 공동리더
    public static final String frcTy = "frcTy";       //멤버 탈퇴, 차단 권한(int)     0:모든멤버, 1:리더(기본), 2:리더와 공동리더
//    public static final String setTy="setTy";

    /*채팅설정 및 메뉴 우선순위 결정*/
    public static final String mnuTy = "mnuTy";       //메뉴우선순위 종류(int)          0:컨텐츠 타임라인, 1:오픈채팅방(기본)
    public static final String opchtTy = "opchtTy";   //오픈채팅방 개설가능 여부(int)     0:개설불가(기본), 1:개설가능
    public static final String snglTy = "snglTy";     //모임 멤버간 1:1채팅 가능여부(int) 0:불가(기본), 1:가능

    public static final String regUsr = "regUsr";     //모임 생성자
    public static final String mLv = "mLv";           //내 사용자 레벨(int) 0:일반, 1:리더, 2:공동리더

    /*알림화면 설*/
    public static final String useMmPush = "useMmPush";       //모임 알림 사용여부(int)   0:사용안함, 1:사용
    public static final String useMmBbsPush = "useMmBbsPush";     //모임 글 알림 사용여부(int) 0:사용안함, 1:사용
    public static final String useMmRpyPush = "useMmRpyPush";     //모임 댓글 알림 사용여부(int) 0:사용안함, 1:사용


    public enum SET_CONFIG_TYPE {opnTy, maxUsr, intr, entTy, csltTy, ldngTy}   //기본설정 설정완료후 핸들러 구분하기위한 enum

    public enum SET_PERMISION_TYPE {covTy, acptTy, ivtTy, notiTy, wrtTy, abmTy, rpyTy, delTy, frcTy, mnuTy, opchtTy, snglTy}   //멤버들의 권한설정 완료후 핸들러 구분하기위한 enum ,


    //프로필 설정
    public static final String UsNm = "UsNm";         //이름
    public static final String pfSbj = "pfSbj";       //상태메시
    public static final String pfImg = "pfImg";       //프로필이미지
    public static final String pfImgTy = "pfImgTy";   //프로필 이미지 종류
    public static final String tel = "tel";
    public static final String xprt = "xprt";   //전문가 구분값
    public static final String mstt = "mstt";   //모임 가입 여부


    //리스트표시 구분위한값 (BoardListRecyclerAdapter 같이 쓰기위해)
    public enum SET_ENTRY_TYPE {
        main_tab, moim_list, written_list, moim_timelineDetail
    }  //메인에서 , 모임리스트, 내가 쓴글리스트 어디서 사용하는지 구분


    //상세페이지
    public static final String MOIM_MAIN_TIMELINE = "moim_main_timeline";   //
    public static final String MOIM_MAIN = "moim_main";
    public static final String MOIM_TIMELINE = "moim_main";
    public static final String MOIM_NOTICE = "moim_main";
    public static final String MOIM_CHAT = "moim_chat";   //

    //모임 글쓰기 결과 화면 구분
    //public static final String MOIM_WRITE_MAIN = "moim_write_main";   //모임 글쓰기  [메인->모임선택>글쓰기 : true], [모임-> 글쓰기:false] 구분
    //public static final String MOIM_PUBLIC = "moim_public";   //공유할모임선택:ture  모임선택:false 구분

    //모임 알림 설정 결과 구분
    public enum SET_NOTY_TYPE {
        useMmPush, useMmBbsPush, useMmRpyPush
    }


    //더보기
    public static final String SET_ALLOW_PASSWORD = "set_allow_password"; //암호잠금
    public static final String FONTSIZE = "FONTSIZE";
    public static final String STATE_MESSAGE = "state_message";

    //전문가그룹 일반.
    public static final String PROFESS_NORMAL = "0";

    public static final String lockPassword = "lockPassword";

    public static final String lockCheck = "lockCheck";
    public static final String refresh_date = "refresh_date";

    public static final String push_set = "push_set";

    //public enum WRITE_IMAGE_TYPE {image, emoticon}

    public static final String WRITE_SAVE_CONTENT = "write_save_content";   //쓰던글 pref 저장

    public static final String IS_FIRST_START ="isFirstStart";    //인트로화면 처음실행 구분 pref
    public static final String IS_LOGIN_TERMS ="isLoginTerms";    //로그인이후 약관 팝업 띄울값 구분 pref
    public static final String IS_LOGIN_TERMS_OK ="isLoginTermsOk";    //약관 값 동의 했는지 구분 pref
   //public static final String IS_LOGIN_TERMS_OPEN ="isLoginTermsOpen";    //약관 팝업 띄우기 구분 pref
    public static final boolean BOTTOM_ADVERTISING=false;   //메인 하단 광고 false: 숨김
}