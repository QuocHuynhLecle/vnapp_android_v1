package com.wave.messenger.util.AbstractAdapter;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by user on 2016-09-01.
 */
public abstract class TextWatcherAbstract implements TextWatcher {
    @Override
    public void afterTextChanged(Editable s) {}
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
}
