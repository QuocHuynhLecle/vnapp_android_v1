package com.wave.messenger.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_SelectMoim;
import com.wave.messenger.dialog.Dialog_share;
import com.wave.messenger.vo.VoMsgItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by 이윤수 on 2017. 3. 9..
 */

public class Util {

    /**
     * 화면의 가로 길이를 가져옴
     *
     * @param context
     * @return
     */
    public static int getwidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        return display.getWidth();
    }


    /**
     * 퍼미션 체크.
     * @param context
     * @return
     */
    public static boolean CheckPermission(Activity context) {
        Log.e("Util","CheckPermission");


        /*boolean result = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // targetSdkVersion >= Android M, we can
                // use Context#checkSelfPermission
                result = context.checkSelfPermission(Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED;
            } else {
                // targetSdkVersion < Android M, we have to use PermissionChecker
                result = PermissionChecker.checkSelfPermission(context, Manifest.permission.CAMERA)
                        == PermissionChecker.PERMISSION_GRANTED;
            }
        }*/


        //TODO 퍼미션체크
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (context.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                ) {

            //퍼미션 거부시 다시 요청
            if (context.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {

                Log.e(context.getLocalClassName(), "Should we show an explanation?");

                context.requestPermissions(
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_CAMERA);

            } else {
                // 퍼미션없을때 처리
                context.requestPermissions(
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_CAMERA);

                Log.e(context.getLocalClassName(), "No explanation needed, we can request the permission");
            }
        } else {
            return true;
        }

        return false;
    }

    public static boolean CheckPhenumPermission(Activity context) {
        /*if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            //퍼미션 거부시 다시 요청
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.READ_CONTACTS)) {

                Log.e(context.getLocalClassName(),"Should we show an explanation?");
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS);


            } else {
                // 퍼미션없을때 처리
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                Log.e(context.getLocalClassName(),"No explanation needed, we can request the permission");

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.


            }
        }else{
            return  true;
        }*/

        return false;
    }


    /**
     * 카메라 퍼미션 허용 팝업
     *
     * @param activity
     */
    public static void showCameraPermissionDialog(Activity activity) {
        Log.e("Util","showCameraPermissionDialog");
        if (activity != null) {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is not null");
            Log.e(activity.getLocalClassName(), "showCameraPermissionDialog");
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.camera_permission_title));
            builder.setMessage(activity.getString(R.string.camera_permission_message));
            builder.setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog ab = builder.create();

            if (ab != null) {
                ab.show();
            }
        } else {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is null");
        }
    }

    /**
     * 내용 뒤에 "더보기" 를 붙여준다
     *
     * @param tv
     * @param maxLine
     * @param expandText
     * @param viewMore
     */
    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 8) + "..." + expandText;
                    tv.setText(text);
                    /*tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.NORMAL);*/
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 8) + "..." + expandText;
                    tv.setText(text);
                   /* tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.NORMAL);*/
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 8);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    /*tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.NORMAL);*/
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.NORMAL);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.NORMAL);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    /**
     * edittext 길이 체크
     *
     * @param etText
     * @return
     */
    public static boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    /**
     * 앱을 삭제한다.
     *
     * @param context
     */
    public static void deleteApp(Context context) {
        Uri packageURI = Uri.parse("package:" + context.getPackageName());
        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
        context.startActivity(uninstallIntent);
    }

    /**
     * 넘길정보없이 화면 인텐트
     *
     * @param context
     * @param activity
     */
    public static void intentActivity(Context context, Class activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }

    /**
     * dp를 px로 변환
     *
     * @param dp
     * @return
     */
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);

    }


    /**
     * px를 dp로 변환
     *
     * @param px
     * @return
     */
    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    /**
     * 온라인 연결되어있는지 체크
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    /**
     * 키보드를 숨긴다
     *
     * @param context
     * @param edt
     */
    public static void hideKeyboard(Context context, EditText edt) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
    }


    /**
     * 키보드강제 내림
     * @param
     */
    public static void forceHideKeyboard(Activity context){
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        /*InputMethodManager inputMethodManager = (InputMethodManager)  context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);*/

        /*InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isActive()){
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
        }*/ //else {
           // imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY); // show
        //}
    }

    /*public static void showKeyboard(Context context, EditText edt) {
        //InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.showSoftInput(edt, 0);

    }*/


    /**
     * 키보드를 보인다
     */
    public static void showSoftKeyboard(Context context, View view) {
        /*if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }*/
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }


    /**
     * 뷰에 마진을 넣는다
     *
     * @param view
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public static void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }


    /**
     * 이미지를 바이트배열로 바꿔서
     * base64문자열로 인코딩한다음
     * json으로 전달할때 사용
     *
     * @param bitmapPicture
     * @return
     */
    public static String getStringFromBitmap(Bitmap bitmapPicture) {
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, 100, byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * 내가 글 주인인지 판단
     * <p>
     * 글수정삭제시 사용
     *
     * @param context
     * @param regUsr
     * @return
     */
    public static boolean isOwner(Context context, String regUsr) {
        return regUsr.equals(MessengerInfo.getUserId(context));
    }


    /*public static class DateComparator implements Comparator<VoBoardRead.VoReadItem> {

        @Override
        public int compare(VoBoardRead.VoReadItem o1, VoBoardRead.VoReadItem o2) {
            Double distance = Double.valueOf(o1.getBbsOrd());
            Double distance1 = Double.valueOf(o2.getBbsOrd());
            if (distance.compareTo(distance1) < 0) {
                return -1;
            } else if (distance.compareTo(distance1) > 0) {
                return 1;
            } else {
                return 0;
            }
        }

    }*/

    /**
     * 모임들어오면서 저장된 regUsr 값과 나의 아이디값 비교
     * 내가 모임 개설자인지 판단
     *
     * @param context
     * @return
     */
    public static boolean isMoimRegUsr(Context context) {
        return MessengerInfo.getUserId(context).equals(SharedObject.getProperty_string(context, Constants.regUsr, ""));
    }


    /**
     * 타임라인 MSG에 있는 글 이미지
     * idx, type, data
     * 파싱한다
     *
     * @param strMsg
     */
    public static ArrayList<VoMsgItem> ParseMsg(String strMsg) {
        ArrayList<VoMsgItem> arMsgItem = new ArrayList<>();

        JSONArray jsonArr = null;
        try {
            jsonArr = new JSONArray(strMsg);

            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject jsonObj = null;

                jsonObj = jsonArr.getJSONObject(i);

                arMsgItem.add(new VoMsgItem(jsonObj.getString("idx").toString()
                        , jsonObj.getString("type").toString()
                        , jsonObj.getString("data").toString()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arMsgItem;
    }


    /**
     * 처음 저장된 나의 아이디와
     * 프로필로 들어오며 저장된 상대아이디 값이 같은지구분
     * 같으면 나의 계정 화면.
     *
     * @param context
     * @return
     */
    public static boolean isMyAccount(Context context) {

        if (MessengerInfo.getUserId(context).equals(
                SharedObject.getProperty_string(context, Constants.MOIM_USER_PROFILE_ID, "")
        )) {
            return true;
        } else {
            return false;
        }


    }

    /**
     * 로그가 긴거
     * 파일로 저장해서 확인
     *
     * @param text
     */
    static public void appendLog(String text) {

        File externalStorageDir = Environment.getExternalStorageDirectory();
        File logFile = new File(externalStorageDir, "log.txt");

        Log.e("Util", "appendLog:" + logFile.toString());
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * 모임에서 채팅화면으로 intent
     * //TODO 아직 톡부분 개발중...
     *
     * @param context
     */
    public static void intentChat(Context context) {
        Log.e("TAG", "intentChat " + context.getClass().getSimpleName());

    }

    /**
     * 프로필이미지표시 (모임 프로필아님)
     *
     * @param regUsr
     * @param imgvProfile
     */
    public static void setGlideProfile(Context context, String regUsr, ImageView imgvProfile) {
        Glide.with(context).load(Constants.PROFILE_IMAGE_URL + regUsr)
                .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.ic_default_my_profile)
                .dontAnimate().into(imgvProfile);
    }


    /**
     * 모임의 프로필 이미지
     *
     * @param context
     * @param moimid  모임아이디
     * @param PfImg   프로필 이미지명
     * @param imgView
     */
    public static void setMoimProfileGlide(Context context, String moimid, String PfImg, ImageView imgView) {

        String url = Constants.MOIM_PROFILE_IMAGE_URL
                + moimid
                + "&serverfile=" + PfImg;

        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile)
                .dontAnimate().into(imgView);
    }


    /**
     * 수정된 모임의 프로필이미지
     * 설정에서 지정된, 채팅에 보여지는 프로필이미지
     * @param context
     * @param imgView
     */
    public static void setMoimProfileGlide2(Context context, String regUsr, ImageView imgView) {

        String profile_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + regUsr;
        Glide.with(context).load(profile_url)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .skipMemoryCache(true).placeholder(R.drawable.ic_default_profile)
                .dontAnimate().into(imgView);

    }

    /**
     * 폰번호 가져오기
     *
     * @param context
     */
    public static String getPhoneNum(Context context) {
        TelephonyManager t = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNum = t.getLine1Number();

        phoneNum = phoneNum.substring(phoneNum.length() - 10, phoneNum.length());
        phoneNum = "0" + phoneNum;
        return phoneNum;
    }


    /**
     * Ellipsized
     *
     * @param textView
     * @return
     */
    public static boolean isTextViewEllipsized(final TextView textView) {
        boolean result = false;
        if (textView != null) {
            final TextUtils.TruncateAt truncateAt = textView.getEllipsize();
            if (truncateAt != null && !TextUtils.TruncateAt.MARQUEE.equals(truncateAt)) {
                final Layout layout = textView.getLayout();
                if (layout != null) {
                    for (int index = 0; index < layout.getLineCount(); ++index) {
                        result = layout.getEllipsisCount(index) > 0;
                        if (result) {
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }


    /**
     * 옵션메뉴 공유하기
     * 공유하기 버튼
     * 상세화면 공유하기버튼
     *
     * @param context
     * @param text
     */
    public static void intentShare(Context context, String text) {
       /* Dialog_share mDialog = new Dialog_share(mContext);
        mDialog.show();*/
        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addCategory(Intent.CATEGORY_DEFAULT);
        msg.putExtra(Intent.EXTRA_TEXT, text);
        msg.putExtra(Intent.EXTRA_TITLE, "EXTRA_TITLE");
        msg.setType("text/plain");
        context.startActivity(Intent.createChooser(msg, "공유"));

    }

    /**
     * 모임 들어오면서 저장 된 mLv
     * 나의 등급 레벨값을 리턴한다
     * 0:일반, 1:리더, 2:공동리더
     */
    public static String getMymLv(Context context) {
        return SharedObject.getProperty_string(context, Constants.mLv, "0");
    }

    /**
     * 모임 이름 및 커버 설정 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     *
     * @param context
     * @return
     */
    public static String getCovTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.covTy, "2");
    }


    /**
     * 멤버 가입신청 수락 권한(int) 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     *
     * @param context
     * @return
     */
    public static String getAcptTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.acptTy, "2");
    }


    /**
     * 멤버 초대 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     *
     * @param context
     * @return
     */
    public static String getIvtTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.ivtTy, "0");
    }

    /**
     * 공지글 등록 권한(int)    0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     */
    public static String getNotiTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.notiTy, "2");
    }

    /**
     * 글쓰기 권한(int)   0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     */
    public static String getWrtTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.wrtTy, "0");
    }

    /**
     * 앨범 만들기 권한(int)    0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     */
    public static String getAbmTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.abmTy, "0");
    }

    /**
     * 댓글 쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
     */
    public static String getRpyTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.rpyTy, "0");
    }

    /**
     * 다른 멤버의 게시물, 댓글 삭제 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
     */
    public static String getDelTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.delTy, "1");
    }

    /**
     * 멤버 탈퇴, 차단 권한(int) 0:모든멤버, 1:리더(기본), 2:리더와 공동리더
     */
    public static String getFrcTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.frcTy, "1");
    }

    /**
     * 모임 멤버간 1:1채팅 가능여부(int)
     * 0:불가, 1:가능(기본)
     */
    public static String getSnglTy(Context context) {
        return SharedObject.getProperty_string(context, Constants.snglTy, "1");
    }


/**
 *  약관 값 가져오기.
 *  false
 */
public static boolean isTerms(Context context) {
    return SharedObject.getProperty_boolean(context, Constants.IS_LOGIN_TERMS, false);
}
    /**
     * 로그인이후  result 값이 no_agree f이면, 약관 값 true 저장
     * @param context
     */
    public static void setTermsTrue(Context context) {
        SharedObject.setProperty_boolean(context, Constants.IS_LOGIN_TERMS, true);
    }

    /**
     *  약관 팝업 이후  false 값 저장
     * @param context
     */
    public static void setTermsFalse(Context context) {
        SharedObject.setProperty_boolean(context, Constants.IS_LOGIN_TERMS, false);
    }
    /**
     * 약관 동의버튼 눌렀을때 true값 저장
     * @param context
     */
    public static void setTermsOkTrue(Context context) {
        SharedObject.setProperty_boolean(context, Constants.IS_LOGIN_TERMS_OK, true);
    }

    /**
     *로그인에서
     *  MOAClient.getInstance().setProperties("agree", "1") 처리후 false
     */
    public static void setTermsOkFalse (Context context) {
        SharedObject.setProperty_boolean(context, Constants.IS_LOGIN_TERMS_OK, false);
    }
    /**
     * 약관 동의 했는지
     * 기본값 false
     */
    public static boolean isTermsOk(Context context) {
        return SharedObject.getProperty_boolean(context, Constants.IS_LOGIN_TERMS_OK, false);
    }

    /**
     * 처음실행인지 구분
     * 인트로화면 띄울때 구분
     *
     * @param context
     * @return
     */
    public static boolean isFirstStart(Context context) {
        return SharedObject.getProperty_boolean(context, Constants.IS_FIRST_START, true);
    }

    /**
     *처음실행 저장
     * @param context
     */
    public static void setFirstStartFalse(Context context) {
        SharedObject.setProperty_boolean(context, Constants.IS_FIRST_START, false);
    }



    /**
     * 권한 비교해서 결과값 리턴
     *
     * @param mLv
     * @param permit 권한 비교대상
     *               return true: 통과 버튼 or 화면 보이기 , false: 제한, 숨기기
     */
    public static boolean getPermissionLv(String mLv, String permit) {
        switch (mLv) {
            case "0":   //멤버
                if (permit.equals(mLv)) { //권한이 멤버이면 true 그외 false
                    return true;
                } else {
                    return false;
                }

            case "1":   //내가 리더이면 항상 true
                return true;

            case "2":   //공동리더

                if (permit.equals("1")) { //권한이 리더이면 false
                    return false;
                } else {
                    return true;
                }
        }
        return false;
    }


    /**
     * 서버에서 보내주는
     * yyyy-MM-dd HH:mm:ss 날짜 형식을 바꿔준다.
     * @param date
     * @param textView
     */
    public static void DateParse(String date, TextView textView) {
        SimpleDateFormat original_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat new_format = new SimpleDateFormat("yyyy년 MM월 dd일  a hh:mm");

        try {
            Date original_date = original_format.parse(date);
            String new_date = new_format.format(original_date);

            textView.setText(new_date);


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * getColor사라짐
     * getResources().getColor() is deprecated [duplicate]
     * @param context
     * @param id
     * @return
     */
    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }


    /**
     * 긴급공지 중요 표시
     * @param text
     */
    public static SpannableString setNotiText(Context context, String text) {

        String actionDisplayText ="중요 "+text;
        SpannableString truncatedSpannableString = new SpannableString(actionDisplayText);
        //int startIndex = actionDisplayText.indexOf(moreString);
        truncatedSpannableString.setSpan(new ForegroundColorSpan(Util.getColor(context, R.color.red)), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //Vholder.content.setText(truncatedSpannableString);
        return truncatedSpannableString;
    }


    /**
     * 클립보드로 복사하기
     * @param
     */

    public static void copyToClipBoard(Context context, String txtNotes)
    {
        ClipboardManager cManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData cData = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            cData = ClipData.newPlainText("text",txtNotes);
            cManager.setPrimaryClip(cData);
        }else{
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(txtNotes);
        }

    }

    /**
     * url이 포함되어있는지 체크.
     * @param text
     * @return
     */
    public static boolean isURL(String text) {
        if (text.contains("http://") || text.contains("https://")) {
            return true;
        } else if (text.contains("www.") || text.contains(".com")) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 공유하기팝업-다른모임공유
     * 공유할 모임 선택 화면
     *
     * 모임타임라인, 글조회회면 팝업 옵션.
     * @param
     */
    public static void intentSelectMoim(Context context, String msgId, String msg ,String noti) {
        //SharedObject.setProperty_boolean(context, Constants.MOIM_PUBLIC, true); //공유할 모임 선택화면 구분
        SharedObject.setProperty_string(context, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_SHARE);   //글쓰기 -공유
        //SharedObject.setProperty_boolean(context, Constants.MOIM_WRITE_TYPE, false); //글수정
        //SharedObject.setProperty_string(Activity_TimeLineDetail.this, Constants.MOIM_ID, voBoardRead.getParams().getMmId());      //모임아이디
        SharedObject.setProperty_string(context, Constants.MOIM_MSGID, msgId);  //메시지 키값
        SharedObject.setProperty_string(context, Constants.MOIM_MSG, msg);      //모임 메시지
        SharedObject.setProperty_string(context, Constants.MOIM_NOTI, noti);    //모임 공지

        Intent intent = new Intent(context, Activity_SelectMoim.class);
        context.startActivity(intent);
    }

    /**
     * 타임라인, 글상세  팝업옵션에서.
     * 공유하기 팝업.
     */
    public static void shareDialog(final Context context, final String msgId, final String msg,final String noti, final String moimName, final String ttl, final String mmShrtId) {

        final Dialog_share mDialog = new Dialog_share(context);
        mDialog.show();
        mDialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nId = v.getId();

                Log.d("공유하기!!!!!", "공유 내용 : " + ttl);
                if(nId==R.id.ll_option0){ //다른 모임으로 공유.

                    //Log.e(TAG,"arvalues.get(position).getMsg(): "+arvalues.get(position).getMsg());

                    Util.intentSelectMoim(context,
                            msgId,
                            msg,
                            noti
                    );
                    mDialog.dismiss();

                }else if(nId==R.id.ll_option1){ //페이스북  지원안함.
                    //그러shareFacebook(context, moimName, ttl, mmShrtId );

                }else if(nId==R.id.ll_option2) { //카카오톡
                    sharekakao(context, moimName, ttl, mmShrtId );
                    mDialog.dismiss();
                }else if(nId==R.id.ll_option3){     //더보기...

                    String msg  = "["+moimName+"]" +ttl +"\n";
                    Util.intentShare(context, msg);
                    mDialog.dismiss();
                }


            }
        });

    }



    /**
     * 타임라인, 글조회 팝업 공유하기
     * 카카오톡으로 내용 공유하기
     */
    public static void sharekakao(Context context, String moimName, String ttl, String mmShrtId) {

        String msg  = "["+moimName+"]" +ttl +"\n"
                //+ Constants.MOIM_INVITE_URL+ mmShrtId
                ;

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        //intent.putExtra(Intent.EXTRA_SUBJECT, "타이틀");
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList) {
            if(resolveInfo.activityInfo.packageName.startsWith("com.kakao.talk")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if(resolved) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "카카오톡 앱이 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 페이스북에서 막아둠.
     * 사용못함
     * @param context
     * @param moimName
     * @param ttl
     * @param mmShrtId
     */
    public static void shareFacebook(Context context, String moimName, String ttl, String mmShrtId) {

        String msg  = "["+moimName+"]" +ttl +"\n"
                //+ Constants.MOIM_INVITE_URL+ mmShrtId
                ;

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, moimName);
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        PackageManager packManager = context.getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList) {
            /*if(resolveInfo.activityInfo.packageName.startsWith("com.facebook.katana")){
                intent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }*/

            //
            if ((resolveInfo.activityInfo.name).startsWith("com.facebook.katana"))
            {
                final ActivityInfo activity = resolveInfo.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                intent.setComponent(name);
                //context.getContext().startActivity(shareIntent);
                resolved = true;
                break;
            }
        }
        if(resolved) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "페이스북 앱이 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 앱버전 가져오기
     */
//    public static int getAppVersion(Context context) {
    public static String getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;     // versionCode -> versionName / 180529 mwju
        } catch (Exception e) {
            e.printStackTrace();
            return "9.9.9";
        }
    }

    /**
     * Making notification bar transparent
     */
    public static void changeStatusBarColor(Activity mActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = mActivity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * 긴로그
     */
    public static void showLog( String message ) {
        if( true ) {
            int maxLogSize = 1000;
            for(int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                android.util.Log.v("showLog", message.substring(start, end));
            }
        }
    }


}
