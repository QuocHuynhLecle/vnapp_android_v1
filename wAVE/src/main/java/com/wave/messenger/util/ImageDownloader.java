package com.wave.messenger.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Parameters <br/>
 * 1. String type 2 - 유저 id로 이미지 다운로드, type 3 - 유저 id로 썸네일 다운로드 2. String userid
 * - 유저 id. 3. usage : new ImageDownloader().execute("2", "sjh");
 * 
 * 
 * @author user
 * 
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

	private String address = Constants.CHATDAWN_PROC + "?";
	
	@Override
	protected Bitmap doInBackground(String... params) {

		try {
			String fullAddress = address + "type=" + params[0] + "&userid="	+ params[1];
			//ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

			URL url = new URL(fullAddress);
			URLConnection conn = url.openConnection();
			conn.connect();
			BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
			Bitmap image = BitmapFactory.decodeStream(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
	
	}

}
