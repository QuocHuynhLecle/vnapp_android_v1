package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class ProfessionalListHolder extends RecyclerView.ViewHolder {
    public TextView tv_groupName;
    public Button iv_edit, iv_delete;

    public static ProfessionalListHolder newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chatlist, parent, false);
        return new ProfessionalListHolder(itemView);
    }

    public ProfessionalListHolder(View itemView) {
        super(itemView);

        tv_groupName = (TextView) itemView.findViewById(R.id.tv_groupName);
        iv_edit = (Button) itemView.findViewById(R.id.iv_edit);
        iv_delete = (Button) itemView.findViewById(R.id.iv_delete);
    }
}
