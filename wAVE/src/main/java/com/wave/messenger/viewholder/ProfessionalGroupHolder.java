package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class ProfessionalGroupHolder extends RecyclerView.ViewHolder {
    public TextView tv_groupName;
    public Button iv_edit, iv_delete;

    public ProfessionalGroupHolder(View itemView) {
        super(itemView);

        tv_groupName = (TextView) itemView.findViewById(R.id.tv_groupName);
        iv_edit = (Button) itemView.findViewById(R.id.iv_edit);
        iv_delete = (Button) itemView.findViewById(R.id.iv_delete);
    }
}
