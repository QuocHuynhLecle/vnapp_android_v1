package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 4. 14..
 */

public class GroupHolder extends RecyclerView.ViewHolder {
    public TextView tv_header;
    public LinearLayout ll_groupmsg;

    public GroupHolder(View itemView) {
        super(itemView);

        tv_header = (TextView) itemView.findViewById(R.id.tv_header);
        ll_groupmsg = (LinearLayout) itemView.findViewById(R.id.ll_groupmsg);
    }
}
