package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 4. 14..
 */

public class FriendHolder extends RecyclerView.ViewHolder {

    public LinearLayout ll_friend;
    public ImageView iv_profile;
    public TextView tv_name, tv_subject;
    public LinearLayout ll_line;

    public FriendHolder(View itemView) {
        super(itemView);

        ll_friend = (LinearLayout) itemView.findViewById(R.id.ll_friend);
        iv_profile = (ImageView) itemView.findViewById(R.id.iv_profile);
        tv_name = (TextView) itemView.findViewById(R.id.tv_name);
        tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
        ll_line = (LinearLayout) itemView.findViewById(R.id.ll_line);
    }

}
