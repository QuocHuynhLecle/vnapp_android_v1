package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.CircleImageView;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class CustomerListHolder extends RecyclerView.ViewHolder {
    public TextView textViewName;
    public CircleImageView imageViewTitle;

    public CustomerListHolder(View itemView) {
        super(itemView);

        textViewName = (TextView) itemView.findViewById(R.id.textViewName);
        imageViewTitle = (CircleImageView) itemView.findViewById(R.id.imageViewTitle);
    }
}
