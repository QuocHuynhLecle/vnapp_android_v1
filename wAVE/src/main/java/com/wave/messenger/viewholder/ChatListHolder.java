package com.wave.messenger.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class ChatListHolder extends RecyclerView.ViewHolder {
    public ImageView iv_profile, iv_alarm, iv_select;
    public TextView tv_roomname, tv_thumb, tv_date, tv_count, tv_usercount, tv_join;
    public LinearLayout ll_chatline;

    public ChatListHolder(View itemView) {
        super(itemView);

        iv_profile = (ImageView) itemView.findViewById(R.id.iv_profile);
        iv_alarm = (ImageView) itemView.findViewById(R.id.iv_alarm);
        iv_select = (ImageView) itemView.findViewById(R.id.iv_select);
        tv_roomname = (TextView) itemView.findViewById(R.id.tv_roomname);
        tv_thumb = (TextView) itemView.findViewById(R.id.tv_thumb);
        tv_date = (TextView) itemView.findViewById(R.id.tv_date);
        tv_count = (TextView) itemView.findViewById(R.id.tv_count);
        tv_usercount = (TextView) itemView.findViewById(R.id.tv_usercount);
        ll_chatline = (LinearLayout) itemView.findViewById(R.id.ll_chatline);
    }
}
