package com.wave.messenger;

import android.app.Application;

import com.google.gson.Gson;

/**
 * Created by aveapp on 2017. 4. 13..
 */

public class ApplicationPool extends Application {

    private static Gson gson;

    public Gson getGson() {
        if(gson == null) {
            gson = new Gson();
        }

        return gson;
    }
}
