package com.wave.messenger.sdk.Util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;


public class AlarmReceiver extends BroadcastReceiver {

	Context context;
	static int tick = 0;
	static int hTick = 0;
	public static long deleteCheckTick = 0;
	public static long smsCheckTick = 0;
	static boolean runningAlarm = false;
	static boolean stopAlarm = false;
	
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		/*context = arg0;
		try {
			if(stopAlarm == false) {
				if (MessengerInfo.getLoginComplete(context) == true) {
				
					if(tick <= 0) {
						tick++;					
					} else if (tick >= 3){
						tick = 0;
						TranService.serviceCheck(context);
					} else {
						tick++;		
						
						boolean isTop = MlBaseActivity.isTop();
						if(isTop == true) {
							if(SimpleChatClient.getInstance() == null || SimpleChatClient.getInstance().isConnected() == false) {
								TranService.serviceCheck(context);
							}
						}
					}		
					
					if(hTick >= 5) {	
						hTick = 0;
						//HeartBeat
						if(SimpleChatClient.getInstance().isConnected()) {	
							
							if(SimpleChatClient.getInstance().getHeartBeatTick() == 0) {
	//Log.i("testApp", "1 send HeartBeat : " + System.currentTimeMillis() + " , HeartBeatTick : " + SimpleChatClient.getInstance().getHeartBeatTick());
								
								SimpleChatClient.getInstance().setHeartBeatTick(System.currentTimeMillis());
							} else {
	//Log.i("testApp", "2 send HeartBeat : " + System.currentTimeMillis() + " , HeartBeatTick : " + SimpleChatClient.getInstance().getHeartBeatTick());							
							}
							
							SimpleChatClient.getInstance().setSendHeartBeatTick(System.currentTimeMillis());
							
							HashMap<String,Object> packet = new HashMap<String, Object>();
							packet.put("id", MessengerInfo.getUserID(TranService.getInstance()));
							packet.put("authCode", MessengerInfo.getUserPW(TranService.getInstance()));
							packet.put("di", MessengerInfo.getDeviceId(TranService.getInstance()));
							
							SimpleChatClient.sendDataThread(ProtocolType.ptHeartBeat
									, ProtocolType.stHeartBeatReq, packet, false);
							
							*//*
							long tick = SimpleChatClient.getInstance().getSendHeartBeatTick();
							long now = System.currentTimeMillis();
							
							if(tick == 0 || now - tick > 60000) {					
								
							}
							*//*
						}
					} else {
						hTick++;
					}
					
				}	
			}
		} catch (Exception e) {
			
		} finally {
			if(Build.VERSION.SDK_INT >= 19) {
				runningAlarm = false;
				if(stopAlarm == false) {
					startAlarmReceiver(context);	
				}
				
			}
		}*/
	}
	
	public static void startAlarmReceiver(Context context) {
		try {
			if(runningAlarm == false) {
				runningAlarm = true;
				stopAlarm = false;
				AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		        Intent intent = new Intent(context, AlarmReceiver.class);
		        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);	
		        
		        int interval = 10;
				if(Build.VERSION.SDK_INT >= 19) {							
					am.set(AlarmManager.RTC, System.currentTimeMillis() + 5000, sender);				
				} else {
					am.setRepeating(AlarmManager.RTC						        									        		
			        		, System.currentTimeMillis() + 5000//약 5초 뒤부터.
			        		, interval * 1000 , sender);				
				}
			}

		} catch (Exception e) {
			
		}
	}
	
	public static boolean isRunningAlarm() {
		return runningAlarm;
	}

	public static void setRunningAlarm(boolean runningAlarm) {
		AlarmReceiver.runningAlarm = runningAlarm;
	}

	public static void stopAlarmReceiver(Context context) {
		try {
			stopAlarm = true;
			runningAlarm = false;
			AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
	        Intent intent = new Intent(context, AlarmReceiver.class);
	        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);	
	        
	        am.cancel(sender);	
		} catch (Exception e) {
			
		}
	}
	
	
}
