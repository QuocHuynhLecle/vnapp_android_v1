package com.wave.messenger.sdk.Util;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_Delivery;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.CustomMultiPartEntity;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import ch.boye.httpclientandroidlib.HttpEntity;
import ch.boye.httpclientandroidlib.HttpResponse;
import ch.boye.httpclientandroidlib.client.HttpClient;
import ch.boye.httpclientandroidlib.client.methods.HttpPost;
import ch.boye.httpclientandroidlib.entity.mime.HttpMultipartMode;
import ch.boye.httpclientandroidlib.entity.mime.content.FileBody;
import ch.boye.httpclientandroidlib.entity.mime.content.StringBody;
import ch.boye.httpclientandroidlib.impl.client.DefaultHttpClient;

public class ImageUploadData {

	private static final char[] KEY_LIST = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z' };

	private static Random rnd = new Random();
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.KOREA);
	String resultData = "";
	VoChatData messagedata = new VoChatData();

	private static String getRandomStr() {
		char[] rchar = { KEY_LIST[rnd.nextInt(36)], KEY_LIST[rnd.nextInt(36)],
				KEY_LIST[rnd.nextInt(36)], KEY_LIST[rnd.nextInt(36)],
				KEY_LIST[rnd.nextInt(36)] };
		return String.copyValueOf(rchar);
	}

	public static String getFileKey(String myID) {
		return getRandomStr()
				+ dateFormat.format(new Date(System.currentTimeMillis())) + "_"
				+ myID;
	}
	
	HttpPost httppost = null;
	int maxFileSize = 0;
	int currentFileSize = 0;
	final String boundary = "---------------------------This is the boundary";
	
	AsyncTask<Void, Void, String> getDataWorker = null;
	/**
	 * 
	 * @param context
	 * @param path  -파일 저장 경로
	 */
	public void profileUpload(final Context context,final String path, final String type) {
		// TODO Auto-generated method stub
		getDataWorker = new AsyncTask<Void, Void, String>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();

				//LoadingManager.with(context).showLoadingDialog();
			}

			@Override
			protected String doInBackground(Void... params) {
				// TODO Auto-generated method stub

                File file = null;

				String result = "";
				try {			
					HttpClient httpclient = new DefaultHttpClient();
					
			        httppost = new HttpPost(
			        	Constants.PROFILEUPLOAD_PROC);
		  
			        CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
						@Override
						public void transferred(long num) {
							try {
								currentFileSize = (int) num;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
		            entity.addPart("userId", new StringBody(SharedObject.getProperty_string(context, "userId", "")));
		            currentFileSize = 0;
		            file = new File(path);
					maxFileSize = (int) file.length();
		            entity.addPart("file", new FileBody(file));
		            httppost.setEntity(entity);
		            httppost.setHeader("Accept-Charset", "UTF-8");
		            httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
		            HttpResponse response = httpclient.execute(httppost);

		            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
		            String sResponse;
		            StringBuilder s = new StringBuilder();
		            while ((sResponse = reader.readLine()) != null) {
		                s = s.append(sResponse);
		            }
		            result = s.toString();
		           	ErrorController.showMessage("Result : " + result);

				} catch (Exception e) {
					e.printStackTrace();
				}

				return result;
			}

			@Override
			protected void onPostExecute(String result) {
                //LoadingManager.with(context).hideLoadingDialog();

				if(result != null && result.length() > 0) {
					try {
						JSONObject jsonObject = new JSONObject(result);
						if(jsonObject.getString("result").equals("success")){
							//Fragment_Profile.getInstance().onUploadComplete(true);
						}
					} catch (JSONException e) {
						//Fragment_Profile.getInstance().onUploadComplete(false);
					}
				} else {
					//Fragment_Profile.getInstance().onUploadComplete(false);
				}
			}
		}.execute();
	}

	public void chatFileUpload(final Activity_ChatRoom context, final String path, final String roomId, final String sender) {

		getDataWorker = new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();

                String senderId = null;

                for (VoFriendList item : Statics.ROOMINFO.getFriends()) {
                    if (!item.getUserId().equals(MessengerInfo.getUserId(context))) {
                        senderId = item.getUserId();
                        break;
                    }
                }

                UUID uuid = UUID.randomUUID();
                messagedata.setUserId(MessengerInfo.getUserId(context));
				messagedata.setUserName(sender);
                messagedata.setChatId(uuid.toString());
                messagedata.setChatType("0");
                messagedata.setMessageType("2");
                messagedata.setTitle("(사진)");

                String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA).format(new Date(System.currentTimeMillis()));
                messagedata.setReg_date(time);
                messagedata.setOwnerId(MessengerInfo.getUserId(context));
                messagedata.setAttachmentLocal(path);

                if (TextUtils.isEmpty(roomId)) {
                    if ("1".equals(Statics.ROOMINFO.getRoom_type())) {
                        context.inviteFriend(roomId, Statics.ROOMINFO.getFriends(), messagedata);
                        ErrorController.showMessage("newFriends", Statics.ROOMINFO.getFriends().toString());
                        return;
                    } else {
                        messagedata.setRoomId("");

                        for (VoFriendList item : Statics.ROOMINFO.getFriends()) {
                            if (!item.getUserId().equals(MessengerInfo.getUserId(context))) {
                                messagedata.setInviteUser(item.getUserId());
                                messagedata.setSenderId(item.getUserId());
                            }
                        }

                        messagedata.setRoomCreate("1");
                    }
                } else {
                    messagedata.setRoomId(roomId);
                    messagedata.setSenderId(senderId);
                }
			}

			@Override
			protected String doInBackground(Void... params) {
				// TODO Auto-generated method stub

                File file = null;

				try {
					HttpClient httpclient = new DefaultHttpClient();
					httppost = new HttpPost(Constants.CHATUPLOAD_PROC);
					CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
						@Override
						public void transferred(long num) {
							// TODO Auto-generated method stub
							try {
								currentFileSize = (int) num;
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					} );

					currentFileSize = 0;
                    file = new File(path);
					maxFileSize = (int) file.length();
					entity.addPart("file", new FileBody(file));
					httppost.setEntity(entity);
					httppost.setHeader("Accept-Charset", "UTF-8");
					httppost.addHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity resEntity = response.getEntity();
					BufferedReader reader = new BufferedReader(new InputStreamReader(
							response.getEntity().getContent(), "UTF-8"));
					String sResponse;
					StringBuilder s = new StringBuilder();
					while ((sResponse = reader.readLine()) != null) {
						s = s.append(sResponse);
					}
					resultData = s.toString();

					ErrorController.showMessage("Result : " + resultData);

					// 이미지 업로드 성공시에만 파일 삭제
					if(resultData != null && resultData.length() > 0) {
						JSONObject jsonObject = new JSONObject(resultData);
						String resultOk=jsonObject.getString("result");
						if("success".equals(resultOk)) {
							messagedata.setAttachmentLocal(null);

							if (file.exists()) {
								file.delete();
								ErrorController.showMessage("file delete");
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				try {
					JSONObject jsonObject = new JSONObject(resultData);
					String resultOk=jsonObject.getString("result");
                    ErrorController.showMessage("imageUploder",jsonObject.toString());

					if("success".equals(resultOk)) {
						String [] imageUpload= new String[5];
						imageUpload[0] = jsonObject.getString("org_file");
						imageUpload[1] = jsonObject.getString("thumb_file");
						imageUpload[2] = jsonObject.getString("save_file");
						imageUpload[3] = jsonObject.getString("thumb_height");
						imageUpload[4] = jsonObject.getString("thumb_width");

						JSONObject value = new JSONObject();
						value.put("org_file", imageUpload[0]);
						value.put("thumb_file", imageUpload[1]);
						value.put("save_file", imageUpload[2]);
						value.put("thumb_height", imageUpload[3]);
						value.put("thumb_width", imageUpload[4]);
						value.put("cuser_type", MessengerInfo.getUserType());
						value.put("crisk_type", MessengerInfo.getRiskType());

						messagedata.setAttachment(value.toString());

						context.setChatDataSend(messagedata);

						ChatPresenter presenter = new ChatPresenter(null);
						presenter.sendMessage(context, messagedata);

					}


				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}.execute();
	}

	public void chatFileUploadReceive(final Context context, final Uri uri) {
		getDataWorker = new AsyncTask<Void, Void, String>() {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
                LogTrace.E("image share start");
			}

			@Override
			protected String doInBackground(Void... params) {

                File file = null;

				try {
					HttpClient httpclient = new DefaultHttpClient();

					httppost = new HttpPost(
							Constants.CHATUPLOAD_PROC);

					CustomMultiPartEntity entity = new CustomMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundary, Charset.forName("UTF-8"), new CustomMultiPartEntity.ProgressListener() {
						@Override
						public void transferred(long num) {
							// TODO Auto-generated method stub
							try {
								currentFileSize = (int) num;
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					} );

					currentFileSize = 0;
                    file = new File(getPath(context,uri));
					maxFileSize = (int) file.length();
					entity.addPart("file", new FileBody(file));
					httppost.setEntity(entity);
					httppost.setHeader("Accept-Charset", "UTF-8");
					httppost.addHeader("Content-Type", "multipart/form-data; boundary="
							+ boundary);
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity resEntity = response.getEntity();
					BufferedReader reader = new BufferedReader(new InputStreamReader(
							response.getEntity().getContent(), "UTF-8"));
					String sResponse;
					StringBuilder s = new StringBuilder();
					while ((sResponse = reader.readLine()) != null) {
						s = s.append(sResponse);
					}
					resultData = s.toString();

					ErrorController.showMessage("Result : " + resultData);

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                }

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				try {
                    LogTrace.E("image share end");

					JSONObject jsonObject = new JSONObject(resultData);
					String resultOk=jsonObject.getString("result");

					if("success".equals(resultOk)) {
                        LogTrace.E("image share success");

						String [] imageUpload = new String[5];
						imageUpload[0]=jsonObject.getString("org_file");
						imageUpload[1]=jsonObject.getString("thumb_file");
						imageUpload[2]=jsonObject.getString("save_file");
						imageUpload[3]=jsonObject.getString("thumb_height");
						imageUpload[4]=jsonObject.getString("thumb_width");

						JSONObject value = new JSONObject();
						value.put("org_file", imageUpload[0]);
						value.put("thumb_file", imageUpload[1]);
						value.put("save_file", imageUpload[2]);
						value.put("thumb_height", imageUpload[3]);
						value.put("thumb_width", imageUpload[4]);

						Bundle bundle = new Bundle();

						bundle.putString("messageType", "2");
						bundle.putString("message", "(사진)");
						bundle.putString("attachment", value.toString());

						Intent intent = new Intent(context, Activity_Delivery.class);
						intent.putExtras(bundle);
						context.startActivity(intent);
					}


				}catch (Exception e){

				}

			}

		}.execute();



	}

	public static String getPath(final Context context, final Uri uri) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];
				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/" + split[1];
				}
			}
			else if (isDownloadsDocument(uri)) {
				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] {
						split[1]
				};

				return getDataColumn(context, contentUri, selection, selectionArgs);
			}
		}
		else if ("content".equalsIgnoreCase(uri.getScheme())) {
			return getDataColumn(context, uri, null, null);
		}
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

	public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = {column};
		try {
			cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
					null);
			if (cursor != null && cursor.moveToFirst()) {
				final int column_index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(column_index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	public static String setValue(String key, String value) {
		return "Content-Disposition: form-data; name=\"" + key + "\"r\n\r\n" + value;
	}

	public static String setFile(String key, String fileName) {
		return "Content-Disposition: form-data; name=\"" + key + "\";filename=\"" + fileName + "\"\r\n";
	}
}