package com.wave.messenger.sdk.Util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

import java.util.HashMap;


public class TranService extends Service {
	//private static final int EVENT_CONTINUE_MMS_CONNECTIVITY = 3;
    //private static final int APN_EXTENSION_WAIT = 30 * 1000;
	public static final int EVENT_RESTART = 4;
    public static final int EVENT_TOAST = 5;
    public static final int EVENT_TOAST_PLAIN = 6;
    
    public class RestartThread extends Thread {
    
    	@Override
		public void run() {
			// TODO Auto-generated method stub
			/*try {
				
				String addr = Connector.getInstance().getTcpAddress();
				int port = Connector.getInstance().getTcpPort();
				
				client.close();                    	                    					
				client.Start(addr, port);
				
				interrupt();
			} catch (Exception e) {
				
			}	*/
		}
    }
    /*
    static CheckTopThread checkTopThread = null;
    public class CheckTopThread extends Thread {
    	
    	@Override
    	public void run() {
    		// TODO Auto-generated method stub
    		try {
    			boolean checkPackage = true;
    			ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
    			List<RunningTaskInfo> info = am.getRunningTasks(1);
    			ComponentName topActivity = info.get(0).topActivity;
    			String name = topActivity.getPackageName();
    			while (checkPackage) {
    				info = am.getRunningTasks(1);
    				topActivity = info.get(0).topActivity;
    				name = topActivity.getPackageName();
    				
    				if(!name.equals(getPackageName())) {
    					if(MlFragmentMain.getInstance() != null) {
    						MlFragmentMain.getInstance().isLocked = true;
    					} else {
    						interrupt();
    						break;
    					}
    				} else {
    					if(MlFragmentMain.getInstance() != null && MlFragmentMain.getInstance().isLocked && !MessengerInfo.getLockPassword(MlFragmentMain.getInstance().getActivity()).equals("")) {
    						MlFragmentMain.getInstance().isLocked = false;
    						com.misslee.hana.MlActivityManager.startActivityLockCheck(MlFragmentMain.getInstance().getActivity(), false);
    					}
    				}
    				Thread.sleep(100);
				}
    		} catch (Exception e) {
    			
    		}					
    	}
    }
    */
    
	/*private static SimpleChatClient client = null;
	public static SimpleChatClient getClient() {
		return client;		
	}
	public static void stopService() {
		try {
			try {
				if(client != null) {
					client.close();					
				}
				*//*
				if(checkTopThread != null && 
						checkTopThread.isAlive()) {
					checkTopThread.interrupt();
					checkTopThread = null;
				}
				*//*
			} catch (Exception e) {
				
			}
			client = null;
		} catch (Exception e) {
			
		}				
	}*/

	private static TranService instance = null;
	public static TranService getInstance() {
		return instance;
	}
	
	//private PowerManager.WakeLock mWakeLock;
	private ConnectivityManager mConnMgr;
	//private ServiceHandler mServiceHandler;
	ServiceHandler handler = new ServiceHandler();
	
	
	public ServiceHandler getHandler() {
		return handler;
	}
	
	@Override
	public void onCreate() {
		try {
			instance = this;
			
			super.onCreate();
		} catch (Exception e) {
			
		}
	}
	
	
	@Override 
	public int onStartCommand(Intent intent, int flags, int startid) {
		/*instance = this;
		ApplicationMain.getDataProvider();
		mConnMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);		
        //mServiceHandler = new ServiceHandler();
        
        //Myinfo.setRcvTic(this, 0);
        //BeepManager.stopBeepSoundAndVibrate(this);
        
        //createWakeLock();
        //acquireWakeLock();
		
		handleStart();				
		
		//return super.onStartCommand(intent, flags, startid);*/
		return START_STICKY;
	}
	
	
	void handleStart() {
		/*try {
			
			if(client == null) {
				client = SimpleChatClient.getInstance();																
			} 
			
			if (client.isConnected() == false) {
				AsyncTask<Void, Void, Void> getDataWorker = new AsyncTask<Void, Void, Void>() {
					
					@Override
					protected void onPreExecute() {
						super.onPreExecute();
						
					}
					
					@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						
						try {
							if(client != null) {	
								String addr = Connector.getInstance().getTcpAddress();
								int port = Connector.getInstance().getTcpPort();
								
								
								//client.Start(MyInfo.getHost(TranService.this), MyInfo.getPort(TranService.this));
								client.Start(addr, port);
							}
							*//*
							if(checkTopThread == null) {
								checkTopThread = new CheckTopThread();
								checkTopThread.start();
							}
								*//*
						} catch (Exception e) {	
							
						}
							
						return null;
					}
					@Override
					protected void onPostExecute(Void result) {
						super.onPostExecute(result);		
					}		
				};
				if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) getDataWorker.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null); else getDataWorker.execute();	
			}
			
		} catch (Exception e) {
			
		}*/
	}
	
	@Override
	public void onDestroy() {
		/*try {
			stopService();
			//releaseWakeLock();
		} catch (Exception e) {
			
		}*/
		
	}
	
   @Override 
    public void onStart(Intent intent, int startId) 
    { 
	   instance = this;
	   
    } 
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		instance = this;		
		return null;
	}
	
	public static void startService(Context context) {
		/*try {
		
			if(MessengerInfo.getLoginComplete(context)) {
				EvnetChat.setFRequestMsgDataTime(0);
				EvnetChat.setFReservedAlarmOff(false);
				EvnetChat.setFReservedMsgRequest(false);
				
				Intent intent = new Intent(context, TranService.class);
				//context.stopService(intent);		
				context.startService(intent);
			}
						
		} catch (Exception e) {
			// TODO: handle exception
		}*/
			
	}
	

	public static void releaseService(Context context) {
		try {
			Intent intent = new Intent(context, TranService.class);
			context.stopService(intent);
		} catch (Exception e) {
			
		}
	}
	
	public static boolean serviceCheck(Context context) {
		boolean result = false;
//		try {
			/*
			 * 1. 소켓 존재 여부.
			 */
			/*if(MessengerInfo.getLoginComplete(context)) {
			
				if(TranService.getInstance() == null || TranService.getClient() == null) {
					startService(context);
				} else {
					//네트워크 활성화.
					if(client.isConnected() == false) {						
						Handler tHandler = TranService.getInstance().getHandler();					
						Message eventMsg = tHandler.obtainMessage( EVENT_RESTART, null);
						tHandler.sendMessage(eventMsg);	

						result = false;
						return result;
					}
					
//Log.i("testApp", "0 check intaval : " + (System.currentTimeMillis() - client.getHeartBeatTick()) 
//							+ " , HeartBeatTick : " + client.getHeartBeatTick());
					
					if(client.getHeartBeatTick() != 0L) {
						long current = System.currentTimeMillis();	
						
						if(current - client.getHeartBeatTick() >= 30000L ) {
							
//Log.i("testApp", "1 check intaval : " + (current - client.getHeartBeatTick()) 
//		+ " , HeartBeatTick : " + client.getHeartBeatTick());
							
							TranService trans = TranService.getInstance();
							int wakeLockResult = 0;
							*//*
							if(trans != null) {
								trans.createWakeLock();
								if (trans.mWakeLock.isHeld() == false) {
									wakeLockResult = 1;
									trans.acquireWakeLock();
								}							
							}
							*//*
							
							Handler tHandler = TranService.getInstance().getHandler();					
							Message eventMsg = tHandler.obtainMessage( EVENT_RESTART, null);
							tHandler.sendMessage(eventMsg);	
							
							
							*//*
							if(trans != null) {
								if (wakeLockResult == 1) {	
									trans.releaseWakeLock();								
								}
							}
							*//*
							
							result = false;
							return result;
						}					
					}										
		
					
					if(client.getSendHeartBeatTick() != 0L) {
						long current = System.currentTimeMillis();	
							
						//120 초 동안 패킷 전송 안하면 해제
						if(current - client.getSendHeartBeatTick() >= 120000L ) {						
							int wakeLockResult = 0;
	
							TranService trans = TranService.getInstance();
							*//*
							if(trans != null) {
								trans.createWakeLock();
								
								if (trans.mWakeLock.isHeld() == false) {
									wakeLockResult = 1;
									trans.acquireWakeLock();
								}							
							}
							*//*
									
							Handler tHandler = TranService.getInstance().getHandler();					
							Message eventMsg = tHandler.obtainMessage( EVENT_RESTART, null);
							tHandler.sendMessage(eventMsg);	
							

							
							*//*
							if(trans != null) {
								if (wakeLockResult == 1) {	
									trans.releaseWakeLock();								
								}
							}
							*//*
						} else {
							result = true;
						}
					}
				}
			}
		} catch (Exception e) {
			
			
		}*/
		
		return result;
	}
	
	
	public final class ServiceHandler extends Handler {
        public ServiceHandler() {
            super();
        }

        @SuppressWarnings("unchecked")
		@Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case EVENT_RESTART:

                    try {                      	
                    	RestartThread restartThread = new RestartThread();
                    	restartThread.start();                    	
                    } catch (Exception e) {
                    	
                    }

                    return;                    
                case EVENT_TOAST:
                	
                	HashMap<String, String> map = (HashMap<String, String>) msg.obj;
                	String name = map.get("name");
                	String dmsg = map.get("msg");
                	String userId = map.get("userId");
                	showToast(getApplication(), name, dmsg, userId);
                	
                	return;
                	
                case EVENT_TOAST_PLAIN:
                
                	HashMap<String, String> map2 = (HashMap<String, String>) msg.obj;
                	String toastMsg = map2.get("msg");
                	
                	showToastPlain(getApplication(), toastMsg);
                	return;
                default:
                    return;
            }
        }
    }
	
	static Toast toast = null;
	public static void showToast(Context context, String userName, String contents, String userId){
		/*try {
			if(toast != null) {
				toast.cancel();
				toast = null;
			}
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View v = inflater.inflate(R.layout.layout_toast, null);
	        TextView textViewUserName = (TextView) v.findViewById(R.id.textViewUserName);
	        TextView textViewContents = (TextView) v.findViewById(R.id.textViewContents);
	        ImageView imageViewUser = (ImageView) v.findViewById(R.id.imageViewUser);
	        
	        textViewUserName.setText(userName);
	        textViewContents.setText(contents);
	        
	        String url = Connector.getInstance().getImageBaseUrl() + "files/profile/thumb/" + userId + ".jpg";								
			UnivImageLoader.getImageLoader(context).displayImage(url
					, imageViewUser,  UnivImageLoader.getNonDiskCashBasicOptions());
	        
	        toast = new Toast(context.getApplicationContext());//TranService.getInstance());
	        toast.setGravity(Gravity.TOP, 0, 150);
	        toast.setDuration(Toast.LENGTH_SHORT);
	        toast.setView(v);
	        toast.show();
			
		} catch (Exception e) {
			
		}*/
    }
	
	

	public static void showToastPlain(Context context, String contents){
		/*try {
			if(toast != null) {
				toast.cancel();
				toast = null;
			}
			
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        View v = inflater.inflate(R.layout.layout_toast, null);
	        TextView textViewUserName = (TextView) v.findViewById(R.id.textViewUserName);
	        TextView textViewContents = (TextView) v.findViewById(R.id.textViewContents);
	        //ImageView imageViewUser = (ImageView) v.findViewById(R.id.imageViewUser);
	        
	        textViewUserName.setVisibility(View.GONE);
	        textViewContents.setText(contents);
	        
	        toast = new Toast(context.getApplicationContext());//TranService.getInstance());
	        toast.setGravity(Gravity.TOP, 0, 150);
	        toast.setDuration(Toast.LENGTH_SHORT);
	        toast.setView(v);
	        toast.show();
	        
		} catch (Exception e) {
			
		}*/
    }
	
	/*
	public synchronized void createWakeLock() {
        // Create a new wake lock if we haven't made one yet.
        if (mWakeLock == null) {
        	try {
        		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
                mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MMS Connectivity");
                mWakeLock.setReferenceCounted(false);				
			} catch (Exception e) {
				
			}
            
        }
    }
    public boolean isHeld() {
    	boolean result = false;
    	
    	try {
			if(mWakeLock != null) {
				result = mWakeLock.isHeld();
			}
		} catch (Exception e) {
			
		}
    	
    	return result;
    }
	public void acquireWakeLock() {
        // It's okay to double-acquire this because we are not using it
        // in reference-counted mode.
        try {
        	if(mWakeLock.isHeld() == false) {
        		mWakeLock.acquire();
        	}
        	
		} catch (Exception e) {
			
		}    	
               
    }

	public void releaseWakeLock() {
        // Don't release the wake lock if it hasn't been created and acquired.
        try {
        	
//        	if (mWakeLock != null && mWakeLock.isHeld()) {
 //               mWakeLock.release();
 //           }			
		} catch (Exception e) {
			
		}    	      
    }

    

    protected int beginMmsConnectivity() throws IOException {
        // Take a wake lock so we don't fall asleep before the message is downloaded.
    	createWakeLock();
        int result =3;
        try {
    
	        result = mConnMgr.startUsingNetworkFeature(
	                ConnectivityManager.TYPE_MOBILE,  "enableMMS");//Phone.FEATURE_ENABLE_MMS);
	
	        switch (result) {
	            case 0 : //Phone.APN_ALREADY_ACTIVE:
	            case 1 : //Phone.APN_REQUEST_STARTED:
	                acquireWakeLock();
	                return result;
	            case 3 : //	Phone.APN_REQUEST_FAILED              	
	            	 //failed
	        }
	        throw new IOException("Cannot establish MMS connectivity");
    	
		} catch (Exception e) {
			
		}
        return result;
    }

    protected void endMmsConnectivity() {
        try {

            // cancel timer for renewal of lease
//            mServiceHandler.removeMessages(EVENT_CONTINUE_MMS_CONNECTIVITY);
//            if (mConnMgr != null) {
//                mConnMgr.stopUsingNetworkFeature(
//                        ConnectivityManager.TYPE_MOBILE,
//                        "enableMMS");//Phone.FEATURE_ENABLE_MMS);
//            }
        } finally {
//			releaseWakeLock();
        }
    }
*/
    
/*
    private final class ServiceHandler extends Handler {
        public ServiceHandler() {
            super();
        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case EVENT_CONTINUE_MMS_CONNECTIVITY:

                    try {
                        int result = beginMmsConnectivity();
                        if (result != 0) {//Phone.APN_ALREADY_ACTIVE) {
                           
                            //        " instead of APN_ALREADY_ACTIVE");
                            // Just wait for connectivity startup without
                            // any new request of APN switch.
                            return;
                        }
                    } catch (IOException e) {
                  
                        return;
                    }

                    // Restart timer
                    sendMessageDelayed(obtainMessage(EVENT_CONTINUE_MMS_CONNECTIVITY),
                                       APN_EXTENSION_WAIT);
                    return;
                default:
                    return;
            }
        }
    }
    
    
    WifiManager.WifiLock wifiLock = null;
    public void enableWifiLock() {
    	try {
        	if (wifiLock == null) {
              	 WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
              	 wifiLock = wifiManager.createWifiLock("wifilock");
              	 wifiLock.setReferenceCounted(true);
              	 wifiLock.acquire();
            }
		} catch (Exception e) {
			
		}
    }
    
    public void disableWifiLock() {
    	try {    	    
            try {
            	if (wifiLock != null) {
            		 wifiLock.release();
            		 wifiLock = null;
            	} 			
    		} catch (Exception e) {
    			
    		}  
		} catch (Exception e) {
			
		}
    }  
    */
}
