package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;


/**
 * Created by leeyunsu on 2017-06-19.
 *
 * 모임정보화면 - 리스트에서 강제탈퇴 팝업
 *
 */

public class Dialog_ForceExit extends Dialog {

    private View.OnClickListener mListener;


    Context mContext;

    public Dialog_ForceExit(Context context) {
        super(context);
        mContext = context;
    }

//    arvalues.get(position).getRegUsr()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_force_exit);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        //init();

    }




    private void setOnClickEvent() {

        findViewById(R.id.tv_force_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });




        findViewById(R.id.tv_block).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    private View.OnClickListener mOnClickListener;
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }



}