package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;


/**
 * Created by aveapp on 2016-12-08.
 */

public class Dialog_MenuPriority extends Dialog {
    private TextView tv_title, tv_sub;
    private Button bt_ok;
    private View.OnClickListener mOnClickListener;
    private Context mContext;

    public Dialog_MenuPriority(Context context) {
        super(context);
        this.mContext = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_priority);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        //mnuTy 메뉴우선순위 종류(int) 0:컨텐츠 타임라인, 1:오픈채팅방(기본)
        String mnuTy= SharedObject.getProperty_string(mContext, Constants.mnuTy, "1");
        if(mnuTy.equals("0")){
            selectOptionA();
        }else {
            selectOptionB();
        }






        findViewById(R.id.ll_menu_priority1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectOptionA();

            }
        });

        findViewById(R.id.ll_menu_priority2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectOptionB();


            }
        });

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {

                    Log.e("Dialog_MenuPriority", "selectedId: ");

                    if (findViewById(R.id.ll_menu_priority1).isSelected()) {
                        v.setSelected(true);
                        SharedObject.setProperty_string(mContext, Constants.mnuTy, "0");

                    } else {
                        v.setSelected(false);
                        SharedObject.setProperty_string(mContext, Constants.mnuTy, "1");
                    }
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

    private void selectOptionA() {
        findViewById(R.id.ll_menu_priority1).setSelected(true);
        findViewById(R.id.ll_menu_priority2).setSelected(false);

        findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_on);
        ((TextView) findViewById(R.id.tv_btn1)).setTextColor(Util.getColor(mContext,R.color.confirm_green));

        findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_off);
        ((TextView) findViewById(R.id.tv_btn2)).setTextColor(Util.getColor(mContext,R.color.font_gray46));
    }

    private void selectOptionB() {
        findViewById(R.id.ll_menu_priority1).setSelected(false);
        findViewById(R.id.ll_menu_priority2).setSelected(true);

        findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_off);
        ((TextView) findViewById(R.id.tv_btn1)).setTextColor(Util.getColor(mContext, R.color.font_gray46));

        findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_on);
        ((TextView) findViewById(R.id.tv_btn2)).setTextColor(Util.getColor(mContext, R.color.confirm_green));

    }

    public void setData(String content) {
        //((TextView) findViewById(R.id.tv_content)).setText(content);
    }

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }
}
