package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;

/**
 * Created by aveapp on 2017-07-18.
 *
 * 1:애널리스트 다이얼로그
 * 0:매니져 다이얼로그
 *
 */

public class Dialog_professionalAccept1 extends Dialog {

    String TAG=this.getClass().getSimpleName();
    String Type;

    public Dialog_professionalAccept1(Context context,String type) {
        super(context);
        this.Type=type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_professional_accept1);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        initView();

        setOnclickEvent();
    }

    private void initView() {

        if(Type.equals("0")){   //메니져 소속지점.
            ((TextView)findViewById(R.id.tv_text)).setText(getContext().getResources().getString(R.string.department));

        }else{  //애널리스트 담당섹터
            ((TextView)findViewById(R.id.tv_text)).setText(getContext().getResources().getString(R.string.sector));
        }

    }


    private void setOnclickEvent() {

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.hideKeyboard(getContext(), (EditText) findViewById(R.id.edt_text1));
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);

                }

            }
        });
    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }




    /**
     *
     * 메니져 소속지점.
     * 애널리스트담당섹터 가져오기
     */
    public String getEdtex1() {

       if(TextUtils.isEmpty(( (EditText)findViewById(R.id.edt_text1)).getText().toString().trim())){
           return null;
       }else{
           return ( (EditText)findViewById(R.id.edt_text1)).getText().toString().trim();
       }
    }

    /**
     * 사번 가져오기
     */
    public String getEdtex2() {

        if(TextUtils.isEmpty(( (EditText)findViewById(R.id.edt_text2)).getText().toString().trim())){
            return null;
        }else{
            return ( (EditText)findViewById(R.id.edt_text2)).getText().toString().trim();
        }
    }


}
