package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_ChatTab_FriendList;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoGroupList;

import org.json.JSONObject;

import java.util.ArrayList;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;


/**
 * Created by aveapp on 2017-03-23.
 */

public class Dialog_GroupAdd extends Dialog {
    String TAG=this.getClass().getName();
    TextView tv_title, tv_cancle, tv_confirm;
    RadioButton button, radioBtnChosen;
    private RadioGroup rg_group;
    private String userId, gId, bId, buddyId;
    public Fragment_ChatTab_FriendList mFragmentFriendList;

    public Dialog_GroupAdd(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_groupadd);

        init();
        setEvent();
    }

    public void init() {
        tv_title = (TextView) findViewById(R.id.tv_dialogtitle);
        tv_cancle = (TextView) findViewById(R.id.tv_cancle);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        rg_group = (RadioGroup) findViewById(R.id.rg_group);
    }

    public void setEvent() {
        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        rg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int idRadiobtn = rg_group.getCheckedRadioButtonId();
                if (idRadiobtn > 0) {
                    radioBtnChosen = (RadioButton) findViewById(idRadiobtn);
//                    gId = button.getTag().toString();
                    gId = radioBtnChosen.getTag().toString();
                    LogTrace.E(gId);
                }
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupUserAddRequest();
                dismiss();
            }
        });
    }

    public void setRadioAdd(ArrayList<VoGroupList> voGroupLists, String bId, String buddyId) {
        userId = MessengerInfo.getUserId(getContext());
        this.bId = bId;
        this.buddyId = buddyId;
        for(int i = 0; i < voGroupLists.size(); i++) {
            button = new RadioButton(getContext());
            button.setText(voGroupLists.get(i).getGrpNm());
            button.setTextColor(Util.getColor(getContext(), R.color.black));
            button.setTag(voGroupLists.get(i).getgId());
            rg_group.addView(button);
        }
    }

    /**
     * 그룹 등록 요청
     */
    public void GroupUserAddRequest() {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", userId);
            value.put("gId", gId);
            value.put("bId", bId);
            value.put("buddyId", buddyId);
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, PacketTypes.PTC_IMS_GROUP_USER_ADD, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mMainActivityHandler.sendMessage(mMainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {

                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //그룹결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_GROUP_USER_ADD :    //그룹 리스트
                        MOALog.i("PTC_IMS_GROUP_LIST :");
                        MOALog.i("result=" + data.body.get("result"));
                        if(data.body.get("result").equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            BusProvider.getInstance().post(new FragmentEventHelper("FriendListRequest", null, null));
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });
}
