package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;


/**
 * Created by leeyunsu on 2017-06-07.
 */

public class Dialog_AddLeader extends Dialog {

    private View.OnClickListener mListener;
    Context mContext;

    public Dialog_AddLeader(Context context) {
        super(context);
        mContext = context;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_leader);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {

    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_later).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickLaterListener != null) {
                    mOnClickLaterListener.onClick(v);
                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;
    private View.OnClickListener mOnClickLaterListener;
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public void setLaterListener(View.OnClickListener listener) {
        mOnClickLaterListener = listener;
    }



}