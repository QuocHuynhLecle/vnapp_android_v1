package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;


/**
 * Created by leeyunsu on 2017-06-19.
 * <p>
 * 모임정보화면 - 리스트에서
 * <p>
 * 리더일때 회원 옵션 다이얼로그
 */

public class Dialog_MoimInfoListOption extends Dialog {

    private View.OnClickListener mListener;


    Context mContext;
    String mUsrLv; //선택한 유저의 레벨

    public Dialog_MoimInfoListOption(Context context, String usrLv) {
        super(context);
        mContext = context;
        mUsrLv =usrLv;
    }

//    arvalues.get(position).getRegUsr()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_member_list_option);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {
        //((TextView)findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.delete));




        Util.getSnglTy(mContext);

        switch (Util.getMymLv(mContext)) { //나의 레벨
            case "0":   //회원
                findViewById(R.id.tv_option2).setVisibility(View.GONE);

                //채팅권한
                if (Util.getSnglTy(mContext).equals("0")) {
                    findViewById(R.id.tv_option1).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.tv_option1).setVisibility(View.VISIBLE);
                }

                break;

            case "1":   //리더

                break;

            case "2":   //공동리더  탈퇴 권한 체크 1:리더(기본), 2:리더와 공동리더


                if (Util.getFrcTy(mContext).equals("1") //탈퇴권한  1이거나
                        || mUsrLv.equals("1")     //상대가 리더
                        || mUsrLv.equals("2")) {  //공동리더도 탈퇴 숨김
                    findViewById(R.id.tv_option2).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.tv_option2).setVisibility(View.VISIBLE);
                }

                break;
        }

    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        findViewById(R.id.tv_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        findViewById(R.id.tv_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


}