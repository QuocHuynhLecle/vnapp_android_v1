package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;


/**
 * Created by aveapp on 2016-12-08.
 */

public class Dialog_share extends Dialog {
    private TextView tv_title, tv_sub;
    private Button bt_ok;
    private View.OnClickListener mListener;

    public Dialog_share(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        /*findViewById(R.id.ll_option0).setOnClickListener(mListener);
        findViewById(R.id.ll_option1).setOnClickListener(mListener);
        findViewById(R.id.ll_option2).setOnClickListener(mListener);
        findViewById(R.id.ll_option3).setOnClickListener(mListener);*/
    }





    public void setListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.ll_option0).setOnClickListener(mListener);
        findViewById(R.id.ll_option1).setOnClickListener(mListener);
        findViewById(R.id.ll_option2).setOnClickListener(mListener);
        findViewById(R.id.ll_option3).setOnClickListener(mListener);
    }
}
