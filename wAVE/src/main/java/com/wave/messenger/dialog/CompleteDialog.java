package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;


/**
 * Created by aveapp on 2016-12-08.
 */

public class CompleteDialog extends Dialog {
    private TextView tv_title, tv_ok;
    private View.OnClickListener mListener;

    public CompleteDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_complete);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        /*tv_title = (TextView) findViewById(R.id.tv_title);
        tv_sub = (TextView) findViewById(R.id.tv_sub);
        bt_ok = (Button) findViewById(R.id.bt_ok);*/
    }

    /*public void setData(String title, String sub) {
        tv_title.setText(title);
        tv_sub.setText(sub);
    }*/

    public void setListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_ok).setOnClickListener(mListener);
    }
}
