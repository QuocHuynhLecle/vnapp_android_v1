package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 5. 10..
 */

public class Dialog_AddGroup extends Dialog {

    public static TextView tv_title, tv_titleDelete;
    public static EditText et_group;
    public static Button bt_cancel, bt_ok;

    public Dialog_AddGroup(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_addgroup);

        init();
        setEvent();
    }

    public void init() {
        tv_title = (TextView) findViewById(R.id.tv_dialogtitle);
        tv_titleDelete = (TextView) findViewById(R.id.tv_titleDelete);
        et_group = (EditText) findViewById(R.id.et_group);
        bt_cancel = (Button) findViewById(R.id.bt_cancel);
        bt_ok = (Button) findViewById(R.id.bt_ok);
    }

    public void setEvent() {
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    public void setTv_title(int title) {
        switch (title) {
            case 0:
                tv_title.setText("그룹 등록");
                break;
            case 1:
                tv_title.setText("그룹 수정");
                break;
            case 2:
                tv_title.setText("그룹 삭제");
                break;
        }
    }

    public void setEt_group(String groupName) {
        et_group.setText(groupName);
    }

    public void setDeleteView_Hide(boolean deleteView) {
        if (deleteView) {
            et_group.setVisibility(View.GONE);
            tv_titleDelete.setVisibility(View.VISIBLE);
        }
    }

    public void setOkListener(View.OnClickListener listener) {
        bt_ok.setOnClickListener(listener);
    }

    public String getGroupName() {
        return et_group.getText().toString().trim();
    }
}
