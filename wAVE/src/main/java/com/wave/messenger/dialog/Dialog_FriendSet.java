package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 8. 15..
 */

public class Dialog_FriendSet extends Dialog {

    String TAG = getClass().getSimpleName();

    public RadioButton btn_cancel, btn_block, btn_delete;
    private TextView tv_cancel, tv_confirm;
    private Context context;


    public Dialog_FriendSet(Context context) {
        super(context);
        this.context = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_friendset);

        init();
        setEvent();
    }

    private void init() {
        btn_cancel = (RadioButton) findViewById(R.id.btn_cancel);
        btn_block = (RadioButton) findViewById(R.id.btn_block);
        btn_delete = (RadioButton) findViewById(R.id.btn_delete);

        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);

//        tv_cancel.setOnClickListener(this);
//        tv_confirm.setOnClickListener(this);
    }

    private void setEvent() {

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.tv_cancel:
//                dismiss();
//                break;
//            case R.id.tv_confirm:
//                if (btn_cancel.isChecked()) {
//                    Moa.hideList(context, phone, 0, mActivityHandler);
//                } else if (btn_block.isChecked()) {
//                    Moa.blockList(context, phone, 1, mActivityHandler);
//                } else if (btn_delete.isChecked()) {
//
//                }
//
//                break;
//        }
//    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


  }
