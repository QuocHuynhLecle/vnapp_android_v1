package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by SJH on 2016-09-02. <br/>
 * 버튼 두개(확인 / 취소) 짜리 커스텀 다이얼로그.
 */
public class Dialog_Default extends Dialog {

    private TextView tvMessage;
    private Button ivCancel, ivOk;

    public Dialog_Default(Context context) {
        super(context,android.R.style.Theme_Translucent_NoTitleBar);
        setContentView(R.layout.dialog_default);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setDimAmount(0.7f);
        initView();
    }


    public Dialog_Default(Context context, String text) {
        super(context,android.R.style.Theme_Translucent_NoTitleBar);
        setContentView(R.layout.dialog_default);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setDimAmount(0.7f);
        initView();
        tvMessage.setText(text);
    }

    private void initView(){
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        ivCancel = (Button) findViewById(R.id.btCancel);
        ivOk = (Button) findViewById(R.id.btOk);
    }

    public void setCancelEvent(View.OnClickListener listener){
        this.ivCancel.setOnClickListener(listener);
    }

    public void setOkEvent(View.OnClickListener listener){
        this.ivOk.setOnClickListener(listener);
    }

    public void setTextMessage(String text){
        this.tvMessage.setText(text);
    }

}
