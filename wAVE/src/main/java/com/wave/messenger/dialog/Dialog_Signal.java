package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.CustomTextWathcer;
import com.wave.messenger.util.Util;


/**
 * Created by aveapp on 2017-03-23.
 */

public class Dialog_Signal extends Dialog {

    public static String itemCode, itemName, itemSg;
    public static TextView tvItemName, tvSg, tvPv, tvCv;
    public static Button btnBuy, btnSell, ivOk, ivCancle;
    public static ImageView ivClose;

    public Dialog_Signal(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_signal);

        init();
        setEvent();
    }

    public void init() {
        tvItemName = (TextView) findViewById(R.id.tvItemName);
        tvSg = (TextView) findViewById(R.id.tvSg);
        tvPv = (TextView) findViewById(R.id.tvPv);
        tvCv = (TextView) findViewById(R.id.tvCv);
        btnBuy = (Button) findViewById(R.id.btnBuy);
        btnSell = (Button) findViewById(R.id.btnSell);
        ivOk = (Button) findViewById(R.id.ivOk);
        ivCancle = (Button) findViewById(R.id.ivCancle);
        ivClose = (ImageView) findViewById(R.id.ivClose);
        btnBuy.setEnabled(false);
        btnBuy.setBackgroundResource(R.drawable.ic_buying);
        btnBuy.setTextColor(Util.getColor(getContext(), R.color.white));
        btnSell.setEnabled(true);
        btnSell.setBackgroundResource(R.drawable.ic_sell_nor);
        btnSell.setTextColor(Util.getColor(getContext(), R.color.black));
        tvPv.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        tvCv.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    public void setEvent() {
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBuy.setEnabled(false);
                btnBuy.setBackgroundResource(R.drawable.ic_buying);
                btnBuy.setTextColor(Util.getColor(getContext(), R.color.white));
                btnSell.setEnabled(true);
                btnSell.setBackgroundResource(R.drawable.ic_sell_nor);
                btnSell.setTextColor(Util.getColor(getContext(), R.color.black));
            }
        });

        btnSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBuy.setEnabled(true);
                btnSell.setEnabled(false);
                btnSell.setBackgroundResource(R.drawable.ic_sell);
                btnSell.setTextColor(Util.getColor(getContext(), R.color.white));
                btnBuy.setBackgroundResource(R.drawable.ic_buying_nor);
                btnBuy.setTextColor(Util.getColor(getContext(), R.color.black));
            }
        });

        tvPv.addTextChangedListener(new CustomTextWathcer((EditText) tvPv));
        tvCv.addTextChangedListener(new CustomTextWathcer((EditText) tvCv));
    }

    public boolean checkButton() {
        int getVisibility = btnBuy.getVisibility();
        return false;
    }

    public void setData(String stnm, String stcd, String stsg) {
        tvItemName.setText(stnm);
        tvSg.setText(stsg);
    }

    public void setCancelEvent(View.OnClickListener listener){
        this.ivCancle.setOnClickListener(listener);
    }

    public void setOkEvent(View.OnClickListener listener){
        this.ivOk.setOnClickListener(listener);
    }
}
