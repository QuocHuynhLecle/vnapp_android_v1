package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;
import com.wave.messenger.vo.VoBoardRead;

/**
 * Created by aveapp on 2017. 7. 19..
 */

public class Dialog_Reply extends Dialog {

    private View.OnClickListener mListener;

    //String refortType;
    Context mContext;
    //String mUser;
    //VoBoardList.VoBoardItem   voBoardItem;
    //TextView tv_option1, tv_option2;
    //boolean isLeader;

    //boolean bOwner;

   /* public Dialog_Reply(Context context, boolean bOwner, boolean isLeader) {
        super(context);
        //this.refortType = type;
        mContext = context;
        //this.mUser = mUser;
        this.isLeader = isLeader;
        this.bOwner=bOwner;

        Log.e("Dialog_Reply","Dialog_Reply bOwner: "+bOwner);
    }*/
   VoBoardRead.VoReadReply mArVoReadReply;

    public Dialog_Reply(Context context, VoBoardRead.VoReadReply arVoReadReply) {
        super(context);
        mContext = context;
        mArVoReadReply=arVoReadReply;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_reply);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {

        //일단 팀장님지시로 수정기능 숨겨놓기.
        //다시 보이기...이게뭔가...0814
        //findViewById(R.id.tv_option1).setVisibility(View.GONE);
        //String strRrefort = null;

        //strRrefort = mContext.getResources().getString(R.string.report_info1);
        /*switch (refortType){
            case "1":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "2":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "3":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "4":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
        }*/

        //tv_option1 = (TextView) findViewById(R.id.tv_option1);
        //tv_option2 = (TextView) findViewById(R.id.tv_option2);
        /*Log.i("TEST", "리더인가요2 : " + isLeader);
        if(isLeader == true){
            findViewById(R.id.tv_option2).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.tv_option2).setVisibility(View.GONE);
        }*/


        findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);  //누구나 댓글 복사.




        switch (Util.getMymLv(mContext)){
            case "1":   //리더
                findViewById(R.id.tv_option2).setVisibility(View.VISIBLE); //리더면 무조건삭제.

                break;
            case "2":   //공동리더


                if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getDelTy(mContext))) {   //나의 권한 체크
                    findViewById(R.id.tv_option2).setVisibility(View.VISIBLE);

                    if(mArVoReadReply.getUsrLv().equals("1") ||mArVoReadReply.getUsrLv().equals("2")){      //상대가 리더,공동리더이면 삭제금지
                        findViewById(R.id.tv_option2).setVisibility(View.GONE);
                    }
                }else{
                    //삭제금지.
                }

                break;
            case "0":   //일

                break;

        }

        //공통
        if(Util.isOwner(mContext, mArVoReadReply.getRegUsr())){
            findViewById(R.id.tv_option1).setVisibility(View.VISIBLE);  //수정
            findViewById(R.id.tv_option2).setVisibility(View.VISIBLE);  //삭제


        } else {
            //findViewById(R.id.tv_option1).setVisibility(View.GONE);
            //findViewById(R.id.tv_option2).setVisibility(View.GONE);
            findViewById(R.id.tv_option3).setVisibility(View.VISIBLE);  //신고
        }
    }

    private void setOnClickEvent() {

    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


    /**
     * 클립보드
     * @param listener
     */
    public void setClipboardListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_option0).setOnClickListener(mListener);
    }

    /**
     * 댓글수정
     * @param listener
     */
    public void setModifyListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_option1).setOnClickListener(mListener);
    }

    /**
     * 댓글삭제
     * @param listener
     */
    public void setDeleteListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_option2).setOnClickListener(mListener);
    }

    /**
     * 댓글신고하기
     * @param listener
     */
    public void setReportListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_option3).setOnClickListener(mListener);
    }

}
