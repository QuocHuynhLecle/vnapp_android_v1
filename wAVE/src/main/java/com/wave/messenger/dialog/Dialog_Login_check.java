package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.wave.massenger.piggy.R;

/**
leeyunsu
 */
public class Dialog_Login_check extends Dialog {

    private Button  ivOk;

    public Dialog_Login_check(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        setContentView(R.layout.dialog_login_check);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        // getWindow().setDimAmount(0.7f);
        initView();
        }

    private void initView() {
        ivOk = (Button) findViewById(R.id.btOk);
        ivOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        dismiss();
            }
        });
    }

    public void setOkEvent(View.OnClickListener listener) {
        this.ivOk.setOnClickListener(listener);
    }


}
