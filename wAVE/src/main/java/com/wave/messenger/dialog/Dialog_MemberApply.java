package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;
import com.wave.messenger.vo.VoMoimMemberList;


/**
 * Created by aveapp on 2016-06-12.
 *
 * 모임
 */

public class Dialog_MemberApply extends Dialog {
    private TextView tv_title, tv_sub;
    private Button bt_ok;
    private View.OnClickListener mOnAcceptClickListener;
    private View.OnClickListener mOnRejecttClickListener;
    private Context mContext;
    private VoMoimMemberList.VoMoimMemberlistItem mVoMoimMemberlistItem;
    private int mnPosition;

    public Dialog_MemberApply(Context context, VoMoimMemberList.VoMoimMemberlistItem voMoimMember, int position) {
        super(context);
        this.mContext = context;
        this.mVoMoimMemberlistItem = voMoimMember;
        this.mnPosition = position;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_apply);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        findViewById(R.id.tv_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnRejecttClickListener != null) {

                    v.setId(mnPosition);
                    mOnRejecttClickListener.onClick(v);
                }
            }
        });



        findViewById(R.id.tv_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnAcceptClickListener != null) {

                    v.setId(mnPosition);
                    mOnAcceptClickListener.onClick(v);
                }
            }
        });


        //Util.setGlideProfile(mContext,mVoMoimMemberlistItem.getUserId(),(ImageView)findViewById(R.id.imgv_profile));
        //Util.setMoimProfileGlide(mContext, SharedObject.getProperty_string(mContext, Constants.MOIM_ID, null), mVoMoimMemberlistItem.getPfImg() ,(ImageView) findViewById(R.id.imgv_profile));
        Util.setMoimProfileGlide2(mContext, mVoMoimMemberlistItem.getUserId(), (ImageView) findViewById(R.id.imgv_profile) );


        //Glide.with(mContext).load(mVoMoimMemberlistItem.getPfImg()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
        //        .placeholder(R.drawable.ic_profile_big_default_android).dontAnimate().into((ImageView)findViewById(R.id.imgv_profile));

        //mVoMoimMember.getDate()

        ((TextView)findViewById(R.id.tv_name)).setText(mVoMoimMemberlistItem.getUsNm());
        ((TextView)findViewById(R.id.tv_date)).setText(mContext.getString(R.string.date_apply, mVoMoimMemberlistItem.getRegDt()));

        //질문가입
        if(mVoMoimMemberlistItem.getQstn()!=null){
            findViewById(R.id.ll_qstn).setVisibility(View.VISIBLE);

            ((TextView)findViewById(R.id.tv_qstn)).setText(mVoMoimMemberlistItem.getQstn());
            ((TextView)findViewById(R.id.tv_answ)).setText(mVoMoimMemberlistItem.getAnsw());
        }


        /*Glide.with(mContext).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL+mVoMoimMemberlistItem.getPfImg()).diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true).placeholder(R.drawable.free)
                .dontAnimate().into(mImage);*/

        //TODO 가입 신청자의 썸네일 처리?? 모임프로필?   개인 프로필사진?
        /*Util.setMoimProfileGlide(mContext, SharedObject.getProperty_string(mContext, Constants.MOIM_ID, null)
                , mVoMoimMemberlistItem.getPfImg()
                , (ImageView)findViewById(R.id.imgv_profile));*/

        //개인 프로필썸네일
        Util.setGlideProfile(mContext, mVoMoimMemberlistItem.getUserId(),(ImageView)findViewById(R.id.imgv_profile));


    }



    public void setAcceptListener(View.OnClickListener listener) {
        mOnAcceptClickListener = listener;
    }
    public void setRejectListener(View.OnClickListener listener) {
        mOnRejecttClickListener = listener;
    }
}
