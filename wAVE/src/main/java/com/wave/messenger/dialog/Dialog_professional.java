package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;


/**
 * Created by aveapp on 2017-07-18.
 * <p>
 * 모임생성
 * 전문가/메니저애널리스트 선택 다이얼로그
 */

public class Dialog_professional extends Dialog {
    private int mnSelectedNum=0; //선택한 아이템번호
    String TAG=this.getClass().getSimpleName();

    public Dialog_professional(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_professional);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        initView();

        setSelectImage(2);

        setOnclickEvent();
    }


    private void initView() {

    }

    private void setOnclickEvent() {

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);

                }
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });


        //전문가
        findViewById(R.id.ll_select1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectImage(2);
            }
        });


        //메니져
        findViewById(R.id.ll_select2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectImage(0);
            }
        });


        //애널리스트
        findViewById(R.id.ll_select3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectImage(1);
            }
        });
    }

    /**
     * 0:매니저, 1:애널리스트, 2:전문가
     */
    private void setSelectImage(int select) {

        mnSelectedNum = select;

        switch (select) {
            case 1:  //애널리스트

                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_on);

                ((TextView) findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView) findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView) findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));

                break;

            case 2: //전문가
                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_on);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_off);

                ((TextView) findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));
                ((TextView) findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView) findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));


                break;

            case 0: //메니져
                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_on);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_off);

                ((TextView) findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView) findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));
                ((TextView) findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                break;
        }

    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public String getSelect(){
        Log.e(TAG,"getSelect :" +mnSelectedNum);
        return mnSelectedNum+"";
    }


}
