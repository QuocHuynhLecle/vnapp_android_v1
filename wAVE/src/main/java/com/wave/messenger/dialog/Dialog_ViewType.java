package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_MoimTab2_MyMoimList;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;


/**
 * Created by leeyunsu on 2017-08-12.
 *
 * 글게시 항목 선택 다이얼로그
 */

public class Dialog_ViewType extends Dialog {

    private View.OnClickListener mListener;
    Fragment_MoimTab2_MyMoimList.OnSelectViewTypeInterface mOnSelectViewTypeInterface;

    Context mContext;
    int mnSelect=1;

    public Dialog_ViewType(Context context, Fragment_MoimTab2_MyMoimList.OnSelectViewTypeInterface onSelectViewTypeInterface) {
        super(context);
        this.mContext=context;
        this.mOnSelectViewTypeInterface=onSelectViewTypeInterface;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_view_type);
        setCanceledOnTouchOutside(true);
        setCancelable(true);


        setViewType();
        setOnClickEvent();
    }


    /**
     * 저장된 타입값 가져와서
     * 라디오버튼 활성화
     */
    private void setViewType(){
        int ntype= SharedObject.getProperty_int(getContext(), Constants.PREF_VIEW_TYPE,1 );
        selectType(ntype);

    }
    /**
     * 아이템 선택
     */
    private void selectType(int selectedId) {
        mnSelect=selectedId;

        ((ImageView)findViewById(R.id.imgv_btn1)).setImageResource(R.drawable.ic_radio_box_off);
        ((ImageView)findViewById(R.id.imgv_btn2)).setImageResource(R.drawable.ic_radio_box_off);
        ((ImageView)findViewById(R.id.imgv_btn3)).setImageResource(R.drawable.ic_radio_box_off);

        ((TextView)findViewById(R.id.tv_type1)).setTextColor(Util.getColor(mContext,R.color.font_gray46));
        ((TextView)findViewById(R.id.tv_type2)).setTextColor(Util.getColor(mContext,R.color.font_gray46));
        ((TextView)findViewById(R.id.tv_type3)).setTextColor(Util.getColor(mContext,R.color.font_gray46));

        switch (selectedId){
            case 1:
                ((ImageView)findViewById(R.id.imgv_btn1)).setImageResource(R.drawable.ic_radio_box_on);
                ((TextView)findViewById(R.id.tv_type1)).setTextColor(Util.getColor(mContext,R.color.font_green));
                break;
            case 2:
                ((ImageView)findViewById(R.id.imgv_btn2)).setImageResource(R.drawable.ic_radio_box_on);
                ((TextView)findViewById(R.id.tv_type2)).setTextColor(Util.getColor(mContext,R.color.font_green));
                break;
            case 3:
                ((ImageView)findViewById(R.id.imgv_btn3)).setImageResource(R.drawable.ic_radio_box_on);
                ((TextView)findViewById(R.id.tv_type3)).setTextColor(Util.getColor(mContext,R.color.font_green));

                break;
        }
    }

    private void setOnClickEvent() {

        findViewById(R.id.tv_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //선택한 타입 저장
                SharedObject.setProperty_int(getContext(), Constants.PREF_VIEW_TYPE, mnSelect);

                mOnSelectViewTypeInterface.onSelectView();
                dismiss();
            }
        });

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        findViewById(R.id.ll_type1).setOnClickListener(new View.OnClickListener() { //2칸
            @Override
            public void onClick(View v) {
                selectType(1);
            }
        });
        findViewById(R.id.ll_type2).setOnClickListener(new View.OnClickListener() { //3칸
            @Override
            public void onClick(View v) {
                selectType(2);
            }
        });
        findViewById(R.id.ll_type3).setOnClickListener(new View.OnClickListener() {  //1칸
            @Override
            public void onClick(View v) {
                selectType(3);
            }
        });
    }


    public void setListener(View.OnClickListener listener) {
        //this.mListener = listener;
        findViewById(R.id.bt_ok).setOnClickListener(listener);
    }


}
