package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.NumberPicker;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;


/**
 * Created by leeyunsu on 2017-03-13.
 *
 * 모임새로만들기화면
 * 가입원제한 팝업
 */

public class Dialog_JoinLimit extends Dialog {

    private View.OnClickListener mListener;

    Constants.SET_PUBLIC_DIALOG_TYPE mType;
    int mNselected_JoinLimit;


    public Dialog_JoinLimit(Context context, Constants.SET_PUBLIC_DIALOG_TYPE type, int mNselected_JoinLimit) {
        super(context);
        this.mType=type;
        this.mNselected_JoinLimit=mNselected_JoinLimit;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_join_limit);
        setCanceledOnTouchOutside(true);

        setCancelable(true);

        initView();

        setOnClickEvent();



    }


    NumberPicker np;
    //TextView tv1, tv2;


    private void initView() {

        np = (NumberPicker) findViewById(R.id.numberPicker1);

        np.setMinValue(0);
        np.setMaxValue(6);
        np.setWrapSelectorWheel(false);
        np.setDisplayedValues(new String[]{"50","100","500","1000","3000","5000","10000"});
        np.setValue(mNselected_JoinLimit);



        /*if(mType== Constants.SET_PUBLIC_DIALOG_TYPE.new_moim_cover){ //새모임

        }else if(mType== Constants.SET_PUBLIC_DIALOG_TYPE.set_public_manage){ //모임설정관리

        }*/
    }

    /**
     * 취소
     */
    private void setOnClickEvent() {
        findViewById(R.id.btn_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    /**
     * 확인
     */
    public void setConfirmistener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.btn_confirm).setOnClickListener(mListener);
    }

    /**
     * 선택타입 리턴
     * @return
     */
    public int getSelect() {
        return np.getValue();
    }

}
