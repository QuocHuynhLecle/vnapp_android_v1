package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;


/**
 * Created by leeyunsu on 2017-03-13.
 * 모임 소개말 다이얼로그
 */

public class Dialog_Moim_introduce extends Dialog {

    String TAG=getClass().getSimpleName();
    private View.OnClickListener mListener;
    String strIntro;

    TextView tvConfirm;

    public Dialog_Moim_introduce(Context context,String intro) {
        super(context);
        strIntro = intro;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_moim_introduce);
        setCanceledOnTouchOutside(true);

        setCancelable(true);



        initView();

        setOnClickEvent();


    }

    private void initView() {
        Log.e(TAG,"initView "+strIntro);

        ((EditText)findViewById(R.id.edt_introduce)).setText(strIntro);

        tvConfirm = (TextView) findViewById(R.id.tv_confirm) ;


        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


    private void setOnClickEvent() {
    }

    /*public void setCancleListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_cancle).setOnClickListener(mListener);
    }*/

    public void setConfirmistener(View.OnClickListener listener) {
        this.mListener = listener;

        if(tvConfirm!=null)
            tvConfirm.setOnClickListener(mListener);
    }

    /**
     * edt소개말 적었는지 체크
     * @return
     */
    public boolean isEmpty() {

        return Util.isEmpty((EditText)findViewById(R.id.edt_introduce));
    }

    /**
     *소개말 텍스트 가져오기
     * @return
     */

    public String getIntroTxt() {
        return ((EditText)findViewById(R.id.edt_introduce)).getText().toString().trim();
    }


}
