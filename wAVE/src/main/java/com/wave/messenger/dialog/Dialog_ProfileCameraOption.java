package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.vo.VoBoardList;


/**
 * Created by leeyunsu on 2017-06-01.

 */
public class Dialog_ProfileCameraOption extends Dialog {

    private View.OnClickListener mListener;

    String mstrRegUsr;
    Context mContext;
    VoBoardList.VoBoardItem   voBoardItem;

    public Dialog_ProfileCameraOption(Context context) {
        super(context);
        //mstrRegUsr = regUsr;
        mContext = context;
        //voBoardItem = item;

    }

//    arvalues.get(position).getRegUsr()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_profile_camera_option);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {
        /*if(Util.isOwner(mContext,voBoardItem.getRegUsr())){ //내가 등록한 글인지 등록자 구분

            findViewById(R.id.tv_option6).setVisibility(View.VISIBLE);
            findViewById(R.id.tv_option7).setVisibility(View.VISIBLE);
        }else {
            findViewById(R.id.tv_option0).setVisibility(View.GONE);
        }

        if(voBoardItem.getNoti().equals("0")){
            //findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.timeline_option0));
        }else{
            ((TextView)findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.timeline_option0_1));
        }*/
    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        /*findViewById(R.id.tv_option3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });
        findViewById(R.id.tv_option4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });
        findViewById(R.id.tv_option5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });*/


    }


    private View.OnClickListener mOnClickListener;
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }



}