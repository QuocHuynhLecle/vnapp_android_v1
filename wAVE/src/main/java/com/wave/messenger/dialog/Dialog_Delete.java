package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.vo.VoChatData;

/**
 * Created by aveapp on 2017. 8. 31..
 */

public class Dialog_Delete extends Dialog {
    private Context context;
    private VoChatData data;
    public Dialog_Delete(Context context, final VoChatData data) {
        super(context);
        this.context = context;
        this.data = data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_delete);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                db.delChatData(data.getChatId());
                data.getListener().deleteMsg(data.getChatId());
                dismiss();
            }
        });
    }
}
