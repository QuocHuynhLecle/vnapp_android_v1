package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;


/**
 * Created by yunsu on 2017-03-24.
 *
 * 텍스트
 * 아니요 예
 * 확인
 *
 */

public class Dialog_Text extends Dialog {
    private View.OnClickListener mListener;
    private int mItemPosition;
    private TextView tvYes;
    private Context mContext;
    private String mstrContent;
    private String mstrtitle;

    Constants.DIALOG_BUTTON_TYPE type;

    private View.OnClickListener mOnClickListener;
    private View.OnClickListener mOnCancleListener;

    /*public Dialog_Text(Context context, String content, int position) {
        super(context);

        this.mContext = context;
        this.mItemPosition = position;
        this.mstrContent = content;
    }*/

    public Dialog_Text(Context context, String content) {
        super(context);
        this.mContext = context;
        this.mstrContent = content;

    }

    public Dialog_Text(Context context, String content, Constants.DIALOG_BUTTON_TYPE type) {
        super(context);
        this.mContext = context;
        this.mstrContent = content;
        this.type=type;
    }

    public Dialog_Text(Context context, String content,String title, Constants.DIALOG_BUTTON_TYPE type) {
        super(context);
        this.mContext = context;
        this.mstrContent = content;
        this.mstrtitle = title;
        this.type=type;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_text);
        setCanceledOnTouchOutside(true);
        setCancelable(true);


        LinearLayout layout = (LinearLayout)findViewById(R.id.ll_dialog);
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        int height = 0;

        switch (type){
            case confirm :  //확인버튼 다이얼로그 -채팅설정및 메뉴 우선순위결정 팝

                findViewById(R.id.tv_no).setVisibility(View.GONE);
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));

                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;
            case chat_invite:  //종목채팅방 친구 초대
                findViewById(R.id.tv_no).setVisibility(View.GONE);
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));

                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;
            case moim_delete ://모임삭제 다이얼로
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));

                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;

            case cancle_confirm :   //리더위임
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());

                break;

            case friend_block:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;
            case friend_delete:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;
            case logout:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;

            case write_out:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.moim_write_out));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.moim_write_delete_out));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;

            case modify_out:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.no));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.yes));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;

            case alram_read:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.no));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.yes));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;

            case leave_member:
                ((TextView)findViewById(R.id.tv_no)).setText(mContext.getResources().getString(R.string.cancel));
                ((TextView)findViewById(R.id.tv_yes)).setText(mContext.getResources().getString(R.string.confirm));
                ((TextView)findViewById(R.id.tv_yes)).setTextColor(Util.getColor(mContext,R.color.bg_purple));
                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, ViewGroup.LayoutParams.WRAP_CONTENT, mContext.getResources().getDisplayMetrics());
                break;
        }

        if(null!=mstrtitle){
            findViewById(R.id.tv_title).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_title)).setText(mstrtitle);
        }

        params.height = height;

        layout.setLayoutParams(params);

        ((TextView) findViewById(R.id.tv_content)).setText(mstrContent);

        findViewById(R.id.tv_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                if (mOnCancleListener != null) {
                    mOnCancleListener.onClick(v);

                }
            }
        });

        findViewById(R.id.tv_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

    /*public void setData(String content) {
        //((TextView) findViewById(R.id.tv_content)).setText(content);
    }*/


    public int getmItemPosition() {
        return mItemPosition;
    }


    //확인 취소버튼
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    //확인 버튼만
    public void setCancleListener(View.OnClickListener listener) {
        mOnCancleListener = listener;
    }
    public void setConfirmListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }
}
