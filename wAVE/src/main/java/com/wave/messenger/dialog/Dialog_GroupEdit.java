package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wave.massenger.piggy.R;


/**
 * Created by aveapp on 2017-03-23.
 *
 * 퍼미션 다이얼로그
 *
 *
 * 설정값 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
 *
 * 디자인순서와 다름.
 */

public class Dialog_GroupEdit extends Dialog {
    TextView tv_title, tv_titleDelete, tv_cancle, tv_confirm;
    EditText et_group;
    Button bt_cancel, bt_ok;

    public Dialog_GroupEdit( Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_groupedit);

        init();
        setEvent();
    }


    public void init() {
        tv_title = (TextView) findViewById(R.id.tv_dialogtitle);
        tv_titleDelete = (TextView) findViewById(R.id.tv_titleDelete);
        et_group = (EditText) findViewById(R.id.et_group);
        tv_cancle = (TextView) findViewById(R.id.tv_cancle);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
//        bt_cancel = (Button) findViewById(R.id.bt_cancel);
//        bt_ok = (Button) findViewById(R.id.bt_ok);
    }

    public void setEvent() {
        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    public void setTv_title(int title) {
        switch (title) {
            case 0:
                tv_title.setText("그룹 등록");
                break;
            case 1:
                tv_title.setText("그룹 수정");
                break;
            case 2:
                tv_title.setText("그룹 삭제");
                break;
        }
    }

    public void setEt_group(String groupName) {
        et_group.setText(groupName);
    }

    public void setDeleteView_Hide(boolean deleteView) {
        if (deleteView) {
            et_group.setVisibility(View.GONE);
            tv_titleDelete.setVisibility(View.VISIBLE);
        }
    }

    public void setOkListener(View.OnClickListener listener) {
        tv_confirm.setOnClickListener(listener);
    }

    public String getGroupName() {
        return et_group.getText().toString().trim();
    }
}
