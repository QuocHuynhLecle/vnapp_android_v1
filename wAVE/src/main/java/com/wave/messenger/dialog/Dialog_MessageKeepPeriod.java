package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;


/**
 * Created by yunsu on 2017-03-24.
 *
 * 대화내용 서버 보관기간
 */

public class Dialog_MessageKeepPeriod extends Dialog {
    private View.OnClickListener mOnClickListener;
    private Context mContext;

    public Dialog_MessageKeepPeriod(Context context) {
        super(context);
        this.mContext = context;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_message_keep_period);
        setCanceledOnTouchOutside(true);
        setCancelable(true);


        findViewById(R.id.ll_select1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEvent(v,0);
            }
        });
        findViewById(R.id.ll_select2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEvent(v,1);
            }
        });
        findViewById(R.id.ll_select3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEvent(v,2);
            }
        });
    }

    private void setEvent(View v, int i) {
        if (mOnClickListener != null) {
            v.setId(i);
            mOnClickListener.onClick(v);
        }
    }

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }




}
