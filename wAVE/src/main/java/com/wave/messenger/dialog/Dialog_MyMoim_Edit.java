package com.wave.messenger.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.InterfaceMymoimListener;


/**
 * 모임편집 팝업
 */
public class Dialog_MyMoim_Edit extends Dialog {

    private View.OnClickListener mListener;
    public InterfaceMymoimListener interfaceMymoimListener;

    public Dialog_MyMoim_Edit(Context context, InterfaceMymoimListener interfaceMymoimListener) {
        super(context);
        this.interfaceMymoimListener = interfaceMymoimListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_mymoim_edit);
        setCanceledOnTouchOutside(true);

        setCancelable(true);

        findViewById(R.id.tv_item1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceMymoimListener.onSelectItem(0);
                dismiss();
            }
        });

        findViewById(R.id.tv_item2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceMymoimListener.onSelectItem(1);
                dismiss();
            }
        });

        findViewById(R.id.tv_item3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interfaceMymoimListener.onSelectItem(2);
                dismiss();
            }
        });
    }
}
