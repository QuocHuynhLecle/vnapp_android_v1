package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;


/**
 * Created by leeyunsu on 2017-03-13.
 * 모임 타입설정
 */

public class Dialog_TypeSetting extends Dialog {

    private View.OnClickListener mListener;
    //private int nSelect=0; //0:비공개, 1:공개(기본), 2:모임명만 공개(삭제)
    //private boolean bToggle=true; //true:가입신청받기 false:받지않기

    String TAG=this.getClass().getSimpleName();
    String mOpen_type;  //공개 설정 0:비공개, 1:공개(기본), 2:모임명만 공개(삭제)
    String mEnter_type; //가입 설정 0:자동가입(기본), 1:승인

    Constants.SET_PUBLIC_DIALOG_TYPE mType;


    public Dialog_TypeSetting(Context context, Constants.SET_PUBLIC_DIALOG_TYPE type, String open_type, String enter_type) {
        super(context);
        this.mType=type;
        this.mOpen_type = open_type;
        this.mEnter_type = enter_type;

        Log.e("Dialog_TypeSetting","mType "+mType +" open_type:"+open_type +" mEnter_type: "+mEnter_type);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_type_setting);
        setCanceledOnTouchOutside(true);

        setCancelable(true);

        initView();

        setOnClickEvent();
    }

    private void initView() {

        Log.e(TAG,"mOpen_type: "+mOpen_type+" mEnter_type: "+mEnter_type);
        if(mType== Constants.SET_PUBLIC_DIALOG_TYPE.new_moim_cover){ //새모임
            //findViewById(R.id.ll_join).setVisibility(View.VISIBLE);
            //((ToggleButton)findViewById(R.id.togglebtn)).setChecked(false);
        }

            if(mOpen_type.equals("0")){

                selectCheckBtn(true,  false);
           /* }else if(mOpen_type.equals("2")){

                selectCheckBtn(false, true, false);*/
            }else if(mOpen_type.equals("1")){

                selectCheckBtn(false,  true);
            }


            if(mEnter_type.equals("1")){

                //findViewById(R.id.imgb_switch).setSelected(false);

                ((ToggleButton)findViewById(R.id.togglebtn)).setChecked(true);
                ((TextView)findViewById(R.id.tv_join_info)).setText(R.string.join_recive_info2);

            }else if(mEnter_type.equals("0")){

                //findViewById(R.id.imgb_switch).setSelected(true);
                ((ToggleButton)findViewById(R.id.togglebtn)).setChecked(false);
                ((TextView)findViewById(R.id.tv_join_info)).setText(R.string.join_recive_info1);


            }


    }

    private void selectCheckBtn(boolean b1,  boolean b3) {
        findViewById(R.id.img_btn1).setSelected(b1);
        //findViewById(R.id.img_btn2).setSelected(b2); 모임명만공개
        findViewById(R.id.img_btn3).setSelected(b3);
    }


    private void setOnClickEvent() {

        findViewById(R.id.ll_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(true,  false);
            }
        });

        findViewById(R.id.img_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(true,  false);
            }
        });
        /*findViewById(R.id.img_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(false,  false);
            }
        });*/



        findViewById(R.id.ll_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(false,  true);
            }
        });
        findViewById(R.id.img_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(false,  true);
            }
        });



        ((ToggleButton)findViewById(R.id.togglebtn)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ((TextView)findViewById(R.id.tv_join_info)).setText(R.string.join_recive_info2);
                }else{
                    ((TextView)findViewById(R.id.tv_join_info)).setText(R.string.join_recive_info1);
                }
            }

        });
    }


    public void setCancleListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_cancle).setOnClickListener(mListener);
    }

    public void setConfirmistener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_confirm).setOnClickListener(mListener);
    }


    /**
     * 선택타입 리턴
     * @return
     */
    public int getSelect() {

        if(findViewById(R.id.img_btn1).isSelected())return 0;//0:비공개
        //if(findViewById(R.id.img_btn2).isSelected())return 2;//2:모임명만 공개
        if(findViewById(R.id.img_btn3).isSelected())return 1;//1:공개(기본)
        return 0;
    }

    /**
     * 가입신청받기 토글값 리턴
     * @return
     * true : 리더신청
     * false : 승인없이 바로
     */
    public int getToggle(){
        if(((ToggleButton)findViewById(R.id.togglebtn)).isChecked()){
            return 1; //0:자동가입(기본), 1:승인
        }else{
            return 0;
        }

    }

}
