package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;

/**
 * Created by aveapp on 2017. 8. 30..
 * 공지화면에서
 *
 * 공지내리기
 * 중요공지내리기
 * 중요공지 올리기 다이얼로그
 *
 */

public class Dialog_Noty extends Dialog {

    private View.OnClickListener mListener;
    Context mContext;
    boolean bEmergency;

    public Dialog_Noty(Context context, boolean bEmergency) {
        super(context);
        mContext = context;
        this.bEmergency=bEmergency;

        //중요공지인지
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_noty);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {

        //나의 권한설정 체크.
        if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getNotiTy(mContext))) {


            if(bEmergency){ //중요공지 이면
                findViewById(R.id.tv_option0).setVisibility(View.GONE);//중요공지 올리기
            }else{ //그냥 공지
                findViewById(R.id.tv_option1).setVisibility(View.GONE);//중요공지 내리기
            }
        } else {


        }
    }


    private void setOnClickEvent() {



        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnClickListener!=null){
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnClickListener!=null){
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnClickListener!=null){
                    mOnClickListener.onClick(v);
                }
            }
        });



    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


    /**
     * 클립보드
     * @param listener
     */
    public void setClipboardListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_option0).setOnClickListener(mListener);
    }




}
