package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Util;


/**
 * Created by yunsu on 2017-03-27.
 * 강제탈퇴 다이얼로그
 * 아니요 예 다이얼로그
 *
 * -> 강제 탈퇴, 강제탈퇴 후 차단 , 취소 3선택 으로 수정
 */

public class Dialog_Withdraw extends Dialog {
    private View.OnClickListener mListener;
    private int mItemPosition;
    private TextView tvYes;
    private Context mContext;
private String content;

    //private View.OnClickListener mOnClickwithdrawListener;
    //private View.OnClickListener mOnClickblockListener;

    private View.OnClickListener mOnClickConfirmListener;
    private View.OnClickListener mOnClickCancleListener;

    public Dialog_Withdraw(Context context, String content, int position) {
        super(context);

        this.mContext= context;
        this.mItemPosition = position;
        this.content=content;

        //findViewById(R.id.tv_yes).setOnClickListener(listener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_withdraw);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        select0();  //디폴트값


       ((TextView)findViewById(R.id.tv_content)).setText(content);

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickConfirmListener != null) {
                    mOnClickConfirmListener.onClick(v);
                }
            }
        });

        //강제탈퇴
        findViewById(R.id.ll_select0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select0();

            }
        });

        //강제 탈퇴후 차단
        findViewById(R.id.ll_select1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select1();



            }
        });
    }



    /**
     * 차단 기본값
     */
    private void select0() {
        mItemPosition=0;

        findViewById(R.id.btn0).setSelected(true);
        findViewById(R.id.btn1).setSelected(false);
        ((TextView)findViewById(R.id.tv_withdraw)).setTextColor(Util.getColor(mContext,R.color.confirm_green));
        ((TextView)findViewById(R.id.tv_block)).setTextColor(Util.getColor(mContext, R.color.font_gray46));
    }

    /**
     * 강제탈퇴후 차단
     */
    private void select1() {
        mItemPosition=1;

        findViewById(R.id.btn0).setSelected(false);
        findViewById(R.id.btn1).setSelected(true);
        ((TextView)findViewById(R.id.tv_withdraw)).setTextColor(Util.getColor(mContext,R.color.font_gray46));
        ((TextView)findViewById(R.id.tv_block)).setTextColor(Util.getColor(mContext,R.color.confirm_green));
    }


    public void setData(String content) {
        //((TextView) findViewById(R.id.tv_content)).setText(content);
    }


    public int getmItemPosition() {
        return mItemPosition;
    }

    public void setConfirmListener(View.OnClickListener listener) {
        mOnClickConfirmListener = listener;
    }

    /*public void setWithdrawListener(View.OnClickListener listener) {
        mOnClickwithdrawListener = listener;
    }
    public void setblockListener(View.OnClickListener listener) {
        mOnClickblockListener = listener;
    }*/
}
