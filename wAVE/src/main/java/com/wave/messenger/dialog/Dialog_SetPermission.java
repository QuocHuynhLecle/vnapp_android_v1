package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.messenger.activity.Activity_SetPermission;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;


/**
 * Created by aveapp on 2017-03-23.
 *
 * 퍼미션 다이얼로그
 *
 *
 * 설정값 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
 *
 * 디자인순서와 다름.
 */

public class Dialog_SetPermission extends Dialog {
    //private TextView tv_title, tv_sub;
    //private Button bt_ok;
    //private View.OnClickListener mListener;
    private Constants.SET_PERMISSION_DIALOG_TYPE type;
    private String mTitle;
    private Activity_SetPermission.IonEventListener ionEventListener;
    private int mnDialogNum; //권한설정 다이얼로그의 고유번호 0~8
    //private int mnSelectedNum; //선택한 아이템번호
    String initValue; // 설정값

    public Dialog_SetPermission(Context context, int nDialogNum, Constants.SET_PERMISSION_DIALOG_TYPE type, String title, String initValue ,Activity_SetPermission.IonEventListener ionEventListener) {
        super(context);
        this.type=type;
        this.mTitle=title;
        this.ionEventListener=ionEventListener;
        this.mnDialogNum=nDialogNum;
        this.initValue=initValue;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_permission);
        setCanceledOnTouchOutside(true);
        setCancelable(true);

        initView();
        setOnclickEvent();
    }


    private void initView() {
        if(type==Constants.SET_PERMISSION_DIALOG_TYPE.leader){
            findViewById(R.id.ll_select1).setVisibility(View.GONE); //첫번째 일반 선택 숨김
        }




        ((TextView)findViewById(R.id.tv_title)).setText(mTitle);


        setSelectImage(initValue);    //설정된 이미지 표시
    }


    private void setOnclickEvent() {
        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {    //취소
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ionEventListener.onEvent(mnDialogNum, Integer.parseInt(initValue));
                dismiss();
            }
        });




        findViewById(R.id.ll_select1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모든 멤버
                initValue="0";
                setSelectImage(initValue);
            }
        });
        findViewById(R.id.imgb_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //모든 멤버
                initValue="0";
                setSelectImage(initValue);
            }
        });



        findViewById(R.id.ll_select2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //리더와 공동리더
                initValue="2";
                setSelectImage(initValue);
            }
        });
        findViewById(R.id.imgb_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //리더와 공동리더
                initValue="2";
                setSelectImage(initValue);
            }
        });

        findViewById(R.id.ll_select3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //리더
                initValue="1";
                setSelectImage(initValue);
            }
        });
        findViewById(R.id.imgb_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //리더
                initValue="1";
                setSelectImage(initValue);
            }
        });
    }

    /**
     * 설정된 값으로 버튼색상 변화,
     * 선택한 값으로 버튼 색상 변화
     * @param select
     *
     * 서버 저장값 0:모든멤버, 1:리더, 2:리더와 공동리더(기본)
     * 다이얼로그디자인: 위부터  모든멤버, 리더와 공동리더, 리더
     *
     */
    private void setSelectImage(String select) {

        switch (select){
            case "0":     //모든멤버
                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_on);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_off);

                ((TextView)findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));
                ((TextView)findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView)findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                break;

            case "1":     //리더
                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_on);

                ((TextView)findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView)findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView)findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));
                break;

            case "2":     //리더와 공동리더
                findViewById(R.id.imgb_btn1).setBackgroundResource(R.drawable.ic_radio_box_off);
                findViewById(R.id.imgb_btn2).setBackgroundResource(R.drawable.ic_radio_box_on);
                findViewById(R.id.imgb_btn3).setBackgroundResource(R.drawable.ic_radio_box_off);

                ((TextView)findViewById(R.id.tv_select1)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                ((TextView)findViewById(R.id.tv_select2)).setTextColor(Util.getColor(getContext(),R.color.confirm_green));
                ((TextView)findViewById(R.id.tv_select3)).setTextColor(Util.getColor(getContext(),R.color.font_gray46));
                break;

        }

    }


}
