package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.wave.messenger.adapter.GrouptAdapterSpinner1;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoGroupt;
import com.wave.messenger.vo.VoGrouptItem;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;

/**
 * Created by aveapp on 2017-07-18.
 * 모임생성
 * 전문가 그룹 선택 다이얼로그
 * 스피너
 */

public class Dialog_professionalAccept2 extends Dialog {

    private int mnSelectedNum=0; //선택한 아이템번호

    String TAG=this.getClass().getSimpleName();

    //UI
    Spinner spinner1;
    //Adapter
    GrouptAdapterSpinner1 adapterSpinner1;

    public Dialog_professionalAccept2(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_professional_accept2);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        initView();

        setOnclickEvent();

       requestGrouptList();
    }


    private void initView() {

    }

    private void setOnclickEvent() {

        /*findViewById(R.id.ll_choice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });*/

        findViewById(R.id.tv_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Util.hideKeyboard(getContext(), (EditText) findViewById(R.id.edt_reason));

                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);

                }
            }
        });
    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


    /**
     * 선택한 전문가리스트의 아이디를 가져온다.
     * @return
     */
    public String getSelect(){
        Log.e(TAG," getSelect "+spinner1.getSelectedItem().toString()+" id : "+adapterSpinner1.geGrouptItemid(spinner1.getSelectedItemPosition()));
        //return mnSelectedNum+"";
        return adapterSpinner1.geGrouptItemid(spinner1.getSelectedItemPosition());

    }



    /**
     * 7.36 모임 전문가 그룹 리스트
     *
     */
    private void requestGrouptList() {
        Moa.moimGrouptList(getContext(), mMainActivityHandler);
    }


    /**
     * 모임 글조회 결과처리
     */
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case 0x00080039 :   //7.36 모임 전문가 그룹 리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {

                            VoGroupt voGroupt;
                            Gson gson = new Gson();
                            voGroupt = gson.fromJson( data.body.toString() , VoGroupt.class);
                            setSpinner(voGroupt);

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 드롭다운 스피너 세팅
     * @param voGroupt
     */
    private void setSpinner(VoGroupt voGroupt) {
        //ArrayList<VoGroupt> data = new ArrayList<>();
        //data.add("가짜 데이터 1"); data.add("가짜 데이터 2"); data.add("가짜 데이터 3"); data.add("가짜 데이터 4"); data.add("가짜 데이터 5");
        //data.add("가짜 데이터 6"); data.add("가짜 데이터 7"); data.add("가짜 데이터 8"); data.add("가짜 데이터 9"); data.add("가짜 데이터 10");

        //UI생성
        spinner1 = (Spinner)findViewById(R.id.spinner1);


        ArrayList<VoGrouptItem> data = new ArrayList<>();



        data.addAll(voGroupt.getParams());
        data.add(new VoGrouptItem(Constants.PROFESS_NORMAL,"일반"));
        //Adapter
        adapterSpinner1 = new GrouptAdapterSpinner1(getContext(), data);

        //Adapter 적용
        spinner1.setAdapter(adapterSpinner1);
        spinner1.setSelection(adapterSpinner1.getCount()-1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG,"onItemSelected "+i);
                //계속 활성화
                /*if(i==adapterSpinner1.getCount()-1){
                    Log.e(TAG,"normal");

                    findViewById(R.id.edt_reason).setEnabled(true);
                    findViewById(R.id.edt_reason).setVisibility(View.VISIBLE);
                }else{
                    findViewById(R.id.edt_reason).setEnabled(false);
                    findViewById(R.id.edt_reason).setVisibility(View.INVISIBLE);
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    /**
     * 신청사유 가져오기
     */
    public String getEdtex() {

       if(TextUtils.isEmpty(( (EditText)findViewById(R.id.edt_reason)).getText().toString().trim())){
           return null;
       }else{
           Log.e(TAG,"getEdtex: "+((EditText)findViewById(R.id.edt_reason)).getText().toString().trim());
           return ( (EditText)findViewById(R.id.edt_reason)).getText().toString().trim();
       }
    }

}
