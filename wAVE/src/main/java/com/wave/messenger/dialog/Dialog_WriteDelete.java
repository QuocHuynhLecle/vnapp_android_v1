package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;


/**
 * Created by leeyunsu on 2017-06-07.
 */

public class Dialog_WriteDelete extends Dialog {

    private View.OnClickListener mListener;

    String mstrRegUsr;
    Context mContext;
    //VoBoardList.VoBoardItem   voBoardItem;

    public Dialog_WriteDelete(Context context) {
        super(context);
        //mstrRegUsr = regUsr;
        mContext = context;
        //voBoardItem = item;

    }

//    arvalues.get(position).getRegUsr()

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_write_delete);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {
        ((TextView)findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.delete));
    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }



}