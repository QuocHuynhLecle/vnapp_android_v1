package com.wave.messenger.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;

import com.wave.massenger.piggy.R;
import com.wave.messenger.activity.Activity_MoimCoverSet;
import com.wave.messenger.activity.Activity_Profile;
import com.wave.messenger.activity.Activity_SetMoimProfile;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.FileUtil;

import java.io.File;


public class Dialog_Camera {


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


    public static Dialog getProfileEditCameraDialog(final Activity context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"카메라", "갤러리", "기본 이미지로 변경"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("프로필 사진");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: //카메라

                        ;
                        //if (HMMessengerShare.getInstance().getCameraPermission()) {
                        if (Util.CheckPermission(context)) {
                            setCamera(context);

                        } else {
                            //onRequestPermissionsResult 에서 처리
                            //Util.showCameraPermissionDialog(context);
                        }
                        break;

                    case 1: //갤러리
                        //if (HMMessengerShare.getInstance().getCameraPermission()) {
                        if (Util.CheckPermission(context)) {
                            setGalleryCamera(context);

                        } else {
                            //Util.showCameraPermissionDialog(context);
                        }
                        break;

                    case 2: //기본이미지
                        if (context instanceof Activity_SetMoimProfile) {
                            ((Activity_SetMoimProfile) context).setDefaultImage();
                        } else if (context instanceof Activity_MoimCoverSet) {
                            ((Activity_MoimCoverSet) context).setDefaultImage();
                        }


                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.

    public static Dialog getProfileEditCameraDialog3(final Activity context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"카메라", "갤러리", "기본 이미지로 변경"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("프로필 사진");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = FileUtil.createImageFile();
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

                        break;

                    case 1:
//                            Intent intent = new Intent(context, Activity_Gallery.class);
//                            intent.putExtra("maxCount", 1);
//                            context.startActivityForResult(intent, Constants.GALLERY_REQUEST);
                        setGalleryCamera(context);

                        break;

                    case 2:
                        ((Activity_Profile) context).setDefaultImage("profile");
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.

    public static Dialog getbgProfileEditCameraDialog(final Activity context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"카메라", "갤러리", "기본 이미지로 변경"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("배경 사진");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = FileUtil.createImageFile();
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

                        break;

                    case 1:
                        setGalleryCamera(context);

                        break;

                    case 2:
                        ((Activity_Profile) context).setDefaultImage("bg");
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.

    public static Dialog getMoimWriteCameraDialog(final Activity context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"카메라", "갤러리", "취소"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: //카메라

                        ;
                        //if (HMMessengerShare.getInstance().getCameraPermission()) {
                        if (Util.CheckPermission(context)) {
                            setCamera(context);
//                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            File f = FileUtil.createImageFile();
//                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                            context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

                        } else {
                            //onRequestPermissionsResult 에서 처리
                            //Util.showCameraPermissionDialog(context);
                        }
                        break;

                    case 1: //갤러리
                        //if (HMMessengerShare.getInstance().getCameraPermission()) {
                        if (Util.CheckPermission(context)) {
                            setGalleryCamera(context);

                        } else {
                            //Util.showCameraPermissionDialog(context);
                        }
                        break;

                    case 2: //기본이미지
                        dialog.dismiss();


                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.

    /**
     * 카메라 옵션 팝업 테스트
     *
     * @param mContext
     */
    public static void getProfileEditCameraDialog2(final Activity mContext) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(mContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);


        final Dialog_ProfileCameraOption dialog_ProfileCameraOption = new Dialog_ProfileCameraOption(mContext);
        dialog_ProfileCameraOption.show();

        dialog_ProfileCameraOption.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_ProfileCameraOption.dismiss();

                int i = v.getId();

                if (i == R.id.tv_option0) { //카메라

                    if (Util.CheckPermission(mContext)) {
                        setCamera(mContext);
                    } else {
                        Util.showCameraPermissionDialog(mContext);

                    }
                    /*Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = FileUtil.createImageFile();
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    mContext.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);*/


                } else if (i == R.id.tv_option1) {   //겔러리
                    if (Util.CheckPermission(mContext)) {
                        setGalleryCamera(mContext);

                    } else {
                        Util.showCameraPermissionDialog(mContext);
                    }


                } else if (i == R.id.tv_option2) {
                    //Toast.makeText(mContext, "공유하기", Toast.LENGTH_SHORT).show();
                    if (mContext instanceof Activity_SetMoimProfile) {
                        ((Activity_SetMoimProfile) mContext).setDefaultImage();
                    } else if (mContext instanceof Activity_MoimCoverSet) {
                        ((Activity_MoimCoverSet) mContext).setDefaultImage();
                    }


                }
            }
        });
    }


    /**
     * 모임 프로필 설정 수정2
     * 결과값이 널인경우 처리.
     *
     * @param mContext
     * @param fileUri
     */
    public static void getProfileEditCameraDialog2_2(final Activity mContext, final Uri fileUri) {

        final Dialog_ProfileCameraOption dialog_ProfileCameraOption = new Dialog_ProfileCameraOption(mContext);
        dialog_ProfileCameraOption.show();

        dialog_ProfileCameraOption.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog_ProfileCameraOption.dismiss();

                int i = v.getId();

                if (i == R.id.tv_option0) { //카메라

                    if (Util.CheckPermission(mContext)) {
                        setCamera2(mContext, fileUri);

                    } else {
                        Util.showCameraPermissionDialog(mContext);
                    }
                } else if (i == R.id.tv_option1) {   //겔러리
                    if (Util.CheckPermission(mContext)) {
                        setGalleryCamera(mContext);

                    } else {
                        Util.showCameraPermissionDialog(mContext);
                    }


                } else if (i == R.id.tv_option2) {
                    if (mContext instanceof Activity_SetMoimProfile) {
                        ((Activity_SetMoimProfile) mContext).setDefaultImage();
                    } else if (mContext instanceof Activity_MoimCoverSet) {
                        ((Activity_MoimCoverSet) mContext).setDefaultImage();
                    }


                }
            }
        });
    }

    /**
     * 사진촬영 수정.
     *
     * @param context
     */
    public static void setCamera2(Activity context, Uri fileUri) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = FileUtil.createImageFile();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
    }

    //

    /**
     * 사진촬영
     *
     * @param context
     */
    public static void setCamera(Activity context) {
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = FileUtil.createImageFile();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri photoUri = FileProvider.getUriForFile(context,
                    context.getPackageName() + ".provider", f);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        } else {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        }
        context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
    }


    public static void setGalleryCamera(Activity context) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        context.startActivityForResult(i, Constants.GALLERY_REQUEST);
    }





/*
    public static Dialog getChatSelectorPopupDialog(final Activity context) {

        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Light_NoTitleBar);
        }

        final CharSequence[] items = {"카메라", "갤러리"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("사진 전송");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //
                if(true){
                    switch (item) {
                        case 0:
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                            break;
                        case 1:
                            Intent intent = new Intent(context, Activity_Gallery.class);
                            intent.putExtra("maxCount", 1);
                            context.startActivityForResult(intent, Constants.GALLERY_REQUEST);
                            break;
                    }
                } else {
                    showCameraPermissionDialog(context);
                }
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        return alert;

    }*/


    /**
     * 카메라 퍼미션 허용 팝업
     *
     * @param activity
     */
    public static void showCameraPermissionDialog(Activity activity) {
        if (activity != null) {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is not null");
            Log.e(activity.getLocalClassName(), "showCameraPermissionDialog");
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.camera_permission_title));
            builder.setMessage(activity.getString(R.string.camera_permission_message));
            builder.setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog ab = builder.create();

            if (ab != null) {
                ab.show();
            }
        } else {
            //ErrorController.showMessage("[showCameraPermissionDialog] context is null");
        }
    }


}
