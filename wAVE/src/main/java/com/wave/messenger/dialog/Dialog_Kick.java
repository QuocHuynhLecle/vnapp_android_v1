package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 2. 23..
 */

public class Dialog_Kick extends Dialog implements View.OnClickListener {

    private Context mContext;

    private TextView tv_title, tv_sub;
    private Button bt_ok;

    public Dialog_Kick(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_kick);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_sub = (TextView) findViewById(R.id.tv_sub);
        bt_ok = (Button) findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(this);
    }

    public void setData(String title) {
        tv_title.setText(mContext.getString(R.string.kick_title, title));
    }

    public void setData(String title, String sub) {
        tv_title.setText(title);
        tv_sub.setText(sub);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
