package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMoimProfile;
import com.wave.messenger.vo.VoMyprofessional;


/**
 * 모임 가입질문 & 비밀번호 다이얼로그
 * Created by leeyunsu on 2017-07-17.
 */

public class Dialog_MoimJoinType extends Dialog {

    String TAG = this.getClass().getName();

    private View.OnClickListener mListener;

    //String refortType;
    Context mContext;
    String strTitle;
    String mEntTy;
    String mNullToast; //널일때 토스트 메시지
    /**
     * 리더가 모임 만들때 질문,비번입력
     * @param context
     * @param mEntTy  0:질문입력,1:비번입력
     */
    public Dialog_MoimJoinType(Context context, String mEntTy) {
        super(context);
        mContext = context;
        this.mEntTy = mEntTy;

        if (mEntTy.equals("0")) {
            strTitle = mContext.getString(R.string.dialog_cover_txt1);
            mNullToast= mContext.getString(R.string.dialog_cover_txt1);
        } else {
            strTitle = mContext.getString(R.string.dialog_cover_txt2);
        }
    }

    /**
     *
     * 모임타임라인 가입시 회원이 답변 입력, 비번입력
     * @param context
     * @param param
     */
    public Dialog_MoimJoinType(Context context, VoMoimProfile.VoMoimProfileNoticeParam param) {
        super(context);

        mContext = context;
        this.mEntTy =param.getEntTy();

        if (mEntTy.equals("0")) {   //질문
            strTitle =param.getQstn();

            if(TextUtils.isEmpty(strTitle)){//입력시 값이 들어가지만 디비에 값이 없을경우 임의 텍스트.
                strTitle="모임 가입질문입니다.";
            }

            mNullToast= mContext.getString(R.string.moim_join_answer);  //답변을 입력하세요.
        }else if(mEntTy.equals("2")){   // 비번
            strTitle = mContext.getResources().getString(R.string.dialog_cover_txt2);
        }
    }


    /**
     * 친구리스트 어댑터에서 사용
     * @param context
     * @param param
     */
    public Dialog_MoimJoinType(Context context,VoFriendList param) {
        super(context);

        mContext = context;
        this.mEntTy = String.valueOf(param.getEntTy());

        if (mEntTy.equals("0")) {   //질문
            strTitle =param.getQstn();
        }else if(mEntTy.equals("2")){   // 비번
            strTitle = mContext.getResources().getString(R.string.dialog_cover_txt2);
        }
    }

    /**
     * 모임만들때사용
     * @param context
     * @param param
     */
    public Dialog_MoimJoinType(Context context,VoMyprofessional param) {
        super(context);

        mContext = context;
        this.mEntTy = String.valueOf(param.getEntTy());

        if (mEntTy.equals("0")) {   //질문
            strTitle =param.getQstn();
        }else if(mEntTy.equals("2")){   // 비번
            strTitle = mContext.getResources().getString(R.string.dialog_cover_txt2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_moim_join_type);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {

        Log.e("Dialog_MakeMoim", "strTitle: " + strTitle);
        ((TextView) findViewById(R.id.tv_title)).setText(strTitle);


        if (mEntTy.equals("0")) { //질문

        }else{  //비번
            //((EditText)findViewById(R.id.edt_input)).setInputType( InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD );

            ((EditText)findViewById(R.id.edt_input)).setInputType( InputType.TYPE_TEXT_VARIATION_PASSWORD );
            ((EditText)findViewById(R.id.edt_input)).setTransformationMethod(PasswordTransformationMethod.getInstance());
        }





    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {


                    if(mEntTy.equals("0")){ // 질문입력
                        if (TextUtils.isEmpty(((EditText) findViewById(R.id.edt_input)).getText().toString().trim()
                        )) {

                            //TODO 개설시, 가입시 구분하기
                             Toast.makeText(mContext, mNullToast, Toast.LENGTH_SHORT).show();
                            return;
                        } /*else {
                            mOnClickListener.onClick(v);
                        }*/
                    }else{ //비번 자리수 체크
                        if (((EditText)findViewById(R.id.edt_input)).getText().toString().trim().length() < 4
                                ||
                                ((EditText)findViewById(R.id.edt_input)).getText().toString().trim().length() > 6
                                ) {

                            Toast.makeText(mContext, "비밀번호는 4자리이상 6자리이하 입니다.", Toast.LENGTH_SHORT).show();
                            return;
                        }


                    }
                    mOnClickListener.onClick(v);



                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public void setCancleListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_cancle).setOnClickListener(mListener);
    }

    public void setConfirmistener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.tv_confirm).setOnClickListener(mListener);
    }

    public String getTitle() {

        return ((TextView) findViewById(R.id.tv_title)).getText().toString().trim();

    }


    public String getJoinTxt() {

            return ((EditText) findViewById(R.id.edt_input)).getText().toString().trim();

    }


}