package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;


/**
 *
 * 타임라인 신고하기 다이얼로그
 * 타임라인 이모임글 보지않기
 * Created by leeyunsu on 2017-06-07.
 */

public class Dialog_Report extends Dialog {

    private View.OnClickListener mListener;

    //String refortType;
    Context mContext;
    String strRefort;
    //VoBoardList.VoBoardItem   voBoardItem;

    public Dialog_Report(Context context,String report) {
        super(context);
        //this.refortType = type;
        mContext = context;
        strRefort= report;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_report);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {
        //String strRrefort = null;

        //strRrefort = mContext.getResources().getString(R.string.report_info1);
        /*switch (refortType){
            case "1":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "2":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "3":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
            case "4":
                strRrefort = mContext.getResources().getString(R.string.report_info1);
                break;
        }*/

        Log.e("Dialog_Report","strRrefort: "+strRefort);
        ((TextView)findViewById(R.id.tv_report_info)).setText(strRefort);
    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;
    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }



}