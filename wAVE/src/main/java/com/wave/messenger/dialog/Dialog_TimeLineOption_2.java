package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;


/**
 * Created by leeyunsu on 2017-05-01.
 * 본문복사 공유하기 북마크 신고하기 이모임글 보지않기 팝업
 */

public class Dialog_TimeLineOption_2 extends Dialog {

    Context mContext;
    Constants.SET_ENTRY_TYPE setEntryType;


    String mStrNoti;    //공지 여부(int) 0:일반, 1:공지
    String mStrRegUsr;  //글쓴사람 아이디
    String mUsrLv;      //레벨


    public Dialog_TimeLineOption_2(Context context, String noti, String regUsr, String usrLv, Constants.SET_ENTRY_TYPE type) {
        super(context);

        mContext = context;
        mStrNoti = noti;
        mStrRegUsr = regUsr;
        setEntryType = type;
        mUsrLv = usrLv;
        Log.e("Dialog_TimeLineOption_2", "setEntryType :" + setEntryType);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_timeline_option);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();

    }

    private void init() {
        //findViewById(R.id.tv_option0).setVisibility(View.GONE);


        /*if (Util.isOwner(mContext, mStrRegUsr)) { //내가 등록한 글인지 등록자 구분
            findViewById(R.id.tv_option4).setVisibility(View.GONE);

        } else {
            findViewById(R.id.tv_option0).setVisibility(View.GONE);
        }*/

            //공지글등록 체크
            //게시물댓글 삭제권한체크
            //내가 쓴글체크


            //등급 다시 정리
            if (Util.getMymLv(mContext).equals("1")) {    //리더---------------------------------------------------------------

                findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);  //다른멤버의 글 공지등록 가능.
                findViewById(R.id.tv_option7).setVisibility(View.VISIBLE);     //모두 삭제가능

            } else if (Util.getMymLv(mContext).equals("2")) {  //공동리더---------------------------------------------------------

                if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getNotiTy(mContext))) {  //권한에 의한 공지
                    findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);
                }

                if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getDelTy(mContext))) { //권한에 의한 삭제

                    findViewById(R.id.tv_option7).setVisibility(View.VISIBLE);
                    if (mUsrLv.equals("1") || mUsrLv.equals("2")) {             //리더, 공동리더 글은 삭제 못함.
                        findViewById(R.id.tv_option7).setVisibility(View.GONE);
                    }
                }


            } else {    //일반멤버--------------------------------------------------------------------------------------------------
                if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getNotiTy(mContext))) {  //권한에 의한 공지
                    findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);
                }

            }


            //공지 올리기, 내리기 부분.
            if (mStrNoti.equals("0")) {
                //findViewById(R.id.tv_option0).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.timeline_option0));
            } else {
                ((TextView) findViewById(R.id.tv_option0)).setText(mContext.getResources().getString(R.string.timeline_option0_1));
            }


            //공통부분
            if(Util.isOwner(mContext, mStrRegUsr)) {  //내가 쓴글이면...
                findViewById(R.id.tv_option4).setVisibility(View.GONE);    //신고하기
                findViewById(R.id.tv_option6).setVisibility(View.VISIBLE); //수정하기
                findViewById(R.id.tv_option7).setVisibility(View.VISIBLE); //삭제하기
            } else {
                findViewById(R.id.tv_option4).setVisibility(View.VISIBLE); //신고하기
            }


        //모임 타임라인

        if (setEntryType == Constants.SET_ENTRY_TYPE.main_tab) {
            findViewById(R.id.tv_option0).setVisibility(View.GONE);
            findViewById(R.id.tv_option5).setVisibility(View.GONE); //이모임글 보지않기

        }else if (setEntryType == Constants.SET_ENTRY_TYPE.written_list) {   //작성글보기
            findViewById(R.id.tv_option0).setVisibility(View.GONE); //공지
            findViewById(R.id.tv_option5).setVisibility(View.GONE); //이모임글 보지않기
            findViewById(R.id.tv_option6).setVisibility(View.GONE); //글수정
            findViewById(R.id.tv_option7).setVisibility(View.GONE); //삭제하기

        } else if (setEntryType == Constants.SET_ENTRY_TYPE.moim_timelineDetail) {  //타임라인 글상세화면
            //findViewById(R.id.tv_option2).setVisibility(View.GONE); //공유
            findViewById(R.id.tv_option5).setVisibility(View.GONE); //이모임글 보지않기
        }



        findViewById(R.id.tv_option5).setVisibility(View.GONE); //무조건 이모임글 보지않기

    }


    private void setOnClickEvent() {

        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        findViewById(R.id.tv_option3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });
        findViewById(R.id.tv_option4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });
        findViewById(R.id.tv_option5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_option7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });


    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


}