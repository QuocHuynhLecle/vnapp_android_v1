package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by SJH on 2016-09-02. <br/>
 * 버튼 두개(확인 / 취소) 짜리 커스텀 다이얼로그.
 */
public class Dialog_TermsHtml extends Dialog {

    private TextView tvMessage;
    private Button ivCancel, ivOk;
    private WebView mWebView;
    private CheckBox check;
    Context mContext;

    public Dialog_TermsHtml(Context context) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        setContentView(R.layout.dialog_term_html);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        // getWindow().setDimAmount(0.7f);
        //setTitle("SSAM 메신저서비스 이용약관");
        initView();
        mContext=context;
        String htmlTxt=getAsset(context);
       // String htmlTxt=getRaw(context);
        setWebview(htmlTxt);
        }

    /**
     *agreement.htm 내용을 가져온다.
     * 20180315 leeyunsu
     * @param context
     * @return
     */
    private String getAsset(Context context) {
        try {
            AssetManager assetMgr = context.getAssets();
            InputStream input = assetMgr.open("html/agreement.htm");

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String bufferText = new String(buffer);
            Log.e("Dialog_TermsHtml", "getAsset: " + bufferText);
            return bufferText.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * webview에 약관내용 넣기
     * @param htmlTxt
     */
    private void setWebview(String htmlTxt) {
        mWebView = (WebView)findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setInitialScale(40);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.loadData(htmlTxt, "text/html", "UTF-8");

        Resources res = mContext.getResources();
       float  fontSize = res.getDimension(R.dimen.webview_text_size);
        mWebView.getSettings().setDefaultFontSize((int)fontSize);


    }

    /**
     * raw에서 agreement.html 내용을 string 로 가져온다
     * 20180315 leeyunsu
     * @deprecated
     */
    private String getRaw(Context context) {
        InputStream in = context.getResources().openRawResource(R.raw.agreement);
        if (in != null) {
            try {
                InputStreamReader stream = null;
                stream = new InputStreamReader(in, "utf-8");

                BufferedReader buffer = new BufferedReader(stream);

                String read;
                StringBuilder sb = new StringBuilder("");

                while ((read = buffer.readLine()) != null) {
                    sb.append(read);
                }
                in.close();
                return  sb.toString();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
       return  null;
    }


    private void initView() {
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        ivCancel = (Button) findViewById(R.id.btCancel);
        ivOk = (Button) findViewById(R.id.btOk);
        check =(CheckBox)findViewById(R.id.chk_agree);

        ivOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check.isChecked()){
//                //TODO 재로그인
//                    com.hanawm.messenger.util.Util.setTermsOkTrue(getContext());
//                    Fragment_Main.getInstance().setStart();
                    dismiss();
                }else{
                    //동의체크 팝업
                    Dialog_Login_check dalog_Login_check = new Dialog_Login_check(getContext());
                    dalog_Login_check.show();
                }
            }
        });
    }

    public void setCancelEvent(View.OnClickListener listener) {
        this.ivCancel.setOnClickListener(listener);
    }

    boolean bflug=false;
    public void setOkEvent(View.OnClickListener listener) {
        this.ivOk.setOnClickListener(listener);

        if(check.isChecked()){
            this.ivOk.setOnClickListener(listener);
        }else{
            if(bflug){
                Log.e("Dialog_TermsHtml","Dialog_Login_check Not checked!!");
                //동의체크 팝업
                Dialog_Login_check dalog_Login_check = new Dialog_Login_check(getContext());
                dalog_Login_check.show();
            }else{
                bflug=true;
            }




        }

    }

    public void setTextMessage(String text) {
        this.tvMessage.setText(text);
    }

}
