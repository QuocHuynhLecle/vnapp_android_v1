package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 2. 23..
 */

public class Dialog_Notice extends Dialog {

    TextView tv_title, tv_sub;
    Button bt_ok, bt_cancel;

    public Dialog_Notice(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_notice);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_sub = (TextView) findViewById(R.id.tv_sub);
        bt_ok = (Button) findViewById(R.id.bt_ok);
        bt_cancel = (Button) findViewById(R.id.bt_cancel);
    }

    public void setData(String title, String sub) {
        tv_title.setText(title);
        tv_sub.setText(sub);
    }

    public void setListener(int resId, View.OnClickListener listener) {
        if(resId == R.id.bt_ok)
            bt_ok.setOnClickListener(listener);
        else if(resId == R.id.bt_cancel)
            bt_cancel.setOnClickListener(listener);
    }
}
