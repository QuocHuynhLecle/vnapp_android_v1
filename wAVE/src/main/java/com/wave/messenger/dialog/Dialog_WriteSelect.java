package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;


/**
 * Created by leeyunsu on 2017-03-13.
 * 모임 타입설정
 */

public class Dialog_WriteSelect extends Dialog {

    String TAG = this.getClass().getSimpleName();

    public Dialog_WriteSelect(Context context) {
        super(context);

    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_write_select);
        setCanceledOnTouchOutside(false);

        setCancelable(true);

        initView();

        setOnClickEvent();
    }

    private void initView() {
        selectCheckBtn(true, false);
    }

    private void selectCheckBtn(boolean b1, boolean b2) {
        findViewById(R.id.img_btn1).setSelected(b1);
        findViewById(R.id.img_btn2).setSelected(b2);
    }


    private void setOnClickEvent() {

        findViewById(R.id.img_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(true, false);
            }
        });

        findViewById(R.id.ll_btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(true, false);
            }
        });

        findViewById(R.id.img_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(false, true);
            }
        });

        findViewById(R.id.ll_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCheckBtn(false, true);
            }
        });


        findViewById(R.id.tv_cancle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }

            }
        });

        findViewById(R.id.tv_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }


    /**
     * 선택타입 리턴
     *
     * @return
     */
    public int getSelect() {

        if (findViewById(R.id.img_btn1).isSelected()) return 0;//
        if (findViewById(R.id.img_btn2).isSelected()) return 1;//
        return 0;
    }

}
