package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.utility.LogTrace;


/**
 * Created by aveapp on 2017-03-23.
 */

public class Dialog_Stock extends Dialog {
    LinearLayout ll_current;
    TextView tv_title,tv_interest, tv_currentValue, tv_compareValue, tv_compareRate, tv_amount;
    ImageView btn_close;
    String itemCode, compareMark;
    int textColor;
    String mark = "";
    String cMark = "";
    Context context;

    public Dialog_Stock(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_stock);

        init();
        setEvent();
    }

    public void init() {
        ll_current = (LinearLayout) findViewById(R.id.ll_current);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_interest = (TextView) findViewById(R.id.tv_interest);
        tv_currentValue = (TextView) findViewById(R.id.tv_currentValue);
        tv_compareValue = (TextView) findViewById(R.id.tv_compareValue);
        tv_compareRate = (TextView) findViewById(R.id.tv_compareRate);
        tv_amount = (TextView) findViewById(R.id.tv_amount);
        btn_close = (ImageView) findViewById(R.id.btn_close);
    }

    public void setEvent() {
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        //현재가 이동
        ll_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Fragment_Main.getInstance().fragmentSlideMenu_main.setDisplay(context, "1000", itemCode);

        if(context instanceof Activity_ChatRoom){
            Activity_ChatRoom.getInstance().openMTS("1");
        }else if(context instanceof Activity_TimeLineDetail){
            Activity_TimeLineDetail.getInstance().openMTS("1");
        }


            }
        });

        //관심등록
        tv_interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_Common.getStockAdd(context, itemCode).show();
            }
        });
    }

    public void setData(String itemCode, String itemName, String currentValue, String compareValue, String compareRate, String compareMark, String amount) {
        LogTrace.E("종목코드 : " + itemCode);
        LogTrace.E("종목명 : " + itemName);
        LogTrace.E("현재가 : " + currentValue);
        LogTrace.E("전일대비 : " + compareValue);
        LogTrace.E("전일대비부호 : " + compareMark);
        LogTrace.E("전일대비율 : " + compareRate);
        LogTrace.E("누적거래량 : " + amount);


        this.itemCode = itemCode;
        this.compareMark = compareMark;
        String compareRate_substring = compareRate.substring(13,18);
        String compareRateLast;
        setColor(compareRate);

        if (compareRate_substring.charAt(0) == '0') {
            compareRateLast = compareRate.substring(14,18);
        } else
            compareRateLast = compareRate.substring(13,18);
        tv_title.setText(itemName);
        tv_currentValue.setText(setFormat(currentValue));

        tv_compareValue.setText(cMark + setFormat(compareValue).replace("-", ""));
        tv_compareRate.setText("(" + mark + compareRateLast + "%)");
        tv_amount.setText(setFormat(amount));
        tv_currentValue.setTextColor(textColor);
        tv_compareValue.setTextColor(textColor);
        tv_compareRate.setTextColor(textColor);
        tv_amount.setTextColor(getContext().getResources().getColor(R.color.gray_44));
    }

    public String setFormat(String data) {
        int dataInt = Integer.parseInt(data);
        String setData = String.format("%,d", dataInt);
        return setData;
    }

    public void setColor(String data) {
        int setColor = getContext().getResources().getColor(R.color.gray_44);
        String dataSet = data.substring(14,18);
        if (data.charAt(0) == '-') {
            setColor = getContext().getResources().getColor(R.color.blue);
            mark = "-";
            cMark = "▼ ";
        } else if (data.charAt(0) == '0') {
            setColor = getContext().getResources().getColor(R.color.red);
            cMark = "▲ ";
            if (dataSet.equals("0.00")) {
                setColor = getContext().getResources().getColor(R.color.gray_44);
                cMark = "";
            }
        }
        textColor = setColor;
    }
}
