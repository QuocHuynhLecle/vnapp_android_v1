package com.wave.messenger.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_Delivery;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import moa.android.api.MOAClient;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;


public class Dialog_Common {


    /**
     * 멤버 모임정보 멤버관리화면 정렬 다이얼로그
     * @param context
     * @return
     */
    /*public static Dialog getMemberListSortDialog(final Context context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        final CharSequence[] items = {context.getString(R.string.by_name) , context.getString(R.string.by_join) };
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        ((Activity_MoimMemberInfoList) context).setAlignByName();
                        break;

                    case 1:
                        ((Activity_MoimMemberInfoList) context).setAlignByDate();
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }*/

    public static Dialog getChatOptionSingleDialog(final Context context, final VoChatData data) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"삭제"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("작업 선택");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //TODO : 삭제
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }

    public static Dialog getDeliveryDialog(final Context context, final List<VoFriendList> list, final VoChatData data) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"1:1채팅방(10명 이하)", "그룹 채팅방"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("전송방법 선택");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        onAndonSend(context, data, list);
                        Toast.makeText(context, "전달되었습니다.", Toast.LENGTH_SHORT).show();
                        break;

                    case 1:
                        createRoom(context, list, data);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }

    private static void createRoom(Context context, List<VoFriendList> list, VoChatData data) {
        UUID uuid = UUID.randomUUID();
        VoChatData messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(context));
        messagedata.setRoomType("1");
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setMessageType("0");
        messagedata.setOwnerId(MessengerInfo.getUserId(context));

        String message = data.getMessage();

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        messagedata.setMessage(message);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);
        String roomId = inviteFriend(context, list);
        sendMessage(context, data, roomId, list);
    }

    public static String inviteFriend(Context context, List<VoFriendList> newFriends) {
        String toInvite = "";
        String roomId = "";
        List<VoFriendList> checkFriends = new ArrayList<>();
        for (VoFriendList friend : newFriends) {
            if (!TextUtils.isEmpty(friend.getUserId())) {
                if (!friend.getUserId().equals(MessengerInfo.getUserId(context))) {
                    if (checkFriends.size() > 0) {
                        for (int i = 0; i < checkFriends.size(); i++) {
                            VoFriendList item = checkFriends.get(i);
                            if (!friend.getUserId().equals(item.getUserId()))
                                checkFriends.add(friend);
                            toInvite += friend.getUserId() + ",";
                        }
                    } else {
                        checkFriends.add(friend);
                        toInvite += friend.getUserId() + ",";
                    }

                }
            }
        }
        toInvite = toInvite.substring(0, toInvite.length() - 1);
        try {
            UUID uuid = UUID.randomUUID();
            roomId = uuid.toString();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomId);
            value.put("roomType", "1");
            value.put("inviteUser", toInvite);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM,
                    PacketTypes.PTC_IMS_CHATROOM_CREATE, body);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return roomId;

    }

    private static void onAndonSend(Context context, VoChatData data, List<VoFriendList> newFriends) {
        for (VoFriendList friend : newFriends) {
            ChatListDbHelper listDb = LocalDB.getChatListDbHelper(context);
            UsersDbHelper userDb = LocalDB.getUsersDbHelper(context);
            String roomType;
            String roomId;

            if(friend.getUserId().equals(MessengerInfo.getUserId(context))) {
                roomType = "4";
                roomId = listDb.selectMyRoom();
            } else {
                roomType = "0";
                ArrayList<String> roomIdList = listDb.selectSingleRoom();
                roomId = userDb.existUserCaht(roomIdList, friend.getUserId());
            }

            LogTrace.E("delivery friend : " + friend.getUserName());

            if (TextUtils.isEmpty(roomId)) {
                sendMessage(context, data, "", roomType, friend);
            } else {
                sendMessage(context, data, roomId, roomType, friend);
            }
        }
    }

    private static void sendMessage(Context context, VoChatData data, String roomId, String type, VoFriendList item) {
        UUID uuid = UUID.randomUUID();
        VoChatData messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(context));
        messagedata.setUserName(MessengerInfo.getUserName(context));
        messagedata.setRoomType(type);
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setMessageType("0");
        messagedata.setOwnerId(MessengerInfo.getUserId(context));
        String message = data.getMessage();

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        messagedata.setMessage(message);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);

        if ("1".equals(type)) {
            messagedata.setRoomId("");
            messagedata.setInviteUser(item.getUserId());
            messagedata.setRoomCreate("1");

        } else {
            if("".equals(roomId)) {
                messagedata.setRoomCreate("1");
            } else {
                messagedata.setRoomCreate("0");
            }

            messagedata.setRoomId(roomId);
            messagedata.setInviteUser(item.getUserId());

        }

        ChatPresenter chatPresenter = new ChatPresenter(null);
        chatPresenter.sendMessage(context, messagedata);
    }

    private static void sendMessage(Context context, VoChatData data, String roomId, List<VoFriendList> newFriends) {
        UUID uuid = UUID.randomUUID();
        VoChatData messagedata = new VoChatData();
        messagedata.setUserId(MessengerInfo.getUserId(context));
        messagedata.setRoomType("1");
        messagedata.setChatId(uuid.toString());
        messagedata.setChatType("0");
        messagedata.setMessageType("0");
        messagedata.setOwnerId(MessengerInfo.getUserId(context));
        String message = data.getMessage();

        if (message.length() < 50) {
            messagedata.setTitle(message);
        } else {
            messagedata.setTitle(message.substring(0, 50));
        }

        messagedata.setMessage(message);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        messagedata.setReg_date(time);
        messagedata.setRoomCreate("0");
        messagedata.setRoomId(roomId);
        ChatPresenter chatPresenter = new ChatPresenter(null);
        chatPresenter.sendMessage(context, messagedata);
        goChatRoom(context, newFriends, roomId);
    }

    private static void goChatRoom(Context context, List<VoFriendList> newFriends, String roomId) {
        VoChatList chatRoomInfo = new VoChatList();
        chatRoomInfo.setRoom_type("1");
        List<VoFriendList> friends = new ArrayList<>();
        List<VoChatData> chats = new ArrayList<>();

        VoFriendList my = new VoFriendList();
        my.setUserId(MessengerInfo.getUserId(context));
        my.setUserName(MessengerInfo.getUserName(context));
        my.setRealUserName(MessengerInfo.getRealUserName(context));
        friends.add(my);

        for (VoFriendList item : newFriends)
            friends.add(item);

        chatRoomInfo.setFriends(friends, context);
        chatRoomInfo.setChats(chats);
        chatRoomInfo.setRoomId(roomId);

        Statics.ROOMINFO = chatRoomInfo;

        Intent mIntent = new Intent(context, Activity_ChatRoom.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(mIntent);

        Activity_Delivery.getInstance().finish();
    }

    public static Dialog getChatOptionImageDialog(final Context context, final VoChatData data) {
        final ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"삭제","공유"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("작업 선택");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //삭제
                        AlertDialog.Builder innBuilder = new AlertDialog.Builder(themedContext);
                        innBuilder.setMessage("\n선택한 이미지를 삭제하시겠습니까?\n\n삭제한 이미지는 내 채팅방에서만 적용되며 상대방의 채팅방에서는 삭제되지 않습니다\n");
                        innBuilder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        innBuilder.setPositiveButton("삭제",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                                db.delChatData(data.getChatId());
                                data.getListener().deleteMsg(data.getChatId());
                            }
                        });
                        innBuilder.show();

                        break;

                    case 2:
                        //공유
                        try {
                            JSONObject object = new JSONObject(data.getAttachment());
                            String url = Constants.URL_IMAGE_BASE + object.getString("thumb_file");

                            Glide.with(context).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                    super.onLoadFailed(e, errorDrawable);
                                    ErrorController.showMessage("[Dialog_Common] getChatOptionImageDialog - Image onLoading Failed");
                                }

                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    ErrorController.showMessage("[Dialog_Common] getChatOptionImageDialog - Image onLoading Comptete");

                                    Bitmap icon = resource;
                                    Intent share = new Intent(Intent.ACTION_SEND);
                                    share.setType("image/jpeg");

                                    ContentValues values = new ContentValues();
                                    values.put(MediaStore.Images.Media.TITLE, "title");
                                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                                    Uri uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                                    OutputStream outstream;
                                    try {
                                        outstream = context.getContentResolver().openOutputStream(uri);
                                        icon.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
                                        outstream.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    share.putExtra(Intent.EXTRA_TITLE, "candleman");
                                    share.putExtra(Intent.EXTRA_STREAM, uri);
                                    context.startActivity(Intent.createChooser(share, "Share Image"));
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }

    public static void showCameraPermissionDialog(Activity activity) {
        if (activity != null) {
            ErrorController.showMessage("[showCameraPermissionDialog] context is not null");

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.camera_permission_title));
            builder.setMessage(activity.getString(R.string.camera_permission_message));
            builder.setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog ab = builder.create();

            if (ab != null) {
                ab.show();
            }
        } else {
            ErrorController.showMessage("[showCameraPermissionDialog] context is null");
        }
    }

    public static Dialog getChatOptionDialog(final Context context, final VoChatData data) {
        final ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"복사", "삭제"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("작업 선택");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //복사
                        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("Mango BAnana", data.getMessage());
                        clipboard.setPrimaryClip(clip);

                        Toast.makeText(context, "클립보드에 복사 되었습니다.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        break;

                    case 1:
                        //삭제
                        AlertDialog.Builder innBuilder = new AlertDialog.Builder(themedContext);
                        innBuilder.setMessage("\n선택한 메시지를 삭제하시겠습니까?\n\n삭제한 메시지는 내 채팅방에서만 적용되며 상대방의 채팅방에서는 삭제되지 않습니다\n");
                        innBuilder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        innBuilder.setPositiveButton("삭제",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                                db.delChatData(data.getChatId());
                                data.getListener().deleteMsg(data.getChatId());
                            }
                        });
                        innBuilder.show();
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }

    public static Dialog getStockAdd(final Context context, final String itemCode) {

        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        Vector<String> m_arrGroupName 		= new Vector<String>();
        final Vector<String> m_arrGroupKey 		= new Vector<String>();
        final Vector<String> m_arrGroupNameKey 	= new Vector<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.simpletextview);
        for(int i=0; i<m_arrGroupName.size(); i++){
            m_arrGroupNameKey.add(i, m_arrGroupKey.get(i)+"."+m_arrGroupName.get(i));
            LogTrace.E("그룹키:"+m_arrGroupKey.get(i)+"  그룹명:"+m_arrGroupName.get(i));
            adapter.add(m_arrGroupName.get(i));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("관심등록");
        // 버튼 생성
        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();

        return alert;
    }

    /*public static Dialog getInviteNonMemberDialog(final Activity context,
                                                  final List<VoFriendList> friendsToInvite) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invite);

        final DialogCallback callback = (DialogCallback) context;

        TextView tvDescription = (TextView) dialog
                .findViewById(R.id.tvDescription);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        Button btnDecline = (Button) dialog.findViewById(R.id.btnDecline);

        String toInvite = "";

        for (VoFriendList friend : friendsToInvite) {
            if (!friend.isMember()) {
                toInvite += friend.getUserName() + ",";
            }
        }
        toInvite = toInvite.substring(0, toInvite.length() - 1);

        tvDescription.setText(toInvite
                + context.getResources().getString(R.string.invite_etc));

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onConfirm(friendsToInvite);
                dialog.dismiss();
            }
        });

        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onDecline(friendsToInvite);
                dialog.dismiss();

            }
        });

        return dialog;
    }*/

    /*public static Dialog getProfileEditCameraDialog(final Activity context) {
        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"카메라", "갤러리", "기본 이미지로 변경"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("프로필 사진");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        if (HMMessengerShare.getInstance().getCameraPermission()) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            File f = FileUtil.createImageFile();
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                            context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);
                        } else {
                            Dialog_Common.showCameraPermissionDialog(context);
                        }
                        break;

                    case 1:
                        if (HMMessengerShare.getInstance().getCameraPermission()) {
                            Intent intent = new Intent(context, Activity_Gallery.class);
                            intent.putExtra("maxCount", 1);
                            context.startActivityForResult(intent, Constants.GALLERY_REQUEST);
                        } else {
                            Dialog_Common.showCameraPermissionDialog(context);
                        }
                        break;

                    case 2:
                        ((Activity_Profile) context).setDefaultImage();
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.*/

    /*public static Dialog getChatSelectorPopupDialog(final Activity context) {

        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Light_NoTitleBar);
        }

        final CharSequence[] items = {"카메라", "갤러리"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        builder.setTitle("사진 전송");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (HMMessengerShare.getInstance().getCameraPermission()) {
                    switch (item) {
                        case 0:
                            LogTrace.E("카메라 인텐트");
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            File f = FileUtil.createImageFile();
                            if(f != null) {
                                LogTrace.E("파일 생성 완료");
                            }
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                            context.startActivityForResult(cameraIntent, Constants.CAMERA_REQUEST);

                            break;
                        case 1:
                            Intent intent = new Intent(context, Activity_Gallery.class);
                            intent.putExtra("maxCount", 1);
                            context.startActivityForResult(intent, Constants.GALLERY_REQUEST);

                            break;
                    }
                } else {
                    Dialog_Common.showCameraPermissionDialog(context);
                }
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        return alert;
    }*/
}
