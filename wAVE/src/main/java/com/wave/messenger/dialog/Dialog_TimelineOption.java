package com.wave.messenger.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Toast;

import com.wave.messenger.util.InterfaceMymoimListener;


/**
 * 본문복사공유하기북마크신고하기 이모임글 보지않기 팝업
 * 텍스트로된거라 사용X
 * @deprecated
 */
public class Dialog_TimelineOption {


    private View.OnClickListener mListener;
    public static InterfaceMymoimListener interfaceMymoimListener;

    public static Dialog DialogTimelineOption(final Activity context) {    // final InterfaceMymoimListener interfaceListener) {
        //interfaceMymoimListener=interfaceListener;

        ContextThemeWrapper themedContext;
        themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);

        final CharSequence[] items = {"본문복사", "공유하기", "북마크", "신고하기", "이 모임글 보지 않기"};
        AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
        //builder.setTitle("편집");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //interfaceMymoimListener.onSelectItem(item);

                switch (item) {
                    case 0:
                        Toast.makeText(context, "본문복사", Toast.LENGTH_SHORT).show();
                        break;

                    case 1:
                        Toast.makeText(context, "공유하기", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(context, "북마크", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(context, "신고하기", Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        Toast.makeText(context, "이 모임글 보지 않기", Toast.LENGTH_SHORT).show();
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        return alert;
    }// end of getProfileEditCameraDialog.


    public void setCancleListener(View.OnClickListener listener) {
        this.mListener = listener;
        //findViewById(R.id.tv_cancle).setOnClickListener(mListener);
    }


}
