package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 7. 19..
 */

public class Dialog_algin extends Dialog {

    Context mContext;

    public Dialog_algin(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_align);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
        setOnClickEvent();
        init();
    }

    private void init() {
    }


    private void setOnClickEvent() {
        findViewById(R.id.tv_option0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {

                    mOnClickListener.onClick(v);
                    dismiss();
                }
            }
        });

        findViewById(R.id.tv_option1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                    dismiss();
                }
            }
        });

        findViewById(R.id.tv_option2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                    dismiss();
                }
            }
        });

    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }



}
