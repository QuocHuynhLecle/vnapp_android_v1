package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 8. 24..
 */

public class Dialog_Friend extends Dialog {

    String TAG = getClass().getSimpleName();

    private TextView tv_option0, tv_option1;
    private Context context;


    public Dialog_Friend(Context context) {
        super(context);
        this.context = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_friend);

        init();
        setEvent();
    }

    private void init() {
        tv_option0 = (TextView) findViewById(R.id.tv_option0);
        tv_option1 = (TextView) findViewById(R.id.tv_option1);
    }

    private void setEvent() {

        tv_option0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        tv_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.tv_cancel:
//                dismiss();
//                break;
//            case R.id.tv_confirm:
//                if (btn_cancel.isChecked()) {
//                    Moa.hideList(context, phone, 0, mActivityHandler);
//                } else if (btn_block.isChecked()) {
//                    Moa.blockList(context, phone, 1, mActivityHandler);
//                } else if (btn_delete.isChecked()) {
//
//                }
//
//                break;
//        }
//    }

    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


}
