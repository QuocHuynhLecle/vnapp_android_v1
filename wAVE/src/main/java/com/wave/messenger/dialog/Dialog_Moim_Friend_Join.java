package com.wave.messenger.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

/**
 * Created by aveapp on 2017. 9. 8..
 */

public class Dialog_Moim_Friend_Join extends Dialog {

    private View.OnClickListener mListener;
    //
    Context mContext;
    private String mTitle, mIntr, mImgtmb;


    public Dialog_Moim_Friend_Join(Context context, String mTitle, String mIntr, String mImgtmb) {
        super(context);
        this.mContext = context;
        this.mTitle = mTitle;
        this.mIntr = mIntr;
        this.mImgtmb = mImgtmb;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_moim_join);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
        setOnClickEvent();

        init();
    }

    private void init() {

        ((TextView) findViewById(R.id.tv_title)).setText(mTitle);
        ((TextView) findViewById(R.id.tv_sub)).setText(mIntr);
        ((TextView) findViewById(R.id.tv_sub)).setMovementMethod(new ScrollingMovementMethod());
//        Glide.with(mContext).load(mImgtmb).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.meet_timeline_default_img).dontAnimate().into(((ImageView) findViewById(R.id.iv_image)));
        if(mImgtmb.equals("")){
            Glide.with(mContext).load(R.drawable.meet_timeline_default_img).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into(((ImageView) findViewById(R.id.iv_image)));
        }else {
            Glide.with(mContext).load(Constants.MOIM_LIST_IMAGE_URL + mImgtmb).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.meet_timeline_default_img).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into(((ImageView) findViewById(R.id.iv_image)));
        }

        ((Button) findViewById(R.id.bt_cancel)).setText("모임 이동");
        ((Button) findViewById(R.id.bt_ok)).setText("대화방 이동");
    }

    private void setOnClickEvent() {

        findViewById(R.id.bt_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });

        findViewById(R.id.bt_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
        findViewById(R.id.ll_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }


    private View.OnClickListener mOnClickListener;

    public void setListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public void setCancleListener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.bt_cancel).setOnClickListener(mListener);
    }

    public void setConfirmistener(View.OnClickListener listener) {
        this.mListener = listener;
        findViewById(R.id.bt_ok).setOnClickListener(mListener);
    }

}
