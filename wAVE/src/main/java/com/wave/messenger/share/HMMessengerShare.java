package com.wave.messenger.share;

import android.content.Context;
import android.content.Intent;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.fragment.NMFragmentManager;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.PushProc;

import org.json.JSONObject;

import moa.android.api.MOALog;

public class HMMessengerShare {

    static HMMessengerShare instance = null;

    public static HMMessengerShare getInstance() {
        if (instance == null) {
            instance = new HMMessengerShare();
        }
        return instance;
    }

    public HMMessengerShare() {
        MOALog.MOA_DEBUG = false;
        ErrorController.DEBUG_MODE = false;
    }

    /**
     * 추천 하기
     * param
     * userId : 전송자 Id
     * screen : 하면 표시
     * receiver : [{name:수신자 이름, phone:수신자 전화번호} ... ]
     * sms : kakao, sms, line
     *  msg : 추천 메시지
     * roomid : 채팅룸Id
     */
    public boolean sendRecommendMsg(JSONObject params,Context context)  {
        try {
            String type=params.getString("screen");

            ErrorController.showMessage("[HMMessengerShare] sendRecommendMsg : type : " + type + ", params = " + params.toString());

            if("0".equals(type)){
                ErrorController.showMessage("[HMMessengerShare] type is 0");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", params.getString("userid"));
                jsonObject.put("receiver", params.getString("receiver"));
                jsonObject.put("sms", params.getString("sms"));
                jsonObject.put("msg", params.getString("msg"));

                String resultCode = "100";

                jsonObject.put("resultCode", resultCode);
                jsonObject.put("resultMsg", "seccess");
                onSendRecoommendMsg(jsonObject);
            }else {
                ErrorController.showMessage("[HMMessengerShare] type is not 0");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("userid", params.getString("userid"));
                jsonObject.put("receiver", params.getString("receiver"));
                jsonObject.put("sms", "sms");
                jsonObject.put("msg", "이야이야");
                jsonObject.put("resultCode", "100");
                jsonObject.put("resultMsg", "seccess");
                onSendRecoommendMsg(jsonObject);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
    /**
     * 추천 하기
     * param
     * userId : 전송자 Id
     * receiver : [{name:수신자 이름, phone:수신자 전화번호} ... ]
     * sms : kakao, sms, line
     * msg : 추천 메시지
     * roomid : 채팅룸Id
     * result_code : 결과표시
     * result_mag: 결과메시지
     */
    public void onSendRecoommendMsg(JSONObject params){
        //WAVEMainActivity.staticMainActivity.onSendRecommendMsg(params);
    }
    /**
     * 추천 하기
     * param
     * receiver : [{name:수신자 이름, phone:수신자 전화번호,realName:실명} ... ]
     * gift:보낸 선물코드
     * chatid : 챗 아이디
     * roomid : 채팅룸Id
     */
    public void sendGift(JSONObject params, Context context){

    }

    /**
     * 포인트 전송.
     * param
     * userId : 전송자 Id
     * receiver :{id : 수신자ID, name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * point : 포인트.
     * roomId : 채팅 룸 ID
     * chitid : 챗 아이디
     */
    public void sendPoint(JSONObject params,Context context)  {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", params.getString("userid"));
            jsonObject.put("receiver", params.getJSONObject("receiver"));
            jsonObject.put("point", params.getString("point"));

            String resultCode = "100";

            jsonObject.put("resultCode", resultCode);

            jsonObject.put("resultMsg", "success");
            jsonObject.put("roomid",params.getString("roomid"));
            jsonObject.put("chatid",params.getString("chatid"));
            jsonObject.put("payment_key","11");
            onSendPoint(jsonObject);
        }catch (Exception e){

        }

    }

    /**
     * 포인트 전송 결과.
     * param
     * userId : 전송자 Id
     * receiver :{id : 수신자ID, name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * point : 포인트.
     * resultCode :  100 : 성공, 400 : 실패(송금한도 초과), 401 : 실패(보유머니 부족)
     * msg : 결과 메시지.
     * roomId : 채팅 룸 ID
     * chatid : 챗 아이디
     */
    public void onSendPoint(JSONObject params)  {
//        Dialog_Boneyo dialog = Dialog_Boneyo.getInstance();
//        dialog.onSendPoint(params);
    }

    /**
     * 포인트 취소.
     * param
     * receiver : {name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * point : 포인트.
     * roomId : 채팅 룸 ID
     * chatid : 챗 아이디
     */
    public void cancelPoint(JSONObject params,Context context){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("receiver", params.getString("receiver"));
            jsonObject.put("point", params.getString("point"));
            jsonObject.put("resultCode", "100");
            jsonObject.put("resultMsg", "success");
            jsonObject.put("roomid",params.getString("roomid"));
            jsonObject.put("chatid",params.getString("chatid"));
            onCancelPoint(jsonObject);
        }catch (Exception e){

        }


    }
    /**
     * 포인트 취소 결과 수신.
     * param
     * receiver : {name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * point : 보낸머니.
     * resultCode :  100 : 성공, 400 : 실패
     * msg : 결과 메시지.
     * roomId : 채팅 룸 ID
     * chatid : 챗 아이디
     */
    public void onCancelPoint(JSONObject params){
//        Fragment_ChatRoom.getInstance().OnCancelPoint(params);


    }


    /**
     * 보유 포인트 확인.
     * param
     * userId: 회원 ID
     * receiver : {id : 수신자ID, name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * roomId : 채팅 룸 ID
     * chatid : 챗 아이디
     */
    public void checkPoint(JSONObject params,Context context) {
        //데이터 바음
        /*HMCheckPoint hmCheckPoint = new HMCheckPoint();
        hmCheckPoint=HMCheckPoint.putJSONData(params);
        //목업데이타
        HMCheckPoint hmCheckPointMokUp = new HMCheckPoint();
        hmCheckPointMokUp.setUserid(hmCheckPoint.getUserid());
        hmCheckPointMokUp.setReceiver(hmCheckPoint.getReceiver());
        hmCheckPointMokUp.setSpoint("10000");
        hmCheckPointMokUp.setSpointlimit("500000");
        hmCheckPointMokUp.setRpointlimit("300000");
        hmCheckPointMokUp.setOnetimelimit("300000");
        hmCheckPointMokUp.setRoomid(hmCheckPoint.getRoomid());
        hmCheckPointMokUp.setChatid(hmCheckPoint.getChatid());
        hmCheckPointMokUp.setResultMsg("");
        hmCheckPointMokUp.setResultCode("100");
        //보유데이터 리턴
        JSONObject hmJsonObject=HMCheckPoint.makeJson(hmCheckPointMokUp);
        onCheckPoint(hmJsonObject);*/
    }


    /**
     * 보유 포인트 리턴
     * param
     * userId: 회원 ID
     * receiver : {id : 수신자ID, name:수신자 이름, phone:수신자 전화번호, realName:실명}
     * spoint : 송신자 보유머니
     * spointlimit : 송신자 머니 1일 송금 한도(남은 한도를 전달)
     * rpointlimit : 수신자 머니 입금 한도(남은 한도를 전달)
     * onetimelimit : 1회 송금한도
     * daylimit : 1일 송금한도
     * roomid : 채팅룸 ID
     * chatid 챗아이디
     */
    public void onCheckPoint(JSONObject params) {
        //데이터 받음
//        Fragment_ChatRoom.getInstance().onCheckMoney(params);
        //  Dialog_Boneyo dialog = Dialog_Boneyo.getInstance();
        // dialog.onCheckPoint(params);

    }
    /**
     * 충전 페이지 호출
     * param
     * userid: 멤버스아이디
     */
    public void chargePoint(JSONObject params, Context context){

    }
    /**
     * 주세요/더치페이
     * param
     * seq: 1:주세요/2.더치페이
     * receiver : [{id : 수신자ID, name:수신자 이름, phone:수신자 전화번호, realName:실명} ... ]
     * point : 요청 머니
     */
    public void reqPoint(JSONObject params, Context context){



    }

    /**
     * 하나멤버스 이벤트 화면 호출
     * param
     * eventcode : 하나멤버스 이벤트 코드
     */

    public void reqEvent(JSONObject params,Context context){

    }

    /**
     * 카메라 권한 체크 (부모앱에서 카메라 사용 여부를 체크해서 리턴)
     *
     * @return 사용 가능하면 true
     */
    public boolean getCameraPermission() {
        return true;
    }

    /**
     * 공유화면 요청
     * @param
     *
     */
    public void onShare(JSONObject Params) {

    }
    /**
     * 푸시알림 수신.
     * @param
     * context : 이벤트 수신시 받은 Context
     * params : 이벤트 수신시 받은 JSONObject
     */
    public void gcmMessage(JSONObject params,Context context){
        MessengerInfo.setPushData(context,params.toString());
    }
    /**
     * 푸시알림 수신.
     * @param
     * context : 이벤트 수신시 받은 Context
     * intent : 이벤트 수신시 받은 intent
     * screenOnOff : 화면 꺼짐 상태 0 : off, 1 : on
     */
    public void onGcmMessage(Context context, Intent intent, String screenOnOff) {
        try {
            PushProc.getInstance().onReceivePushMsg(context,intent, screenOnOff);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Back 키 이벤트.
     */
    public void onBackPressed() {
        try {
            NMFragmentManager manager = NMFragmentManager.getInstance();
            manager.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLuckyBagUrl(String kind, String userId) {
        StringBuilder sb = new StringBuilder();
        sb.append("?").append("kind=").append(kind);
        sb.append("&").append("MEB_CST_NO=").append(userId);

        return Constants.LUCKY_BAG_URL + sb.toString();
    }
}