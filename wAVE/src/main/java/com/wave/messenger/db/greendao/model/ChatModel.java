package com.wave.messenger.db.greendao.model;

import android.content.Context;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.greendao.dao.ChatEntityDao;
import com.wave.messenger.utility.DateUtil;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.GreenDaoManager;
import com.wave.messenger.utility.NumberUtil;
import com.wave.messenger.vo.VoChatData;

import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class ChatModel {
    public static ChatEntity getChatEntity(Context context, String chatId) {
        ChatEntityDao dao = GreenDaoManager.getInstance().getSession(context).getChatEntityDao();
        return dao.queryBuilder().where(ChatEntityDao.Properties.ChatId.eq(chatId)).unique();
    }

    public static boolean checkSendedToday(Context context, String toId, Date regDate) {
        List<Date> today = DateUtil.getToday(regDate);
        ChatEntityDao dao = GreenDaoManager.getInstance().getSession(context).getChatEntityDao();

        QueryBuilder qb = dao.queryBuilder();
        long count = qb.where(
                ChatEntityDao.Properties.MessageType.in("0", "1", "2"),
                ChatEntityDao.Properties.SenderId.eq(toId),
                ChatEntityDao.Properties.RegDate.between(today.get(0), today.get(1)),
                ChatEntityDao.Properties.OwnerId.eq(MessengerInfo.getUserId(context))
        ).count();

        ErrorController.writeLog("checkSendedToday::" + count);

        return count > 0;
    }

    public static ChatEntity convertChatEntity(VoChatData data) {
        if (data == null) {
            return null;
        }

        ChatEntity result = new ChatEntity();
        result.setChatIndex(data.getCid());
        result.setChatId(data.getChatId());
        result.setOwnerId(data.getOwnerId());
        result.setChatType(data.getChatType());
        result.setMessageType(data.getMessageType());
        result.setTitle(data.getTitle());
        result.setMessage(data.getMessage());
        result.setAttachment(data.getAttachment());
        result.setRoomId(data.getRoomId());
        result.setSenderId(data.getInviteUser());
        result.setRetryCnt(data.getRetryCnt());
        result.setAttachmentLocal(data.getAttachmentLocal());
        result.setMessage_more(data.getMessageMore());

        result.setStatus(NumberUtil.toInt(data.getStatus(), -1));
        result.setRegDate(DateUtil.parse("yyyy-MM-dd HH:mm:ss", data.getReg_date(), new Date()));

        return result;
    }

    public static boolean insert(Context context, ChatEntity entity) {
        try {
            if (entity != null) {
                ChatEntityDao dao = GreenDaoManager.getInstance().getSession(context).getChatEntityDao();

                dao.insert(entity);
                return true;
            }
        } catch (Exception ignored) {
            return false;
        }

        return false;
    }

    public static boolean update(Context context, ChatEntity entity) {
        try {
            if (entity != null) {
                ChatEntityDao dao = GreenDaoManager.getInstance().getSession(context).getChatEntityDao();

                dao.update(entity);
                return true;
            }
        } catch (Exception ignored) {
            return false;
        }

        return false;
    }
}
