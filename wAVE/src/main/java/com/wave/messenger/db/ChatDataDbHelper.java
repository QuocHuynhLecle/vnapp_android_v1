package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class ChatDataDbHelper extends DbHelper {

    public ChatDataDbHelper(Context context) {
        super(context);
    }

    public boolean addChatData(LBJSONArray array, String roomId, String room_type) {
        try {
            ArrayList<VoChatData> buddys = new ArrayList<>();

            for (Object o : array) {
                LBJSONObject jo = (LBJSONObject) o;

                VoChatData item = Statics.loadFromJsonChatList(jo);
                item.setRoomId(roomId);
                buddys.add(item);
            }
            boolean readcount;
            if(room_type.equals("7") || room_type.equals("8")){
                readcount = false;
            }else{
                readcount = true;
            }

            return checkChatData(buddys, readcount);
        } catch (Exception e) {
            return false;
        }
    }

    public void addSendChatData(MOAData data, String roomId) {
        try {
            LBJSONArray array = data.body.getArray("params");

            ArrayList<VoChatData> buddys = new ArrayList<VoChatData>();
            for (int i = 0; i < array.size(); i++) {
                LBJSONObject jo = (LBJSONObject) array.get(i);

                VoChatData item = Statics.loadFromJsonChatList(jo);
                item.setRoomId(roomId);
                buddys.add(item);
            }

//            checkChatData(buddys);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public ArrayList<VoChatData> getImage(String room_Id) {
        ArrayList<VoChatData> list = new ArrayList<VoChatData>();
        try {

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
//					String query = "SELECT * from tb_chatData WHERE roomId='"+room_id+"'"+" order by CAST(regDate AS date) asc";
                String query = "SELECT * from tb_chatData WHERE roomId='" + room_Id + "' and messageType ='2' " + " order by CAST(cId AS INTEGER) asc";
                c = db.rawQuery(query, new String[]{});

                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    VoChatData item = new VoChatData();
                    item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                    //item.setOwnerId(c.getString(c.getColumnIndex("senderId")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setChatType(c.getString(c.getColumnIndex("chatType")));
                    item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                    item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setMessage(c.getString(c.getColumnIndex("message")));
                    item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                    item.setAttachmentLocal(c.getString(c.getColumnIndex("attachmentLocal")));
                    item.setStatus(c.getString(c.getColumnIndex("status")));
                    item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                    item.setCancel(c.getString(c.getColumnIndex("cancel")));
                    list.add(item);
                    c.moveToNext();
                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return list;

    }

    public VoChatData addChatDataRecive(MOAData data, String roomId, String roomtype) {
        VoChatData item;

        try {
            ArrayList<VoChatData> buddys = new ArrayList<>();

            LBJSONObject jo = data.body;

            //LogTrace.E("addchat data : " + data.body);

            item = Statics.loadFromJsonChatMSG(jo);
            item.setRoomId(roomId);
            item.setStatus("100");
            item.setCancel("N");
            item.setAttachmentLocal("");
            buddys.add(item);

            boolean readcount;
            if(roomtype.equals("7") || roomtype.equals("8")){
                readcount = false;
            }else{
                readcount = true;
            }
            boolean result = checkChatData(buddys, readcount);

            if (!result) {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return item;
    }

    public VoChatData addChatDataRecive(LBJSONObject data, String roomId, String roomtype) {
        VoChatData item;

        try {
            ArrayList<VoChatData> buddys = new ArrayList<>();

//            LBJSONObject jo = data.body;

            //LogTrace.E("addchat data : " + data.body);

            item = Statics.loadFromJsonChatMSG(data);
            item.setRoomId(roomId);
            item.setStatus("100");
            item.setCancel("N");
            item.setAttachmentLocal("");
            buddys.add(item);

            boolean readcount;
            if(roomtype.equals("7") || roomtype.equals("8")){
                readcount = false;
            }else{
                readcount = true;
            }
            boolean result = checkChatData(buddys, readcount);

            if (!result) {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return item;
    }

    public boolean checkChatData(ArrayList<VoChatData> list, boolean readcount) {
        boolean result = false;

        SQLiteDatabase wdb = getWritableDatabase();

        try {

            for (VoChatData item : list) {

                if (!existChatId(item.getChatId())) {
                    addChatData(item, wdb);
                } else {
                    //Todo 18.01.25 채팅방 입장할 때 속도가 너무 느려서 주석처리.
                   /* if(readcount){
                       updateChatData(item, wdb);
                         }*/
                    updateChatData(item, wdb);//TODO 20180323 test leeyunsu
                }
            }

            result = true;

        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        }

        return result;
    }

    public void addChatData(VoChatData item, SQLiteDatabase wdb) {
        wdb.beginTransaction();

        try {
            String query = "INSERT INTO tb_chatData(cId, chatId, ownerId, chatType, messageType, staffMsg, title, message, attachment, roomId, senderName, regDate, message_more, retryCnt, status, attachmentLocal, cancel)"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            wdb.execSQL(query, new Object[]{
                    item.getCid(),
                    item.getChatId(),
                    item.getOwnerId(),
                    item.getChatType(),
                    item.getMessageType(),
                    item.getStaffMsg(),
                    item.getTitle(),
                    item.getMessage(),
                    item.getAttachment(),
                    item.getRoomId(),
                    item.getSenderName(),
                    item.getReg_date(),
                    item.getMessageMore(),
                    item.getRetryCnt(),
                    item.getStatus(),
                    item.getAttachmentLocal(),
                    item.getCancel()

            });

            wdb.setTransactionSuccessful();

            LogTrace.E("insert : " + item.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }

    }

    public void updateChatData(VoChatData item, SQLiteDatabase wdb) {

        wdb.beginTransaction();

        try {

            String query = "";


            query = "UPDATE tb_chatData set cId='" + item.getCid() + "', " +
                    "ownerId='" + item.getOwnerId() + "', " +
                    "chatType='" + item.getChatType() + "', " +
                    "messageType='" + item.getMessageType() + "', " +
                    "senderName='" + item.getSenderName() + "', " +
                    "title='" + item.getTitle() + "', " +
                    "message='" + item.getMessage() + "', " +
                    "attachment='" + item.getAttachment() + "', " +
                    "roomId='" + item.getRoomId() + "', " +
                    "staffMsg='" + item.getStaffMsg() + "', " +
//                    "senderId='" + item.getOwnerId() + "', " +
                    "regDate='" + item.getReg_date() + "', " +
                    "message_more='" + item.getMessageMore() + "', " +
                    "status='" + item.getStatus() + "', " +
                    "retryCnt=" + item.getRetryCnt() + ", " +
                    "cancel='" + item.getCancel() + "', " +
                    "attachmentLocal='" + item.getAttachmentLocal() + "' " +
                    "where chatId='" + item.getChatId() + "' ";

            wdb.execSQL(query, new Object[]{});

            wdb.setTransactionSuccessful();

            LogTrace.E("update : " + item.getMessage());


        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        } finally {
            wdb.endTransaction();
        }
    }

    public ArrayList<VoChatData> getFailMsg() {
        ArrayList<VoChatData> list = new ArrayList<>();
        try {

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT * from tb_chatData WHERE status= 1 order by CAST(cId AS INTEGER) asc";
                c = db.rawQuery(query, new String[]{});

                c.moveToFirst();

                while (!c.isAfterLast()) {
                    VoChatData item = new VoChatData();
                    item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setChatType(c.getString(c.getColumnIndex("chatType")));
                    item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                    item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setMessage(c.getString(c.getColumnIndex("message")));
                    item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                    item.setAttachmentLocal(c.getString(c.getColumnIndex("attachmentLocal")));
                    item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                    item.setMessageMore(c.getString(c.getColumnIndex("message_more")));
                    item.setStatus(c.getString(c.getColumnIndex("status")));
                    item.setRetryCnt(c.getInt(c.getColumnIndex("retryCnt")));
                    list.add(item);
                    c.moveToNext();
                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return list;
    }

    public void updateFailMsg(String chatId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "UPDATE tb_chatData set status= 400 " +
                    "where chatId='" + chatId + "' ";

            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }

    }

    public void updateCancleBoneyo(String chatId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "UPDATE tb_chatData set cancel= 'Y' " +
                    "where chatId='" + chatId + "' ";

            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }

    }

    public void updateTryCount(String chatId, int cnt) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "UPDATE tb_chatData set retryCnt= " + String.valueOf(cnt) +
                    " where chatId='" + chatId + "' ";

            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public void updateClearTryMsg(String chatId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "UPDATE tb_chatData set retryCnt= 0 ,status = 1" +
                    " where chatId='" + chatId + "' ";

            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public void updateBlind(String roomId, String chatId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            String query = "UPDATE tb_chatData set " +
                    "messageType='0', " +
                    "message='블라인드 된 메세지 입니다.' " +
                    "where roomId = '" + roomId + "' and chatId='" + chatId + "' ";

            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public boolean existChatData(String room_id, String chat_id) {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_chatData WHERE roomId='" + room_id + "' and chatId='" + chat_id + "'";

            c = db.rawQuery(query, null);
            int i = 0;
            c.moveToFirst();

            while (!c.isAfterLast()) {
                i++;
            }

            if (i > 0) {
                result = true;
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (c != null) {
                c.close();
            }

        }

        return result;
    }

    public boolean existChatId(String chat_id) {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            String query = "SELECT * FROM tb_chatData WHERE chatId='" + chat_id + "'";

            c = db.rawQuery(query, new String[]{});
            int i = 0;
            c.moveToFirst();
            if (c.isAfterLast() == false) {
                i++;
            }
            if (i > 0) {
                result = true;
            }
        } catch (Exception e) {

        } finally {
            if (c != null) {
                c.close();

            }
        }

        return result;
    }

    public String[] lastChatId(String roomId) {
        String cId[] = new String[2];

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            //
            String query = "SELECT cId,chatId FROM tb_chatData WHERE roomId='" + roomId + "' and cId <> ''order By CAST(cId AS INTEGER) desc limit 1";

            c = db.rawQuery(query, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                cId[0] = c.getString(c.getColumnIndex("cId"));
                cId[1] = c.getString(c.getColumnIndex("chatId"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();

            }
        }

        return cId;
    }

    public boolean exitChatData(String room_id) {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            String query = "DELETE FROM tb_chatData WHERE roomId='" + room_id + "'";

            c = db.rawQuery(query, null);
            c.moveToFirst();

            int i = 0;

            while (!c.isAfterLast()) {
                i++;
            }

            if (i > 0) {
                result = true;
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (c != null) {
                c.close();

            }

        }

        return result;
    }

    public boolean delChatData(String chatId) {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "DELETE FROM tb_chatData WHERE chatId='" + chatId + "'";

            c = db.rawQuery(query, null);
            c.moveToFirst();

            int i = 0;

            while (!c.isAfterLast()) {
                i++;
            }

            if (i > 0) {
                result = true;
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (c != null) {
                c.close();

            }

        }

        return result;
    }

    public void delChatData(String roomId, String chatId) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM tb_chatData WHERE chatId='" + chatId + "' and roomId='" + roomId + "'";

        try {
            db.beginTransaction();

            db.execSQL(query);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();

        }
    }

    public void cleanChatData(String roomId, String cId) {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.beginTransaction();

            String query = "DELETE FROM tb_chatData WHERE roomId = ? and cId <= ?";

            db.execSQL(query, new String[]{roomId, cId});
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();

        }
    }

    public String getLastCid(String roomId) {
        SQLiteDatabase db = getReadableDatabase();

        String cId = "";

        Cursor c = null;

        try {
            String query = "SELECT cId FROM tb_chatData WHERE roomId = '" + roomId + "' ORDER BY CAST(cId AS INTEGER) desc";

            c = db.rawQuery(query, null);
            c.moveToFirst();

            cId = c.getString(c.getColumnIndex("cId"));

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (c != null) {
                c.close();

            }

        }

        return cId;
    }

    public ArrayList<VoChatData> getStaffChatData(String room_id) {
        ArrayList<VoChatData> list = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT * from tb_chatData WHERE roomId='" + room_id + "' and staffMsg = '1'" + "order by regDate desc, cId desc";
            c = db.rawQuery(query, new String[]{});
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoChatData item = new VoChatData();
                item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setCid(c.getString(c.getColumnIndex("cId")));
                item.setChatId(c.getString(c.getColumnIndex("chatId")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                item.setChatType(c.getString(c.getColumnIndex("chatType")));
                item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setMessage(c.getString(c.getColumnIndex("message")));
                item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                item.setMessageMore(c.getString(c.getColumnIndex("message_more")));
                item.setRetryCnt(c.getInt(c.getColumnIndex("retryCnt")));
                item.setStatus(c.getString(c.getColumnIndex("status")));
                item.setCancel(c.getString(c.getColumnIndex("cancel")));
                list.add(item);
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public ArrayList<VoChatData> getChatData(String room_id) {
        ArrayList<VoChatData> list = new ArrayList<>();

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;


            try {
                String query = "SELECT * from tb_chatData WHERE roomId='" + room_id + "'" + "order by regDate desc, cId desc";
                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                while (!c.isAfterLast()) {
                    VoChatData item = new VoChatData();
                    item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                    item.setChatType(c.getString(c.getColumnIndex("chatType")));
                    item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                    item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setMessage(c.getString(c.getColumnIndex("message")));
                    item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                    item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                    item.setMessageMore(c.getString(c.getColumnIndex("message_more")));
                    item.setRetryCnt(c.getInt(c.getColumnIndex("retryCnt")));
                    item.setStatus(c.getString(c.getColumnIndex("status")));
                    item.setCancel(c.getString(c.getColumnIndex("cancel")));
//                    item.setMTS(true);
                    list.add(item);
                    c.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<VoChatData> getChatDataLinit(String room_id, int count) {
        ArrayList<VoChatData> list = new ArrayList<>();

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;


            try {
                String query = "SELECT * from tb_chatData WHERE roomId='" + room_id + "'" + "order by regDate desc, cId desc limit 30 offset " + count;
                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                while (!c.isAfterLast()) {
                    VoChatData item = new VoChatData();
                    item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                    item.setChatType(c.getString(c.getColumnIndex("chatType")));
                    item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                    item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setMessage(c.getString(c.getColumnIndex("message")));
                    item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                    item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                    item.setMessageMore(c.getString(c.getColumnIndex("message_more")));
                    item.setRetryCnt(c.getInt(c.getColumnIndex("retryCnt")));
                    item.setStatus(c.getString(c.getColumnIndex("status")));
                    item.setCancel(c.getString(c.getColumnIndex("cancel")));
//                    item.setMTS(true);
                    list.add(item);
                    c.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public VoChatData getRead(String roomId) {
        VoChatData item = null;
        try {

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT cId,chatId from tb_chatData WHERE roomId='" + roomId + "'" + " and cId <> '' order by cast(cId as integer) desc limit 1";

                c = db.rawQuery(query, new String[]{});

                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    item = new VoChatData();
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    c.moveToNext();
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return item;
    }

    public ArrayList<VoChatData> getChatDataNext(String room_id, String chatData_seq) {


        ArrayList<VoChatData> list = new ArrayList<VoChatData>();
        try {

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT * from tb_chatData WHERE roomId='" + room_id + "'" + "and chatData_seq < " + chatData_seq + " order by regDate desc limit 50 ";
                c = db.rawQuery(query, new String[]{});

                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    VoChatData item = new VoChatData();
                    item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setCid(c.getString(c.getColumnIndex("cId")));
                    item.setChatId(c.getString(c.getColumnIndex("chatId")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                    item.setChatType(c.getString(c.getColumnIndex("chatType")));
                    item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                    item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setMessage(c.getString(c.getColumnIndex("message")));
                    item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                    item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                    item.setMessageMore(c.getString(c.getColumnIndex("message_more")));
                    item.setRetryCnt(c.getInt(c.getColumnIndex("retryCnt")));
                    item.setStatus(c.getString(c.getColumnIndex("status")));
                    item.setCancel(c.getString(c.getColumnIndex("cancel")));
                    list.add(item);
                    c.moveToNext();
                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return list;
    }

    //
    public long getSearchFirstDate(long chat_room_seq, String contents, String orderType) {
        long result = -1;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            Log.i("TEST", "chat_room_seq : " + chat_room_seq + "contents : " + contents + "orderType : " + orderType);
            try {
                String query = "select regDate from tb_chatData where chatData_seq=? and message like ? order by regDate " + orderType + " limit 1;";
                String query2 = "select message from tb_chatData where message like '%" + contents + "%'";

                c = db.rawQuery(query, new String[]{String.valueOf(chat_room_seq), "%" + contents + "%"});
                Cursor c2 = db.rawQuery(query2, new String[]{});

                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    result = c.getLong(0);
                    c.moveToNext();
                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {

        }

        return result;
    }

    public int getSearchChatCount(String contents){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c2 = null;
        try {
            String query2 = "select message from tb_chatData where message like '%" + contents + "%'";
             c2 = db.rawQuery(query2, new String[]{});

        }catch (Exception e){

        }

        return c2.getCount();
    }

    public void getChatMsgListAsDate(Context context, ArrayList<VoChatData> items, long chat_room_seq, long searchDate) {

        ArrayList<VoChatData> tempItems = new ArrayList<VoChatData>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            String query = "select chatData_seq,roomId,cId,chatId,senderName,ownerId,chatType,messageType,staffMsg,title,message,attachment,attachmentLocal,status,regDate,cancel from tb_chatData where chat_room_seq=? and reg_date < ? and  reg_date >= ? order by reg_date asc;";

            c = db.rawQuery(query, new String[]{String.valueOf(chat_room_seq), String.valueOf(items.get(0).getReg_date()), String.valueOf(searchDate)});

            c.moveToFirst();
            while (c.isAfterLast() == false) {
                VoChatData item = new VoChatData();

                item.setChatData_seq(c.getString(c.getColumnIndex("chatData_seq")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setCid(c.getString(c.getColumnIndex("cId")));
                item.setChatId(c.getString(c.getColumnIndex("chatId")));
                item.setSenderName(c.getString(c.getColumnIndex("senderName")));
                //item.setOwnerId(c.getString(c.getColumnIndex("senderId")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setChatType(c.getString(c.getColumnIndex("chatType")));
                item.setMessageType(c.getString(c.getColumnIndex("messageType")));
                item.setStaffMsg(c.getString(c.getColumnIndex("staffMsg")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setMessage(c.getString(c.getColumnIndex("message")));
                item.setAttachment(c.getString(c.getColumnIndex("attachment")));
                item.setAttachmentLocal(c.getString(c.getColumnIndex("attachmentLocal")));
                item.setStatus(c.getString(c.getColumnIndex("status")));
                item.setReg_date(c.getString(c.getColumnIndex("regDate")));
                item.setCancel(c.getString(c.getColumnIndex("cancel")));

                tempItems.add(item);

                c.moveToNext();

            }

            items.addAll(0, tempItems);

        } catch (Exception e) {

        } finally {
            if (c != null) {
                c.close();
            }
        }
    }


}
