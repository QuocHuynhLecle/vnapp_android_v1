package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.vo.VoGroupList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class GroupDbHelper extends DbHelper {

    String TAG=GroupDbHelper.class.getName();
    Context mContext;
    public GroupDbHelper(Context context) {
        super(context);
        this.mContext=context;
    }

    //그룹리스트
    public void addGroupList(Context context, MOAData data, boolean db) {
        ArrayList<VoGroupList> result = new ArrayList<>();
        LBJSONArray grpLstArray = data.body.getArray("params");
        ArrayList<VoGroupList> groupList = new ArrayList<>();
        for (int i = 0; i < grpLstArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) grpLstArray.get(i);
            VoGroupList item = VoGroupList.loadFromJsonServerResponse(jo);
            if ("0".equals(item.getGrpTy()))
                groupList.add(item);
        }

        new RefreshGroupListAsync(groupList, context).execute();
    }

    private class RefreshGroupListAsync extends AsyncTask<Void, Void, Void> {

        private ArrayList<VoGroupList> groupLists;
        private Context context;
        private SQLiteDatabase wdb;

        public RefreshGroupListAsync(ArrayList<VoGroupList> receivedGroupList, Context context) {
            this.groupLists = receivedGroupList;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wdb = getWritableDatabase();
                wdb.beginTransaction();

                //그룹 리스트 삭제
                wdb.execSQL("DELETE FROM "+ LocalDB.tb_groupList);

                //그룹 목록 db삽입
                for (VoGroupList voGroupList : groupLists) {


                    String query = "INSERT INTO " + LocalDB.tb_groupList + " (gId, grpNm, rmId, grpTy)"
                            + " VALUES(?,?,?,?);";

                    wdb.execSQL(query,
                            new Object[]{
                                    voGroupList.getgId(),
                                    voGroupList.getGrpNm(),
                                    voGroupList.getRmId(),
                                    voGroupList.getGrpTy()
                            });

                }//end of for [add received list to db]

                wdb.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                wdb.endTransaction();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            /*if (Fragment_ChatTab_FriendList.fragmentChatTabFriendList != null)
                Fragment_ChatTab_FriendList.fragmentChatTabFriendList.onUpdateFriend();*/
        }
    }

    public void addGroupList(String userId, VoGroupList item) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "INSERT INTO " + LocalDB.tb_groupList + "(gId, grpNm, rmId, grpTy)"
                    + " VALUES(?,?,?,?);";

            wdb.execSQL(query, new Object[]{
                    item.getgId(),
                    item.getGrpNm(),
                    item.getRmId(),
                    item.getGrpTy()
            });

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateGroupList(String gId, String grpNm) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "UPDATE " + LocalDB.tb_groupList + " set grpNm='" + grpNm + "' WHERE gid = '" + gId + "'";

            wdb.execSQL(query, new Object[]{
                    grpNm
            });

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void deleteGroupList(String gId) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            wdb.execSQL("DELETE FROM "+ LocalDB.tb_groupList + " WHERE gId = '" + gId + "'");

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public ArrayList<VoGroupList> getGroupList() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoGroupList> list = new ArrayList<>();
        try {
            String query = "SELECT * FROM " + LocalDB.tb_groupList + " ORDER BY grpNm ASC";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoGroupList item = new VoGroupList();
                item.setGroup_seq(c.getInt(c.getColumnIndex("group_seq")));
                item.setgId(c.getString(c.getColumnIndex("gId")));
                item.setGrpNm(c.getString(c.getColumnIndex("grpNm")));
                item.setRmId(c.getString(c.getColumnIndex("rmId")));
                item.setGrpTy(c.getString(c.getColumnIndex("grpTy")));

                if ("0".equals(item.getGrpTy()))
                    list.add(item);
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public ArrayList<String> getGroupNameList(String userId) {
        ArrayList<String> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + LocalDB.tb_groupList;
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (c.isAfterLast() == false) {
            String groupName = c.getString(c.getColumnIndex("grpNm"));
            result.add(groupName);
            c.moveToNext();
        }

        return result;
    }

    public boolean existGroup(String userId) {
        boolean result = false;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT * FROM " + LocalDB.tb_groupList;

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    i++;
                }
                if (i > 0) {
                    result = true;
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        ErrorController.showMessage("exits", String.valueOf(result));
        return result;
    }
}