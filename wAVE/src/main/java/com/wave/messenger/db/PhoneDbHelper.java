package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aveapp on 2017. 5. 10..
 */

public class PhoneDbHelper extends DbHelper {

    public PhoneDbHelper(Context context) {
        super(context);
    }

    public void insert(String phone, String name) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();
            String query = "INSERT INTO tb_phonebook(phonenumber, name, flag)"
                    + " VALUES(?,?,?);";

            wdb.execSQL(query,
                    new Object[]{phone, name, 1});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }

    }

    public void checkList(String phone, String name) {
        SQLiteDatabase rdb = getReadableDatabase();

        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_phonebook WHERE phonenumber = '" + phone + "'";

            c = rdb.rawQuery(query, null);

            c.moveToFirst();

            if (c.getCount() != 0) {
                if(phone.equals(c.getString(0)) && name.equals(c.getString(1))) {
                    updateFlag("0", phone);
                } else {
                    update(phone, name);
                }

            } else {
                insert(phone, name);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }

    public void update(String phone, String name) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            String query = "UPDATE tb_phonebook SET name = '" + name
                    + "', flag = '1'"
                    + " WHERE phonenumber = '" + phone + "'";

            wdb.beginTransaction();
            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public void updateFlag(String flag, String phone) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            String query = "UPDATE tb_phonebook SET flag = '" + flag
                    + "' WHERE phonenumber = '" + phone + "'";

            wdb.beginTransaction();
            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public void updateFlagAll() {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            String query = "UPDATE tb_phonebook SET flag = '-1'";

            wdb.beginTransaction();
            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public void delete() {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            String query = "DELETE FROM tb_phonebook WHERE flag = '-1'";

            wdb.beginTransaction();
            wdb.execSQL(query);
            wdb.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            wdb.endTransaction();

        }
    }

    public int checkDelete() {
        SQLiteDatabase rdb = getReadableDatabase();

        Cursor c = null;

        int count = 0;

        try {
            String query = "SELECT * FROM tb_phonebook WHERE flag = -1";

            c = rdb.rawQuery(query, null);

            count = c.getCount();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return count;
    }

    public ArrayList<String> getDeleteList() {
        SQLiteDatabase rdb = getReadableDatabase();

        Cursor c = null;

        ArrayList<String> list = new ArrayList<>();

        try {
            String query = "SELECT * FROM tb_phonebook WHERE flag = -1";

            c = rdb.rawQuery(query, null);

            c.moveToFirst();

            while(!c.isAfterLast()) {
                list.add(c.getString(0));
                c.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return list;
    }

    public HashMap<String, String> select() {
        SQLiteDatabase rdb = getReadableDatabase();

        HashMap<String, String> hashMap = new HashMap<>();

        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_phonebook WHERE flag != '0' and flag != '-1'";

            c = rdb.rawQuery(query, null);

            c.moveToFirst();

            while(!c.isAfterLast()) {
                hashMap.put(c.getString(0), c.getString(1));
                c.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return hashMap;
    }

    public HashMap<String, String> selectAll() {
        SQLiteDatabase rdb = getReadableDatabase();

        HashMap<String, String> hashMap = new HashMap<>();

        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_phonebook";

            c = rdb.rawQuery(query, null);

            c.moveToFirst();

            while(!c.isAfterLast()) {
                hashMap.put(c.getString(0), c.getString(1));
                c.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return hashMap;
    }

}
