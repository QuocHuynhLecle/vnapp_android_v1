package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.vo.VoGroupList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class GroupUserDbHelper extends DbHelper {

    String TAG=GroupUserDbHelper.class.getName();
    Context mContext;
    public GroupUserDbHelper(Context context) {
        super(context);
        this.mContext=context;
    }

    //그룹유저리스트
    public void addGroupUserList(Context context, MOAData data) {
        ArrayList<VoGroupList> result = new ArrayList<>();
        LBJSONArray grpUsrsArray = data.body.getArray("params");
        ArrayList<VoGroupList> groupUserList = new ArrayList<>();
        for (int i = 0; i < grpUsrsArray.size(); i++) {
            LBJSONObject jo = (LBJSONObject) grpUsrsArray.get(i);
            VoGroupList item = VoGroupList.loadFromJsonServerResponse(jo);
            groupUserList.add(item);
        }

        new RefreshGroupUserListAsync(groupUserList, context).execute();
    }

    private class RefreshGroupUserListAsync extends AsyncTask<Void, Void, Void> {

        private ArrayList<VoGroupList> groupLists;
        private Context context;
        private SQLiteDatabase wdb;

        public RefreshGroupUserListAsync(ArrayList<VoGroupList> receivedGroupList, Context context) {
            this.groupLists = receivedGroupList;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wdb = getWritableDatabase();
                wdb.beginTransaction();

                //그룹 리스트 삭제
                wdb.execSQL("DELETE FROM "+ LocalDB.tb_groupUserList);

                //그룹 목록 db삽입
                for (VoGroupList voGroupList : groupLists) {


                    String query = "INSERT INTO " + LocalDB.tb_groupUserList + " (gId, bId)"
                            + " VALUES(?,?);";

                    wdb.execSQL(query,
                            new Object[]{
                                    voGroupList.getgId(),
                                    voGroupList.getbId()
                            });

                }//end of for [add received list to db]

                wdb.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                wdb.endTransaction();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            /*if (Fragment_ChatTab_FriendList.fragmentChatTabFriendList != null)
                Fragment_ChatTab_FriendList.fragmentChatTabFriendList.onUpdateFriend();*/
        }
    }

    public void addGroupList(String userId, VoGroupList item) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "INSERT INTO " + LocalDB.tb_groupUserList + "(gId, bId)"
                    + " VALUES(?,?);";

            wdb.execSQL(query, new Object[]{
                    item.getgId(),
                    item.getbId()
            });

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void deleteGroupUserList(String bId, String gId) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            wdb.execSQL("DELETE FROM "+ LocalDB.tb_groupUserList + " WHERE bId = '" + bId + "' AND gId = '" + gId + "'");

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public ArrayList<VoGroupList> getGroupUserList() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoGroupList> list = new ArrayList<>();
        try {
            String query = "SELECT * FROM " + LocalDB.tb_groupUserList;
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoGroupList item = new VoGroupList();
                item.setGroup_seq(c.getInt(c.getColumnIndex("groupuser_seq")));
                item.setgId(c.getString(c.getColumnIndex("gId")));
                item.setbId(c.getString(c.getColumnIndex("bId")));

                list.add(item);
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public boolean existGroup(String userId) {
        boolean result = false;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT * FROM " + LocalDB.tb_groupUserList;

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    i++;
                }
                if (i > 0) {
                    result = true;
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        ErrorController.showMessage("exits", String.valueOf(result));
        return result;
    }
}