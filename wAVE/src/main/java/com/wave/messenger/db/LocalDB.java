package com.wave.messenger.db;

import android.content.Context;

public class LocalDB {
	public final static int DB_VERSION = 1;
	public static String DB_NAME="candleman.db";
	public final static String tb_buddy = "tb_buddy";
	public final static String tb_groupList = "tb_groupList";
	public final static String tb_groupUserList = "tb_groupUserList";
	public final static String tb_chatList = "tb_chatList";
	public final static String tb_chatData="tb_chatData";
	public final static String tb_usersRead="tb_usersRead";
	public final static String tb_users="tb_users";
	public final static String tb_phonebook = "tb_phonebook";
	public final static String TB_MOIM_LIST = "tb_moimList";

	private static DbHelper dbHelper = null;

	public static synchronized DbHelper getDbHelper(Context context) {
		if (dbHelper == null) {
			dbHelper = new DbHelper(context);			
		}
		return dbHelper;
	}
	
	private static BuddyDbHelper buddyDbHelper = null;

	public static synchronized BuddyDbHelper getBuddyDbHelper(Context context) {
		if (buddyDbHelper == null) {
			buddyDbHelper = new BuddyDbHelper(context);			
		}
		return buddyDbHelper;
	}

	private static GroupDbHelper groupDbHelper = null;

	public static synchronized GroupDbHelper getGroupDbHelper(Context context) {
		if (groupDbHelper == null) {
			groupDbHelper = new GroupDbHelper(context);
		}
		return groupDbHelper;
	}

	private static GroupUserDbHelper groupUserDbHelper = null;

	public static synchronized GroupUserDbHelper getGroupUserDbHelper(Context context) {
		if (groupUserDbHelper == null) {
			groupUserDbHelper = new GroupUserDbHelper(context);
		}
		return groupUserDbHelper;
	}

	private static ChatListDbHelper chatListDbHelper = null;

	public static synchronized ChatListDbHelper getChatListDbHelper(Context context){
		if(chatListDbHelper == null) {
			chatListDbHelper = new ChatListDbHelper(context);
		}
		return chatListDbHelper;
	}

	private static ChatDataDbHelper chatDataDbHelper = null;

	public static synchronized ChatDataDbHelper getChatDataDbHelper(Context context){
		if(chatDataDbHelper == null) {
			chatDataDbHelper = new ChatDataDbHelper(context);
		}
		return chatDataDbHelper;
	}

	private static UsersReadDbHelper usersReadDbHelper = null;

	public static synchronized UsersReadDbHelper getUsersReadDbHelper(Context context){
		if(usersReadDbHelper == null) {
			usersReadDbHelper = new UsersReadDbHelper(context);
		}
		return usersReadDbHelper;
	}

	private static UsersDbHelper usersDbHelper = null;

	public static synchronized UsersDbHelper getUsersDbHelper(Context context){
		if(usersDbHelper == null) {
			usersDbHelper = new UsersDbHelper(context);
		}
		return usersDbHelper;
	}

	private static PhoneDbHelper phoneDbHelper = null;

	public static synchronized PhoneDbHelper getPhoneDbHelper(Context context) {
		if(phoneDbHelper == null) {
			phoneDbHelper = new PhoneDbHelper(context);
		}

		return phoneDbHelper;
	}

	private static MoimDbHelper moimDbHelper = null;

	public static synchronized MoimDbHelper getMoimDbHelper(Context context){
		if(moimDbHelper == null) {
			moimDbHelper = new MoimDbHelper(context);
		}
		return moimDbHelper;
	}

	public static void resetDB() {
		DB_NAME = "";
		dbHelper = null;
		buddyDbHelper = null;
		groupDbHelper = null;
		groupUserDbHelper = null;
		chatListDbHelper = null;
		chatDataDbHelper = null;
		usersReadDbHelper = null;
		usersDbHelper = null;
		phoneDbHelper = null;
		moimDbHelper = null;
	}

	public static void close() {
		dbHelper.close();
		buddyDbHelper.close();
		groupDbHelper.close();
		groupUserDbHelper.close();
		chatListDbHelper.close();
		chatDataDbHelper.close();
		usersReadDbHelper.close();
		usersDbHelper.close();
		phoneDbHelper.close();
		moimDbHelper.close();
	}
}
