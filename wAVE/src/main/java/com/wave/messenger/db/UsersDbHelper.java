package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoUsers;

import java.util.ArrayList;

/**
 * Created by user on 2016-09-02.
 */
public class UsersDbHelper extends DbHelper {

    public UsersDbHelper(Context context) {
        super(context);
    }

    public void addUsersList(ArrayList<VoUsers> data, String roomId) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            for(VoUsers item : data) {
                if(!existUser(roomId, item.getUserId())) {
                    addUsers(item, wdb);
                } else {
                    updateUsers(item, wdb);
                }
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }


    private void addUsers(VoUsers item, SQLiteDatabase wdb) {
        try {
            String query = "INSERT INTO tb_users(roomId, userId, userName, tel, realUserName, profile_image, profile_image_type, profile_subject, staff)"
                    + " VALUES(?,?,?,?,?,?,?,?,?);";

            wdb.execSQL(
                    query,
                    new Object[]{item.getRoomId(),
                            item.getUserId(),
                            item.getUserName(),
                            item.getTel(),
                            item.getRealUserName(),
                            item.getProfile_image(),
                            item.getProfile_image_type(),
                            item.getProfile_subject(),
                            item.getStaff()});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUsers(VoUsers item, SQLiteDatabase wdb) {
        try {

            String query = "UPDATE tb_users set userName='" + item.getUserName() + "', " +
                    "tel='" + item.getTel() + "', " +
                    "realUserName='" + item.getRealUserName() + "', " +
                    "profile_image='" + item.getProfile_image() + "', " +
                    "profile_image_type='" + item.getProfile_image_type() + "', " +
                    "profile_subject='" + item.getProfile_subject() + "', " +
                    "staff='" + item.getStaff() + "' " +
                    "where roomId='" + item.getRoomId() + "' and userId='" + item.getUserId() + "'";
            wdb.execSQL(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStaff(VoUsers item) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {

            wdb.beginTransaction();

            String query = "UPDATE tb_users set staff='" + item.getStaff() + "' " +
                    "where roomId='" + item.getRoomId() + "' and userId='" + item.getUserId() + "'";
            wdb.execSQL(query);

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public ArrayList<VoUsers> getUserList(String roomId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoUsers> list = new ArrayList<>();

        try {
            String query = "SELECT * FROM tb_users where roomId='"+roomId+"'";
            c = db.rawQuery(query, null);

            c.moveToFirst();

            while(!c.isAfterLast()) {
                VoUsers item = new VoUsers();
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setTel(c.getString(c.getColumnIndex("tel")));
                item.setRealUserName(c.getString(c.getColumnIndex("realUserName")));
                item.setProfile_image(c.getString(c.getColumnIndex("profile_image")));
                item.setProfile_image_type(c.getString(c.getColumnIndex("profile_image_type")));
                item.setProfile_subject(c.getString(c.getColumnIndex("profile_subject")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                list.add(item);

                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return list;
    }

    public ArrayList<VoUsers> getUserListLimit(String roomId, int count) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoUsers> list = new ArrayList<>();

        try {
            String query = "SELECT * FROM tb_users where roomId='"+roomId+"' limit 10 offset " + count;
            c = db.rawQuery(query, null);

            c.moveToFirst();

            while(!c.isAfterLast()) {
                VoUsers item = new VoUsers();
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setTel(c.getString(c.getColumnIndex("tel")));
                item.setRealUserName(c.getString(c.getColumnIndex("realUserName")));
                item.setProfile_image(c.getString(c.getColumnIndex("profile_image")));
                item.setProfile_image_type(c.getString(c.getColumnIndex("profile_image_type")));
                item.setProfile_subject(c.getString(c.getColumnIndex("profile_subject")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                list.add(item);

                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(c != null) {
                c.close();
            }
        }

        return list;
    }

    public boolean existUser(String roomId, String userId) {
        SQLiteDatabase rdb = getReadableDatabase();
        String query = "SELECT * FROM tb_users WHERE userId = '" + userId + "' and roomId = '" + roomId + "'";
        Cursor c = rdb.rawQuery(query, null);

        int count = c.getCount();

        c.close();

        if(count != 0) {
            return true;
        } else {
            return false;
        }

    }

    public VoFriendList getUserInfo(String roomId, String userId) {
        SQLiteDatabase rdb = getReadableDatabase();

        VoFriendList friend = new VoFriendList();

        String query = "SELECT * FROM tb_users WHERE userId = '" + userId + "' and roomId = '" + roomId + "'";
        Cursor c = rdb.rawQuery(query, null);

        if(c.getCount() != 0) {
//            LogTrace.E("friend not null");

            c.moveToFirst();

            friend.setUserId(c.getString(c.getColumnIndex("userId")));
            friend.setUserName(c.getString(c.getColumnIndex("userName")));
            friend.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
        }

        c.close();

        return friend;

    }

    public boolean deleteUser(String roomId, String userId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "DELETE FROM tb_users WHERE userId = ? and roomId= ?";
            wdb.execSQL(query, new String[]{userId, roomId});

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }

        return true;
    }

    public boolean deleteUsers(String roomId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "DELETE FROM tb_users WHERE roomId= ?";
            wdb.execSQL(query, new String[]{roomId});

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }

        return true;
    }

    public String existUserCaht(ArrayList<String> roomIdList, String userId) {
        String result = "";

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            for(int j = 0 ; j < roomIdList.size() ; j++) {

                String query = "SELECT * FROM tb_users WHERE roomId='" + roomIdList.get(j) + "' and userId='" + userId + "'";

                c = db.rawQuery(query, null);

                c.moveToFirst();

                int i = 0;

                while (!c.isAfterLast()) {
                    i++;
                    c.moveToNext();
                }

                if (i > 0) {
                    result = roomIdList.get(j);
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(c != null) {
                c.close();
            }

        }

        return result;
    }

    public String getUserName(String userId) {
        SQLiteDatabase db = getReadableDatabase();
        String name = "";
        Cursor c = null;

        try {
            String query = "SELECT userName FROM tb_users WHERE userId='" + userId + "'";

            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                name = c.getString(c.getColumnIndex("userName"));
                c.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(c != null) {
                c.close();
            }

        }

        return name;
    }

}
