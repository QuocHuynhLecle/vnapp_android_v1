package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.vo.VoUserRead;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

/**
 * Created by user on 2016-08-31.
 */
public class UsersReadDbHelper extends DbHelper {

    public UsersReadDbHelper(Context context) {
        super(context);
    }

    public void addUsers(MOAData data) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            String roomId = data.body.getString("roomId");
            LBJSONArray array = data.body.getArray("params");
            wdb.beginTransaction();

            deleteUsers(roomId);

            for (int i = 0; i < array.size(); i++) {
                LBJSONObject jo = (LBJSONObject) array.get(i);
                VoUserRead item = VoUserRead.loadFromJson(jo);
                item.setRoomId(roomId);
                addUsers(item, wdb);
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void addUser(MOAData data) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            String roomId = data.body.getString("roomId");
            LBJSONArray array = data.body.getArray("params");
            wdb.beginTransaction();

            for (int i = 0; i < array.size(); i++) {
                LBJSONObject jo = (LBJSONObject) array.get(i);
                VoUserRead item = VoUserRead.loadFromJson(jo);
                item.setRoomId(roomId);
                if (!existChatRoom(item.getRoomId(), item.getUserId())) {
                    addUsers(item, wdb);
                } else {
                    updateChatList(item, wdb);
                }
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void addRevUser(MOAData data) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();
            LBJSONArray jo = data.body.getArray("params");
            ArrayList<VoUserRead> item = VoUserRead.loadFromJsonRev(jo);

            for(int i = 0 ; i < item.size() ; i ++) {
                if (!existChatRoom(item.get(i).getRoomId(), item.get(i).getUserId())) {
                    addUsers(item.get(i), wdb);
                } else {
                    updateChatList(item.get(i), wdb);
                }
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void addRevUsers(MOAData data) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();
            LBJSONObject jo = data.body.getJson("params");
            VoUserRead item = VoUserRead.loadFromJsonRev(jo);

            if (!existChatRoom(item.getRoomId(), item.getUserId())) {
                addUsers(item, wdb);
            } else {
                updateChatList(item, wdb);
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void addUsersList(ArrayList<VoUserRead> data, String roomId) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();
            deleteUsers(roomId);

            for (VoUserRead item : data) {
                addUsers(item, wdb);
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void addUsers(VoUserRead item, SQLiteDatabase wdb) {
        String query = "INSERT INTO tb_usersRead(roomId, userId, start_cid, start_chat_id, last_read_cid, last_read_chat_id)" + " VALUES(?,?,?,?,?,?);";

        wdb.execSQL(query, new Object[]{
                item.getRoomId(),
                item.getUserId(),
                item.getStart_cid(),
                item.getStart_chat_id(),
                item.getLast_read_cid(),
                item.getLast_read_chat_id()
        });
    }

    public void updateChatList(VoUserRead item, SQLiteDatabase wdb) {
        try {
            if (TextUtils.isEmpty(item.getLast_read_cid())) {
                ErrorController.showMessage("dddd");
            }

            String query = "UPDATE tb_usersRead set start_cid='" + item.getStart_cid() + "', " +
                    "start_chat_id='" + item.getStart_chat_id() + "', " +
                    "last_read_cid='" + item.getLast_read_cid() + "', " +
                    "last_read_chat_id='" + item.getLast_read_chat_id() + "' " +
                    "where roomId='" + item.getRoomId() + "' and userId='" + item.getUserId() + "'";
            wdb.execSQL(query, new Object[]{});
        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        }
    }

    public ArrayList<VoUserRead> getUsers(String roomId) {
        ArrayList<VoUserRead> list = new ArrayList<>();

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT * FROM tb_usersRead where roomId='" + roomId + "'";
                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                while (c.isAfterLast() == false) {
                    VoUserRead item = new VoUserRead();
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setUserId(c.getString(c.getColumnIndex("userId")));
                    item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                    item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                    item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                    item.setLast_read_chat_id(c.getString(c.getColumnIndex("last_read_chat_id")));
                    list.add(item);
                    c.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public boolean existChatRoom(String room_id, String userId) {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_usersRead WHERE roomId='" + room_id + "' and userId='" + userId + "'";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            int i = 0;

            while (!c.isAfterLast()) {
                i++;
                c.moveToNext();
            }

            if (i > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return result;
    }

    public boolean deleteUsers(String roomId) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_usersRead WHERE roomId= ?";
            wdb.execSQL(query, new String[]{roomId});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteUser(String roomId, String userId) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_usersRead WHERE userId = ? and roomId= ?";
            wdb.execSQL(query, new String[]{userId, roomId});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}