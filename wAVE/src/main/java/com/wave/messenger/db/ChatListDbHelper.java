package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.StringProcesser;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoChatRoom;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class ChatListDbHelper extends DbHelper {
Context mContext;
    public ChatListDbHelper(Context context) {
        super(context);
        mContext=context;
    }

    public ArrayList<VoChatList> addChatList(MOAData data) {
        ArrayList<VoChatList> result = new ArrayList<>();
        try {
            LBJSONArray array = data.body.getArray("params");
            ArrayList<VoChatList> buddys = new ArrayList<>();

            for (int i = 0; i < array.size(); i++) {
                LBJSONObject jo = (LBJSONObject) array.get(i);
                VoChatList item = VoChatList.loadFromJson(jo);
                if (!"5".equals(item.getRoom_type())){
                    buddys.add(item);
                }else{ //이벤트 룸아이디를 저장해둔다. 알림 리스트에 활용.
                    SharedObject.setProperty_string(mContext, Constants.CHAT_ROOM_ID, item.getRoomId());
                    //SharedObject.setProperty_string(mContext, Constants.CHAT_UNREAD_COUNT, item.getUnread());
                    //Log.e("addChatList","item.getRoomId(): "+item.getRoomId() +"item.getUnread(): "+item.getUnread());

                }

                LogTrace.E("room " + i + " name : " + item.getRoom_name());
            }

            checkChatList(buddys);

            result.addAll(buddys);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void checkChatList(ArrayList<VoChatList> list) {
        for (VoChatList item : list) {
            if (!existChatRoom(item.getRoomId())) {
                addChatList(item);
            } else {
                updateChatList(item);
            }
        }
    }

    public void addChatList(VoChatList item) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "INSERT INTO " + LocalDB.tb_chatList + "(ruid, ownerId, roomSubject, roomId, roomShortId, room_type, room_name, roomComment, room_reg_date, room_profile_image, change_name, last_msg_date, title,"
                    + " unread, useNoti, usercount, start_cid, start_chat_id, last_read_cid, last_read_chat_id, enableSearch, enableWriteMsg, maxUser, hidden, notice_chat_id, subTitle, isMembersChat, entered, staff, mmid)"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            wdb.execSQL(query, new Object[]{
                    item.getRuid(),
                    item.getOwnerId(),
                    item.getRoomSubject(),
                    item.getRoomId(),
                    item.getRoomShortId(),
                    item.getRoom_type(),
                    item.getRoom_name(),
                    item.getRoomComment(),
                    item.getRoom_reg_date(),
                    item.getRoom_profile_image(),
                    item.getChange_name(),
                    item.getLast_msg_date(),
                    item.getTitle(),
                    item.getUnread(),
                    item.getUseNoti(),
                    item.getUsercount(),
                    item.getStart_cid(),
                    item.getStart_chat_id(),
                    item.getLast_read_cid(),
                    item.getLast_read_chat_id(),
                    item.getEnableSearch(),
                    item.getEnableWriteMsg(),
                    item.getMaxUser(),
                    item.getHidden(),
                    item.getNotice_chat_id(),
                    item.getSubTitle(),
                    item.getIsMemberchat(),
                    item.getEntered(),
                    item.getStaff(),
                    item.getMmid()
            });

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateChatList(VoChatList item) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "UPDATE " + LocalDB.tb_chatList + " set ruid='" + item.getRuid() + "', " +
                    "ownerId='" + item.getOwnerId() + "', " +
                    "roomSubject='" + item.getRoomSubject() + "', " +
                    "roomShortId='" + item.getRoomShortId() + "', " +
                    "room_type='" + item.getRoom_type() + "', " +
                    "room_name='" + item.getRoom_name() + "', " +
                    "roomComment='" + item.getRoomComment() + "', " +
                    "room_reg_date='" + item.getRoom_reg_date() + "', " +
                    "room_profile_image='" + item.getRoom_profile_image() + "', " +
                    "change_name='" + item.getChange_name() + "', " +
                    "last_msg_date='" + item.getLast_msg_date() + "', " +
                    "title='" + item.getTitle() + "', " +
                    "unread='" + item.getUnread() + "', " +
                    "useNoti='" + item.getUseNoti() + "', " +
                    "usercount='" + item.getUsercount() + "', " +
                    "start_cid='" + item.getStart_cid() + "', " +
                    "start_chat_id='" + item.getStart_chat_id() + "', " +
                    "last_read_cid='" + item.getLast_read_cid() + "', " +
                    "last_read_chat_id='" + item.getLast_read_chat_id() + "', " +
                    "enableSearch='" + item.getEnableSearch() + "', " +
                    "enableWriteMsg='" + item.getEnableWriteMsg() + "', " +
                    "maxUser='" + item.getMaxUser() + "', " +
                    "hidden='" + item.getHidden() + "', " +
                    "notice_chat_id='" + item.getNotice_chat_id() + "', " +
                    "subTitle='" + item.getSubTitle() + "', " +
                    "isMembersChat='" + item.getIsMemberchat() + "', " +
                    "entered='" + item.getEntered() + "', " +
                    "staff='" + item.getStaff() + "' " +
                    //"mmid='" + item.getMmid() + "' " +
                    "where roomId='" + item.getRoomId() + "' ";

            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateMsg(VoChatList item) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();
            String query = "UPDATE " + LocalDB.tb_chatList + " set " +
                    "last_msg_date='" + item.getLast_msg_date() + "', " +
                    "title='" + item.getTitle() + "', " +
                    "unread='" + item.getUnread() + "'  " +
                    "where roomId='" + item.getRoomId() + "' ";

            LogTrace.E("update msg : " + item.getTitle());

            wdb.execSQL(query, new Object[]{});

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            LogTrace.E("updateMsg Exception");
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateUnread(String roomId, String unread) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();
            String query = "UPDATE " + LocalDB.tb_chatList + " set " +
                    "unread='" + unread + "'  " +
                    "where roomId='" + roomId + "' ";
            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateEnableMsg(String roomId, String enable) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();
            String query = "UPDATE " + LocalDB.tb_chatList + " set " +
                    "enableWriteMsg='" + enable + "'  " +
                    "where roomId='" + roomId + "' ";
            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateEnter(String roomId, String enter) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();
            String query = "UPDATE " + LocalDB.tb_chatList + " set " +
                    "entered='" + enter + "'  " +
                    "where roomId='" + roomId + "' ";
            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            ErrorController.showMessage("insert", "error");
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateRoomSubject(String roomId, String chatName) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();
            String query = "UPDATE " + LocalDB.tb_chatList + " set " +
                    "roomSubject='" + chatName + "'  " +
                    "where roomId='" + roomId + "' ";
            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateNoti(String roomId, String noti) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "UPDATE " + LocalDB.tb_chatList + " set useNoti='" + noti + "' " +
                    "where roomId='" + roomId + "' ";

            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateStaff(String roomId, String staff) {
        SQLiteDatabase wdb = getWritableDatabase();
        try {
            wdb.beginTransaction();

            String query = "UPDATE " + LocalDB.tb_chatList + " set staff='" + staff + "' " +
                    "where roomId='" + roomId + "' ";

            wdb.execSQL(query, new Object[]{});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public void updateHidden(String roomId, String hidden) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            wdb.beginTransaction();

            String query = "UPDATE " + LocalDB.tb_chatList + " set hidden='" + hidden + "' " +
                    "where roomId='" + roomId + "' ";

            wdb.execSQL(query, new Object[]{});

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();

        }
    }

    public ArrayList<VoChatList> getChatList() {
        int count = 0;
        ArrayList<String> groupList = new ArrayList<>();
        groupList.clear();

        //종목 채팅방
        VoChatList jmData = new VoChatList();
        ArrayList<VoChatList> tempList = new ArrayList<>();
        jmData.setList(tempList);
        jmData.setRoom_name("진행 중인 종목채팅방");

        //내 채팅방
        VoChatList myData = new VoChatList();
        tempList = new ArrayList<>();
        myData.setList(tempList);
        myData.setRoom_name("내 채팅방");

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoChatList> list = new ArrayList<>();
        try {
//            String query = "SELECT * FROM " + LocalDB.tb_chatList + " where hidden <> '1' and room_type <>'5' order by last_msg_date desc";
            String query = "SELECT * FROM " + LocalDB.tb_chatList + " where hidden <> '1' order by last_msg_date desc";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoChatList item = new VoChatList();
                item.setChatList_seq(c.getInt(c.getColumnIndex("chatList_seq")));
                item.setRuid(c.getString(c.getColumnIndex("ruid")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setRoomSubject(c.getString(c.getColumnIndex("roomSubject")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setRoomShortId(c.getString(c.getColumnIndex("roomShortId")));
                item.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                if ("8".equals(item.getRoom_type()))
                    item.setJongmok(true);
                else
                    item.setJongmok(false);
                item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                item.setRoomComment(c.getString(c.getColumnIndex("roomComment")));
                item.setRoom_reg_date(c.getString(c.getColumnIndex("room_reg_date")));
                item.setRoom_profile_image(c.getString(c.getColumnIndex("room_profile_image")));
                item.setChange_name(c.getString(c.getColumnIndex("change_name")));
                item.setLast_msg_date(c.getString(c.getColumnIndex("last_msg_date")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setUnread(c.getString(c.getColumnIndex("unread")));
                item.setUseNoti(c.getString(c.getColumnIndex("useNoti")));
                item.setUsercount(c.getString(c.getColumnIndex("usercount")));
                item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                item.setLast_read_chat_id(c.getString(c.getColumnIndex("last_read_chat_id")));
                item.setEnableSearch(c.getString(c.getColumnIndex("enableSearch")));
                item.setEnableWriteMsg(c.getString(c.getColumnIndex("enableWriteMsg")));
                item.setMaxUser(c.getString(c.getColumnIndex("maxUser")));
                item.setHidden(c.getString(c.getColumnIndex("hidden")));
                item.setNotice_chat_id(c.getString(c.getColumnIndex("notice_chat_id")));
                item.setSubTitle(c.getString(c.getColumnIndex("subTitle")));
                item.setIsMemberchat(c.getString(c.getColumnIndex("isMembersChat")));
                item.setEntered(c.getString(c.getColumnIndex("entered")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                item.setMmid(c.getString(c.getColumnIndex("mmid")));


                if ("8".equals(item.getRoom_type())) {
                    if (count < 2) {
                        jmData.getList().add(item);
                        count++;
                    }
                } else {
                    if (!"5".equals(item.getRoom_type()))
                        myData.getList().add(item);
                }

//                list.add(item);
                c.moveToNext();
            }

            list.add(jmData);
            groupList.add("진행 중인 종목채팅방");
            list.add(myData);
            groupList.add("내 채팅방");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public ArrayList<VoChatList> getChatJongmokList(ArrayList<VoChatList> voChatList2) {
        ArrayList<String> groupList = new ArrayList<>();
        groupList.clear();

        //종목 채팅방
        VoChatList jmData = new VoChatList();
        ArrayList<VoChatList> tempList = new ArrayList<>();
        jmData.setList(tempList);
        jmData.setRoom_name(Constants.CHATLISTTYPE_JONGMOK_ING);

        VoChatList jmData2 = new VoChatList();
        ArrayList<VoChatList> tempList2 = new ArrayList<>();
        jmData2.setList(voChatList2);
        jmData2.setRoom_name(Constants.CHATLISTTYPE_JONGMOK);

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoChatList> list = new ArrayList<>();
        try {
            String query = "SELECT * FROM " + LocalDB.tb_chatList + " where hidden <> '1' and room_type <>'5' order by last_msg_date desc";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoChatList item = new VoChatList();
                item.setChatList_seq(c.getInt(c.getColumnIndex("chatList_seq")));
                item.setRuid(c.getString(c.getColumnIndex("ruid")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setRoomSubject(c.getString(c.getColumnIndex("roomSubject")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setRoomShortId(c.getString(c.getColumnIndex("roomShortId")));
                item.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                if ("8".equals(item.getRoom_type()))
                    item.setJongmok(true);
                else
                    item.setJongmok(false);
                item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                item.setRoomComment(c.getString(c.getColumnIndex("roomComment")));
                item.setRoom_reg_date(c.getString(c.getColumnIndex("room_reg_date")));
                item.setRoom_profile_image(c.getString(c.getColumnIndex("room_profile_image")));
                item.setChange_name(c.getString(c.getColumnIndex("change_name")));
                item.setLast_msg_date(c.getString(c.getColumnIndex("last_msg_date")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setUnread(c.getString(c.getColumnIndex("unread")));
                item.setUseNoti(c.getString(c.getColumnIndex("useNoti")));
                item.setUsercount(c.getString(c.getColumnIndex("usercount")));
                item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                item.setLast_read_chat_id(c.getString(c.getColumnIndex("last_read_chat_id")));
                item.setEnableSearch(c.getString(c.getColumnIndex("enableSearch")));
                item.setEnableWriteMsg(c.getString(c.getColumnIndex("enableWriteMsg")));
                item.setMaxUser(c.getString(c.getColumnIndex("maxUser")));
                item.setHidden(c.getString(c.getColumnIndex("hidden")));
                item.setNotice_chat_id(c.getString(c.getColumnIndex("notice_chat_id")));
                item.setSubTitle(c.getString(c.getColumnIndex("subTitle")));
                item.setIsMemberchat(c.getString(c.getColumnIndex("isMembersChat")));
                item.setEntered(c.getString(c.getColumnIndex("entered")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                item.setMmid(c.getString(c.getColumnIndex("mmid")));

                if ("8".equals(item.getRoom_type())) {
                    jmData.getList().add(item);

                }

                c.moveToNext();
            }

            list.add(jmData);
            groupList.add(Constants.CHATLISTTYPE_JONGMOK_ING);

            list.add(jmData2);
            groupList.add(Constants.CHATLISTTYPE_JONGMOK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public ArrayList<VoChatList> getDeliveryChatList() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        ArrayList<VoChatList> list = new ArrayList<>();
        try {
            String query = "SELECT * FROM " + LocalDB.tb_chatList + " where hidden <> '1' and entered <> '0' and room_type <>'5' order by last_msg_date desc";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VoChatList item = new VoChatList();
                item.setChatList_seq(c.getInt(c.getColumnIndex("chatList_seq")));
                item.setRuid(c.getString(c.getColumnIndex("ruid")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setRoomSubject(c.getString(c.getColumnIndex("roomSubject")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setRoomShortId(c.getString(c.getColumnIndex("roomShortId")));
                item.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                item.setRoomComment(c.getString(c.getColumnIndex("roomComment")));
                item.setRoom_reg_date(c.getString(c.getColumnIndex("room_reg_date")));
                item.setRoom_profile_image(c.getString(c.getColumnIndex("room_profile_image")));
                item.setChange_name(c.getString(c.getColumnIndex("change_name")));
                item.setLast_msg_date(c.getString(c.getColumnIndex("last_msg_date")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setUnread(c.getString(c.getColumnIndex("unread")));
                item.setUseNoti(c.getString(c.getColumnIndex("useNoti")));
                item.setUsercount(c.getString(c.getColumnIndex("usercount")));
                item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                item.setLast_read_chat_id(c.getString(c.getColumnIndex("last_read_chat_id")));
                item.setEnableSearch(c.getString(c.getColumnIndex("enableSearch")));
                item.setEnableWriteMsg(c.getString(c.getColumnIndex("enableWriteMsg")));
                item.setMaxUser(c.getString(c.getColumnIndex("maxUser")));
                item.setHidden(c.getString(c.getColumnIndex("hidden")));
                item.setNotice_chat_id(c.getString(c.getColumnIndex("notice_chat_id")));
                item.setSubTitle(c.getString(c.getColumnIndex("subTitle")));
                item.setIsMemberchat(c.getString(c.getColumnIndex("isMembersChat")));
                item.setEntered(c.getString(c.getColumnIndex("entered")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                item.setMmid(c.getString(c.getColumnIndex("mmid")));

                list.add(item);
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return list;
    }

    public VoChatList getChatData(String roomId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        VoChatList item = new VoChatList();

        try {
            String query = "SELECT * FROM " + LocalDB.tb_chatList + " where roomId='" + roomId + "' order by last_msg_date desc";
            c = db.rawQuery(query, null);

            c.moveToFirst();
            if (c.isAfterLast() == false) {
                item.setChatList_seq(c.getInt(c.getColumnIndex("chatList_seq")));
                item.setRuid(c.getString(c.getColumnIndex("ruid")));
                item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                item.setRoomSubject(c.getString(c.getColumnIndex("roomSubject")));
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setRoomShortId(c.getString(c.getColumnIndex("roomShortId")));
                item.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                item.setRoomComment(c.getString(c.getColumnIndex("roomComment")));
                item.setRoom_reg_date(c.getString(c.getColumnIndex("room_reg_date")));
                item.setRoom_profile_image(c.getString(c.getColumnIndex("room_profile_image")));
                item.setChange_name(c.getString(c.getColumnIndex("change_name")));
                item.setLast_msg_date(c.getString(c.getColumnIndex("last_msg_date")));
                item.setTitle(c.getString(c.getColumnIndex("title")));
                item.setUnread(c.getString(c.getColumnIndex("unread")));
                item.setUseNoti(c.getString(c.getColumnIndex("useNoti")));
                item.setUsercount(c.getString(c.getColumnIndex("usercount")));
                item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                item.setLast_read_chat_id(c.getString(c.getColumnIndex("last_read_chat_id")));
                item.setEnableSearch(c.getString(c.getColumnIndex("enableSearch")));
                item.setEnableWriteMsg(c.getString(c.getColumnIndex("enableWriteMsg")));
                item.setMaxUser(c.getString(c.getColumnIndex("maxUser")));
                item.setHidden(c.getString(c.getColumnIndex("hidden")));
                item.setNotice_chat_id(c.getString(c.getColumnIndex("notice_chat_id")));
                item.setSubTitle(c.getString(c.getColumnIndex("subTitle")));
                item.setIsMemberchat(c.getString(c.getColumnIndex("isMembersChat")));
                item.setEntered(c.getString(c.getColumnIndex("entered")));
                item.setStaff(c.getString(c.getColumnIndex("staff")));
                item.setMmid(c.getString(c.getColumnIndex("mmid")));

                LogTrace.E("room : " + item.getRoomId() + " / " + item.getRoom_name());
            } else {
                LogTrace.E("room is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return item;
    }

    public boolean exitChatRoom(String room_id) {
        boolean result = false;

        try {
            SQLiteDatabase db = getWritableDatabase();
            Cursor c = null;
            try {
                String query = "DELETE FROM " + LocalDB.tb_chatList + " WHERE roomId='" + room_id + "'";

                c = db.rawQuery(query, null);
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    i++;
                }
                if (i > 0) {
                    result = true;
                    LogTrace.E("chat list remove");
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return result;
    }

    public ArrayList<String> selectSingleRoom() {
        boolean result = false;
        ArrayList<String> roomIdList = new ArrayList<>();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT roomId FROM " + LocalDB.tb_chatList + " WHERE room_type='0' and hidden <>'1';";

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                while (c.isAfterLast() == false) {
                    roomIdList.add(c.getString(c.getColumnIndex("roomId")));
                    c.moveToNext();
                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return roomIdList;
    }

    public String selectMyRoom() {
        boolean result = false;
        String roomId = "";
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT roomId FROM " + LocalDB.tb_chatList + " WHERE room_type='4';";

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    roomId = c.getString(c.getColumnIndex("roomId"));

                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return roomId;
    }

    public VoChatList selectHana() {
        boolean result = false;
        VoChatList item = new VoChatList();
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT * FROM " + LocalDB.tb_chatList + " WHERE room_type='5';";

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    item.setChatList_seq(c.getInt(c.getColumnIndex("chatList_seq")));
                    item.setRuid(c.getString(c.getColumnIndex("ruid")));
                    item.setOwnerId(c.getString(c.getColumnIndex("ownerId")));
                    item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                    item.setRoom_type(c.getString(c.getColumnIndex("room_type")));
                    item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                    item.setRoom_reg_date(c.getString(c.getColumnIndex("room_reg_date")));
                    item.setRoom_profile_image(c.getString(c.getColumnIndex("room_profile_image")));
                    item.setChange_name(c.getString(c.getColumnIndex("change_name")));
                    item.setLast_msg_date(c.getString(c.getColumnIndex("last_msg_date")));
                    item.setTitle(c.getString(c.getColumnIndex("title")));
                    item.setUnread(c.getString(c.getColumnIndex("unread")));
                    item.setUseNoti(c.getString(c.getColumnIndex("useNoti")));
                    item.setUsercount(c.getString(c.getColumnIndex("usercount")));
                    item.setStart_cid(c.getString(c.getColumnIndex("start_cid")));
                    item.setStart_chat_id(c.getString(c.getColumnIndex("start_chat_id")));
                    item.setLast_read_cid(c.getString(c.getColumnIndex("last_read_cid")));
                    item.setEnableSearch(c.getString(c.getColumnIndex("enableSearch")));
                    item.setEnableWriteMsg(c.getString(c.getColumnIndex("enableWriteMsg")));
                    item.setMaxUser(c.getString(c.getColumnIndex("maxUser")));
                    item.setHidden(c.getString(c.getColumnIndex("hidden")));
                    item.setNotice_chat_id(c.getString(c.getColumnIndex("notice_chat_id")));
                    item.setSubTitle(c.getString(c.getColumnIndex("subTitle")));
                    item.setIsMemberchat(c.getString(c.getColumnIndex("isMembersChat")));

                }

            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        return item;
    }

    public String getRoomName(String room_id) {
        String roomName = "";
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT room_name FROM " + LocalDB.tb_chatList + " WHERE roomId='" + room_id + "'";

                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    roomName = c.getString(c.getColumnIndex("room_name"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return roomName;
    }

    public boolean existChatRoom(String room_id) {
        boolean result = false;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT * FROM " + LocalDB.tb_chatList + " WHERE roomId='" + room_id + "'";

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (c.isAfterLast() == false) {
                    i++;
                }
                if (i > 0) {
                    result = true;
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {

        }

        ErrorController.showMessage("exits", String.valueOf(result));
        return result;
    }

    public int chatRoomNoReadCount(String room_id) {
        String count = "";

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT unread FROM " + LocalDB.tb_chatList + " WHERE roomId='" + room_id + "'";

            c = db.rawQuery(query, new String[]{});

            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getString(c.getColumnIndex("unread"));
                LogTrace.E("chatlist unread : " + count);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();

            }
        }

        if (TextUtils.isEmpty(count)) {
            return 0;
        } else {
            return Integer.parseInt(count);
        }
    }

    public int chatRoomNoReadTotalCount() {
        String count = "";
        int totalCount = 0;

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT sum(CAST(unread AS INTEGER)) as cnt FROM " + LocalDB.tb_chatList + " where hidden = '0'";

                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                if (!c.isAfterLast()) {
                    count = c.getString(c.getColumnIndex("cnt"));
                    totalCount = Integer.parseInt(count);
                    LogTrace.E("unread : " + totalCount);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return totalCount;
    }

    public boolean isHidden(String room_id) {
        boolean result = false;
        String hidden = "";
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT hidden FROM " + LocalDB.tb_chatList + " WHERE roomId='" + room_id + "'";

                c = db.rawQuery(query, new String[]{});
                int i = 0;
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    hidden = c.getString(c.getColumnIndex("hidden"));
                    if ("1".equals(hidden)) {
                        result = true;
                    } else {
                        result = false;
                    }
                }
                if (i > 0) {
                    result = true;
                }
            } catch (Exception e) {

            } finally {
                if (c != null) {
                    c.close();

                }
            }
        } catch (Exception e) {

        }

        ErrorController.showMessage("exits", String.valueOf(result));
        return result;
    }

    public String getUserCount(String roomId) {
        String count = "";
        int userCount = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            String query = "SELECT usercount FROM " + LocalDB.tb_chatList + " where roomId = '" + roomId + "'";

            c = db.rawQuery(query, new String[]{});

            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getString(c.getColumnIndex("usercount"));
                userCount = Integer.valueOf(count);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();

            }
        }

        return String.valueOf(userCount);
    }

    public VoChatRoom getChatRoom(String roomId) {
        VoChatRoom vo = null;
        Cursor c = null;

        try {
            SQLiteDatabase db = getReadableDatabase();

            String query = "SELECT * FROM " + LocalDB.tb_chatList + " WHERE roomId = '" + roomId + "'";
            c = db.rawQuery(query, null);

            if (c != null && c.moveToFirst()) {
                vo = new VoChatRoom();
                vo.setRoomId(roomId);
                vo.setRoomType(c.getString(c.getColumnIndex("room_type")));
                vo.setHidden("1".equals(c.getString(c.getColumnIndex("hidden"))));
            }
        } catch (Exception ignored) {
        } finally {
            if (c != null) {
                c.close();

            }
        }

        return vo;
    }

    public List<String> getAllRoomName(final CharSequence s) {
        List<String> searchedResult_RoomNames = new ArrayList<>();
        List<VoChatList> allUserListOnChatRoom = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try{
            //모든 유저 정보를 긁어온다.
            String query ="SELECT * FROM " + LocalDB.tb_chatList;
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while(!c.isAfterLast()) {
                VoChatList item = new VoChatList();
                item.setRoomId(c.getString(c.getColumnIndex("roomId")));
                item.setRoom_name(c.getString(c.getColumnIndex("room_name")));
                allUserListOnChatRoom.add(item);
                c.moveToNext();
            }

            //검색어와 비교한다.
            for(VoChatList vo : allUserListOnChatRoom) {
                if(vo.getRoom_name().contains(s.toString())){
                    ErrorController.showMessage("[ChatListDBHelper] UserName : " + vo.getRoom_name() + ", searchQuery : " + s);
                    searchedResult_RoomNames.add(vo.getRoomId());
                }
            }

            if(searchedResult_RoomNames.size() == 0) {
                for(VoChatList vo : allUserListOnChatRoom) {
                    if (StringProcesser.matchString(vo.getRoom_name(), s.toString()) && !searchedResult_RoomNames.contains(vo.getRoomId())) {
                        searchedResult_RoomNames.add(vo.getRoomId());
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(c != null)
                c.close();
        }

        return searchedResult_RoomNames;
    }//end of getAllRoomName
}
