package com.wave.messenger.db.greendao.model;

import android.content.Context;

import com.wave.messenger.db.greendao.dao.LuckyBagEntityDao;
import com.wave.messenger.utility.DateUtil;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.GreenDaoManager;

import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class LuckyBagModel {
    public static void deleteAll(Context context) {
        GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao().deleteAll();
    }

    private static boolean checkBoneyoSendedToday(Context context, String toId, Date regDate) {
        List<Date> today = DateUtil.getToday(regDate);
        LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

        QueryBuilder qb = dao.queryBuilder();
        long count = qb.where(LuckyBagEntityDao.Properties.ToId.eq(toId),
                LuckyBagEntityDao.Properties.RegDate.between(today.get(0), today.get(1)),
                LuckyBagEntityDao.Properties.MessageType.eq("6")).count();

        ErrorController.writeLog("checkBoneyoSendedToday Count::" + count);

        return count > 0;
    }

    private static boolean checkBoneyoReceivedToday(Context context, String fromId, Date regDate) {
        List<Date> today = DateUtil.getToday(regDate);
        LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

        QueryBuilder qb = dao.queryBuilder();
        long count = qb.where(LuckyBagEntityDao.Properties.FromId.eq(fromId),
                LuckyBagEntityDao.Properties.RegDate.between(today.get(0), today.get(1)),
                LuckyBagEntityDao.Properties.MessageType.eq("6")).count();

        ErrorController.writeLog("checkBoneyoReceivedToday Count::" + count);

        return count > 0;
    }

    private static boolean checkNormalReceivedToday(Context context, String fromId, Date regDate) {
        List<Date> today = DateUtil.getToday(regDate);
        LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

        QueryBuilder qb = dao.queryBuilder();
        long count = qb.where(LuckyBagEntityDao.Properties.FromId.eq(fromId),
                LuckyBagEntityDao.Properties.RegDate.between(today.get(0), today.get(1)),
                LuckyBagEntityDao.Properties.MessageType.in("0", "1", "2")).count();

        ErrorController.writeLog("checkNormalReceivedToday Count::" + count);

        return count > 0;
    }

    private static boolean checkMaxOverToday(Context context, int maxCount, Date regDate) {
        List<Date> today = DateUtil.getToday(regDate);
        LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

        QueryBuilder qb = dao.queryBuilder();
        long count = qb.where(LuckyBagEntityDao.Properties.MessageType.notEq("6"),
                LuckyBagEntityDao.Properties.RegDate.between(today.get(0), today.get(1))).count();

        ErrorController.writeLog("checkMaxOverToday Count::" + count, "maxCount::" + maxCount);

        return count >= maxCount;
    }

    public static LuckyBagEntity getLuckyBagEntity(Context context, String chatId) {
        LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();
        return dao.queryBuilder().where(LuckyBagEntityDao.Properties.ChatId.eq(chatId)).unique();
    }

    public static boolean insert(Context context, LuckyBagEntity entity) {
        try {
            if (entity != null) {
                Date date = entity.getRegDate();

                if (DateUtil.isToday(date)) {
                    entity.setIsUsed(false);

                    LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

                    dao.insert(entity);
                    return true;
                }
            }
        } catch (Exception ignored) {
            return false;
        }

        return false;
    }

    public static boolean update(Context context, LuckyBagEntity entity) {
        try {
            if (entity != null) {
                LuckyBagEntityDao dao = GreenDaoManager.getInstance().getSession(context).getLuckyBagEntityDao();

                dao.update(entity);
                return true;
            }
        } catch (Exception ignored) {
            return false;
        }

        return false;
    }

    public static boolean checkLuckyBag(Context context, String chatId, String senderId, String messageType, String roomType, Date regDate) {
        boolean isSuccess = true;

        /*try {
            if (!TextUtils.isEmpty(chatId)) {
                LuckyBagEntity luckyBagEntity = new LuckyBagEntity();
                luckyBagEntity.setRegDate(regDate);
                luckyBagEntity.setChatId(chatId);
                luckyBagEntity.setMessageType(messageType);

                String receiverId = senderId;

                if (senderId.equals(MessengerInfo.getUserId(context))) {
                    ErrorController.writeLog("메시지 전송 했음::" + chatId);
                    ChatEntity entity = ChatModel.getChatEntity(context, chatId);

                    if (entity != null) {
                        receiverId = entity.getSenderId();
                    }

                    if ("6".equals(messageType)) {
                        if (!LuckyBagModel.checkBoneyoSendedToday(context, receiverId, regDate)) {
                            luckyBagEntity.setToId(receiverId);

                            isSuccess = LuckyBagModel.insert(context, luckyBagEntity);
                            ErrorController.writeLog("복주머니 등록::" + isSuccess);
                        }
                    }
                } else {
                    ErrorController.writeLog("메시지 수신 했음::" + chatId);

                    if ("6".equals(messageType)) {
                        if (!LuckyBagModel.checkBoneyoReceivedToday(context, receiverId, regDate)) {
                            luckyBagEntity.setFromId(receiverId);

                            isSuccess = LuckyBagModel.insert(context, luckyBagEntity);
                            ErrorController.writeLog("복주머니 등록::" + isSuccess);
                        }
                    } else if ("0".equals(messageType) || "1".equals(messageType) || "2".equals(messageType)) {
                        ErrorController.writeLog("roomType::" + roomType);

                        if ("0".equals(roomType)) {
                            if (!LuckyBagModel.checkMaxOverToday(context, 100, regDate)) {
                                if (!LuckyBagModel.checkNormalReceivedToday(context, receiverId, regDate)) {
                                    if (ChatModel.checkSendedToday(context, receiverId, regDate)) {
                                        luckyBagEntity.setFromId(receiverId);

                                        isSuccess = LuckyBagModel.insert(context, luckyBagEntity);
                                        ErrorController.writeLog("복주머니 등록::" + isSuccess);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
            return false;
        }*/

        return isSuccess;
    }
}
