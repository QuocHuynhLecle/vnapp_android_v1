package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wave.messenger.vo.VoMoimList;

import java.lang.reflect.Type;
import java.util.ArrayList;

import moa.android.api.MOAData;

/**
 * Created by yunsu on 2017-04-15.
 */
public class MoimDbHelper extends DbHelper {

    String TAG=MoimDbHelper.class.getName();
    Context mContext;
    public MoimDbHelper(Context context) {
        super(context);
        this.mContext=context;
    }


    /**
     *
     * 서버에서 받아온 모임리스트를 파싱후, 디비에 저장한다
     * @param data
     * @param context
     */
    public void addMoimList(MOAData data, Context context) {
        //Log.e("MoimDbHelper","addMoimList "+data.toString());

        try {
            //LBJSONArray array = data.body.getArray("params");
            ArrayList<VoMoimList> arMoimList = new ArrayList<>();

            Type listType = new TypeToken<ArrayList<VoMoimList>>(){}.getType();
            arMoimList=new GsonBuilder().create().fromJson(String.valueOf(data.body.getArray("params")), listType);

            /*for (int i = 0; i < arMoimList.size(); i++) {
                Log.e(TAG,"addMoimList Moim_name "+arMoimList.get(i).getMmNm()+" getMmNm: "+arMoimList.get(i).getMmId());
            }*/

            new RefreshMoimListAsync(arMoimList, context).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 디비에서 모임 리스트를 가져온다.

     * @param context
     * @return
     */
    public ArrayList<VoMoimList> getContactMoimListInDB(Context context) {
        //Log.e(TAG,"#getContactMoimListInDB");

        ArrayList<VoMoimList> arVoMoimList = new ArrayList<>();
        VoMoimList item;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+ LocalDB.TB_MOIM_LIST + " ORDER BY regDt DESC"; //asc => 등록 날짜순
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (!c.isAfterLast()) {
            //img_org, moim_tag, reg_user, moim_type, max_user, open_type, my_status, moimId, reg_date, user_cnt, img_type, enter_type, moim_name, write_type, img_thumb
            item = new VoMoimList(
                    c.getString(c.getColumnIndex("mmId")),
                    c.getString(c.getColumnIndex("mmNm")),
                    c.getString(c.getColumnIndex("mmTy")),
                    c.getString(c.getColumnIndex("mmTg")),
                    c.getString(c.getColumnIndex("rmId")),
                    c.getString(c.getColumnIndex("imgTy")),
                    c.getString(c.getColumnIndex("imgOrg")),
                    c.getString(c.getColumnIndex("imgTmb")),
                    c.getString(c.getColumnIndex("intr")),
                    c.getString(c.getColumnIndex("usrCnt")),
                    c.getString(c.getColumnIndex("maxUsr")),
                    c.getString(c.getColumnIndex("opnTy")),
                    c.getString(c.getColumnIndex("entTy")),
                    c.getString(c.getColumnIndex("covTy")),
                    c.getString(c.getColumnIndex("acptTy")),
                    c.getString(c.getColumnIndex("ivtTy")),
                    c.getString(c.getColumnIndex("notiTy")),
                    c.getString(c.getColumnIndex("wrtTy")),
                    c.getString(c.getColumnIndex("abmTy")),
                    c.getString(c.getColumnIndex("rpyTy")),
                    c.getString(c.getColumnIndex("delTy")),
                    c.getString(c.getColumnIndex("frcTy")),
                    c.getString(c.getColumnIndex("mnuTy")),
                    c.getString(c.getColumnIndex("opchtTy")),
                    c.getString(c.getColumnIndex("snglTy")),
                    c.getString(c.getColumnIndex("chtPd")),
                    c.getString(c.getColumnIndex("fl5Pd")),
                    c.getString(c.getColumnIndex("fl10Pd")),
                    c.getString(c.getColumnIndex("mStt")),
                    c.getString(c.getColumnIndex("regDt")),
                    c.getString(c.getColumnIndex("regUsr")),
                    c.getString(c.getColumnIndex("hdn"))
                    );

            arVoMoimList.add(item);
            c.moveToNext();
            //Log.e(TAG,"#item Moim_name"+item.getMmNm()+" Moim_tag: "+item.getMmTg()+" getImgTy: "+item.getImgTy()+" getMmId: "+item.getMmId());

        }
        //Log.e(TAG,"#arVoMoimList "+arVoMoimList.size());

        return arVoMoimList;
    }

    /**
     * 모임 리스트를 삭제
     * 새로운 모임 리스트를 디비에 저장한다.
     */
    private class RefreshMoimListAsync extends AsyncTask<Void, Void, Void> {

        private ArrayList<VoMoimList> receivedMemberList;
        private Context context;
        private SQLiteDatabase wdb;

        public RefreshMoimListAsync(ArrayList<VoMoimList> receivedMemberList, Context context) {

            Log.e(TAG,"#RefreshMoimListAsync receivedMemberList "+receivedMemberList.size() );
            this.receivedMemberList = receivedMemberList;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wdb = getWritableDatabase();
                wdb.beginTransaction();

                //모임 리스트 삭제
                wdb.execSQL("DELETE FROM "+ LocalDB.TB_MOIM_LIST);  //+" WHERE remoteType=1;");


                //모임 목록 db삽입
                for (VoMoimList voMoimList : receivedMemberList) {


                    //"mmId,mmNm,mmTy,mmTg,rmId,imgTy,imgOrg,imgTmb,intr,usrCnt,maxUsr,opnTy,entTy,covTy,acptTy,ivtTy,notiTy,wrtTy,abmTy,rpyTy,delTy,frcTy,mnuTy,opchtTy,snglTy,chtPd,fl5Pd,fl10Pd,mStt,regDt,regUsr,hdn"


                        String query = "INSERT INTO "+ LocalDB.TB_MOIM_LIST+" ( mmId,mmNm,mmTy,mmTg,rmId,imgTy,imgOrg,imgTmb,intr,usrCnt,maxUsr,opnTy,entTy,covTy,acptTy,ivtTy,notiTy,wrtTy,abmTy,rpyTy,delTy,frcTy,mnuTy,opchtTy,snglTy,chtPd,fl5Pd,fl10Pd,mStt,regDt,regUsr,hdn )"
                                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

                    //Log.e(TAG,"#RefreshMoimListAsync query "+voMoimList.getMoim_name());




                    //"mmId,mmNm,mmTy,mmTg,rmId,imgTy,imgOrg,imgTmb,intr,usrCnt,maxUsr,opnTy,
                    // entTy,covTy,acptTy,ivtTy,notiTy,wrtTy,abmTy,rpyTy,delTy,
                    // frcTy,mnuTy,opchtTy,snglTy,chtPd,fl5Pd,fl10Pd,mStt,regDt,regUsr,hdn"

                        wdb.execSQL(query,
                                new Object[]{voMoimList.getMmId(),
                                        voMoimList.getMmNm(),
                                        voMoimList.getMmTy(),
                                        voMoimList.getMmTg(),
                                        voMoimList.getRmId(),
                                        voMoimList.getImgTy(),
                                        voMoimList.getImgOrg(),
                                        voMoimList.getImgTmb(),
                                        voMoimList.getIntr(),
                                        voMoimList.getUsrCnt(),
                                        voMoimList.getMaxUsr(),
                                        voMoimList.getOpnTy(),
                                        voMoimList.getEntTy(),
                                        voMoimList.getCovTy(),
                                        voMoimList.getAcptTy(),
                                        voMoimList.getIvtTy(),
                                        voMoimList.getNotiTy(),
                                        voMoimList.getWrtTy(),
                                        voMoimList.getAbmTy(),
                                        voMoimList.getRpyTy(),
                                        voMoimList.getDelTy(),
                                        voMoimList.getFrcTy(),
                                        voMoimList.getMnuTy(),
                                        voMoimList.getOpchtTy(),
                                        voMoimList.getSnglTy(),
                                        voMoimList.getChtPd(),
                                        voMoimList.getFl5Pd(),
                                        voMoimList.getFl10Pd(),
                                        voMoimList.getmStt(),
                                        voMoimList.getRegDt(),
                                        voMoimList.getRegUsr(),
                                        voMoimList.getHdn()
                                        });
                        //  }

                    }//end of for [add received list to db]


                wdb.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                wdb.endTransaction();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            //test
            ArrayList<VoMoimList> arVoMoimList;
            arVoMoimList = new MoimDbHelper(context).getContactMoimListInDB(context);

            //Log.e(TAG,"#onPostExecute arVoMoimList "+arVoMoimList.size());

            /*if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                WAVEMainActivity.staticMainActivity.mFragmentFriendList.onUpdateFriend();*/
        }
    }

}