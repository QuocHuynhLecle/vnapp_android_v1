package com.wave.messenger.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, LocalDB.DB_NAME, null, LocalDB.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(f_get_create_tableBuddy());
        db.execSQL(f_get_create_tableGroupList());
        db.execSQL(f_get_create_tableChatList());
        db.execSQL(f_get_create_tableChatData());
        db.execSQL(f_get_create_tableUsersRead());
        db.execSQL(f_get_create_tableUsers());
        db.execSQL(f_get_create_tablePhone());
        db.execSQL(f_get_create_tableMoimList());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            dropAllTable();
            db.execSQL(f_get_create_tableBuddy());
            db.execSQL(f_get_create_tableGroupList());
            db.execSQL(f_get_create_tableChatList());
            db.execSQL(f_get_create_tableChatData());
            db.execSQL(f_get_create_tableUsersRead());
            db.execSQL(f_get_create_tableUsers());
            db.execSQL(f_get_create_tablePhone());
            db.execSQL(f_get_create_tableMoimList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean dropTable(String tableName) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("drop table " + tableName + "; ");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public boolean dropAllTable() {
        try {
            dropTable(LocalDB.tb_buddy);
            dropTable(LocalDB.tb_groupList);
            dropTable(LocalDB.tb_chatList);
            dropTable(LocalDB.tb_chatData);
            dropTable(LocalDB.tb_users);
            dropTable(LocalDB.tb_usersRead);
            dropTable(LocalDB.tb_phonebook);
            dropTable(LocalDB.TB_MOIM_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public String f_get_create_tablePhone() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_phonebook).append(" ");
        sb.append("(");
        sb.append(" phonenumber VARCHAR PRIMARY KEY,"
                + " name VARCHAR,"
                + " flag VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableBuddy() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_buddy).append(" ");
        sb.append("(");
        sb.append(" buddy_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " buddy_hidden VARCHAR,"
                + " userId VARCHAR,"
                + " group_id VARCHAR,"
                + " profile_image VARCHAR,"
                + " email VARCHAR,"
                + " buddy_status VARCHAR,"
                + " phone VARCHAR,"
                + " bid VARCHAR,"
                + " buddy_type VARCHAR,"
                + " buddy_blocked VARCHAR,"
                + " profile_subject VARCHAR,"
                + " user_type VARCHAR,"
                + " profile_image_type VARCHAR,"
                + " userName VARCHAR,"
                + " buddy_favorite VARCHAR,"
                + " sort_id INTEGER,"
                + " remoteType INTEGER,"
                + " addDate VARCHAR,"
                + " realUserName VARCHAR,"
                + " iscustomer INTEGER,"
                + " risk_type INTEGER,"
                + " cuser_type VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableGroupList() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_groupList).append(" ");
        sb.append("(");
        sb.append(" group_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " userId VARCHAR,"
                + " gId VARCHAR,"
                + " grpNm VARCHAR,"
                + " rmId VARCHAR,"
                + " grpTy VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableChatList() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_chatList).append(" ");
        sb.append("(");
        sb.append(" chatList_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " ruid VARCHAR,"
                + " ownerId VARCHAR,"
                + " roomSubject VARCHAR,"
                + " roomId VARCHAR,"
                + " roomShortId VARCHAR,"
                + " room_type VARCHAR,"
                + " room_name VARCHAR,"
                + " roomComment VARCHAR,"
                + " room_reg_date VARCHAR,"
                + " room_profile_image VARCHAR,"
                + " change_name VARCHAR,"
                + " last_msg_date VARCHAR,"
                + " title VARCHAR,"
                + " unread VARCHAR,"
                + " useNoti VARCHAR,"
                + " usercount VARCHAR,"
                + " start_cid VARCHAR,"
                + " start_chat_id VARCHAR,"
                + " last_read_cid VARCHAR,"
                + " last_read_chat_id VARCHAR,"
                + " enableSearch VARCHAR,"
                + " enableWriteMsg VARCHAR,"
                + " maxUser VARCHAR,"
                + " hidden VARCHAR,"
                + " subTitle VARCHAR,"
                + " isMembersChat VARCHAR,"
                + " entered VARCHAR,"
                + " staff VARCHAR,"
                + " mmid VARCHAR,"
                + " roomStatus VARCHAR,"
                + " notice_chat_id VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableChatData() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_chatData).append(" ");
        sb.append("(");
        sb.append(" chatData_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " cId VARCHAR,"
                + " chatId VARCHAR,"
                + " ownerId VARCHAR,"
                + " senderName VARCHAR,"
                + " chatType VARCHAR,"
                + " messageType VARCHAR,"
                + " staffMsg VARCHAR,"
                + " title VARCHAR,"
                + " message VARCHAR,"
                + " attachment VARCHAR,"
                + " roomId VARCHAR,"
                + " senderId VARCHAR,"
                + " status INTEGER, "
                + " retryTick INTEGER, "
                + " retryCnt INTEGER, "
                + " attachmentLocal VARCHAR, "
                + " regDate VARCHAR, "
                + " cancel VARCHAR, "
                + " message_more VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableUsersRead() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_usersRead).append(" ");
        sb.append("(");
        sb.append(" usersRead_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " roomId VARCHAR,"
                + " userId VARCHAR,"
                + " tel VARCHAR,"
                + " realUserName VARCHAR,"
                + " start_cid VARCHAR,"
                + " start_chat_id VARCHAR,"
                + " last_read_cid VARCHAR,"
                + " last_read_chat_id VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public String f_get_create_tableUsers() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.tb_users).append(" ");
        sb.append("(");
        sb.append(" users_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " roomId VARCHAR,"
                + " userId VARCHAR,"
                + " userName VARCHAR,"
                + " tel VARCHAR,"
                + " realUserName VARCHAR,"
                + " profile_image VARCHAR,"
                + " profile_image_type VARCHAR,"
                + " profile_subject VARCHAR,"
                + " staff VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    /**
     *
     모임리스트 디비테이블
     {
     mmId,mmNm,mmTy,mmTg,rmId,imgTy,imgOrg,imgTmb,intr,usrCnt,maxUsr,opnTy,entTy,covTy,acptTy,ivtTy,notiTy,wrtTy,abmTy,rpyTy,delTy,frcTy,mnuTy,opchtTy,snglTy,chtPd,fl5Pd,fl10Pd,mStt,regDt,regUsr,hdn"

     ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
     },

     *
     * @return
     */
    public String f_get_create_tableMoimList() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE IF NOT EXISTS ").append(LocalDB.TB_MOIM_LIST).append(" ");
        sb.append("(");
        sb.append(" mlimlist_seq INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + " mmId VARCHAR,"
                + " mmNm VARCHAR,"
                + " mmTy VARCHAR,"
                + " mmTg VARCHAR,"
                + " rmId VARCHAR,"
                + " imgTy VARCHAR,"
                + " imgOrg VARCHAR,"
                + " imgTmb VARCHAR,"
                + " intr VARCHAR,"
                + " usrCnt VARCHAR,"
                + " maxUsr VARCHAR,"
                + " opnTy VARCHAR,"
                + " entTy VARCHAR,"
                + " covTy VARCHAR,"
                + " acptTy VARCHAR,"
                + " ivtTy VARCHAR,"
                + " notiTy VARCHAR,"
                + " wrtTy VARCHAR,"
                + " abmTy VARCHAR,"
                + " rpyTy VARCHAR,"
                + " delTy VARCHAR,"
                + " frcTy VARCHAR,"
                + " mnuTy VARCHAR,"
                + " opchtTy VARCHAR,"
                + " snglTy VARCHAR,"
                + " chtPd VARCHAR,"
                + " fl5Pd VARCHAR,"
                + " fl10Pd VARCHAR,"
                + " mStt VARCHAR,"
                + " regDt VARCHAR,"
                + " regUsr VARCHAR,"
                + " hdn VARCHAR"
        );
        sb.append(");");
        return sb.toString();
    }

    public void deleteFriendData() {
        SQLiteDatabase db = getWritableDatabase();
        dropTable(LocalDB.tb_buddy);

        try {

            db.execSQL(f_get_create_tableBuddy());
        } catch (Exception ignored) {
        }
    }

    public void deleteTotalChatData() {
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL("DELETE FROM tb_chatList");
        } catch (Exception ignored) {
        }

        try {
            db.execSQL("DELETE FROM tb_usersRead");
        } catch (Exception ignored) {
        }

        try {
            db.execSQL("DELETE FROM tb_users");
        } catch (Exception ignored) {
        }
    }
}
