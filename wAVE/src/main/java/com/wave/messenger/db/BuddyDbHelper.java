package com.wave.messenger.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.fragment.Fragment_ChatTab_FriendList;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.mvp.AddFriend.AddFriendModel;
import com.wave.messenger.mvp.AddFriend.AddFriendPresenter;
import com.wave.messenger.mvp.Main.MainPresenter;
import com.wave.messenger.mvp.Profile.IProfileView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.UtilityMethods;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoGroupList;
import com.wave.messenger.vo.VoMyprofessional;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAData;
import moa.android.api.util.json.LBJSONArray;
import moa.android.api.util.json.LBJSONObject;

public class BuddyDbHelper extends DbHelper {
    public BuddyDbHelper(Context context) {
        super(context);
    }

    public boolean deleteBuddy(long buddy_seq) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_buddy WHERE buddy_seq = ? ";
            wdb.execSQL(query, new String[]{String.valueOf(buddy_seq)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public ArrayList<VoFriendList> getUpdatedSearchList(ArrayList<VoFriendList> param, Context context) {
        ArrayList<VoFriendList> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            for (VoFriendList flvo : param) {
                String query = "SELECT * FROM tb_buddy WHERE userName='" + flvo.getUserName() + "'"
                        + " ORDER BY sort_id ASC, userName COLLATE NOCASE, buddy_seq ASC";
                c = db.rawQuery(query, null);
                c.moveToFirst();

                while (!c.isAfterLast()) {
                    String hidden = c.getString(c.getColumnIndex("buddy_hidden"));
                    String blocked = c.getString(c.getColumnIndex("buddy_blocked"));

                    flvo.setBuddyBlocked(blocked);
                    flvo.setBuddyHidden(hidden);

                    if (TextUtils.isEmpty(flvo.getBuddyBlocked())) {
                        flvo.setBuddyBlocked("0");
                    }

                    if (TextUtils.isEmpty(flvo.getBuddyHidden())) {
                        flvo.setBuddyHidden("0");
                    }

                    //ErrorController.showMessage("CHECK : h : " + hidden + ", b : " + blocked);
                    c.moveToNext();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (VoFriendList voFriendList : param.get(0).getList()) {
            if ("0".equals(voFriendList.getBuddyBlocked()) && "0".equals(voFriendList.getBuddyHidden())) {
                list.add(voFriendList);
            } else {
                //ErrorController.showMessage("BLOCKED OR HIDDEN USER : " + voFriendList.getUserName());
            }
        }

        //ErrorController.showMessage("BuddyDB - check : original : " + param.get(0).getList().size() + ", new : " + list.size());

        ArrayList<VoFriendList> resultList = new ArrayList<>();
        VoFriendList result = new VoFriendList();
        result.setUserName(Constants.LISTTYPE_MYFRIEND);
        result.setUserType(Constants.LISTTYPE_MYFRIEND);
        result.setList(list);
        resultList.add(result);

        return resultList;
    }

    public boolean deleteBuddyToPhone(String phone_num) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_buddy WHERE phone_num = ? ";
            wdb.execSQL(query, new String[]{String.valueOf(phone_num)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteBuddyToId(String buddy_id) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_buddy WHERE userId = ? ";
            wdb.execSQL(query, new String[]{String.valueOf(buddy_id)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //내 이름 및 비회원 친구 이름 변경
    public boolean updateBuddyName(String buddy_id, String buddy_newName, IProfileView view) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "UPDATE tb_buddy SET userName='" + buddy_newName + "' WHERE userId='" + buddy_id + "'";
            wdb.execSQL(query);
            view.onChangeName(buddy_newName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /*if(WAVEMainActivity.staticMainActivity.mCURRENT == WAVEMainActivity.CURRENT_FRAG.FRND) {

                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }else if(WAVEMainActivity.staticMainActivity.mCURRENT == WAVEMainActivity.CURRENT_FRAG.MORE){
                Fragment_MoreInform.getInstance().onResume();
            }*/

            if (Fragment_Main.fragmentMain.mCURRENT == Fragment_Main.CURRENT_FRAG.FRND) {
                //  if (Fragment_Main.fragmentMain.mFragmentFriendList != null)
                //Fragment_Main.fragmentMain.mFragmentFriendList.onResume();
                Fragment_ChatTab_FriendList.getInstance().onResume();
            } else if (Fragment_Main.fragmentMain.mCURRENT == Fragment_Main.CURRENT_FRAG.MORE) {
                //
            }
        }
        return true;
    }

    //회원 친구 이름 변경 적용
    public boolean updateBuddyName2(String buddy_id, String buddy_newName, IProfileView view) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "UPDATE tb_buddy SET userName='" + buddy_newName + "' WHERE bid='" + buddy_id + "'";
            wdb.execSQL(query);
            view.onChangeName(buddy_newName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            /*if(WAVEMainActivity.staticMainActivity.mCURRENT == WAVEMainActivity.CURRENT_FRAG.FRND) {

                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }else if(WAVEMainActivity.staticMainActivity.mCURRENT == WAVEMainActivity.CURRENT_FRAG.MORE){
                Fragment_MoreInform.getInstance().onResume();
            }*/
            if (Fragment_Main.fragmentMain.mCURRENT == Fragment_Main.CURRENT_FRAG.FRND) {
                //if (Fragment_Main.fragmentMain.mFragmentFriendList != null)
                //Fragment_Main.fragmentMain.mFragmentFriendList.onResume();
                Fragment_ChatTab_FriendList.getInstance().onResume();
            } else if (Fragment_Main.fragmentMain.mCURRENT == Fragment_Main.CURRENT_FRAG.MORE) {
                //
            }
        }
        return true;
    }

    /**
     * 회원 친구 db 추가
     */
    public void addMemberFriend(String userId, String userName, String tel, Context context) {
        SQLiteDatabase wdb = getWritableDatabase();

        try {
            //ErrorController.showMessage("(db)addMemberFriend val check : userId " + userId + ", userName : " + userName + ", tel : " + tel);

            VoFriendList buddy = new VoFriendList();
            buddy.setBuddyBlocked("0");
            buddy.setBuddyHidden("0");
            buddy.setBuddyFavorite("0");
            buddy.setRemoteType(1);
            buddy.setPhone(tel);
            buddy.setUserId(userId);
            buddy.setUserName(userName);
            buddy.setAddDate(UtilityMethods.getFormattedYearDateDay());

            wdb.beginTransaction();
            String query = "INSERT INTO tb_buddy(buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, sort_id, remoteType, addDate)"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
            int sort = 100;

            wdb.execSQL(query,
                    new Object[]{buddy.getBuddyHidden(), buddy.getUserId(),
                            buddy.getGroupId(), buddy.getProfileImage(),
                            buddy.getEmail(), buddy.getBuddyStatus(),
                            buddy.getPhone(), buddy.getBid(),
                            buddy.getBuddyType(), buddy.getBuddyBlocked(),
                            buddy.getProfileSubject(), buddy.getUserType(),
                            buddy.getProfileImageType(), buddy.getUserName(),
                            buddy.getBuddyFavorite(), sort,
                            buddy.getRemoteType(), buddy.getAddDate()});
            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }

        try {
            //if (Fragment_Main.fragmentMain.mFragmentFriendList != null)
            //Fragment_Main.fragmentMain.mFragmentFriendList.onUpdateFriend();
            Fragment_ChatTab_FriendList.getInstance().onUpdateFriend();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 비회원 친구 db 추가.
     */
    public void addNonMemberFriend(final String userId, final String userName, final String tel, Context context) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();

                try {
                    //ErrorController.showMessage("(db)addNonMemberFriend val check : userId " + userId + ", userName : " + userName + ", tel : " + tel);

                    VoFriendList buddy = new VoFriendList();
                    buddy.setBuddyBlocked("0");
                    buddy.setBuddyHidden("0");
                    buddy.setBuddyFavorite("0");
                    buddy.setRemoteType(0);
                    buddy.setPhone(tel);
                    buddy.setUserId("$$" + buddy.getUnalignedPhone());
                    buddy.setUserName(userName);
                    buddy.setAddDate(UtilityMethods.getFormattedYearDateDay());

                    wdb.beginTransaction();
                    String query = "INSERT INTO tb_buddy(buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, sort_id, remoteType, addDate)"
                            + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                    int sort = 100;

                    wdb.execSQL(query,
                            new Object[]{buddy.getBuddyHidden(), buddy.getUserId(),
                                    buddy.getGroupId(), buddy.getProfileImage(),
                                    buddy.getEmail(), buddy.getBuddyStatus(),
                                    buddy.getPhone(), buddy.getBid(),
                                    buddy.getBuddyType(), buddy.getBuddyBlocked(),
                                    buddy.getProfileSubject(), buddy.getUserType(),
                                    buddy.getProfileImageType(), buddy.getUserName(),
                                    buddy.getBuddyFavorite(), sort,
                                    buddy.getRemoteType(), buddy.getAddDate()});
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                try {
                    //if (Fragment_Main.fragmentMain.mFragmentFriendList != null)
                    //Fragment_Main.fragmentMain.mFragmentFriendList.onUpdateFriend();
                    Fragment_ChatTab_FriendList.getInstance().onUpdateFriend();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    public void searchIfExists2(final Context context, final String userName, final String phone, final AddFriendModel model, final Handler handler, final AddFriendPresenter presenter) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                SQLiteDatabase db = getReadableDatabase();
                try {
                    db.beginTransaction();

                    ErrorController.showMessage("[BuddyDB] searchIfExsist : QUERY NAME : " + userName + ", QUERY PHONE : " + phone);

                    String query = "SELECT * FROM tb_buddy" + " ORDER BY sort_id ASC, userName COLLATE NOCASE, buddy_seq ASC";
                    Cursor c = db.rawQuery(query, null);
                    c.moveToFirst();

                    while (c.isAfterLast() == false) {
                        String tempName = c.getString(c.getColumnIndex("userName"));
                        String tempPhone = c.getString(c.getColumnIndex("phone"));

                        ErrorController.showMessage("[BuddyDB] searchIfExsist : tempName : " + tempName + ", tempPhone : " + tempPhone + ", phone : " + phone);

                        if (phone.equals(tempPhone)) {
                            ErrorController.showMessage("[BuddyDB] searchIfExist - tempPhone equals phone");
                            return true;
                        }

                        c.moveToNext();
                    }
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    db.endTransaction();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    ErrorController.showMessage("[BuddyDB] searchIfExsist : TRUE");
                    presenter.showExists(userName, phone);
                } else {
                    ErrorController.showMessage("[BuddyDB] searchIfExsist : FALSE");
                    model.askFriendType(context, userName, phone, handler);
                }
            }
        }.execute();
    }

    public void searchIfExists(final Context context, final String userName, final String phone, final AddFriendPresenter presenter) {
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                SQLiteDatabase db = getReadableDatabase();
                try {
                    db.beginTransaction();

                    ErrorController.showMessage("[BuddyDB] searchIfExsist : QUERY NAME : " + userName + ", QUERY PHONE : " + phone);

                    String query = "SELECT * FROM tb_buddy" + " ORDER BY sort_id ASC, userName COLLATE NOCASE, buddy_seq ASC";
                    Cursor c = db.rawQuery(query, null);
                    c.moveToFirst();

                    while (c.isAfterLast() == false) {
                        String tempName = c.getString(c.getColumnIndex("userName"));
                        String tempPhone = c.getString(c.getColumnIndex("phone"));

                        if (userName.equals(tempName)) {
                            ErrorController.showMessage("[BuddyDB] searchIfExist - userName equals tempname");
                            return true;
                        }
                        if (phone.equals(tempPhone)) {
                            ErrorController.showMessage("[BuddyDB] searchIfExist - tempPhone equals phone");
                            return true;
                        }

                        c.moveToNext();
                    }
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    db.endTransaction();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    ErrorController.showMessage("[BuddyDB] searchIfExsist : TRUE");
                    presenter.showExists(userName, phone);
                } else {
                    ErrorController.showMessage("[BuddyDB] searchIfExsist : FALSE");
                    presenter.addFriend(context, userName, phone);
                }
            }
        }.execute();
    }

    //새친구 프로필 클릭 시 더 이상 새 친구가 아니게 되야함.
    public void setNotFriendAnymore(Context context, final VoFriendList friendToBeNotNew) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                try {
                    wdb.beginTransaction();
                    wdb.execSQL("UPDATE tb_buddy SET addDate='" + UtilityMethods.getDefaultDateString() + "' WHERE userId='" + friendToBeNotNew.getUserId() + "'");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                LogTrace.E("setNotFriendAnymore");
                //if (Fragment_Main.fragmentMain.mFragmentFriendList != null)
                //Fragment_Main.fragmentMain.mFragmentFriendList.onUpdateFriend();
                Fragment_ChatTab_FriendList.getInstance().onUpdateFriend();
            }
        }.execute();
    }


    public void addBuddys(MOAData data, Context context) {
        try {
            LBJSONArray array = data.body.getArray("params");
            LBJSONArray grpLstArray = data.body.getArray("grpLst");
            LBJSONArray grpUsrsArray = data.body.getArray("grpUsrs");
            LBJSONArray xprtLstArray = data.body.getArray("xprtLst");
            ArrayList<VoFriendList> buddys = new ArrayList<>();
            ArrayList<VoGroupList> groupLists = new ArrayList<>();

            for (int i = 0; i < array.size(); i++) {
                LBJSONObject jo = (LBJSONObject) array.get(i);
                VoFriendList item = VoFriendList.loadFromJsonServerResponse(jo);
                item.setAddDate(UtilityMethods.getFormattedYearDateDay());
                buddys.add(item);
            }

            new RefreshFriendListAsync(buddys, groupLists, context).execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class RefreshFriendListAsync extends AsyncTask<Void, Void, Void> {

        private ArrayList<VoFriendList> receivedMemberList;
        private ArrayList<VoGroupList> receivedGroupList;
        private Context context;
        private SQLiteDatabase wdb;

        public RefreshFriendListAsync(ArrayList<VoFriendList> receivedMemberList, ArrayList<VoGroupList> receivedGroupList, Context context) {
            this.receivedMemberList = receivedMemberList;
            this.receivedGroupList = receivedGroupList;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                wdb = getWritableDatabase();
                wdb.beginTransaction();

                ArrayList<String> newMemberList = getNewMemberList(context);

                //회원 목록 삭제
                wdb.execSQL("DELETE FROM tb_buddy WHERE remoteType=1;");

                //기존 비회원이 회원이 된 경우
                //이 리스트는 삭제된 하나멤버스를 제외한 db상의 친구 목록이다.
                List<VoFriendList> friendsInContactListDB2 = getContactFriendsInDB(context);//개인 db의 친구 목록

                //회원 목록 db삽입
                for (VoFriendList receivedMember : receivedMemberList) {
                    if (!receivedMember.getUserId().equals(MessengerInfo.getUserId(context))) {
                        for (VoFriendList friendInMyDB : friendsInContactListDB2) {
                            //기존 비회원이 회원이 된 경우...
                            if (friendInMyDB.getUserName().equals(receivedMember.getUserName())) {
                                if (friendInMyDB.getPhoneNoBar().equals(receivedMember.getPhoneNoBar())) {
                                    //신규 추가를 위한 플래그 삽입
                                    receivedMember.setAddDate(UtilityMethods.getFormattedYearDateDay());
                                    wdb.execSQL("DELETE FROM tb_buddy WHERE userId='" + friendInMyDB.getUserId() + "';");
                                    break;
                                }
                            }
                        }

                        String query = "INSERT INTO tb_buddy(buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, sort_id, remoteType, addDate, realUserName, iscustomer, risk_type, cuser_type)"
                                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        int sort;

                        if ("1".equals(receivedMember.getBuddyType())) {
                            sort = 1;
                        } else if ("2".equals(receivedMember.getBuddyType())) {
                            sort = 2;
                        } else {
                            sort = 100;
                        }

                        receivedMember.setAddDate(UtilityMethods.getDefaultDateString());
                        for (String newFs : newMemberList) {
                            if (newFs.equals(receivedMember.getUserId())) {
                                receivedMember.setAddDate(UtilityMethods.getFormattedYearDateDay());
                                break;
                            }
                        }

                        wdb.execSQL(query,
                                new Object[]{receivedMember.getBuddyHidden(),
                                        receivedMember.getUserId(), receivedMember.getGroupId(),
                                        receivedMember.getProfileImage(), receivedMember.getEmail(),
                                        receivedMember.getBuddyStatus(), receivedMember.getPhone(),
                                        receivedMember.getBid(), receivedMember.getBuddyType(),
                                        receivedMember.getBuddyBlocked(),
                                        receivedMember.getProfileSubject(),
                                        receivedMember.getUserType(),
                                        receivedMember.getProfileImageType(),
                                        receivedMember.getUserName(),
                                        receivedMember.getBuddyFavorite(), sort,
                                        receivedMember.getRemoteType(), receivedMember.getAddDate(), receivedMember.getRealUserName(), receivedMember.getIscustomer(), receivedMember.getRisk_type(), receivedMember.getCuser_type()});
                        //  }

                    }
                }//end of for [add received list to db]

                wdb.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                wdb.endTransaction();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (Fragment_ChatTab_FriendList.fragmentChatTabFriendList != null)
                Fragment_ChatTab_FriendList.fragmentChatTabFriendList.onUpdateFriend();
        }
    }

    public void addBuddys(ArrayList<VoFriendList> Buddys, Context context) {

        ErrorController.showMessage("addBuddys");
        SQLiteDatabase wdb = getWritableDatabase();
        SQLiteDatabase rdb = getReadableDatabase();

        try {
            wdb.beginTransaction();
            wdb.execSQL("DELETE FROM tb_buddy WHERE remoteType=1;");
            Cursor cursor;

            for (VoFriendList buddy : Buddys) {
                String query = "SELECT * FROM tb_buddy WHERE userName='" + buddy.getUserName() + "'";
                cursor = rdb.rawQuery(query, null);
                cursor.moveToFirst();

                while (cursor.isAfterLast() == false) {
                    String name = cursor.getString(cursor.getColumnIndex("phone"));

                    if (!name.equals(buddy.getPhone())) {
                        query = "DELETE FROM tb_buddy WHERE phone='" + name + "';";
                        ErrorController.showMessage("DELETE : " + buddy.getUserName());
                        wdb.execSQL(query);
                        break;
                    }

                    cursor.moveToNext();
                }

                query = "INSERT INTO tb_buddy(buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, sort_id, remoteType)"
                        + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                int sort;

                if ("1".equals(buddy.getBuddyType())) {
                    sort = 1;
                } else if ("2".equals(buddy.getBuddyType())) {
                    sort = 2;
                } else {
                    sort = 100;
                }

                wdb.execSQL(query,
                        new Object[]{buddy.getBuddyHidden(),
                                buddy.getUserId(), buddy.getGroupId(),
                                buddy.getProfileImage(), buddy.getEmail(),
                                buddy.getBuddyStatus(), buddy.getPhone(),
                                buddy.getBid(), buddy.getBuddyType(),
                                buddy.getBuddyBlocked(),
                                buddy.getProfileSubject(),
                                buddy.getUserType(),
                                buddy.getProfileImageType(),
                                buddy.getUserName(),
                                buddy.getBuddyFavorite(), sort,
                                buddy.getRemoteType()});
            }

            // ///////////////////로컨 연락처와 비교 후 삽입
            List<VoFriendList> contactsPrivate = new MainPresenter(null).getContactList(context);
            List<VoFriendList> entireList = getUnalignedBuddyList(context, new ArrayList<String>());

            String query2;

            ErrorController.showMessage(entireList.size() + "");

            // 추가할꺼 추가하고 없앨거 없엠.
            for (VoFriendList flvo : contactsPrivate) {

                boolean present = false;
                String name = "";
                for (VoFriendList entire : entireList) {

                    for (VoFriendList entire_in : entire.getList()) {
                        // ErrorController.showMessage("BUDDY DB ADD : " + entire_in.getUserName() + ", " + entire_in.getRemoteType());
                        if (entire_in.getRemoteType() == 1) {
                            present = true;
                        }
                        //flvo = 내 개인 주소록
                        //entire_in = 내 db주소록
                        if (flvo.getUnalignedPhone().equals(entire_in.getUnalignedPhone())) {
                            // 로컬 목록에 있고, 전체 목록에도 있다.
                            present = true;
                            name = entire_in.getUserName();
                            break;
                        } else {
                            present = false;
                            name = entire_in.getUserName();
                        }
                    }
                }
                if (present) {
                    // 이미 존재함
                    if (!name.equals(flvo.getUserName())) {
                        query2 = "UPDATE tb_buddy SET userName='" + flvo.getUserName() + "' WHERE phone='" + flvo.getPhone() + "'";
                        ErrorController.showMessage("update");
                        wdb.execSQL(query2);
                    }
                } else {
                    // 전체 리스트에 없으나 개인 연락처에는 있음.
                    boolean isMember = false;

                    for (VoFriendList _vo : entireList) {
                        for (VoFriendList _vo_vo : _vo.getList()) {
                            if (_vo_vo.getUnalignedPhone().equals(flvo.getUnalignedPhone()) && _vo_vo.getRemoteType() == 1) {
                                isMember = true;
                                break;
                            }
                        }
                    }

                    if (!isMember) {
                        ErrorController.showMessage("insert : " + flvo.getUserName() + ", " + flvo.getPhone());

                        query2 = "INSERT INTO tb_buddy(buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, sort_id, remoteType)"
                                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

                        wdb.execSQL(query2,
                                new Object[]{flvo.getBuddyHidden(),
                                        flvo.getUserId(), flvo.getGroupId(),
                                        flvo.getProfileImage(), flvo.getEmail(),
                                        flvo.getBuddyStatus(), flvo.getPhone(),
                                        flvo.getBid(), flvo.getBuddyType(),
                                        flvo.getBuddyBlocked(),
                                        flvo.getProfileSubject(),
                                        flvo.getUserType(),
                                        flvo.getProfileImageType(),
                                        flvo.getUserName(),
                                        flvo.getBuddyFavorite(), 100,
                                        flvo.getRemoteType()});
                    }
                }
                present = false;
                name = "";
            }

            // 없으면 삭제
            for (VoFriendList entire : entireList) {
                boolean present = false;
                for (VoFriendList entire_in : entire.getList()) {
                    if (entire_in.getRemoteType() != 0) {
                        present = true;
                        continue;
                    }

                    for (VoFriendList flvo : contactsPrivate) {
                        if (flvo.getUnalignedPhone().equals(entire_in.getUnalignedPhone())) {
                            // 로컬 목록에 있고, 전체 목록에도 있다.
                            present = true;
                            break;
                        }
                    }

                    if (!present) {
                        query2 = "DELETE FROM tb_buddy WHERE phone='" + entire_in.getPhone() + "'";
                        wdb.execSQL(query2);
                        ErrorController.showMessage("delete " + entire_in.getUserName() + ", " + entire_in.getPhone());
                    }
                    present = false;
                }
            }

            wdb.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wdb.endTransaction();
        }
    }

    public ArrayList<VoFriendList> getBuddyList(Context context, ArrayList<VoGroupList> groupbuddyList, ArrayList<VoGroupList> groupUserbuddyList, ArrayList<String> groupList, ArrayList<VoMyprofessional> myproList, ArrayList<VoMyprofessional> proList, ArrayList<VoGroupList> newGroup, ArrayList<VoGroupList> newGroupUser) {
        ArrayList<VoFriendList> items = new ArrayList<>();

        try {
            groupList.clear();

            // 내프로필
            /*VoFriendList myData = new VoFriendList();
            ArrayList<VoFriendList> tempList = new ArrayList<>();
            VoFriendList vo1 = new VoFriendList();

            vo1.setUserName(MessengerInfo.getUserName(context));
            vo1.setPhone(MessengerInfo.getUserPhoneNumber(context));
            vo1.setUserId(MessengerInfo.getUserId(context));
            vo1.setBuddyType("0");
            vo1.setProfileImage(MessengerInfo.getProfileImage(context));

            tempList.add(vo1);
            myData.setList(tempList);
            myData.setUserName(Constants.LISTTYPE_MYPROFILE);*/


            //내관리고객, 담당직원
            VoFriendList buddyData = new VoFriendList();
            ArrayList<VoFriendList> tempList = new ArrayList<>();
            if (MessengerInfo.getUserType().equals("UT_ST_HQ") || MessengerInfo.getUserType().equals("UT_ME") || MessengerInfo.getUserType().equals("UT_ME_JU")) {
                buddyData.setUserName(Constants.LISTTYPE_MYMANAGER);
            } else {
                buddyData.setUserName(Constants.LISTTYPE_MYCUSTOMER);
            }

            if (groupbuddyList != null && groupbuddyList.size() > 0) {
                buddyData.setList(tempList);
                buddyData.setgId(groupbuddyList.get(0).getgId());
            } else
                buddyData.setList(tempList);

            //미관리고객
            VoFriendList customerData = new VoFriendList();
            tempList = new ArrayList<>();
            customerData.setUserName(Constants.LISTTYPE_CUSTOMER);

            if (MessengerInfo.getUserType().equals("UT_ST")) {
                if (groupbuddyList != null && groupbuddyList.size() > 0) {
                    customerData.setList(tempList);
                    customerData.setgId(groupbuddyList.get(1).getgId());
                } else
                    customerData.setList(tempList);
            }

            // 나의 전문가
            VoFriendList rmdmyData = new VoFriendList();
            tempList = new ArrayList<>();
            if (myproList != null) {
                for (int i = 0; i < myproList.size(); i++) {
                    myproList.get(i).isPro = true;
                    tempList.add(myproList.get(i));
                }
                rmdmyData.setList(tempList);
                rmdmyData.setUserName(Constants.LISTTYPE_MYPROFESSIONALFRIEND);
            }

            // 추천 전문가
            VoFriendList rmdData = new VoFriendList();
            tempList = new ArrayList<>();
            if (proList != null) {
                for (int i = 0; i < proList.size(); i++) {
                    proList.get(i).isPro = true;
                    tempList.add(proList.get(i));
                }
                rmdData.setList(tempList);
                rmdData.setUserName(Constants.LISTTYPE_PROFESSIONALFRIEND);
            }

            //새친구
            VoFriendList newData = new VoFriendList();
            tempList = new ArrayList<>();
            newData.setList(tempList);
            newData.setUserName(Constants.LISTTYPE_NEWFRIEND);

            // 즐겨 찾기
            VoFriendList favData = new VoFriendList();
            tempList = new ArrayList<>();
            favData.setList(tempList);
            favData.setUserName(Constants.LISTTYPE_BOOKMARK);

            // 내 그룹
            ArrayList<VoFriendList> grpList = new ArrayList<>();
            if (newGroup != null) {
                for (int i = 0; i < newGroup.size(); i++) {
                    VoFriendList grpData = new VoFriendList();
                    tempList = new ArrayList<>();

                    grpData.setList(tempList);
                    grpData.setUserName(newGroup.get(i).getGrpNm());
                    grpData.setgId(newGroup.get(i).getgId());
                    grpList.add(grpData);
                }
            }

            // 내친구
            VoFriendList frdData = new VoFriendList();
            tempList = new ArrayList<>();
            frdData.setList(tempList);
            frdData.setUserName(Constants.LISTTYPE_MYFRIEND);

            VoFriendList item;

            SQLiteDatabase db = getReadableDatabase();
            //try {
            String query = "SELECT * FROM tb_buddy where buddy_blocked <> '1' and buddy_hidden <> '1' " + "ORDER BY userName COLLATE LOCALIZED ASC";
            Cursor c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                //LogTrace.E("BuddyDbHelper getBuddyList :" +c.getString(c.getColumnIndex("userName")));
                item = new VoFriendList();
                item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                item.setEmail(c.getString(c.getColumnIndex("email")));
                item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                item.setPhone(c.getString(c.getColumnIndex("phone")));
                item.setBid(c.getString(c.getColumnIndex("bid")));
                item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                item.setUserType(c.getString(c.getColumnIndex("user_type")));
                item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));
                item.setRemoteType(c.getInt(c.getColumnIndex("remoteType")));
                item.setAddDate(c.getString(c.getColumnIndex("addDate")));
                item.setRealUserName(c.getString(c.getColumnIndex("realUserName")));
                item.setIscustomer(c.getInt(c.getColumnIndex("iscustomer")));
                item.setRisk_type(c.getInt(c.getColumnIndex("risk_type")));
                item.setCuser_type(c.getString(c.getColumnIndex("cuser_type")));


                if (groupbuddyList != null) {
                    for (int i = 0; i < groupbuddyList.size(); i++) {
                        if (item.getIscustomer() == 1) {
                            //내관리고객, 담당직원
                            if (groupbuddyList.get(i).getGrpTy().equals("1") || groupbuddyList.get(i).getGrpTy().equals("2")) {
                                for (int j = 0; j < groupUserbuddyList.size(); j++) {
                                    if (groupbuddyList.get(i).getgId().equals(groupUserbuddyList.get(j).getgId())) {
                                        if (groupUserbuddyList.get(j).getBuddyId().equals(item.getUserId())) {
                                            buddyData.getList().add(item);
                                        } else if (groupUserbuddyList.get(j).getBuddyId().equals("")) {
                                            if (groupUserbuddyList.get(j).getbId().equals(item.getBid())) {
                                                buddyData.getList().add(item);
                                            }
                                        }
                                    }
                                }

                            }
                            //미관리 손님
                            else if (groupbuddyList.get(i).getGrpTy().equals("3")) {
                                for (int j = 0; j < groupUserbuddyList.size(); j++) {
                                    if (groupbuddyList.get(i).getgId().equals(groupUserbuddyList.get(j).getgId())) {
                                        if (groupUserbuddyList.get(j).getBuddyId().equals(item.getUserId())) {
                                            customerData.getList().add(item);
                                        } else if (groupUserbuddyList.get(j).getBuddyId().equals("")) {
                                            if (groupUserbuddyList.get(j).getbId().equals(item.getBid())) {
                                                customerData.getList().add(item);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //새로운 친구
                if (item.getAddDate().equals(UtilityMethods.getFormattedYearDateDay())) {
                    newData.getList().add(item);
                }

                //즐찾 친구
                if ("1".equals(item.getBuddyFavorite())) {
                    if (!"1".equals(item.getBuddyType()) && !"2".equals(item.getBuddyType()))
                        favData.getList().add(item);
                }

                //그룹 친구
                if (newGroup != null) {
                    for (int i = 0; i < newGroupUser.size(); i++) {
                        for (int j = 0; j < newGroup.size(); j++) {
                            if (newGroupUser.get(i).getBuddyId().equals(item.getUserId())) {
                                if (newGroup.get(j).getgId().equals(newGroupUser.get(i).getgId()) && item.getIscustomer() == 0) {
                                    grpList.get(j).getList().add(item);
                                }
                            }
                        }
                    }
                }

                //내친구
                if (item.getIscustomer() == 0)
                    frdData.getList().add(item);

                c.moveToNext();
            }

            //미관리손님
            if (MessengerInfo.getUserType().equals("UT_ST")) {
                items.add(customerData);
                groupList.add(Constants.LISTTYPE_CUSTOMER);
            }

            //관리손님, 담당직원
            items.add(buddyData);
            if (MessengerInfo.getUserType().equals("UT_ME") || MessengerInfo.getUserType().equals("UT_ME_JU") || MessengerInfo.getUserType().equals("UT_ST_HQ"))
                groupList.add(Constants.LISTTYPE_MYMANAGER);
            else
                groupList.add(Constants.LISTTYPE_MYCUSTOMER);

            if (rmdmyData.getList().size() >= 0) {
                items.add(rmdmyData);
                groupList.add(Constants.LISTTYPE_MYPROFESSIONALFRIEND);
            }

            if (rmdData.getList().size() >= 0) {
                items.add(rmdData);
                groupList.add(Constants.LISTTYPE_PROFESSIONALFRIEND);
            }

            if (newData.getList().size() > 0) {
                items.add(newData);
                groupList.add(Constants.LISTTYPE_NEWFRIEND);
            }

            if (favData.getList().size() > 0) {
                items.add(favData);
                groupList.add(Constants.LISTTYPE_BOOKMARK);
            }

            if (newGroup != null) {
                items.addAll(grpList);
                for (int i = 0; i < newGroup.size(); i++) {
                    groupList.add(newGroup.get(i).getGrpNm());
                }
            }

            if (frdData.getList().size() > 0) {
                items.add(frdData);
                groupList.add(Constants.LISTTYPE_MYFRIEND);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public ArrayList<VoFriendList> getInviteBuddyList(Context context, ArrayList<String> groupList) {
        ArrayList<VoFriendList> items = new ArrayList<>();

        try {
            groupList.clear();

            // 내프로필
            VoFriendList myData = new VoFriendList();
            ArrayList<VoFriendList> tempList = new ArrayList<>();
            VoFriendList vo1 = new VoFriendList();

            vo1.setUserName(MessengerInfo.getUserName(context));
            vo1.setPhone(MessengerInfo.getUserPhoneNumber(context));
            vo1.setUserId(MessengerInfo.getUserId(context));
            vo1.setBuddyType("0");
            vo1.setProfileImage(MessengerInfo.getProfileImage(context));

            //ErrorController.showMessage("BuddyDB : getBuddyList check user phone : " + MessengerInfo.getUserPhoneNumber(context) + "!");
            tempList.add(vo1);
            myData.setList(tempList);
            myData.setUserName(Constants.LISTTYPE_MYPROFILE);
            // /////

            //새친구
            VoFriendList newData = new VoFriendList();
            tempList = new ArrayList<>();
            newData.setList(tempList);
            newData.setUserName(Constants.LISTTYPE_NEWFRIEND);

            // 즐겨 찾기
            VoFriendList favData = new VoFriendList();
            tempList = new ArrayList<>();
            favData.setList(tempList);
            favData.setUserName(Constants.LISTTYPE_BOOKMARK);
            // /////

            // 내친구
            VoFriendList frdData = new VoFriendList();
            tempList = new ArrayList<>();
            frdData.setList(tempList);
            frdData.setUserName(Constants.LISTTYPE_MYFRIEND);
            // /////

            VoFriendList item;

            SQLiteDatabase db = getReadableDatabase();
            //try {
            String query = "SELECT * FROM tb_buddy where buddy_blocked <> '1' and buddy_hidden <> '1' " + "ORDER BY userName COLLATE LOCALIZED ASC";
            Cursor c = db.rawQuery(query, null);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                //LogTrace.E("BuddyDbHelper getBuddyList :" +c.getString(c.getColumnIndex("userName")));
                item = new VoFriendList();
                item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                item.setEmail(c.getString(c.getColumnIndex("email")));
                item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                item.setPhone(c.getString(c.getColumnIndex("phone")));
                item.setBid(c.getString(c.getColumnIndex("bid")));
                item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                item.setUserType(c.getString(c.getColumnIndex("user_type")));
                item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));
                item.setRemoteType(c.getInt(c.getColumnIndex("remoteType")));
                item.setAddDate(c.getString(c.getColumnIndex("addDate")));
                item.setRealUserName(c.getString(c.getColumnIndex("realUserName")));
                item.setIscustomer(c.getInt(c.getColumnIndex("iscustomer")));
                item.setRisk_type(c.getInt(c.getColumnIndex("risk_type")));
                item.setCuser_type(c.getString(c.getColumnIndex("cuser_type")));

                //내친구
                if (item.getIscustomer() == 0)
                    frdData.getList().add(item);

                //새로운 친구
                /*if (item.getAddDate().equals(UtilityMethods.getFormattedYearDateDay())) {
                    newData.getList().add(item);
                }*/

                //즐찾 친구
                /*if ("1".equals(item.getBuddyFavorite())) {
                    if (!"1".equals(item.getBuddyType()) && !"2".equals(item.getBuddyType()))
                        favData.getList().add(item);
                }*/
//                }
                c.moveToNext();
            }

//            items.add(myData);
//            groupList.add(Constants.LISTTYPE_MYPROFILE);

            if (newData.getList().size() > 0) {
                items.add(newData);
                groupList.add(Constants.LISTTYPE_NEWFRIEND);
            }

            if (favData.getList().size() > 0) {
                items.add(favData);
                groupList.add(Constants.LISTTYPE_BOOKMARK);
            }

            if (frdData.getList().size() > 0) {
                items.add(frdData);
                groupList.add(Constants.LISTTYPE_MYFRIEND);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }


    public ArrayList<String> getNewMemberList(Context context) {
        ArrayList<String> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM tb_buddy WHERE addDate='" + UtilityMethods.getFormattedYearDateDay() + "'";
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (c.isAfterLast() == false) {
            String userId = c.getString(c.getColumnIndex("userId"));
            result.add(userId);
            c.moveToNext();
        }

        return result;
    }


    /**
     * db상의 친구을 모두 돌려준다.
     *
     * @param context
     * @return
     */
    public ArrayList<VoFriendList> getContactFriendsInDB(Context context) {
        ArrayList<VoFriendList> result = new ArrayList<>();
        VoFriendList item;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM tb_buddy" + " ORDER BY sort_id ASC, userName COLLATE NOCASE, buddy_seq ASC";
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while (c.isAfterLast() == false) {
            item = new VoFriendList();
            item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
            item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
            item.setUserId(c.getString(c.getColumnIndex("userId")));
            item.setGroupId(c.getString(c.getColumnIndex("group_id")));
            item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
            item.setEmail(c.getString(c.getColumnIndex("email")));
            item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
            item.setPhone(c.getString(c.getColumnIndex("phone")));
            item.setBid(c.getString(c.getColumnIndex("bid")));
            item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
            item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
            item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
            item.setUserType(c.getString(c.getColumnIndex("user_type")));
            item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
            item.setUserName(c.getString(c.getColumnIndex("userName")));
            item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));
            item.setRemoteType(c.getInt(c.getColumnIndex("remoteType")));
            item.setAddDate(c.getString(c.getColumnIndex("addDate")));

            if (!"1".equals(item.getBuddyType()) && !"2".equals(item.getBuddyType())) {
                result.add(item);
                LogTrace.E("getContact : " + item.getUserId());
            }

            c.moveToNext();
        }
        return result;
    }


    /**
     * 차단,블록된 친구 정보까지 모두
     */
    public ArrayList<VoFriendList> getUnalignedBuddyList(Context context, ArrayList<String> groupList) {
        ArrayList<VoFriendList> items = new ArrayList<>();

        try {
            groupList.clear();
            VoFriendList myData = new VoFriendList();
            ArrayList<VoFriendList> tempList = new ArrayList<>();

            VoFriendList vo1 = new VoFriendList();
            vo1.setUserName(MessengerInfo.getUserName(context));
            vo1.setPhone(MessengerInfo.getUserPhoneNumber(context));
            vo1.setUserId(MessengerInfo.getUserId(context));
            vo1.setBuddyType("0");

            tempList.add(vo1);

            myData.setList(tempList);
            myData.setUserName(Constants.LISTTYPE_MYPROFILE);

            VoFriendList rmdData = new VoFriendList();
            tempList = new ArrayList<>();
            rmdData.setList(tempList);
            rmdData.setUserName(Constants.LISTTYPE_RECOMMENDFRIEND);

            VoFriendList favData = new VoFriendList();
            tempList = new ArrayList<>();
            favData.setList(tempList);
            favData.setUserName(Constants.LISTTYPE_BOOKMARK);

            VoFriendList frdData = new VoFriendList();
            tempList = new ArrayList<>();
            frdData.setList(tempList);
            frdData.setUserName(Constants.LISTTYPE_MYFRIEND);

            VoFriendList item;

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT * FROM tb_buddy" + " ORDER BY sort_id ASC, userName COLLATE NOCASE, buddy_seq ASC";
                c = db.rawQuery(query, null);
                c.moveToFirst();

                while (c.isAfterLast() == false) {
                    item = new VoFriendList();
                    item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                    item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                    item.setUserId(c.getString(c.getColumnIndex("userId")));
                    item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                    item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                    item.setEmail(c.getString(c.getColumnIndex("email")));
                    item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                    item.setPhone(c.getString(c.getColumnIndex("phone")));
                    item.setBid(c.getString(c.getColumnIndex("bid")));
                    item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                    item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                    item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                    item.setUserType(c.getString(c.getColumnIndex("user_type")));
                    item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                    item.setUserName(c.getString(c.getColumnIndex("userName")));
                    item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));
                    item.setRemoteType(c.getInt(c.getColumnIndex("remoteType")));
                    item.setAddDate(c.getString(c.getColumnIndex("addDate")));

                    if ("1".equals(item.getBuddyType()) || "2".equals(item.getBuddyType())) {
                        rmdData.getList().add(item);
                    } else {
                        frdData.getList().add(item);
                    }

                    if ("1".equals(item.getBuddyFavorite())) {
                        if (!"1".equals(item.getBuddyType()) && !"2".equals(item.getBuddyType()))
                            favData.getList().add(item);
                    }

                    c.moveToNext();
                }

                items.add(myData);
                groupList.add(Constants.LISTTYPE_MYPROFILE);

                if (rmdData.getList().size() > 0) {
                    items.add(rmdData);
                    groupList.add(Constants.LISTTYPE_RECOMMENDFRIEND);
                }

                if (favData.getList().size() > 0) {
                    items.add(favData);
                    groupList.add(Constants.LISTTYPE_BOOKMARK);
                }

                if (frdData.getList().size() > 0) {
                    items.add(frdData);
                    groupList.add(Constants.LISTTYPE_MYFRIEND);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public VoFriendList getBuddy(String buddy_id) {
        VoFriendList item = null;

        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT buddy_seq, buddy_hidden, userId, group_id, profile_image, email, buddy_status, phone, bid, buddy_type, buddy_blocked, profile_subject, user_type, profile_image_type, userName, buddy_favorite, risk_type, cuser_type sort_id FROM tb_buddy WHERE userId=?";
                c = db.rawQuery(query, new String[]{String.valueOf(buddy_id)});
                c.moveToFirst();

                while (c.isAfterLast() == false) {
                    item = new VoFriendList();
                    item.setBuddySeq(c.getLong(0));
                    item.setBuddyHidden(c.getString(1));
                    item.setUserId(c.getString(2));
                    item.setGroupId(c.getString(3));
                    item.setProfileImage(c.getString(4));
                    item.setEmail(c.getString(5));
                    item.setBuddyStatus(c.getString(6));
                    item.setPhone(c.getString(7));
                    item.setBid(c.getString(8));
                    item.setBuddyType(c.getString(9));
                    item.setBuddyBlocked(c.getString(10));
                    item.setProfileSubject(c.getString(11));
                    item.setUserType(c.getString(12));
                    item.setProfileImageType(c.getString(13));
                    item.setUserName(c.getString(14));
                    item.setBuddyFavorite(c.getString(15));
                    item.setRisk_type(c.getInt(16));
                    item.setCuser_type(c.getString(17));

                    c.moveToNext();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public boolean existBuddy(String buddy_id) {
        boolean result = false;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT * FROM tb_buddy WHERE userName='" + buddy_id + "'";
                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                int i = 0;

                if (c.isAfterLast() == false) {
                    i++;
                }

                if (i > 0) {
                    result = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public int getBuddyCnt() {
        int result = 0;
        try {
            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;
            try {
                String query = "SELECT count(*) FROM tb_buddy";
                c = db.rawQuery(query, new String[]{});
                c.moveToFirst();

                if (c.isAfterLast() == false) {
                    result = c.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean deleteBuddy(String buddyId) {
        try {
            SQLiteDatabase wdb = getWritableDatabase();
            String query = "DELETE FROM tb_buddy WHERE buddy_id = ? ";
            wdb.execSQL(query, new String[]{buddyId});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean hideBuddy(final String buddyId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();
                try {
                    String query = "UPDATE tb_buddy set buddy_hidden = '1' WHERE userId = ? ";
                    wdb.execSQL(query, new String[]{buddyId});
                    ErrorController.showMessage("[BuddyDb] hideBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

//                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
//                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();

            }
        }.execute();
        return true;
    }

//    /**
//     * @param buddyId
//     * @param callback isolationModel에서 호출시에는 값을, 아닐때는 null.
//     * @return
//     */
    /*public boolean unHideBuddy(final String buddyId, final IUnhideCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();
                try {
                    String query = "UPDATE tb_buddy set buddy_hidden = '0' WHERE userId = ? ";
                    wdb.execSQL(query, new String[]{buddyId});
                    ErrorController.showMessage("[BuddyDb] unHideBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (callback != null) {
                    callback.onUnHideComplete();
                }

                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }
        }.execute();
        return true;
    }*/

    public boolean blockBuddy(final String buddyId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();
                try {
                    String query = "UPDATE tb_buddy set buddy_blocked = '1' WHERE userId = ? ";
                    wdb.execSQL(query, new String[]{buddyId});
                    ErrorController.showMessage("[BuddyDb] blockBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

//                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
//                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }
        }.execute();

        return true;
    }

    /**
     * @param //buddyId
     * @param //callback isolationModel에서 호출시에는 값을, 아닐때는 null.
     * @return
     */
    /*public boolean unblockBuddy(final String buddyId, final IUnBlockCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();

                try {
                    String query = "UPDATE tb_buddy set buddy_blocked = '0' WHERE userId = ? ";
                    wdb.execSQL(query, new String[]{buddyId});
                    ErrorController.showMessage("[BuddyDb] unBlockBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (callback != null) {
                    callback.onUnBlockComplete();
                }

                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }
        }.execute();

        return true;
    }*/

    /*public boolean favoriteBuddy(final String buddyId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();

                try {
                    String query = "UPDATE tb_buddy set buddy_favorite = '1' WHERE userId = ? ";
                    wdb.execSQL(query, new String[]{buddyId});
                    ErrorController.showMessage("[BuddyDb] favoriteBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }
        }.execute();

        return true;
    }*/


    /*public boolean unfavoriteBuddy(final String buddyId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase wdb = getWritableDatabase();
                wdb.beginTransaction();

                try {
                    String query = "UPDATE tb_buddy set buddy_favorite = '0' WHERE userId = '" + buddyId +"'";
                    wdb.execSQL(query);
                    ErrorController.showMessage("[BuddyDb] unfavoriteBuddy complete");
                    wdb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    wdb.endTransaction();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (WAVEMainActivity.staticMainActivity.mFragmentFriendList != null)
                    WAVEMainActivity.staticMainActivity.mFragmentFriendList.onResume();
            }
        }.execute();

        return true;
    }*/
    public ArrayList<VoFriendList> getHiddenBuddy() {
        ArrayList<VoFriendList> items = new ArrayList<>();

        VoFriendList item;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_buddy WHERE buddy_hidden = '1'";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (c.isAfterLast() == false) {
                item = new VoFriendList();
                item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                item.setEmail(c.getString(c.getColumnIndex("email")));
                item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                item.setPhone(c.getString(c.getColumnIndex("phone")));
                item.setBid(c.getString(c.getColumnIndex("bid")));
                item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                item.setUserType(c.getString(c.getColumnIndex("user_type")));
                item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));

                items.add(item);

                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return items;
    }

    public ArrayList<VoFriendList> getBlockBuddy() {
        ArrayList<VoFriendList> items = new ArrayList<>();

        VoFriendList item;

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            String query = "SELECT * FROM tb_buddy WHERE buddy_blocked = '1'";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (c.isAfterLast() == false) {
                item = new VoFriendList();
                item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                item.setUserId(c.getString(c.getColumnIndex("userId")));
                item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                item.setEmail(c.getString(c.getColumnIndex("email")));
                item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                item.setPhone(c.getString(c.getColumnIndex("phone")));
                item.setBid(c.getString(c.getColumnIndex("bid")));
                item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                item.setUserType(c.getString(c.getColumnIndex("user_type")));
                item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                item.setUserName(c.getString(c.getColumnIndex("userName")));
                item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));

                items.add(item);

                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return items;
    }

    public ArrayList<VoFriendList> getHiddenBuddy(Context context, ArrayList<String> groupList) {
        ArrayList<VoFriendList> items = new ArrayList<>();

        try {
            groupList.clear();

            // 내프로필
            VoFriendList myData = new VoFriendList();
            ArrayList<VoFriendList> tempList = new ArrayList<>(); // 내프로필

            VoFriendList vo1 = new VoFriendList();
            vo1.setUserName(MessengerInfo.getUserName(context));
            vo1.setPhone(MessengerInfo.getUserPhoneNumber(context));
            vo1.setBuddyType("0");

            tempList.add(vo1);

            myData.setList(tempList);
            myData.setUserName(Constants.LISTTYPE_MYPROFILE);

            // 추천 친구.
            VoFriendList rmdData = new VoFriendList();
            tempList = new ArrayList<>();
            rmdData.setList(tempList);
            rmdData.setUserName(Constants.LISTTYPE_RECOMMENDFRIEND);

            // 즐겨 찾기.
            VoFriendList favData = new VoFriendList();
            tempList = new ArrayList<>();
            favData.setList(tempList);
            favData.setUserName(Constants.LISTTYPE_BOOKMARK);

            // 내친구
            VoFriendList frdData = new VoFriendList();
            tempList = new ArrayList<>();
            frdData.setList(tempList);
            frdData.setUserName(Constants.LISTTYPE_MYFRIEND);

            VoFriendList item;

            SQLiteDatabase db = getReadableDatabase();
            Cursor c = null;

            try {
                String query = "SELECT * FROM tb_buddy";
                c = db.rawQuery(query, null);
                c.moveToFirst();

                while (c.isAfterLast() == false) {
                    item = new VoFriendList();
                    item.setBuddySeq(c.getLong(c.getColumnIndex("buddy_seq")));
                    item.setBuddyHidden(c.getString(c.getColumnIndex("buddy_hidden")));
                    item.setUserId(c.getString(c.getColumnIndex("userId")));
                    item.setGroupId(c.getString(c.getColumnIndex("group_id")));
                    item.setProfileImage(c.getString(c.getColumnIndex("profile_image")));
                    item.setEmail(c.getString(c.getColumnIndex("email")));
                    item.setBuddyStatus(c.getString(c.getColumnIndex("buddy_status")));
                    item.setPhone(c.getString(c.getColumnIndex("phone")));
                    item.setBid(c.getString(c.getColumnIndex("bid")));
                    item.setBuddyType(c.getString(c.getColumnIndex("buddy_type")));
                    item.setBuddyBlocked(c.getString(c.getColumnIndex("buddy_blocked")));
                    item.setProfileSubject(c.getString(c.getColumnIndex("profile_subject")));
                    item.setUserType(c.getString(c.getColumnIndex("user_type")));
                    item.setProfileImageType(c.getString(c.getColumnIndex("profile_image_type")));
                    item.setUserName(c.getString(c.getColumnIndex("userName")));
                    item.setBuddyFavorite(c.getString(c.getColumnIndex("buddy_favorite")));

                    if ("1".equals(item.getBuddyType())) {
                        rmdData.getList().add(item);
                    } else {
                        frdData.getList().add(item);
                    }

                    c.moveToNext();
                }

                items.add(myData);
                groupList.add(Constants.LISTTYPE_MYPROFILE);

                if (rmdData.getList().size() > 0) {
                    items.add(rmdData);
                    groupList.add(Constants.LISTTYPE_RECOMMENDFRIEND);
                }

                if (favData.getList().size() > 0) {
                    items.add(favData);
                    groupList.add(Constants.LISTTYPE_BOOKMARK);
                }

                if (frdData.getList().size() > 0) {
                    items.add(frdData);
                    groupList.add(Constants.LISTTYPE_MYFRIEND);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return items;
    }

    public String getUserName(String userId) {
        SQLiteDatabase db = getReadableDatabase();
        String name = "";
        Cursor c;

        try {
            String query = "SELECT userName FROM tb_buddy WHERE userId='" + userId + "'";
            c = db.rawQuery(query, null);
            c.moveToFirst();

            while (c.isAfterLast() == false) {
                name = c.getString(c.getColumnIndex("userName"));
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return name;
    }
}