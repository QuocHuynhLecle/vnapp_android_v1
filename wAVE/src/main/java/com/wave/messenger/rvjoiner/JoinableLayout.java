package com.wave.messenger.rvjoiner;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Wraps layout to use in {@link RvJoiner}.
 */
public class JoinableLayout implements RvJoiner.Joinable {

	public interface Callback {

		/**
		 * Runs after layout was inflated (ViewHolderItem created). You can use it to perform extra
         * initialization, such as, for ex. setting onClick listener. NOTE: if you're using this
         * method to save links to child views for later changing values (for example, text in
         * TextView), you should be aware of RecyclerView behavior - it can recycle your layout
         * and recreate it later, and all your changes will be lost, so ensure you also bind your
         * current data in onInflateComplete callback.
		 * @param view just inflated view
		 * @param parent view parent
		 */
		void onInflateComplete(View view, ViewGroup parent);

	}

	private Context mContext;
	private JoinableLayout.Adapter mAdapter;
	private int mItemType = 0;

	/**
	 * @param layoutResId layout resource to inflate view
	 * @param itemType type constant, or 0, or other value if you don't need it
	 * @param callback callback if you want to customize view after inflating
	 * @param stableId unique id if you need to use stable id feature (calls setHasStableIds(true)
	 *                  on adapter automatically), or {@link RecyclerView#NO_ID} if no stable ids
	 */
	public JoinableLayout(@LayoutRes int layoutResId, int itemType, @Nullable Callback callback,
                          long stableId, boolean nEmptyView) {
		//Log.e("JoinableLayout","JoinableLayout(@LayoutRes int layoutResId, int itemType, @Nullable Callback callback,long stableId) {");
		mItemType = itemType;
		mAdapter = new Adapter(layoutResId, itemType, callback, stableId,nEmptyView);
	}

	/**
	 */
	public JoinableLayout(@LayoutRes int layoutResId, int itemType, @Nullable Callback callback) {
		this(layoutResId, itemType, callback, RecyclerView.NO_ID,false);
	}

	/**
	 */
	public JoinableLayout(@LayoutRes int layoutResId, @Nullable Callback callback) {
		this(layoutResId, 0, callback, RecyclerView.NO_ID,false);
	}

	/**
	 *
	 */
	public JoinableLayout(@LayoutRes int layoutResId, int itemType) {
		this(layoutResId, itemType, null, RecyclerView.NO_ID,false);
	}

	public JoinableLayout(@LayoutRes int layoutResId, int itemType, Context context, boolean emptyView){

		this(layoutResId, itemType, null, RecyclerView.NO_ID, emptyView);
		this.mContext=context;
		//Log.e("Adapter","JoinableLayout emptyView "+emptyView);
	}




	/**
	 * Simple constructor if you don't need any customization.
	 * @param layoutResId layout resource to inflate view
	 */
	public JoinableLayout(@LayoutRes int layoutResId) {
		this(layoutResId, 0, null, RecyclerView.NO_ID,false);
	}

	public boolean isVisible() {
		return mAdapter.isVisible();
	}

	public void setVisible(boolean visible) {
		mAdapter.setVisible(visible);
	}

	@Override
	public RecyclerView.Adapter getAdapter() {
		return mAdapter;
	}

	@Override
	public int getTypeCount() {
		return 1;
	}

	@Override
	public int getTypeByIndex(int typeIndex) {
		return mItemType;//doesn't matter index (we have only one type)
	}

	public void visibleEmptyView(Context context){
		mAdapter.visibleEmptyView(context);
	}

	public void inVisibleEmptyView(){
		mAdapter.inVisibleEmptyView();
	}

	private static class Adapter extends RecyclerView.Adapter<Adapter.LayoutVh> {

		private int mLayoutResId;
		private int mItemType;
		private long mStableId;
		private Callback mCallback;
		Context mContext;
		private boolean mVisible = true;
		boolean bEmptyView=false;
		View mView;
		//pass stableId == RecyclerView.NO_ID if stable ids not used
		private Adapter(int layoutResId, int itemType, Callback callback, long stableId,boolean emptyView) {
			//Log.e("Adapter","Adapter(int layoutResId, int itemType, Callback callback, long stableId) {");
			mLayoutResId = layoutResId;
			mItemType = itemType;
			mCallback = callback;
			mStableId = stableId;
			bEmptyView = emptyView;
			setHasStableIds(stableId != RecyclerView.NO_ID);
		}

		//타이틀 뷰 아이디를 가져온다
		public int getLayoutResId(){
			return  mLayoutResId;
		}
		@Override
		public LayoutVh onCreateViewHolder(ViewGroup parent, int viewType) {
			//Log.e("Adapter","onCreateViewHolder bEmptyView "+bEmptyView);

			mContext= parent.getContext();
			mView = LayoutInflater.from(mContext)
					.inflate(mLayoutResId, parent, false);
			if (mCallback != null) {
				mCallback.onInflateComplete(mView, parent);
			}

			if(bEmptyView){	//true  이면 비어있음 뷰를 뵤여준다
				String strID = "ll_empty";
				int resID = mContext.getResources().getIdentifier(strID, "id", mContext.getPackageName());
				mView.findViewById(resID).setVisibility(View.VISIBLE);
			}

			return new LayoutVh(mView);
		}

		/**
		 * 꺼내기,숨기기 이후 비어있음 표시
		 * @param context
         */
		public void visibleEmptyView(Context context) {
			//Log.e("Adapter","Adapter(int layoutResId, int itemType, Callback callback, long stableId) {");
			String strID = "ll_empty"; // empty id
			int resID = context.getResources().getIdentifier(strID, "id", context.getPackageName());
			mView.findViewById(resID).setVisibility(View.VISIBLE);
		}

		/**
		 * 꺼내기,숨기기 이후 비어있음 표시 숨김
		 */
		public void inVisibleEmptyView() {
			String strID = "ll_empty";
			int resID = mContext.getResources().getIdentifier(strID, "id", mContext.getPackageName());
			mView.findViewById(resID).setVisibility(View.GONE);
		}

		@Override
		public void onBindViewHolder(LayoutVh holder, int position) {}

		@Override
		public int getItemCount() {
			return mVisible ? 1 : 0;
		}

		@Override
		public long getItemId(int position) {
			return mStableId;
		}

		@Override
		public int getItemViewType(int position) {
			return mItemType;
		}

		public boolean isVisible() {
			return mVisible;
		}

		public void setVisible(boolean visible) {
			if (mVisible && !visible) {
				mVisible = false;
				notifyItemRemoved(0);
			} else if (!mVisible && visible) {
				mVisible = true;
				notifyItemInserted(0);
			}
		}



		protected static class LayoutVh extends RecyclerView.ViewHolder {

			public LayoutVh(View itemView) {
				super(itemView);
			}

		}

	}

}
