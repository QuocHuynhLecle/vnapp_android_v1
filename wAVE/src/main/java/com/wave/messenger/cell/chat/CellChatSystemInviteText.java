package com.wave.messenger.cell.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CellChatSystemInviteText extends LinearLayout{

	private Context mContext;
	private TextView tvMessage;
	private TextView tvMessageNotMember;
	private LinearLayout linearLayoutTimeLine, ll_message;
	private TextView textViewTimeLine;
	private BuddyDbHelper buddyDb;
	private UsersDbHelper db;
	private String notMemberName="";
	private String text;

	public CellChatSystemInviteText(Context context) {
		super(context);
		mContext = context;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE ); 
		inflater.inflate(R.layout.chat_systeminvite, this, true);
		buddyDb = LocalDB.getBuddyDbHelper(getContext());
		db= LocalDB.getUsersDbHelper(getContext());
		tvMessage = (TextView) findViewById(R.id.tvMessage);
		tvMessageNotMember=(TextView) findViewById(R.id.tvMessageNotMember);
		linearLayoutTimeLine=(LinearLayout)findViewById(R.id.linearLayoutTimeLine);
		textViewTimeLine=(TextView)findViewById(R.id.textViewTimeLine);
		ll_message=(LinearLayout)findViewById(R.id.ll_message);
	}
	
	public void setUI(VoChatData data){
		BuddyDbHelper buddyDb = LocalDB.getBuddyDbHelper(getContext());
		UsersDbHelper db= LocalDB.getUsersDbHelper(getContext());
		if(data.isShowTimeLine()) {
			linearLayoutTimeLine.setVisibility(View.VISIBLE);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
			try {
				Date day1 = format.parse(data.getReg_date().substring(0, 10));
				String time = format2.format(day1);
				textViewTimeLine.setText(time+" "+ Statics.selectDay_of_week(data.getReg_date().substring(0, 10))+"요일");
			} catch (Exception e) {
				textViewTimeLine.setText(data.getReg_date().substring(0, 10));
			}
		}

		if(TextUtils.isEmpty(buddyDb.getUserName(data.getOwnerId()))){
			text = "'"+db.getUserName(data.getOwnerId()) + "' 님이 "+setUserNmae(data)+"을(를) 초대하였습니다.";
		}else{
			text = "'"+buddyDb.getUserName(data.getOwnerId()) + "' 님이 "+setUserNmae(data)+"을(를) 초대하였습니다.";
		}

		if(!TextUtils.isEmpty(notMemberName)) {
			String notMemberText = "(" + notMemberName + "- 비회원 초대)";
			tvMessageNotMember.setVisibility(View.VISIBLE);
			tvMessageNotMember.setText(notMemberText);
		}

		if ("3".equals(data.getChatType())) {
			String name = "";
			try {
				JSONObject jsonObject = new JSONObject(data.getAttachment());
				JSONArray array = new JSONArray(jsonObject.getJSONArray("user").toString());
				JSONObject user = (JSONObject) array.get(0);

				name = user.getString("userName");
			} catch (Exception e) {
				e.printStackTrace();
			}

			tvMessage.setText(mContext.getString(R.string.enter_message, name));
		} else {
			tvMessage.setText(text);
		}
	}

	public void setUIVisible(String visible) {
		ll_message.setVisibility(View.GONE);
	}

	private String setUserNmae(VoChatData data) {
		String name="";
		try {
			JSONObject object = new JSONObject(data.getAttachment());
			JSONArray array = object.getJSONArray("user");

			for(int i=0;i<array.length();i++) {
				JSONObject jo=array.getJSONObject(i);
				String userId=jo.getString("userId");
				if(userId.contains("$$")){
					if (TextUtils.isEmpty(buddyDb.getUserName(userId))) {
						notMemberName+= jo.getString("userName") + ", ";
					} else {
						notMemberName += buddyDb.getUserName(userId) + ", ";
					}

				}else {
					if (TextUtils.isEmpty(buddyDb.getUserName(userId))) {
						name += jo.getString("userName") + ", ";
					} else {
						name += buddyDb.getUserName(userId) + ", ";
					}
				}
			}
			name = name.substring(0, name.length()-2);
			if(!TextUtils.isEmpty(notMemberName)){
				notMemberName = notMemberName.substring(0, name.length()-2);
			}

		}catch (Exception e){
			e.printStackTrace();
		}
		return name;
	}
}
