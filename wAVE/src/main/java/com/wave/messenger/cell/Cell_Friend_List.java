package com.wave.messenger.cell;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

public class Cell_Friend_List extends LinearLayout {
    private ImageView imageViewMember;
    private ImageView imageViewTitle;
    private TextView textViewName;
    private TextView textViewPhoneNumber;
    private LinearLayout llEditLayout;
    private LinearLayout ll_friend;
    private LinearLayout llFListLayout;
    private ImageView ivCheckBox;
    private View vLiner;
    private ImageView btnDoHideAction, btnDoBlockAction;

    private Context mContext;

    public Cell_Friend_List(Context context) {
        super(context);
        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.cell_friendlist, this, true);
        init();
    }

    private void init() {
        //vLiner = (View) findViewById(R.id.vLiner);
        ll_friend = (LinearLayout) findViewById(R.id.ll_friend);
        //imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        textViewName = (TextView) findViewById(R.id.textViewName);

//        llEditLayout = (LinearLayout) findViewById(R.id.llEditLayout);
//        ivCheckBox = (ImageView) findViewById(R.id.ivCheckBox);
//
//        llFListLayout = (LinearLayout) findViewById(R.id.llFListLayout);
    }

    public void noLine(boolean isNoLine) {
        if (isNoLine) {
            vLiner.setVisibility(View.GONE);
        } else {
            vLiner.setVisibility(VISIBLE);
        }
    }

    public void setTextViewName(String name) {
        textViewName.setText(name);
        //imageViewTitle.setImageResource(Statics.setRandomProfile(name));
    }

    public String getTextViwName() {
        return textViewName.getText().toString();
    }

    public void setTextViewPhoneNumber(String phoneNumber) {
        textViewPhoneNumber.setText(phoneNumber);
    }

    public String getTextViewPhoneNumber() {
        return textViewPhoneNumber.getText().toString();
    }

    public void setMember(boolean member) {
        if (member) {
            imageViewMember.setVisibility(View.VISIBLE);
        } else {
            imageViewMember.setVisibility(View.GONE);
        }
    }

    public void setVisibility(boolean flag) {
        if(flag)
            ivCheckBox.setVisibility(View.VISIBLE);
        else
            ivCheckBox.setVisibility(View.GONE);
    }

    public void hideExceptFriend() {
        llFListLayout.setVisibility(View.GONE);
    }

    public void setHideActionListener(OnClickListener listener) {
        this.btnDoHideAction.setOnClickListener(listener);
    }

    public void setBlockActionListener(OnClickListener listener) {
        this.btnDoBlockAction.setOnClickListener(listener);
    }

    public void setChecked(boolean isChecked) {
        ivCheckBox.setSelected(isChecked);
    }

    public boolean isSelected() {
        return ivCheckBox.isSelected();
    }

    public void setOnTime() {
//        imageViewTitle.setImageResource(R.drawable.img_friend_list_onetime_android);
//        textViewPhoneNumber.setText("");
//        textViewPhoneNumber.setBackgroundResource(R.drawable.img_box_list_violet_comments_01_android);
//        textViewPhoneNumber.setVisibility(View.VISIBLE);
//        textViewName.setText("One Time 송금");
//        imageViewMember.setVisibility(View.INVISIBLE);
    }

    public void setHanaMembers() {
        /*//2016106 하나멤버스 프로필 이미지 변경 S
        imageViewTitle.setImageResource(R.drawable.blue_bird_148px);
        //2016106 하나멤버스 프로필 이미지 변경 E
        textViewPhoneNumber.setText("보내요/더치페이 안내");
        textViewPhoneNumber.setTextColor(Color.parseColor("#673bb7"));
        textViewPhoneNumber.setBackgroundResource(R.drawable.img_box_list_violet_comments_android);

        textViewPhoneNumber.setVisibility(View.VISIBLE);
        imageViewMember.setVisibility(View.INVISIBLE);*/
    }

    public void setSelected(boolean flag) {
        /*if(flag)
            ll_friend.setBackgroundColor(getResources().getColor(R.color.bg_selfriend));
        else
            ll_friend.setBackgroundColor(getResources().getColor(R.color.bg_friend));*/
    }

    public void setPhoneVisibility(int visibility) {
        textViewPhoneNumber.setVisibility(visibility);
    }

    /**
     * 프로필 사진을 로딩한다.
     * 회원이 아니라면 없으므로 할 필요가 없다.
     *
     * @param userId
     * @param isMember
     */
    public void loadProfile(String userId, boolean isMember, String userName) {
        /*if (isMember) {
//            new ImageDownloader().execute("3", userId);
            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + userId;

            //Log.e("loadProfile","url: "+url);

            //Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_profile_man).dontAnimate().into(imageViewTitle);
            Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.ic_profile_man).dontAnimate().into(imageViewTitle);
        }*/
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        private String address = Constants.CHATDAWN_PROC + "?";

        public ImageDownloader() {
            super();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
//            try {
//                String fullAddress = address + "type=" + params[0] + "&userid="	+ params[1];
//                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);
//
//                URL url = new URL(fullAddress);
//                URLConnection conn = url.openConnection();
//                conn.connect();
//                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
//                Bitmap image = BitmapFactory.decodeStream(bis);
//                bis.close();
//                ErrorController.showMessage("end of doinBg");
//                Statics.getProfileImageCache().put(params[1], new ThumbnailBitmap(image, false));
//                return image;
//            } catch (Exception e) {
//                ErrorController.showMessage("[ProfileModel] downloadImage : error");
//            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
//            if(result != null) {
//                imageViewTitle.setImageBitmap(result);
//            }else{
//                imageViewTitle.setImageResource(R.drawable.ic_profile_man);
//            }
        }
    }

}
