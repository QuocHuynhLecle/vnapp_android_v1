package com.wave.messenger.cell.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_LongTextView;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.dialog.Dialog_Default;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.MTSItemMaster;
import com.wave.messenger.utility.OpenGraph;
import com.wave.messenger.utility.OpenGraphData;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoItemCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 대화방 LinearLayout
 */
public class CellChatDefaultText extends LinearLayout {

    private static final String TAG = CellChatDefaultText.class.getSimpleName();

    private SpannableString mLinkableText;
    private TextView tvDate;
    private TextView tvDateURL;
    private TextView tvMessage;
    private LinearLayout llMoreText;
    private LinearLayout linearLayoutTimeLine;
    private LinearLayout linearLayoutURLPreview;
    private LinearLayout linearLayoutTime;
    private ImageView imageViewTitle;
    private ImageView imageViewMember;
    private ImageView imageViewURLTitle;
    private TextView tvSender;
    private TextView textViewUnRead;
    private TextView textViewUnReadURL;
    private TextView textViewTimeLine;
    private TextView textViewURLTitle;
    private TextView textViewURLContent;
    private TextView textViewURL;
    private Context context;
    private VoChatData voChatData;
    private LinearLayout linearLayoutFailMsg;
    private ImageView imageViewDelete;
    private ImageView imageViewStatus;
    private boolean isMTS = true;
    public static Map<String, OpenGraphData> cachedWebLinkData = new HashMap<>();
    int mnUrlLayoutHeight = 0;
    private LinearLayout llBubble;
    String longText = "";

    public CellChatDefaultText(Context context, VoChatData data) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (data.getOwnerId().equals(MessengerInfo.getUserId(getContext()))) {//my message
            inflater.inflate(R.layout.chat_defaulttext, this, true);
            initBasic();
            setUIBasic(data);
        } else {//other's message
            inflater.inflate(R.layout.chat_defaulttext_op, this, true);
            init_op();
            setUI_op(data);
        }

        init();
        setUI(data);
    }

    /**
     * 공통된 view를 초기화
     */
    private void init() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDateURL = (TextView) findViewById(R.id.tvDateURL);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        llMoreText = (LinearLayout) findViewById(R.id.llMoreText);
        textViewUnReadURL = (TextView) findViewById(R.id.textViewUnReadURL);
        linearLayoutTimeLine = (LinearLayout) findViewById(R.id.linearLayoutTimeLine);
        linearLayoutURLPreview = (LinearLayout) findViewById(R.id.linearLayoutURLPreview);
        linearLayoutTime = (LinearLayout) findViewById(R.id.linearLayoutTime);
        textViewTimeLine = (TextView) findViewById(R.id.textViewTimeLine);
        imageViewURLTitle = (ImageView) findViewById(R.id.imageViewURLTitle);
        textViewURLTitle = (TextView) findViewById(R.id.textViewURLTitle);
        textViewURLContent = (TextView) findViewById(R.id.textViewURLContent);
        textViewURL = (TextView) findViewById(R.id.textViewURL);
        llBubble = (LinearLayout) findViewById(R.id.llBubble);
    }

    private void initBasic() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        linearLayoutFailMsg = (LinearLayout) findViewById(R.id.linearLayoutFailMsg);
        imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUIBasic(final VoChatData data) {
        //LogTrace.E("data.getStatus : " + data.getStatus() + " / " + data.getMessage());

        if ("400".equals(data.getStatus())) {
            linearLayoutFailMsg.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.GONE);
            textViewUnRead.setVisibility(View.GONE);

        } else {
            linearLayoutFailMsg.setVisibility(View.GONE);
            tvDate.setVisibility(View.VISIBLE);
            textViewUnRead.setVisibility(View.VISIBLE);

            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
        }

        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 삭제하시겠습니까?";

                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);

                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.show();
            }
        });

        imageViewStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 재전송하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        data.setUserName(data.getSenderName());

                        ChatPresenter chatPresenter = new ChatPresenter(null);
                        chatPresenter.sendMessage(getContext(), data);

                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getContext());
                        db.updateClearTryMsg(data.getChatId());

                        data.getListener().reSend(data.getChatId(), data.getCid(), data.getAttachment(), "1");

                        dialogDefault.cancel();

                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();

                    }
                });

                dialogDefault.show();
            }
        });
    }

    /**
     * op에만 있는 뷰 초기화
     */
    private void init_op() {
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        //imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        tvSender = (TextView) findViewById(R.id.tvSender);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUI(final VoChatData data) {

        if (data.getRoomType().equals("7") || data.getRoomType().equals("8")) {
            textViewUnRead.setVisibility(View.GONE);
        }

        ErrorController.showMessage("[CellChatDefaultText] setUI called");
        this.voChatData = data;
        mLinkableText = new SpannableString(data.getMessage());

        linearLayoutURLPreview.setVisibility(View.GONE);

        tvDate.setText(getFormattedTime(data.getReg_date()));

        if (data.isShowTimeLine()) {
            linearLayoutTimeLine.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            try {
                Date day1 = format.parse(data.getReg_date().substring(0, 10));
                String time = format2.format(day1);
                textViewTimeLine.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                textViewTimeLine.setText(data.getReg_date().substring(0, 10));
            }
        } else {
            linearLayoutTimeLine.setVisibility(View.GONE);
        }

//        int font = Statics.getFontState().getFontSize();
        int font = SharedObject.getProperty_int(context, Constants.FONTSIZE, 14);

        tvMessage.setTextSize(font);

        if ("1".equals(data.getMessageMore()) || data.getMessage().length() >= 500) {
            longText = data.getMessage();
            tvMessage.setText(data.getMessage().subSequence(0, 100));
            llMoreText.setVisibility(View.VISIBLE);
        } else {
//            if (data.isMTS()) {
            llMoreText.setVisibility(View.GONE);
            tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
            AsyncTask<Void, Void, Void> mtsDataLoad = new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
//                        tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
                    tvMessage.setText(mLinkableText);

                }

                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        gatherLinksForText();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    tvMessage.setText(mLinkableText);
                }
            };
            try { //TODO test
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    mtsDataLoad.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                else mtsDataLoad.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
            textViewUnReadURL.setVisibility(View.GONE);
        }

        tvMessage.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                if("1".equals(Statics.ROOMINFO.getStaff())) {
//                    Dialog_Common.getStaffChatOptionDialog(context, data).show();
//                } else {
                Dialog_Common.getChatOptionDialog(context, data).show();
//                }
                return true;
            }
        });

        linearLayoutURLPreview.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Dialog_Common.getChatOptionDialog(context, data).show();
                return false;
            }
        });

        if (isURL(data.getMessage())) {
            try {
                String urlString = null;

                if (data.getMessage().contains("http://") || data.getMessage().contains("https://")) {
                    Log.d(TAG, "contains http, https");
                    openGraph(data.getMessage());
                } else {
                    urlString = setUrlString(data.getMessage());
                }
                if (cachedWebLinkData != null && cachedWebLinkData.containsKey(urlString)) {
                    for (String key : cachedWebLinkData.keySet()) {
                        Log.e(TAG, "key : " + key + ", urlString : " + urlString);
                    }
                    Log.e(TAG, "check : " + cachedWebLinkData.size());
                    OpenGraphData resultData = cachedWebLinkData.get(urlString);

                    if (resultData == null) {
                        Log.d(TAG, "OpenGraphData is null");
                        openGraph(urlString);
                        return;
                    }

//                    Log.d(TAG, "Title = " + resultData.getTitle());
//                    Log.d(TAG, "Url = " + resultData.getUrl());
//                    Log.d(TAG, "Domain = " + resultData.getDomain());
//                    Log.d(TAG, "Image = " + resultData.getImage());
//                    Log.d(TAG, "Description = " + resultData.getDescription());

                    linearLayoutURLPreview.setVisibility(View.VISIBLE);
                    linearLayoutURLPreview.setOnClickListener(new UrlOnClickEvent(resultData.getUrl()));
                    Log.d(TAG, "voChatData : " + voChatData.toString());
                    linearLayoutTime.setVisibility(View.GONE);
                    Log.e(TAG, "[CellChatDefaultText] net link called by cache");
                    if (voChatData != null) {
                        if ("0".equals(voChatData.getUnReadCount())) {
                            textViewUnReadURL.setText("");
                        } else {
                            textViewUnReadURL.setText(voChatData.getUnReadCount());
                        }
                        tvDateURL.setText(getFormattedTime(voChatData.getReg_date()));
                    }

                    textViewURLTitle.setText(resultData.getTitle());
                    textViewURLContent.setText(resultData.getDescription());
                    textViewURL.setText(resultData.getDomain());
                    String imageUrl = resultData.getImage();

                    Glide.with(context).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewURLTitle);
                } else {
                    //Check
                    if (cachedWebLinkData == null) {
                        Log.e(TAG, "[CellChatDefaulttext] cachedWebLinkData is null");
                    }
                    openGraph(urlString);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else {
            linearLayoutURLPreview.setVisibility(View.GONE);
        }

        //공통 이벤트 정의
        //1. 500자 이상일 경우..
        llMoreText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Activity_LongTextView.class);
                intent.putExtra("contents", longText);
                intent.putExtra("chatId", data.getChatId());
                intent.putExtra("roomId", data.getRoomId());
                context.startActivity(intent);
            }
        });

    }//end of SetUI.

    private String setUrlString(String data) {
        String urlString = null;
        if (data.contains(".com") || data.contains(".net") || data.contains(".kr") || data.contains(".io") || data.contains("www.")) {
            Log.d(TAG, "no contains http, https");

            urlString = data;

            if ((urlString.contains(".com") || urlString.contains(".net") || urlString.contains(".kr")) && !urlString.contains("www.")) {
                urlString = "http://www." + urlString;
            } else if ((urlString.contains(".com") || urlString.contains(".net") || urlString.contains(".kr")) && urlString.contains("www.")) {
                urlString = "http://" + urlString;
            } else if (urlString.contains(".io")) {
                urlString = "http://" + urlString;
            }
            ErrorController.showMessage("[CellChatDefaultText] before load net link : url check : " + urlString);
        }
        return urlString;
    }


    private class UrlOnClickEvent implements OnClickListener {
        // TODO : url 빼와서 intent 넘기기 수정!!!
        private String urlString;

        private UrlOnClickEvent(String urlString) {
            this.urlString = urlString;
        }

        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                getContext().startActivity(intent);
            } catch (Exception e){
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }

//    private void showUrlInfo(final String urlString) {
//        Log.d(TAG, "showUrlInfo");
//        if (cachedWebLinkData != null && cachedWebLinkData.containsKey(urlString)) {
//            OpenGraphData resultData = cachedWebLinkData.get(urlString);
//
//            if (resultData == null) {
//                openGraph(urlString);
//                return;
//            }
//
//            Log.d(TAG, "******************************************");
//            Log.d(TAG, "Title = " + resultData.getTitle());
//            Log.d(TAG, "Url = " + resultData.getUrl());
//            Log.d(TAG, "Domain = " + resultData.getDomain());
//            Log.d(TAG, "Image = " + resultData.getImage());
//            Log.d(TAG, "Description = " + resultData.getDescription());
//            Log.d(TAG, "******************************************");
//
//            linearLayoutURLPreview.setVisibility(View.VISIBLE);
//            linearLayoutTime.setVisibility(View.GONE);
//            ErrorController.showMessage("[CellChatDefaultText] net link called by cache");
//            if (voChatData != null) {
//                if ("0".equals(voChatData.getUnReadCount())) {
//                    textViewUnReadURL.setText("");
//                } else {
//                    textViewUnReadURL.setText(voChatData.getUnReadCount());
//                }
//                tvDateURL.setText(getFormattedTime(voChatData.getReg_date()));
//            }
//
//            textViewURLTitle.setText(resultData.getTitle());
//            textViewURLContent.setText(resultData.getDescription());
//            textViewURL.setText(resultData.getDomain());
//            String imageUrl = resultData.getImage();
//
//            Glide.with(context).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageViewURLTitle);
//
//        } else {
//            openGraph(urlString);
//        }
//
//        linearLayoutURLPreview.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (urlString.contains("http://") || urlString.contains("https://")) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
//                    getContext().startActivity(intent);
//                } else if (urlString.contains(".com") || urlString.contains("www.")) {
//                    String url = urlString;
//                    if (urlString.contains(".com") && !urlString.contains("www.")) {
//                        url = "http://www." + urlString;
//                    } else if (!urlString.contains(".com") && urlString.contains("www.")) {
//                        url = "http://" + urlString + ".com";
//                    } else if (urlString.contains(".com") && urlString.contains("www.")) {
//                        url = "http://" + urlString;
//                    }
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                    getContext().startActivity(intent);
//                }
//            }
//        });
//    }

    private boolean isURL(String text) {
        if ((text.contains("http://") || text.contains("https://"))
                && (text.contains(".com") || text.contains(".kr") || text.contains(".net") || text.contains(".io"))) {
            return true;
        } else if (text.contains(".com") || text.contains(".kr") || text.contains(".net") || text.contains(".io")) {
            return true;
        } else {
            return false;
        }
    }

    private void openGraph(final String url) {
        Log.d(TAG, "openGraph()");
        new AsyncTask<Object, Void, Object>() {
            @Override
            protected Object doInBackground(Object[] params) {
                OpenGraphData data = new OpenGraphData();
                try {
                    final OpenGraph openGraph = new OpenGraph.Builder(url).build();
                    data = openGraph.getOpenGraph();
                } catch (Exception e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
                return data;
            }

            @Override
            protected void onPostExecute(Object data) {
                super.onPostExecute(data);
                OpenGraphData resultData = (OpenGraphData) data;

                if (resultData == null) {
                    Log.e(TAG, "resultData is null");
                    return;
                }

                if (resultData.getTitle() != null) {
//                    Log.d(TAG, "******************************************");
//                    Log.d(TAG, "Title = " + resultData.getTitle());
//                    Log.d(TAG, "Url = " + resultData.getUrl());
//                    Log.d(TAG, "Domain = " + resultData.getDomain());
//                    Log.d(TAG, "Image = " + resultData.getImage());
//                    Log.d(TAG, "Description = " + resultData.getDescription());
//                    Log.d(TAG, "******************************************");

                    ErrorController.showMessage("[CellChatDefaultText] net link called by async : url Key : " + url);

                    cachedWebLinkData.put(url, resultData);
                    linearLayoutURLPreview.setVisibility(View.VISIBLE);
                    Log.d(TAG, "voChatData : " + voChatData.toString());
                    linearLayoutURLPreview.setOnClickListener(new UrlOnClickEvent(resultData.getUrl()));
                    linearLayoutTime.setVisibility(View.GONE);

                    if (voChatData != null) {
                        if ("0".equals(voChatData.getUnReadCount())) {
                            textViewUnReadURL.setText("");
                        } else {
                            textViewUnReadURL.setText(voChatData.getUnReadCount());
                        }
                        tvDateURL.setText(getFormattedTime(voChatData.getReg_date()));
                    }

                    textViewURLTitle.setText(resultData.getTitle());
                    textViewURLContent.setText(resultData.getDescription());
                    textViewURL.setText(resultData.getDomain());
                    String imageUrl = resultData.getImage();

                    //TODO
                    try {
                        //이미지로딩후 하단으로 스크롤. //XXX 높이값 맞지않음
                        Glide.with(context)
                                .load(imageUrl)
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .skipMemoryCache(false)
                                .into(new BitmapImageViewTarget(imageViewURLTitle) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        super.setResource(resource);

                                        linearLayoutURLPreview.post(new Runnable() {
                                            public void run() {
                                                mnUrlLayoutHeight = linearLayoutURLPreview.getHeight();
                                                voChatData.getPreView().setPreViewHeight(mnUrlLayoutHeight);
                                            }
                                        });

                                    }
                                });

                    } catch (Exception e) {
                        LogTrace.E("onPostExecute Exception " + resultData.getDomain());
                    }
                } else {
                    ErrorController.showMessage("[URL CHEKC ] is null " + resultData.getTitle());
                }
            }
        }.execute();
    }

    public void setUI_op(final VoChatData data) {
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getContext());

        String cuser_type = "";
        String crisk_type = "";
        String user_type = "";
        String risk_type = "";
        try {
            JSONObject obj = new JSONObject(data.getAttachment());
            cuser_type = obj.getString("cuser_type");
            crisk_type = obj.optString("crisk_type");

            if ("UT_AD".equals(MessengerInfo.getUserType()) || "UT_ST".equals(MessengerInfo.getUserType()) || "UT_ST_RE".equals(MessengerInfo.getUserType()) || "UT_ST_CO".equals(MessengerInfo.getUserType()) || "UT_ST_HQ".equals(MessengerInfo.getUserType())) {
                if ("UT_ME_JU".equals(cuser_type))
                    user_type = "(준)";
                else
                    user_type = "";

                if ("0".equals(crisk_type))
                    risk_type = "(투자권유불가)";
                else if ("1".equals(crisk_type))
                    risk_type = "(투자권유가능)";
                else
                    risk_type = "";
            } else {
                user_type = "";
                risk_type = "";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            tvSender.setText(user_type + data.getSenderName() + risk_type);
        } else {
            if (!"".equals(db.getUserName(data.getOwnerId())) && db.getUserName(data.getOwnerId()) != null) {
//                LogTrace.E("CellChatDefaultText user name : " + db.getUserName(data.getOwnerId()));
                tvSender.setText(user_type + db.getUserName(data.getOwnerId()) + risk_type);
            } else {
                LogTrace.E("CellChatDefaultText sender name : " + data.getSenderName());
                tvSender.setText(user_type + data.getSenderName() + risk_type);
            }
        }

        if (data.getOwnerId() != null && !data.getOwnerId().startsWith("$$")) {
            VoFriendList user = LocalDB.getUsersDbHelper(context).getUserInfo(data.getRoomId(), data.getOwnerId());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfileImage();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
            //imageViewMember.setVisibility(GONE);
        } else {
            //imageViewMember.setVisibility(GONE);
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            textViewUnRead.setVisibility(View.GONE);
        } else {
            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        imageViewTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.getOwnerId());
                data.getProfileListener().onClick(v);
            }
        });
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String address;
        private WeakReference<ImageView> ivWeak;

        public ImageDownloader(ImageView iv) {
            super();
            ivWeak = new WeakReference<>(iv);

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = params[0];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[CellChatDefaultText] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null && ivWeak != null) {
                ImageView iv = ivWeak.get();
                iv.setImageBitmap(result);
            } else {
            }
        }

    }

    String tempText = "";

    public void gatherLinksForText() {
        //find(mLinkableText, MTSItemMaster.getInstance().getItemCodes());
    }

    static final String[] hanJosa1 = {"은", "는", "이", "가", "을", "를", "다", "의", "와", "과", "도"
            , "만", "에", "야", "여", "아", "면", "께"
    };

    static final String[] hanJosa2 = {"께서", "에서", "이다"
            , "까지", "부터", "마저", "에서", "에게", "이여", "시여", "보다"
            , "일까", "인가", "여서", "만큼", "니까"
    };

    static final String[] hanJosa3 = {"이니까", "이어서"};

    String tmpLowerText = "";

    public void find(Spannable s, ArrayList<VoItemCode> itemCodeList) {
        tempText = s.toString();
        tmpLowerText = tempText.toLowerCase();
        for (int i = 0; i < itemCodeList.size(); i++) {

            VoItemCode item = itemCodeList.get(i);
            String codeName = itemCodeList.get(i).getName().toLowerCase();

            boolean skipTxt = false;
            int start = tmpLowerText.indexOf(codeName);
            if (start > -1) { //종목명이 있을때

                int end = start + codeName.length();

                //바로앞이 문자가 오면안된다.
                if (start > 0) {
                    Character ch = tmpLowerText.charAt(start - 1);
                    if (Character.isLetter(ch)) {
                        skipTxt = true;
                    }
                }

                if (skipTxt == false && end + 1 <= tmpLowerText.length()) {
                    //뒤에 문자가 있는경우 특수문자(공백) 이거나 한글조사외에는 스킵한다.


                    Character ch = tmpLowerText.charAt(end);
                    if (Character.isLetter(ch)) {
                        //한글 조사 체크.
                        skipTxt = true;

                        String letter1 = String.valueOf(ch);
                        String letter2 = "";
                        String letter3 = "";
                        if (end + 2 <= tmpLowerText.length()) {
                            letter2 = tmpLowerText.substring(end, end + 2);
                        }
                        if (end + 3 <= tmpLowerText.length()) {
                            letter3 = tmpLowerText.substring(end, end + 3);
                        }

                        for (String value : hanJosa1) {
                            if (value.equals(letter1)) {
                                skipTxt = false;
                                break;
                            }
                        }

                        if (skipTxt == true && "".equals(hanJosa2) == false) {
                            for (String value : hanJosa2) {
                                if (value.equals(letter2)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }

                        if (skipTxt == true && "".equals(hanJosa3) == false) {
                            for (String value : hanJosa3) {
                                if (value.equals(letter3)) {
                                    skipTxt = false;
                                    break;
                                }
                            }
                        }

                    }
                }

                if (skipTxt == false) {
                    mLinkableText.setSpan(new InternalURLSpan(s.subSequence(start, end).toString()
                                    , item.getCode(), 2)
                            , start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(getContext(), R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getName()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }

            if (skipTxt) {
                start = tempText.indexOf(item.getCode());
                if (start > -1) { //종목코드가 있을때
                    int end = start + item.getCode().length();
                    mLinkableText.setSpan(new InternalURLSpan(s.subSequence(start, end).toString()
                            , item.getCode(), 2), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(getContext(), R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tempText = tempText.substring(0, start) + replaceText(item.getCode()) + tempText.substring(end, tempText.length());
                    i--;
                    tmpLowerText = tempText.toLowerCase();
                }
            }
        }

        addLinks(s);
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10) {
                        hourString = "" + hour;
                    } else {
                        hourString = hour + "";
                    }
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static final Pattern WEB_URL = Pattern.compile(
            "((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
                    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
                    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
                    + "((?:(?:[" + Patterns.GOOD_IRI_CHAR + "][" + Patterns.GOOD_IRI_CHAR + "\\-]{0,64}\\.)+"   // named host
                    + Patterns.TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL
                    + "|(?:(?:25[0-5]|2[0-4]" // or ip address
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
                    + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9])))"
                    + "(?:\\:\\d{1,5})?)" // plus option port number
                    + "(\\/(?:(?:[" + Patterns.GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
                    + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
                    + "(?:\\b|$)"); // and finally, a word boundary or end of
    // input.  This is to stop foo.sure from
    // matching as foo.su

    public void addLinks(Spannable spannable) {
        // TextView textView

        Linkify.addLinks(spannable, WEB_URL, "");

        URLSpan[] urlSpans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan urlSpan : urlSpans) {
            int start = spannable.getSpanStart(urlSpan);
            int end = spannable.getSpanEnd(urlSpan);
            mLinkableText.removeSpan(urlSpan);
            mLinkableText.setSpan(new InternalURLSpan(urlSpan.getURL(), urlSpan.getURL(), 1), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(getContext(), R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
//        tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
//        tvMessage.setText(mLinkableText);
    }

    public String replaceText(String text) {
        String replaceText = "";
        for (int i = 0; i < text.length(); i++) {
            replaceText = replaceText + "-";
        }
        return replaceText;
    }

    private class InternalURLSpan extends ClickableSpan {
        private String clickedSpan;
        private String itemCode;
        int linkType;

        public InternalURLSpan(String clickedString, String itemCode, int linkType) {
            this.clickedSpan = clickedString;
            this.itemCode = itemCode;
            this.linkType = linkType;
        }

        @Override
        public void onClick(View textView) {
            tvMessage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = itemCode;
                    switch (linkType) {
                        case 1:
                            try {
                                if (url.contains("http://") || url.contains("https://")) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    getContext().startActivity(intent);
                                } else if (url.contains(".com") || url.contains("www.")) {
                                    String urlString = url;
                                    if (urlString.contains(".com") && !urlString.contains("www.")) {
                                        urlString = "http://www." + urlString;
                                    } else if (!urlString.contains(".com") && urlString.contains("www.")) {
                                        urlString = "http://" + urlString;
                                    } else if (urlString.contains(".com") && urlString.contains("www.")) {
                                        urlString = "http://" + urlString;
                                    }
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                                    getContext().startActivity(intent);
                                }
                            } catch (Exception e) {
                                Toast.makeText(getContext(), "링크할 수 없는 url 입니다.", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                }
            });

        }
    }

    public TextView getTextViewContents() {
//        llBubble.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake_animation));
        return tvMessage;
    }

    public LinearLayout getLinearLayoutContents() {
        return llBubble;
    }
}
