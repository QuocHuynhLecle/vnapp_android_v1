package com.wave.messenger.cell;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

public class Cell_Friend_ListTitle extends LinearLayout {
    private LinearLayout wholeGroupLayout;
    private TextView textViewTitle;
    private LinearLayout cellTitleLayout;
    private LinearLayout groupLayout;
    private LinearLayout groupmsg;
    private LinearLayout ll_bottomView;
    private TextView tv_more, tv_no_jongmok;
    private ImageView iv_arrow, iv_groupImage;

    public Cell_Friend_ListTitle(Context context) {
        super(context);
        if (context != null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.cell_friendlisttitle, this, true);
            init();
        }
    }

    private void init() {
        wholeGroupLayout = (LinearLayout) findViewById(R.id.ll_group);
        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
        cellTitleLayout = (LinearLayout) findViewById(R.id.cellTitleLayout);
        groupLayout = (LinearLayout) findViewById(R.id.ll_more);
        groupmsg = (LinearLayout) findViewById(R.id.ll_groupmsg);
        ll_bottomView = (LinearLayout) findViewById(R.id.ll_bottomView);
        tv_more = (TextView) findViewById(R.id.tv_more);
        iv_arrow = (ImageView) findViewById(R.id.iv_arrow);
        tv_no_jongmok = (TextView) findViewById(R.id.tv_no_jongmok);
        iv_groupImage = (ImageView) findViewById(R.id.iv_groupImage);
    }

    public void hideWholeGroupLayout() {
        wholeGroupLayout.setVisibility(GONE);
    }

    public void setTextViewTitle(String name) {
        textViewTitle.setText(name);
    }

    public void setTextViewMore(String name) {
        tv_more.setText(name);
    }

    public String getTextViewTitle() {
        return textViewTitle.getText().toString();
    }

    public void setTextViewSize() {
        textViewTitle.setTextSize(14);
    }

    public void hideLayout() {
//        Statics.getfListState().hideGroupListLayout(cellTitleLayout);
        cellTitleLayout.setVisibility(View.GONE);
        ll_bottomView.setVisibility(View.GONE);
    }

    public void hidejongmok() {
        tv_no_jongmok.setVisibility(View.GONE);
    }

    public void showjongmok() {
        tv_no_jongmok.setVisibility(View.VISIBLE);
    }

    public void hideMoreLayout() {
//        Statics.getfListState().hideGroupListLayout(cellTitleLayout);
        groupLayout.setVisibility(View.GONE);
    }

    public void showMoreLayout() {
        groupLayout.setVisibility(View.VISIBLE);
    }

    public void hideGroupLayout() {
        groupmsg.setVisibility(View.GONE);
    }

    public void setLayoutColor(int layoutColor, int textColor) {
//        cellTitleLayout.setBackgroundColor(Util.getColor(getContext(), layoutColor));
//        textViewTitle.setTextColor(Util.getColor(getContext(), textColor));
//        tv_more.setTextColor(Util.getColor(getContext(), textColor));
//        iv_arrow.setImageResource(R.drawable.ic_arrow_left_white);
    }

    public void setGroupImage(String group) {
        int groupImage = 0;
        switch (group) {
            case Constants.LISTTYPE_MYCUSTOMER:
                groupImage = R.drawable.ic_mycustomer;
                break;
            case Constants.LISTTYPE_MYPROFESSIONALFRIEND:
                groupImage = R.drawable.ic_myprofriend;
                break;
            case Constants.LISTTYPE_PROFESSIONALFRIEND:
                groupImage = R.drawable.ic_plusprofriend;
                break;
            case Constants.LISTTYPE_MYFRIEND:
                groupImage = R.drawable.ic_myfriend;
                break;
        }
        iv_groupImage.setVisibility(VISIBLE);
        iv_groupImage.setImageResource(groupImage);
    }
}
