package com.wave.messenger.cell.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CellChatSystemOutText extends LinearLayout {

	private TextView tvMessage;
	private LinearLayout linearLayoutTimeLine, ll_message;
	private TextView textViewTimeLine;
	private BuddyDbHelper buddyDb;

	public CellChatSystemOutText(Context context) {
		super(context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.chat_systeminvite, this, true);
		buddyDb = LocalDB.getBuddyDbHelper(getContext());
		tvMessage = (TextView) findViewById(R.id.tvMessage);
		linearLayoutTimeLine=(LinearLayout)findViewById(R.id.linearLayoutTimeLine);
		textViewTimeLine=(TextView)findViewById(R.id.textViewTimeLine);
		ll_message=(LinearLayout)findViewById(R.id.ll_message);

	}

	public void setUI(VoChatData data) {
		if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
			String name = "";
			try {
				JSONObject jsonObject = new JSONObject(data.getAttachment());
				JSONArray array = new JSONArray(jsonObject.getJSONArray("user").toString());
				JSONObject user = (JSONObject) array.get(0);

				name = user.getString("userName");
			} catch (Exception e) {
				e.printStackTrace();
			}

			tvMessage.setText(name + " 님이 방을 나가셨습니다.");
		} else {
			tvMessage.setText(setUserNmae(data) + " 님이 방을 나가셨습니다.");
		}


		if(data.isShowTimeLine()) {
			linearLayoutTimeLine.setVisibility(View.VISIBLE);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
			try {
				Date day1 = format.parse(data.getReg_date().substring(0, 10));
				String time = format2.format(day1);
				textViewTimeLine.setText(time+" "+ Statics.selectDay_of_week(data.getReg_date().substring(0, 10))+"요일");
			} catch (Exception e) {
				textViewTimeLine.setText(data.getReg_date().substring(0, 10));
			}
		}
	}
	public void setUIVisible(String visible) {
		ll_message.setVisibility(View.GONE);
	}
	private String setUserNmae(VoChatData data){
		String name="";
		try {
			JSONObject object = new JSONObject(data.getAttachment());
			JSONArray array = object.getJSONArray("user");
			for(int i=0;i<array.length();i++){
				JSONObject jo=array.getJSONObject(i);
				String userId=jo.getString("userId");
				if(TextUtils.isEmpty(buddyDb.getUserName(userId))) {
					name+=jo.getString("userName")+", ";
				}else{
					name+=buddyDb.getUserName(userId)+", ";
				}
			}
			name = name.substring(0, name.length()-2);


		}catch (Exception e){

		}
		return name;
	}
}
