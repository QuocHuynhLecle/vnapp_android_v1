package com.wave.messenger.cell.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_PreviewImage;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.dialog.Dialog_Default;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.wave.massenger.piggy.R.id.textViewUnRead;

public class CellChatImage extends LinearLayout {

    private TextView tvDate;
    private ImageView ivImage;
    private TextView textViewUnread;
    private Context context;
    private TextView textViewTimeLine;
    private LinearLayout linearLayoutTimeLine;
    private ImageView imageViewTitle, imageViewMember;
    private TextView tvSender;
    private LinearLayout linearLayoutFailMsg;
    private ImageView imageViewDelete;
    private ImageView imageViewStatus;

    public CellChatImage(Context context, VoChatData data) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (data.getOwnerId().equals(MessengerInfo.getUserId(getContext()))) {//my message
            inflater.inflate(R.layout.chat_image, this, true);
            initBasic();
            setUIBasic(data);
        } else {//other's message
            inflater.inflate(R.layout.chat_image_op, this, true);
            init_op();
            setUI_op(data);
        }

        tvDate = (TextView) findViewById(R.id.tvDate);
        ivImage = (ImageView) findViewById(R.id.ivImage);

        if (!TextUtils.isEmpty(data.getAttachment())) {
            try {
                JSONObject object = new JSONObject(data.getAttachment());
                String width = object.getString("thumb_width");
                String height = object.getString("thumb_height");
                if (!TextUtils.isEmpty(width) && !TextUtils.isEmpty(height)) {
                    int w = Integer.valueOf(object.getString("thumb_width"));
                    int h = Integer.valueOf(object.getString("thumb_height"));
                    int density = (int) context.getResources().getDisplayMetrics().density;

                    LayoutParams parmas = new LayoutParams(w*density/2, h*density/2);
                    ivImage.setLayoutParams(parmas);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        textViewUnread = (TextView) findViewById(textViewUnRead);
        linearLayoutTimeLine = (LinearLayout) findViewById(R.id.linearLayoutTimeLine);
        textViewTimeLine = (TextView) findViewById(R.id.textViewTimeLine);
        setUI(data);
    }

    private void initBasic() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        linearLayoutFailMsg = (LinearLayout) findViewById(R.id.linearLayoutFailMsg);
        imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        textViewUnread = (TextView) findViewById(textViewUnRead);
    }

    private void setUIBasic(final VoChatData data) {
        if ("400".equals(data.getStatus())) {
            linearLayoutFailMsg.setVisibility(View.VISIBLE);
            textViewUnread.setVisibility(View.GONE);
            tvDate.setVisibility(View.GONE);

        } else {
            linearLayoutFailMsg.setVisibility(View.GONE);
            textViewUnread.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.VISIBLE);

            if ("0".equals(data.getUnReadCount())) {
                textViewUnread.setText("");
            } else {
                textViewUnread.setText(data.getUnReadCount());
            }
        }

        if(!data.isShowUnRead()) {
            textViewUnread.setVisibility(View.GONE);
        }

        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 삭제하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.show();
            }
        });

        imageViewStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 재전송하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        data.setUserName(data.getSenderName());

                        ChatPresenter chatPresenter = new ChatPresenter(null);
                        chatPresenter.sendMessage(getContext(), data);

                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getContext());
                        db.updateClearTryMsg(data.getChatId());

                        data.getListener().reSend(data.getChatId(), data.getCid(), data.getAttachment(), "1");

                        dialogDefault.cancel();
                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();

                    }
                });

                dialogDefault.show();
            }
        });
    }

    private void init_op() {
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        tvSender = (TextView) findViewById(R.id.tvSender);
        textViewUnread = (TextView) findViewById(textViewUnRead);
    }

    public void setUI(final VoChatData data) {
        String attachment = data.getAttachment();

        String url = null;
        if(data.getRoomType().equals("7") || data.getRoomType().equals("8")){
            textViewUnread.setVisibility(View.GONE);
        }
        tvDate.setText(getFormattedTime(data.getReg_date()));
        //TODO: Set Image to ivImage
        if (data.isShowTimeLine()) {
            linearLayoutTimeLine.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            try {
                Date day1 = format.parse(data.getReg_date().substring(0, 10));
                String time = format2.format(day1);
                textViewTimeLine.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                textViewTimeLine.setText(data.getReg_date().substring(0, 10));
            }
        } else {
            linearLayoutTimeLine.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(attachment)) {
            try {
                JSONObject object = new JSONObject(data.getAttachment());
                String path = object.getString("thumb_file");

                if (!TextUtils.isEmpty(path)) {
                    url = Constants.URL_IMAGE_BASE + path;
                }
            } catch (Exception ignored) {
            }
        }

        if (!TextUtils.isEmpty(url)) {
            Glide.with(context).load(url).asBitmap().diskCacheStrategy(DiskCacheStrategy.ALL).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    if (resource != null) {
                        ivImage.setImageBitmap(resource);

                        ivImage.setOnLongClickListener(new OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Dialog_Common.getChatOptionImageDialog(context, data).show();
                                return true;
                            }
                        });

                        ivImage.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                                ArrayList<VoChatData> list = db.getImage(Statics.ROOMINFO.getRoomId());

                                if (list.size() > 0) {

                                    List<AlbumPicture> pictureList = new ArrayList<>();
                                    AlbumPicture albumPicture;

                                    String url = "";
                                    for (VoChatData item : list) {
                                        String path = "";


                                        if (TextUtils.isEmpty(item.getAttachment())) {
                                            url = item.getAttachmentLocal();
                                            albumPicture = new AlbumPicture(url, item.getChatId());
                                            albumPicture.setType("1");
                                        } else {
                                            try {
                                                JSONObject object = new JSONObject(item.getAttachment());
                                                path = object.getString("save_file");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            url = Constants.URL_IMAGE_BASE + path;
                                            albumPicture = new AlbumPicture(url, item.getChatId());
                                        }


                                        pictureList.add(albumPicture);
                                    }

                                    AlbumPicture picture = null;

                                    try {

                                        if (TextUtils.isEmpty(data.getAttachment())) {
                                            picture = new AlbumPicture(data.getAttachmentLocal(), data.getChatId());
                                            picture.setType("1");
                                        } else {

                                            JSONObject object = new JSONObject(data.getAttachment());
                                            picture = new AlbumPicture(Constants.URL_IMAGE_BASE + object.getString("save_file"), data.getChatId());
                                        }

                                    } catch (Exception e) {
                                          e.printStackTrace();
                                    }

                                    Intent intent = new Intent(context, Activity_PreviewImage.class);
                                    intent.putExtra("picture", picture);
                                    intent.putExtra("pictureList", (Serializable) pictureList);
                                    context.startActivity(intent);
                                }
                            }
                        });
                    } else {
                        ivImage.setImageResource(R.drawable.img_picture_default_android);
                    }
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    super.onLoadFailed(e, errorDrawable);
                    ivImage.setImageResource(R.drawable.img_picture_default_android);
                }
            });
        } else {
            LogTrace.E("image url is null");
        }
    }

    public void setUI_op(final VoChatData data) {
        //TODO : Set User Profile and Membership
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getContext());

        String cuser_type = "";
        String crisk_type = "";
        String user_type = "";
        String risk_type = "";
        try {
            JSONObject obj = new JSONObject(data.getAttachment());
            cuser_type = obj.getString("cuser_type");
            crisk_type = obj.optString("crisk_type");

            if ("UT_AD".equals(MessengerInfo.getUserType()) || "UT_ST".equals(MessengerInfo.getUserType()) || "UT_ST_RE".equals(MessengerInfo.getUserType()) || "UT_ST_CO".equals(MessengerInfo.getUserType()) || "UT_ST_HQ".equals(MessengerInfo.getUserType())) {
                if ("UT_ME_JU".equals(cuser_type))
                    user_type = "(준)";
                else
                    user_type = "";

                if ("0".equals(crisk_type))
                    risk_type = "(투자권유불가)";
                else if ("1".equals(crisk_type))
                    risk_type = "(투자권유가능)";
                else
                    risk_type = "";
            } else {
                user_type = "";
                risk_type = "";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            tvSender.setText(user_type + data.getSenderName() + risk_type);
        } else {
            if (!"".equals(db.getUserName(data.getOwnerId())) && db.getUserName(data.getOwnerId()) != null) {
                LogTrace.E("user name : " + db.getUserName(data.getOwnerId()));
                tvSender.setText(user_type + db.getUserName(data.getOwnerId()) + risk_type);
            } else {
                LogTrace.E("sender name : " + data.getSenderName());
                tvSender.setText(user_type + data.getSenderName() + risk_type);
            }
        }

        if(!data.isShowUnRead()) {
            textViewUnread.setVisibility(View.GONE);
        }

        if (data.getOwnerId() != null && !data.getOwnerId().startsWith("$$")) {
            VoFriendList user = LocalDB.getUsersDbHelper(context).getUserInfo(data.getRoomId(), data.getOwnerId());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfileImage();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
            imageViewMember.setVisibility(GONE);

        } else {
            imageViewMember.setVisibility(GONE);
        }

        imageViewTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.getOwnerId());
                data.getProfileListener().onClick(v);
            }
        });
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10) {
                        hourString = "" + hour;
                    } else {
                        hourString = hour + "";
                    }
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }

                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
