package com.wave.messenger.cell;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.utility.ImageLoaderAsync;

public class CellGalleryTitle extends LinearLayout {

	private ImageView ivThumbnail;
	private TextView tvTitle;

	public CellGalleryTitle(Context context, GalleryDataModel data) {
		super(context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.cell_gallerytitle, this, true);
		initView();
		setView(data, context);
	}

	private void initView() {
		ivThumbnail = (ImageView) findViewById(R.id.ivThumbnail);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
	}


	private void setView(GalleryDataModel data, Context context) {
		if(data.getFullPath()!=null){
			new ImageLoaderAsync(ivThumbnail).execute(data.getFullPath());
		}
		tvTitle.setText(data.getDirectoryName());
	}
	
}
