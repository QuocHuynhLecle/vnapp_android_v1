package com.wave.messenger.cell.chat;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Default;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.wave.massenger.piggy.R.id.textViewUnRead;

public class CellChatSignal extends LinearLayout {

    private TextView tvDate;
    private ImageView ivImage;
    private TextView textViewUnread;
    private Context context;
    private TextView textViewTimeLine;
    private LinearLayout linearLayoutTimeLine;
    private ImageView imageViewTitle, imageViewMember;
    private TextView tvSender;
    private LinearLayout linearLayoutFailMsg;
    private ImageView imageViewDelete;
    private ImageView imageViewStatus;
    private TextView tvItemName, tvSg, tvPv, tvCv, tvTp;
    private Button ivMTS;
    private String stnm, stcd, stsg, stpv, stcv, sttp;

    public CellChatSignal(Context context, VoChatData data) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (data.getOwnerId().equals(MessengerInfo.getUserId(getContext()))) {//my message
            inflater.inflate(R.layout.chat_signal, this, true);
            initBasic();
            setUIBasic(data);
        } else {//other's message
            inflater.inflate(R.layout.chat_signal_op, this, true);
            init_op();
            setUI_op(data);
        }

        tvDate = (TextView) findViewById(R.id.tvDate);

        textViewUnread = (TextView) findViewById(textViewUnRead);
        linearLayoutTimeLine = (LinearLayout) findViewById(R.id.linearLayoutTimeLine);
        textViewTimeLine = (TextView) findViewById(R.id.textViewTimeLine);
        tvItemName = (TextView) findViewById(R.id.tvItemName);
        tvSg = (TextView) findViewById(R.id.tvSg);
        tvPv = (TextView) findViewById(R.id.tvPv);
        tvCv = (TextView) findViewById(R.id.tvCv);
        tvTp = (TextView) findViewById(R.id.tvTp);
        ivMTS = (Button) findViewById(R.id.ivMTS);
//        btnBuy = (Button) findViewById(R.id.btnBuy);
//        btnSell = (Button) findViewById(R.id.btnSell);
//        btnOk = (Button) findViewById(R.id.btnOk);
//        btnCancle = (Button) findViewById(R.id.btnCancle);
        setUI(data);
    }

    private void initBasic() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        linearLayoutFailMsg = (LinearLayout) findViewById(R.id.linearLayoutFailMsg);
        imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        textViewUnread = (TextView) findViewById(textViewUnRead);
    }

    private void setUIBasic(final VoChatData data) {
        if ("400".equals(data.getStatus())) {
            linearLayoutFailMsg.setVisibility(View.VISIBLE);
            textViewUnread.setVisibility(View.GONE);
            tvDate.setVisibility(View.GONE);

        } else {
            linearLayoutFailMsg.setVisibility(View.GONE);
            textViewUnread.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.VISIBLE);

            if ("0".equals(data.getUnReadCount())) {
                textViewUnread.setText("");
            } else {
                textViewUnread.setText(data.getUnReadCount());
            }
        }

        if(!data.isShowUnRead()) {
            textViewUnread.setVisibility(View.GONE);
        }

        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 삭제하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });
                dialogDefault.show();
            }
        });

        imageViewStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 재전송하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        data.setUserName(data.getSenderName());

                        ChatPresenter chatPresenter = new ChatPresenter(null);
                        chatPresenter.sendMessage(getContext(), data);

                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getContext());
                        db.updateClearTryMsg(data.getChatId());

                        data.getListener().reSend(data.getChatId(), data.getCid(), data.getAttachment(), "1");

                        dialogDefault.cancel();
                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();

                    }
                });

                dialogDefault.show();
            }
        });
    }

    private void init_op() {
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        tvSender = (TextView) findViewById(R.id.tvSender);
        textViewUnread = (TextView) findViewById(textViewUnRead);
    }

    public void setUI(final VoChatData data) {
        String attachment = data.getAttachment();

        String url = null;
        if(data.getRoomType().equals("7") || data.getRoomType().equals("8")){
            textViewUnread.setVisibility(View.GONE);
        }
        tvDate.setText(getFormattedTime(data.getReg_date()));
        //TODO: Set Image to ivImage
        if (data.isShowTimeLine()) {
            linearLayoutTimeLine.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            try {
                Date day1 = format.parse(data.getReg_date().substring(0, 10));
                String time = format2.format(day1);
                textViewTimeLine.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                textViewTimeLine.setText(data.getReg_date().substring(0, 10));
            }
        } else {
            linearLayoutTimeLine.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(attachment)) {
            //종목명, 종목코드, 신호가, 목표가, 손절가, 매수/매도 = 0:매수 /1:매도
            try {
                JSONObject object = new JSONObject(data.getAttachment());
                LogTrace.E("종목명 : " + stnm);
                LogTrace.E("종목코드 : " + stcd);
                LogTrace.E("신호가 : " + stsg);
                LogTrace.E("목표가 : " + stpv);
                LogTrace.E("손절가 : " + stcv);
                LogTrace.E("매수,매도 : " + sttp);

                stnm = object.getString("stnm");
                stcd = object.getString("stcd");
                stsg = object.getString("stsg");
                stpv = object.getString("stpv");
                stcv = object.getString("stcv");
                sttp = object.getString("sttp");

                if (object != null) {
                    tvItemName.setText(stnm);
                    tvSg.setText(stsg);
                    tvPv.setText(stpv);
                    tvCv.setText(stcv);
                    if (sttp.equals("0")) {
                        tvTp.setText("매수");
                        tvTp.setTextColor(Util.getColor(getContext(), R.color.red));
                        ivMTS.setText("매수창으로 Go");
                        ivMTS.setBackgroundResource(R.drawable.ic_signal_goto_buying);
                    } else if (sttp.equals("1")) {
                        tvTp.setText("매도");
                        tvTp.setTextColor(Util.getColor(getContext(), R.color.blue));
                        ivMTS.setText("매도창으로 Go");
                        ivMTS.setBackgroundResource(R.drawable.ic_signal_goto_sell);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setUI_op(final VoChatData data) {
        //TODO : Set User Profile and Membership
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getContext());

        String cuser_type = "";
        String crisk_type = "";
        String user_type = "";
        String risk_type = "";
        try {
            JSONObject obj = new JSONObject(data.getAttachment());
            cuser_type = obj.getString("cuser_type");
            crisk_type = obj.optString("crisk_type");

            if ("UT_AD".equals(MessengerInfo.getUserType()) || "UT_ST".equals(MessengerInfo.getUserType()) || "UT_ST_RE".equals(MessengerInfo.getUserType()) || "UT_ST_CO".equals(MessengerInfo.getUserType()) || "UT_ST_HQ".equals(MessengerInfo.getUserType())) {
                if ("UT_ME_JU".equals(cuser_type))
                    user_type = "(준)";
                else
                    user_type = "";

                if ("0".equals(crisk_type))
                    risk_type = "(투자권유불가)";
                else if ("1".equals(crisk_type))
                    risk_type = "(투자권유가능)";
                else
                    risk_type = "";
            } else {
                user_type = "";
                risk_type = "";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            tvSender.setText(user_type + data.getSenderName() + risk_type);
        } else {
            if (!"".equals(db.getUserName(data.getOwnerId())) && db.getUserName(data.getOwnerId()) != null) {
                LogTrace.E("user name : " + db.getUserName(data.getOwnerId()));
                tvSender.setText(user_type + db.getUserName(data.getOwnerId()) + risk_type);
            } else {
                LogTrace.E("sender name : " + data.getSenderName());
                tvSender.setText(user_type + data.getSenderName() + risk_type);
            }
        }

        if(!data.isShowUnRead()) {
            textViewUnread.setVisibility(View.GONE);
        }

        if (data.getOwnerId() != null && !data.getOwnerId().startsWith("$$")) {
            VoFriendList user = LocalDB.getUsersDbHelper(context).getUserInfo(data.getRoomId(), data.getOwnerId());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfileImage();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
            imageViewMember.setVisibility(GONE);

        } else {
            imageViewMember.setVisibility(GONE);
        }

        imageViewTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.getOwnerId());
                data.getProfileListener().onClick(v);
            }
        });
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10) {
                        hourString = "" + hour;
                    } else {
                        hourString = hour + "";
                    }
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }

                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
