package com.wave.messenger.cell.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aveapp on 2017. 2. 28..
 */

public class CellChatOpen extends LinearLayout {

    Context mContext;

    SimpleDateFormat convertFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
    SimpleDateFormat showFormat = new SimpleDateFormat("yyyy년 MM월 dd일", Locale.KOREA);

    LinearLayout ll_timeline;
    TextView tv_timeline, tv_msg;
    String action = "";
    String value = "";

    public CellChatOpen(Context context, VoChatData data) {
        super(context);
        mContext = context;
        init(data);
    }

    private void init(VoChatData data) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = mInflater.inflate(R.layout.chat_open, this, true);

        ll_timeline = (LinearLayout) v.findViewById(R.id.ll_timeline);
        tv_timeline = (TextView) v.findViewById(R.id.tv_timeline);
        tv_msg = (TextView) v.findViewById(R.id.tv_msg);

        setData(data);
    }

    private void setData(VoChatData data) {
        if(data.isShowTimeLine()) {
            ll_timeline.setVisibility(View.VISIBLE);

            try {
                Date day = convertFormat.parse(data.getReg_date().substring(0, 10));
                String time = showFormat.format(day);

                tv_timeline.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ll_timeline.setVisibility(View.GONE);
        }

        try {
            JSONObject object = new JSONObject(data.getAttachment());
            action = object.getString("code");
            value = object.getString("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if ("0".equals(action)) {
            if("".equals(data.getAttachment())) {
                tv_msg.setText(mContext.getString(R.string.clean_msg));
            } else {
                tv_msg.setText(mContext.getString(R.string.shake_msg));
            }
        } else if ("voice".equals(action)) {
            if ("1".equals(value)) {
                tv_msg.setText(mContext.getString(R.string.audioon_msg));
            } else if ("0".equals(value)) {
                tv_msg.setText(mContext.getString(R.string.audiooff_msg));
            }
        }


    }
}
