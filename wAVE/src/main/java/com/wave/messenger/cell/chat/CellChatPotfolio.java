package com.wave.messenger.cell.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.adapter.PortfolioAdapter;
import com.wave.messenger.candleman.MessengerInterface;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.dialog.Dialog_Default;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoPortfolioList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.wave.messenger.activity.Activity_ChatRoom.aveContext;

/**
 * 대화방 LinearLayout
 */
public class CellChatPotfolio extends LinearLayout {
    private SpannableString mLinkableText;
    private TextView tvDate;
    private TextView tvDateURL;
    private TextView tvMessage;
    private LinearLayout llMoreText;
    private LinearLayout linearLayoutTimeLine;
    private LinearLayout linearLayoutURLPreview;
    private LinearLayout linearLayoutTime;
    private ImageView imageViewTitle;
    private ImageView imageViewMember;
    private ImageView imageViewURLTitle;
    private TextView tvSender;
    private TextView textViewUnRead;
    private TextView textViewUnReadURL;
    private TextView textViewTimeLine;
    private TextView textViewURLTitle;
    private TextView textViewURLContent;
    private TextView textViewURL;
    private TextView tvPortfolio;
    private Context context;
    private VoChatData voChatData;
    private LinearLayout linearLayoutFailMsg;
    private ImageView imageViewDelete;
    private ImageView imageViewStatus;
    private RecyclerView lv_portfolio;
    private PortfolioAdapter adapter;
    private LinearLayout ll_portfolio, ll_portfolio_more;
    private TextView tv_ptcount;
    private View ll_portfolio_default;
    private List<VoPortfolioList> portfolioList;

    public CellChatPotfolio(Context context, VoChatData data) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (data.getOwnerId().equals(MessengerInfo.getUserId(getContext()))) {//my message
            inflater.inflate(R.layout.chat_potfolio, this, true);
            initBasic();
            setUIBasic(data);
        } else {//other's message
            inflater.inflate(R.layout.chat_potfolio_op, this, true);
            init_op();
            setUI_op(data);
        }

        init(data);
        setUI(data);
    }

    /**
     * 공통된 view를 초기화
     */
    private void init(VoChatData data) {
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDateURL = (TextView) findViewById(R.id.tvDateURL);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvPortfolio = (TextView) findViewById(R.id.tvPortfoliomore);
        llMoreText = (LinearLayout) findViewById(R.id.llMoreText);
        textViewUnReadURL = (TextView) findViewById(R.id.textViewUnReadURL);
        linearLayoutTimeLine = (LinearLayout) findViewById(R.id.linearLayoutTimeLine);
//        linearLayoutURLPreview = (LinearLayout) findViewById(R.id.linearLayoutURLPreview);
        linearLayoutTime = (LinearLayout) findViewById(R.id.linearLayoutTime);
        textViewTimeLine = (TextView) findViewById(R.id.textViewTimeLine);
        imageViewURLTitle = (ImageView) findViewById(R.id.imageViewURLTitle);
        textViewURLTitle = (TextView) findViewById(R.id.textViewURLTitle);
        textViewURLContent = (TextView) findViewById(R.id.textViewURLContent);
        textViewURL = (TextView) findViewById(R.id.textViewURL);
        tv_ptcount = (TextView) findViewById(R.id.tv_ptcount);

        ll_portfolio = (LinearLayout) findViewById(R.id.ll_portfolio);
        ll_portfolio_more = (LinearLayout) findViewById(R.id.ll_portfolio_more);
        ll_portfolio_default = (View) findViewById(R.id.ll_portfolio_default);
        lv_portfolio = (RecyclerView) findViewById(R.id.lv_portfolio);
        adapter = new PortfolioAdapter(context);
        if ("15".equals(data.getMessageType())) {
            lv_portfolio.setLayoutManager(new LinearLayoutManager(context));
            lv_portfolio.setAdapter(adapter);
            setListView(data.getAttachment());
        } else {
            lv_portfolio.setVisibility(View.GONE);
            ll_portfolio.setVisibility(View.GONE);
        }
    }

    private void initBasic() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        linearLayoutFailMsg = (LinearLayout) findViewById(R.id.linearLayoutFailMsg);
        imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUIBasic(final VoChatData data) {
        LogTrace.E("data.getStatus : " + data.getStatus() + " / " + data.getMessage());

        if ("400".equals(data.getStatus())) {
            linearLayoutFailMsg.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.GONE);
            textViewUnRead.setVisibility(View.GONE);

        } else {
            linearLayoutFailMsg.setVisibility(View.GONE);
            tvDate.setVisibility(View.VISIBLE);
            textViewUnRead.setVisibility(View.VISIBLE);

            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
        }

        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 삭제하시겠습니까?";

                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);

                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.show();
            }
        });

        imageViewStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 재전송하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        data.setUserName(data.getSenderName());

                        ChatPresenter chatPresenter = new ChatPresenter(null);
                        chatPresenter.sendMessage(getContext(), data);

                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getContext());
                        db.updateClearTryMsg(data.getChatId());

                        data.getListener().reSend(data.getChatId(), data.getCid(), data.getAttachment(), "1");

                        dialogDefault.cancel();

                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();

                    }
                });

                dialogDefault.show();
            }
        });
    }

    /**
     * op에만 있는 뷰 초기화
     */
    private void init_op() {
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        //imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        tvSender = (TextView) findViewById(R.id.tvSender);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUI(final VoChatData data) {
        if(data.getRoomType().equals("7") || data.getRoomType().equals("8")){
            textViewUnRead.setVisibility(View.GONE);
        }
        ErrorController.showMessage("[CellChatDefaultText] setUI called");
        this.voChatData = data;
        mLinkableText = new SpannableString(data.getMessage());

//        linearLayoutURLPreview.setVisibility(View.GONE);

        tvDate.setText(getFormattedTime(data.getReg_date()));

        if (data.isShowTimeLine()) {
            linearLayoutTimeLine.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            try {
                Date day1 = format.parse(data.getReg_date().substring(0, 10));
                String time = format2.format(day1);
                textViewTimeLine.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                textViewTimeLine.setText(data.getReg_date().substring(0, 10));
            }
        } else {
            linearLayoutTimeLine.setVisibility(View.GONE);
        }

//        int pont = Statics.getFontState().getFontSize();
        int font = SharedObject.getProperty_int(context, Constants.FONTSIZE, 15);

        tvMessage.setTextSize(font);
        llMoreText.setVisibility(View.GONE);
        tvMessage.setText(data.getMessage());

        SpannableString content = new SpannableString("- 자세히 보기");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvPortfolio.setText(content);

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
            textViewUnReadURL.setVisibility(View.GONE);
        }

        tvMessage.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Dialog_Common.getChatOptionDialog(context, data).show();
                return true;
            }
        });

        tvPortfolio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                portfolioMenu();
            }
        });

        lv_portfolio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                portfolioMenu();
            }
        });

    }//end of SetUI.

    public void setUI_op(final VoChatData data) {
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getContext());

        String cuser_type = "";
        String crisk_type = "";
        String user_type = "";
        String risk_type = "";
        try {
            JSONObject obj = new JSONObject(data.getAttachment());
            cuser_type = obj.getString("cuser_type");
            crisk_type = obj.optString("crisk_type");

            if ("UT_ST".equals(MessengerInfo.getUserType()) || "UT_ST_HQ".equals(MessengerInfo.getUserType())) {
                if ("UT_ME_JU".equals(cuser_type))
                    user_type = "(준)";
                else
                    user_type = "";

                if ("0".equals(crisk_type))
                    risk_type = "(투자권유불가)";
                else if ("1".equals(crisk_type))
                    risk_type = "(투자권유가능)";
                else
                    risk_type = "";
            } else {
                user_type = "";
                risk_type = "";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            tvSender.setText(user_type + data.getSenderName() + risk_type);
        } else {
            if (!"".equals(db.getUserName(data.getOwnerId())) && db.getUserName(data.getOwnerId()) != null) {
                LogTrace.E("user name : " + db.getUserName(data.getOwnerId()));
                tvSender.setText(user_type + db.getUserName(data.getOwnerId()) + risk_type);
            } else {
                LogTrace.E("sender name : " + data.getSenderName());
                tvSender.setText(user_type + data.getSenderName() + risk_type);
            }
        }

        if (data.getOwnerId() != null && !data.getOwnerId().startsWith("$$")) {
            VoFriendList user = LocalDB.getUsersDbHelper(context).getUserInfo(data.getRoomId(), data.getOwnerId());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfileImage();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
            //imageViewMember.setVisibility(GONE);
        } else {
            //imageViewMember.setVisibility(GONE);
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            textViewUnRead.setVisibility(View.GONE);
        } else {
            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        imageViewTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.getOwnerId());
                data.getProfileListener().onClick(v);
            }
        });
    }

    public void setListView(String attachment) {
        int portfolioCount = 0;
        ArrayList<VoPortfolioList> voPortfolioLists = new ArrayList<>();
        try {

            JSONObject object = new JSONObject(attachment);
            JSONArray array = object.getJSONArray("data");
            portfolioCount = array.length();

            for (int i = 0; i < 5; i++) {
                JSONObject jo = array.getJSONObject(i);
                String sItemCode = jo.getString("stcd");
                String sItemName = jo.getString("stnm");
                String sPurchase = jo.getString("stav");
                String sPurchasePs = jo.getString("bywt") + "%";
                String sProfitPs = jo.getString("strt");

                VoPortfolioList voPortfolioList = new VoPortfolioList();
                voPortfolioList.setM_ItemCode(sItemCode);
                voPortfolioList.setM_ItemName(sItemName);
                voPortfolioList.setM_Purchase(sPurchase);
                voPortfolioList.setM_PurchasePs(sPurchasePs);
                voPortfolioList.setM_ProfitPs(sProfitPs);

                voPortfolioLists.add(voPortfolioList);
            }

            if (array.length() > 5) {
                ll_portfolio_more.setVisibility(View.VISIBLE);
                ll_portfolio_default.setVisibility(View.GONE);
            }
            else {
                ll_portfolio_more.setVisibility(View.GONE);
                ll_portfolio_default.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tv_ptcount.setText("편입종목수 : " + portfolioCount + "개");
        portfolioList = voPortfolioLists;
        adapter.setList(portfolioList);
    }

    public void portfolioMenu() {
        String userId = MessengerInfo.getUserId(getContext());
        String roomNum = Statics.ROOMINFO.getRoomId();
        String sUserInfo = "1";
        if (MessengerInfo.getUserId(getContext()).equals(Statics.ROOMINFO.getOwnerId())) {
            sUserInfo = Integer.toString(0) +"," + userId +","+roomNum;
        }
        else {
            sUserInfo = Integer.toString(1) +"," + Statics.ROOMINFO.getOwnerId() +","+roomNum;
        }

        Fragment_Main.getInstance().fragmentSlideMenu_main.setDisplay(aveContext, "6910", sUserInfo);
        Activity_ChatRoom.getInstance().openMTS("1");
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String address;
        private WeakReference<ImageView> ivWeak;

        public ImageDownloader(ImageView iv) {
            super();
            ivWeak = new WeakReference<>(iv);

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = params[0];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[CellChatDefaultText] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null && ivWeak != null) {
                ImageView iv = ivWeak.get();
                iv.setImageBitmap(result);
            } else {
            }
        }

    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10) {
                        hourString = "" + hour;
                    } else {
                        hourString = hour + "";
                    }
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static final Pattern WEB_URL = Pattern.compile(
            "((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)"
                    + "\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_"
                    + "\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?"
                    + "((?:(?:[" + Patterns.GOOD_IRI_CHAR + "][" + Patterns.GOOD_IRI_CHAR + "\\-]{0,64}\\.)+"   // named host
                    + Patterns.TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL
                    + "|(?:(?:25[0-5]|2[0-4]" // or ip address
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]"
                    + "|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9])))"
                    + "(?:\\:\\d{1,5})?)" // plus option port number
                    + "(\\/(?:(?:[" + Patterns.GOOD_IRI_CHAR + "\\;\\/\\?\\:\\@\\&\\=\\#\\~"  // plus option query params
                    + "\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?"
                    + "(?:\\b|$)"); // and finally, a word boundary or end of
    // input.  This is to stop foo.sure from
    // matching as foo.su

    public void addLinks(Spannable spannable) {
        // TextView textView

        Linkify.addLinks(spannable, WEB_URL, "");

        URLSpan[] urlSpans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan urlSpan : urlSpans) {
            int start = spannable.getSpanStart(urlSpan);
            int end = spannable.getSpanEnd(urlSpan);
            mLinkableText.removeSpan(urlSpan);
            mLinkableText.setSpan(new InternalURLSpan(urlSpan.getURL(), urlSpan.getURL(), 1), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mLinkableText.setSpan(new ForegroundColorSpan(Util.getColor(getContext(), R.color.blue)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
//        tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
//        tvMessage.setText(mLinkableText);
    }

    private class InternalURLSpan extends ClickableSpan {
        private String clickedSpan;
        private String itemCode;
        int linkType;

        public InternalURLSpan(String clickedString, String itemCode, int linkType) {
            this.clickedSpan = clickedString;
            this.itemCode = itemCode;
            this.linkType = linkType;
        }

        @Override
        public void onClick(View textView) {
            tvMessage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = itemCode;
                    switch (linkType) {
                        case 1:
                            try {
                                if (url.contains("http://") || url.contains("https://")) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    getContext().startActivity(intent);
                                } else if (url.contains(".com") || url.contains("www.")) {
                                    String urlString = url;
                                    if (urlString.contains(".com") && !urlString.contains("www.")) {
                                        urlString = "http://www." + urlString;
                                    } else if (!urlString.contains(".com") && urlString.contains("www.")) {
                                        urlString = "http://" + urlString;
                                    } else if (urlString.contains(".com") && urlString.contains("www.")) {
                                        urlString = "http://" + urlString;
                                    }
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                                    getContext().startActivity(intent);
                                }
                            } catch (Exception e) {
                                Toast.makeText(getContext(), "링크할 수 없는 url 입니다.", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                }
            });

        }
    }
    public TextView getTextViewContents() {

        return tvMessage;
    }
}
