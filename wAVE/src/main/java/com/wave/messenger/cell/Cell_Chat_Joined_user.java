package com.wave.messenger.cell;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

public class Cell_Chat_Joined_user extends LinearLayout {

    //TODO : LOGIC TO ADD USER PICTURE, MEMBERSHIP OVERLAY.

    private LinearLayout llFListLayout;

    private ImageView imageViewTitle;
    //private ImageView imageViewMember;
    private TextView textViewName;
    private TextView textViewNoMember;
    private TextView tvMe;
    private Context context;

    public Cell_Chat_Joined_user(Context context) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.chat_user, this, true);
        init();
    }

    private void init() {
        llFListLayout = (LinearLayout) findViewById(R.id.llFListLayout);
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        //imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewNoMember = (TextView) findViewById(R.id.textViewNoMember);
        tvMe = (TextView) findViewById(R.id.tvMe);

    }

    public void setUserName(String name) {
        textViewName.setText(name);

        if (!name.equals(MessengerInfo.getUserName(context))) {
            tvMe.setVisibility(GONE);
        }else {
            tvMe.setVisibility(VISIBLE);
        }

    }

    /*public void setMember(boolean member) {
        if (member) {
            imageViewMember.setVisibility(View.GONE);
        } else {
            imageViewMember.setVisibility(View.GONE);
        }
    }*/

    public void setProfile(String userId) {
        if (!userId.equals(MessengerInfo.getUserId(context))) {
            tvMe.setVisibility(GONE);
        }else {
            tvMe.setVisibility(VISIBLE);
        }

        if(userId != null && !userId.startsWith("$$")) {
            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + userId;
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
        }
    }

}
