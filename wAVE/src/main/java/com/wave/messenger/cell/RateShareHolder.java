package com.wave.messenger.cell;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class RateShareHolder extends RecyclerView.ViewHolder {

    public LinearLayout ll_ptitem;
    public TextView tv_itemname, tv_purchase, tv_purchaseps, tv_profitps;

    public RateShareHolder(View itemView) {
        super(itemView);
        ll_ptitem = (LinearLayout) itemView.findViewById(R.id.ll_ptitem);
        tv_itemname = (TextView) itemView.findViewById(R.id.tv_itemname);
        tv_purchase = (TextView) itemView.findViewById(R.id.tv_purchase);
        tv_purchaseps = (TextView) itemView.findViewById(R.id.tv_purchaseps);
        tv_profitps = (TextView) itemView.findViewById(R.id.tv_profitps);
    }
}
