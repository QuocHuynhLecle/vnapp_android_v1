package com.wave.messenger.cell.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.messenger.db.BuddyDbHelper;
import com.wave.messenger.db.ChatDataDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.dialog.Dialog_Common;
import com.wave.messenger.dialog.Dialog_Default;
import com.wave.messenger.mvp.ChatRoom.ChatPresenter;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoFriendList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 대화방 LinearLayout
 */
public class CellChatMoimText extends LinearLayout {
    private SpannableString mLinkableText;
    private TextView tvDate;
    private TextView tvDateURL;
    private TextView tvMessage;
    private LinearLayout llMoreText;
    private LinearLayout linearLayoutTimeLine;
//    private LinearLayout linearLayoutURLPreview;
    private LinearLayout linearLayoutTime;
    private ImageView imageViewTitle;
    private ImageView imageViewMember;
    private ImageView imageViewURLTitle;
    private TextView tvSender;
    private TextView textViewUnRead;
    private TextView textViewUnReadURL;
    private TextView textViewTimeLine;
    private TextView textViewURLTitle;
    private TextView textViewURLContent;
    private TextView textViewURL;
    private Context context;
    private VoChatData voChatData;
    private LinearLayout linearLayoutFailMsg;
    private ImageView imageViewDelete;
    private ImageView imageViewStatus;
    private LinearLayout llBubble;

    public CellChatMoimText(Context context, VoChatData data) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (data.getOwnerId().equals(MessengerInfo.getUserId(getContext()))) {//my message
            inflater.inflate(R.layout.chat_moimtext, this, true);
            initBasic();
            setUIBasic(data);
        } else {//other's message
            inflater.inflate(R.layout.chat_moimtext_op, this, true);
            init_op();
            setUI_op(data);
        }

        init();
        setUI(data);
    }

    /**
     * 공통된 view를 초기화
     */
    private void init() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDateURL = (TextView) findViewById(R.id.tvDateURL);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        llMoreText = (LinearLayout) findViewById(R.id.llMoreText);
        textViewUnReadURL = (TextView) findViewById(R.id.textViewUnReadURL);
        linearLayoutTimeLine = (LinearLayout) findViewById(R.id.linearLayoutTimeLine);
//        linearLayoutURLPreview = (LinearLayout) findViewById(R.id.linearLayoutURLPreview);
        linearLayoutTime = (LinearLayout) findViewById(R.id.linearLayoutTime);
        textViewTimeLine = (TextView) findViewById(R.id.textViewTimeLine);
        imageViewURLTitle = (ImageView) findViewById(R.id.imageViewURLTitle);
        textViewURLTitle = (TextView) findViewById(R.id.textViewURLTitle);
        textViewURLContent = (TextView) findViewById(R.id.textViewURLContent);
        textViewURL = (TextView) findViewById(R.id.textViewURL);
        llBubble = (LinearLayout) findViewById(R.id.llBubble);
    }

    private void initBasic() {
        tvDate = (TextView) findViewById(R.id.tvDate);
        linearLayoutFailMsg = (LinearLayout) findViewById(R.id.linearLayoutFailMsg);
        imageViewDelete = (ImageView) findViewById(R.id.imageViewDelete);
        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUIBasic(final VoChatData data) {

        //LogTrace.E("data.getStatus : " + data.getStatus() + " / " + data.getMessage());

        if ("400".equals(data.getStatus())) {
            linearLayoutFailMsg.setVisibility(View.VISIBLE);
            tvDate.setVisibility(View.GONE);
            textViewUnRead.setVisibility(View.GONE);

        } else {
            linearLayoutFailMsg.setVisibility(View.GONE);
            tvDate.setVisibility(View.VISIBLE);
            textViewUnRead.setVisibility(View.VISIBLE);

            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
        }

        imageViewDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 삭제하시겠습니까?";

                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);

                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(context);
                        db.delChatData(data.getChatId());
                        data.getListener().deleteMsg(data.getChatId());
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();
                    }
                });

                dialogDefault.show();
            }
        });

        imageViewStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String mag = "해당 메세지를 재전송하시겠습니까?";
                final Dialog_Default dialogDefault = new Dialog_Default(getContext(), mag);
                dialogDefault.setOkEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        data.setUserName(data.getSenderName());

                        ChatPresenter chatPresenter = new ChatPresenter(null);
                        chatPresenter.sendMessage(getContext(), data);

                        ChatDataDbHelper db = LocalDB.getChatDataDbHelper(getContext());
                        db.updateClearTryMsg(data.getChatId());

                        data.getListener().reSend(data.getChatId(), data.getCid(), data.getAttachment(), "1");

                        dialogDefault.cancel();

                    }
                });

                dialogDefault.setCancelEvent(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDefault.cancel();

                    }
                });

                dialogDefault.show();
            }
        });
    }

    /**
     * op에만 있는 뷰 초기화
     */
    private void init_op() {
        imageViewTitle = (ImageView) findViewById(R.id.imageViewTitle);
        //imageViewMember = (ImageView) findViewById(R.id.imageViewMember);
        tvSender = (TextView) findViewById(R.id.tvSender);
        textViewUnRead = (TextView) findViewById(R.id.textViewUnRead);
    }

    private void setUI(final VoChatData data) {

        ErrorController.showMessage("[CellChatDefaultText] setUI called");
        this.voChatData = data;
        mLinkableText = new SpannableString(data.getMessage());

        tvDate.setText(getFormattedTime(data.getReg_date()));

        if (data.isShowTimeLine()) {
            linearLayoutTimeLine.setVisibility(View.VISIBLE);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy년 MM월 dd일");
            try {
                Date day1 = format.parse(data.getReg_date().substring(0, 10));
                String time = format2.format(day1);
                textViewTimeLine.setText(time + " " + Statics.selectDay_of_week(data.getReg_date().substring(0, 10)) + "요일");
            } catch (Exception e) {
                textViewTimeLine.setText(data.getReg_date().substring(0, 10));
            }
        } else {
            linearLayoutTimeLine.setVisibility(View.GONE);
        }

        int font = SharedObject.getProperty_int(context, Constants.FONTSIZE, 14);

        tvMessage.setTextSize(font);
        tvMessage.setText(data.getMessage());
        llMoreText.setVisibility(View.VISIBLE);

        if (!data.isShowUnRead()) {
            textViewUnRead.setVisibility(View.GONE);
            textViewUnReadURL.setVisibility(View.GONE);
        }





        tvMessage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String moimId = null;
                String msgId = null;
                String link_action= null;
                try {
                    JSONObject obj = new JSONObject(data.getAttachment());
                    JSONObject obj2 = obj.getJSONObject("link_data");
                    moimId = obj2.getString("moimId");
                    msgId = obj2.optString("msgId","msgId_null");
                    link_action = obj.getString("link_action");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(link_action.equals("3")){
                    //모임 타임라인으로 이동.
                    intentMoimProfile(moimId);

                }else if(link_action.equals("4")){
                    //모임 글상세화면으로 이동

                    intentTimeLineDetail(msgId,moimId,false);
                }
            }
        });

        tvMessage.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Dialog_Common.getChatOptionDialog(context, data).show();
                return true;
            }
        });


        //공통 이벤트 정의
        //1. 500자 이상일 경우..

        llMoreText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String moimId = null;
                String msgId = null;
                String link_action= null;
                try {
                    JSONObject obj = new JSONObject(data.getAttachment());
                    JSONObject obj2 = obj.getJSONObject("link_data");
                    moimId = obj2.getString("moimId");
                    msgId = obj2.optString("msgId","msgId_null");
                    link_action = obj.getString("link_action");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(link_action.equals("3")){
                    //모임 타임라인으로 이동.
                    intentMoimProfile(moimId);

                }else if(link_action.equals("4")){
                    //모임 글상세화면으로 이동
                    intentTimeLineDetail(msgId,moimId,false);
                }
            }
        });

    }//end of SetUI.

    public void setUI_op(final VoChatData data) {
        BuddyDbHelper db = LocalDB.getBuddyDbHelper(getContext());

        String cuser_type = "";
        String crisk_type = "";
        String user_type = "";
        String risk_type = "";
        try {
            JSONObject obj = new JSONObject(data.getAttachment());
            cuser_type = obj.getString("cuser_type");
            crisk_type = obj.optString("crisk_type");

            if ("UT_AD".equals(MessengerInfo.getUserType()) || "UT_ST".equals(MessengerInfo.getUserType()) || "UT_ST_RE".equals(MessengerInfo.getUserType()) || "UT_ST_CO".equals(MessengerInfo.getUserType()) || "UT_ST_HQ".equals(MessengerInfo.getUserType())) {
                if ("UT_ME_JU".equals(cuser_type))
                    user_type = "(준)";
                else
                    user_type = "";

                if ("0".equals(crisk_type))
                    risk_type = "(투자권유불가)";
                else if ("1".equals(crisk_type))
                    risk_type = "(투자권유가능)";
                else
                    risk_type = "";
            } else {
                user_type = "";
                risk_type = "";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            tvSender.setText(user_type + data.getSenderName() + risk_type);
        } else {
            if (!"".equals(db.getUserName(data.getOwnerId())) && db.getUserName(data.getOwnerId()) != null) {
                LogTrace.E("user name : " + db.getUserName(data.getOwnerId()));
                tvSender.setText(user_type + db.getUserName(data.getOwnerId()) + risk_type);
            } else {
                LogTrace.E("sender name : " + data.getSenderName());
                tvSender.setText(user_type + data.getSenderName() + risk_type);
            }
        }

        if (data.getOwnerId() != null && !data.getOwnerId().startsWith("$$")) {
            VoFriendList user = LocalDB.getUsersDbHelper(context).getUserInfo(data.getRoomId(), data.getOwnerId());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfileImage();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(imageViewTitle);
            //imageViewMember.setVisibility(GONE);
        } else {
            //imageViewMember.setVisibility(GONE);
        }

        if ("2".equals(data.getRoomType()) || "3".equals(data.getRoomType())) {
            textViewUnRead.setVisibility(View.GONE);
        } else {
            if ("0".equals(data.getUnReadCount())) {
                textViewUnRead.setText("");
            } else {
                textViewUnRead.setText(data.getUnReadCount());
            }
        }

        imageViewTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.getOwnerId());
                data.getProfileListener().onClick(v);
            }
        });
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String address;
        private WeakReference<ImageView> ivWeak;

        public ImageDownloader(ImageView iv) {
            super();
            ivWeak = new WeakReference<>(iv);

        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = params[0];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[CellChatDefaultText] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null && ivWeak != null) {
                ImageView iv = ivWeak.get();
                iv.setImageBitmap(result);
            } else {
            }
        }

    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10) {
                        hourString = "" + hour;
                    } else {
                        hourString = hour + "";
                    }
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 모임타임라인으로 이동.
     * @param moimId
     */
    private void intentMoimProfile(String moimId) {
        SharedObject.setProperty_string(context, Constants.MOIM_ID,moimId);
        Intent intent = new Intent(context, Activity_MoimTimeLineList.class);
        context.startActivity(intent);
    }

    /**
     * 글상세화면으로 이동.
     * @param msgId
     * @param reply
     */
    private void intentTimeLineDetail(String msgId,String mmid, boolean reply) {
        SharedObject.setProperty_string(context, Constants.MOIM_ID,mmid);
        SharedObject.setProperty_string(context, Constants.MOIM_MSGID, msgId);
        SharedObject.setProperty_boolean(context, Constants.MOIM_REPLY, reply);
        SharedObject.setProperty_boolean(context, Constants.MOIM_CHAT, true);

        Intent intent = new Intent(context, Activity_TimeLineDetail.class);
        context.startActivity(intent);
    }
}
