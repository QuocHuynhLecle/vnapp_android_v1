package com.wave.messenger.cell;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

public class Cell_Professional_ListTitle extends LinearLayout {
    private TextView tv_header, tv_proListHeader;
    private LinearLayout ll_prolistHeader, ll_prolistHeader_more, ll_proheader;
    private ImageView iv_profile;
    private View topvliner;

    public Cell_Professional_ListTitle(Context context) {
        super(context);
        if (context != null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.item_header_pro, this, true);
            init();
        }
    }

    private void init() {
        tv_header = (TextView) findViewById(R.id.tv_header);
        tv_proListHeader = (TextView) findViewById(R.id.tv_proListHeader);
        ll_prolistHeader = (LinearLayout) findViewById(R.id.ll_prolistHeader);
        ll_prolistHeader_more = (LinearLayout) findViewById(R.id.ll_prolistHeader_more);
        ll_proheader = (LinearLayout) findViewById(R.id.ll_proheader);
        iv_profile = (ImageView) findViewById(R.id.iv_profile);
        iv_profile.setClipToOutline(true);
        topvliner = findViewById(R.id.topvliner);
    }

    public void setTextViewTitle(String name) {
        tv_header.setText(name);
    }

    public String getTextViewTitle() {
        return tv_header.getText().toString();
    }

    public void setTextViewMoreTitle(String name) {
        tv_proListHeader.setText(name);
    }

    public String getTextViewMoreTitle() {
        return tv_proListHeader.getText().toString();
    }

    public void proListHeaderMore() {
        ll_prolistHeader.setVisibility(View.GONE);
        ll_prolistHeader_more.setVisibility(View.VISIBLE);
    }

    public void proHeaderVisibility() {
        ll_proheader.setVisibility(View.GONE);
    }

    public void setTopVlinerVisibility() {
        topvliner.setVisibility(View.GONE);
    }
}
