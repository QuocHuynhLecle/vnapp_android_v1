package com.wave.messenger.cell;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class OpenUserHolder extends RecyclerView.ViewHolder {

    public ImageView iv_profile;
    public TextView tv_name, tv_id;
    public Button bt_kickuser;

    public OpenUserHolder(View itemView) {
        super(itemView);
        iv_profile = (ImageView) itemView.findViewById(R.id.iv_profile);
        tv_name = (TextView) itemView.findViewById(R.id.tv_name);
        tv_id = (TextView) itemView.findViewById(R.id.tv_id);
        bt_kickuser = (Button) itemView.findViewById(R.id.bt_kickuser);
    }
}
