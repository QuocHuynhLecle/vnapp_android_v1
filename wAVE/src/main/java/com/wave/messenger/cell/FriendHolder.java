package com.wave.messenger.cell;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by aveapp on 2017. 3. 21..
 */

public class FriendHolder {
    public LinearLayout ll_friend, ll_item, ll_line, ll_cirecleView, ll_imageView, ll_cell, ll_circleImageView;
    public ImageView iv_profile, iv_proIcon, iv_talk, iv_join, iv_proimage, iv_audio;
    public TextView tv_name, tv_proname, tv_prorole, tv_role, tv_counseling, tv_reading, tv_counseling2, tv_reading2, tv_moim_name;
    public View vLiner;


}
