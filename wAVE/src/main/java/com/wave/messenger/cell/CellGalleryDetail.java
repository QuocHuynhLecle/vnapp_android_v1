package com.wave.messenger.cell;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.mvp.Gallery.GalleryDetailDataModel;
import com.wave.messenger.utility.ImageLoaderAsync;
import com.wave.messenger.utility.OnListClickCallback;

import java.util.List;

public class CellGalleryDetail extends LinearLayout implements OnClickListener {

    private ImageView ivGallery1, ivGallery2, ivGallery3, ivGallery4;
    private ImageView ivCheckBox1, ivCheckBox2, ivCheckBox3, ivCheckBox4;

    private GalleryDetailDataModel data;
    private OnListClickCallback callback;
    private Context context;

    public CellGalleryDetail(Context context, GalleryDetailDataModel data,
                             OnListClickCallback callback) {
        super(context);
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.cell_gallerydetail, this, true);
        this.data = data;
        this.callback = callback;
        initView();

    }

    public void loadImage1(Context context) {
        if (data.getData().get(0) != null) {
            new ImageLoaderAsync(ivGallery1).execute(data.getData().get(0)
                    .getFullPath());
        }

    }

    public void loadImage2(Context context) {

        if (data.getData().size() >= 2 && data.getData().get(1) != null) {
            new ImageLoaderAsync(ivGallery2).execute(data.getData().get(1)
                    .getFullPath());
        }

    }

    public void loadImage3(Context context) {

        if (data.getData().size() >= 3 && data.getData().get(2) != null) {
            new ImageLoaderAsync(ivGallery3).execute(data.getData().get(2)
                    .getFullPath());
        }

    }

    public void loadImage4(Context context) {
        if (data.getData().size() >= 4 && data.getData().get(3) != null) {
            new ImageLoaderAsync(ivGallery4).execute(data.getData().get(3)
                    .getFullPath());
        }

    }

    private void initView() {
        ivGallery1 = (ImageView) findViewById(R.id.ivGallery1);
        ivGallery2 = (ImageView) findViewById(R.id.ivGallery2);
        ivGallery3 = (ImageView) findViewById(R.id.ivGallery3);
        ivGallery4 = (ImageView) findViewById(R.id.ivGallery4);

        ivCheckBox1 = (ImageView) findViewById(R.id.ivCheckBox1);
        ivCheckBox2 = (ImageView) findViewById(R.id.ivCheckBox2);
        ivCheckBox3 = (ImageView) findViewById(R.id.ivCheckBox3);
        ivCheckBox4 = (ImageView) findViewById(R.id.ivCheckBox4);

        ivGallery1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                List<GalleryDataModel> item = data.getData();
                if (item.size() >= 1)
                    callback.onClick(item.get(0));
            }
        });

        ivGallery2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                List<GalleryDataModel> item = data.getData();
                if (item.size() >= 2)
                    callback.onClick(item.get(1));
            }
        });

        ivGallery3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                List<GalleryDataModel> item = data.getData();
                if (item.size() >= 3)
                    callback.onClick(item.get(2));
            }
        });

        ivGallery4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                List<GalleryDataModel> item = data.getData();
                if (item.size() >= 4)
                    callback.onClick(item.get(3));
            }
        });

    }

    @Override
    public void onClick(View v) {


    }

}
