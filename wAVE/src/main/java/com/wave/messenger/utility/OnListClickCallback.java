package com.wave.messenger.utility;

import com.wave.messenger.mvp.Gallery.GalleryDataModel;

public interface OnListClickCallback {
    void onClick(GalleryDataModel item);
}
