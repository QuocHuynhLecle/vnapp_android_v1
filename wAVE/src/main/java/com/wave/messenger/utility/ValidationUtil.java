package com.wave.messenger.utility;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dongho on 2016. 10. 18..
 */
public class ValidationUtil {
    /**
     * 휴대폰 번호인지 체크
     *
     * @param phoneNumber 휴대폰 번호
     * @return 휴대폰 번호(010, 011, 016, 017, 019)이면 true
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return false;
        }

        phoneNumber = phoneNumber.replace("-", "").replace(".", "");

        String regex = "^(01[0|1|6|7|8|9])(\\d{3}|\\d{4})(\\d{4})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(phoneNumber);

        return match.find();
    }
}
