package com.wave.messenger.utility;

import com.squareup.otto.Bus;

/**
 * Created by aveapp on 2017. 6. 30..
 */

public final class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {

    }
}
