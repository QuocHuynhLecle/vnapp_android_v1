package com.wave.messenger.utility;

import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Profile.DataModel.ThumbnailBitmap;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoChatList;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import moa.android.api.util.json.LBJSONObject;

public class Statics {
    public static VoChatList ROOMINFO = null;
    private static boolean ROOMONOFF;
    /**
     * 친구 목록의 프로필에 대한 비트맵 객체들
     */
    private static Map<String, ThumbnailBitmap> THUMBNAIL_MAP;

    private static ThumbnailBitmap MY_PROFILE_PIC;

    private static Map<String, ThumbnailBitmap> PROFILE_IMAGE_CACHE;

    public static Map<String, ThumbnailBitmap> getProfileImageCache() {
        if (PROFILE_IMAGE_CACHE == null)
            PROFILE_IMAGE_CACHE = new HashMap<>();
        return PROFILE_IMAGE_CACHE;
    }

    public static ThumbnailBitmap getMyProfilePic() {
        if (MY_PROFILE_PIC == null) {
            MY_PROFILE_PIC = new ThumbnailBitmap();
        }
        return MY_PROFILE_PIC;
    }

    public static void setMyProfilePic(ThumbnailBitmap myProfilePic) {
        MY_PROFILE_PIC = myProfilePic;
    }

    public static void setRoomOnStop(boolean roomOnOff) {
        ROOMONOFF = roomOnOff;
    }

    public static boolean getRoomOnStop() {
        return ROOMONOFF;
    }
    /**
     * 일정한 규칙에 따라 랜덤한 초상화를 돌려준다.
     *
     * @param userName
     * @return
     */
    public static int setRandomProfile(String userName) {
        return R.drawable.ic_list_friend_profile;
    }

    public static Map<String, ThumbnailBitmap> getThumbnailMap() {
        if (THUMBNAIL_MAP == null)
            THUMBNAIL_MAP = new HashMap<>();
        return THUMBNAIL_MAP;
    }

    public static String selectDay_of_week(String date) throws Exception {
        String day = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        Date nDate = format.parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(nDate);
        int dayNum = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayNum) {
            case 1:
                day = "일";
                break;
            case 2:
                day = "월";
                break;
            case 3:
                day = "화";
                break;
            case 4:
                day = "수";
                break;
            case 5:
                day = "목";
                break;
            case 6:
                day = "금";
                break;
            case 7:
                day = "토";
                break;
        }
        return day;
    }

    static public VoChatData loadFromJsonChatList(LBJSONObject jObject) {

        VoChatData result = new VoChatData();
        try {
            result.setCid(jObject.getString("cId"));
            result.setChatId(jObject.getString("chatId"));
            result.setOwnerId(jObject.getString("ownerId"));
            result.setSenderName(jObject.getString("senderName"));
            result.setChatType(jObject.getString("chatType"));
            result.setMessageType(jObject.getString("messageType"));
            result.setStaffMsg(jObject.getString("staffMsg"));
            result.setTitle(jObject.getString("title"));
            result.setMessage(jObject.getString("message"));
            result.setAttachment(jObject.getString("attachment"));
            result.setReg_date(jObject.getString("reg_date"));
            //result.setMessageMore(jObject.getString("message_more"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    static public VoChatData loadFromJsonChatMSG(LBJSONObject jObject) {
        VoChatData result = new VoChatData();

        try {
            result.setRoomId(jObject.getString("roomId"));
            result.setCid(jObject.getString("cId"));
            result.setChatId(jObject.getString("chatId"));
            result.setOwnerId(jObject.getString("senderId"));
            result.setSenderName(jObject.getString("senderName"));
            result.setChatType(jObject.getString("chatType"));
            result.setMessageType(jObject.getString("messageType"));
            result.setTitle(jObject.getString("title"));
            result.setMessage(jObject.getString("message"));
            result.setAttachment(jObject.getString("attachment"));
            result.setReg_date(jObject.getString("regDate"));
            result.setMessageMore(jObject.getString("message_more"));
            result.setStaffMsg(jObject.getString("staffMsg"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
