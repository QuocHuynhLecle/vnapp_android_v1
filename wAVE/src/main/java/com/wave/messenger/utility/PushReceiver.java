package com.wave.messenger.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.wave.messenger.share.HMMessengerShare;

import org.json.JSONObject;

/**
 * Created by aveapp on 2017. 9. 4..
 */

public class PushReceiver extends BroadcastReceiver {
    private Bundle mBundle;
    private String pushData;
    private Context mContext;
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            mContext = context;
            mBundle = intent.getExtras();
            savePushMessage(mBundle);

            intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
            intent.putExtra("pushmsg", pushData);
            context.startActivity(intent);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void savePushMessage(Bundle bundle) {
        String type = bundle.getString("type");
        String roomId = bundle.getString("roomId");
        String cId = bundle.getString("cId");
        String chatId = bundle.getString("chatId");
        String senderId = bundle.getString("senderId");
        String senderName = bundle.getString("senderName");
        String regDate = bundle.getString("regDate");
        String title = bundle.getString("title");
        String unread = bundle.getString("unread");
        String useNoti = bundle.getString("useNoti");

        JSONObject object = new JSONObject();

        try {
            object.put("type", type);
            object.put("roomId", roomId);
            object.put("cId", cId);
            object.put("chatId", chatId);
            object.put("senderId", senderId);
            object.put("senderName", senderName);
            object.put("regDate", regDate);
            object.put("title", title);
            object.put("unread", unread);
            object.put("useNoti", useNoti);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HMMessengerShare hMMessengerShare = HMMessengerShare.getInstance();
        hMMessengerShare.gcmMessage(object, mContext);

        pushData = object.toString();
    }
}
