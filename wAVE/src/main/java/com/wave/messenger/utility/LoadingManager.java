package com.wave.messenger.utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.wave.massenger.piggy.R;


public class LoadingManager {

    private static LoadingManager sInstance = null;

    private static final int HIDE_DIALOG_MESSAGE = 9102;
    public static final int INFINITY = 0;
    private static final int DEFAULT_LOADING_TIMEOUT = 10000;

    private Context mContext;
    private Dialog mDialog;
    private boolean mCancelable = false;
    private long mLoadingTimeout = DEFAULT_LOADING_TIMEOUT;

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (mDialog != null) {
                mDialog.dismiss();
            }
        }
    };

    public LoadingManager() {
    }

    public static LoadingManager with(Context context) {
        if (null == sInstance) {
            synchronized (LoadingManager.class) {
                if (null == sInstance) {
                    sInstance = new LoadingManager();
                }
            }
        }
        sInstance.setContext(context);

        return sInstance;
    }

    private void setContext(Context context) {
        mContext = context;
    }

    public void cancelable(boolean cancelable) {
        mCancelable = cancelable;
    }

    public LoadingManager timeout(long loadingTimeout) {
        mLoadingTimeout = loadingTimeout;
        return this;
    }

    public void showLoadingDialog() {
        hideLoadingDialog();
        Log.e("LoadingManager","showLoadingDialog");

        mDialog = new Dialog(mContext, R.style.TransparentDialog);
        mDialog = new Dialog(mContext, R.style.TransparentDialog);
        mDialog.addContentView(new ProgressBar(mContext), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mDialog.setCancelable(mCancelable);

        try {
            if (mLoadingTimeout != INFINITY) {
                mHandler.sendEmptyMessageDelayed(HIDE_DIALOG_MESSAGE, mLoadingTimeout);
            }

            mDialog.show();
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingDialog() {
        Log.e("LoadingManager","hideLoadingDialog");

        try {
            mHandler.removeMessages(HIDE_DIALOG_MESSAGE);

            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
