package com.wave.messenger.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by user on 2016-09-07.
 */
public class UtilityMethods {
    public static String getFormattedYearDateDay(){
        String dateString = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA).format(new Date());
        return dateString;
    }

    public static String getDefaultDateString(){
        return "2000-01-01";
    }
}
