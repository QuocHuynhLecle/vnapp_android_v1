package com.wave.messenger.utility;

import moa.android.api.MOAData;

/**
 * Created by zeonkoon on 2017. 2. 10..
 */

public class BUSMOAData  {

    private static BUSMOAData instantce;
    private MOAData mMOAData;

    private BUSMOAData() {}

    public static synchronized BUSMOAData getInstantce() {

        if (instantce == null) {
            instantce = new BUSMOAData();
        }

        return instantce;
    }

    public void setmMOAData(MOAData moaData) {
        this.mMOAData = moaData;
        LogTrace.E("moaData : " + mMOAData.toString());
    }

    public MOAData getmMOAData() {
        return mMOAData;
    }

    public void remove() {

        if (instantce != null) {

            instantce.mMOAData = null;
            instantce = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
