package com.wave.messenger.utility;

public interface DialogCallback {
	void onConfirm(Object... params);
	void onDecline(Object... params);
}