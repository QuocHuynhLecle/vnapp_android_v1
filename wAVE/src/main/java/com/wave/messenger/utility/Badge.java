package com.wave.messenger.utility;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;

import java.util.List;

/**
 * Created by aveapp on 2017-01-13.
 */

public class Badge {
    Context mContext;

    public Badge(Context context) {
        this.mContext = context;
    }

    public void setBadge(int count) {
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        // 패키지 네임과 클래스 네임 설정
        intent.putExtra("badge_count_package_name", mContext.getPackageName());
        intent.putExtra("badge_count_class_name", getLauncherClassName());
        // 업데이트 카운트
        intent.putExtra("badge_count", count);

        // Version이 3.1이상일 경우에는 Flags를 설정하여 준다.
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            intent.setFlags(Intent.FILL_IN_SOURCE_BOUNDS);
        }
        mContext.sendBroadcast(intent);
    }

    private String getLauncherClassName() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(mContext.getPackageName());

        List<ResolveInfo> resolveInfoList = mContext.getPackageManager().queryIntentActivities(intent, 0);
        if(resolveInfoList != null && resolveInfoList.size() > 0) {
            return resolveInfoList.get(0).activityInfo.name;
        }
        return null;
    }
}