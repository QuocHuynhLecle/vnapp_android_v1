package com.wave.messenger.utility;

import android.app.Activity;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by zeonkoon on 2017. 2. 13..
 */

public class BackPress {

    private static BackPress instance;
    Activity mActivity;
    Boolean mbFlagBack = false;

    private BackPress() {
    }

    public static synchronized BackPress activity(Activity activity) {

        if (instance == null) {

            instance = new BackPress();
        }

        instance.mActivity = activity;

        return instance;
    }

    public void backPress() {

        if(mbFlagBack) {

            instance.mActivity.finish();
            //android.os.Process.killProcess(android.os.Process.myPid());
        } else {
            
            Toast.makeText(mActivity, "뒤로가기를 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT).show();

            mbFlagBack = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mbFlagBack = false;
                }
            }, 1000);
        }
    }
}
