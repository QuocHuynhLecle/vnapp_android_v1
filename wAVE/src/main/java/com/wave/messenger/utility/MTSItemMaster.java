//
//  UserInfo.java
//  pipe
//
//  Created by 안경종 on 2015. 10. 20..
//  Copyright © 2015년 Misslee. All rights reserved.
//

package com.wave.messenger.utility;

import android.annotation.SuppressLint;

@SuppressLint("NewApi")
public class MTSItemMaster {
    /*private final static String LOG_TAG = "MTSItemMaster";
    
	private static ArrayList<VoItemCode> items;
	private static MTSItemMaster instance;
	
	public static MTSItemMaster getInstance() 
	{
		if(instance == null)
		{
			instance = new MTSItemMaster();
			initItems();
		}
		return instance;
	}
	
	*//**
	 * 2016.07.18 - LSS
	 * 흐음... 왜 마스터 로딩하다 마는지 모르겠다...
	 * 일단 로딩은 다 하게 변경
	 *//*
	private static int[] m_nCompleteSize = new int[20];
	private static boolean isCompleteLoad()
	{
	    for( int i = 0; i < m_nCompleteSize.length; i++ )
	    {
	        if(m_nCompleteSize[i] <= 0 || m_nCompleteSize[i] != getItemListSize(i))
	        {
	            return false;
	        }
	    }
	    
	    return true;
	}
	
	private static int getItemListSize(int nIndex)
	{
	    int[] arrSize =
	    {
	    };
	    
	    return arrSize[nIndex];
	}
	
	public synchronized static void initItems() 
	{
		if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "initItems()");
//		Util.printStackTrace("링크테스트");
		items = new ArrayList<VoItemCode>();
		try {
            //if( m_nCompleteSize[0] != getItemListSize(0) )
            {
                for (int i = 0; i < ItemMaster.m_arrKospiList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrKospiList.get(i).getCode());
                    item.setName(ItemMaster.m_arrKospiList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[0] = ItemMaster.m_arrKospiList.size();    
            }
            
            //if( m_nCompleteSize[1] != getItemListSize(1) )
            {
                for (int i = 0; i < ItemMaster.m_arrELWList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrELWList.get(i).getCode());
                    item.setName(ItemMaster.m_arrELWList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[1] = ItemMaster.m_arrELWList.size();
            }
            
            //if( m_nCompleteSize[2] != getItemListSize(2) )
            {
                for (int i = 0; i < ItemMaster.m_arrETFList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrETFList.get(i).getCode());
                    item.setName(ItemMaster.m_arrETFList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[2] = ItemMaster.m_arrETFList.size();
            }
            
            //if( m_nCompleteSize[3] != getItemListSize(3) )
            {
                for (int i = 0; i < ItemMaster.m_arrWarrantList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrWarrantList.get(i).getCode());
                    item.setName(ItemMaster.m_arrWarrantList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[3] = ItemMaster.m_arrWarrantList.size();
            }
            
            //if( m_nCompleteSize[4] != getItemListSize(4) )
            {
                for (int i = 0; i < ItemMaster.m_arrFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrFutureList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[4] = ItemMaster.m_arrFutureList.size();
            }
            
            //if( m_nCompleteSize[5] != getItemListSize(5) )
            {
                for (int i = 0; i < ItemMaster.m_arrOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrOptionList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[5] = ItemMaster.m_arrOptionList.size();
            }
            
            //if( m_nCompleteSize[6] != getItemListSize(6) )
            {
                for (int i = 0; i < ItemMaster.m_arrSortedStockList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrSortedStockList.get(i).getCode());
                    item.setName(ItemMaster.m_arrSortedStockList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[6] = ItemMaster.m_arrSortedStockList.size();
            }
            
            //if( m_nCompleteSize[7] != getItemListSize(7) )
            {
                for (int i = 0; i < ItemMaster.m_arrMFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrMFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrMFutureList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[7] = ItemMaster.m_arrMFutureList.size();
            }
            
            //if( m_nCompleteSize[8] != getItemListSize(8) )
            {
                for (int i = 0; i < ItemMaster.m_arrMOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrMOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrMOptionList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[8] = ItemMaster.m_arrMOptionList.size();
            }
            
            //if( m_nCompleteSize[9] != getItemListSize(9) )
            {
                for (int i = 0; i < ItemMaster.m_arrCMEFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrCMEFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrCMEFutureList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[9] = ItemMaster.m_arrCMEFutureList.size();
            }
            
            //if( m_nCompleteSize[10] != getItemListSize(10) )
            {
                for (int i = 0; i < ItemMaster.m_arrEUREXOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrEUREXOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrEUREXOptionList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[10] = ItemMaster.m_arrEUREXOptionList.size();
            }
            
            //if( m_nCompleteSize[11] != getItemListSize(11) )
            {
                for (int i = 0; i < ItemMaster.m_arrFuOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrFuOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrFuOptionList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[11] = ItemMaster.m_arrFuOptionList.size();
            }
            
            //if( m_nCompleteSize[12] != getItemListSize(12) )
            {
                for (int i = 0; i < ItemMaster.m_arrMFuOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrMFuOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrMFuOptionList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[12] = ItemMaster.m_arrMFuOptionList.size();
            }
            
            //if( m_nCompleteSize[13] != getItemListSize(13) )
            {
                for (int i = 0; i < ItemMaster.m_arrFFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrFFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrFFutureList.get(i).getDivName());
                    
                    items.add(item);
                }
                m_nCompleteSize[13] = ItemMaster.m_arrFFutureList.size();
            }
            
            //if( m_nCompleteSize[14] != getItemListSize(14) )
            {
                for (int i = 0; i < ItemMaster.m_arrSFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrSFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrSFutureList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[14] = ItemMaster.m_arrSFutureList.size();
            }
            
            //if( m_nCompleteSize[15] != getItemListSize(15) )
            {
                for (int i = 0; i < ItemMaster.m_arrSFutureGStockList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrSFutureGStockList.get(i).getCode());
                    item.setName(ItemMaster.m_arrSFutureGStockList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[15] = ItemMaster.m_arrSFutureGStockList.size();
            }
            
            //if( m_nCompleteSize[16] != getItemListSize(16) )
            {
                for (int i = 0; i < ItemMaster.m_arrCFutureList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrCFutureList.get(i).getCode());
                    item.setName(ItemMaster.m_arrCFutureList.get(i).getName());
                    items.add(item);
                }
                m_nCompleteSize[16] = ItemMaster.m_arrCFutureList.size();
            }
            
            //if( m_nCompleteSize[17] != getItemListSize(17) )
            {
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "ItemMaster.m_arrKonexList.size() = "+ ItemMaster.m_arrKonexList.size());
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "m_nCompleteSize[17] = "+ m_nCompleteSize[17]);
                for (int i = 0; i < ItemMaster.m_arrKonexList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrKonexList.get(i).getCode());
                    item.setName(ItemMaster.m_arrKonexList.get(i).getName());
                    
//                    if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "[" +i +"] = "+ ItemMaster.m_arrKonexList.get(i).getCode() + "," + ItemMaster.m_arrKonexList.get(i).getName());
                    
                    items.add(item);
                }
                m_nCompleteSize[17] = ItemMaster.m_arrKonexList.size();
            }
            
            //if( m_nCompleteSize[18] != getItemListSize(18) )
            {
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "ItemMaster.m_arrKOtcList.size() = "+ ItemMaster.m_arrKOtcList.size());
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "m_nCompleteSize[18] = "+ m_nCompleteSize[18]);
                for (int i = 0; i < ItemMaster.m_arrKOtcList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrKOtcList.get(i).getCode());
                    item.setName(ItemMaster.m_arrKOtcList.get(i).getName());
                    
//                    if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "[" +i +"] = "+ ItemMaster.m_arrKOtcList.get(i).getCode() + "," + ItemMaster.m_arrKOtcList.get(i).getName());
                    
                    items.add(item);
                }
                m_nCompleteSize[18] = ItemMaster.m_arrKOtcList.size();
            }

            
            {
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "ItemMaster.m_arrFOptionList.size() = "+ ItemMaster.m_arrFOptionList.size());
//            	if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "m_nCompleteSize[19] = "+ m_nCompleteSize[19]);
                for (int i = 0; i < ItemMaster.m_arrFOptionList.size(); i++) {
                    VoItemCode item = new VoItemCode();
                    item.setCode(ItemMaster.m_arrFOptionList.get(i).getCode());
                    item.setName(ItemMaster.m_arrFOptionList.get(i).getName());
                    
//                    if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "[" +i +"] = "+ ItemMaster.m_arrFOptionList.get(i).getCode() + "," + ItemMaster.m_arrFOptionList.get(i).getName());
                    
                    items.add(item);
                }
                m_nCompleteSize[19] = ItemMaster.m_arrFOptionList.size();
            }

            
//            if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "기본 마스터 추가 완료");
//            if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "items.size() = " + items.size());
            
            VoItemCode item = new VoItemCode();
//            item.setCode(Constants.CODE_HANA_MEMBERS);
//            item.setName(Constants.CODE_HANA_MEMBERS);
//            items.add(item);
//
//            item = new VoItemCode();
//            item.setCode(Constants.CODE_ACCOUNT1);
//            item.setName(Constants.CODE_ACCOUNT1);
//            items.add(item);
//
//            item = new VoItemCode();
//            item.setCode(Constants.CODE_ACCOUNT2);
//            item.setName(Constants.CODE_ACCOUNT2);
//            items.add(item);

            //티슈진
            item = new VoItemCode();
            item.setCode(Constants.CODE_TISSUEGENE_NUM);
            item.setName(Constants.CODE_TISSUEGENE);
            items.add(item);

            //레드존
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC001);
            item.setName(Constants.CODE_ETC001);
            items.add(item);

            //투자의달인
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC002);
            item.setName(Constants.CODE_ETC002);
            items.add(item);

            //RG
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC003);
            item.setName(Constants.CODE_ETC003);
            items.add(item);

            //스톡봇
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC004);
            item.setName(Constants.CODE_ETC004);
            items.add(item);

            //파봇
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC005);
            item.setName(Constants.CODE_ETC005);
            items.add(item);

            //모닝스타
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC006);
            item.setName(Constants.CODE_ETC006);
            items.add(item);

            //로보스탁
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC007);
            item.setName(Constants.CODE_ETC007);
            items.add(item);

            //멘토스
            item = new VoItemCode();
            item.setCode(Constants.CODE_ETC008);
            item.setName(Constants.CODE_ETC008);
            items.add(item);
            
            Collections.sort(items, new NoDescCompare());

        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		
		if(ConfigUtil.ms_isLogEnable)HLog.e("링크테스트", "ENDOF __________ initItems()");
	}
	
	public ArrayList<VoItemCode> getItemCodes() 
	{
//		if(items == null || !isCompleteLoad()) 
//		{
//	        initItems();
//		}

		return items;
	}
	
	static class NoDescCompare implements Comparator<VoItemCode> {
		 
		*//**
		 * 내림차순(DESC)
		 *//*
		@Override
		public int compare(VoItemCode arg0, VoItemCode arg1) {
			// TODO Auto-generated method stub
			return arg0.getName().length() > arg1.getName().length() ? -1 : arg0.getName().length() < arg1.getName().length() ? 1:0;
		}
 
	}*/
}
