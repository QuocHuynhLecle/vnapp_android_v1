package com.wave.messenger.utility;

import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aveapp on 2017. 3. 22..
 */

public class FileUtil {
    public static File image;

    public static File getAlbumDir() {
        LogTrace.E("앨범 디렉토리");
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "candleman");

        if (!storageDir.exists()) {
            storageDir.mkdir();
        }

        return storageDir;
    }

    public static File createImageFile() {
        LogTrace.E("파일 생성");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA).format(new Date());

        try {
            image = File.createTempFile(timeStamp, ".png", getAlbumDir());
        } catch (Exception e) {
            LogTrace.E("파일 생성 익셉션");
            e.printStackTrace();
        }

        return image;
    }
}
