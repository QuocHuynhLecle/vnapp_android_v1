package com.wave.messenger.utility;

import android.text.TextUtils;

/**
 * Created by dongho on 2016. 11. 8..
 */
public class NumberUtil {
    public static int getInt(Integer value) {
        return getInt(value, 0);
    }

    public static int getInt(Integer value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    public static int toInt(String value) {
        return toInt(value, 0);
    }

    public static int toInt(String value, int defaultValue) {
        try {
            if (TextUtils.isEmpty(value)) {
                return defaultValue;
            }

            return Integer.parseInt(value);
        } catch (Exception ignored) {
            return defaultValue;
        }
    }

    public static String parseInt(Integer value) {
        return parseInt(value, "");
    }

    public static String parseInt(Integer value, String defaultValue) {
        try {
            if (value == null) {
                return defaultValue;
            }

            return String.valueOf(value);
        } catch (Exception ignored) {
            return defaultValue;
        }
    }

    public static boolean equals(int value, Integer compareValue) {
        return compareValue != null && value == compareValue;
    }
}
