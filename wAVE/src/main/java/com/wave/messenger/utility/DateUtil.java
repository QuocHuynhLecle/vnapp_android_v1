package com.wave.messenger.utility;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by dongho on 2016. 11. 15..
 */
public class DateUtil {
    public static String format(String pattern, Date date) {
        return format(pattern, date, "");
    }

    public static String format(String pattern, Date date, String defaultValue) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.KOREA);

            if (date == null) {
                return defaultValue;
            }

            return sdf.format(date);
        } catch (Exception ignored) {
            return defaultValue;
        }
    }

    public static Date parse(String pattern, String value) {
        return parse(pattern, value, null);
    }

    public static Date parse(String pattern, String value, Date defaultdate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.KOREA);

            if (TextUtils.isEmpty(value)) {
                return defaultdate;
            }

            return sdf.parse(value);
        } catch (Exception ignored) {
            return defaultdate;
        }
    }

    public static List<Date> getToday() {
        return getToday(new Date());
    }

    public static List<Date> getToday(Date regDate) {
        List<Date> result = new ArrayList<>();

        Calendar firstCal = Calendar.getInstance();
        firstCal.setTime(regDate);
        firstCal.set(Calendar.HOUR_OF_DAY, 0);
        firstCal.set(Calendar.MINUTE, 0);
        firstCal.set(Calendar.SECOND, 0);

        Calendar secondCal = Calendar.getInstance();
        secondCal.setTime(regDate);
        secondCal.set(Calendar.HOUR_OF_DAY, 23);
        secondCal.set(Calendar.MINUTE, 59);
        secondCal.set(Calendar.SECOND, 59);

        result.add(firstCal.getTime());
        result.add(secondCal.getTime());

        return result;
    }

    public static boolean isToday(Date date) {
        if (date == null) {
            return false;
        }

        Calendar today = Calendar.getInstance();
        today.setTime(date);

        Calendar firstCal = Calendar.getInstance();
        firstCal.set(Calendar.HOUR_OF_DAY, 0);
        firstCal.set(Calendar.MINUTE, 0);
        firstCal.set(Calendar.SECOND, 0);

        Calendar secondCal = Calendar.getInstance();
        secondCal.set(Calendar.HOUR_OF_DAY, 23);
        secondCal.set(Calendar.MINUTE, 59);
        secondCal.set(Calendar.SECOND, 59);

        return today.after(firstCal) && today.before(secondCal);
    }

    public static boolean isShown(long timeMillis) {
        boolean result = false;
        if(timeMillis > 0) {
            Calendar shownDay = Calendar.getInstance();
            shownDay.setTimeInMillis(timeMillis);
            shownDay.set(Calendar.HOUR_OF_DAY, 0);
            shownDay.set(Calendar.MINUTE, 0);
            shownDay.set(Calendar.SECOND, 0);
            shownDay.set(Calendar.DAY_OF_MONTH, shownDay.get(Calendar.DAY_OF_MONTH) + 7);

            Calendar calendar = Calendar.getInstance();
            if(!(calendar.getTimeInMillis() > shownDay.getTimeInMillis())) {
                result = true;
            }
        }

        return result;
    }
}

