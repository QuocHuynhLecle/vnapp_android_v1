package com.wave.messenger.utility;

import android.content.Context;

import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.greendao.dao.DaoMaster;
import com.wave.messenger.db.greendao.dao.DaoSession;

import de.greenrobot.dao.database.Database;

public class GreenDaoManager {
    private static GreenDaoManager sInstance = null;

    private DaoSession mDaoSession = null;
    private MyOpenHelper mHelper;

    public static GreenDaoManager getInstance() {
        if (null == sInstance) {
            synchronized (GreenDaoManager.class) {
                if (null == sInstance) {
                    sInstance = new GreenDaoManager();
                }
            }
        }

        return sInstance;
    }

    private Database getDatabase(Context context, boolean readOnly) {
        if (mHelper == null) {
            mHelper = new MyOpenHelper(context, LocalDB.DB_NAME + "_gdao");
        }

        if (readOnly) {
            return mHelper.getReadableDatabase();
        } else {
            return mHelper.getWritableDatabase();
        }
    }

    public DaoSession getSession(Context context) {
        if (mDaoSession == null) {
            mDaoSession = new DaoMaster(getDatabase(context, false)).newSession();
        }

        return mDaoSession;
    }

    public void close() {
        if (mDaoSession != null) {
            mDaoSession.clear();
            mDaoSession = null;

        }

        if (mHelper != null) {
            mHelper.getReadableDatabase().close();
            mHelper.getWritableDatabase().close();
            mHelper.close();
            mHelper = null;
        }
    }

    private class MyOpenHelper extends DaoMaster.DevOpenHelper {
        public MyOpenHelper(Context context, String name) {
            super(context, name);
        }

        @Override
        public void onCreate(Database db) {
            super.onCreate(db);
        }

        @Override
        public void onUpgrade(Database db, int oldVersion, int newVersion) {
            super.onUpgrade(db, oldVersion, newVersion);

            switch (oldVersion) {
                case 1:
//                    db.execSQL(SQL_UPGRADE_1To2);
                case 2:
//                    db.execSQL(SQL_UPGRADE_2To3);
                    break;
                default:
                    break;
            }
        }
    }
}
