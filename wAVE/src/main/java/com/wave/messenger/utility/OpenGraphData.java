package com.wave.messenger.utility;

/**
 * Created by user on 2016-09-07.
 */
public class OpenGraphData {

    protected String domain;
    protected String url;
    protected String html;

    protected String title;
    protected String image;
    protected String description;

    public String getDomain() {
        return domain;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getHtml() {
        return html;
    }

    @Override
    public String toString() {
        return "OpenGraphData{" +
                "domain='" + domain + '\'' +
                ", url='" + url + '\'' +
                ", html='" + html + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
