package com.wave.messenger.utility;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.wave.massenger.piggy.R;

public class ViewManager {
	
	static ViewManager instance = null;
    
	public static synchronized ViewManager getViewManager() {
		if(instance == null) {
			instance = new ViewManager();
		}				
		return instance;
	}
	
	
	public void replaceFragment(FragmentActivity parentActivity, int parentLayoutId, Fragment fragment) {
		parentActivity.getSupportFragmentManager().beginTransaction().replace(parentLayoutId, fragment).addToBackStack(fragment.getClass().getName()).commitAllowingStateLoss();
	}
	
	public void replaceFragment(Fragment parentFragment, int parentLayoutId, Fragment fragment) {
		parentFragment.getChildFragmentManager().beginTransaction().replace(parentLayoutId, fragment).addToBackStack(fragment.getClass().getName()).commitAllowingStateLoss();
	}
	
	public void addFragment(FragmentActivity parentActivity, int parentLayoutId, Fragment fragment) {
		parentActivity.getSupportFragmentManager().beginTransaction().add(parentLayoutId, fragment).commitAllowingStateLoss();
	}
	
	public void addFragment(Fragment parentFragment, int parentLayoutId, Fragment fragment) {
		FragmentTransaction ft = parentFragment.getChildFragmentManager().beginTransaction();				
		ft.add(parentLayoutId, fragment).commitAllowingStateLoss();				
	}
	
	public void addFragmentWithAnimation(Fragment parentFragment, int parentLayoutId, Fragment fragment) {
		FragmentTransaction ft = parentFragment.getChildFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_up, 0);
		ft.add(parentLayoutId, fragment).commitAllowingStateLoss();				
	}
	
	public void removeFragment(FragmentActivity parentActivity, Fragment fragment) {
		parentActivity.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
		fragment.onDestroy();
	}
	public void removeFragment(Fragment parentFragment, Fragment fragment) {
		parentFragment.getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
		fragment.onDestroy();
	}
	public void backFragment(Fragment parentFragment, Fragment fragment) {
		parentFragment.getChildFragmentManager().popBackStack();
		fragment.onDestroy();
	}
	
	public int getBackStackCount(FragmentActivity parentActivity){
	    int count = parentActivity.getSupportFragmentManager().getBackStackEntryCount();
	    return count;    
	}
	
	public int getBackStackCount(Fragment parentFragment){
		int count = parentFragment.getChildFragmentManager().getBackStackEntryCount();
		return count;    
	}
	
	public void clearBackStack(FragmentActivity parentActivity){
		try {
			parentActivity.getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			/*
			for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {            
				fragmentManager.popBackStackImmediate();			
			}    
			*/	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	public void clearBackStack(Fragment parentFragment){
		try {
			parentFragment.getChildFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			/*
			for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {            
				fragmentManager.popBackStackImmediate();			
			}    
			 */	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	static public void hideSoftKeyboad(Activity parentActivity, View view) {
		try {
			InputMethodManager imm = (InputMethodManager)parentActivity.getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	static public void hideSoftKeyboad(Fragment parentFragment, View view) {
		try {
			InputMethodManager imm = (InputMethodManager)parentFragment.getActivity().getSystemService(
					Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
