package com.wave.messenger.utility;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ErrorController {
    public static boolean DEBUG_MODE = true;

	public static void showError(String tag, String message, Exception e){
        if (DEBUG_MODE) {
            Log.e(tag, message, e);
        }
	}
	
	public static void showError(String msg, Exception e){
        if (DEBUG_MODE) {
            Log.e("DefaultTag", msg, e);
        }
	}

	public static void showMessage(String msg){
        if (DEBUG_MODE) {
            Log.e("DefaultTag", msg);
        }
	}

	public static void showMessage(String tag, String msg){
        if (DEBUG_MODE) {
            Log.e(tag, msg);
        }
	}

	public static synchronized void writeLog(Object... msg) {
        if (DEBUG_MODE) {
            Date date = new Date();

            StringBuilder sb = new StringBuilder();
            sb.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));

            StringBuilder sb1 = new StringBuilder();

            for (Object o : msg) {
                sb.append(" ").append("[").append(o).append("]");
                sb1.append("[").append(o).append("]").append(" ");
            }

            BufferedWriter resultFile = null;

            try {
                File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "/Talklogs");
                File file = new File(folder, new SimpleDateFormat("yyyy-MM-dd_HH").format(date) + ".txt");

                if (!folder.exists()) {
                    folder.mkdirs();
                }

                resultFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
                resultFile.write(sb.toString());
                resultFile.newLine();
                Log.e("hoya", sb1.toString());
            } catch (UnsupportedEncodingException ignored) {
            } catch (FileNotFoundException ignored) {
            } catch (IOException ignored) {
            } finally {
                if (null != resultFile) {
                    try {
                        resultFile.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
    }
}
