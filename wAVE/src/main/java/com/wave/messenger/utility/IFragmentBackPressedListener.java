package com.wave.messenger.utility;

public interface IFragmentBackPressedListener {
	void onBackPressed();
}