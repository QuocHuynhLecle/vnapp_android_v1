package com.wave.messenger.utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.share.HMMessengerShare;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.SharedUserInfo;

import org.json.JSONObject;

public class PushProc {

    private static Toast toast;
    private Context mContext;
    Bundle bundle;
    String pushData;
    private int ring_vibration;

    private static PushProc instance = null;

    public static PushProc getInstance() {
        if (instance == null) {
            instance = new PushProc();
        }
        return instance;
    }

    public void onReceivePushMsg(final Context context, Intent intent, String screenOnOff) {
        try {
            mContext = context;
            bundle = intent.getExtras();
            Log.d("???", "bundle : " + bundle.toString() + " / screenOnOff :" + screenOnOff);
            LogTrace.E("push data : " + bundle.toString());
            String useNoti = bundle.getString("useNoti");
            final String type = bundle.getString("type");
            int unread = Integer.valueOf(bundle.getString("unread"));

            SharedUserInfo sharedUserInfo = SharedUserInfo.getInstance(context);
            new Badge(context).setBadge(unread + sharedUserInfo.getAlaramCount());

            ErrorController.showMessage("hoya", "onReceivePushMsg::" + screenOnOff);

            if (SharedObject.getProperty_boolean(context, "push_ring", true) && SharedObject.getProperty_boolean(context, "push_vibration", true)) {
                ring_vibration = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            } else if (SharedObject.getProperty_boolean(context, "push_ring", true) && !SharedObject.getProperty_boolean(context, "push_vibration", true)) {
                ring_vibration = Notification.DEFAULT_SOUND;
            } else if (!SharedObject.getProperty_boolean(context, "push_ring", true) && SharedObject.getProperty_boolean(context, "push_vibration", true)) {
                ring_vibration = Notification.DEFAULT_VIBRATE;
            } else {
                ring_vibration = 0;
            }


            if (Fragment_Main.getInstance() != null) {
                //알림 카운터 새로고침.
                Fragment_Main.getInstance().setMainTotalCount();
            }

            if (MessengerInfo.getReceiveAlarm(context)) {
                if ("1".equals(useNoti)) {
                    LogTrace.E("useNoti");
                    KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                    final String title = bundle.getString("title");
                    final String senderName = bundle.getString("senderName");
                    String senderId = bundle.getString("senderId");
                    String sender_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + senderId;
                    int requestID = (int) System.currentTimeMillis();
                    final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    Intent notiIntent = new Intent();

                    //잠금화면
                    if ("0".equals(screenOnOff) || keyguardManager.inKeyguardRestrictedInputMode()) {
                        notiIntent.setClassName(context.getPackageName(), Constants.TIMEPRIME_ACTIVITY);
                        notiIntent.putExtra("pushmsg", savePushMessage(bundle, context));
                        notiIntent.putExtra("push", "pushCheck");
                        notiIntent.putExtra("pushNoti", 1);
                        notiIntent.setAction(Intent.ACTION_MAIN);
                        notiIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        notiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        final PendingIntent intent2 = PendingIntent.getActivity(context, requestID, notiIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        final PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
                        final int iconType, notiNum;
                        if ("moim".equals(type)) {
                            notiNum = 1500;
                            iconType = R.drawable.icon;
                        } else {
                            notiNum = 2000;
                            iconType = R.drawable.icon;
                        }
                        try {
                            Glide.with(context)
                                    .load(sender_url)
                                    .asBitmap()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .placeholder(R.drawable.ic_default_profile)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                            Bitmap lartgeIcon = getCircularBitmap(bitmap);

                                            NotificationCompat.Builder mNotification = new NotificationCompat.Builder(context)
                                                    .setLargeIcon(lartgeIcon)
                                                    .setSmallIcon(iconType)
                                                    .setTicker(senderName + " : " + title)
                                                    .setContentTitle(senderName)
                                                    .setContentText(title)
                                                    .setContentIntent(intent2)
                                                    .setAutoCancel(true)
                                                    .setDefaults(ring_vibration)
                                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                                    .setWhen(System.currentTimeMillis());

                                            nm.notify(notiNum, mNotification.build());
                                            wakeLock.acquire(3000);
                                        }
                                    });
                        } catch (Exception e) {
                            Bitmap lartgeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_default_profile);
                            NotificationCompat.Builder mNotification = new NotificationCompat.Builder(context)
                                    .setLargeIcon(lartgeIcon)
                                    .setSmallIcon(iconType)
                                    .setTicker(senderName + " : " + title)
                                    .setContentTitle(senderName)
                                    .setContentText(title)
                                    .setContentIntent(intent2)
                                    .setAutoCancel(true)
                                    .setDefaults(ring_vibration)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                    .setWhen(System.currentTimeMillis());

                            nm.notify(notiNum, mNotification.build());
                            wakeLock.acquire(3000);
                        }
                    }

                    //대화방에서 RoomID 체크
                    if (Statics.getRoomOnStop() || Statics.ROOMINFO == null || !bundle.getString("roomId").equals(Statics.ROOMINFO.getRoomId())) {
                        notiIntent.setClassName(context.getPackageName(), Constants.TIMEPRIME_ACTIVITY);
                        notiIntent.putExtra("pushmsg", savePushMessage(bundle, context));
                        notiIntent.putExtra("pushNoti", 1);
                        notiIntent.putExtra("push", "pushCheck");
                        notiIntent.setAction(Intent.ACTION_MAIN);
                        notiIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        notiIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        final PendingIntent intent2 = PendingIntent.getActivity(context, requestID, notiIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//                        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        final int iconType, notiNum;
                        if ("moim".equals(type)) {
                            notiNum = 1500;
                            iconType = R.drawable.icon;
                        } else {
                            notiNum = 2000;
                            iconType = R.drawable.icon;
                        }

                        try {
                            Glide.with(mContext)
                                    .load(sender_url)
                                    .asBitmap()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .placeholder(R.drawable.ic_default_profile)
                                    .error(R.drawable.ic_default_profile)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                            Bitmap lartgeIcon = getCircularBitmap(bitmap);

                                            NotificationCompat.Builder mNotification = new NotificationCompat.Builder(context)
                                                    .setFullScreenIntent(null, true)
                                                    .setLargeIcon(lartgeIcon)
                                                    .setSmallIcon(iconType)
                                                    .setTicker(senderName + " : " + title)
                                                    .setContentTitle(senderName)
                                                    .setContentText(title)
                                                    .setContentIntent(intent2)
                                                    .setAutoCancel(true)
                                                    .setDefaults(ring_vibration)
                                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                                    .setWhen(System.currentTimeMillis());

                                            nm.notify(notiNum, mNotification.build());
                                        }
                                    });
                        } catch (Exception e) {
                            Bitmap lartgeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_default_profile);
                            NotificationCompat.Builder mNotification = new NotificationCompat.Builder(context)
                                    .setFullScreenIntent(null, true)
                                    .setLargeIcon(lartgeIcon)
                                    .setSmallIcon(iconType)
                                    .setTicker(senderName + " : " + title)
                                    .setContentTitle(senderName)
                                    .setContentText(title)
                                    .setContentIntent(intent2)
                                    .setAutoCancel(true)
                                    .setDefaults(ring_vibration)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                    .setWhen(System.currentTimeMillis());

                            nm.notify(notiNum, mNotification.build());
                        }
                    }


                }
            }
        } catch (Exception ignored) {
        }
    }

    public static Bitmap getCircularBitmap(@NonNull Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    private String isAppRunning(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        String packageName = "";
        if (Build.VERSION.SDK_INT > 20) {
            packageName = am.getRunningAppProcesses().get(0).processName;
        } else {
            packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
        }

        return packageName;
    }

    private String savePushMessage(Bundle bundle, Context context) {


        String attachment = bundle.getString("attachment");
        String type = bundle.getString("type");
        String roomId = bundle.getString("roomId");
        String cId = bundle.getString("cId");
        String chatId = bundle.getString("chatId");
        String senderId = bundle.getString("senderId");
        String senderName = bundle.getString("senderName");
        String regDate = bundle.getString("regDate");
        String title = bundle.getString("title");
        String unread = bundle.getString("unread");
        String useNoti = bundle.getString("useNoti");

        JSONObject object = new JSONObject();

        try {


            object.put("attachment", attachment);
            object.put("type", type);
            object.put("roomId", roomId);
            object.put("cId", cId);
            object.put("chatId", chatId);
            object.put("senderId", senderId);
            object.put("senderName", senderName);
            object.put("regDate", regDate);
            object.put("title", title);
            object.put("unread", unread);
            object.put("useNoti", useNoti);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HMMessengerShare hMMessengerShare = HMMessengerShare.getInstance();
        hMMessengerShare.gcmMessage(object, context);

        pushData = object.toString();
        return pushData;
    }

    private int setNotiImage() {
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.ic_notification;
        } else {
            return R.drawable.ic_launcher;
        }*/
        return 0;
    }
}