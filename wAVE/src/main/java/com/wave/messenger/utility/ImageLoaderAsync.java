package com.wave.messenger.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.File;
import java.lang.ref.WeakReference;

public class ImageLoaderAsync extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;

    public ImageLoaderAsync(ImageView imageView) {
        imageViewReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        File image = new File(params[0]);
        Bitmap resultBitmap = null;
        try {
            BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(image.getPath(), bounds);

            if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
                return null;
            }

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = 8;

            resultBitmap = BitmapFactory.decodeFile(image.getPath(), opts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultBitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (imageViewReference != null && result != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(result);
            }
        }
    }//end of onPostExecute.
}
