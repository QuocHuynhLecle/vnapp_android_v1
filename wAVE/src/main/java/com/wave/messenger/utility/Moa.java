package com.wave.messenger.utility;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedUserInfo;
import com.wave.messenger.vo.VoMoimSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import moa.android.api.MOAClient;
import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketBody;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by apple on 2017. 4. 14..
 */

public class Moa {

    public static void logout(final Context context, final Handler ActivityHandler) {


        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("deviceType", "android");
            body.put("params", value);
            Log.i("TEST", "result : " + value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_LOGIN, 0x00010002, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    ActivityHandler.sendMessage(ActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.3 친구삭제
     *
     * @param context
     * @param tel
     * @param ActivityHandler
     */
    public static void friendDelete(final Context context, JSONArray tel, final Handler ActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
//            value.put("userId", MessengerInfo.getUserId(context));
//            value.put("tel", tel);
            body.put("params", tel);
            body.put("userId", MessengerInfo.getUserId(context));
            Log.i("TEST", "result : " + value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_DELETE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    ActivityHandler.sendMessage(ActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.4친구 차단/해제
     *
     * @param context
     * @param users
     * @param mode
     * @param ActivityHandler 2017.08.15
     */
    public static void blockList(final Context context, String users, int mode, final Handler ActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", users);
            value.put("mode", mode);
            body.put("params", value);

            Log.i("TEST", "result : " + value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_BLOCK, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    ActivityHandler.sendMessage(ActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.5 친구 숨김/해제
     *
     * @param context
     * @param users
     * @param ActivityHandler 2017.08.14
     */
    public static void hideList(final Context context, String users, int mode, final Handler ActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("users", users);
            value.put("mode", mode);
            body.put("params", value);
            Log.i("TEST", "result : " + value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_HIDDEN, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    ActivityHandler.sendMessage(ActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.10 차단 리스트 조회
     *
     * @param context
     * @param mainActivityHandler
     */
    public static void blockListRequest(final Context context, final Handler mainActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_BLOCK_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActivityHandler.sendMessage(mainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 3.11 숨김 리스트 조회
     *
     * @param context
     * @param mainActivityHandler 2017.08.14
     */
    public static void hideListRequest(final Context context, final Handler mainActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            //value.put("userId", MessengerInfo.getUserId(context));
            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);
            //MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_LIST, body);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_HIDDEN_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActivityHandler.sendMessage(mainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 4.35 알림방 카운트 요청
     *PTG : 0x00040000, PTC : 0x00040039
     PTG_IMS_CHATROOM, PTC_IMS_CHATROOM_ALARM_COUNT
     * @param context
     * @param mainActivityHandler 2017.09.28
     */
    public static void chatroomAlarmCount(final Context context ,final Handler mainActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("roomId", roomId);
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, 0x00040039, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActivityHandler.sendMessage(mainActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");

//                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
    * 4.9 대화방 정보 요청
     *
     * 모임 챗 이동.
    * */

    public static void chatRoomInfoRequest(final Context context, String roomid, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", roomid);
            value.put("uinfo", "1");
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_INFO, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 180000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 5.15 알림방 메시지 삭제
     *
     * chatId: 챗 아이디가 없으면 전체 알림방 메시지를 삭제함.
     *
     * PTG : 0x00050000, PTC : 0x00050020
      PTG_IMS_CHAT, PTC_IMS_CHAT_DELETE
     */
    public static void alramDeleteRequest(final Context context, String chatId, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("roomId", SharedObject.getProperty_string(context, Constants.CHAT_ROOM_ID, ""));
            value.put("chatId", chatId);

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, 0x00050020, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 5.16 알림방 메시지 리스트 요청
     *
     * PTG : 0x00050000, PTC : 0x00050021
     * PTG_IMS_CHAT, PTC_IMS_ALARM_LIST
     */
    public static void alarmListRequest(final Context context, String cid,final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("cId", cid);

            value.put("roomId", SharedObject.getProperty_string(context, Constants.CHAT_ROOM_ID, ""));
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHAT, 0x00050021, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.1 모임  리스트
     * <p>
     * 탭2_나의모임리스트
     * 탭2_편집_숨김모임관리
     */
    public static void moimListRequest(final Context context, String md, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            //value.put("userId", MessengerInfo.getUserId(context));
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("md", md);

            body.put("params", value);
            //MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_LIST, body);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.4 모임 프로필
     * (모임 정보만)
     * <p>
     * PTG : 0x00080000, PTC : 0x00080014
     * PTG_IMS_MOIM, PTC_IMS_MOIM_PROFILE
     *
     * @param mainActvtHandler 2017.05.11 leeyunsu.
     */
    public static void moimProfileRequest(final Context context, final Handler mainActvtHandler) {
        Log.e("Moa", "MoimProfileRequest : " + " getUserId: " + MessengerInfo.getUserId(context));

        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("mmId", "63a7dcb9-cc23-4e9d-94da-4aac37a2b08c");   //TODO 테스트 값을 위한 임의처리
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_PROFILE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.5 모임 검색 [완료]
     * PTG : 0x00080000, PTC : 0x00080004
     * PTG_IMS_MOIM, PTC_IMS_MOIM_SEARCH
     */
    public static void moimSearchRequest(final Context context, String md, String mmId, String srchWrd, String mmTg, final Handler mainActvtHandler) {
        Log.e("Moa", "moimSearchRequest : " + " mmId: " + mmId + " getUserId: " + MessengerInfo.getUserId(context) + " srchWrd:" + srchWrd);

        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("md", md);    //0:검색어로검색 1:주제별모임찾기 2:새로시작한모임
            value.put("mmId", mmId);    //첫 페이지 번호 : 0
            value.put("srchWrd", srchWrd);  //검색어로검색 시에만 사용
            value.put("mmTg", mmTg);        //주제별모임찾기 시에만 사용 1:주식 2:국내파생 3:해외파생 4:해외주식 5:금융상품 6:기타

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_SEARCH, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 15000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.6 모임 생성 [완]
     * <p>
     * PTG : 0x00080000, PTC : 0x00080002
     * PTG_IMS_MOIM, PTC_IMS_MOIM_CREATE
     */
    public static void moimCreate(final Context context, String mmNm, String mmStTy, String mmTg, String xprt, String imgTy, String imgOrg, String imgTmb,
                                  String intr, String qstn, String mmPw, String maxUsr, String wrtTy,
                                  String opnTy, String entTy, String mXprt, String mXprtTy, String mXprtRsn,
                                  String mXprtGrpId, String mXprtEmpNo, String mXprtLoc, String stMm, final Handler mainActvtHandler) {
        Log.e("MOA", "moimCreate");
        try {

            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", uuid.toString());
            value.put("mmNm", mmNm);        //모임 제목
            value.put("mmStTy", mmStTy);    //0:상담모임, 1:종목추천모임
            value.put("mmTg", mmTg);     // 1 : 주식 2 : 국내파생 3 : 해외파생 4 : 해외주식 5 : 금융상품 6 : 기타
            value.put("xprt", xprt);     //전문가 0:미신청, 1:신청
            value.put("imgTy", imgTy);   //이미지 타입  0:이미지 없음, 1:있음
            value.put("imgOrg", imgOrg); //모임 이미지 원본
            value.put("imgTmb", imgTmb); //모임 썸네일 원본
            value.put("intr", intr);     //모임 설명
            value.put("qstn", qstn);    //모임 가입질문
            value.put("mmPw", mmPw);    //모임 가입비밀번호
            value.put("maxUsr", getMaxUser(maxUsr)); //가입 최대 사용자 기본 100
            value.put("wrtTy", wrtTy);   //글쓰기 권한 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
            value.put("opnTy", opnTy);   //공개 제한(int) 0:비공개, 1:공개, 2:모임명만 공개(기본),
            value.put("entTy", entTy);   //가입 제한(int) 0:자동가입(기본), 1:승인, 2:비밀번호입력

            value.put("xprt", mXprt);   //전문가 신청(int)            0:미신청, 1:신청
            value.put("xprtTy", mXprtTy);   //전문가 타입(int)            0:매니저, 1:애널리스트, 2:전문가
            value.put("xprtRsn", mXprtRsn); //전문가 신청사유            전문가 그룹이 아닐 경우 신청사유 입력
            value.put("xprtGrpId", mXprtGrpId); //전문가 그룹 아이디(int)            전문가 타입이 전문가일 때 신청할 전문가 그룹 아이디 입력
            value.put("xprtEmpNo", mXprtEmpNo); //사원번호            전문가 타입이 매니저, 애널리스트 일 경우 사원번호 입력
            value.put("xprtLoc", mXprtLoc); //부서 혹은 담당섹터            전문가 타입이 매니저, 애널리스트 일 경우 부서 혹은 담당섹터 입력
            value.put("stMm", stMm);      //직원전용모임여부          0:일반모임, 1:직원전용모임

            body.put("params", value);
            //MOAClient.getInstance().writePacket(packetPTG, packetPTC, body);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_CREATE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.8 모임 설정
     * PTG : 0x00080000, PTC : 0x00080005
     * PTG_IMS_MOIM, PTC_IMS_MOIM_CONFIG
     * 모임수정
     */
    public static void moimModify(final Context context, String mmId, String mmNm, String imgTy, String imgOrg, String imgTmb,
                                  final Handler mainActvtHandler) {
        Log.e("MOA", "moimCreate");
        try {

            //UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", mmId);
            value.put("mmNm", mmNm);        //모임 제목
           /* value.put("mmStTy", mmStTy);    //0:상담모임, 1:종목추천모임
            value.put("mmTg", mmTg);     // 1 : 주식 2 : 국내파생 3 : 해외파생 4 : 해외주식 5 : 금융상품 6 : 기타
            value.put("xprt", xprt);     //전문가 0:미신청, 1:신청*/
            value.put("imgTy", imgTy);   //이미지 타입  0:이미지 없음, 1:있음
            value.put("imgOrg", imgOrg); //모임 이미지 원본
            value.put("imgTmb", imgTmb); //모임 썸네일 원본
           /* value.put("intr", intr);     //모임 설명
            value.put("qstn", qstn);    //모임 가입질문
            value.put("mmPw", mmPw);    //모임 가입비밀번호
            value.put("maxUsr", getMaxUser(maxUsr)); //가입 최대 사용자 기본 100
            value.put("wrtTy", wrtTy);   //글쓰기 권한 0:모든멤버(기본), 1:리더, 2:리더와 공동리더
            value.put("opnTy", opnTy);   //공개 제한(int) 0:비공개, 1:공개, 2:모임명만 공개(기본),
            value.put("entTy", entTy);   //가입 제한(int) 0:자동가입(기본), 1:승인, 2:비밀번호입력

            value.put("xprt", mXprt);   //전문가 신청(int)            0:미신청, 1:신청
            value.put("xprtTy", mXprtTy);   //전문가 타입(int)            0:매니저, 1:애널리스트, 2:전문가
            value.put("xprtRsn", mXprtRsn); //전문가 신청사유            전문가 그룹이 아닐 경우 신청사유 입력
            value.put("xprtGrpId", mXprtGrpId); //전문가 그룹 아이디(int)            전문가 타입이 전문가일 때 신청할 전문가 그룹 아이디 입력
            value.put("xprtEmpNo", mXprtEmpNo); //사원번호            전문가 타입이 매니저, 애널리스트 일 경우 사원번호 입력
            value.put("xprtLoc", mXprtLoc); //부서 혹은 담당섹터            전문가 타입이 매니저, 애널리스트 일 경우 부서 혹은 담당섹터 입력*/


            body.put("params", value);
            //MOAClient.getInstance().writePacket(packetPTG, packetPTC, body);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_CONFIG, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 최대사용자 설정
     * 0: 50, 1:100, 2: 500 을 리턴한다, 기본 : 100
     *
     * @param maxUsr
     * @return
     */
    public static String getMaxUser(String maxUsr) {
        switch (maxUsr) {
            case "0":
                return "50";
            case "1":
                return "100";
            case "2":
                return "500";
        }
        return "0";
    }


    /**
     * 7.7 모임 삭제
     *
     * @param context
     * @param mainActvtHandler 2017.05.11 leeyunsu.
     */
    public static void moimDelete(final Context context, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_DELETE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.8 모임 설정
     * PTC_IMS_MOIM_CONFIG
     * <p>
     * 2017.05.11 leeyunsu.
     */
    public static void moimConfig(final Context context, VoMoimSet VoMoimSet, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("mmNm", VoMoimSet.getMmNm());
            value.put("imgTy", VoMoimSet.getImgTy());
            value.put("imgOrg", VoMoimSet.getImgOrg());
            value.put("imgTmb", VoMoimSet.getImgTmb());
            value.put("intr", VoMoimSet.getIntr());
            value.put("maxUsr", VoMoimSet.getMaxUsr());
            value.put("opnTy", VoMoimSet.getOpnTy());
            value.put("entTy", VoMoimSet.getEntTy());
            value.put("covTy", VoMoimSet.getCovty());
            value.put("acptTy", VoMoimSet.getAcptTy());
            value.put("ivtTy", VoMoimSet.getIvtTy());
            value.put("notiTy", VoMoimSet.getNotiTy());
            value.put("wrtTy", VoMoimSet.getWrtTy());
            value.put("abmTy", VoMoimSet.getAbmTy());
            value.put("rpyTy", VoMoimSet.getRpyTy());
            value.put("delTy", VoMoimSet.getDelTy());
            value.put("frcTy", VoMoimSet.getFrcTy());
            value.put("mnuTy", VoMoimSet.getMnuTy());
            value.put("opchtTy", VoMoimSet.getOpchtTy());
            value.put("snglTy", VoMoimSet.getSnglTy());
            value.put("qstn", VoMoimSet.getQstn());
            value.put("mmPw", VoMoimSet.getMmPw());

            value.put("csltTy", VoMoimSet.getCsltTy()); //종목상담여부
            value.put("ldngTy", VoMoimSet.getLdngTy()); //종목리딩여부

            //value.put("chtPd", null);
            //value.put("fl5Pd", null);
            //value.put("fl10Pd", null);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_CONFIG, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.9 모임 멤버 리스트
     * <p>
     * PTG : 0x00080000, PTC : 0x00080025
     * PTG_IMS_MOIM, PTC_IMS_MOIM_MEMBERLIST
     * <p>
     * 2017.06.12 leeyunsu.
     */
    public static void moimMemberlistRequest(final Context context, String md, String ordTy,String ord, final Handler mainActvtHandler) {

        Log.e("Moa","MoimProfileRequest ordTy: "+ordTy +" ord: "+ord);

        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("userId", usid);
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("md", md);    //"0":가입신청자 리스트 "1":가입된 사용자 리스트 "2":차단된 사용자 리스트
            value.put("ordTy", ordTy);  //정렬 기준값 0:이름, 1:가입일, 2:레벨
            value.put("ord", ord);      //정렬값(int)  0:내림차순, 1:올림차순

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_MEMBERLIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.10 모임 초대
     *
     * @param context
     * @param ivtUsr
     * @param ActivityHandler 2017.08.04
     */
    public static void moimInvite(final Context context, String ivtUsr, final Handler ActivityHandler) {

        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("ivtUsr", ivtUsr);   //초대할 사용자 아이디 리스트

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_INVITE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    ActivityHandler.sendMessage(ActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.11 모임 강제탈퇴
     * 리더나 공동 리더
     * <p>
     * PTG : 0x00080000, PTC : 0x00080008
     * PTG_IMS_MOIM, PTC_IMS_MOIM_FORCE_EXIT
     * <p>
     * 2017.05.11 leeyunsu.
     */
    public static void moimForceExit(final Context context, String mmExtUsr, String mmUsrBlk, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("mmExtUsr", mmExtUsr);   //강퇴할 사용자 아이디 리스트
            value.put("mmUsrBlk", mmUsrBlk);   //강제퇴장 후 차단 여부(int) 0:차단안함, 1:차단

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_FORCE_EXIT, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.11 모임대화방 강제탈퇴
     * 리더나 공동 리더
     * <p>
     * PTG : 0x00040000, PTC : 0x00040020
     * PTG_IMS_CHATROOM, PTC_IMS_CHATROOM_USEROUT
     * <p>
     * 2018.01.12 jaedeuk.
     */
    public static void chatRoomForceExit(final Context context, String roomId, String userOutId, String roomUserName, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("roomId", roomId);               //강퇴할 대화방 룸아이디
            value.put("userOutId", userOutId);         //강퇴할 사용자 아이디
            value.put("roomUserName", roomUserName);   //강퇴할 사용자 사용자대화명

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_USEROUT, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
//                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.12. 모임 가입하기
     * PacketTypes.PTC_IMS_MOIM_APPLY
     * PTG : 0x00080000, PTC : 0x00080017
     * PTG_IMS_MOIM, PTC_IMS_MOIM_APPLY
     */
    public static void moimApply(final Context context, String answ, String mmPw, final Handler mainActvtHandler) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();


            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("usId", "test0004");
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("answ", answ);
            value.put("mmPw", mmPw);

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_APPLY, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.13 모임 탈퇴
     * 모임 설정화면에서 스스로 탈퇴
     * <p>
     * PTG : 0x00080000, PTC : 0x00080019
     * PTG_IMS_MOIM, PTC_IMS_MOIM_EXIT
     */
    public static void moimExit(final Context context, final Handler mainActvtHandler) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();


            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("usId", "test0004");
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080019, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 180000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.14 모임 가입 승인/거절
     * <p>
     * PTG : 0x00080000, PTC : 0x00080018
     * PTG_IMS_MOIM, PTC_IMS_MOIM_ALLOW
     */
    public static void moimAllowRequest(final Context context, String alwUsr, String alw, final Handler mainActvtHandler) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("alwUsr", alwUsr);
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            //value.put("rmId", alwUsr);
            value.put("alw", alw);

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_ALLOW, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }

            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.15 모임 글 리스트
     *
     *
     md 값이 없을시 0으로 처리됨
     0:모임글리스트(기본)   공지사항, 모임타임라인
     1:모임공지사항
     2:모임작성글
     3:모임작성댓글
     4:모임별작성글   내가쓴글 리스
     5:모임별작성댓글

     * <p>
     * 2017.05.16 leeyunsu.
     * 2017.09.25 모임글검색 기능 추가. 파라미터 kw
     */
    public static void moimBoardListRequest(final Context context, String userId, String md, String mmId, String msgId, String kw ,final Handler ActvtHandler) {

        Log.e("MOA", "moimBoardListRequest " + userId);
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            //value.put("userId", MessengerInfo.getUserId(context));

            value.put("userId", userId);
            if (md.equals("2") || md.equals("3")) {
                //2:모임작성글 3:모임작성댓글 요청시 사용 안 함
            } else if (md.equals("4")) {
                value.put("mmId", mmId);
            } else {
                value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            }

            value.put("kw", kw);    //검색

            value.put("md", md);    //md 값이 없을시 0으로 처리됨 0:모임글리스트(기본) 1:모임공지사항 2:모임작성글 3:모임작성댓글 4:모임별작성글 5:모임별작성댓글
            value.put("msgId", msgId);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_BOARD_LIST, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.16 모임 공지사항 설정/해제
     * <p>
     * PTG : 0x00080000, PTC : 0x00080006
     * PTG_IMS_MOIM, PTC_IMS_MOIM_NOTICE
     *
     * @param msgId 메시지 키값
     * @param noti  0:일반, 1:공지
     *              <p>
     *              2017.05.11 leeyunsu.
     */
    public static void moimNotice(final Context context, String msgId, String noti, final Handler ActvtHandler) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("msgId", msgId);
            value.put("noti", noti);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_NOTICE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.17 모임 글 삭제
     * <p>
     * PTG : 0x00080000, PTC : 0x00080010
     * PTG_IMS_MOIM, PTC_IMS_MOIM_BOARD_DELETE
     * <p>
     * <p>
     * 2017.05.31 leeyunsu.
     */
    public static void moimBoardDelete(final Context context, String msgId, String mmid, final Handler ActvtHandler) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("mmId", mmid);
            value.put("msgId", msgId);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_BOARD_DELETE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.18 모임 글 쓰기
     * 댓글쓰기
     * PTG : 0x00080000, PTC : 0x00080009
     * PTG_IMS_MOIM, PTC_IMS_MOIM_BOARD_WRITE
     */
    public static void moimBoardWriteRequest(final Context context, String title, String msg
            , String grpNo, String prntNo, String bbsDph, String bbsOrd, String noti, String prvTy, String stCd, String stNm, final Handler ActvtHandler) {
        try {


            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("moimId", "63a7dcb9-cc23-4e9d-94da-4aac37a2b08c");   //TODO 테스트 값을 위한 임의처리
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("msgId", uuid);

            value.put("grpNo", grpNo);      //최상위 글 번호(int) 댓글 쓰기인 경우 입력
            value.put("prntNo", prntNo);    //상위 글 번호(int)  댓글 쓰기인 경우 입력
            value.put("bbsDph", bbsDph);    //상위 글 depth(int) 댓글 쓰기인 경우 입력
            value.put("bbsOrd", bbsOrd);    //상위 글 order(int) 댓글 쓰기인 경우 입력

            value.put("ttl", title);
            value.put("msg", msg);      //글자 텍스트 json array
            value.put("msgTy", "0");    //0:일반 텍스트, 기본
            value.put("noti", noti);     //0:일반, 1:공지 2:긴급공지
            value.put("atch", "");      //Json 스트링

            value.put("prvTy", prvTy);  //prvTy 비공개 여부 0:공개, 1:비공개 종목대화방에 게시인 경우 0:공개로 설정
            value.put("stCd", stCd);    //stCd 주식종목코드
            value.put("stNm", stNm);    //stNm 주식종목명

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_BOARD_WRITE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 7000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.19 모임 글 조회
     * PTG : 0x00080000, PTC : 0x00080022
     * PTG_IMS_MOIM, PTC_IMS_MOIM_BOARD_READ
     * upcnt
     읽음 카운트 증가여부 0:증가안함, 1:증가처리 (2018-05-11추가)
     */
    public static void moimBoardReadRequest(final Context context, final Handler ActvtHandler , String upcnt) {
        try {


            ErrorController.showMessage( "##moimBoardReadRequest mmId: " + SharedObject.getProperty_string(context, Constants.MOIM_ID, null)
                    + " msgId: " + SharedObject.getProperty_string(context, Constants.MOIM_MSGID, null));

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("msgId", SharedObject.getProperty_string(context, Constants.MOIM_MSGID, null));
            value.put("upcnt", upcnt);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080022, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    ErrorController.showMessage("##onData moaData.toString() "+moaData.toString());

                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void moimBoardReadReplyRequest(final Context context, final Handler ActvtHandler) {
        try {

            //UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            //value.put("mmId", "63a7dcb9-cc23-4e9d-94da-4aac37a2b08c");   //TODO 테스트 값을 위한 임의처리
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_RE_ID, null));
            value.put("msgId", SharedObject.getProperty_string(context, Constants.MOIM_RE_MSGID, null));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080022, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.21 모임 멤버레벨 설정
     * PTG : 0x00080000, PTC : 0x00080016
     * PTG_IMS_MOIM, PTC_IMS_MOIM_USER_LEVEL
     * <p>
     * 리더를 위임할 경우 사용
     * 리더를 위임할 경우 userLevel 값 1 설정
     * 위임할 사용자 아이디(trgtUsr) 입력
     * <p>
     * 2017.07.11 leeyunsu.
     */
    public static void moimUserLeverRequest(final Context context, String usrLv, String trgtUsr, final Handler ActvtHandler) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("usrLv", usrLv);      //0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)
            value.put("trgtUsr", trgtUsr);  //여러명일 경우 콤마텍스트로 보냄

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_USER_LEVEL, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.22 모임 공지사항 수신(현재 사용안함)
     * PTG : 0x00080000, PTC : 0x00080021
     * PTG_IMS_MOIM, PTC_IMS_MOIM_NOTICE_RECV
     */
    public static void moimNoticeRecvRequest(final Context context, final Handler ActvtHandler) {
        try {
            //Log.e("","moimBoardReadRequest mmId: "+SharedObject.getProperty_string(context, Constants.MOIM_ID,null)
            //        +" msgId: "+SharedObject.getProperty_string(context, Constants.MOIM_MSGID,null) );
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("msgId", SharedObject.getProperty_string(context, Constants.MOIM_MSGID, null));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_NOTICE_RECV, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.23 모임 글 감정표현
     * PTG : 0x00080000, PTC : 0x00080026
     * PTG_IMS_MOIM, PTC_IMS_MOIM_EMOTION
     */
    public static void moimEmotionRequest(final Context context, String mmId, String msgId, String emt, final Handler ActvtHandler) {
        try {

            Log.e("MOA", "moimEmotionRequest getUserId " + MessengerInfo.getUserId(context));

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", mmId);    //모임 키값
            value.put("msgId", msgId);  //메시지 키값
            value.put("emt", emt);  //0:기존감정표현취소, 1:좋아요, 2:싫어요

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_EMOTION, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.24 모임 글 수정
     * PTG : 0x00080000, PTC : 0x00080027
     * PTG_IMS_MOIM, PTC_IMS_MOIM_BOARD_UPDATE
     */
    public static void moimBoardUpdateRequest(final Context context, String ttl, String msg, String noti, final Handler ActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, ""));    //모임 키값
            value.put("msgId", SharedObject.getProperty_string(context, Constants.MOIM_MODIFY_MSGID, ""));  //메시지 키값
            value.put("ttl", ttl);  //메시지에서 첫줄
            value.put("msg", msg);  //메시지
            value.put("noti", noti);  //0:일반, 1:공지 2:긴급공지
            value.put("msgTy", "0");  //메시지타입


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_BOARD_UPDATE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.25 모임 숨김/해제
     * <p>
     * PTG : 0x00080000, PTC : 0x00080028
     * PTG_IMS_MOIM, PTC_IMS_MOIM_HIDDEN
     */
    public static void moimHiddenRequest(final Context context, String mmid, String hdn, final Handler ActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID,null));        //모임 키값
            value.put("mmId", mmid);        //모임 키값
            value.put("hdn", hdn);      //숨김 여부(int) 0:안숨김, 1:숨김

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_HIDDEN, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.26 모임 글 신고 [완료]
     * <p>
     * PTG : 0x00080000, PTC : 0x00080029
     * PTG_IMS_MOIM, PTC_IMS_MOIM_REPORT
     */
    public static void moimReportRequest(final Context context, String rsn, final Handler ActvtHandler) {
        try {

            Log.e("moimReportRequest", "rsn" + rsn);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("mmId", mmId);    //모임 키값
            //value.put("msgId", msgId);  //메시지 키값

            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));        //모임 키값
            value.put("msgId", SharedObject.getProperty_string(context, Constants.MOIM_MSGID, null));    //메시지 키값
            value.put("rsn", rsn);      //신고사유

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080029, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.27 모임 타임라인
     * PTG : 0x00080000, PTC : 0x00080031
     * PTG_IMS_MOIM, PTC_IMS_MOIM_TIMELINE
     */
    public static void moimTimeLineRequest(final Context context, String mmId, String msgId, final Handler ActvtHandler) {
        try {

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", mmId);
            value.put("msgId", msgId);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_TIMELINE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 15000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.29 모임 글 차단 [완료]
     * <p>
     * PTG : 0x00080000, PTC : 0x00080030
     * PTG_IMS_MOIM, PTC_IMS_MOIM_BBS_BLOCK
     */
    public static void moimBbsBlockRequest(final Context context, String mmBbsBlk, final Handler ActvtHandler) {
        try {

            Log.e("moimBbsBlockRequest", "mmBbsBlk" + mmBbsBlk);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));        //모임 키값
            value.put("mmBbsBlk", mmBbsBlk);      //글 차단 여부(int) 0:허용, 1:차단

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_BBS_BLOCK, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    /*
    7.20 모임 금칙어 리스트(현재 사용안함)
    7.21 모임 멤버레벨 설정
    7.23 모임 글 감정표현
    7.25 모임 숨김/해제
    7.28 모임 전문가 리스트
    */

    /**
     * 7.30 모임 사용자 프로필
     * <p>
     * PTG : 0x00080000, PTC : 0x00080033
     * PTG_IMS_MOIM, PTC_IMS_MOIM_USER_PROFILE
     */
    public static void moimUserProfileRequest(final Context context, String userId, String mmId, final Handler ActvtHandler) {
        try {

            Log.e("moimUserProfileRequest", "userId" + userId);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", userId);
            value.put("mmId", mmId);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080033, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.31 모임 사용자 프로필 수정
     * <p>
     * PTG : 0x00080000, PTC : 0x00080034
     * PTG_IMS_MOIM, PTC_IMS_MOIM_USER_PROFILE_UPDATE
     */
    public static void moimUserProfileUpdateRequest(final Context context, String usNm, String pfImg, String pfImgTy, String pfSbj, final Handler ActvtHandler) {
        try {

            Log.e("moimUserProfileUpdate", "userId");

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));

            //Log.e("moimUserProfileUpdate", "MessengerInfo.getUserIdKey(): "+MessengerInfo.getUserIdKey());
            //value.put("userId", MessengerInfo.getUserIdKey()); //TODO 보안키 수정

            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));

            value.put("usNm", usNm);        //사용자 이름
            value.put("pfImg", pfImg);      //pfImg 프로필 이미지 경로
            value.put("pfImgTy", pfImgTy);  //프로필 이미지 종류(int) 0:사용안함 1:이미지
            value.put("pfSbj", pfSbj);      //프로필 상태메시지

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080034, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.33 모임 차단해제
     *
     * @param context
     * @param mmUnblkUsr
     * @param mainActvtHandler
     */
    public static void moimUserUnblock(final Context context, String mmUnblkUsr, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("mmUnblkUsr", mmUnblkUsr);   //차단해제 할 사용자 아이디

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080036, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.35 모임알림 설정
     * PTC_IMS_MOIM_NOTI_CONFIG
     * <p>
     * 2017.05.11 leeyunsu.
     */
    public static void moimNotiConfig(final Context context, String useMmPush, String useMmBbsPush, String useMmRpyPush, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID, null));
            value.put("useMmPush", useMmPush);
            value.put("useMmBbsPush", useMmBbsPush);
            value.put("useMmRpyPush", useMmRpyPush);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080038, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.36 모임 전문가 그룹 리스트
     * PTC_IMS_MOIM_NOTI_CONFIG
     * <p>
     * 2017.05.11 leeyunsu.
     */
    public static void moimGrouptList(final Context context, final Handler mainActvtHandler) {
        PacketBody body = new PacketBody();
        JSONObject value = new JSONObject();

            /*value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", SharedObject.getProperty_string(context, Constants.MOIM_ID,null));
            value.put("useMmPush", useMmPush);
            value.put("useMmBbsPush", useMmBbsPush);
            value.put("useMmRpyPush", useMmRpyPush);

        body.put("params", value);*/

        MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080039, body, new MOAClient.OnDataListener() {
            @Override
            public void onData(MOAData moaData) {
                MOALog.w("onData:" + moaData.toString());
                //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
            }

            @Override
            public void onTimeOut() {
                MOALog.w("onTimeOut()");
                Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(int error) {
                //현재 사용 안함
            }


        }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

    }


    /**
     * 7.37 모임 관리(상단고정)
     * <p>
     * 2017.07.17 leeyunsu.
     */
    public static void moimOrderRequest(final Context context, String mmmId, String mmOrd, final Handler mainActvtHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("mmId", mmmId);
            value.put("mmOrd", mmOrd);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080040, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.38 모임 초대장 리스트
     *
     * @param context
     * @param activityHandler 2017.08.04
     */

    public static void moimInviteList(final Context context, final Handler activityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080041, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    activityHandler.sendMessage(activityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 8.1 개인정보 요청
     *
     * @param context
     * @param activityHandler 2017.08.08
     */
    public static void UserInfo(final Context context, final Handler activityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_USERINFO, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    activityHandler.sendMessage(activityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 8.2 상태메시지 변경
     *
     * @param context
     * @param activityHandler
     * @param profile_subject 2017.08.11
     */
    public static void StateMessageChange(final Context context, final Handler activityHandler, String profile_subject) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("profile_subject", profile_subject);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_TITLE, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    activityHandler.sendMessage(activityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 8.3 프로필 사진 변경
     PTG : 0x00030000, PTC : 0x00030005
     PTG_IMS_USER, PTC_IMS_USER_CHANGE_PHOTO
     *
     */
    /*public static void moimChangePhotoRequest(final Context context, String photoType, String photoData, final Handler ActvtHandler) {
        try {

            Log.e("moimChangePhotoRequest","photoType" +photoType+" photoData:"+photoData);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("photoType", photoType);  //0: 없음, 1:사진데이터 사용
            value.put("photoData", photoData);  //사진 파일 URL

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_USER_CHANGE_USERNAME, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }
                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/


    /**
     * 8.5 사용자 이름 변경
     * PTG : 0x00030000, PTC : 0x00030009
     * PTG_IMS_USER, PTC_IMS_USER_CHANGE_USERNAME
     */
    public static void UsernameChangeRequest(final Context context, String userName, final Handler ActvtHandler) {
        try {

            Log.e("ChangeUsername"," userName:"+userName);

            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();
            value.put("userId", MessengerInfo.getUserId(context));
            value.put("userName", userName);

            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_CHANGE_USERNAME, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    ActvtHandler.sendMessage(ActvtHandler.obtainMessage(moaData.ptc, moaData));
                }
                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static void UserPushRequest(final Context context, int useNoti, final Handler mActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("device", "mobile");
            value.put("useNoti", useNoti);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_NOTI_CONFIG, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static void UserPushRingRequest(final Context context, String sound, final Handler mActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("device", "mobile");
            value.put("sound", sound);


            body.put("params", value);
            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, PacketTypes.PTC_IMS_USER_PUSH_SOUND_CONFIG, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 7.13 모임 탈퇴
     * 전문가 리스트에서 탈퇴
     * <p>
     * PTG : 0x00080000, PTC : 0x00080019
     * PTG_IMS_MOIM, PTC_IMS_MOIM_EXIT
     */
    public static void moimExitProfessional(final Context context, final Handler mainActvtHandler, final String mmid) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();


            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("usId", "test0004");
            value.put("mmId", mmid);
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, 0x00080019, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 7.12. 모임 가입하기
     * 전문가 리스트에서 가입
     * PacketTypes.PTC_IMS_MOIM_APPLY
     * PTG : 0x00080000, PTC : 0x00080017
     * PTG_IMS_MOIM, PTC_IMS_MOIM_APPLY
     */
    public static void moimApplyProfessional(final Context context, String answ, String mmPw, final Handler mainActvtHandler, String mmid) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();


            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("usId", "test0004");
            value.put("mmId", mmid);
            value.put("answ", answ);
            value.put("mmPw", mmPw);

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_MOIM, PacketTypes.PTC_IMS_MOIM_APPLY, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mainActvtHandler.sendMessage(mainActvtHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 4.12 오픈대화방 입장
     * 전문가 리스트에서 가입
     * PacketTypes.PTC_IMS_MOIM_APPLY
     * PTG : 0x00080000, PTC : 0x00080017
     * PTG_IMS_MOIM, PTC_IMS_MOIM_APPLY
     */
    public static void opentalkenter(final Context context, final String roomId, String roomName, String roomType, final Handler mActivityHandler) {
        try {
            UUID uuid = UUID.randomUUID();
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();


            value.put("userId", MessengerInfo.getUserId(context));
            //value.put("usId", "test0004");
            value.put("roomId", roomId);
            value.put("roomUserName", MessengerInfo.getUserName(context));
            value.put("roomName", roomName);
            value.put("roomType", roomType);

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, PacketTypes.PTC_IMS_CHATROOM_ENTER_ROOM, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            },180000); //기본 180초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param context
     * @param mActivityHandler
     * 종목대화방 리스트 요청
     */
    public static void JongMokList(final Context context, final Handler mActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_CHATROOM, 0x00040038, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void GroupDelete(final Context context, int gId, int bId, int buddyId, final Handler mActivityHandler){
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("gId", gId);
            value.put("bId", bId);
            value.put("buddyId", buddyId);
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_GROUP, 0x00060008, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    //Toast.makeText(context, moaData.body.toJSONString(), Toast.LENGTH_SHORT).show();
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 친구추천 리스트 불러오기
     * @param context
     * @param mActivityHandler
     */
    public static void RecommendFriendList(final Context context, final Handler mActivityHandler){
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));

            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, 0x00020017, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void friendsAdd(final Context context, String name, String id, final Handler mActivityHandler ){
        try {
            PacketBody body = new PacketBody();
            JSONObject value = new JSONObject();

            value.put("userId", MessengerInfo.getUserId(context));
            value.put("buddyName", name);
            value.put("buddyId", id);
            body.put("params", value);

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_ADDRESS, PacketTypes.PTC_IMS_ADDRESS_ADDBUDDY, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    MOALog.w("onData:" + moaData.toString());
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    MOALog.w("onTimeOut()");
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int error) {
                    //현재 사용 안함
                }


            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 회원 탈퇴
    public static void leaveMember(final Context context, final Handler mActivityHandler) {
        try {
            PacketBody body = new PacketBody();
            body.putParams("userId", SharedUserInfo.getInstance(context).getUserId());

            MOAClient.getInstance().writePacket(PacketTypes.PTG_IMS_USER, 0x00030007, body, new MOAClient.OnDataListener() {
                @Override
                public void onData(MOAData moaData) {
                    mActivityHandler.sendMessage(mActivityHandler.obtainMessage(moaData.ptc, moaData));
                }

                @Override
                public void onTimeOut() {
                    Toast.makeText(context, "onTimeOut", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(int i) {
                    //현재 사용 안함
                }
            }, 5000); //기본 10초, 시간을 지정할경우 시간 지정
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
