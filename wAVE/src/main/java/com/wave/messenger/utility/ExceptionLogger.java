package com.wave.messenger.utility;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by aveapp on 2017. 3. 30..
 */

public class ExceptionLogger {
    private String url = "http://moa.aveapp.com:81/log/moaLog.jsp";
    private String logFolder = "wavelog";

    private static ExceptionLogger instance;
    private UncaughtExceptionHandler mUncaughtException = null;
    private String logPath;
    private HashMap<String, String> postData = new HashMap<>();

    private ExceptionLogger() {
        try {
            postData.put("appKey", "wave");
            postData.put("osVer", String.valueOf(Build.VERSION.SDK_INT));
            postData.put("osType", "android");
            postData.put("model", Build.MODEL);

            logPath = Environment.getExternalStorageDirectory() + File.separator + logFolder + File.separator;

            setExceptionHandler();
            sendTraceFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ExceptionLogger getInstance() {
        if (instance == null) {
            synchronized(ExceptionLogger.class) {
                instance = new ExceptionLogger();
            }
        }
        return instance;
    }

    public void setContext(Context context) {
        try {
            postData.put("versionCode", String.valueOf(getAppVersion(context)));
        } catch (Exception e) {
            //
        }
    }

    public void setId(String value) {
        postData.put("id", value);
    }

    private int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setExceptionHandler() {
        if (mUncaughtException == null) {
            mUncaughtException = new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    //writeTraceFile(thread, ex);
                    writeTraceFile2(thread, ex);
                    System.exit(10);
                }
            };
        }
        Thread.setDefaultUncaughtExceptionHandler(mUncaughtException);
    }

    public void setExceptionHandler(UncaughtExceptionHandler handler) {
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

    public void setPostData(String key, String value) {
        postData.put(key, value);
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setLocalPath(String path) {
        this.logPath = path;
    }

    private void writeTraceFile2(Thread thread, final Throwable ex) {

        BufferedWriter fw = null;
        try {
            File path = new File(logPath);

            if (!path.exists()) {
                if(!path.mkdir()){
                    return;
                }
            }

            String fileName = logPath + "Log_"+System.currentTimeMillis() + ".txt";
            File file = new File(fileName);
            fw = new BufferedWriter(new FileWriter(file, true));

            StringBuffer sbData = new StringBuffer();
            sbData.append("exception//");
            sbData.append(ex.toString()).append("\n");

            for (StackTraceElement s : ex.getStackTrace()) {
                sbData.append(s.toString()).append("\n");
            }

//            LogTrace.E("b : " + ex.getCause().getMessage());

//            LogTrace.E("c : " + ex.getCause().toString());

            sbData.append("//time");
            sbData.append("//");
            sbData.append(getCurrentTime());

            for (String key : postData.keySet()) {
                sbData.append("//");
                sbData.append(key);
                sbData.append("//");
                sbData.append(postData.get(key));
            }

            LogTrace.E(sbData.toString());

            fw.write(sbData.toString());
            fw.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void writeTraceFile(Thread thread, final Throwable ex) {
        new Thread() {
            @Override
            public void run() {
                BufferedWriter fw = null;
                try {
                    File path = new File(logPath);
                    if (!path.exists()) {
                        if(!path.mkdir()){
                            return;
                        }
                    }
                    String fileName = logPath + "Log_"+System.currentTimeMillis() + ".txt";
                    File file = new File(fileName);
                    fw = new BufferedWriter(new FileWriter(file, true));

                    StringBuffer sbData = new StringBuffer();
                    sbData.append("exception//");
                    sbData.append(ex.toString()).append("\n");
                    for (StackTraceElement s : ex.getStackTrace()) {
                        sbData.append(s.toString()).append("\n");
                    }

                    sbData.append("//time");
                    sbData.append("//");
                    sbData.append(getCurrentTime());

                    for (String key : postData.keySet()) {
                        sbData.append("//");
                        sbData.append(key);
                        sbData.append("//");
                        sbData.append(postData.get(key));
                    }

                    fw.write(sbData.toString());
                    fw.flush();

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (fw != null) {
                        try {
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }

    public void sendTraceFile() {
        new Thread() {
            @Override
            public void run() {
                try {
                    File[] fileList = new File(logPath).listFiles();
                    if (fileList != null && fileList.length > 0) {
                        String temp;

                        BufferedReader fileReader = null;
                        DataOutputStream postMsgStream = null;
                        HttpURLConnection con = null;
                        for (File file : fileList) {
                            if (!file.getName().startsWith("Log_")) {
                                continue;
                            }
                            try {
                                URL obj = new URL(url);
                                con = (HttpURLConnection) obj.openConnection();
                                // add reuqest header
                                con.setRequestMethod("POST");
                                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                                con.setDoOutput(true);

                                StringBuffer sbData = new StringBuffer();
                                fileReader = new BufferedReader(new FileReader(file));

                                while ((temp = fileReader.readLine()) != null) {
                                    sbData.append(temp).append("\n");
                                }
                                fileReader.close();

                                String[] data = sbData.toString().split("//");

                                sbData = new StringBuffer();

                                for (int i = 0; i < data.length; i++) {
                                    if (i == 0 || i % 2 == 0) {
                                        sbData.append(data[i]).append("=");
                                    } else {
                                        sbData.append(data[i]).append("&");
                                    }
                                }
                                postMsgStream = new DataOutputStream(con.getOutputStream());
                                postMsgStream.writeBytes(sbData.toString());
                                postMsgStream.flush();

                                int responseCode = con.getResponseCode();
                                if (responseCode == 200) {
                                    file.delete();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                if (con != null) {
                                    con.disconnect();
                                    con = null;
                                }
                                if (fileReader != null) {
                                    try {
                                        fileReader.close();
                                        fileReader = null;
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (postMsgStream != null) {
                                    try {
                                        postMsgStream.close();
                                        postMsgStream = null;
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception  e) {
                    //
                }
            }
        }.start();
    }

    private String getCurrentTime() {
        long time = System.currentTimeMillis();
        SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dayTime.format(new Date(time));
    }

    public void sendException(final Exception e) {
        new Thread() {
            @Override
            public void run() {
                HttpURLConnection con = null;
                DataOutputStream postMsgStream = null;
                try {
                    URL obj = new URL(url);
                    con = (HttpURLConnection) obj.openConnection();
                    // add reuqest header
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                    StringBuffer sbData = new StringBuffer();
                    sbData.append(e.toString()).append("\n");
                    for (StackTraceElement s : e.getStackTrace()) {
                        sbData.append(s.toString()).append("\n");
                    }
                    String exceptionData = sbData.toString();

                    sbData = new StringBuffer();

                    sbData.append("exception").append("=").append(exceptionData).append("&");
                    sbData.append("time").append("=").append(getCurrentTime()).append("&");

                    for( String key : postData.keySet() ){
                        sbData.append(key);
                        sbData.append("=");
                        sbData.append(postData.get(key));
                        sbData.append("&");
                    }

                    con.setDoOutput(true);
                    postMsgStream = new DataOutputStream(con.getOutputStream());
                    postMsgStream.writeBytes(sbData.toString());
                    postMsgStream.flush();

                    int responseCode = con.getResponseCode();

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                    if (postMsgStream != null) {
                        try {
                            postMsgStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }
}
