package com.wave.messenger.Info;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Vibrator;
import android.telephony.TelephonyManager;

import com.wave.massenger.piggy.R;

import java.util.UUID;

/**
 * Created by aveapp on 2017-06-09.
 */

public class DeviceInfo {
    public static String getDeviceID(Context context) {
        String result = "";
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            final String tmDevice, tmSerial, androidId;
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            result = deviceUuid.toString();
        } catch (Exception e) {

        }

        return result;
    }


    public static String getOsType() {
        return "ANDROID";
    }

    public static String getOsVersion() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    public static String GetAppVersion(Context context) {
        try {
            PackageInfo pinfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);

            return pinfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return null;

    }

    public static String getDeviceName() {
        return Build.MODEL;
    }

    public static int getSDKVersion(){
        int nSDKVersion = Integer.parseInt(Build.VERSION.SDK);
        return nSDKVersion;
    }

    public static String getPhoneNumber(Context context) {
        TelephonyManager telManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNum = telManager.getLine1Number();

        if (phoneNum != null) {
            if (phoneNum.startsWith("+82")) {
                phoneNum = "0" + phoneNum.substring(3);
            }
        } else
            phoneNum = "";
        return phoneNum;
    }

    public static String getDeviceInfo() {
        String deviceId = String.valueOf(Build.VERSION.SDK_INT) + "," + Build.MODEL;
        return deviceId;
    }

    public static void playSoundAndVibrate(Context context) {
        try {
            boolean isPlaySound = MessengerInfo.getReceiveSound(context);
            boolean isVibrate = MessengerInfo.getReceiveVibration(context);

            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            switch (audioManager.getRingerMode()) {
                case AudioManager.RINGER_MODE_VIBRATE:
                    if (isPlaySound) {
                        isPlaySound = false;
                    }

                    break;

                case AudioManager.RINGER_MODE_SILENT:
                    if (isPlaySound) {
                        isPlaySound = false;
                    }

                    if (isVibrate) {
                        isVibrate = false;
                    }

                    break;

                default:
                    break;
            }

            if(telephonyManager.getCallState() != 2) {
                if (isPlaySound) {
                    SoundPool soundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
                    soundPool.load(context, R.raw.alarm0, 1);
                    soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int soundId, int status) {
                            soundPool.play(soundId, 1, 1, 0, 0, 1);
                        }
                    });

                }

                if (isVibrate) {
                    Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    vibe.vibrate(500);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
