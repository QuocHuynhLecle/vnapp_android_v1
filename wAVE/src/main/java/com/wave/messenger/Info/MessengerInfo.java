package com.wave.messenger.Info;

import android.content.Context;
import android.text.TextUtils;

import com.wave.messenger.utility.SharedObject;


public class MessengerInfo {

	private static Boolean agree = null;
	private static Boolean firstLogin = null;
	/** 직원이면 1, 일반이면 0*/
	private static String staff = null;
	public static Boolean receiveAlarm = null;
	public static Boolean receiveSound = null;
	public static Boolean receiveVibration = null;
	public static Boolean autoLogin = null;


	private static String userId = null;
	private static String userPw = null;
	private static String UserName = null;
	private static String sRealUserName = null;
	private static String sClearBuddy = null;
	private static String UserPhoneNumber = null;
	private static String userTocken = null;
	private static String pushKey = null;
	private static String mode = null;
	private static String data = null;
	private static String pushData = null;
	private static String firstContactUploaded = null;
	private static String profileImage = null;
	private static String userIdKey = null;
	private static String userType = "";        //유저타입 입니다. UT_AD <관리자>, UT_ST <직원>, UT_ST_RE <종목추천직원>,  UT_ST_CO <상담직원>, UT_ME <회원>, UT_ME_JU <준회원>
	private static int    riskType = 0;         //투자위험도 입니다. 종목 추천을 받는 회원은 1 받지않는 회원은 0 입니다.
	private static String empNo = "";			//직원인경우 사번, 고객인경우 "";
	private static String offYN = "";			//최근 6개월 off 약정 여부
	public static String pushStart = "";
	public static String branchCode = "";
	public static String branchName = "";
	//Y.최근6개월 OFF약정만 발생시(ON 약정 미발생)
	//N.최근6개월 OFF약정 미발생 또는 ON 약정 발생시

	public static String getStaff(Context context){
		if(staff == null || TextUtils.isEmpty(staff)){
			staff = SharedObject.getProperty_string(context, "staff", "0");
		}
		return staff;
	}

	/** staff값이 0이면 일반, 1이면 직원*/
	public static void setStaff(Context context, String newValue) {
		SharedObject.setProperty_string(context, "staff", newValue);
		staff = null;
	}

	public static boolean getReceiveAlarm(Context context) {
		if (receiveAlarm == null || "".equals(receiveAlarm)) {
			receiveAlarm = SharedObject.getProperty_boolean(context, "receiveAlarm", true);
		}
		return receiveAlarm;
	}

	public static void setReceiveAlarm(Context context, boolean firstLogin) {
		SharedObject.setProperty_boolean(context, "receiveAlarm", firstLogin);
		MessengerInfo.receiveAlarm = null;
	}

	public static boolean getReceiveSound(Context context) {
		if (receiveSound == null || "".equals(receiveSound)) {
			receiveSound = SharedObject.getProperty_boolean(context, "receiveSound", true);
		}
		return receiveSound;
	}

	public static void setReceiveSound(Context context, boolean firstLogin) {
		SharedObject.setProperty_boolean(context, "receiveSound", firstLogin);
		MessengerInfo.receiveSound = null;
	}

	public static boolean getReceiveVibration(Context context) {
		if (receiveVibration == null || "".equals(receiveVibration)) {
			receiveVibration = SharedObject.getProperty_boolean(context, "receiveVibration", true);
		}
		return receiveVibration;
	}

	public static void setReceiveVibration(Context context, boolean firstLogin) {
		SharedObject.setProperty_boolean(context, "receiveVibration", firstLogin);
		MessengerInfo.receiveVibration = null;
	}

	public static boolean isAutoLogin(Context context) {
		if (autoLogin == null || "".equals(autoLogin)) {
			autoLogin = SharedObject.getProperty_boolean(context, "autoLogin", false);
		}
		return autoLogin;
	}

	public static void setAutoLogin(Context context, boolean autoLogin) {
		SharedObject.setProperty_boolean(context, "autoLogin", autoLogin);
		MessengerInfo.autoLogin = autoLogin;
	}

	public static boolean getAutoLogin(Context context, boolean auto) {
		if(autoLogin == null || "".equals(autoLogin)) {
			autoLogin = SharedObject.getProperty_boolean(context, "autoLogin", auto);
		}

		return autoLogin;
	}

	public static boolean isFirstLogin(Context context) {
		if (firstLogin == null || "".equals(firstLogin)) {
			firstLogin = SharedObject.getProperty_boolean(context, "firstLogin", false);
		}
		return firstLogin;
	}

	public static void setFirstLogin(Context context, boolean firstLogin) {
		SharedObject.setProperty_boolean(context, "firstLogin", firstLogin);
		MessengerInfo.firstLogin = null;
	}

	public static boolean getAgree(Context context) {
		if (agree == null || "".equals(agree)) {
			agree = SharedObject.getProperty_boolean(context, "agree", false);
		}
		return agree;
	}

	public static void setAgree(Context context, boolean value) {
		SharedObject.setProperty_boolean(context, "agree", value);
		agree = null;
	}

	public static String getUserName(Context context) {
		if (UserName == null || TextUtils.isEmpty(UserName)) {
			UserName = SharedObject.getProperty_string(context, "UserName", "");
		}
		return UserName;
	}

	public static void setUsername(Context context, String value) {
		SharedObject.setProperty_string(context, "UserName", value);
		UserName = null;
	}

	public static String getRealUserName(Context context) {
		if (TextUtils.isEmpty(sRealUserName)) {
            sRealUserName = SharedObject.getProperty_string(context, "RealUserName", "");
		}
		return sRealUserName;
	}

	public static void setRealUserName(Context context, String value) {
		SharedObject.setProperty_string(context, "RealUserName", value);
        sRealUserName = null;
	}

	public static String getClearBuddy(Context context) {
		if (TextUtils.isEmpty(sClearBuddy)) {
            sClearBuddy = SharedObject.getProperty_string(context, "ClearBuddy", "0");
		}
		return sClearBuddy;
	}

	public static void setClearBuddy(Context context, String value) {
		SharedObject.setProperty_string(context, "ClearBuddy", value);
        sClearBuddy = null;
	}

	public static String getUserPhoneNumber(Context context) {
		if (UserPhoneNumber == null || TextUtils.isEmpty(UserPhoneNumber)) {
			UserPhoneNumber = SharedObject.getProperty_string(context, "UserPhoneNumber", "");
		}
		return UserPhoneNumber;
	}

	public static void setUserPhoneNumber(Context context, String value) {
		SharedObject.setProperty_string(context, "UserPhoneNumber", value);
		UserPhoneNumber = null;
	}

	public static String getUserId(Context context) {
		if (userId == null || TextUtils.isEmpty(userId)) {
			userId = SharedObject.getProperty_string(context, "userId", "");
		}
		return userId;
	}

	public static void setUserId(Context context, String value) {
		SharedObject.setProperty_string(context, "userId", value);
		userId = null;
	}

	public static String getUserPw(Context context) {
		if (userPw == null || TextUtils.isEmpty(userPw)) {
			userPw = SharedObject.getProperty_string(context, "userPw", "");
		}
		return userPw;
	}

	public static void setUserPw(Context context, String value) {
		SharedObject.setProperty_string(context, "userPw", value);
		userPw = null;
	}

	public static String getUserTocken(Context context) {
		if (userTocken == null || TextUtils.isEmpty(userTocken)) {
			userTocken = SharedObject.getProperty_string(context, "userTocken","");
		}
		return userTocken;
	}

	public static void setUserTocken(Context context, String value) {
		SharedObject.setProperty_string(context, "userTocken", value);
		userTocken = null;
	}

	public static String getPushKey(Context context) {
		if (pushKey == null || TextUtils.isEmpty(pushKey)) {
			pushKey = SharedObject.getProperty_string(context, "pushKey",
					"");
		}
		return pushKey;
	}

	public static void setPushKey(Context context, String value) {
		SharedObject.setProperty_string(context, "pushKey", value);
		pushKey = null;
	}

	public static String getMode(Context context) {
		if (mode == null || TextUtils.isEmpty(mode)) {
			mode = SharedObject.getProperty_string(context, "mode","");
		}
		return mode;
	}

	public static void setMode(Context context, String value) {
		SharedObject.setProperty_string(context, "mode", value);
		mode = null;
	}

	public static String getData(Context context) {
		if (data == null || TextUtils.isEmpty(data)) {
			data = SharedObject.getProperty_string(context, "data", "");
		}
		return data;
	}

	public static void setData(Context context, String value) {
		SharedObject.setProperty_string(context, "data", value);
		data = null;
	}

	public static String getPushData(Context context) {
		if (pushData == null || TextUtils.isEmpty(pushData)) {
			pushData = SharedObject.getProperty_string(context, "pushData", "");
		}
		return pushData;
	}

	public static void setPushData(Context context, String value) {
		SharedObject.setProperty_string(context, "pushData", value);
		pushData = null;
	}

    public static String getFirstContactUploaded(Context context) {
        if (TextUtils.isEmpty(firstContactUploaded)) {
            firstContactUploaded = SharedObject.getProperty_string(context, "firstContactUploaded", "1");
        }
        return firstContactUploaded;
    }

    public static void setFirstContactUploaded(Context context, boolean firstContactUploaded) {
        SharedObject.setProperty_string(context, "firstContactUploaded", firstContactUploaded ? "1" : "0");
        MessengerInfo.firstContactUploaded = null;
    }

	public static String getProfileImage(Context context) {
		if(TextUtils.isEmpty(profileImage)) {
			profileImage = SharedObject.getProperty_string(context, "profileImage", "");
		}
		return profileImage;
	}

	public static void setProfileImage(Context context, String profileImage) {
		SharedObject.setProperty_string(context, "profileImage", profileImage);
		MessengerInfo.profileImage = null;
	}

	public static String getLockPassword(Context context) {
		return SharedObject.getProperty_string(context, "lockPassword", "");
	}
	public static void setLockPassowrd(Context context, String value) {
		SharedObject.setProperty_string(context, "lockPassword", value);
	}
	public static boolean getLockCheck(Context context) {
		return SharedObject.getProperty_boolean(context, "lockCheck", false);
	}
	public static void setLockCheck(Context context, boolean value) {
		SharedObject.setProperty_boolean(context, "lockCheck", value);
	}

	public static String getUserIdKey() {
		return userIdKey;
	}

	public static String getUserType() {
		return userType;
	}

	public static void setUserType(String userType) {
		MessengerInfo.userType = userType;
	}

	public static int getRiskType() {
		return riskType;
	}

	public static void setRiskType(int riskType) {
		MessengerInfo.riskType = riskType;
	}

	public static String getEmpNo() {
		return empNo;
	}

	public static void setEmpNo(String empNo) {
		MessengerInfo.empNo = empNo;
	}

	public static String getOffYN() {
		return offYN;
	}

	public static void setOffYN(String offYN) {
		MessengerInfo.offYN = offYN;
	}

	public static String getBranchCode() {
		return branchCode;
	}

	public static void setBranchCode(String branchCode) {
		MessengerInfo.branchCode = branchCode;
	}

	public static String getBranchName() {
		return branchName;
	}

	public static void setBranchName(String branchName) {
		MessengerInfo.branchName = branchName;
	}

	public static void setUserIdKey(String userIdKey) {
		MessengerInfo.userIdKey = userIdKey;
	}

	public static String getPushStart(Context context) {
		if (pushStart == null || TextUtils.isEmpty(pushStart)) {
			pushStart = SharedObject.getProperty_string(context, "pushStart", "");
		}
		return pushStart;
	}

	public static void setPushStart(Context context, String pushStart) {
		SharedObject.setProperty_string(context, "pushStart", pushStart);
	}

	public static void clear(Context context) {
		setUserId(context, "");
		setAgree(context, false);
		setFirstLogin(context,false);
		setUserPhoneNumber(context,"");
		setUserPw(context,"");
		setUserTocken(context,"");
		setUsername(context,"");
		setPushKey(context,"");
		setMode(context,"");
		setData(context,"");
		setPushData(context,"");
	}
}
