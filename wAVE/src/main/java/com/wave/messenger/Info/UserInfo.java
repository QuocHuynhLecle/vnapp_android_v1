package com.wave.messenger.Info;

/**
 * Created by aveapp on 2017-06-09.
 */

public class UserInfo {
    String userName = "";
    String userId = "";
    String password = "";
    String pushKey = "";
    String phoneNumber = "";
    String userType = "";     //유저타입 입니다. UT_AD <관리자>, UT_ST <직원>, UT_ST_RE <종목추천직원>,  UT_ST_CO <상담직원>, UT_ME <회원>, UT_ME_JU <준회원>
    int riskType = 0;         //투자위험도 입니다. 종목 추천을 받는 회원은 1 받지않는 회원은 0 입니다.

    String	empNo = "";			//직원인경우 사번, 고객인경우 "";
    String  offYN = "";			//최근 6개월 off 약정 여부
    //Y.최근6개월 OFF약정만 발생시(ON 약정 미발생)
    //N.최근6개월 OFF약정 미발생 또는 ON 약정 발생시
    String branchCode = "";
    String branchName = "";
    boolean agreeYN = false; //약관 동의 여부

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPushKey() {
        return pushKey;
    }
    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    public int getRiskType() {
        return riskType;
    }
    public void setRiskType(int riskType) {
        this.riskType = riskType;
    }
    public String getEmpNo()
    {
        return empNo;
    }

    public void setEmpNo(String empNo)
    {
        this.empNo = empNo;
    }

    public String getOffYN()
    {
        return offYN;
    }

    public void setOffYN(String offYN)
    {
        this.offYN = offYN;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public boolean isAgreeYN() {
        return agreeYN;
    }

    public void setAgreeYN(boolean agreeYN) {
        this.agreeYN = agreeYN;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        String ret = "";

        ret += "userName = " + userName + "\n";
        ret += "userId = " + userId + "\n";
        ret += "password = " + password + "\n";
        ret += "pushKey = " + pushKey + "\n";
        ret += "phoneNumber = " + phoneNumber + "\n";
        ret += "userType = " + userType + "\n";
        ret += "riskType = " + riskType + "\n";
        ret += "empNo = " + empNo + "\n";
        return ret;
    }
}
