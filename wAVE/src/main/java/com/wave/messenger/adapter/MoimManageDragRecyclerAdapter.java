/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.helper.ItemTouchHelperAdapter;
import com.wave.messenger.helper.ItemTouchHelperViewHolder;
import com.wave.messenger.helper.OnStartDragListener;
import com.wave.messenger.vo.VoMoim;

import java.util.ArrayList;
import java.util.Collections;


/**
 * Simple RecyclerView.Adapter that implements {@link ItemTouchHelperAdapter} to respond to move and
 * dismiss events from a {@link android.support.v7.widget.helper.ItemTouchHelper}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class MoimManageDragRecyclerAdapter extends RecyclerView.Adapter<MoimManageDragRecyclerAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    //private final List<String> mItems = new ArrayList<>();

    ArrayList<VoMoim> arVoMoim;

    IonManageEventListener onManageEventListener;

    private final OnStartDragListener mDragStartListener;

    public MoimManageDragRecyclerAdapter(Context context, OnStartDragListener dragStartListener, ArrayList<VoMoim> arVoMoim, IonManageEventListener onManageEventListener1) {
        mDragStartListener = dragStartListener;
        this.onManageEventListener=onManageEventListener1;
        this.arVoMoim = arVoMoim;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_moim_manage, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.textView.setText(arVoMoim.get(position).getTitle());

        // Start a drag whenever the handle view it touched 이동기능 하지말기
        /*holder.handleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });*/

        if(position==getItemCount()-1){
            holder.imgvLine.setVisibility(View.GONE);
        }else{
            holder.imgvLine.setVisibility(View.VISIBLE);
        }

        holder.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               removeAt(position);
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        Log.e(MoimManageDragRecyclerAdapter.this.getClass()+"","onItemDismiss"+position);
       /*
       arVoMoim.remove(position);
       notifyItemRemoved(position);
       */
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Log.e(MoimManageDragRecyclerAdapter.this.getClass()+"","onItemMove "+fromPosition+" "+toPosition +" arVoMoim "+arVoMoim.size());
        Collections.swap(arVoMoim, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return arVoMoim.size();
    }

    /*public void removeAt(int position) {
        arVoMoim.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arVoMoim.size());
    }*/

    /**
     * 상단에 고정 리스트 해제버튼
     * @param position
     */
    public void removeAt(int position) {
        onManageEventListener.onClearEvent(position);
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final TextView textView;
        public final ImageView handleView;
        public final Button btnClear;
        public final ImageView imgvLine;

        public ItemViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_text);
            handleView = (ImageView) itemView.findViewById(R.id.handle);
            btnClear=(Button) itemView.findViewById(R.id.btn_clear);
            imgvLine = (ImageView) itemView.findViewById(R.id.imgv_line);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}
