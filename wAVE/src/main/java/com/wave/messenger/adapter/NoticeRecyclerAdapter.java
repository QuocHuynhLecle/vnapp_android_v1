package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.activity.Activity_MoimNotice;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Noty;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 4. 10..
 * 공지사항 어댑터
 */
//public class NoticeRecyclerAdapter extends RecyclerView.Adapter<NoticeRecyclerAdapter.ViewHolderItem> {


import android.os.Handler;
import android.os.Message;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardList;
import com.wave.messenger.vo.VoMsgItem;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 10..
 * 모임 공지사항  어댑터
 */
public class NoticeRecyclerAdapter extends RecyclerView.Adapter<NoticeRecyclerAdapter.ViewHolder> {

    String TAG = this.getClass().getSimpleName();


    ArrayList<VoBoardList.VoBoardItem> arvalues = new ArrayList<>();

    Context mContext;
    ImageView mImage;


    public NoticeRecyclerAdapter(Context con) {
        mContext = con;
    }


    /**
     * @param values
     * @param deleteList 리스트를 초기화하고 다시 붙인다
     */
    public void setItem(ArrayList<VoBoardList.VoBoardItem> values, boolean deleteList) {
        if (deleteList) {
            arvalues = new ArrayList<>();
            for (VoBoardList.VoBoardItem item : values) {
                if(!item.getNoti().equals("0")){//일반글이 아니면.
                    arvalues.add(item);
                }

            }

        } else {
            for (VoBoardList.VoBoardItem item : values) {
                if(!item.getNoti().equals("0")){
                    arvalues.add(item);
                }

            }
        }

    }

    /**
     * 노티 값을 변경한다
     */
    /*public void setNotiDate(String msgId, String noti) {
        Log.e("BoardListRecycler","setNotiDate: "+msgId+" "+noti);

        for (int i = 0; i <arvalues.size() ; i++) {
            if(arvalues.get(i).getMsgId().equals(msgId)){
                arvalues.get(i).setNoti(noti);

                if(mContext instanceof Activity_MoimTimeLineList){
                    ((Activity_MoimTimeLineList)mContext).reFreshAdapter();
                    //상단 헤더 부분을 새로고침한다.

                }
                break;
            }
        }
    }*/

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_regUsr;
        public TextView time;
        public TextView content;
        public LinearLayout llItem;
        private LinearLayout llThumbnailes;
        public View vDot;

        public ImageView imgvThum;
        public ImageButton imgbOption;
        //private ImageView imgvProfile;

        public ViewHolder(View v) {
            super(v);
            tv_regUsr = (TextView) v.findViewById(R.id.tv_regusr);
            content = (TextView) v.findViewById(R.id.tv_contents);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            //imgvProfile = (ImageView) v.findViewById(R.id.imgv_profile);
            llThumbnailes = (LinearLayout) v.findViewById(R.id.ll_thum);

            imgvThum = (ImageView) v.findViewById(R.id.imgv_thum);
            vDot = (View)v.findViewById(R.id.view_red_dot);

            imgbOption = (ImageButton) v.findViewById(R.id.imgb_option);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_notice_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {


        //등록자
        Vholder.tv_regUsr.setText(arvalues.get(position).getUsNm());

        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //메시지 키값저장

                intentTimeLineDetail(arvalues.get(position).getMsgId(), false);
            }
        });


        if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getNotiTy(mContext))) {
            Vholder.imgbOption.setVisibility(View.VISIBLE);
        }else{
            Vholder.imgbOption.setVisibility(View.GONE);
        }


        Vholder.imgbOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //공지 내리기 다이얼로그.
                final Dialog_Noty dialog_Noty = new Dialog_Noty(mContext, arvalues.get(position).getNoti().equals("2"));

                dialog_Noty.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(v.getId()==R.id.tv_option0){
                        //중요공지로 등록
                            moimNotice(arvalues.get(position).getMsgId(), "2");
                            dialog_Noty.dismiss();

                        }else if(v.getId()==R.id.tv_option1){
                        //중요공지 내리기 =>공지
                            moimNotice(arvalues.get(position).getMsgId(), "1");
                            dialog_Noty.dismiss();

                        }else if(v.getId()==R.id.tv_option2){
                        //공지에서 내리기
                            moimNotice(arvalues.get(position).getMsgId(), "0");
                            dialog_Noty.dismiss();

                        }


                    }
                });
                dialog_Noty.show();


            }
        });


        //글내용 표시부분
        String strMsg = arvalues.get(position).getMsg();
        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(strMsg);


        ArrayList<String> arImage = new ArrayList<>();  //썸네일 데이터
        ArrayList<String> arEmoticon = new ArrayList<>();  //썸네일 데이터

       String strText = "";
        for (int i = 0; i < arMsgItem.size(); i++) {

            if (arMsgItem.get(i).getType().equals("0")) {
                //strText = strText + arMsgItem.get(i).getData() + mContext.getResources().getString(R.string.nextline);
            } else if (arMsgItem.get(i).getType().equals("1")) {    //이미지
                arImage.add(arMsgItem.get(i).getData());
            }else if (arMsgItem.get(i).getType().equals("4")) {     //이모티콘
                arEmoticon.add(arMsgItem.get(i).getData());
            }
        }


        //오늘 날짜만 빨간 점표시
        if(arvalues.get(position).getRegDt().contains("분전") ||arvalues.get(position).getRegDt().contains("시간전")){
            Vholder.vDot.setVisibility(View.VISIBLE);
        }else{
            Vholder.vDot.setVisibility(View.GONE);
        }


        strText=arvalues.get(position).getTtl();

        if(arvalues.get(position).getNoti().equals("2")){ //긴급공지표시

            String actionDisplayText ="중요 "+strText;
            SpannableString truncatedSpannableString = new SpannableString(actionDisplayText);
            //int startIndex = actionDisplayText.indexOf(moreString);
            truncatedSpannableString.setSpan(new ForegroundColorSpan(Util.getColor(mContext,R.color.red)), 0, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            Vholder.content.setText(truncatedSpannableString);

        }else{
            Vholder.content.setText(strText);
        }

        //글내용없고 이미지만 있을경우
        if(strText=="" && arImage.size() > 0){
            Vholder.content.setText("사진을 올렸습니다.");

        }else if(strText=="" && arEmoticon.size() > 0){
            Vholder.content.setText("이모티콘을 올렸습니다.");

       }


        //썸네일 표시 부분
        if (arImage.size() > 0) {
           // Vholder.llThumbnailes.setVisibility(View.VISIBLE);

            //Vholder.llThumbnailes.removeAllViews();

            Log.e(TAG,"arImage.get(0): "+ Constants.MOIM_TIMELINE_LIST_IMAGE_URL + arImage.get(0));
            Glide.with(mContext).load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + arImage.get(0)).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into(Vholder.imgvThum);

        } else {
            //Vholder.llThumbnailes.setVisibility(View.GONE);
        }

        //이모티콘 표시 부분.
        if(arEmoticon.size() >0){
            //Vholder.llThumbnailes.setVisibility(View.VISIBLE);
            Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), getDrawableResourceByName(arEmoticon.get(0)));
            Vholder.imgvThum.setImageBitmap(bm);

        }else{
            //Vholder.llThumbnailes.setVisibility(View.GONE);
        }
    }


    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(mContext,str);
        String packageName = mContext.getPackageName();
        return mContext.getResources().getIdentifier(emoticon, "drawable", packageName);
    }


    /**
     * 글삭제이후
     * 데이터를 삭제한다
     *
     * @param position
     */
    public void removeAt(int position) {
        Log.e(TAG, "removeAt: " + position + " " + arvalues.get(position).getMsg());
        arvalues.remove(position);
        ((Activity_MoimTimeLineList) mContext).RemoveAdapter(position, arvalues.size());
    }


    @Override
    public int getItemCount() {

        return arvalues.size();
    }


    /**
     * 타임라인 상세화면 intent
     *
     * @param msgId
     * @param reply true :댓글달기
     */
    private void intentTimeLineDetail(String msgId, boolean reply) {

        SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, msgId);
        SharedObject.setProperty_boolean(mContext, Constants.MOIM_REPLY, reply);

        Intent intent = new Intent(mContext, Activity_TimeLineDetail.class);
        mContext.startActivity(intent);
    }

    /**
     * 7.16 모임 공지사항 설정/해제
     * noti     공지 여부(int)     0:일반, 1:공지
     */
    private void moimNotice(String msgId, String noti) {
        Moa.moimNotice(mContext, msgId, noti, mMainActvtHandler);
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_NOTICE:  //모임 공지사항 설정/해제
                        Log.e(TAG, "PTC_IMS_MOIM_NOTICE: ");

                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            //String msgId = data.body.getJson("params").getString("msgId");

                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            // Log.e(TAG,"noti: "+data.body.get("noti").toString());

                            //공지 등록 결과 처리

                            //setNotiDate(data.body.getJson("params").getString("msgId").toString()
                            //        , data.body.getJson("params").getString("noti").toString());

                            ((Activity_MoimNotice)mContext).refreshItem();
                            ((Activity_MoimNotice)mContext).mDeleteList=true;




                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

}