package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_MoimFindBySubject;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.vo.VoMoimList;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 13..
 * 모임-탭2
 * 리스트 어댑터
 */
public class MyMoimRecyclerAdapter extends RecyclerView.Adapter<MyMoimRecyclerAdapter.ViewHolder> {

    String TAG = MyMoimRecyclerAdapter.class.getName();
    ArrayList<VoMoimList> arvalues;
    Context context;
    int nViewType=1;  //2칸 3칸 1줄 구분


    public MyMoimRecyclerAdapter(Context con, ArrayList<VoMoimList> values, int property_int) {
        arvalues = values;
        context = con;
        nViewType=property_int;
    }

    /**
     * 뷰타입 설정.
     * 2칸 3칸 1칸.
     * @param property_int
     */
    public void setViewType(int property_int) {
        nViewType=property_int;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;
        public LinearLayout llItem;
        public RelativeLayout rlInfo;
        public LinearLayout llMoimThum;
        public ImageView imgvMoimThum;

        public LinearLayout llAdd;
        public ImageView imgvIcon;
        public ImageView imgvIconBig;
        public View llGap;
        public ImageView imgvBLkCnt;    //좋아요 뱃지 카운트 뱃지 기준값:100이상, 80이상, 60이상, 40이상
        public ImageView imgvBCttCnt;   //컨텐츠 뱃지 카운트 뱃지 기준값:8이상, 7이상, 6이상, 5이상
        public ImageView imgvUsrCnt;    //뱃지 기준값: 1000이상, 500이상, 100이상
        public ImageView imgvLeader;    //리더표시

        public LinearLayout llBadgeBest;    //추천벳지
        public LinearLayout llBadge;    //벳지

        public View viewRedDot;



        public ViewHolder(View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_moim_title);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            llMoimThum= (LinearLayout) v.findViewById(R.id.ll_moim_thum);
            rlInfo= (RelativeLayout) v.findViewById(R.id.rl_info);


            imgvMoimThum = (ImageView) v.findViewById(R.id.imgv_moim_thum);
            llAdd =(LinearLayout) v.findViewById(R.id.ll_add);
            imgvIcon = (ImageView) v.findViewById(R.id.imgv_icon);
            imgvIconBig = (ImageView) v.findViewById(R.id.imgv_moim_icon_big);
            llGap = (View) v.findViewById(R.id.ll_gap);

            imgvBLkCnt = (ImageView) v.findViewById(R.id.imgv_bLkCnt);
            imgvBCttCnt = (ImageView) v.findViewById(R.id.imgv_bCttCnt);
            imgvUsrCnt= (ImageView) v.findViewById(R.id.imgv_usrCnt);

            imgvLeader= (ImageView) v.findViewById(R.id.imgv_leader);


            llBadgeBest=(LinearLayout) v.findViewById(R.id.ll_badge_best);

            viewRedDot = (View) v.findViewById(R.id.view_red_dot);

            llBadge=(LinearLayout) v.findViewById(R.id.ll_badge);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //View view1 = null; // = LayoutInflater.from(context).inflate(R.layout.recycler_mymoim_item, parent, false);
        Log.e(TAG,"onCreateViewHolder: "+context);

        ViewHolder viewHolder1 = null;

        switch (nViewType){
            case 1: //기본 layout사이즈 대로 적용.  두칸
                View view1;
                view1 = LayoutInflater.from(context).inflate(R.layout.recycler_mymoim_item, parent, false);
                view1.findViewById(R.id.ll_item).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline1_height);
                view1.findViewById(R.id.ll_moim_thum).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline1_thum_height);

                //RelativeLayout.LayoutParams rParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                //view1.findViewById(R.id.ll_icon).setLayoutParams(rParams);

                //LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                //view1.findViewById(R.id.imgv_icon).setLayoutParams(lParams);
                viewHolder1 = new ViewHolder(view1);
                break;

            case 2:// 세칸
                View view2;
                view2 = LayoutInflater.from(context).inflate(R.layout.recycler_mymoim_item2, parent, false);
                view2.findViewById(R.id.ll_item).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline2_height);  // 총 높이
                view2.findViewById(R.id.ll_moim_thum).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline2_thum_height);    //상단 썸네일 이미지 레이어

                view2.findViewById(R.id.ll_icon).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline2_icon_bg_height);
                view2.findViewById(R.id.ll_icon).getLayoutParams().width= context.getResources().getDimensionPixelSize(R.dimen.timeline2_icon_bg_height);

                view2.findViewById(R.id.imgv_icon).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline2_icon_height);
                view2.findViewById(R.id.imgv_icon).getLayoutParams().width= context.getResources().getDimensionPixelSize(R.dimen.timeline2_icon_height);


                //RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                //llp.setMargins(10, 20, 0, 0);
                //view2.findViewById(R.id.tv_moim_title).setLayoutParams(llp);

                viewHolder1 = new ViewHolder(view2);
                break;

            case 3: //한줄
                //Log.e("onCreateViewHolder","timeline1_height");
                View view3;
                view3 = LayoutInflater.from(context).inflate(R.layout.recycler_mymoim_item3, parent, false);
                float fWtPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, context.getResources().getDisplayMetrics());

                view3.findViewById(R.id.ll_icon).measure(0, 0);
                int nWidth = view3.findViewById(R.id.ll_icon).getMeasuredWidth();
                view3.findViewById(R.id.tv_moim_title).setPadding(0,0, (int) (nWidth+fWtPx),0);


/*
                ((LinearLayout)view1.findViewById(R.id.ll_item)).setOrientation(LinearLayout.HORIZONTAL);

                LinearLayout.LayoutParams itemParams = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, context.getResources().getDimensionPixelSize(R.dimen.timeline3_height));
                itemParams.gravity = Gravity.CENTER_VERTICAL;
                view1.findViewById(R.id.ll_item).setLayoutParams(itemParams);

                view1.findViewById(R.id.ll_moim_thum).getLayoutParams().height= context.getResources().getDimensionPixelSize(R.dimen.timeline3_thum_height);
                view1.findViewById(R.id.ll_moim_thum).getLayoutParams().width= context.getResources().getDimensionPixelSize(R.dimen.timeline3_thum_height);


                RelativeLayout.LayoutParams iconParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                iconParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                iconParams.addRule(RelativeLayout.CENTER_VERTICAL);
                iconParams.setMargins(0,0,context.getResources().getDimensionPixelSize(R.dimen.timeline3_icon_margin),0);
                view1.findViewById(R.id.ll_icon).setLayoutParams(iconParams);*/
                viewHolder1 = new ViewHolder(view3);
                break;
        }
        //ViewHolderItem viewHolder1 = new ViewHolderItem(view1);

        return viewHolder1;
    }


    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {
        Log.e(TAG,"onBindViewHolder: "+nViewType);

        if(position==getItemCount()-1){ //마지막 추가 아이템

            //TODO 상단 갭 처리.

            Vholder.llMoimThum.setVisibility(View.GONE); //썸네일
            Vholder.llAdd.setVisibility(View.VISIBLE);  //추가버튼
            Vholder.rlInfo.setVisibility(View.GONE);    //하단

            Vholder.imgvLeader.setVisibility(View.GONE);    //리더표시 숨김.


            Vholder.llBadgeBest.setVisibility(View.GONE);   //추천뱃지숨김.
            Vholder.llBadge.setVisibility(View.GONE);       //뱃지숨김.


            Vholder.llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {   //모임개설
                    Intent intent = new Intent(context, Activity_MoimFindBySubject.class);
                    //intent.putExtra("TYPE",1);
                    intent.putExtra(Constants.MOIM_TYPE, Constants.SET_MOIM_LIST.moim_create);
                    context.startActivity(intent);
                }
            });

            setGapLayout(nViewType,position,Vholder.llGap);




        }else{ //모임 썸네일 처리.

            setGapLayout(nViewType,position,Vholder.llGap);


            Vholder.rlInfo.setVisibility(View.VISIBLE);
            Vholder.tvTitle.setText(arvalues.get(position).getMmNm());
            Vholder.llMoimThum.setVisibility(View.VISIBLE);
            Vholder.llAdd.setVisibility(View.GONE);
            Vholder.tvTitle.setVisibility(View.VISIBLE);




                switch (arvalues.get(position).getMmTg()) {
                    case "1":   //1 : 피기맘
                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_time_edu);

                        /*if(nViewType==3){ //1칸이면 작은 아이콘
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_mid_stock);
                        }else{
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_stock);
                        }*/
                        Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_time_edu);

                        break;
                    case "2":   //2 : 개인

                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_personal);
                       Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_personal);


                        break;
                    case "3":    //3 : 해외파생
                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_global_derivation);
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_global_derivation);

                        break;

                    case "4":   //4 : 해외주식
                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_global_stock);
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_global_stock);


                        break;

                    case "5":   //5 : 금융상품
                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_bank_product);
                        //Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_bank_product);
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_bank_product);
                        break;
                    case "6":   //6 : 기타
                        Vholder.imgvIcon.setBackgroundResource(R.drawable.ic_mini_etc);
                            Vholder.imgvIconBig.setBackgroundResource(R.drawable.ic_etc);


                        break;
                    default:
                        //Vholder.llItem.setBackgroundResource(R.drawable.ic_candle_stock);
                        break;
                }

            if(arvalues.get(position).getImgTy().equals("1")) {//이미지가 있을 경우
                Vholder.imgvIconBig.setVisibility(View.GONE);
                Vholder.imgvMoimThum.setVisibility(View.VISIBLE);

                Glide.with(context).load(Constants.MOIM_LIST_IMAGE_URL +arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                        .dontAnimate().into(Vholder.imgvMoimThum);
            }else{
                Vholder.imgvIconBig.setVisibility(View.VISIBLE);
                Vholder.imgvMoimThum.setVisibility(View.GONE);
            }

            Vholder.llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentMoimProfile(arvalues.get(position).getMmId());
                }
            });


            int [] intLkCnt = new int [] { 100, 80, 60, 40 };


            if(!TextUtils.isEmpty(arvalues.get(position).getbLkCnt())){   //TODO  운영서버에는 아직 적용 안됨.

                //좋아요 아이콘 처리.뱃지 기준값:100이상, 80이상, 60이상, 40이상
                int nLkCnt=Integer.parseInt(arvalues.get(position).getbLkCnt());
                if(intLkCnt[0] <= nLkCnt){
                    Vholder.imgvBLkCnt.setBackgroundResource(R.drawable.ic_like_yellow);
                    Vholder.imgvBLkCnt.setVisibility(View.VISIBLE);
                }else if(nLkCnt >= intLkCnt[1] && nLkCnt < intLkCnt[0]){
                    Vholder.imgvBLkCnt.setBackgroundResource(R.drawable.ic_like_white);
                    Vholder.imgvBLkCnt.setVisibility(View.VISIBLE);
                }else if(nLkCnt >= intLkCnt[2] && nLkCnt < intLkCnt[1]){
                    Vholder.imgvBLkCnt.setBackgroundResource(R.drawable.ic_like_red);
                    Vholder.imgvBLkCnt.setVisibility(View.VISIBLE);
                }else if(nLkCnt >= intLkCnt[3] && nLkCnt < intLkCnt[2]){
                    Vholder.imgvBLkCnt.setBackgroundResource(R.drawable.ic_like_blue);
                    Vholder.imgvBLkCnt.setVisibility(View.VISIBLE);
                }else{
                    Vholder.imgvBLkCnt.setVisibility(View.GONE);
                }


                int [] intCttCnt = new int [] { 8,7,6,5 };
                int bCttCnt=Integer.parseInt(arvalues.get(position).getbCttCnt()); //bCttCnt
                if(intCttCnt[0] <= bCttCnt){
                    Vholder.imgvBCttCnt.setBackgroundResource(R.drawable.ic_prize_yellow);
                    Vholder.imgvBCttCnt.setVisibility(View.VISIBLE);
                }else if(bCttCnt >= intCttCnt[1] && bCttCnt < intCttCnt[0]){
                    Vholder.imgvBCttCnt.setBackgroundResource(R.drawable.ic_prize_white);
                    Vholder.imgvBCttCnt.setVisibility(View.VISIBLE);
                }else if(bCttCnt >= intCttCnt[2] && bCttCnt < intCttCnt[1]){
                    Vholder.imgvBCttCnt.setBackgroundResource(R.drawable.ic_prize_red);
                    Vholder.imgvBCttCnt.setVisibility(View.VISIBLE);
                }else if(bCttCnt >= intCttCnt[3] && bCttCnt < intCttCnt[2]){
                    Vholder.imgvBCttCnt.setBackgroundResource(R.drawable.ic_prize_blue);
                    Vholder.imgvBCttCnt.setVisibility(View.VISIBLE);
                }else{
                    Vholder.imgvBCttCnt.setVisibility(View.GONE);
                }

                //현재 가입 유저수(int) 뱃지 기준값: 1000이상, 500이상, 100이상
                int [] intUsrCnt = new int [] { 1000,500,100 };
                int nUsrCnt=Integer.parseInt(arvalues.get(position).getUsrCnt()); //bCttCnt

                Log.e(TAG,"nUsrCnt: "+nUsrCnt+" getMmNm: "+arvalues.get(position).getMmNm() );
                if(intUsrCnt[0] <= nUsrCnt){
                    Vholder.imgvUsrCnt.setBackgroundResource(R.drawable.ic_gold_medal);
                    Vholder.imgvUsrCnt.setVisibility(View.VISIBLE);
                }else if(nUsrCnt >= intUsrCnt[1] && nUsrCnt < intUsrCnt[0]){
                    Vholder.imgvUsrCnt.setBackgroundResource(R.drawable.ic_silver_medal);
                    Vholder.imgvUsrCnt.setVisibility(View.VISIBLE);
                }else if(nUsrCnt >= intUsrCnt[2] && nUsrCnt < intUsrCnt[1]){
                    Vholder.imgvUsrCnt.setBackgroundResource(R.drawable.ic_bronze_medal);
                    Vholder.imgvUsrCnt.setVisibility(View.VISIBLE);
                }else{
                    Vholder.imgvUsrCnt.setVisibility(View.GONE);
                }


            }
            //리더표시
            if(arvalues.get(position).getRegUsr().equals(MessengerInfo.getUserId(context))){
                Vholder.imgvLeader.setVisibility(View.VISIBLE);
            }else{
                Vholder.imgvLeader.setVisibility(View.INVISIBLE);
            }


            //Log.e(TAG,"arvalues.get(position).getMmTy() "+arvalues.get(position).getMmTy());
            //추천표시
            if(arvalues.get(position).getMmTy().equals("1")){
                Vholder.llBadgeBest.setVisibility(View.VISIBLE);
            }else{
                Vholder.llBadgeBest.setVisibility(View.GONE);
            }


            //금일 새 글 존재 여부
            if(arvalues.get(position).getbNew().equals("1")){
                Vholder.viewRedDot.setVisibility(View.VISIBLE);
            }else{
                Vholder.viewRedDot.setVisibility(View.GONE);
            }


        }
    }

    /**
     *
     * 썸네일 상단 간격
     */
    private void setGapLayout(int viewType, int position, View llGap) {
        switch (nViewType){//상단 간격  위에 검색,글쓰기,옵션 내려오는 부분이 생겨서 임의로 갭을 줌
            case 1://2칸
                if(position==0|position==1) {
                    llGap.setVisibility(View.VISIBLE);
                }else{
                    llGap.setVisibility(View.GONE);
                }
                break;

            case 2: //3칸

                //Vholder.imgvIconBig.
                if(position==0|position==1|position==2) {
                    llGap.setVisibility(View.VISIBLE);
                }else{
                    llGap.setVisibility(View.GONE);
                }
                break;

            case 3: //1줄
                if(position==0) {
                    llGap.setVisibility(View.VISIBLE);
                }else{
                    llGap.setVisibility(View.GONE);
                }
                break;
            default:
                llGap.setVisibility(View.GONE);
                break;
        }
    }


    /**
     * 모임 타임라인 리스트로
     * @param moimId
     */
    private void intentMoimProfile(String moimId) {
        SharedObject.setProperty_string(context, Constants.MOIM_ID,moimId);
        Intent intent = new Intent(context, Activity_MoimTimeLineList.class);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return arvalues.size();
    }
}