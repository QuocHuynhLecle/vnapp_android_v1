package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wave.messenger.activity.Activity_GroupList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_GroupEdit;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.viewholder.GroupListHolder;
import com.wave.messenger.vo.VoGroupList;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class GroupListRecyclerAdapter extends RecyclerView.Adapter<GroupListHolder> {

    Context mContext;
    private ArrayList<VoGroupList> groupLists;
    LayoutInflater mInflater;

    public GroupListRecyclerAdapter(Context context, ArrayList<VoGroupList> list) {
        super();
        mContext = context;
        groupLists = list;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList<VoGroupList> list) {
        groupLists = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public GroupListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_grouplist, parent, false);

        return new GroupListHolder(v);
    }

    @Override
    public void onBindViewHolder(GroupListHolder holder, final int position) {
        holder.tv_groupName.setText(groupLists.get(position).getGrpNm());
        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogTrace.E(groupLists.get(position).getgId() + " 수정");
                final Dialog_GroupEdit addGroup = new Dialog_GroupEdit(mContext);
                addGroup.show();
                addGroup.setTv_title(1);
                addGroup.setEt_group(groupLists.get(position).getGrpNm());
                addGroup.setOkListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!"".equals(addGroup.getGroupName())) {
                            Activity_GroupList.activity.GroupEditRequest(groupLists.get(position).getgId(), addGroup.getGroupName());
                            addGroup.dismiss();
                        } else {
                            Toast.makeText(mContext, "그룹명을 입력해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_GroupEdit addGroup = new Dialog_GroupEdit(mContext);
                addGroup.show();
                addGroup.setTv_title(2);
                addGroup.setDeleteView_Hide(true);
                addGroup.setOkListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Activity_GroupList.activity.GroupDeleteRequest(groupLists.get(position).getgId());
                        addGroup.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupLists.size();
    }
}
