package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.Cell_Friend_ListTitle;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoUsers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.wave.massenger.piggy.R.id.iv_cell_frag_frag_chat_user_image;

public class ChatListExpandableAdapter extends BaseExpandableListAdapter {

    private static final int CHILD_TYPE_1 = 0;
//    private static final int CHILD_TYPE_2 = 1;
    /**
     * 종목 채팅방 숨김처리 180418
     */
    private ArrayList<String> groupList;
    private List<VoChatList> chatList = Collections.emptyList();
    private Context mContext;

    private List<VoChatList> cachedData = null;

    public ChatListExpandableAdapter(ArrayList<String> groupList, List<VoChatList> chatList, Context context) {

        this.groupList = groupList;
        this.chatList = chatList;
        this.mContext = context;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (groupList.get(groupPosition).equals(Constants.CHATLISTTITLE_JONGMOK)) {
            return 0;
        }
        return chatList.get(groupPosition).getList().size();
    }

    public void setGroupList(ArrayList<String> groupList) {
        this.groupList = groupList;
    }

    public void setChatList(List<VoChatList> list) {
        chatList = alignList(list);
        LogTrace.E("chat list : " + chatList.size());
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return chatList.get(groupPosition).getList().get(childPosition);
    }

    public ArrayList<VoChatList> getGroupChildList(int groupPosition) {
        ArrayList<VoChatList> result = chatList.get(groupPosition).getList();
        return result;
    }

    public void showPartialSearchedList(List<String> roomIds) {
        ErrorController.showMessage("[ChatListViewAdapter] showPartialSearchedList called");

        if (cachedData != null) {
            chatList.clear();
            for (VoChatList cData : cachedData) {
                chatList.add(cData);
            }
        }

        List<VoChatList> searchedList = new ArrayList<>();

        for (String ids : roomIds) {
            VoChatList room = getDataFromId(ids);
            if (room != null) {
                searchedList.add(room);
            }
        }

        //검색 리스트와 전체 리스트를 스왑
        cachedData = new ArrayList<>();
        for (VoChatList totalData : chatList) {
            cachedData.add(totalData);
        }

        chatList.clear();
        for (VoChatList searchData : searchedList) {
            chatList.add(searchData);
        }
        ErrorController.showMessage("Check chatList size : " + chatList.size());
        notifyDataSetChanged();
    }

    public void resetSearch() {
        if (cachedData != null) {
            chatList.clear();
            for (VoChatList cData : cachedData) {
                chatList.add(cData);
            }
            cachedData = null;
            notifyDataSetChanged();
        }
    }

    private List<VoChatList> alignList(List<VoChatList> list) {
        List<VoChatList> align = new ArrayList<>();

        for (VoChatList room : list) {
            if ("3".equals(room.getRoom_type())) {
                align.add(room);
            }
        }

        for (VoChatList room : list) {
            if (!"3".equals(room.getRoom_type())) {
                align.add(room);
            }
        }

        return align;
    }

    public VoChatList getDataFromId(String id) {
        for (VoChatList room : chatList) {
            ErrorController.showMessage("[idcheck] id : " + id + ", Room ID : " + room.getRoomId());
            if (room.getRoomId().equals(id)) {
                ErrorController.showMessage("[getDataFromId] id equals room ID");
                return room;
            }
        }
        return null;
    }

    public List<VoChatList> getList() {
        return chatList;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getChildTypeCount() {
        return 1;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        VoChatList item = chatList.get(groupPosition).getList().get(childPosition);
        int result = 0;
        if (groupPosition == 0) {
            if (!item.isJongmok()) {
                result = CHILD_TYPE_1;
            }
//            else {
//                result = CHILD_TYPE_2;
//            }
        }
        return result;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Cell_Friend_ListTitle itemView;
        itemView = new Cell_Friend_ListTitle(mContext);
        if (groupList.get(groupPosition).equals("내 채팅방")) {
            itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")");
            itemView.setLayoutColor(R.color.pro_title, R.color.white);
        } else {
//            itemView.setTextViewTitle(groupList.get(groupPosition));
//            itemView.setLayoutColor(R.color.mypro_title, R.color.white);
            itemView.hideWholeGroupLayout();
        }

        switch (groupList.get(groupPosition)) {
//            case Constants.CHATLISTTITLE_JONGMOK:
//                if (mContext != Activity_JongmokList.getInstance()) {
//                    itemView.showMoreLayout();
//                    itemView.setTextViewMore("채팅방 더보기 >");
//                    if (getChildrenCount(groupPosition) <= 0) {
//                        itemView.showjongmok();
//                    }
//                } else
//                    itemView.hideMoreLayout();
//
//                itemView.hideGroupLayout();
//
//                break;
            case Constants.CHATLISTTYPE_MYCHAT:
                itemView.hideGroupLayout();
                itemView.hideMoreLayout();
                break;

            default:
                itemView.hideGroupLayout();
                itemView.hideMoreLayout();
        }
        itemView.setTextViewSize();
        return itemView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final VoChatList model = chatList.get(groupPosition).getList().get(childPosition);
        if (getChild(groupPosition, childPosition) == null) {
            LogTrace.E("chat list null : " + childPosition);
        }

        LayoutInflater inflater;
        ChatListViewHolder holder;
        int childType = getChildType(groupPosition, childPosition);

        if (convertView == null) {
            holder = new ChatListViewHolder();
            switch (childType) {
                case CHILD_TYPE_1:  //채팅 리스
                    inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.cell_chatlist_2, null);

                    holder.iv_cell_frag_frag_chat_user_image = (CircleImageView) convertView.findViewById(iv_cell_frag_frag_chat_user_image);
                    holder.iv_cell_frag_frag_chat_pro_image = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_pro_image);
                    //holder.iv_cell_frag_frag_chat_hana_members = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_hana_members);
                    holder.tv_cell_frag_frag_chat_user_name = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_name);
                    holder.tv_cell_frag_frag_chat_user_contents = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_contents);
                    holder.tv_cell_frag_frag_chat_date = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_date);
                    holder.tv_cell_frag_frag_chat_msg_count = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_msg_count);
                    holder.tv_join = (TextView) convertView.findViewById(R.id.tv_join);
                    holder.ivSelection = (ImageView) convertView.findViewById(R.id.ivSelection);
                    holder.tvMultiUserCount = (TextView) convertView.findViewById(R.id.tvMultiUserCount);
                    holder.ivMultiUserBell = (ImageView) convertView.findViewById(R.id.ivMultiUserBell);

                    holder.tv_cell_frag_frag_chat_user_name.setTextSize(17);
                    holder.tv_cell_frag_frag_chat_user_contents.setTextSize(15);
                    holder.tv_cell_frag_frag_chat_date.setTextSize(11);

                    break;
//                case CHILD_TYPE_2: //종목리스트
//                    inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    convertView = inflater.inflate(R.layout.cell_chatjongmoklist, null);
//
////                    holder.ci_cell_frag_frag_chat_user_image = (CircleImageView) convertView.findViewById(R.id.ci_cell_frag_frag_chat_user_image);
//                    holder.iv_cell_frag_frag_jmchat_user_image = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_jmchat_user_image);
//                    //holder.iv_cell_frag_frag_chat_hana_members = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_hana_members);
//                    holder.tv_cell_frag_frag_chat_user_name = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_name);
//                    holder.tv_cell_frag_frag_chat_user_contents = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_contents);
//                    holder.tv_cell_frag_frag_chat_date = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_date);
//                    holder.tv_cell_frag_frag_chat_msg_count = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_msg_count);
//                    holder.tv_join = (TextView) convertView.findViewById(R.id.tv_join);
//                    holder.ivSelection = (ImageView) convertView.findViewById(R.id.ivSelection);
//                    holder.tvMultiUserCount = (TextView) convertView.findViewById(R.id.tvMultiUserCount);
//                    holder.ivMultiUserBell = (ImageView) convertView.findViewById(R.id.ivMultiUserBell);
//
//                    holder.tv_cell_frag_frag_chat_user_name.setTextSize(17);
//                    holder.tv_cell_frag_frag_chat_user_contents.setTextSize(15);
//                    holder.tv_cell_frag_frag_chat_date.setTextSize(11);
//
//                    break;
            }
            convertView.setTag(holder);
        } else {
            holder = (ChatListViewHolder) convertView.getTag();
        }

        String url;

        switch (childType) {
            case CHILD_TYPE_1:
                //1:1 대화방
                if ("0".equals(model.getRoom_type())) {

                    ArrayList<VoUsers> list = LocalDB.getUsersDbHelper(mContext).getUserList(model.getRoomId());
                    VoUsers user = new VoUsers();

                    for (VoUsers userData : list) {
                        if (!userData.getUserId().equals(MessengerInfo.getUserId(mContext))) {
                            user = userData;
                        }
                    }

                    if (list != null)
                        holder.tv_cell_frag_frag_chat_user_name.setText(user.getUserName());
                    else
                        holder.tv_cell_frag_frag_chat_user_name.setText(user.getUserName());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId();
                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(false).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

                    holder.tvMultiUserCount.setVisibility(View.GONE);

                }

                //1:N 대화방
                else if ("1".equals(model.getRoom_type())) {
                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
//                    LogTrace.E(model.getList().get(0).getFriends().get(0).getProfileImage());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_group_default_profile);

                    holder.tvMultiUserCount.setText(model.getUsercount());
                    holder.tvMultiUserCount.setTextColor(Util.getColor(mContext, R.color.gray));
                    holder.tvMultiUserCount.setVisibility(View.VISIBLE);
                }

                //오픈 대화방, 공용 오픈 대화방
                else if ("2".equals(model.getRoom_type()) || "3".equals(model.getRoom_type())) {
                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    url = Constants.CHATDAWN_PROC + "?type=7&serverfile=" + model.getRoom_profile_image();
                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

                    if ("1".equals(model.getEntered())) {
                        holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.VISIBLE);
                        holder.tv_join.setVisibility(View.GONE);
                    } else {
                        holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.INVISIBLE);
                        holder.tv_join.setVisibility(View.VISIBLE);
                    }

                    holder.tvMultiUserCount.setText(model.getUsercount());
                    holder.tvMultiUserCount.setTextColor(Util.getColor(mContext, R.color.gray));
                    holder.tvMultiUserCount.setVisibility(View.VISIBLE);

                }

                //나와의 대화방
                else if ("4".equals(model.getRoom_type())) {
                    holder.tv_cell_frag_frag_chat_user_name.setText(MessengerInfo.getUserName(mContext));
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    url = Constants.CHATDAWN_PROC + "?type=3&userid=" + MessengerInfo.getUserId(mContext);
                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

                    holder.tvMultiUserCount.setVisibility(View.GONE);
                }

                //시스템 대화방
                else if ("5".equals(model.getRoom_type())) {
                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_default_profile_candle_img);
                    holder.tvMultiUserCount.setVisibility(View.GONE);
                }

                //그룹 쪽지방
                else if ("6".equals(model.getRoom_type())) {
                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.VISIBLE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.GONE);
                    url = Constants.CHATDAWN_PROC + "?type=3&userid=" + MessengerInfo.getUserId(mContext);
                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_group_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

                    holder.tvMultiUserCount.setVisibility(View.GONE);
                }

                //모임 대화방
                else if ("7".equals(model.getRoom_type())) {
                    ArrayList<VoUsers> list = LocalDB.getUsersDbHelper(mContext).getUserList(model.getRoomId());
                    VoUsers user = new VoUsers();

                    for (VoUsers userData : list) {
                        if (!userData.getUserId().equals(model.getOwnerId())) {
                            user = userData;
                        }
                    }
                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
                    holder.iv_cell_frag_frag_chat_user_image.setVisibility(View.GONE);
                    holder.iv_cell_frag_frag_chat_pro_image.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.iv_cell_frag_frag_chat_pro_image.setClipToOutline(true);
                    }
                    url = Constants.CHATDAWN_PROC + "?type=3&userid=" + model.getOwnerId();
                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_master_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_pro_image);

                    holder.tvMultiUserCount.setText(model.getUsercount());
                    holder.tvMultiUserCount.setTextColor(Util.getColor(mContext, R.color.gray));
                    holder.tvMultiUserCount.setVisibility(View.VISIBLE);
                }
                break;

//            case CHILD_TYPE_2:
//                if ("8".equals(model.getRoom_type())) {
//                    holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
//
//                    String itemCode = model.getRoomId();
//                    url = Constants.CHATDAWN_PROC + "?type=9&serverfile=" + itemCode.substring(4, 10);
//                    Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_master_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_jmchat_user_image);
//
//                    holder.tvMultiUserCount.setText(model.getUsercount());
//                    holder.tvMultiUserCount.setTextColor(Util.getColor(mContext, R.color.gray));
//                    holder.tvMultiUserCount.setVisibility(View.VISIBLE);
//                }
//                break;
        }
        holder.tv_cell_frag_frag_chat_user_contents.setText(model.getTitle());
        holder.ivSelection.setVisibility(View.GONE);

        if (!isToday(model.getLast_msg_date())) {
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedDate(model.getLast_msg_date()));
        } else {
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedTime(model.getLast_msg_date()));
        }

        if ("0".equals(model.getUnread()) || TextUtils.isEmpty(model.getUnread())) {
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.VISIBLE);
            holder.tv_cell_frag_frag_chat_msg_count.setText(model.getUnread());
        }

        if ("0".equals(model.getUseNoti())) {
            holder.ivMultiUserBell.setVisibility(View.VISIBLE);
        } else {
            holder.ivMultiUserBell.setVisibility(View.GONE);
        }

        return convertView;
    }

    public boolean isToday(String time) {
        boolean result = false;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            long now = System.currentTimeMillis();
            Date date = new Date(now);
            String today = format.format(date);
            Date today1 = format.parse(today);
            Date day1 = format.parse(time);
            int comareTo = day1.compareTo(today1);
            if (comareTo < 0) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception e) {

        }
        return result;

    }

    public String getFormattedDate(String time) {
        String result = "";
        String temp1;
        try {
            temp1 = time.substring(0, 10);
            if (time != null) {
                String[] temp = temp1.split("-");


                result = temp[0] + "년 " + temp[1] + "월 " + temp[2] + "일";

            }
        } catch (Exception e) {
            ErrorController.showMessage("[ChatLIstViewAdapter] getFormattedDate error occurred");
        }

        return result;
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                } else {
                    noonText = "오전";
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "" + hour;
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ViewHolderItem Class
    private class ChatListViewHolder {
        public CircleImageView iv_cell_frag_frag_chat_user_image;
        public ImageView iv_cell_frag_frag_jmchat_user_image,
                iv_cell_frag_frag_chat_pro_image, ivSelection;
        public TextView tv_cell_frag_frag_chat_user_name,
                tv_cell_frag_frag_chat_user_contents,
                tv_cell_frag_frag_chat_date, tv_cell_frag_frag_chat_msg_count;
        public TextView tvMultiUserCount, tv_join;
        public ImageView ivMultiUserBell;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
