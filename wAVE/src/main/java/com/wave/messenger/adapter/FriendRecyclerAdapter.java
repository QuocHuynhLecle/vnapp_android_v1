package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wave.massenger.piggy.R;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.viewholder.FriendHolder;
import com.wave.messenger.viewholder.GroupHolder;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 4. 14..
 */

public class FriendRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;

    final int VIEWTYPE_HEADER = 0;
    final int VIEWTYPE_ITEM = 1;

    ArrayList<String> group = new ArrayList<>();
    ArrayList<VoFriendList> friends = new ArrayList<>();

    LayoutInflater mInflater;

    boolean isSearchMode = false;

    public FriendRecyclerAdapter(Context context) {
        super();
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList<String> groupList, ArrayList<VoFriendList> friendList, boolean isSearch) {
        group = groupList;
        friends = friendList;
        isSearchMode = isSearch;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(friends.get(position).isHeader) {
            return VIEWTYPE_HEADER;
        } else {
            return VIEWTYPE_ITEM;
        }
//        if(group.get(position).) {
//            return VIEWTYPE_HEADER;
//        } else {
//            return VIEWTYPE_ITEM;
//        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;

        if(viewType == VIEWTYPE_HEADER) {
            v = mInflater.inflate(R.layout.item_header_pro, parent, false);
            LogTrace.E("VIEWTYPE_HEADER");

            return new GroupHolder(v);

        } else {
            v = mInflater.inflate(R.layout.item_friendlist_, parent, false);

            LogTrace.E("VIEWTYPE_ITEM");
            return new FriendHolder(v);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof GroupHolder) {
            ((GroupHolder) holder).tv_header.setText(friends.get(position).getUserName());

            if(isSearchMode || position == 0) {
                ((GroupHolder) holder).ll_groupmsg.setVisibility(View.GONE);

            } else {
                ((GroupHolder) holder).ll_groupmsg.setVisibility(View.VISIBLE);

            }

        } else {
            ((FriendHolder) holder).tv_name.setText(friends.get(position).getUserName());
            ((FriendHolder) holder).tv_subject.setText(friends.get(position).getProfile_subject());

            if(position == friends.size() - 1 || friends.get(position + 1).isHeader) {
                ((FriendHolder) holder).ll_line.setVisibility(View.GONE);

            } else {
                ((FriendHolder) holder).ll_line.setVisibility(View.VISIBLE);

            }

            ((FriendHolder) holder).ll_friend.setOnLongClickListener(friends.get(position).getListener());

        }
        LogTrace.E(position + " : " + friends.get(position).getUserName());
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }
}
