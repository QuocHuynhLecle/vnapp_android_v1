package com.wave.messenger.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoUsers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ChatListViewAdapter extends BaseAdapter {

    private List<VoChatList> chatList = Collections.emptyList();
    private Context mContext;

    private List<VoChatList> cachedData = null;

    // Constructor
    public ChatListViewAdapter(List<VoChatList> data, Context context) {
        chatList = alignList(data);
        mContext = context;
    }

    @Override
    public int getCount() {
        return chatList.size();
    }

    public void setChatList(List<VoChatList> list) {
        chatList = alignList(list);
        LogTrace.E("chat list : " + chatList.size());
    }

    @Override
    public Object getItem(int position) {
        return chatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void showPartialSearchedList(List<String> roomIds) {
        ErrorController.showMessage("[ChatListViewAdapter] showPartialSearchedList called");

        if (cachedData != null) {
            chatList.clear();
            for (VoChatList cData : cachedData) {
                chatList.add(cData);
            }
        }

        List<VoChatList> searchedList = new ArrayList<>();

        for (String ids : roomIds) {
            VoChatList room = getDataFromId(ids);
            if (room != null) {
                searchedList.add(room);
            }
        }

        //검색 리스트와 전체 리스트를 스왑
        cachedData = new ArrayList<>();
        for (VoChatList totalData : chatList) {
            cachedData.add(totalData);
        }

        chatList.clear();
        for (VoChatList searchData : searchedList) {
            chatList.add(searchData);
        }
        ErrorController.showMessage("Check chatList size : " + chatList.size());
        notifyDataSetChanged();
    }

    public void resetSearch() {
        if (cachedData != null) {
            chatList.clear();
            for (VoChatList cData : cachedData) {
                chatList.add(cData);
            }
            cachedData = null;
            notifyDataSetChanged();
        }
    }

    private List<VoChatList> alignList(List<VoChatList> list) {
        List<VoChatList> align = new ArrayList<>();

        for (VoChatList room : list) {
            if ("3".equals(room.getRoom_type())) {
                align.add(room);
            }
        }

        for (VoChatList room : list) {
            if (!"3".equals(room.getRoom_type())) {
                align.add(room);
            }
        }

        return align;
    }

    public VoChatList getDataFromId(String id) {
        for (VoChatList room : chatList) {
            ErrorController.showMessage("[idcheck] id : " + id + ", Room ID : " + room.getRoomId());
            if (room.getRoomId().equals(id)) {
                ErrorController.showMessage("[getDataFromId] id equals room ID");
                return room;
            }
        }
        return null;
    }

    public List<VoChatList> getList() {
        return chatList;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() < 1) {
            return 1;
        } else {
            return getCount();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(getItem(position) == null) {
            LogTrace.E("chat list null : " + position);
        }

        ChatListViewHolder holder;

        if (convertView == null) {
            holder = new ChatListViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_chatlist, parent, false);

            holder.iv_cell_frag_frag_chat_user_image = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_user_image);
            //holder.iv_cell_frag_frag_chat_hana_members = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_hana_members);
            holder.tv_cell_frag_frag_chat_user_name = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_name);
            holder.tv_cell_frag_frag_chat_user_contents = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_contents);
            holder.tv_cell_frag_frag_chat_date = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_date);
            holder.tv_cell_frag_frag_chat_msg_count = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_msg_count);
            holder.tv_join = (TextView) convertView.findViewById(R.id.tv_join);
            holder.ivSelection = (ImageView) convertView.findViewById(R.id.ivSelection);
            holder.tvMultiUserCount = (TextView) convertView.findViewById(R.id.tvMultiUserCount);
            holder.ivMultiUserBell = (ImageView) convertView.findViewById(R.id.ivMultiUserBell);

            convertView.setTag(holder);

        } else {
            holder = (ChatListViewHolder) convertView.getTag();

        }

        VoChatList model = chatList.get(position);

        //1:1 대화방
        if("0".equals(model.getRoom_type())) {

            ArrayList<VoUsers> list = LocalDB.getUsersDbHelper(mContext).getUserList(model.getRoomId());
            VoUsers user = new VoUsers();

            for(VoUsers userData : list) {
                if(!userData.getUserId().equals(MessengerInfo.getUserId(mContext))) {
                    user = userData;
                }
            }

            holder.tv_cell_frag_frag_chat_user_name.setText(user.getUserName());

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + user.getUserId() + "&date=" + user.getProfile_image();

            Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false).placeholder(R.drawable.ic_list_friend_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

            holder.tvMultiUserCount.setVisibility(View.GONE);

        }

        //1:N 대화방
        else if("1".equals(model.getRoom_type())) {
            holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());

            holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_main_group);

            holder.tvMultiUserCount.setText(model.getUsercount());
            holder.tvMultiUserCount.setVisibility(View.VISIBLE);

        }

        //오픈 대화방, 공용 오픈 대화방
        else if("2".equals(model.getRoom_type()) || "3".equals(model.getRoom_type())) {
            holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());

            String url = Constants.CHATDAWN_PROC + "?type=7&serverfile=" + model.getRoom_profile_image();
            Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_list_friend_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

            if ("1".equals(model.getEntered())) {
                holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.VISIBLE);
                holder.tv_join.setVisibility(View.GONE);
            } else {
                holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.INVISIBLE);
                holder.tv_join.setVisibility(View.VISIBLE);
            }

            holder.tvMultiUserCount.setText(model.getUsercount());
            holder.tvMultiUserCount.setVisibility(View.VISIBLE);

        }

        //나와의 대화방
        else if("4".equals(model.getRoom_type())) {
            holder.tv_cell_frag_frag_chat_user_name.setText(MessengerInfo.getUserName(mContext));

            String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + MessengerInfo.getUserId(mContext) + "&date=" + MessengerInfo.getProfileImage(mContext);
            Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_list_friend_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);

            holder.tvMultiUserCount.setVisibility(View.GONE);
        }


        //
        else if ("7".equals(model.getRoom_type())){
            holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());

            holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_main_group);

            holder.tvMultiUserCount.setText(model.getUsercount());
            holder.tvMultiUserCount.setVisibility(View.VISIBLE);
        }

        else if ("8".equals(model.getRoom_type())){
            holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());

            holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_main_group);

            holder.tvMultiUserCount.setText(model.getUsercount());
            holder.tvMultiUserCount.setVisibility(View.VISIBLE);
        }

        holder.tv_cell_frag_frag_chat_user_contents.setText(model.getTitle());
        holder.ivSelection.setVisibility(View.GONE);

        if (!isToday(model.getLast_msg_date())) {
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedDate(model.getLast_msg_date()));
        } else {
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedTime(model.getLast_msg_date()));
        }

        if ("0".equals(model.getUnread()) || TextUtils.isEmpty(model.getUnread())) {
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.INVISIBLE);
        } else {
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.VISIBLE);
            holder.tv_cell_frag_frag_chat_msg_count.setText(model.getUnread());
        }

        if ("0".equals(model.getUseNoti())) {
            holder.ivMultiUserBell.setVisibility(View.VISIBLE);
        } else {
            holder.ivMultiUserBell.setVisibility(View.GONE);
        }

        return convertView;
    }

    public boolean isToday(String time) {
        boolean result = false;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            long now = System.currentTimeMillis();
            Date date = new Date(now);
            String today = format.format(date);
            Date today1 = format.parse(today);
            Date day1 = format.parse(time);
            int comareTo = day1.compareTo(today1);
            if (comareTo < 0) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception e) {

        }
        return result;

    }

    public String getFormattedDate(String time) {
        String result = "";
        String temp1;
        try {
            temp1 = time.substring(0, 10);
            if (time != null) {
                String[] temp = temp1.split("-");


                result = temp[0] + "년 " + temp[1] + "월 " + temp[2] + "일";

            }
        } catch (Exception e) {
            ErrorController.showMessage("[ChatLIstViewAdapter] getFormattedDate error occurred");
        }

        return result;
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "0" + hour;
                } else {
                    noonText = "오전";
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "0" + hour;
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ViewHolderItem Class
    private class ChatListViewHolder {
        public ImageView iv_cell_frag_frag_chat_user_image,
                iv_cell_frag_frag_chat_hana_members, ivSelection;
        public TextView tv_cell_frag_frag_chat_user_name,
                tv_cell_frag_frag_chat_user_contents,
                tv_cell_frag_frag_chat_date, tv_cell_frag_frag_chat_msg_count;
        public TextView tvMultiUserCount, tv_join;
        public ImageView ivMultiUserBell;
    }

}
