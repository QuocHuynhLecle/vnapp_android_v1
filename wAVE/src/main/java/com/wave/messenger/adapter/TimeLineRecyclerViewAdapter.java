package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.messenger.activity.Activity_MoimContentReport;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_share;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimTimeline;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 8..
 *
 * @deprecated
 */
public class TimeLineRecyclerViewAdapter extends RecyclerView.Adapter<TimeLineRecyclerViewAdapter.ViewHolder> {
    //private String[] mDataset;

    String TAG=getClass().getSimpleName();
    ArrayList<VoMoimTimeline.VoMoimTimelineItem> arvalues= new ArrayList<>();
    Context mContext;

    int mnLikeNum=0;
    Fragment_MoimTab1_Timeline fragment_MoimTab1_Timeline;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtContent;
        private final TextView txtTime;
        private final TextView tvRegUsr;
        private final TextView tvLkCnt;

        private final LinearLayout llcontent;
        //private final ImageView imgvImage;
        private final LinearLayout llThumbnailes;


        private final ImageView ImgbOption;
        //private final LinearLayout llShare;


        private final ImageButton imgbLike;
        private final ImageButton imgbReply;
        private final ImageButton imgbShare;
        public View viewTopGap;


        String TAG = "ViewHolderItem";

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });

            tvRegUsr= (TextView) v.findViewById(R.id.tv_regUsr);
            tvLkCnt= (TextView) v.findViewById(R.id.tv_like_count);

            //textView1 = (TextView) v.findViewById(R.id.textView1);
            //textView2 = (TextView) v.findViewById(R.id.textView2);
            txtTime = (TextView) v.findViewById(R.id.tv_time);
            txtContent = (TextView) v.findViewById(R.id.tv_contents);
            // imgvImage = (ImageView) v.findViewById(R.id.imgv_image);
            llThumbnailes = (LinearLayout) v.findViewById(R.id.ll_thumbnailes);
            llcontent = (LinearLayout) v.findViewById(R.id.ll_content);

            ImgbOption=(ImageView) v.findViewById(R.id.imgb_option);
            //llShare=(LinearLayout) v.findViewById(R.id.ll_share);

            imgbLike = (ImageButton) v.findViewById(R.id.imgb_like);
            imgbReply = (ImageButton) v.findViewById(R.id.imgb_reply);
            imgbShare = (ImageButton) v.findViewById(R.id.imgb_share);

            viewTopGap = (View) v.findViewById(R.id.view_top_gap);

        }

        /*public TextView getTextView() {
            return textView;
        }*/
    }


    public TimeLineRecyclerViewAdapter(FragmentActivity activity, Fragment_MoimTab1_Timeline fragment_moimTab1_timeline) {
        this.mContext = activity;
        this.fragment_MoimTab1_Timeline = fragment_moimTab1_timeline;
    }



    public void setItem(ArrayList<VoMoimTimeline.VoMoimTimelineItem>  values, boolean mbRefresh) {
        //arvalues = values;

        if(mbRefresh)
            arvalues=new ArrayList<>();

        arvalues.addAll(values);

        //Log.e("MoimRecyclerAdapter","setItem arvalues "+arvalues.size());

    }




    // Create new views (invoked by the layout manager)
    @Override
    public TimeLineRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    ImageView mImage;

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

    if(position==0){  //내려오는 메뉴로 추가된 상단 간격
        holder.viewTopGap.setVisibility(View.VISIBLE);

    }

        //holder.textView1.setText(arvalues.get(position).getText1());
        //holder.textView2.setText(arvalues.get(position).getText2());
        holder.txtTime.setText(arvalues.get(position).getRegDt());
        holder.txtContent.setText(arvalues.get(position).getMsg());
        holder.tvRegUsr.setText(arvalues.get(position).getRegUsr());
        holder.tvLkCnt.setText(arvalues.get(position).getLkCnt());



        //Util.makeTextViewResizable(holder.txtContent, 3, mContext.getResources().getString(R.string.more), false);



        //test image
        /*ArrayList<String> arImage = new ArrayList<>();
        arImage.add("http://cdn.bgr.com/2012/11/android-icon.jpg");
        arImage.add("http://storage.googleapis.com/ix_choosemuse/uploads/2016/02/android-logo.png");
        arImage.add("https://mobilemarketingwatch.com/wp-content/uploads/2016/11/android.png");
        arImage.add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSt5yADixHqaiXDQ5QqoHBR-bBz8PaKEWC84tO_gdMTyii_p24Kgg");
        arImage.add("http://img.talkandroid.com/uploads/2015/08/android_marshmallow_large.jpg");
        arImage.add("http://zdnet2.cbsistatic.com/hub/i/2016/04/22/a1ced73f-6628-4930-96f4-d224e1d3a707/8a43709ccee674396403ee99472e38f3/android-security-1.jpg");


        holder.llThumbnailes.removeAllViews();

        for (int i = 0; i < arImage.size(); i++) {
            mImage = new ImageView(mContext);
            mImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Util.getwidth(mContext) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 0);
            layoutParams.gravity = Gravity.CENTER;

            ///mImage.setBackgroundColor(Color.parseColor("#ff999999"));

            mImage.setLayoutParams(layoutParams);
            //mImage.setScaleType(ImageView.ScaleType.FIT_XY);

            Glide.with(mContext).load(arImage.get(i)).diskCacheStrategy(DiskCacheStrategy.ALL).into(mImage);


            final String sUrl = arImage.get(i);
            holder.llThumbnailes.addView(mImage);
            //holder.llThumbnailes.setId(i);
            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("mImage", " " + sUrl + " " + position);


                    intentTimeLineDetail(arvalues.get(position));
                    *//*for (int i = 0; i < linear_below_img_view.getChildCount(); i++) {
                        View view = linear_below_img_view.getChildAt(i);
                        if (view == v) {
                            view.setBackgroundResource(R.drawable.xml_popup_border_with_bg);
                        } else {
                            view.setBackgroundResource(R.drawable.common_bg_with_dark_border);
                        }
                    }*//*

                    //loadImageonCenter(model_ProductMaster.listProductImages.get(v.getId()).ImageUrl);

                }
            });

        }*/

        //컨텐츠
        holder.llcontent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentTimeLineDetail();
            }
        });

        //본문복사, 공하기, 북마크, 신고하기, 이모임 글보지않기
        /*holder.ImgbOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dialog_TimelineOption.DialogTimelineOption((Activity) mContext).show();

                //TODO 옵션 다이얼로그
                final Dialog_TimeLineOption_2 dialog_TimeLineOption = new Dialog_TimeLineOption_2(mContext,arvalues.get(position));
                dialog_TimeLineOption.show();

                dialog_TimeLineOption.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_TimeLineOption.dismiss();

                        int i = v.getId();
                        if (i == R.id.tv_option1) {

                            Toast.makeText(mContext, "본문복사", Toast.LENGTH_SHORT).show();
                        }else if(i == R.id.tv_option2) {
                            Toast.makeText(mContext, "공유하기", Toast.LENGTH_SHORT).show();
                        }else if(i == R.id.tv_option3) {
                            Toast.makeText(mContext, "북마크", Toast.LENGTH_SHORT).show();
                        }else if(i == R.id.tv_option4) {
                            Toast.makeText(mContext, "신고하기", Toast.LENGTH_SHORT).show();

                            SharedObject.setProperty_string(mContext, Constants.MOIM_ID,arvalues.get(position).getMmId());
                            SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID,arvalues.get(position).getMsgId());

                            intentContentReport();

                        }else if(i == R.id.tv_option5) {
                            Toast.makeText(mContext, "이 모임글 보지 않기", Toast.LENGTH_SHORT).show();
                            SharedObject.setProperty_string(mContext, Constants.MOIM_ID,arvalues.get(position).getMmId());

                            moimBbsBlockRequest("1");
                        }
                    }
                });




            }
        });*/


        //좋아요
        holder.imgbLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moimEmotionRequest(arvalues.get(position).getMmId(),arvalues.get(position).getMsgId(),"1");

                mnLikeNum=position;
            }
        });

        holder.imgbReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //공유하기 다이얼로그
        holder.imgbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog_share mDialog = new Dialog_share(mContext);
                mDialog.show();
            }
        });

    }

    /**
     * 타임라인 상세화면 intent
     *
     */
    private void intentTimeLineDetail() {
        Intent intent = new Intent(mContext, Activity_TimeLineDetail.class);
        mContext.startActivity(intent);
    }

    /**
     * 컨텐츠 신고하기
     */
    private void intentContentReport() {
        Intent intent = new Intent(mContext, Activity_MoimContentReport.class);
        mContext.startActivity(intent);
    }



    @Override
    public int getItemCount() {
        return arvalues.size();

    }



    /**
     * 7.16 모임 공지사항 설정/해제
     noti     공지 여부(int)     0:일반, 1:공지
     */
    private void moimNotice(String msgId, String noti) {
        Moa.moimNotice(mContext, msgId, noti, mMainActvtHandler);
    }


    /**
     * 7.23 모임 글 감정표현

     * @param mmId
     * @param msgId
     * @param emt  감정표현(int) 0:기존감정표현취소, 1:좋아요, 2:싫어요
     */
    private void moimEmotionRequest( String mmId, String msgId, String emt) {
            Moa.moimEmotionRequest(mContext, mmId, msgId, emt, mMainActvtHandler);
    }

    /**
     * 7.29 모임 글 차단
     * @param mmBbsBlk
     */
    private void moimBbsBlockRequest(String mmBbsBlk) {
        Moa.moimBbsBlockRequest(mContext, mmBbsBlk, mMainActvtHandler);
    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);
                String strResult;

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_EMOTION :    //7.23 모임 글 감정표현
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());
                            Toast.makeText(mContext, "좋아요 완료", Toast.LENGTH_SHORT).show();





                            arvalues.get(mnLikeNum).setLkCnt((Integer.parseInt(arvalues.get(mnLikeNum).getLkCnt())+1)+"");
                            notifyDataSetChanged();


                        }else{
                            Toast.makeText(mContext, "좋아요 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_BBS_BLOCK :    //7.29 모임 글 차단
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());
                            Toast.makeText(mContext, "차단 완료", Toast.LENGTH_SHORT).show();

                            //notifyDataSetChanged();
                            //fragment_MoimTab1_Timeline.moimSearchRequest("","");

                            fragment_MoimTab1_Timeline.refreshItems();//7.27 모임 타임라인 다시 호출

                        }else{
                            Toast.makeText(mContext, "차단 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


}



