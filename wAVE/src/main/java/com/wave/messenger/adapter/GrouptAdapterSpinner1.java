package com.wave.messenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.vo.VoGrouptItem;

import java.util.ArrayList;

public class GrouptAdapterSpinner1 extends BaseAdapter {


    Context context;
    ArrayList<VoGrouptItem> data;
    LayoutInflater inflater;


    public GrouptAdapterSpinner1(Context context, ArrayList<VoGrouptItem> data){
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data!=null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.spinner_spinner1_normal, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String text = data.get(position).getXprtGrpNm();
            ((TextView)convertView.findViewById(R.id.spinnerText)).setText(text);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.spinner_spinner1_dropdown, parent, false);
        }

        //데이터세팅
        String text = data.get(position).getXprtGrpNm();
        ((TextView)convertView.findViewById(R.id.spinnerText)).setText(text);

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String geGrouptItemid(int position) {
        return  data.get(position).getXprtGrpId();
    }
}
