package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.RateShareHolder;
import com.wave.messenger.vo.VoRateStock;

import java.util.List;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class RateShareAdapter extends RecyclerView.Adapter<RateShareHolder> {
    Context mContext;
    List<VoRateStock> portfolioList;
    String who;

    public RateShareAdapter(Context context, String who) {
        mContext = context;
        this.who = who;
    }

    public void setList(List<VoRateStock> list) {
        portfolioList = list;
        notifyDataSetChanged();
    }



    @Override
    public RateShareHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(parent.getContext(), R.layout.cell_rateshare, null);
        return new RateShareHolder(v);
    }

    @Override
    public void onBindViewHolder(RateShareHolder holder, final int position) {
        holder.tv_itemname.setText(portfolioList.get(position).getItemName());
        holder.tv_purchase.setText(portfolioList.get(position).getAvgPrice());
        holder.tv_purchaseps.setText(portfolioList.get(position).getProfit());
        holder.tv_profitps.setText(portfolioList.get(position).getProfitVal());

        if(who.equals("me")){
            holder.tv_itemname.setBackgroundColor(Color.parseColor("#efebf6"));
            holder.tv_purchase.setBackgroundColor(Color.parseColor("#efebf6"));
            holder.tv_purchaseps.setBackgroundColor(Color.parseColor("#efebf6"));
            holder.tv_profitps.setBackgroundColor(Color.parseColor("#efebf6"));
        }else {
            holder.tv_itemname.setBackgroundColor(Color.parseColor("#e1e1e1"));
            holder.tv_purchase.setBackgroundColor(Color.parseColor("#e1e1e1"));
            holder.tv_purchaseps.setBackgroundColor(Color.parseColor("#e1e1e1"));
            holder.tv_profitps.setBackgroundColor(Color.parseColor("#e1e1e1"));
        }
    }

    @Override
    public int getItemCount() {
        return portfolioList.size();
    }
}
