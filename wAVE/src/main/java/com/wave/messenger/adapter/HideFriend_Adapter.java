package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.wave.messenger.activity.Activity_HideFriend;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_FriendSet;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoHiddenList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 14..
 */

public class HideFriend_Adapter extends RecyclerView.Adapter<HideFriend_Adapter.ViewHolder> {
    String TAG = HideFriend_Adapter.class.getName();
    ArrayList<VoHiddenList> arvalues;
    Context context;
    private String type;

    public HideFriend_Adapter(Context con, ArrayList<VoHiddenList> values, String type) {
        this.arvalues = values;
        this.context = con;
        this.type = type;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public CircleImageView ivProfile;
        public TextView tv_set;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            ivProfile = (CircleImageView) itemView.findViewById(R.id.iv_profile);
            tv_set = (TextView) itemView.findViewById(R.id.tv_set);
        }
    }


    @Override
    public HideFriend_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_hidden_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HideFriend_Adapter.ViewHolder holder, final int position) {

        if (type.equals("0")) {
            holder.tv_set.setText("관리");
        } else if (type.equals("1")) {
            holder.tv_set.setText("해제");
        }
        holder.tv_name.setText(arvalues.get(position).getUserName());

        final String url = Constants.CHATDAWN_PROC + "?type=2&userid=" + arvalues.get(position).getUserId() + "&date=" + arvalues.get(position).getProfile_image();

        Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).signature(new StringSignature(UUID.randomUUID().toString())).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.ivProfile);

        holder.tv_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //숨김친구관리일때
                if (type.equals("0")) {
                    final Dialog_FriendSet dialog_friendSet = new Dialog_FriendSet(context);
                    dialog_friendSet.show();

                    dialog_friendSet.setListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_friendSet.dismiss();

                            int i = v.getId();
                            if (i == R.id.tv_cancel) {
                                dialog_friendSet.dismiss();

                            } else if (i == R.id.tv_confirm) {
                                if (dialog_friendSet.btn_cancel.isChecked()) {
                                    Moa.hideList(context, arvalues.get(position).getPhone(), 0, mActivityHandler);
                                    Fragment_Main.getInstance().FriendListRequest();
                                } else if (dialog_friendSet.btn_block.isChecked()) {
                                    final Dialog_Text dialog_text = new Dialog_Text(context, context.getResources().getString(R.string.block), "차단", Constants.DIALOG_BUTTON_TYPE.friend_block);
                                    dialog_text.show();

                                    dialog_text.setListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog_text.dismiss();


                                            Moa.blockList(context, arvalues.get(position).getPhone(), 1, mActivityHandler);

                                        }
                                    });
                                } else if (dialog_friendSet.btn_delete.isChecked()) {


                                    final Dialog_Text dialog_text = new Dialog_Text(context, context.getResources().getString(R.string.friend_delete), "친구 삭제", Constants.DIALOG_BUTTON_TYPE.friend_delete);
                                    dialog_text.show();

                                    dialog_text.setListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog_text.dismiss();
                                            JSONArray ja = new JSONArray();
                                            JSONObject jo = new JSONObject();

                                            try {
                                                jo.put("tel", arvalues.get(position).getPhone());
                                                ja.put(jo);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            Moa.friendDelete(context, ja, mActivityHandler);

                                        }
                                    });


                                }


                            }
                        }
                    });
                }
                //차단친구관리일때
                else if (type.equals("1")) {
                    Moa.blockList(context, arvalues.get(position).getPhone(), 0, mActivityHandler);
                    Fragment_Main.getInstance().FriendListRequest();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if (arvalues != null) return arvalues.size();
        else
            return 0;
    }

    //결과 처리 핸들러
    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);
                String strResult = (String) data.body.get("result");
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_ADDRESS_HIDDEN:    //

                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            String user_phone = data.body.getJson("params").getString("users");

                            for (int i = 0; i < arvalues.size(); i++) {
                                if (user_phone.equals(arvalues.get(i).getPhone())) {
                                    removeAt(i);
                                    break;
                                }
                            }


                        } else {


                        }
                        break;
                    case PacketTypes.PTC_IMS_ADDRESS_BLOCK:
                        Log.e(TAG, "strResult: " + strResult);


                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            String user_phone = data.body.getJson("params").getString("users");

                            for (int i = 0; i < arvalues.size(); i++) {
                                if (user_phone.equals(arvalues.get(i).getPhone())) {
                                    removeAt(i);
                                    break;
                                }
                            }


                        } else {


                        }
                        break;
                    case PacketTypes.PTC_IMS_ADDRESS_DELETE:
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            String user_phone = null;
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            JSONArray ja = new JSONArray(data.body.get("params").toString());
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                user_phone = jo.getString("tel");
                            }
                            for (int i = 0; i < arvalues.size(); i++) {
                                if (user_phone.equals(arvalues.get(i).getPhone())) {
                                    removeAt(i);
                                    break;
                                }
                            }


                        } else {


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    public void removeAt(int position) {
        arvalues.remove(position);

        if (context instanceof Activity_HideFriend) {
            ((Activity_HideFriend) context).RemoveAdapter(position, arvalues.size());
        }


        //Fragment_MoimTab1_Timeline
    }


}
