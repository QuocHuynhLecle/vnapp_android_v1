package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.fragment.Fragment_RecommendFriend;
import com.wave.messenger.mvp.AddFriend.AddFriendPresenter;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 9. 22..
 */

public class FriendRecommendAdapter extends RecyclerView.Adapter<FriendRecommendAdapter.ViewHolder> {
    ArrayList<VoFriendList> arvalues;
    Context context;
    AddFriendPresenter presenter;
    Fragment_RecommendFriend fragment_recommendFriend;


    public FriendRecommendAdapter(Context context, ArrayList<VoFriendList> arvalues) {
        this.arvalues = arvalues;
        this.context = context;
    }

    public void setItem(ArrayList<VoFriendList> arvalues) {
        this.arvalues = arvalues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_hidden_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.iv_plus_friends.setVisibility(View.VISIBLE);
        holder.tv_set.setVisibility(View.GONE);
        holder.tv_name.setText(arvalues.get(position).getUserName());

        holder.iv_plus_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Moa.friendsAdd(context, arvalues.get(position).getUserName(), arvalues.get(position).getUserId(), mActivityHandler);
            }
        });

        String friend_url = Constants.CHATDAWN_PROC + "?type=3&userid=" + arvalues.get(position).getUserId() + "&date=" + arvalues.get(position).getProfileImage();
        Log.i("TEST", "프로필사진 : " +  friend_url);
        Glide.with(context).load(friend_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.ivProfile);


    }

    @Override
    public int getItemCount() {
        if (arvalues != null) return arvalues.size();
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public CircleImageView ivProfile;
        public TextView tv_set;
        public ImageView iv_plus_friends;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            ivProfile = (CircleImageView) itemView.findViewById(R.id.iv_profile);
            tv_set = (TextView) itemView.findViewById(R.id.tv_set);
            iv_plus_friends = (ImageView) itemView.findViewById(R.id.iv_plus_friends);
        }
    }

    //결과 처리 핸들러
    private Handler mActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                String strResult = (String) data.body.get("result");
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_ADDRESS_ADDBUDDY:    //

                        if (strResult.equals(Constants.SUCCESS)) {

                            Fragment_Main.getInstance().FriendListRequest();

                            String user_phone = data.body.getJson("params").getString("userId");

                            for (int i = 0; i < arvalues.size(); i++) {
                                if (user_phone.equals(arvalues.get(i).getUserId())) {
                                    removeAt(i);

                                    break;
                                }
                            }


                        } else {


                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    public void removeAt(int position) {

        arvalues.remove(position);

        Fragment_RecommendFriend.fragment.RemoveAdapter(position, arvalues.size());
    }
}
