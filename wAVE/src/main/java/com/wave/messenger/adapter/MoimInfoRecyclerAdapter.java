package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_MoimMemberInfoList;
import com.wave.messenger.activity.Activity_MoimProfile;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_ForceExit;
import com.wave.messenger.dialog.Dialog_MoimInfoListOption;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 6. 15..
 * 모임정보 어댑터
 * 모임가입된 회원 리스트 어댑
 */
public class MoimInfoRecyclerAdapter extends RecyclerView.Adapter<MoimInfoRecyclerAdapter.ViewHolder> {

    String TAG =getClass().getSimpleName();
    //ArrayList<VoMoimMember> arvalues;
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arvalues= new ArrayList<>();

    Context context;

    public MoimInfoRecyclerAdapter(Context con) {
        context = con;
    }

    public void setItem(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        Log.e(TAG,"setItem: params "+params.size());
        this.arvalues=params;
    }

    /**
     * 멤버리스트를 가져온다
     * @return
     */
    public ArrayList<VoMoimMemberList.VoMoimMemberlistItem> getMemberlistItem() {
        return arvalues;
    }
    public void setSearchMemberlistItem() {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public TextView tvProfile;
        public TextView tvPhone;
        public ImageView imgLeader; //리더
        public ImageView imgJointLeader;    //공동리더
        public ImageView imgvProfile;       //프로필 이미지

        public ImageButton imgbOption;    //1:1대화버튼 [다른사람]
        public ImageButton imgbSetProfile;  //프로필설정 [나]
        public LinearLayout llItem;

        public TextView tvRegDt;    //가입일 표시안함


        public ViewHolder(View v) {

            super(v);
            tvName = (TextView) v.findViewById(R.id.tv_name);
            tvProfile= (TextView) v.findViewById(R.id.tv_set_profile);
            tvPhone = (TextView) v.findViewById(R.id.tv_phone);
            imgvProfile= (ImageView) v.findViewById(R.id.imgv_profile);
            imgLeader = (ImageView) v.findViewById(R.id.imgv_leader);
            imgJointLeader = (ImageView) v.findViewById(R.id.imgv_joint_leader);
            imgbOption = (ImageButton) v.findViewById(R.id.imgb_option);
            imgbSetProfile = (ImageButton) v.findViewById(R.id.imgb_set_profile);


            llItem = (LinearLayout) v.findViewById(R.id.ll_item);


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_member_moim_info, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

       Log.e("MoimInfoRecyclerAdapter","onBindViewHolder: "+arvalues.get(position).getUsNm());

       Vholder.tvName.setText(arvalues.get(position).getUsNm());
       //번호 숨기기
        // Vholder.tvPhone.setText(arvalues.get(position).getTel());  //전화번호 값없음


        //TODO 임의 처리.//나일경우
       if(position==0 && arvalues.get(position).getUserId().equals(MessengerInfo.getUserId(context)) ){
            Vholder.imgbSetProfile.setVisibility(View.VISIBLE);
            Vholder.imgbOption.setVisibility(View.GONE);
        }else{
           Vholder.imgbSetProfile.setVisibility(View.GONE);
           Vholder.imgbOption.setVisibility(View.VISIBLE);
       }


        //Vholder.imgLeader

        //리더마크
        switch (arvalues.get(position).getUsrLv()){
            case "0":   //일반
                Vholder.imgLeader.setVisibility(View.GONE);
                Vholder.imgJointLeader.setVisibility(View.GONE);
                break;
            case "1":   //리더
                Vholder.imgLeader.setVisibility(View.VISIBLE);
                Vholder.imgJointLeader.setVisibility(View.GONE);
                break;

            case "2":   //공동리더
                Vholder.imgLeader.setVisibility(View.GONE);
                Vholder.imgJointLeader.setVisibility(View.VISIBLE);
                break;
        }

        //자신일경우 프로필설정 표시
        /*if(arvalues.get(position).getUserId().equals(MessengerInfo.getUserId(context))){
            Vholder.tvProfile.setVisibility(View.VISIBLE);
        }*/

        //프로필화면
        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentProfile(arvalues.get(position).getUserId());
            }
        });


        //프로필이미지
        //Util.setMoimProfileGlide(context, SharedObject.getProperty_string(context, Constants.MOIM_ID, null), arvalues.get(position).getPfImg() ,Vholder.imgvProfile);

        Util.setMoimProfileGlide2(context, arvalues.get(position).getUserId(), Vholder.imgvProfile);



        //나의 프로필
        //우측 옵션버튼 설정
        Vholder.imgbSetProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentProfile(arvalues.get(position).getUserId());
            }
        });


        Vholder.imgbOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog_MoimInfoListOption dialog_MoimInfoListOption = new Dialog_MoimInfoListOption(context, arvalues.get(position).getUsrLv()  );
                dialog_MoimInfoListOption.show();

                dialog_MoimInfoListOption.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_MoimInfoListOption.dismiss();

                        int i = v.getId();

                        if (i == R.id.tv_option0) {     //프로필
                            intentProfile(arvalues.get(position).getUserId());


                        }else if (i == R.id.tv_option1){    //1:1대
                            intentChat(context, arvalues.get(position).getUserId(),arvalues.get(position).getUsNm());

                        }else if (i == R.id.tv_option2){    //강제탈퇴 팝업
                            dialogForseExit(arvalues.get(position).getUserId());
                        }
                    }
                });


            }//onClick
        });

        /*

        switch (Util.getMymLv(context)){
            case "0":   //멤버
                Vholder.imgbOption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intentProfile(arvalues.get(position).getUserId());
                    }
                });



                break;

            case "1": //리더일경우 팝업

                Vholder.imgbOption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Dialog_MoimInfoListOption dialog_MoimInfoListOption = new Dialog_MoimInfoListOption(context);
                        dialog_MoimInfoListOption.show();

                        dialog_MoimInfoListOption.setListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog_MoimInfoListOption.dismiss();

                                int i = v.getId();

                                if (i == R.id.tv_option0) {     //프로필
                                    intentProfile(arvalues.get(position).getUserId());


                                }else if (i == R.id.tv_option1){    //1:1대
                                    intentChat(context, arvalues.get(position).getUserId(),arvalues.get(position).getUsNm());

                                }else if (i == R.id.tv_option2){    //강제탈퇴 팝업
                                    dialogForseExit(arvalues.get(position).getUserId());
                                }
                            }
                        });


                    }//onClick
                });


                break;

            case "2": //공동리더 일경우? //TODO

                break;

        }*/

        //내가 리더인지 구qns
       /*if(Util.isMoimRegUsr(context)){
            //옵션
        }else{

        }*/
        //사용자 레벨(int) 0:일반, 1:리더, 2:공동리더
        //arvalues.get(position).getUsrLv().equals("");


    }


        /**
         * 채팅 인탠트
         */
        private void intentChat(Context context, String friendProfileId, String userName) {
            //TODO 채팅 연결
            //Toast.makeText(context, "1:1대화", Toast.LENGTH_SHORT).show();

            //String friendProfileId = SharedObject.getProperty_string(context, Constants.MOIM_USER_PROFILE_ID, null);


            //BuddyDbHelper db = LocalDB.getBuddyDbHelper(Activity_MoimProfile.this);
            //VoFriendList friend=db.getBuddy(friendProfileId);

            VoFriendList friend= new VoFriendList();
            friend.setUserId(friendProfileId);
            friend.setUserName(userName);



            ChatListDbHelper listDb = LocalDB.getChatListDbHelper(context);
            List<VoFriendList> friends = new ArrayList<>();
            VoChatList chatRoomInfo = new VoChatList();
            String roomId;


            UsersDbHelper userDb = LocalDB.getUsersDbHelper(context);
            ArrayList<String> roomIdList = listDb.selectSingleRoom();
            roomId = userDb.existUserCaht(roomIdList, friendProfileId);
            chatRoomInfo.setRoom_type("0");


            VoFriendList my = new VoFriendList();
            my.setUserId(MessengerInfo.getUserId(context));
            my.setUserName(MessengerInfo.getUserName(context));
            my.setRealUserName(MessengerInfo.getRealUserName(context));

            friends.add(my);
            friends.add(friend);


            chatRoomInfo.setFriends(friends, context);

            if (!TextUtils.isEmpty(roomId)) {
                chatRoomInfo.setRoomId(roomId);
            }
            Statics.ROOMINFO = chatRoomInfo;

            BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

            Intent mIntent = new Intent(context, Activity_ChatRoom.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(mIntent);


        }



    /**
     * 강제 탈퇴 가이얼로그
     * @param userId
     */
    private void dialogForseExit(final String userId) {

        final Dialog_ForceExit dialog_ForceExit = new Dialog_ForceExit(context);
        dialog_ForceExit.show();

        dialog_ForceExit.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_ForceExit.dismiss();
                int i = v.getId();
                if (i == R.id.tv_force_exit) {     //탈퇴

                    MoimForceExitRequest(userId,"0");
                }else if (i == R.id.tv_block){    //탈퇴후 차단

                    MoimForceExitRequest(userId,"1");
                }
            }
        });
    }

    /**
     * 프로필 설정화면
     * @param userId
     */
    private void intentProfile(String userId) {
        SharedObject.setProperty_string(context, Constants.MOIM_USER_PROFILE_ID, userId);
        //SharedObject.getProperty_string(Activity_MoimProfile.this, Constants.MOIM_ID
        Intent mIntent = new Intent(context, Activity_MoimProfile.class);
        context.startActivity(mIntent);
    }


    @Override
    public int getItemCount() {

        // 리더, 일반회원 구분없이 모두 표출 180515 mwj
        return arvalues.size();

//        if(Util.getPermissionLv(Util.getMymLv(context),Util.getFrcTy(context))){ //TODO 탈퇴권한으로 구분 일반회원은 리스트0
//            return arvalues.size();
//        }else{
//            return 0;
//        }

    }


    /**
     * 7.11 모임 강제탈퇴
     * PTG : 0x00080000, PTC : 0x00080008
     PTG_IMS_MOIM, PTC_IMS_MOIM_FORCE_EXIT(차단로직구현필요)
     */
    private void MoimForceExitRequest(String mmExtUsr,String mmUsrBlk) {
        MOALog.w(TAG+" requestMoimProfile");
        String mRoomId = SharedObject.getProperty_string(context, Constants.MOIM_CHATROOMID, null);
        Moa.moimForceExit(context,mmExtUsr, mmUsrBlk, mMainActivityHandler);
        //대화방 강제탈퇴
        //Moa.chatRoomForceExit(context, mRoomId, mUserOutId, mRoomUserName, mMainActivityHandler);
    }

    //강제탈 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_FORCE_EXIT :    //모임 멤버 리스트
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            String extUsr= data.body.getJson("params").getString("mmExtUsr");

                            Log.e(TAG,"params: extUsrextUsr "+extUsr);

                            removeItem(extUsr); //리스트에서 삭제
                            Toast.makeText(context, "처리했습니다", Toast.LENGTH_SHORT).show();

                            //TODO 탈퇴 강제탈퇴후 멤버수 카운 -1
                            Activity_MoimTimeLineList.activity.memberCountDown();


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 차단한 아이디를 리스트에서 삭제한다
     * @param extUsr
     */
    private void removeItem(String extUsr) {

        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if(item.getUserId().equals(extUsr)){
                arvalues.remove(item);

                Log.e(TAG,"removeItem: "+extUsr);
                break;
            }
        }

        ((Activity_MoimMemberInfoList)context).refreshList();

    }


}