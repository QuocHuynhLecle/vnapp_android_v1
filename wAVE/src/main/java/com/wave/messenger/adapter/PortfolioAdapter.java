package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.PortfolioHolder;
import com.wave.messenger.vo.VoPortfolioList;

import java.util.List;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioHolder> {
    Context mContext;
    List<VoPortfolioList> portfolioList;

    public PortfolioAdapter(Context context) {
        mContext = context;
    }

    public void setList(List<VoPortfolioList> list) {
        portfolioList = list;
        notifyDataSetChanged();
    }

    @Override
    public PortfolioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(parent.getContext(), R.layout.cell_portfolio, null);
        return new PortfolioHolder(v);
    }

    @Override
    public void onBindViewHolder(PortfolioHolder holder, final int position) {
        holder.tv_itemname.setText(portfolioList.get(position).getM_ItemName());
        holder.tv_purchase.setText(portfolioList.get(position).getM_Purchase());
        holder.tv_purchaseps.setText(portfolioList.get(position).getM_PurchasePs());
        holder.tv_profitps.setText(portfolioList.get(position).getM_ProfitPs());
    }

    @Override
    public int getItemCount() {
        return portfolioList.size();
    }
}
