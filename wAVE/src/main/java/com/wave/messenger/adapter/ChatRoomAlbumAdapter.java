package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.mvp.Profile.DataModel.ThumbnailBitmap;
import com.wave.messenger.mvp.Profile.IProfileImageDownloadListener;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 2016-09-07.
 */
public class ChatRoomAlbumAdapter extends RecyclerView.Adapter<ChatRoomAlbumAdapter.ChatRoomAlbumViewHolder> {

    private OnListClickListener listener;
    private OnListLongClickListener longClickListener;

    public List<AlbumPicture> data = Collections.emptyList();
    private Context context;

    public ChatRoomAlbumAdapter(List<AlbumPicture> data, OnListClickListener listener, OnListLongClickListener longClickListener, Context context) {
        this.data = data;
        this.longClickListener = longClickListener;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ChatRoomAlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_album, parent, false);
        return new ChatRoomAlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatRoomAlbumViewHolder holder, int position) {
        AlbumPicture item = data.get(position);
        holder.ivLayout.setSelected(item.isSelected());
        if(!item.isSelectionMode()) {
            holder.ivLayout.setImageResource(0);
        }else{
            holder.ivLayout.setImageResource(R.drawable.selector_album_sel);
        }

        Glide.with(context).load(item.getUrlPath()).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.img_picture_default_android).error(R.drawable.img_picture_default_android).dontAnimate().into(holder.ivPicture);
    }

    /**
     * @return 선택된 사진들의 정보를 돌려준다.
     */
    public List<AlbumPicture> getSelectedData() {
        List<AlbumPicture> result = new ArrayList<>();
        for (AlbumPicture pic : data) {
            if (pic.isSelected())
                result.add(pic);
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItemSelected(int position) {

        if (!data.get(position).isSelected()) {
            data.get(position).setSelected(true);
        } else {
            data.get(position).setSelected(false);
        }
        notifyDataSetChanged();
    }

    public void setUnSelected() {

        for(AlbumPicture pic : data){
            pic.setSelected(false);
        }
        notifyDataSetChanged();

    }

    public void setBGFrame(boolean b) {

        for(AlbumPicture pic : data){
            pic.setSelectionMode(b);
        }
        notifyDataSetChanged();
    }

    public class ChatRoomAlbumViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivPicture, ivLayout;

        public ChatRoomAlbumViewHolder(View itemView) {
            super(itemView);

            ivPicture = (ImageView) itemView.findViewById(R.id.ivPicture);
            ivLayout = (ImageView) itemView.findViewById(R.id.ivLayout);

            ivLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickList(data.get(getAdapterPosition()), getAdapterPosition());
                }
            });

            ivLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    longClickListener.onLongClick();
                    return true;
                }
            });
        }
    }//end of inner class

    public interface OnListClickListener {
        void onClickList(AlbumPicture picture, int position);
    }

    public interface OnListLongClickListener {
        void onLongClick();
    }

    //ImageDownloader
    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        private String address = Constants.CHATDAWN_PROC + "?";

        private IProfileImageDownloadListener listener;

        public ImageDownloader(IProfileImageDownloadListener listener) {
            super();
            this.listener = listener;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = address + "type=" + params[0] + "&userid="
                        + params[1];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");
                Statics.getProfileImageCache().put(params[1], new ThumbnailBitmap(image, false));
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[ProfileModel] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                listener.onImageDownloadSuccess(result);
            } else {
                listener.onImageDownloadFailed();
            }
        }
    }
}
