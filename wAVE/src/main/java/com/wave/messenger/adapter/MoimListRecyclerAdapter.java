package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.RadiusImageView;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.vo.VoMoimSearch;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 9..
 * 모임명 및 종목코드 검색 화면 어댑터
 *
 * Activity_MoimList
 */
public class MoimListRecyclerAdapter extends RecyclerView.Adapter<MoimListRecyclerAdapter.ViewHolder>{

    //ArrayList<ArrayList<VoMoimSearch.VoMoimSearchItem> arvalues;
    ArrayList<VoMoimSearch.VoMoimSearchItem>arvalues= new ArrayList<>();
    Context context;

    public MoimListRecyclerAdapter(Context con){
        //arvalues = values;
        context = con;
    }

    public void setItem(ArrayList<VoMoimSearch.VoMoimSearchItem> values, boolean reset) {
        //arvalues = values;
        if (reset) {
            arvalues = new ArrayList<>();
        }
        arvalues.addAll(values);
        Log.e("MoimListRecyclerAdapter","arvalues.size:" +arvalues.size());
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvMoimNm;
        public TextView tvUsrCnt;
        public TextView tvLeader;
        public TextView tvMoimInfo;
        //public RadiusImageView imgThum;
        public LinearLayout llItem;

        public ImageView imgThum;
        public ImageView imgThumIcon;

        public ViewHolder(View v){

            super(v);
            //imgThum=(RadiusImageView)  v.findViewById(R.id.imgv_moim_thum);
            tvMoimNm = (TextView) v.findViewById(R.id.tv_moim_name);
            tvUsrCnt = (TextView) v.findViewById(R.id.tv_user_cnt);
            tvLeader = (TextView) v.findViewById(R.id.tv_leader);
            llItem =(LinearLayout) v.findViewById(R.id.ll_item);
            tvMoimInfo = (TextView) v.findViewById(R.id.tv_moim_introduce);

            imgThum = (RadiusImageView)v.findViewById(R.id.imgv_thum_img);
            imgThumIcon= (ImageView)v.findViewById(R.id.imgv_thum_icon);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View view1 = LayoutInflater.from(context).inflate(R.layout.recycler_moimlist_item,parent,false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position){

        Log.e("","MmNm: "+arvalues.get(position).getMmNm()+" ImgTy: "+arvalues.get(position).getImgTy()+" MmTg: "+arvalues.get(position).getMmTg()+" ImgTmb: "+arvalues.get(position).getImgTmb() + " MmInfo : " + arvalues.get(position).getIntr());

        Vholder.tvMoimNm.setText(arvalues.get(position).getMmNm());
        Vholder.tvUsrCnt.setText(arvalues.get(position).getUsrCnt());
        //Vholder.tvLeader.setText(arvalues.get(position).getRegUsr());  //TODO 이름으로 수정
        Vholder.tvLeader.setText(arvalues.get(position).getUsNm());
        Vholder.tvMoimInfo.setText(arvalues.get(position).getIntr());


        String intr =arvalues.get(position).getIntr();
        /*if(arvalues.get(position).getIntr() == ""){
            Vholder.tvMoimInfo.setVisibility(View.GONE);
        }*/

        if(TextUtils.isEmpty(intr)){
            Vholder.tvMoimInfo.setVisibility(View.GONE);
        }else{
            Vholder.tvMoimInfo.setVisibility(View.VISIBLE);
            Vholder.tvMoimInfo.setText(intr);
        }


        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(context, Activity_MoimTimeLineList.class);
                SharedObject.setProperty_string(context, Constants.MOIM_ID,arvalues.get(position).getMmId());
                context.startActivity(mIntent);
            }
        });

        Vholder.imgThum.setImageResource(0);

        if(arvalues.get(position).getImgTy().equals("0")){

            Vholder.imgThumIcon.setVisibility(View.VISIBLE);
            Vholder.imgThum.setVisibility(View.GONE);


            switch (arvalues.get(position).getMmTg()) {
                case "1":   //1 : 타임교육
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_time_edu);
                    break;

                case "2":   //2 : 개인
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_personal);
                    break;

//                case "1":   //1 : 주식
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_stock);
//                    break;
//
//                case "2":   //2 : 국내파생
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_derivation);
//                    break;
//
//                case "3":    //3 : 해외파생
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_derivation);
//                    break;
//
//                case "4":   //4 : 해외주식
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_stock);
//                    break;
//
//                case "5":   //5 : 금융상품
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_bank_product);
//                    break;
//
//                case "6":   //6 : 기타
//                    //Vholder.imgThum.getDrawable(R.drawable.ic_etc);
////                    Vholder.imgThum.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_etc));
//                   // Vholder.imgThum.setImageResource(R.drawable.ic_etc);
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_etc);
//                    break;
            }

        }else if(arvalues.get(position).getImgTy().equals("1")) {

            Vholder.imgThumIcon.setVisibility(View.GONE);
            Vholder.imgThum.setVisibility(View.VISIBLE);

            Glide.with(context).load(Constants.MOIM_LIST_IMAGE_URL + arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_default_my_profile).dontAnimate().into(Vholder.imgThum);      //TODO 기본이미지 수정

        }

    }

    @Override
    public int getItemCount(){

        return arvalues.size();
    }


    /**
     * 마지막 읽은 모임 키값
     */
    public String getLastMmId() {
        //Log.e(TAG,"loadNextDataFromApi getLastMmId : "+arvalues.get(arvalues.size()-1).getMmId()+" "+arvalues.get(arvalues.size()-1).getMmNm());
        return arvalues.get(arvalues.size()-1).getMmId();
    }
}