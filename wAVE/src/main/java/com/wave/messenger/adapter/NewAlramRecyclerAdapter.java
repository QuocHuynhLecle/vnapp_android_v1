package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.activity.Activity_AlramList;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.vo.VoNewAlramList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 9. 7..
 * 새로운 알림 어댑터
 */
public class NewAlramRecyclerAdapter extends RecyclerView.Adapter<NewAlramRecyclerAdapter.ViewHolder> {

    String TAG=getClass().getName();
    ArrayList<VoNewAlramList.VoNewAlramItem> arvalues= new ArrayList<>();
    Context context;


    public NewAlramRecyclerAdapter(Context con){
        context = con;
    }


    public void setItem(ArrayList<VoNewAlramList.VoNewAlramItem> values, boolean mbRefresh) {

        if(mbRefresh)
            arvalues=new ArrayList<>();

        arvalues.addAll(values);

        Log.e("NewAlramRecyclerAdapter","setItem arvalues "+arvalues.size());
    }

    /**
     * chatId 인 아이템을 삭제한다.
     * @param
     */
    public void deleteItem(String chatId) {
        Log.e(TAG,"deleteItem:" +chatId);
        for (int i = 0; i < arvalues.size(); i++) {
            if(arvalues.get(i).getChatId().equals(chatId)){
                arvalues.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, arvalues.size());
                notifyDataSetChanged();

            }
        }
    }

    /**
     * 모든 아이템을 삭제
     */
    public void deleteAll() {
        //Log.e(TAG,"deleteAll");
        arvalues.clear();
        notifyDataSetChanged();
    }

    /**
     * 리스트의 마지막 cid값을 가져온다.
     * @return
     */
    public String getLastCid() {
        return arvalues.get(arvalues.size()-1).getcId();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;
        public TextView tvTime;
        public TextView tvMessage;

        public LinearLayout llItem;
        public ImageView imgThum;
        public ImageView imgThumIcon;

        public ViewHolder(View v) {

            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            imgThum = (ImageView) v.findViewById(R.id.imgv_profile);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);

            tvMessage = (TextView) v.findViewById(R.id.tv_message);
        }
    }

    @Override
    public NewAlramRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context).inflate(R.layout.recycler_newalram_items_view, parent, false);
        ViewHolder viewHolder1 = new ViewHolder(view1);
        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

        Vholder.tvTitle.setText(arvalues.get(position).getTitle());
        //Vholder.tvTime.setText(arvalues.get(position).getReg_date());

        Util.DateParse(arvalues.get(position).getReg_date(), Vholder.tvTime);


        Vholder.tvMessage.setText(arvalues.get(position).getMessage());

        Util.setMoimProfileGlide2(context, arvalues.get(position).getOwnerId(), Vholder.imgThum);


        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO 화면이동. 어떻게 분기하는지?


                try {
                    JSONObject obj = new JSONObject(arvalues.get(position).getAttachment());

                    JSONObject obj2 = obj.getJSONObject("link_data");
                    String moimId = obj2.getString("moimId");

                    String msgId = null;

                    msgId = obj2.optString("msgId","msgId_null");
                    /*if(obj2.getString("msgId").equals(JSONObject.NULL)){

                    }else{
                        msgId = obj2.getString("msgId");
                    }*/




                    String link_action=obj.getString("link_action");

                    Log.e(TAG,"moimId:"+moimId +" msgId: "+msgId +" link_action: "+link_action);


                    if(link_action.equals("3")){
                        //모임 타임라인으로 이동.
                        intentMoimProfile(moimId);

                    }else if(link_action.equals("4")){
                        //모임 글상세화면으로 이동
                        intentTimeLineDetail(msgId,moimId,false);
                    }

                    Fragment_Main.getInstance().chatroomReadCountSubtraction();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //TODO 확인후 읽은건 삭제해주기
                ((Activity_AlramList)context).chatDeleteRequest(arvalues.get(position).getChatId());
            }
        });

    }

    @Override
    public int getItemCount() {

        if(arvalues!=null)return arvalues.size();
        else
            return 0;
    }

    /**
     * 글상세화면으로 이동.
     * @param msgId
     * @param reply
     */
    private void intentTimeLineDetail(String msgId,String mmid, boolean reply) {
        Log.e(TAG,"intentTimeLineDetail :"+msgId);
        com.wave.messenger.util.SharedObject.setProperty_string(context, Constants.MOIM_ID,mmid);
        com.wave.messenger.util.SharedObject.setProperty_string(context, Constants.MOIM_MSGID, msgId);
        com.wave.messenger.util.SharedObject.setProperty_boolean(context, Constants.MOIM_REPLY, reply);

        Intent intent = new Intent(context, Activity_TimeLineDetail.class);
        context.startActivity(intent);
    }

    /**
     * 모임타임라인으로 이동.
     * @param moimId
     */
    private void intentMoimProfile(String moimId) {
        com.wave.messenger.util.SharedObject.setProperty_string(context, Constants.MOIM_ID,moimId);
        Intent intent = new Intent(context, Activity_MoimTimeLineList.class);
        context.startActivity(intent);
    }
    /**
     * 마지막 읽은 모임 키값
     */
    /*public String getLastMmId() {
        //Log.e(TAG,"loadNextDataFromApi getLastMmId : "+arvalues.get(arvalues.size()-1).getMmId()+" "+arvalues.get(arvalues.size()-1).getMmNm());
        return arvalues.get(arvalues.size()-1).getMmId();
    }*/
}