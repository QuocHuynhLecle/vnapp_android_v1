package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.activity.Activity_ProfessionalList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.Cell_Professional_ListTitle;
import com.wave.messenger.cell.FriendHolder;
import com.wave.messenger.util.Constants;
import com.wave.messenger.vo.VoMyprofessional;

import java.util.ArrayList;

import moa.android.api.MOAData;

import static com.wave.massenger.piggy.R.id.iv_profile;

public class ProfessionalListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> groupList;
    private ArrayList<VoMyprofessional> childList;
    String TAG = getClass().getSimpleName();

    public ProfessionalListViewAdapter(Context context, ArrayList<String> groupList, ArrayList<VoMyprofessional> childList) {
        this.context = context;
        this.groupList = groupList;
        this.childList = childList;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).getProList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childList.get(childPosition);
    }

    public void removeChild(VoMyprofessional removedFriend) {

        for (VoMyprofessional grp : childList) {
            try {
                grp.getProList().remove(removedFriend);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Cell_Professional_ListTitle itemView = new Cell_Professional_ListTitle(context);
        if (groupList.get(groupPosition) != null && getChildrenCount(groupPosition) > 0) {
            if (groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_MANAGER)) {
                itemView.proHeaderVisibility();
            } else {
                itemView.setTextViewTitle(groupList.get(groupPosition) + " (총" + getChildrenCount(groupPosition) + "명)");
            }

        } else {
            if (!groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_MYPROFESSIONAL) &&
                    !groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_MANAGER) &&
                    !groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_ANALYST) &&
                    !groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_PROFESSIONAL) &&
                    !groupList.get(groupPosition).equals(Constants.PROFESSIONAL_LIST_PROFESSIONAL_GROUP)) {
                itemView.proListHeaderMore();
                itemView.setTextViewMoreTitle(groupList.get(groupPosition));
            } else {
                itemView.setTextViewTitle(groupList.get(groupPosition));
            }
        }
        return itemView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final VoMyprofessional item = childList.get(groupPosition).getProList().get(childPosition);

        LayoutInflater mInflater;
        FriendHolder holder;

        if (convertView == null) {
            holder = new FriendHolder();
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.cell_professionllist_detail, null);

            holder.ll_friend = (LinearLayout) convertView.findViewById(R.id.ll_friend);
            holder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);
            holder.iv_profile = (ImageView) convertView.findViewById(iv_profile);
            holder.tv_proname = (TextView) convertView.findViewById(R.id.textViewProName);
//            holder.tv_prorole = (TextView) convertView.findViewById(R.id.textViewProRole);
            holder.iv_proIcon = (ImageView) convertView.findViewById(R.id.iv_proIcon);
            holder.tv_role = (TextView) convertView.findViewById(R.id.textViewRole);
            holder.tv_counseling = (TextView) convertView.findViewById(R.id.textViewCounseling);
            holder.tv_reading = (TextView) convertView.findViewById(R.id.textViewReading);
            holder.tv_counseling2 = (TextView) convertView.findViewById(R.id.textViewCounseling2);
            holder.tv_reading2 = (TextView) convertView.findViewById(R.id.textViewReading2);
//            holder.iv_talk = (ImageView) convertView.findViewById(R.id.iv_talk);
//            holder.iv_join = (ImageView) convertView.findViewById(R.id.iv_join);
//            holder.ll_cell = (LinearLayout) convertView.findViewById(R.id.ll_cell);
            holder.tv_moim_name = (TextView) convertView.findViewById(R.id.tv_moim_name);

            holder.vLiner = convertView.findViewById(R.id.vLiner);
            convertView.setTag(holder);
        } else {
            holder = (FriendHolder) convertView.getTag();
        }
        String url;
        url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();
        Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_pro_default_profile).dontAnimate().into(holder.iv_profile);
        holder.tv_proname.setText(item.getUsNm());
        holder.iv_profile.setClipToOutline(true);
        holder.tv_moim_name.setText(item.getMmNm());
//        if (item.getmStt() == 1) {
////            holder.iv_join.setVisibility(View.INVISIBLE);
//            holder.iv_join.setImageResource(R.drawable.ic_list_minus);
//        } else {
////            holder.iv_join.setVisibility(View.VISIBLE);
//
//            holder.iv_join.setImageResource(R.drawable.ic_list_plus);
//        }

        //전문가 타입구분    0:매니저 1:애널리스트 2:전문가
//        switch (item.getXprtTy()) {
//            case 0:
//                holder.tv_prorole.setText(Constants.PROFESSIONAL_MANAGER);
//                break;
//            case 1:
//                holder.tv_prorole.setText(Constants.PROFESSIONAL_ANALYST);
//                break;
//            case 2:
//                holder.tv_prorole.setText(Constants.PROFESSIONAL_STOCK);
//                break;
//        }

        //모임 타입구분    1:주식 2:국내파생 3:해외파생 4:해외주식 5:금융상품 6:기타
        switch (item.getMmTg()) {
            case 1:
//                holder.iv_proIcon.setImageResource(R.drawable.ic_mini_stock);
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_STOCK);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_STOCK);
                break;
            case 2:
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_DERIVATION);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_DERIVATION);
                break;
            case 3:
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_GLOBALDERIVATION);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_GLOBALDERIVATION);
                break;
            case 4:
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_GLOBALSTOCK);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_GLOBALSTOCK);
                break;
            case 5:
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_BANKPRODUCT);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_BANKPRODUCT);
                break;
            case 6:
                if (item.getBranchName().equals(""))
                    holder.tv_role.setText("￨ " +Constants.PROFESSIONAL_MOIM_ETC);
                else
                    holder.tv_role.setText("￨ " + item.getBranchName() + " ￨ " +Constants.PROFESSIONAL_MOIM_ETC);
                break;
        }
        //종목 상담여부    0:OFF 1:ON
//        switch (item.getCsltTy()) {
//            case 0:
//                holder.tv_counseling.setText("OFF");
//                holder.tv_counseling.setVisibility(View.GONE);
//                holder.tv_counseling2.setVisibility(View.GONE);
//                //holder.tv_counseling.setTextColor(red);
//                break;
//            case 1:
//                holder.tv_counseling.setText("ON");
//                holder.tv_counseling.setTextColor(Color.RED);
//                holder.tv_counseling.setVisibility(View.VISIBLE);
//                holder.tv_counseling2.setVisibility(View.VISIBLE);
//                break;
//        }
        //종목 리딩여부    0:OFF 1:ON
//        switch (item.getLdngTy()) {
//            case 0:
//                holder.tv_reading.setText("OFF");
//                holder.tv_reading.setVisibility(View.GONE);
//                holder.tv_reading2.setVisibility(View.GONE);
//                break;
//            case 1:
//                holder.tv_reading.setText("ON");
//                holder.tv_reading.setTextColor(Color.RED);
//                holder.tv_reading.setVisibility(View.VISIBLE);
//                holder.tv_reading2.setVisibility(View.VISIBLE);
//                holder.tv_reading2.setText("종목리딩 :");
//                break;
//        }


        if (childList.get(groupPosition).getProList().size() == childPosition + 1) {
            holder.vLiner.setVisibility(View.GONE);
        } else {
            holder.vLiner.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;

                String strResult;
                switch (data.ptc) {


                    case 0x00080019:    // 모임 탈퇴
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Toast.makeText(context, "모임 탈퇴 완료", Toast.LENGTH_SHORT).show();

                            ((Activity_ProfessionalList) context).RefreshAdapter();
                        } else {
                            Toast.makeText(context, "모임탈퇴 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });
}
