package com.wave.messenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
/**
 * Created by aveapp on 2017. 9. 1..
 */

@Deprecated
public class JongmokCodeSearchAdapter extends BaseAdapter {
    private Context context;

    /*public JongmokCodeSearchAdapter(Context context, ArrayList<IStructItemCode> iStructItemCodes) {
        this.context = context;
        this.iStructItemCodes = iStructItemCodes;
    }*/

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        JongMokListViewHolder holder;
        if (convertView == null) {
            holder = new JongMokListViewHolder();

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_jongmokcode, parent, false);

            holder.tv_jongmok_name = (TextView) convertView.findViewById(R.id.tv_jongmok_name);
            holder.tv_jongmok_code = (TextView) convertView.findViewById(R.id.tv_jongmok_code);

            convertView.setTag(holder);
        } else {
            holder = (JongMokListViewHolder) convertView.getTag();
        }

        return convertView;
    }

    // ViewHolderItem Class
    private class JongMokListViewHolder {

        public TextView tv_jongmok_name, tv_jongmok_code;

    }

}
