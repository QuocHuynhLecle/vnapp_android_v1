package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.RadiusImageView;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.vo.VoMoimSearch;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 9..
 * 모임 종목 코멘트 찾기
 */
public class MoimRecyclerAdapter extends RecyclerView.Adapter<MoimRecyclerAdapter.ViewHolder> {

    ArrayList<VoMoimSearch.VoMoimSearchItem> arvalues= new ArrayList<>();
    Context context;


    public MoimRecyclerAdapter(Context con){
        context = con;
    }
    /*public MoimRecyclerAdapter(Context con, ArrayList<VoMoimSearch.VoMoimSearchItem> values) {
        arvalues = values;
        context = con;
    }*/

    public void setItem(ArrayList<VoMoimSearch.VoMoimSearchItem> values, boolean mbRefresh) {
        //arvalues = values;

        if(mbRefresh)
            arvalues=new ArrayList<>();

        arvalues.addAll(values);

        Log.e("MoimRecyclerAdapter","setItem arvalues "+arvalues.size());

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public TextView textIntr;

        public TextView tvUsrCnt;
        public TextView tvLeader;

        public LinearLayout llItem;
        public ImageView imgThum;
        public ImageView imgThumIcon;

        public ViewHolder(View v) {

            super(v);
            textView = (TextView) v.findViewById(R.id.tv_moim_title);
            textIntr = (TextView) v.findViewById(R.id.tv_moim_intr);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            imgThum = (RadiusImageView)v.findViewById(R.id.imgv_thum_img);
            imgThumIcon= (ImageView)v.findViewById(R.id.imgv_thum_icon);

            tvUsrCnt = (TextView) v.findViewById(R.id.tv_user_cnt);
            tvLeader = (TextView) v.findViewById(R.id.tv_leader);
        }
    }

    @Override
    public MoimRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context).inflate(R.layout.recycler_moimsearch_items_view, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

        Vholder.textView.setText(arvalues.get(position).getMmNm());

        Vholder.tvUsrCnt.setText(arvalues.get(position).getUsrCnt());
        //Vholder.tvLeader.setText(arvalues.get(position).getRegUsr());  //TODO 이름으로 수정
        Vholder.tvLeader.setText(arvalues.get(position).getUsNm());



        String intr=arvalues.get(position).getIntr();

        if(TextUtils.isEmpty(intr)){
            Vholder.textIntr.setVisibility(View.GONE);
        }else{
            Vholder.textIntr.setVisibility(View.VISIBLE);
            Vholder.textIntr.setText(intr);
        }



        if(arvalues.get(position).getImgTy().equals("0")){

            Vholder.imgThumIcon.setVisibility(View.VISIBLE);
            Vholder.imgThum.setVisibility(View.GONE);

            switch (arvalues.get(position).getMmTg()) {
                case "1":   //1 : 주식
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_time_edu);
                    break;

                case "2":   //2 : 국내파생
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_personal);
                    break;

//                case "3":    //3 : 해외파생
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_derivation);
//                    break;
//
//                case "4":   //4 : 해외주식
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_stock);
//                    break;
//
//                case "5":   //5 : 금융상품
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_bank_product);
//                    break;
//
//                case "6":   //6 : 기타
//                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_etc);
//                    break;
            }

        }else if(arvalues.get(position).getImgTy().equals("1")) {
            Vholder.imgThum.setVisibility(View.VISIBLE);
            Vholder.imgThumIcon.setVisibility(View.GONE);

            Glide.with(context).load(Constants.MOIM_LIST_IMAGE_URL + arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_default_my_profile).dontAnimate().into(Vholder.imgThum);

        }


        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("MoimRecyclerAdapter","onBindViewHolder "+position);
                Intent mIntent = new Intent(context, Activity_MoimTimeLineList.class);
                SharedObject.setProperty_string(context, Constants.MOIM_ID, arvalues.get(position).getMmId());
                context.startActivity(mIntent);
            }
        });


    }

    @Override
    public int getItemCount() {

        if(arvalues!=null)return arvalues.size();
        else
            return 0;
    }

    /**
     * 마지막 읽은 모임 키값
     */
    public String getLastMmId() {
        //Log.e(TAG,"loadNextDataFromApi getLastMmId : "+arvalues.get(arvalues.size()-1).getMmId()+" "+arvalues.get(arvalues.size()-1).getMmNm());
        if (arvalues.size() == 0) {
            return "";
        }
        return arvalues.get(arvalues.size()-1).getMmId();
    }
}