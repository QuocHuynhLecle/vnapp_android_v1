package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_MoimJoin_Ing;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;

import com.wave.messenger.util.RadiusImageView;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by aveapp on 2017. 8. 9..
 */

public class MoimJoinIng_Adapter extends RecyclerView.Adapter<MoimJoinIng_Adapter.ViewHolder> {
    String TAG = MoimJoinIng_Adapter.class.getName();
    ArrayList<VoMoimList> arvalues;
    Context context;

    public MoimJoinIng_Adapter(Context con, ArrayList<VoMoimList> values) {
        this.arvalues = values;
        this.context = con;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public TextView tvDate;
        public RadiusImageView ivProfile;
        public TextView tvJoinCancel;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            ivProfile = (RadiusImageView) itemView.findViewById(R.id.iv_moim);
            tvJoinCancel = (TextView) itemView.findViewById(R.id.tv_join_cancel);


        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_moim_join, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tvTitle.setText(arvalues.get(position).getMmNm());
        holder.tvDate.setText(arvalues.get(position).getRegDt());

        if (arvalues.get(position).getImgTy().equals("0")) {
            switch (arvalues.get(position).getMmTg()) {
                case "1":   //1 : 주
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_stock);
                    break;
                case "2":   //2 : 국내파생
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_derivation);
                    break;
                case "3":    //3 : 해외파생
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_global_derivation);
                    break;

                case "4":   //4 : 해외주식
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_global_stock);
                    break;

                case "5":   //5 : 금융상품
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_bank_product);
                    break;
                case "6":   //6 : 기타
                    holder.ivProfile.setBackgroundResource(R.drawable.ic_mini_etc);
                    break;
                default:
                    break;
            }
        } else if (arvalues.get(position).getImgTy().equals("1")) {

            Glide.with(context).load(Constants.MOIM_LIST_IMAGE_URL + arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_profile_big_default_android).dontAnimate().into(holder.ivProfile);      //TODO 기본이미지 수정

        }

        holder.tvJoinCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedObject.setProperty_string(context, Constants.MOIM_ID,arvalues.get(position).getMmId());
                Moa.moimAllowRequest(context, MessengerInfo.getUserId(context), "0",  mMainActivityHandler);
            }
        });
    }

    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_ALLOW :    //모임 가입 승인/거절
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            try {

                                ((Activity_MoimJoin_Ing)context).moimListRequest();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    @Override
    public int getItemCount() {
        if (arvalues != null) return arvalues.size();
        else
            return 0;
    }
}
