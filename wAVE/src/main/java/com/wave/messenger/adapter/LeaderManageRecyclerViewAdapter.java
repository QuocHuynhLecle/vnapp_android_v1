package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.messenger.activity.Activity_SetLeaderManage;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;
import java.util.Collections;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 23..
 * 공동리더 관리 어댑터
 *
 */


public class LeaderManageRecyclerViewAdapter extends RecyclerView.Adapter<LeaderManageRecyclerViewAdapter.ViewHolder> {


    String TAG=this.getClass().getName();
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arvalues= new ArrayList<>();
    Context mContext;

    int nDeleteLeaderPos;  //삭제할 포지션 서버 응답값 없어서...


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvText;
        private final ImageView imgProfile;
        private final ImageView imgvUsrlv;

        private final Button btnDelete;



        String TAG = "ViewHolderItem";

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });
            tvText = (TextView) v.findViewById(R.id.tv_id);
            imgProfile=(ImageView) v.findViewById(R.id.imgv_profile);
            btnDelete=(Button) v.findViewById(R.id.btn_delete);
            imgvUsrlv=(ImageView) v.findViewById(R.id.imgv_usr_level);
        }

    }




    public void setItem(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        arvalues= new ArrayList<>();

        Collections.sort(params,  new Activity_SetLeaderManage.NameAscCompare());


        for (VoMoimMemberList.VoMoimMemberlistItem item : params) {
            if(!item.getUsrLv().equals("0")){

                if(item.getUsrLv().equals("1")){
                    arvalues.add(0,item);
                }else{
                    arvalues.add(item);
                }

            }



        }

        /*for (VoMoimMemberList.VoMoimMemberlistItem item : params) { //나만 뺀 아이디
            if(!item.getUserId().equals(MessengerInfo.getUserId(context))){
                arvalues.add(item);
            }
        }*/
    }

    public LeaderManageRecyclerViewAdapter(Context activity) {
        mContext = activity;
    }

    @Override
    public LeaderManageRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leader_manage, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    ImageView mImage;

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        //썸네일
        //Util.setGlideProfile(mContext, arvalues.get(position).getUserId(), holder.imgProfile);
        Util.setMoimProfileGlide(mContext, SharedObject.getProperty_string(mContext, Constants.MOIM_ID, null)
                , arvalues.get(position).getPfImg()
                , holder.imgProfile);


        //이름
        holder.tvText.setText(arvalues.get(position).getUsNm());

        //변경
        if(arvalues.get(position).getUsrLv().equals("2")){  //공동리더
            holder.btnDelete.setVisibility(View.VISIBLE);
            holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    nDeleteLeaderPos=position;
                    //제외 팝업
                    showDeleteDialog(arvalues.get(position).getUserId());
                }
            });
        }else{
            holder.btnDelete.setVisibility(View.GONE);
        }


        //리더 표시
        switch (arvalues.get(position).getUsrLv()){

            case "1": //리더
                holder.imgvUsrlv.setBackgroundResource(R.drawable.ic_badge_leader);
                break;
            case "2": //공동리
                holder.imgvUsrlv.setBackgroundResource(R.drawable.ic_badge_public_leader);
                break;
        }



    }


    /**
     * 제외 팝업
     */
    private void showDeleteDialog(final String userId) {
        final Dialog_Text dialog = new Dialog_Text(mContext,mContext.getResources().getString(R.string.leader_remove_dialog_info) , Constants.DIALOG_BUTTON_TYPE.cancle_confirm);
        //dialog.setData(mContext.getResources().getString(R.string.leader_remove_dialog_info));

        dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"onClick"+v.getId());
                //TODO removeAt(v.getId());



                moimUserLeverRequest("0", userId);

                dialog.dismiss();
            }
        });
        /*dialog.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 제외기능
                removeAt(dialog.getmItemPosition());

            }
        });*/

        dialog.show();

    }

    /**
     * 리스트에서 삭제
     * @param position
     */
    public void removeAt(int position) {

        arvalues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arvalues.size());
    }


    @Override
    public int getItemCount() {
        return arvalues.size();
    }

    /**
     * 7.21 모임 멤버레벨 설정
     * <p>
     * usrLv     사용자 레벨(int)     0:일반, 1:리더, 2:공동리더(리더 위임 시에만 1 입력)
     * trgtUsr     설정할 사용자 아이디     여러명일 경우 콤마텍스트로 보냄
     */
    private void moimUserLeverRequest(String usrLv, String trgtUsr) {
        Moa.moimUserLeverRequest(mContext, usrLv, trgtUsr, mMainActivityHandler);
    }


    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_USER_LEVEL:   //7.21 모임 멤버레벨 설정
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            removeAt(nDeleteLeaderPos);
                            //TODO 공동리더에서 제외 처리


                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


}



