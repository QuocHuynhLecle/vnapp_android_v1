package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Gallery.GalleryDataModel;
import com.wave.messenger.utility.ErrorController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 2016-09-13.
 */
public class GalleryDetailAdapter extends RecyclerView.Adapter<GalleryDetailAdapter.GalleryDetailViewHolder> {

    private List<GalleryDataModel> data = Collections.emptyList();
    private Context context;
    private OnDetailClickListener listener;

    private List<LoadThumbnailAsync> asyncList = new ArrayList<>();

    //선택된 포지션을 저장해둔다.
    private int selectedPosition = -1;


    public GalleryDetailAdapter(List<GalleryDataModel> data, Context context, OnDetailClickListener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public GalleryDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_album, parent, false);
        return new GalleryDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GalleryDetailViewHolder holder, final int position) {
        try {
            if (data != null && data.get(position) != null) {
                String path = data.get(position).getFullPath();

                if (!path.contains("file://")) {
                    path = "file://" + path;
                    ErrorController.showMessage("[GalleryDetail2] path : " + path);
                }

                Glide.with(context).load(path).placeholder(R.drawable.img_picture_default_android).dontAnimate().into(holder.ivPicture);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewDetachedFromWindow(GalleryDetailViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GalleryDetailViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivPicture, ivLayout;

        public GalleryDetailViewHolder(View itemView) {
            super(itemView);
            ivPicture = (ImageView) itemView.findViewById(R.id.ivPicture);
            ivLayout = (ImageView) itemView.findViewById(R.id.ivLayout);
            ivLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //선택된 아이템이 있는지 체크
                    if (selectedPosition != -1) {
                        //선택된 아이템이 있다면 클릭이 되면 안된다. 단, 이미 선택된 것은 취소 가능해야한다.
                        if(selectedPosition == getAdapterPosition()) {
                            selectedPosition = -1;
                            ivLayout.setSelected(false);
                            listener.onClickListItem(data.get(getAdapterPosition()), getAdapterPosition(), false);

                        }else{
                            //이미 선택된게 있고, 선택에 대한 해제도 아닐 경우...
                            listener.onAttemptNewSelection();
                        }
                        return;
                    }

                    if (!ivLayout.isSelected()) {
                        selectedPosition = getAdapterPosition();
                        ivLayout.setSelected(true);
                        listener.onClickListItem(data.get(getAdapterPosition()), getAdapterPosition(), true);
                    }
                }
            });
        }
    }


    /**
     * isSelected - 사진이 선택 상태인지, 아니면 선택이 해제되었는지 알려줌.
     */
    public interface OnDetailClickListener {
        void onClickListItem(GalleryDataModel item, int position, boolean isSelected);
        void onAttemptNewSelection();
    }


    private class LoadThumbnailAsync extends AsyncTask<Void, Void, Bitmap> {

        private int position;
        private GalleryDetailViewHolder holder;
        private boolean isCanceled = false;

        public LoadThumbnailAsync(int position, GalleryDetailViewHolder holder) {
            this.position = position;
            this.holder = holder;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            BitmapFactory.Options op = new BitmapFactory.Options();
            op.inSampleSize = 8;
            Bitmap bm = BitmapFactory.decodeFile(data.get(position).getFullPath(), op);
            Bitmap mThumbnail = ThumbnailUtils.extractThumbnail(bm, 300, 300);
            return mThumbnail;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (!isCanceled) {
                holder.ivPicture.setImageBitmap(bitmap);
                synchronized (asyncList) {
                    asyncList.remove(this);
                }
            }
        }

        public void cancel() {
            isCanceled = true;
            synchronized (asyncList) {
                asyncList.remove(this);
            }
        }
    }
}
