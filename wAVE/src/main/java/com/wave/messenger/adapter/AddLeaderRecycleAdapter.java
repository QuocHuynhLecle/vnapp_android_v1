package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;


public class AddLeaderRecycleAdapter extends RecyclerView.Adapter<AddLeaderRecycleAdapter.ViewHolder> {

    String TAG=this.getClass().getName();
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arvalues= new ArrayList<>();
    Context context;
    String sType;

    /*public ApplicantRecyclerAdapter(Context con, ArrayList<VoMoimMember> values) {
        arvalues = values;
        context = con;
    }*/

    public AddLeaderRecycleAdapter(Context con, String sType) {
        context = con;
        this.sType=sType;
    }


    public void setItem(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        arvalues= new ArrayList<>();

        for (VoMoimMemberList.VoMoimMemberlistItem item : params) { //나만 뺀 아이디
                if(!item.getUserId().equals(MessengerInfo.getUserId(context)) && item.getUsrLv().equals("0")){
                    arvalues.add(item);
                }
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public ImageView imgvProfile;
//        public TextView tvApply;
        public LinearLayout llItem;
        public CheckBox checkBox;

        public ViewHolder(View v) {

            super(v);
            /*tvName = (TextView) v.findViewById(R.id.tv_name);
            tvDate= (TextView) v.findViewById(R.id.tv_date);
            imgvProfile= (ImageView) v.findViewById(R.id.imgv_profile);
            tvApply = (TextView) v.findViewById(R.id.tv_apply);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);*/

            tvName = (TextView) v.findViewById(R.id.tv_name);
            imgvProfile= (ImageView) v.findViewById(R.id.imgv_profile);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            checkBox = (CheckBox) v.findViewById(R.id.checkBox);

        }
    }

    @Override
    public AddLeaderRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_addleader, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        arvalues.get(position).setnNum(position);

        holder.tvName.setText(arvalues.get(position).getUsNm());
        Util.setGlideProfile(context, arvalues.get(position).getUserId(), holder.imgvProfile);

        //Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
        //arvalues.get(position).isSelected()

        holder.checkBox.setChecked(arvalues.get(position).isSelected());
        holder.checkBox.setTag(position);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sType.equals(Constants.ADD_LEADER)){
                    //CheckBox cb = (CheckBox) v;
                    int position = (Integer) v.getTag();
                    //Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
                    //FriendBean.setSelected(cb.isChecked());
                    arvalues.get(position).setSelected(holder.checkBox.isChecked());
                    notifyDataSetChanged();

                    //mSelectionDialog.checkForSelectAllCb(mFriendBeanList);
                }else if(sType.equals(Constants.MANDATE_LEADER)){   //리더위임
                    deselectAllitem();

                    CheckBox cb = (CheckBox) v;
                    int position = (Integer) v.getTag();
                    //Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
                    //FriendBean.setSelected(cb.isChecked());
                    //arvalues.get(position).setSelected(cb.isChecked());
                    arvalues.get(position).setSelected(holder.checkBox.isChecked());
                    notifyDataSetChanged();

                }


            }


        });

    }


    /**
     * 체크 모두 false로
     */
    private void deselectAllitem() {
        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if (item.isSelected()) {
                item.setSelected(false);
            }
        }
    }




    /**
     * 신청자  다이얼로그
     * @param
     * @param
     */
    /*private void showApplyDialog(final VoMoimMemberList.VoMoimMemberlistItem voMoimMemberList, int position) {
        final Dialog_MemberApply mDialog = new Dialog_MemberApply(context, voMoimMemberList, position);
        mDialog.show();

        mDialog.setRejectListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 가입거절 처리

                Log.e("showApplyDialog" , v.getId()+"");

                moimAllowRequest(voMoimMemberList.getUserId(),"0");
                //removeAt(v.getId());
                mDialog.dismiss();
            }
        });

        mDialog.setAcceptListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("showApplyDialog" , v.getId()+"");

                //TODO 가입수락 처리
                moimAllowRequest(voMoimMemberList.getUserId(),"1");

                //removeAt(v.getId());
                mDialog.dismiss();
            }
        });
    }*/


    @Override
    public int getItemCount() {

        return arvalues.size();
    }

    public void removeAt(int position) {

        arvalues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arvalues.size());
    }





    /**
     * 선택된 공동리더 리턴
     * @return
     */
    public String getSelectedList(){
        String memberId = "";

        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if (item.isSelected()) {
                if(TextUtils.isEmpty(memberId)){
                    memberId=item.getUserId();
                }else{
                    memberId=memberId+","+item.getUserId();
                }

            }
        }

        return memberId;
    }


    /**
     * 선택된 리더위임 리턴
     * @return
     */
    public String  getSelectedLeaderItem() {

        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if (item.isSelected()) {
                return item.getUserId();
            }
        }

        return null;
    }
}






//public final class AddLeaderRecycleAdapter extends FilterRecycleAdapter<Item_LeaderProfile> {
/*public final class AddLeaderRecycleAdapter extends FilterRecycleAdapter<Item_LeaderProfile> {

    private ArrayList<Item_LeaderProfile> mFriendBeanList;
    private Context mContext;
    private Item_LeaderProfile Selectedleder;
    private String sType;

    public AddLeaderRecycleAdapter(Activity context, ArrayList<Item_LeaderProfile> friendBeanList, String sType) {
        super(friendBeanList);
        this.mFriendBeanList = friendBeanList;
        this.mContext = context;
        this.sType=sType;
    }




    public ArrayList<Item_LeaderProfile> getFriendBeanList() {
        return mFriendBeanList;
    }


    *//**
     * 선택된 리더위임 리턴
     * @return
     *//*
    public Item_LeaderProfile getSelectedLeaderList() {

        ArrayList<Item_LeaderProfile> arSelectedFriend= new ArrayList<>();

        for (Item_LeaderProfile FriendBean : mFriendBeanList) {
            if (FriendBean.isSelected()) {
                return FriendBean;

            }
        }
        return null;
    }


    *//**
     *선택된 공동리더 리턴
     *//*
    public ArrayList<Item_LeaderProfile> getSelectedList(){

        ArrayList<Item_LeaderProfile> arSelectedFriend= new ArrayList<>();

            for (Item_LeaderProfile FriendBean : mFriendBeanList) {
                if (FriendBean.isSelected()) {
                    arSelectedFriend.add(FriendBean);

                }
            }
            //mSelectAllCb.setChecked(isSelectAll);
        return arSelectedFriend;


    }

    @Override
    public RecyclerView.ViewHolderItem onCreateViewHolder(ViewGroup viewGroup, int i) {
        //View convertView = parent.inflate(R.layout.listrow_friend, viewGroup, false);
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_addleader, viewGroup, false);
        return new ViewHolderItem(v);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolderItem viewHolder, int position) {

        ViewHolderItem holder = (ViewHolderItem) viewHolder;
        Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
        holder.FriendBeanTv.setText(FriendBean.getFriend());
        holder.checkBox.setChecked(FriendBean.isSelected());
        holder.checkBox.setTag(position);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sType.equals(Constants.ADD_LEADER)){
                    CheckBox cb = (CheckBox) v;
                    int position = (Integer) v.getTag();
                    Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
                    FriendBean.setSelected(cb.isChecked());
                    notifyDataSetChanged();

                    //mSelectionDialog.checkForSelectAllCb(mFriendBeanList);
                }else if(sType.equals(Constants.MANDATE_LEADER)){   //리더위임
                    deselectAllitem();

                    CheckBox cb = (CheckBox) v;
                    int position = (Integer) v.getTag();
                    Item_LeaderProfile FriendBean = (Item_LeaderProfile) getItem(position);
                    FriendBean.setSelected(cb.isChecked());
                    notifyDataSetChanged();

                }


            }


        });
    }




/*
    private class ViewHolderItem extends RecyclerView.ViewHolderItem {
        public TextView FriendBeanTv;
        public CheckBox checkBox;

        public ViewHolderItem(View itemView) {
            super(itemView);
            FriendBeanTv = (TextView) itemView.findViewById(R.id.tv_friend_name);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
        }
    }*/

