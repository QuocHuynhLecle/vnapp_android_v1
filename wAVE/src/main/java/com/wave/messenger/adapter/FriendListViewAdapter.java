package com.wave.messenger.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.cell.Cell_Friend_ListTitle;
import com.wave.messenger.cell.FriendHolder;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.dialog.Dialog_MoimJoinType;
import com.wave.messenger.fragment.Fragment_Main;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.mvp.Main.MainPresenter;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;
import com.wave.messenger.vo.VoMyprofessional;

import java.util.ArrayList;
import java.util.List;

import moa.android.api.MOAData;
import moa.android.api.protocol.PacketTypes;

import static com.wave.massenger.piggy.R.id.iv_profile;

public class FriendListViewAdapter extends BaseExpandableListAdapter {

    private static final int CHILD_TYPE_1 = 0;
    private static final int CHILD_TYPE_2 = 1;
    private Context context;
    private ArrayList<String> groupList;
    private ArrayList<VoFriendList> childList;
    private ArrayList<VoMyprofessional> proList;
    private MainPresenter presenter;
    private boolean isInvite;
    private String TAG = getClass().getSimpleName();
    private int customerListCount = 0;

    public FriendListViewAdapter(Context context, ArrayList<String> groupList, ArrayList<VoFriendList> childList, MainPresenter presenter, boolean flag) {
        this.context = context;
        this.groupList = groupList;
        this.childList = childList;
        this.presenter = presenter;
        this.isInvite = flag;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).getList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childList.get(groupPosition).getList().get(childPosition);
    }

    public ArrayList<VoFriendList> getGroupChildList(int groupPosition) {
        ArrayList<VoFriendList> result = childList.get(groupPosition).getList();
        return result;
    }

    public void removeChild(VoFriendList removedFriend) {

        for (VoFriendList grp : childList) {
            try {
                grp.getList().remove(removedFriend);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getChildTypeCount() {
        return 2;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
        int result = 0;
        if (MessengerInfo.getUserType().equals("UT_ST")) {
            if (groupPosition == 2 || groupPosition == 3) {
                if (!item.isPro) {
                    result = CHILD_TYPE_1;
                } else {
                    result = CHILD_TYPE_2;
                }
            }
        } else {
            if (groupPosition == 1 || groupPosition == 2) {
                if (!item.isPro) {
                    result = CHILD_TYPE_1;
                } else {
                    result = CHILD_TYPE_2;
                }
            }
        }
        return result;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Cell_Friend_ListTitle itemView;
        itemView = new Cell_Friend_ListTitle(context);
        if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYCUSTOMER) || groupList.get(groupPosition).equals(Constants.LISTTYPE_MYMANAGER)) {
//            if (getChildrenCount(groupPosition) > 0) {
//                itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")");
//                itemView.setLayoutColor(R.color.mypro_title, R.color.white);
//            } else {
//                itemView.setTextViewTitle(groupList.get(groupPosition));
//                itemView.setLayoutColor(R.color.mypro_title, R.color.white);
//            }
//            itemView.setGroupImage(Constants.LISTTYPE_MYCUSTOMER);
            itemView.hideWholeGroupLayout();
        }
//        else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_CUSTOMER)) {
        if (groupList.get(groupPosition).equals(Constants.LISTTYPE_CUSTOMER)) {
            if (customerListCount > 0) {
                itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + customerListCount + ")");
                itemView.setTextViewMore("더보기");
                itemView.setLayoutColor(R.color.pro_title, R.color.white);
            } else {
                itemView.setTextViewTitle(groupList.get(groupPosition));
                itemView.hideMoreLayout();
                itemView.setLayoutColor(R.color.pro_title, R.color.white);
            }
            itemView.setGroupImage(Constants.LISTTYPE_MYCUSTOMER);
        } else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYPROFILE)) {
            itemView.setTextViewTitle(groupList.get(groupPosition));
        } else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYPROFESSIONALFRIEND)) {
            itemView.setLayoutColor(R.color.pro_title, R.color.white);
            itemView.setTextViewTitle(groupList.get(groupPosition));
            itemView.setGroupImage(Constants.LISTTYPE_MYPROFESSIONALFRIEND);
        } else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_PROFESSIONALFRIEND)) {
            itemView.setLayoutColor(R.color.mypro_title, R.color.white);
            itemView.setTextViewTitle(MessengerInfo.getUserName(context) + groupList.get(groupPosition));
            itemView.setGroupImage(Constants.LISTTYPE_PROFESSIONALFRIEND);
        } else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYFRIEND)) {
            if (getChildrenCount(groupPosition) > 0) {
                itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")");
                itemView.setLayoutColor(R.color.mypro_title, R.color.white);
            } else {
                itemView.setTextViewTitle(groupList.get(groupPosition));
                itemView.setLayoutColor(R.color.mypro_title, R.color.white);
            }
            itemView.setGroupImage(Constants.LISTTYPE_MYFRIEND);
        } else {
            if (getChildrenCount(groupPosition) > 0) {
                itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")");
                itemView.setLayoutColor(R.color.pro_title, R.color.white);
            } else {
                itemView.setTextViewTitle(groupList.get(groupPosition));
                itemView.setLayoutColor(R.color.pro_title, R.color.white);
            }
            itemView.setGroupImage(Constants.LISTTYPE_MYFRIEND);
        }


        if (!isInvite) {
            itemView.setTextViewSize();
        }

        switch (groupList.get(groupPosition)) {
//            case Constants.LISTTYPE_MYMANAGER:
//                itemView.hideMoreLayout();
//                itemView.hideGroupLayout();
//                break;
//            case Constants.LISTTYPE_MYCUSTOMER:
//                itemView.hideMoreLayout();
//                if (getChildrenCount(groupPosition) <= 0) {
//                    itemView.hideGroupLayout();
//                }
//                break;
            case Constants.LISTTYPE_CUSTOMER:
                itemView.hideGroupLayout();
                break;
            case Constants.LISTTYPE_MYPROFESSIONALFRIEND:
                itemView.hideMoreLayout();
                itemView.hideGroupLayout();
                break;
            case Constants.LISTTYPE_PROFESSIONALFRIEND:
                itemView.hideGroupLayout();
                break;
            case Constants.LISTTYPE_MYFRIEND:
                itemView.hideMoreLayout();
                itemView.hideGroupLayout();
                break;
            default:
                if (getChildrenCount(groupPosition) > 0)
                    itemView.hideMoreLayout();
                else {
                    itemView.hideMoreLayout();
                    itemView.hideGroupLayout();
                }
        }

        /** ActivityInviteFriend에서 호출 되었을 경우*/
        if (isInvite) {
            if (groupList.get(groupPosition).equals(Constants.LISTTYPE_PROFESSIONALFRIEND) && groupList.get(groupPosition).equals(Constants.LISTTYPE_MYPROFESSIONALFRIEND)) {
                itemView.hideLayout();
            }
            if (childList.get(groupPosition).getList().size() == 0) {
                itemView.hideLayout();
            }
        }

        return itemView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
        LayoutInflater mInflater;
        FriendHolder holder;

        int childType = getChildType(groupPosition, childPosition);

        if (convertView == null) {
            holder = new FriendHolder();
            switch (childType) {
                case CHILD_TYPE_1:
                    mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = mInflater.inflate(R.layout.cell_friendlist, null);

                    holder.ll_friend = (LinearLayout) convertView.findViewById(R.id.ll_friend);
                    holder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);
                    holder.iv_profile = (CircleImageView) convertView.findViewById(R.id.imageViewTitle);
                    holder.tv_name = (TextView) convertView.findViewById(R.id.textViewName);
                    holder.vLiner = convertView.findViewById(R.id.vLiner);

                    if (!isInvite) {
                        holder.tv_name.setTextSize(16);
                    } else {
                        holder.tv_name.setTextSize(15);
                    }
                    holder.ll_cirecleView = (LinearLayout) convertView.findViewById(R.id.ll_circleImageView);
                    break;
                case CHILD_TYPE_2:
                    mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = mInflater.inflate(R.layout.cell_professionllist, null);

                    holder.ll_friend = (LinearLayout) convertView.findViewById(R.id.ll_friend);
                    holder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);
                    holder.iv_profile = (ImageView) convertView.findViewById(iv_profile);
                    holder.tv_proname = (TextView) convertView.findViewById(R.id.textViewProName);
//                    holder.tv_prorole = (TextView) convertView.findViewById(R.id.textViewProRole);
//                    holder.iv_proIcon = (ImageView) convertView.findViewById(R.id.iv_proIcon);
//                    holder.tv_role = (TextView) convertView.findViewById(R.id.textViewRole);
                    holder.tv_counseling = (TextView) convertView.findViewById(R.id.textViewCounseling);
                    holder.tv_reading = (TextView) convertView.findViewById(R.id.textViewReading);
                    holder.tv_counseling2 = (TextView) convertView.findViewById(R.id.textViewCounseling2);
                    holder.tv_reading2 = (TextView) convertView.findViewById(R.id.textViewReading2);
                    holder.iv_audio = (ImageView) convertView.findViewById(R.id.iv_audio);
                    holder.tv_moim_name = (TextView) convertView.findViewById(R.id.tv_moim_name);

                    holder.vLiner = convertView.findViewById(R.id.vLiner);

                    if (!isInvite) {
                        holder.tv_proname.setTextSize(13);
                        holder.tv_counseling.setTextSize(13);
                        holder.tv_counseling2.setTextSize(13);
                        holder.tv_reading.setTextSize(13);
                        holder.tv_reading2.setTextSize(13);
                        holder.tv_moim_name.setTextSize(15);
                    }
                    holder.ll_cirecleView = (LinearLayout) convertView.findViewById(R.id.ll_circleImageView);
                    break;
            }
            convertView.setTag(holder);
        } else {
            holder = (FriendHolder) convertView.getTag();
        }
        String url;

        switch (childType) {
            case CHILD_TYPE_1:

                url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();
                if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYPROFILE))
                    Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_my_profile).dontAnimate().into(holder.iv_profile);
                else if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYMANAGER))
                    Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_manager_profile).dontAnimate().into(holder.iv_profile);
                else
                    Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_profile);
                holder.tv_name.setText(item.getUserName());

                if (childList.get(groupPosition).getList().size() == childPosition + 1) {
                    holder.vLiner.setVisibility(View.GONE);
                } else {
                    holder.vLiner.setVisibility(View.VISIBLE);
                }

                if (childList.get(groupPosition).getList().size() == childPosition + 1) {
                    //holder.ll_line.setVisibility(View.GONE);
                } else {
                    //holder.ll_line.setVisibility(View.VISIBLE);
                }

                if (isInvite) {
                    if (item.isSelected()) {
                        //holder.iv_checkbox.setSelected(true);
                    } else {
                        //holder.iv_checkbox.setSelected(false);
                    }

                } else {
                    //holder.iv_checkbox.setVisibility(View.GONE);
                }

                if (groupList.get(groupPosition).equals(Constants.LISTTYPE_NEWFRIEND)) {
                    //holder.ll_friend.setBackgroundColor(context.getResources().getColor(R.color.bg_selfriend));
                } else {
                    //holder.ll_friend.setBackgroundColor(context.getResources().getColor(R.color.bg_friend));
                }

                holder.ll_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChatListDbHelper listDb = LocalDB.getChatListDbHelper(context);
                        List<VoFriendList> friends = new ArrayList<>();
                        VoChatList chatRoomInfo = new VoChatList();
                        String roomId;

                        if (item.getUserId().equals(MessengerInfo.getUserId(context))) {
                            roomId = listDb.selectMyRoom();
                            chatRoomInfo.setRoom_type("4");
                            friends.add(item);
                        } else {
                            UsersDbHelper userDb = LocalDB.getUsersDbHelper(context);
                            ArrayList<String> roomIdList = listDb.selectSingleRoom();
                            roomId = userDb.existUserCaht(roomIdList, item.getUserId());
                            chatRoomInfo.setRoom_type("0");
                            chatRoomInfo.setRoom_name(item.getUserName());
                            VoFriendList my = new VoFriendList();
                            my.setUserId(MessengerInfo.getUserId(context));
                            my.setUserName(MessengerInfo.getUserName(context));
                            my.setRealUserName(MessengerInfo.getRealUserName(context));
                            friends.add(my);
                            friends.add(item);
                        }

                        chatRoomInfo.setFriends(friends, context);

                        if (!TextUtils.isEmpty(roomId)) {
                            chatRoomInfo.setRoomId(roomId);
                        }
                        Statics.ROOMINFO = chatRoomInfo;

                        BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

                        Intent mIntent = new Intent(context, Activity_ChatRoom.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(mIntent);
                    }
                });

                holder.ll_item.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return false;
                    }
                });
                break;
            case CHILD_TYPE_2:
                url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();
                Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_master_default_profile).dontAnimate().into(holder.iv_profile);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.iv_profile.setClipToOutline(true);
                }
                if (item.getBranchName().equals(""))
                    holder.tv_proname.setText(item.getUsNm());
                else
                    holder.tv_proname.setText(item.getUsNm() + " ￨ " + item.getBranchName());

//                if (item.getmStt() == 1) {
////            holder.iv_join.setVisibility(View.INVISIBLE);
//                    holder.iv_join.setImageResource(R.drawable.ic_list_minus);
//                } else {
////            holder.iv_join.setVisibility(View.VISIBLE);
//                    holder.iv_join.setImageResource(R.drawable.ic_list_plus);
//                }

//                holder.iv_join.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (item.getmStt() == 1) {
//                            Moa.moimExitProfessional(context, mMainActivityHandler, item.getMmId());
//
//                        } else {
//                            showDialogJoin(item);
//                        }
//                    }
//                });

//                if(item.getmStt() == 1){
//                    holder.iv_join.setVisibility(View.INVISIBLE);
//                }else {
//                    holder.iv_join.setVisibility(View.VISIBLE);
//                }

//                switch (item.getXprtTy()) {
//                    case 0:
//                        holder.tv_prorole.setText(Constants.PROFESSIONAL_MANAGER);
//                        break;
//                    case 1:
//                        holder.tv_prorole.setText(Constants.PROFESSIONAL_ANALYST);
//                        break;
//                    case 2:
//                        holder.tv_prorole.setText(Constants.PROFESSIONAL_STOCK);
//                        break;
//                }

//                switch (item.getMmTg()) {
//                    case 1:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_stock);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_STOCK);
//                        break;
//                    case 2:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_derivation);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_DERIVATION);
//                        break;
//                    case 3:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_global_derivation);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_DERIVATION);
//                        break;
//                    case 4:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_global_stock);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_GLOBALDERIVATION);
//                        break;
//                    case 5:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_bank_product);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_BANKPRODUCT);
//                        break;
//                    case 6:
////                        holder.iv_proIcon.setImageResource(R.drawable.ic_mini_etc);
//                        holder.tv_role.setText(Constants.PROFESSIONAL_MOIM_ETC);
//                        break;
//                }

                switch (item.getCsltTy()) {
                    case 0:
                        holder.tv_counseling.setText("OFF");
                        holder.tv_counseling.setVisibility(View.GONE);
                        holder.tv_counseling2.setVisibility(View.GONE);
                        break;
                    case 1:
                        holder.tv_counseling.setText("ON");
                        holder.tv_counseling.setTextColor(Color.RED);
                        holder.tv_counseling.setVisibility(View.VISIBLE);
                        holder.tv_counseling2.setVisibility(View.VISIBLE);
                        break;
                }

                switch (item.getLdngTy()) {
                    case 0:
                        holder.tv_reading.setText("OFF");
                        holder.tv_reading.setVisibility(View.GONE);
                        holder.tv_reading2.setVisibility(View.GONE);
                        break;
                    case 1:
                        holder.tv_reading.setText("ON");
                        holder.tv_reading.setTextColor(Color.RED);
                        holder.tv_reading.setVisibility(View.VISIBLE);
                        holder.tv_reading2.setVisibility(View.VISIBLE);
                        holder.tv_reading2.setText("종목리딩 :");
                        break;
                }

                holder.tv_moim_name.setText(item.getMmNm());

                //방송아이콘
                switch (item.getVoice()) {
                    case "0":
                        holder.iv_audio.setVisibility(View.GONE);
                        break;
                    case "1":
                        holder.iv_audio.setVisibility(View.VISIBLE);
                        break;
                }

                if (childList.get(groupPosition).getList().size() == childPosition + 1) {
                    holder.vLiner.setVisibility(View.GONE);
                } else {
                    holder.vLiner.setVisibility(View.VISIBLE);
                }
                break;
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * 모임 가입하기 다이얼로그
     * 현재 질문가입형
     * <p>
     * 0:승인, 1:자동가입(기본), 2:비밀번호
     */
    private void showDialogJoin(final VoFriendList param) {
        switch (param.getEntTy()) {
            case 1:  //팝업 필요없이 바로 가입
                Moa.moimApplyProfessional(context, "", "", mMainActivityHandler, param.getMmId());
                Toast.makeText(context, "가입 되었습니다.", Toast.LENGTH_SHORT).show();
                break;
            case 0:  //승인 질문입력 팝업
            case 2: //비밀번호 파법
                //TODO
                final Dialog_MoimJoinType dialog_MakeMoim = new Dialog_MoimJoinType(context, param); //param.getQstn() getEntTy
                dialog_MakeMoim.setListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int id = view.getId();

                        if (id == R.id.tv_cancle) {
                            dialog_MakeMoim.dismiss();
                        } else if (id == R.id.tv_confirm) {
                            dialog_MakeMoim.getJoinTxt();   //비밀번호

                            String answ = null;   //모임 가입질문
                            String mmPw = null;  //모임 가입비밀번호

                            if (param.getEntTy() == 0) { //리더승인이면
                                answ = dialog_MakeMoim.getJoinTxt();   //가입질문

                            } else if (param.getEntTy() == 2) {
                                mmPw = dialog_MakeMoim.getJoinTxt();   //비밀번호
                            }

                            Util.hideKeyboard(context, ((EditText) dialog_MakeMoim.findViewById(R.id.edt_input)));
                            dialog_MakeMoim.dismiss();

                            Moa.moimApplyProfessional(context, answ, mmPw, mMainActivityHandler, param.getMmId());

                        }

                    }
                });
                dialog_MakeMoim.show();
                break;
        }
    }

    public AlertDialog.Builder getAlert() {
        AlertDialog.Builder alert;
        alert = new AlertDialog.Builder(context);
        return alert;
    }

    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                MOAData data = (MOAData) msg.obj;

                String strResult;
                switch (data.ptc) {
                    case PacketTypes.PTC_IMS_MOIM_APPLY:    //모임 가입
                        strResult = (String) data.body.get("result");
                        Log.e("TEST", "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Fragment_Main.getInstance().FriendListRequest();

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("apply")) {
                                Toast.makeText(context, "가입신청을 완료하였습니다.", Toast.LENGTH_SHORT).show();
                                Fragment_Main.getInstance().FriendListRequest();
                            } else {
                                //완료 표시
                            }

                        } else {

                            String reason = (String) data.body.get("reason");
                            if (reason.equals("already_applied")) {
                                Toast.makeText(context, "이미 가입신청함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_exist")) {
                                Toast.makeText(context, "이미 가입함", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("user_blocked")) {
                                Toast.makeText(context, "차단됨", Toast.LENGTH_SHORT).show();
                            } else if (reason.equals("wrong_moim_password")) {
                                Toast.makeText(context, "비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                            }

                        }
                        break;

                    case 0x00080019:    // 모임 탈퇴
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Toast.makeText(context, "모임 탈퇴 완료", Toast.LENGTH_SHORT).show();

                            Fragment_Main.getInstance().FriendListRequest();
                        } else {
                            Toast.makeText(context, "모임탈퇴 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

}
