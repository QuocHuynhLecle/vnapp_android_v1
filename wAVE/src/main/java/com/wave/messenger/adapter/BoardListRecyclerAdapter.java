package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.activity.Activity_MoimContentReport;
import com.wave.messenger.activity.Activity_MoimProfile;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_MoimWrite;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Report;
import com.wave.messenger.dialog.Dialog_Text;
import com.wave.messenger.dialog.Dialog_TimeLineOption_2;
import com.wave.messenger.dialog.Dialog_share;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.EllipsizeTextView;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardList;
import com.wave.messenger.vo.VoMsgItem;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 10..
 * 앱첫화면의 타임라인 어댑터
 * , 모임 타임라인 어댑터
 * , 내가쓴글 리스트 타임라인 어댑터
 * 같이사용.
 * <p>
 * 7.15 모임 글 리스트
 * 7.27 모임 타임라인
 * 같이사용
 */
public class BoardListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    String TAG = this.getClass().getSimpleName();

    ArrayList<VoBoardList.VoBoardItem> arvalues = new ArrayList<>();
    ArrayList<VoBoardList.VoBoardItemReply> arReply = new ArrayList<>();   //글msgNo   댓글 prntNo

    Constants.SET_ENTRY_TYPE SET_ENTRY_TYPE;
    Context mContext;
    ImageView mImage;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    public BoardListRecyclerAdapter(Context con, Constants.SET_ENTRY_TYPE main_tab) {
        mContext = con;
        SET_ENTRY_TYPE = main_tab;
    }

    /**
     * @param values
     * @param deleteList 리스트를 초기화하고 다시 붙인다
     */
    public void setItem(VoBoardList values, boolean deleteList) {

        //Log.e(TAG, "mSwipeRefreshLayout setItem : " + deleteList);
        if (deleteList) {
            arvalues = new ArrayList<>(values.getParams());
            arReply = new ArrayList<>(values.getRpyLst());
        } else {

            arvalues.addAll(values.getParams());
            arReply.addAll(values.getRpyLst());
        }


        //메인 타임라인에서는 댓글 숨기기
        if (mContext instanceof Activity_MoimTimeLineList) {
            // 최신댓글2개 붙이기
            // 글리스트의  getMsgNo 값과 댓글리스트의 PrntNo값 맞는지 확인후  글리스트로 붙인다
            ArrayList<VoBoardList.VoBoardItemReply> alRep;

            for (int i = 0; i < arvalues.size(); i++) {
                VoBoardList.VoBoardItem item = arvalues.get(i);
                alRep = new ArrayList<>();

                for (int y = 0; y < arReply.size(); y++) {

                    if (item.getMsgNo().equals(arReply.get(y).getPrntNo())) {
                        //Log.e(TAG, "item MsgNo: " + item.getMsgNo() + " itemReply PrntNo:" + arReply.get(y).getPrntNo());
                        alRep.add(arReply.get(y));
                    }
                }
                arvalues.get(i).setAlReply(alRep);
            }
        }


    }

    /**
     * 리스트의 공지상태 값을 변경한다
     *
     * @param msgId
     * @param noti
     */
    public void setNotiDate(String msgId, String noti) {
        //Log.e("BoardListRecycler","setNotiDate: "+msgId+" "+noti);

        for (int i = 0; i < arvalues.size(); i++) {
            if (arvalues.get(i).getMsgId().equals(msgId)) {
                arvalues.get(i).setNoti(noti);

                if (mContext instanceof Activity_MoimTimeLineList) {
                    ((Activity_MoimTimeLineList) mContext).reFreshAdapter();
                    //상단 헤더 부분을 새로고침한다.
                    ((Activity_MoimTimeLineList) mContext).bMoimProfile = true;
                    ((Activity_MoimTimeLineList) mContext).MoimProfileRequest();


                }
                break;
            }
        }
    }

    /**
     * 마지막 읽은 모임 키값
     */
    public String getLastMmId() {
        //Log.e(TAG,"loadNextDataFromApi getLastMmId : "+arvalues.get(arvalues.size()-1).getMmId()+" "+arvalues.get(arvalues.size()-1).getMmNm());
        if (arvalues.size() > 0) {
            return arvalues.get(arvalues.size() - 1).getMmId();
        } else {
            return "";
        }
    }

    /**
     * 마지막 읽은 메시지 키값
     */
    public String getMsgId() {

        //Log.e(TAG,"loadNextDataFromApi getMsgId : arvalues.size() " +arvalues.size()+" "+arvalues.get(arvalues.size()-1).getMmNm()+" Msg: "+arvalues.get(arvalues.size()-1).getMsg());
        if (arvalues.size() > 0) {
            return arvalues.get(arvalues.size() - 1).getMsgId();
        }

        return "";
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private VoBoardList.VoBoardItem getItem(int position) {
        return arvalues.get(position - 1);
    }

    @Override
    public int getItemCount() {
        return arvalues.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        private LinearLayout ll_popular_moim;

        public ViewHolderHeader(View v) {
            super(v);
            ll_popular_moim = v.findViewById(R.id.ll_popular_moim);
        }
    }

    public static class ViewHolderItem extends RecyclerView.ViewHolder {

        private TextView tv_regUsr;
        private TextView time;
        //private TextView content;
        private EllipsizeTextView content;
        private LinearLayout llItem;
        private LinearLayout llThumbnailes;
        private TextView tvLkCnt;
        private TextView tvRpyCnt;
        private TextView tvNoti;
        private TextView tvRdCnt;

        private ImageView ImgbOption;
        private final ImageView imgvProfile;

        private LinearLayout ll_like_btn;
        private TextView tv_like;
        private LinearLayout ll_reply_btn;

        private final ImageButton imgbShare;
        private View viewTopGap;
        private LinearLayout llMmtg; //상당 모임종류

        private ImageView imgvSubject;
        private TextView tvMoimName;

        private LinearLayout llFile; //파일첨부

        //댓글 2
        private LinearLayout llReply;

        private LinearLayout llRep1;
        private LinearLayout llRep2;

        private LinearLayout llReplyCount;

        private ImageView imgvProfileRep1;
        private ImageView imgvProfileRep2;
        private TextView tvReplyUsid1;
        private TextView tvReplyUsid2;
        private TextView tvReplyReply1;
        private TextView tvReplyReply2;
        private TextView tvReplyTime1;
        private TextView tvReplyTime2;

        private ImageView imgvEmoticon;

        public ViewHolderItem(View v) {
            super(v);
            imgvProfile = (ImageView) v.findViewById(R.id.imgv_profile);
            tv_regUsr = (TextView) v.findViewById(R.id.tv_regUsr);
            time = (TextView) v.findViewById(R.id.tv_time);
            content = (EllipsizeTextView) v.findViewById(R.id.tv_contents);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            llThumbnailes = (LinearLayout) v.findViewById(R.id.ll_thumbnailes);
            tvLkCnt = (TextView) v.findViewById(R.id.tv_like_count);
            tvRpyCnt = (TextView) v.findViewById(R.id.tv_reply_count);
            tvNoti = (TextView) v.findViewById(R.id.tv_noti);
            tvRdCnt = (TextView) v.findViewById(R.id.tv_rdCnt);
            ImgbOption = (ImageView) v.findViewById(R.id.imgb_option);


            ll_like_btn = (LinearLayout) v.findViewById(R.id.ll_like_btn);
            ll_reply_btn = (LinearLayout) v.findViewById(R.id.ll_reply_btn);
            tv_like = (TextView) v.findViewById(R.id.tv_like);

            imgbShare = (ImageButton) v.findViewById(R.id.imgb_share);

            viewTopGap = (View) v.findViewById(R.id.view_top_gap);
            llMmtg = (LinearLayout) v.findViewById(R.id.ll_mmtg);

            imgvSubject = (ImageView) v.findViewById(R.id.imgv_subject);
            tvMoimName = (TextView) v.findViewById(R.id.tv_moim_name);

            llFile = (LinearLayout) v.findViewById(R.id.inc_file);


            imgvEmoticon = (ImageView) v.findViewById(R.id.imgv_emoticon);

            //댓글
            llReply = (LinearLayout) v.findViewById(R.id.ll_reply);
            llRep1 = (LinearLayout) v.findViewById(R.id.reply1);
            llRep2 = (LinearLayout) v.findViewById(R.id.reply2);

            imgvProfileRep1 = (ImageView) v.findViewById(R.id.reply1).findViewById(R.id.imgv_reply_profile);
            imgvProfileRep2 = (ImageView) v.findViewById(R.id.reply2).findViewById(R.id.imgv_reply_profile);

            tvReplyUsid1 = (TextView) v.findViewById(R.id.reply1).findViewById(R.id.tv_reply_usid);
            tvReplyUsid2 = (TextView) v.findViewById(R.id.reply2).findViewById(R.id.tv_reply_usid);

            tvReplyReply1 = (TextView) v.findViewById(R.id.reply1).findViewById(R.id.tv_reply_reply);
            tvReplyReply2 = (TextView) v.findViewById(R.id.reply2).findViewById(R.id.tv_reply_reply);

            tvReplyTime1 = (TextView) v.findViewById(R.id.reply1).findViewById(R.id.tv_reply_time);
            tvReplyTime2 = (TextView) v.findViewById(R.id.reply2).findViewById(R.id.tv_reply_time);

            llReplyCount = (LinearLayout) v.findViewById(R.id.ll_reply_count);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_popular_moim_header, parent, false);
            return new ViewHolderHeader(view);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_popular_item, parent, false);
            return new ViewHolderItem(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        //Log.e(TAG," onBindViewHolder position: "+position);
        if (viewHolder instanceof ViewHolderHeader) {
            ((ViewHolderHeader) viewHolder).ll_popular_moim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO::: Go to popular moim activity
                    Toast.makeText(mContext, "TODO: Go to popular moim screen!!!", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (viewHolder instanceof ViewHolderItem) {
            //댓글 표시
            if (getItem(position).getAlReply() != null)
                switch (getItem(position).getAlReply().size()) { //댓글 0~2개
                    case 0: //없으면 숨기고
                        ((ViewHolderItem) viewHolder).llReply.setVisibility(View.GONE);
                        break;

                    case 1: //하나면 llRep1만 보이고

                        ((ViewHolderItem) viewHolder).llReply.setVisibility(View.VISIBLE);
                        ((ViewHolderItem) viewHolder).llRep1.setVisibility(View.VISIBLE);
                        ((ViewHolderItem) viewHolder).llRep2.setVisibility(View.GONE);

                        Util.setMoimProfileGlide2(mContext, getItem(position).getAlReply().get(0).getRegUsr(), ((ViewHolderItem) viewHolder).imgvProfileRep1);

                        ((ViewHolderItem) viewHolder).tvReplyUsid1.setText(getItem(position).getAlReply().get(0).getUsNm());
                        ((ViewHolderItem) viewHolder).tvReplyTime1.setText(getItem(position).getAlReply().get(0).getRegDt());

                        ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(getItem(position).getAlReply().get(0).getMsg());

                        VoMsgItem voMsgItem1 = arMsgItem.get(arMsgItem.size() - 1);
                        switch (voMsgItem1.getType()) {
                            case "0":   //텍스트
                                ((ViewHolderItem) viewHolder).tvReplyReply1.setText(voMsgItem1.getData().replace("&nbsp;", ""));
                                break;

                            case "1":   //이미지
                                ((ViewHolderItem) viewHolder).tvReplyReply1.setText("(사진)");
                                break;

                            case "4":   //이모티몬
                                ((ViewHolderItem) viewHolder).tvReplyReply1.setText("(이모티콘)");
                                break;
                        }

                        break;

                    case 2:
                        ((ViewHolderItem) viewHolder).llReply.setVisibility(View.VISIBLE);
                        ((ViewHolderItem) viewHolder).llRep1.setVisibility(View.VISIBLE);
                        ((ViewHolderItem) viewHolder).llRep2.setVisibility(View.VISIBLE);

                        Util.setMoimProfileGlide2(mContext, getItem(position).getAlReply().get(0).getRegUsr(), ((ViewHolderItem) viewHolder).imgvProfileRep1);
                        Util.setMoimProfileGlide2(mContext, getItem(position).getAlReply().get(1).getRegUsr(), ((ViewHolderItem) viewHolder).imgvProfileRep2);

                        ((ViewHolderItem) viewHolder).tvReplyUsid1.setText(getItem(position).getAlReply().get(0).getUsNm());
                        ((ViewHolderItem) viewHolder).tvReplyTime1.setText(getItem(position).getAlReply().get(0).getRegDt());
                        ((ViewHolderItem) viewHolder).tvReplyUsid2.setText(getItem(position).getAlReply().get(1).getUsNm());
                        ((ViewHolderItem) viewHolder).tvReplyTime2.setText(getItem(position).getAlReply().get(1).getRegDt());

                        ArrayList<VoMsgItem> arMsgItem2_1 = Util.ParseMsg(getItem(position).getAlReply().get(0).getMsg());
                        ArrayList<VoMsgItem> arMsgItem2_2 = Util.ParseMsg(getItem(position).getAlReply().get(1).getMsg());

                        VoMsgItem voMsgItem2_1 = arMsgItem2_1.get(arMsgItem2_1.size() - 1);
                        VoMsgItem voMsgItem2_2 = arMsgItem2_2.get(arMsgItem2_2.size() - 1);

                        setTop2Replay(voMsgItem2_1, ((ViewHolderItem) viewHolder).tvReplyReply1);
                        setTop2Replay(voMsgItem2_2, ((ViewHolderItem) viewHolder).tvReplyReply2);
                        break;
                }

            //댓글 상세화면으로 이동
            ((ViewHolderItem) viewHolder).llReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBoardItemData(getItem(position), true);
                }
            });

            //댓글 텍스트
            ((ViewHolderItem) viewHolder).llReplyCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBoardItemData(getItem(position), true);
                }
            });

            //if(mContext instanceof Activity_CandleMain){ // 화면 구분수정  Activity_CandleMain삭제예정
            if (SET_ENTRY_TYPE == Constants.SET_ENTRY_TYPE.main_tab) { //메인 타임라인에서 오는경우
                ((ViewHolderItem) viewHolder).tvMoimName.setTextSize(17);
                ((ViewHolderItem) viewHolder).llMmtg.setVisibility(View.VISIBLE); //모임종류
                ((ViewHolderItem) viewHolder).tvMoimName.setText(getItem(position).getMmNm());

                setMmtgIcon(((ViewHolderItem) viewHolder).imgvSubject, getItem(position).getMmTg());

                // 내려오는 메뉴로 추가된 첫째게시물 상단 간격 (Top spacing of first post added by descending menu)
                if (position == 0) {
                    ((ViewHolderItem) viewHolder).viewTopGap.setVisibility(View.VISIBLE);
                } else {
                    ((ViewHolderItem) viewHolder).viewTopGap.setVisibility(View.GONE);
                }

            } else { //모임타암라인
                ((ViewHolderItem) viewHolder).viewTopGap.setVisibility(View.GONE);
                ((ViewHolderItem) viewHolder).llMmtg.setVisibility(View.GONE);
            }

            ((ViewHolderItem) viewHolder).tv_regUsr.setText(getItem(position).getUsNm());    //이름
            ((ViewHolderItem) viewHolder).time.setText(getItem(position).getRegDt());        //시간

            //아이디
            ((ViewHolderItem) viewHolder).tv_regUsr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentmProfile(getItem(position).getRegUsr(), getItem(position).getMmId());
                }
            });

            //프로필
            ((ViewHolderItem) viewHolder).imgvProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentmProfile(getItem(position).getRegUsr(), getItem(position).getMmId());
                }
            });

            //프로필 이미지 썸네일
            Util.setMoimProfileGlide2(mContext, getItem(position).getRegUsr(), ((ViewHolderItem) viewHolder).imgvProfile);

            ((ViewHolderItem) viewHolder).llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setBoardItemData(getItem(position), false);
                }
            });

            ((ViewHolderItem) viewHolder).llFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setBoardItemData(getItem(position), false);
                }
            });

            ((ViewHolderItem) viewHolder).llMmtg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intentTimeLine(getItem(position));
                }
            });

            ((ViewHolderItem) viewHolder).tvLkCnt.setText(getItem(position).getLkCnt());
            ((ViewHolderItem) viewHolder).tvRpyCnt.setText(getItem(position).getRpyCnt());
            ((ViewHolderItem) viewHolder).tvRdCnt.setText(getItem(position).getRdCnt());

            //글내용 표시부분
            String strMsg = getItem(position).getMsg();
            ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(strMsg);

            ArrayList<String> arImage = new ArrayList<>();  //썸네일 데이터

            String strText = ""; //내용
            String strFilename = null;  //파일 표시
            ArrayList<String> arEmoticon = new ArrayList<>();   //이모티콘 리스트
            int nFilecount = 0;

            for (int i = 0; i < arMsgItem.size(); i++) {

                switch (arMsgItem.get(i).getType()) {
                    case "0"://글
                        if (!TextUtils.isEmpty(arMsgItem.get(i).getData().trim())) {
                            strText = strText + arMsgItem.get(i).getData().trim() + mContext.getResources().getString(R.string.nextline);    //글 합치기
                        }
                        break;

                    case "1":  //이미지 썸네일
                        arImage.add(arMsgItem.get(i).getData());
                        break;

                    case "3": //파일
                        strFilename = arMsgItem.get(i).getData();
                        nFilecount++;
                        break;

                    case "4": //이모티콘
                        arEmoticon.add(arMsgItem.get(i).getData());
                        break;
                }
            }


            //----------텍스트 처리부분

            final String strContent = strText.replace("&nbsp;", "");

            //Log.e(TAG,"strContent: "+strContent);

            if (strText.equalsIgnoreCase("")) { //글이 없으면   텍스트는 숨기고.
                ((ViewHolderItem) viewHolder).content.setVisibility(View.GONE);

            } else {

                final int MAX_LINES = 5;
                final String TWO_SPACES = " ";

                ((ViewHolderItem) viewHolder).content.setVisibility(View.VISIBLE);
                //Vholder.content.setText(strContent);
                ((ViewHolderItem) viewHolder).content.setTextSize(SharedObject.getProperty_int(mContext, Constants.FONTSIZE, 16));

                SpannableString moreText = new SpannableString("...더보기");
                moreText.setSpan(new ForegroundColorSpan(Util.getColor(mContext, R.color.chatroom_thumb)), 0, moreText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                ((ViewHolderItem) viewHolder).content.setEllipsizeText(moreText, 0);
                ((ViewHolderItem) viewHolder).content.setText(strContent);

                //setLabelAfterEllipsis(Vholder.content,  R.string.friend, 5);

           /* Vholder.content.post(new Runnable() {  //TODO
                @Override
                public void run() {
                    //Log.e(TAG,"Vholder.content.getLineCount() :"+Vholder.content.getLineCount() );


                    if (Vholder.content.getLineCount() > MAX_LINES) {
                        int lastCharShown = Vholder.content.getLayout().getLineVisibleEnd(MAX_LINES -1);  //TODO
                        //Log.e(TAG,"lastCharShown: "+lastCharShown);
                        Vholder.content.setMaxLines(MAX_LINES);

                        String moreString = mContext.getString(R.string.more);
                        String suffix = TWO_SPACES + moreString;
                        String actionDisplayText;

                        try {
                            actionDisplayText = strContent.substring(0, lastCharShown - suffix.length() - 2) + "..." + suffix;
                            SpannableString truncatedSpannableString = new SpannableString(actionDisplayText);
                            int startIndex = actionDisplayText.indexOf(moreString);
                            truncatedSpannableString.setSpan(new ForegroundColorSpan(Util.getColor(mContext, R.color.chatroom_thumb)), startIndex, startIndex + moreString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            Vholder.content.setText(truncatedSpannableString);
                            //Log.e(TAG,"truncatedSpannableString: "+truncatedSpannableString);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Vholder.content.setText(strContent);
                    }
                }
            });*/

            }
            //이모티콜과 이미지 둘다있으면 이미지우선으로 보여주기.

            //썸네일 표시 부분
            if (arImage.size() > 0) {
                ((ViewHolderItem) viewHolder).imgvEmoticon.setVisibility(View.GONE);//이모티콘 숨김.
                ((ViewHolderItem) viewHolder).llThumbnailes.setVisibility(View.VISIBLE);
                ((ViewHolderItem) viewHolder).llThumbnailes.removeAllViews();

                //1장일때 이미지 사이즈 변화
                ViewGroup.LayoutParams params = ((ViewHolderItem) viewHolder).llThumbnailes.getLayoutParams();
                int height = 0;
                params.width = mContext.getResources().getDisplayMetrics().widthPixels;
                if (arImage.size() == 1) {
//                height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 230, mContext.getResources().getDisplayMetrics());

                    //3:4 비율
                    height = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.75);
                } else {
                    height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 180, mContext.getResources().getDisplayMetrics());

                    //4:3 비율
//                height = (int) (mContext.getResources().getDisplayMetrics().widthPixels * 0.75);
                }

                params.height = height;
                ((ViewHolderItem) viewHolder).llThumbnailes.setLayoutParams(params);

                for (int i = 0; i < arImage.size(); i++) {
                    mImage = new ImageView(mContext);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(getThumWidth(arImage.size()), LinearLayout.LayoutParams.MATCH_PARENT);
                    layoutParams.setMargins(0, 0, 5, 0);
                    layoutParams.gravity = Gravity.CENTER;

                    mImage.setLayoutParams(layoutParams);
                    mImage.setScaleType(ImageView.ScaleType.CENTER_CROP);


                    //Log.e(TAG,"arImage: "+Constants.MOIM_TIMELINE_LIST_IMAGE_URL + arImage.get(i).substring(0,11)+"thumb_"+arImage.get(i).substring(11,arImage.get(i).length()).replace(".jpg", ".png")  );
                    //Log.e(TAG,"arImage: "+Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                    //        + arImage.get(i).substring(0,11)+"thumb_"+arImage.get(i).substring(11,arImage.get(i).indexOf("."))+".png");

                    LogTrace.E("onBindViewHolder  arImage.get(i): " + arImage.get(i));

              /* if(arImage.get(i).contains("thumb_")){ //thumb_이 이미 있는경우.
                    Glide.with(mContext)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                    + arImage.get(i).substring(0,arImage.get(i).indexOf("."))+".png")

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)
                            .dontAnimate().into(mImage);

                }else{
                    Glide.with(mContext)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                    + arImage.get(i).substring(0,11)+"thumb_"+arImage.get(i).substring(11,arImage.get(i).indexOf("."))+".png")

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)
                            .dontAnimate().into(mImage);
                }*/

                    //이미지 jpg로딩 20180323 leeyunsu
                    Glide.with(mContext)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                    + arImage.get(i))

                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .skipMemoryCache(true)
                            .dontAnimate().into(mImage);


                /*Glide.with(mContext)
                        .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL
                                //+ arImage.get(i).substring(0,11)+"thumb_"+arImage.get(i).substring(11,arImage.get(i).length()).replace(".jpg", ".png") )
                                + arImage.get(i).substring(0,11)+"thumb_"+arImage.get(i).substring(11,arImage.get(i).indexOf("."))+".png")

                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .skipMemoryCache(true)//.placeholder(R.drawable.free)
                        .dontAnimate().into(mImage);*/

                    ((ViewHolderItem) viewHolder).llThumbnailes.addView(mImage);

                    mImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setBoardItemData(getItem(position), false);
                        }
                    });

                }

            } else if (arEmoticon.size() > 0 && arImage.size() == 0) { //사진없고 이모티콘 있으면 이모티콘을 보여준다.
                ((ViewHolderItem) viewHolder).llThumbnailes.setVisibility(View.GONE);//사진숨김
                ((ViewHolderItem) viewHolder).imgvEmoticon.setVisibility(View.VISIBLE);
                Bitmap bm;
                try {
                    bm = BitmapFactory.decodeResource(mContext.getResources(), getDrawableResourceByName(arEmoticon.get(0)));
                } catch (Exception e) {
                    bm = null;
                }
                ((ViewHolderItem) viewHolder).imgvEmoticon.setImageBitmap(bm);

            } else if (arEmoticon.size() == 0 && arImage.size() == 0) {
                ((ViewHolderItem) viewHolder).llThumbnailes.setVisibility(View.GONE);//사진숨김
                ((ViewHolderItem) viewHolder).imgvEmoticon.setVisibility(View.GONE);
            }

            //파일첨부
            if (nFilecount > 0) {
                ((ViewHolderItem) viewHolder).llFile.setVisibility(View.VISIBLE);
                ((TextView) ((ViewHolderItem) viewHolder).llFile.findViewById(R.id.tv_file)).setText("총 " + nFilecount + "개 파일");
                ((TextView) ((ViewHolderItem) viewHolder).llFile.findViewById(R.id.tv_filename)).setText(strFilename);

            } else {
                ((ViewHolderItem) viewHolder).llFile.setVisibility(View.GONE);
            }

            //공지, 긴급공지 처리
            switch (getItem(position).getNoti()) {
                case "0":
                    ((ViewHolderItem) viewHolder).tvNoti.setVisibility(View.GONE);
                    break;
                case "1":
                    ((ViewHolderItem) viewHolder).tvNoti.setVisibility(View.VISIBLE);
                    ((ViewHolderItem) viewHolder).tvNoti.setText(mContext.getString(R.string.noti));
                    break;
                case "2":
                    ((ViewHolderItem) viewHolder).tvNoti.setVisibility(View.VISIBLE);
                    ((ViewHolderItem) viewHolder).tvNoti.setText(mContext.getString(R.string.noti2));
                    break;
            }

            //본문복사, 공하기, 북마크, 신고하기, 이모임 글보지않기
            ((ViewHolderItem) viewHolder).ImgbOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mstt = SharedObject.getProperty_string(mContext, Constants.mstt, "");

                    if (mContext instanceof Activity_MoimTimeLineList) {
                        if (!"1".equals(mstt)) {
                            Toast.makeText(mContext, "모임에 가입후 이용 가능합니다!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    //글조회후 옵션팝업
                    SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, getItem(position).getMsgId());
                    SharedObject.setProperty_string(mContext, Constants.MOIM_ID, getItem(position).getMmId());
                    requestBoardReadOption(position, strContent);
                }
            });


            //좋아요 표시
            switch (getItem(position).getEmtStt()) {

                case "":   //감정없음
                    ((ViewHolderItem) viewHolder).tv_like.setText(mContext.getResources().getString(R.string.like));
                    break;

                case "1":  //좋아요
                    ((ViewHolderItem) viewHolder).tv_like.setText(mContext.getResources().getString(R.string.like_cancle2));
                    break;
            }


            //좋아요 버튼
            ((ViewHolderItem) viewHolder).ll_like_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String mstt = SharedObject.getProperty_string(mContext, Constants.mstt, "");

                    if (mContext instanceof Activity_MoimTimeLineList) {
                        if (!"1".equals(mstt)) {
                            Toast.makeText(mContext, "모임에 가입후 이용 가능합니다!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    //Log.e(TAG, "arvalues.get(position).getEmtStt(): " + arvalues.get(position).getEmtStt());
                    switch (getItem(position).getEmtStt()) {
                        case "":   //감정없음
                            moimEmotionRequest(getItem(position).getMmId(), getItem(position).getMsgId(), "1");//좋아요
                            break;

                        case "1":  //좋아요
                            moimEmotionRequest(getItem(position).getMmId(), getItem(position).getMsgId(), "0");//좋아요 취소
                            break;
                    }

                }
            });


            //댓글달기
            ((ViewHolderItem) viewHolder).ll_reply_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setBoardItemData(getItem(position), true);
                }
            });


            //공유하기 다이얼로그 //버튼삭제
            ((ViewHolderItem) viewHolder).imgbShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DialogShare("[" + arvalues.get(position).getMmNm() + "]" + arvalues.get(position).getTtl());
                    //dialogShare();

                    //TODO
                    //Util.intentShare(mContext, "[" + arvalues.get(position).getMmNm() + "]" + arvalues.get(position).getTtl()
                    //        + "http://naver.com"
                    //);

                }
            });
        }
    }


    /**
     * @param textView
     * @param labelId
     * @param maxLines
     */
    private void setLabelAfterEllipsis(TextView textView, int labelId, int maxLines) {

        try {
            if (textView.getLayout().getEllipsisCount(maxLines - 1) == 0) {
                return; // Nothing to do
            }

            int start = textView.getLayout().getLineStart(0);
            int end = textView.getLayout().getLineEnd(textView.getLineCount() - 1);
            String displayed = textView.getText().toString().substring(start, end);
            int displayedWidth = getTextWidth(displayed, textView.getTextSize());

            String strLabel = textView.getContext().getResources().getString(labelId);
            String ellipsis = "...";
            String suffix = ellipsis + strLabel;

            int textWidth;
            String newText = displayed;
            textWidth = getTextWidth(newText + suffix, textView.getTextSize());

            while (textWidth > displayedWidth) {
                newText = newText.substring(0, newText.length() - 1).trim();
                textWidth = getTextWidth(newText + suffix, textView.getTextSize());
            }

            textView.setText(newText + suffix);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private int getTextWidth(String text, float textSize) {
        Rect bounds = new Rect();
        Paint paint = new Paint();
        paint.setTextSize(textSize);
        paint.getTextBounds(text, 0, text.length(), bounds);

        int width = (int) Math.ceil(bounds.width());
        return width;
    }


    /**
     * 이모티콘 이름으로 이미지 가져오기.
     *
     * @param str
     * @return
     */
    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(mContext, str);
        String packageName = mContext.getPackageName();
        return mContext.getResources().getIdentifier(emoticon, "drawable", packageName);
    }

    /**
     * 모임 타임라인  댓글2개만 표시되는부분
     * 글,이모티콘,사진
     * 텍스트표시로표시
     *
     * @param voMsgItem
     * @param tvReplyReply
     */
    private void setTop2Replay(VoMsgItem voMsgItem, TextView tvReplyReply) {
        switch (voMsgItem.getType()) {
            case "0":   //텍스트
                tvReplyReply.setText(voMsgItem.getData().replace("&nbsp;", ""));
                tvReplyReply.setTextSize(SharedObject.getProperty_int(mContext, Constants.FONTSIZE, 14));
                break;

            case "1":   //이미지
                tvReplyReply.setText("(사진)");
                break;

            case "4":   //이모티몬
                tvReplyReply.setText("(이모티콘)");
                break;
        }
    }

    /**
     * @param imgvSubject
     * @param mmTg
     */
    private void setMmtgIcon(ImageView imgvSubject, String mmTg) {

        switch (mmTg) {
            case "1":   //1 : 피기맘
                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_time_edu));
                break;

            case "2":   //2 : 개인
                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_personal));
                break;

//            case "1":   //1 : 주식
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_stock));
//                break;
//
//            case "2":   //2 : 국내파생
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_derivation));
//                break;

//            case "3":    //3 : 해외파생
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_global_derivation));
//                break;
//
//            case "4":   //4 : 해외주식
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_global_stock));
//                break;
//
//            case "5":   //5 : 금융상품
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_bank_product));
//                break;
//
//            case "6":   //6 : 기타
//                imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_etc));
//                break;
        }
    }

    /**
     * 타임라인 썸네일 갯수별 가로 사이즈를 가져온다
     *
     * @param size
     */
    private int getThumWidth(int size) {
        int nWidth = 0;

        if (size > 2) {
            nWidth = (Util.getwidth(mContext) / 2) - 60;

        } else if (size == 2) {
            nWidth = (Util.getwidth(mContext) / 2);

        } else if (size == 1) {
            nWidth = Util.getwidth(mContext);
        }

        return nWidth;
    }

    /**
     * 공유하기 팝업
     */
    private void dialogShare() {
        Dialog_share mDialog = new Dialog_share(mContext);
        mDialog.show();
    }

    /**
     * 옵션메뉴  공유하기
     * 기능삭제됨
     */
    /*private void intentShare(String text) {
     *//* Dialog_share mDialog = new Dialog_share(mContext);
        mDialog.show();*//*
        Intent msg = new Intent(Intent.ACTION_SEND);
        msg.addCategory(Intent.CATEGORY_DEFAULT);
        msg.putExtra(Intent.EXTRA_TEXT, text);
        msg.putExtra(Intent.EXTRA_TITLE, "EXTRA_TITLE");
        msg.setType("text/plain");
        mContext.startActivity(Intent.createChooser(msg, "공유"));

    }*/

    /**
     * 7.16 모임 공지사항 설정/해제
     * noti     공지 여부(int)     0:일반, 1:공지
     */
    private void moimNotice(String msgId, String noti) {
        Moa.moimNotice(mContext, msgId, noti, mMainActvtHandler);
    }

    /**
     * 7.17 모임 글 삭제
     *
     * @param msgId
     */
    private void moimBoardDeleteRequest(String msgId, String mmId) {
        Moa.moimBoardDelete(mContext, msgId, mmId, mMainActvtHandler);
    }

    /**
     * 7.23 모임 글 감정표현
     *
     * @param mmId
     * @param msgId
     * @param emt   감정표현(int) 0:기존감정표현취소, 1:좋아요, 2:싫어요
     */
    private void moimEmotionRequest(String mmId, String msgId, String emt) {
        Moa.moimEmotionRequest(mContext, mmId, msgId, emt, mMainActvtHandler);
    }

    /**
     * 7.29 모임 글 차단
     *
     * @param mmBbsBlk
     */
    private void moimBbsBlockRequest(String mmBbsBlk) {
        Moa.moimBbsBlockRequest(mContext, mmBbsBlk, mMainActvtHandler);
    }


    /**
     * 7.19 모임 글 조회
     * 한번조회후 있는글이면 이동
     */
    private void requestBoardRead() {
        Moa.moimBoardReadRequest(mContext, mMainActvtHandler, "0");
    }

    /**
     * 삭제된 글확인을 위한
     * 7.19 모임 글 조회
     * 옵션메뉴전 호출 핸들러 처리
     */
    private void requestBoardReadOption(final int pos, final String strContent) {
        Moa.moimBoardReadRequest(mContext, new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                try {
                    MOAData data = (MOAData) msg.obj;
                    //MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                    String strResult;

                    switch (data.ptc) {

                        case PacketTypes.PTC_IMS_MOIM_BOARD_READ:   //7.19 모임 글 조회
                            strResult = (String) data.body.get("result");
                            Log.e(TAG, "strResult: " + strResult);
                            Log.i("TEST", "moim_strResult: " + data.body);
                            if (strResult.equals(Constants.SUCCESS)) {
                                //intentTimeLineDetail();
                                //팝업 처리
                                setOptionPopup(pos, strContent);

                            } else {
                                final Dialog_Text dialog = new Dialog_Text(mContext, "삭제된 글입니다.", Constants.DIALOG_BUTTON_TYPE.confirm);
                                dialog.setListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Log.e(TAG, "onClick" + v.getId());
                                        dialog.dismiss();

                                        refresh();
                                    }
                                });
                                dialog.show();
                            }
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }), "0");
    }

    /**
     * 타임라인 옵션 팝업  설정
     *
     * @param position
     */
    private void setOptionPopup(final int position, final String strContent) {

        final Dialog_TimeLineOption_2 dialog_TimeLineOption = new Dialog_TimeLineOption_2(
                mContext
                , getItem(position).getNoti()
                , getItem(position).getRegUsr()
                , getItem(position).getUsrLv()
                , SET_ENTRY_TYPE);

        dialog_TimeLineOption.show();

        dialog_TimeLineOption.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_TimeLineOption.dismiss();

                int i = v.getId();

                if (i == R.id.tv_option0) {
                    if (getItem(position).getNoti().equals("0")) {   //공지가 아닐경우 공지 등록
                        moimNotice(getItem(position).getMsgId(), "1");
                    } else {
                        moimNotice(getItem(position).getMsgId(), "0"); //공지 해제
                    }
                } else if (i == R.id.tv_option1) {
                    Util.copyToClipBoard(mContext, strContent);

                    Toast.makeText(mContext, "" + mContext.getString(R.string.moim_copy_complete), Toast.LENGTH_SHORT).show();
                } else if (i == R.id.tv_option2) {

                    //본문내용 공유
                    Log.e(TAG, "arvalues.get(position).getMmNm(): " + getItem(position).getMmNm() + " " + getItem(position).getTtl());

                    Util.shareDialog(mContext, getItem(position).getMsgId(),
                            getItem(position).getMsg(),
                            getItem(position).getNoti(),
                            getItem(position).getMmNm(),
                            strContent,
                            "");

                } else if (i == R.id.tv_option3) {  //북마크

                } else if (i == R.id.tv_option4) {//신고하기
                    SharedObject.setProperty_string(mContext, Constants.MOIM_ID, getItem(position).getMmId());
                    SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, getItem(position).getMsgId());
                    SharedObject.setProperty_string(mContext, Constants.MOIM_REGUSER, getItem(position).getRegUsr());//작성자
                    SharedObject.setProperty_string(mContext, Constants.MOIM_TTL, getItem(position).getTtl());//첫줄

                    intentContentReport();

                } else if (i == R.id.tv_option5) { //이 모임글 보지 않기

                    SharedObject.setProperty_string(mContext, Constants.MOIM_ID, getItem(position).getMmId());
                    dialogBlock(getItem(position).getMmNm());

                } else if (i == R.id.tv_option6) { //글수정하기

                    SharedObject.setProperty_string(mContext, Constants.MOIM_WRITE_TYPE, Constants.MOIM_WRITE_TYPE_MODIFY);
                    SharedObject.setProperty_string(mContext, Constants.MOIM_ID, getItem(position).getMmId());      //모임아이디
                    SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, getItem(position).getMsgId());  //메시지 키값
                    SharedObject.setProperty_string(mContext, Constants.MOIM_MSG, getItem(position).getMsg());      //모임 메시지
                    SharedObject.setProperty_string(mContext, Constants.MOIM_NOTI, getItem(position).getNoti());    //모임 공지
                    intentmModify();

                } else if (i == R.id.tv_option7) {  //글삭제
                    moimBoardDeleteRequest(getItem(position).getMsgId(), getItem(position).getMmId());
                }
            }
        });
    }

    /**
     * 리스트 새로고침.
     */
    private void refresh() {
        if (mContext instanceof Activity_MoimTimeLineList) {
            ((Activity_MoimTimeLineList) mContext).refreshItems();
        } else {
            Fragment_MoimTab1_Timeline.instance.refreshItems();
        }
    }


    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : " + data.body.toString());
                Log.e(TAG, "data.ptc  " + data.ptc);

                String strResult;

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_BOARD_READ:   //7.19 모임 글 조회
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        Log.i("TEST", "moim_strResult: " + data.body);

                        if (strResult.equals(Constants.SUCCESS)) {

                            intentTimeLineDetail();

                        } else {

                            final Dialog_Text dialog = new Dialog_Text(mContext, "삭제된 글입니다.", Constants.DIALOG_BUTTON_TYPE.confirm);

                            dialog.setListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Log.e(TAG, "onClick" + v.getId());
                                    dialog.dismiss();

                                    refresh();
                                }
                            });


                            dialog.show();
                        }

                        break;

                    case PacketTypes.PTC_IMS_MOIM_BOARD_DELETE:    //글 삭제
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Toast.makeText(mContext, "글 삭제 완료", Toast.LENGTH_SHORT).show();

                            String msgId = data.body.getJson("params").getString("msgId");

                            for (int i = 0; i < arvalues.size(); i++) { //타임라인 삭제.
                                if (msgId.equals(arvalues.get(i).getMsgId())) {

                                    if (arvalues.get(i).getNoti().equals("0")) {

                                    } else {//공지,중요공지 처리
                                        if (mContext instanceof Activity_MoimTimeLineList) {

                                            Activity_MoimTimeLineList.activity.bMoimProfile = true;
                                            Activity_MoimTimeLineList.activity.MoimProfileRequest(); //프로필 부분만 다시 호출한다.
                                            //((Activity_MoimTimeLineList) mContext).MoimProfileRequest(); //프로필 부분만 다시 호출한다.
                                        }
                                    }

                                    removeAt(i);
                                    break;
                                }
                            }
                        } else {
                            Toast.makeText(mContext, "글삭제 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_EMOTION:    //7.23 모임 글 감정표현
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());

                            String msgId = data.body.getJson("params").getString("msgId");
                            String emt = data.body.getJson("params").getString("emt");

                            for (int i = 0; i < arvalues.size(); i++) {
                                if (msgId.equals(arvalues.get(i).getMsgId())) {

                                    if (emt.equals("0")) {
                                        arvalues.get(i).setEmtStt("");
                                        arvalues.get(i).setLkCnt((Integer.parseInt(arvalues.get(i).getLkCnt()) - 1) + "");  //좋아요 내리기
                                        //Toast.makeText(mContext, "좋아요 취소", Toast.LENGTH_SHORT).show();
                                    } else {
                                        arvalues.get(i).setEmtStt("1");
                                        arvalues.get(i).setLkCnt((Integer.parseInt(arvalues.get(i).getLkCnt()) + 1) + "");  //좋아요 올리기
                                        //Toast.makeText(mContext, "좋아요 완료", Toast.LENGTH_SHORT).show();
                                    }

                                    break;
                                }
                            }

                            if (mContext instanceof Activity_MoimTimeLineList) {
                                ((Activity_MoimTimeLineList) mContext).reFreshAdapter();
                            } else {
                                Fragment_MoimTab1_Timeline.instance.reFreshAdapter();
                            }

                        } else {
                            Toast.makeText(mContext, "좋아요 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_BBS_BLOCK:    //7.29 모임 글 차단
                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);

                        if (strResult.equals(Constants.SUCCESS)) {
                            Log.e(TAG, "params: " + data.body.get("params").toString());
                            Toast.makeText(mContext, "차단 완료", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(mContext, "차단 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case PacketTypes.PTC_IMS_MOIM_NOTICE:  //모임 공지사항 설정/해제
                        Log.e(TAG, "PTC_IMS_MOIM_NOTICE: ");

                        strResult = (String) data.body.get("result");
                        Log.e(TAG, "strResult: " + strResult);
                        if (strResult.equals(Constants.SUCCESS)) {

                            //String msgId = data.body.getJson("params").getString("msgId");

                            //Log.e(TAG, "params: " + data.body.get("params").toString());

                            //공지 등록 결과 처리
                            setNotiDate(data.body.getJson("params").getString("msgId").toString()
                                    , data.body.getJson("params").getString("noti").toString());

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 글삭제이후
     * 데이터를 삭제한다
     *
     * @param position
     */
    public void removeAt(int position) {
        Log.e(TAG, "removeAt: " + position + " " + getItem(position).getMsg());
        arvalues.remove(position - 1);

        if (mContext instanceof Activity_MoimTimeLineList) {
            ((Activity_MoimTimeLineList) mContext).RemoveAdapter(position - 1, arvalues.size());
        } else {
            Fragment_MoimTab1_Timeline.instance.RemoveAdapter(position - 1, arvalues.size());
        }
    }

    /**
     * 타임라인 상세화면 intent 하기전 체크
     * 메시지아이디, 모임아이디 저장하고
     * 조회가능한 글인지 체크
     *
     * @param reply true :댓글달기 키보드보이기.  false:키보드없이 이동만.
     */
    private void setBoardItemData(VoBoardList.VoBoardItem voBoardItem, boolean reply) {
        String mstt = SharedObject.getProperty_string(mContext, Constants.mstt, "");

        if (mContext instanceof Activity_MoimTimeLineList) {
            if (!"1".equals(mstt)) {
                Toast.makeText(mContext, "모임에 가입후 이용 가능합니다!", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, voBoardItem.getMsgId());
        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, voBoardItem.getMmId());
        SharedObject.setProperty_boolean(mContext, Constants.MOIM_REPLY, reply);

        if (SET_ENTRY_TYPE == Constants.SET_ENTRY_TYPE.main_tab) {
            SharedObject.setProperty_boolean(mContext, Constants.MOIM_MAIN_TIMELINE, true);
            SharedObject.setProperty_string(mContext, Constants.rpyTy, voBoardItem.getRpyTy());    //메인에서 들어올때 댓글 권한. 댓글 쓰기 권한(int) 0:모든멤버(기본), 1:리더, 2:리더와 공동리더

        } else {
            SharedObject.setProperty_boolean(mContext, Constants.MOIM_MAIN_TIMELINE, false);
        }


        //삭제된 모임글 클릭 혹은 터치시 ‘삭제된 글입니다’ 메시지 팝업 필요
        //글조회 후 성공이면  상세 화면으로 이동
        requestBoardRead();
    }

    /**
     * 글상세 화면으로 이동
     */
    private void intentTimeLineDetail() {
        //Log.e(TAG, "intentTimeLineDetail " + SET_ENTRY_TYPE);
        Intent intent = new Intent(mContext, Activity_TimeLineDetail.class);
        mContext.startActivity(intent);
    }


    /**
     * 모임타임라인으로 이동
     *
     * @param voBoardItem
     */
    private void intentTimeLine(VoBoardList.VoBoardItem voBoardItem) {
        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, voBoardItem.getMmId());
        Intent intent = new Intent(mContext, Activity_MoimTimeLineList.class);
        mContext.startActivity(intent);
    }


    /**
     * 컨텐츠 신고하기
     */
    private void intentContentReport() {
        Intent intent = new Intent(mContext, Activity_MoimContentReport.class);
        mContext.startActivity(intent);
    }

    /**
     * 글쓰기화면
     * 글수정하기 intent
     */
    private void intentmModify() {

        Intent intent = new Intent(mContext, Activity_MoimWrite.class);
        if (mContext instanceof Activity_MoimTimeLineList) {  //모임타임라인
            ((Activity_MoimTimeLineList) mContext).startActivityForResult(intent, Constants.RESULT_MOIM_WRITE);
        } else {
            mContext.startActivity(intent); //메인화면
        }
    }


    /**
     * 프로필화면
     *
     * @param
     * @param
     */

    private void intentmProfile(String regUsr, String mmid) {
        String mstt = SharedObject.getProperty_string(mContext, Constants.mstt, "");

        if (mContext instanceof Activity_MoimTimeLineList) {
            if (!"1".equals(mstt)) {
                Toast.makeText(mContext, "모임에 가입후 이용 가능합니다!", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Log.e(TAG, "intentmProfile regUsr:" + regUsr + " mmid: " + mmid);
        SharedObject.setProperty_string(mContext, Constants.MOIM_USER_PROFILE_ID, regUsr);    //글등록자
        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, mmid);  //모임아이디
        // 모임정보 추가
        Intent intent = new Intent(mContext, Activity_MoimProfile.class);
        mContext.startActivity(intent);
    }


    /**
     * 이모임글 보지않기
     *
     * @param user
     */
    private void dialogBlock(final String user) {

        final Dialog_Report dialog_Report = new Dialog_Report(mContext, mContext.getString(R.string.report_block, user));
        dialog_Report.show();

        dialog_Report.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Report.dismiss();

                int i = v.getId();
                if (i == R.id.tv_cancle) {

                } else if (i == R.id.tv_confirm) {
                    moimBbsBlockRequest("1");
                }
            }
        });

    }
}