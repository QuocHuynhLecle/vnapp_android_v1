package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.activity.Activity_MoimWrite;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.RadiusImageView;
import com.wave.messenger.utility.SharedObject;
import com.wave.messenger.vo.VoMoimHiddenList;

import java.util.ArrayList;


/**
 * Created by yunsu on 2017. 7. 04..
 *  모임선택   어댑터
 */
public class SelectMoimAdapter extends RecyclerView.Adapter<SelectMoimAdapter.ViewHolder> {

    String TAG = this.getClass().getSimpleName();

    ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arvalues = new ArrayList<>();

    Context mContext;
    ImageView mImage;
    //String mMmid;


    private SparseBooleanArray selectedItems= new SparseBooleanArray(); //선택 표시 저장위

    public SelectMoimAdapter(Context con) {
        mContext = con;
    }

    public void setItem(ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> values) {

        arvalues = new ArrayList<>();

        for (VoMoimHiddenList.VoMoimHiddenListItem item:values) {
            if(item.getmStt().equals("1")){ //가입된 모임만  모임선택 리스트에 추가하기.
                arvalues.add(item);
            }

        }

    }

    /**
     * 모임아이디
     * @return
     */
    public VoMoimHiddenList.VoMoimHiddenListItem getMmid() {
        Log.e(TAG,"getMmid "+selectedItems.size());

        for(int i = 0; i < selectedItems.size(); i++) {
            int key = selectedItems.keyAt(i);
            // get the object by the key.
            return arvalues.get(key);

            //return arvalues.get(key).getMmId();
            //Log.e(TAG,"arvalues.get(key) "+arvalues.get(key).getMmId());
        }


        return null;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout llItem;
        //final public ImageView imgvThum;  //모임썸네일
        public TextView tvMoimName; //모임네임
        public ImageView imgvCheck; //체크 이미지
        public ImageView imgThum;
        public ImageView imgThumIcon;

        public ViewHolder(View v) {
            super(v);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            tvMoimName = (TextView) v.findViewById(R.id.tv_moim_name);
            //imgvThum = (ImageView) v.findViewById(R.id.imgv_thum);
            imgvCheck = (ImageView) v.findViewById(R.id.imgv_check);

            imgThum = (RadiusImageView)v.findViewById(R.id.imgv_thum_img);
            imgThumIcon= (ImageView)v.findViewById(R.id.imgv_thum_icon);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_moim_select, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, final int position) {

        Vholder.imgvCheck.setSelected(selectedItems.get(position, false));


        //모임명
        Vholder.tvMoimName.setText(arvalues.get(position).getMmNm());

        /*Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allSelectDelete();
                if (selectedItems.get(position, false)) {
                    //selectedItems.delete(position);
                    Vholder.imgvCheck.setSelected(false);
                }
                else {
                    selectedItems.put(position, true);
                    Vholder.imgvCheck.setSelected(true);
                }
            }
        });*/


        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentWrite(arvalues.get(position));


            }
        });





/*
        Vholder.imgvCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //모두

            }
        });*/



        /*Glide.with(mContext).load(
               Constants.MOIM_TIMELINE_LIST_IMAGE_URL + arImage.get(0)).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into(Vholder.imgvThum);*/



        if(arvalues.get(position).getImgTy().equals("1")) {//이미지가 있을 경우

            Vholder.imgThumIcon.setVisibility(View.GONE);
            Vholder.imgThum.setVisibility(View.VISIBLE);

            Glide.with(mContext).load(Constants.MOIM_LIST_IMAGE_URL +arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)  //.placeholder(R.drawable.free)
                    .dontAnimate().into(Vholder.imgThum);


            //Vholder.imgvThum.setPadding(0, 0, 0, 0);

        }else{
            Vholder.imgThumIcon.setVisibility(View.VISIBLE);
            Vholder.imgThum.setVisibility(View.GONE);

            Glide.clear(Vholder.imgThum);
            setMmtgIcon(Vholder.imgThumIcon, arvalues.get(position).getMmTg());
        }




    }

    /**
     * 선택한 값들을 모두 삭제
     */
    private void allSelectDelete() {
        selectedItems.clear();
        notifyDataSetChanged();
    }


    /**
     * 종류 표시
     * @param imgvSubject
     * @param mmTg
     */
    private void setMmtgIcon(ImageView imgvSubject, String mmTg) {

        //imgvSubject.setPadding(40, 40, 40, 40);

        switch (mmTg) {
            case "1":   //1 : 타임교육
                imgvSubject.setImageResource(R.drawable.ic_time_edu);
                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_stock));
                break;

            case "2":   //2 : 개인
                imgvSubject.setImageResource(R.drawable.ic_personal);
                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_derivation));
                break;

//            case "1":   //1 : 주식
//                imgvSubject.setImageResource(R.drawable.ic_stock);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_stock));
//                break;
//
//            case "2":   //2 : 국내파생
//                imgvSubject.setImageResource(R.drawable.ic_derivation);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_derivation));
//                break;
//
//            case "3":    //3 : 해외파생
//                imgvSubject.setImageResource(R.drawable.ic_global_derivation);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_global_derivation));
//                break;
//
//            case "4":   //4 : 해외주식
//                imgvSubject.setImageResource(R.drawable.ic_global_stock);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_global_stock));
//                break;
//
//            case "5":   //5 : 금융상품
//                imgvSubject.setImageResource(R.drawable.ic_bank_product);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_bank_product));
//                break;
//
//            case "6":   //6 : 기타
//                imgvSubject.setImageResource(R.drawable.ic_etc);
//                //imgvSubject.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.ic_mini_etc));
//                break;
        }

    }




    /**
     * 글쓰기화면으로 넘어감
     * @param voMoimHiddenListItem
     */
    private void intentWrite(VoMoimHiddenList.VoMoimHiddenListItem voMoimHiddenListItem) {
        //모임 아이디, 모임명 저장
        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, voMoimHiddenListItem.getMmId());
        SharedObject.setProperty_string(mContext, Constants.mmNm, voMoimHiddenListItem.getMmNm());
        //com.hanawm.massenger.utility.SharedObject.setProperty_boolean(mContext, Constants.MOIM_WRITE_MAIN, true);   //메인
        //com.hanawm.massenger.utility.SharedObject.setProperty_boolean(mContext, Constants.MOIM_WRITE_TYPE, true);   //글쓰기기

        Intent intent = new Intent(mContext, Activity_MoimWrite.class);
        mContext.startActivity(intent);
    }


    @Override
    public int getItemCount() {

        return arvalues.size();
    }


    /**
     * 타임라인 상세화면 intent
     *
     * @param msgId
     * @param reply true :댓글달기
     */
    /*private void intentTimeLineDetail(String msgId, boolean reply) {

        SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, msgId);
        SharedObject.setProperty_boolean(mContext, Constants.MOIM_REPLY, reply);

        Intent intent = new Intent(mContext, Activity_TimeLineDetail.class);
        mContext.startActivity(intent);
    }*/

}