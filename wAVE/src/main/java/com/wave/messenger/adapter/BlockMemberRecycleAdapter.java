package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_SetBlockMember;
import com.wave.messenger.dialog.Dialog_Withdraw;
import com.wave.messenger.fragment.dummy.Item_LeaderProfile;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;


/**
 * 멤버차단탈퇴차단목록 어댑터
 */
public class BlockMemberRecycleAdapter extends RecyclerView.Adapter<BlockMemberRecycleAdapter.ViewHolder> {

    String TAG=this.getClass().getName();
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arvalues= new ArrayList<>();
    Context mContext;
    String mType;



    public BlockMemberRecycleAdapter(Context con, String mType) {
        mContext = con;
        this.mType = mType;
    }


    public void setItem(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {
        arvalues= new ArrayList<>();

        for (VoMoimMemberList.VoMoimMemberlistItem item : params) { //나만 뺀 아이디
            if(!item.getUserId().equals(MessengerInfo.getUserId(mContext))){
                arvalues.add(item);
            }
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public ImageView imgvProfile;
        public LinearLayout llItem;
        public Button btnWithdraw;
        public Button btnBlock;


        public ViewHolder(View v) {
            super(v);

            tvName = (TextView) v.findViewById(R.id.tv_name);
            imgvProfile= (ImageView) v.findViewById(R.id.imgv_profile);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            btnWithdraw = (Button) v.findViewById(R.id.btn_withdraw);
            btnBlock = (Button) v.findViewById(R.id.btn_block);

        }
    }

    @Override
    public BlockMemberRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_blockmember, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        arvalues.get(position).setnNum(position);

        holder.tvName.setText(arvalues.get(position).getUsNm());
        //Util.setGlideProfile(mContext, arvalues.get(position).getUserId(), holder.imgvProfile);


        /*Util.setMoimProfileGlide(mContext, SharedObject.getProperty_string(mContext, Constants.MOIM_ID, null)
                , arvalues.get(position).getPfImg()
                , holder.imgvProfile);*/

        Util.setMoimProfileGlide2(mContext, arvalues.get(position).getUserId(), holder.imgvProfile);






        //type 0:강제탈퇴기능 1:차단해제기능
        if(mType.equals("0")){  //차단

            //리스트에 있는 리더는 무조건 삭제 불가능 usrLv
            if(arvalues.get(position).getUsrLv().equals("1")){
                holder.btnWithdraw.setVisibility(View.GONE);
            }else{
                holder.btnWithdraw.setVisibility(View.VISIBLE);
            }

            if (Util.getMymLv(mContext).equals("2") && arvalues.get(position).getUsrLv().equals("2")) {  //내가 공동리더이고 상대가 공동리더이면  탈퇴 불가능.
                holder.btnWithdraw.setVisibility(View.GONE);
            }


            //holder.btnWithdraw.setVisibility(View.VISIBLE);
            holder.btnBlock.setVisibility(View.GONE);

            holder.btnWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //강제탈퇴 dialog
                    final Dialog_Withdraw dialog= new Dialog_Withdraw(mContext, mContext.getResources().getString(R.string.withdraw_selected_member), 0);

                    dialog.setConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            MoimForceExitRequest(arvalues.get(position).getUserId(),arvalues.get(position).getUsNm(),dialog.getmItemPosition()+"");
                            //removeAt(v.getId());

                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            });

        }else{  //해제
            holder.btnWithdraw.setVisibility(View.GONE);
            holder.btnBlock.setVisibility(View.VISIBLE);

            holder.btnBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MoimUnblockRequest(arvalues.get(position).getUserId());
                }
            });

            //MoimForceExitRequest(arvalues.get(position).getUserId(),"1");

            /*holder.btnWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //강제탈퇴 dialog
                    final Dialog_Withdraw dialog= new Dialog_Withdraw(mContext, mContext.getResources().getString(R.string.withdraw_selected_member), position);

                    dialog.setblockListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            MoimForceExitRequest(arvalues.get(position).getUserId(),"1");

                            //removeAt(v.getId());
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
            });*/


        }




    }


    /**
     * 체크 모두 false로
     */
    /*private void deselectAllitem() {
        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if (item.isSelected()) {
                item.setSelected(false);
            }
        }
    }*/


    @Override
    public int getItemCount() {

        return arvalues.size();
    }

    /*public void removeAt(int position) {

        arvalues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arvalues.size());
    }*/





    /**
     * 선택된 공동리더 리턴
     * @return
     */
    public ArrayList<Item_LeaderProfile> getSelectedList(){

        /*
        ArrayList<Item_LeaderProfile> arSelectedFriend= new ArrayList<>();

        for (Item_LeaderProfile FriendBean : mFriendBeanList) {
            if (FriendBean.isSelected()) {
                arSelectedFriend.add(FriendBean);
            }
        }
        //mSelectAllCb.setChecked(isSelectAll);
        return arSelectedFriend;*/

        return null;
    }


    /**
     * 선택된 리더위임 리턴
     * @return
     */
    public String  getSelectedLeaderItem() {
        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if (item.isSelected()) {
                return item.getUserId();
            }
        }
        /*ArrayList<Item_LeaderProfile> arSelectedFriend= new ArrayList<>();

        for (Item_LeaderProfile FriendBean : mFriendBeanList) {
            if (FriendBean.isSelected()) {
                return FriendBean;
            }
        }*/
        return null;
    }

    /**
     * 7.11 모임 강제탈퇴
     * PTG : 0x00080000, PTC : 0x00080008
     PTG_IMS_MOIM, PTC_IMS_MOIM_FORCE_EXIT(차단로직구현필요)

     mmExtUsr     강퇴할 사용자 아이디
     mmUsrBlk     강제퇴장 후 차단 여부(int)     0:차단안함, 1:차단
     */
    private void MoimForceExitRequest(String mmExtUsr, String mRoomUserName, String mmUsrBlk) {
        MOALog.w(TAG+" requestMoimProfile");
        String mRoomId = SharedObject.getProperty_string(mContext, Constants.MOIM_CHATROOMID, null);
        Moa.moimForceExit(mContext,mmExtUsr, mmUsrBlk, mMainActivityHandler);
        Moa.chatRoomForceExit(mContext, mRoomId, mmExtUsr, mRoomUserName, mMainActivityHandler);
    }


    /**
     *7.33 모임 차단해제
     * mmUnblkUsr     차단해제 할 사용자 아이디
     */
    private void MoimUnblockRequest(String mmUnblkUsr) {
        Moa.moimUserUnblock(mContext, mmUnblkUsr, mMainActivityHandler);
    }


    //강제탈 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case 0x00080036 :    //차단해제
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            String mmUnblkUsr= data.body.getJson("params").getString("mmUnblkUsr");

                            removeItem(mmUnblkUsr);

                            Toast.makeText(mContext, "차단 해제를 완료했습니다", Toast.LENGTH_SHORT).show();
                        }

                        break;

                    case PacketTypes.PTC_IMS_MOIM_FORCE_EXIT :    // 탈퇴 차단
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            String mmExtUsr= data.body.getJson("params").getString("mmExtUsr");

                            removeItem(mmExtUsr);

                            Toast.makeText(mContext, "처리했습니다", Toast.LENGTH_SHORT).show();


                            //새로고침
                            ((Activity_SetBlockMember)mContext).refresh();


                            /*
                            Log.e(TAG,"params: extUsrextUsr "+extUsr);

                            removeItem(extUsr); //리스트에서 삭제
                            Toast.makeText(context, "처리했습니다", Toast.LENGTH_SHORT).show();
                            */

                        }
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });


    /**
     * 차단한 아이디를 리스트에서 삭제한다
     * @param extUsr
     */
    private void removeItem(String extUsr) {

        for (VoMoimMemberList.VoMoimMemberlistItem item : arvalues) {
            if(item.getUserId().equals(extUsr)){
                arvalues.remove(item);
                notifyDataSetChanged();
                Log.e(TAG,"removeItem: "+extUsr);
                break;
            }
        }


        //((Activity_MoimMemberInfoList)context).refreshList();

    }

}