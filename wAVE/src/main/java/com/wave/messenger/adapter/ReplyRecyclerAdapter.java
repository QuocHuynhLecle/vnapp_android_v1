package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Emoticon.Emoticon_Ko;
import com.wave.messenger.activity.Activity_MoimContentReport;
import com.wave.messenger.activity.Activity_MoimProfile;
import com.wave.messenger.activity.Activity_MoimReplyModify;
import com.wave.messenger.activity.Activity_MoimTimeLineList;
import com.wave.messenger.activity.Activity_PreviewImage;
import com.wave.messenger.activity.Activity_Re_Reply;
import com.wave.messenger.activity.Activity_TimeLineDetail;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_Reply;
import com.wave.messenger.fragment.Fragment_MoimTab1_Timeline;
import com.wave.messenger.mvp.ChatAlbum.AlbumPicture;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.SharedObject;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoBoardRead;
import com.wave.messenger.vo.VoMsgItem;

import java.io.Serializable;
import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 3. 17..
 * 모임상세 화면 댓글 어댑터
 * 공지사항 상세화면 댓글 어댑터
 */
public class ReplyRecyclerAdapter extends RecyclerView.Adapter<ReplyRecyclerAdapter.ViewHolder> {

    //ArrayList<VoMoimReplyList> arvalues;
    ArrayList<VoBoardRead.VoReadReply> arVoReadReply;
    Context mContext;
    private VoMsgItem item;
    String TAG = getClass().getSimpleName();



    public ReplyRecyclerAdapter(Context con, ArrayList<VoBoardRead.VoReadReply> values) {
        Log.e("ReplyRecyclerAdapter", "ReplyRecyclerAdapter values:" + values.size());
        this.arVoReadReply = values;
        this.mContext = con;


//        Collections.sort(arVoReadReply, VoBoardRead.DESCENDING_COMPARATOR);
        /*for (int i = 0; i < arVoReadReply.size(); i++) {
            Log.e("ReplyRecyclerAdapter", " Msg:" + arVoReadReply.get(i).getMsg() + " BbsOrd: " + arVoReadReply.get(i).getBbsOrd() + " bbsDph:" + arVoReadReply.get(i).getBbsDph());

        }*/
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView tvId;
        public ImageView imgvProfile;
        public TextView tvReply;
        public TextView tvLikeCount;
        public TextView tvTime;
        public TextView tvReplyLike;
        public LinearLayout llItem;
        public ImageView iv_reply;
        public TextView tv_reply_reply;

        public ViewHolder(View v) {

            super(v);

            tvId = (TextView) v.findViewById(R.id.tv_id);
            tvReply = (TextView) v.findViewById(R.id.tv_reply);
            tvLikeCount = (TextView) v.findViewById(R.id.tv_like_count);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            imgvProfile = (ImageView) v.findViewById(R.id.imgv_profile);
            iv_reply = (ImageView) v.findViewById(R.id.iv_reply);
            tv_reply_reply = (TextView) v.findViewById(R.id.tv_reply_reply);

            tvReplyLike = (TextView) v.findViewById(R.id.tv_reply_like);


        }
    }
    private int getDrawableResourceByName(String str) {
        Emoticon_Ko emoticon_ko = new Emoticon_Ko();
        String emoticon = emoticon_ko.Emoticon_Ko(mContext,str);
        String packageName = mContext.getPackageName();
        return mContext.getResources().getIdentifier(emoticon, "drawable", packageName);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(mContext).inflate(R.layout.item_detail_reply, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

        //Log.e("ReplyRecyclerAdapter", "onBindViewHolder: " + arVoReadReply.get(position).getMsg() + " " + arVoReadReply.get(position).getBbsOrd());
        Vholder.tvId.setText(arVoReadReply.get(position).getUsNm());
        //Vholder.imgvProfile

        final ArrayList<VoMsgItem> arMsgItem = Util.ParseMsg(arVoReadReply.get(position).getMsg());


        for (final VoMsgItem item : arMsgItem) {
            //Log.e(TAG, "onBindViewHolder : " + item.getData());

            switch (item.getType()) {
                case "0": //text
                    Log.e(TAG, "onBindViewHolder  TEXT: " + item.getData());
                    Vholder.tvReply.setVisibility(View.VISIBLE);
                    Vholder.tvReply.setText(item.getData().toString().trim().replace("&nbsp;",""));

                    Vholder.tvReply.setTextSize(SharedObject.getProperty_int(mContext, Constants.FONTSIZE, 14));

                    //Vholder.iv_reply.setVisibility(View.GONE);

                    //이모티콘과 이미지가 없을때 처리.
                    //arMsgItem에서 이미지가 없는지 체크.
                    if(isArMsgItemHaveImg(arMsgItem)){
                        Vholder.iv_reply.setVisibility(View.VISIBLE);
                    }else{
                        Vholder.iv_reply.setVisibility(View.GONE);
                    }

                    break;

                case "1":  //image
                    Vholder.iv_reply.setVisibility(View.VISIBLE);

                    if(TextUtils.isEmpty((Vholder.tvReply).getText().toString().trim())){
                        Vholder.tvReply.setVisibility(View.GONE);
                    }

                    Glide.with(mContext)
                            .load(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData()).diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(true)
                            //.placeholder(R.drawable.ic_list_friend_profile_160x160)
                            .dontAnimate().into(Vholder.iv_reply);

                    final ArrayList pictureList= new ArrayList();
                    pictureList.add(new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData() , item.getIdx()));
                    //댓글 미리보기
                    Vholder.iv_reply.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            AlbumPicture picture = null;
                            picture= new AlbumPicture(Constants.MOIM_TIMELINE_LIST_IMAGE_URL + item.getData(), item.getIdx());
                            Intent intent = new Intent(mContext, Activity_PreviewImage.class);
                            intent.putExtra("picture", picture);
                            intent.putExtra("pictureList", (Serializable) pictureList);
                            intent.putExtra("type", "moim");
                            mContext.startActivity(intent);

                        }
                    });
                    //

                    break;

                case "4":   //이모티콘

                    if(TextUtils.isEmpty((Vholder.tvReply).getText().toString().trim())){
                        Vholder.tvReply.setVisibility(View.GONE);
                     }

                    Vholder.iv_reply.setVisibility(View.VISIBLE);
                    Vholder.iv_reply.setImageResource(getDrawableResourceByName(item.getData()));
                    //Log.i("TEST", "data : " + item.getData());
                    break;
            }
        }





        Vholder.tvLikeCount.setText(arVoReadReply.get(position).getLkCnt());
        Vholder.tvTime.setText(arVoReadReply.get(position).getRegDt());

        if (arVoReadReply.get(position).getBbsDph().equals("1")) {  //첫번째 댓글
            Util.setMargins(Vholder.llItem, 10, 0, 0, 0);
            Vholder.tv_reply_reply.setVisibility(View.GONE);//덧글삭제

        } else if (arVoReadReply.get(position).getBbsDph().equals("2")) {   //두번째 덧글
            Util.setMargins(Vholder.llItem, 40, 0, 0, 0);
            Vholder.tv_reply_reply.setVisibility(View.GONE);

        } else if (arVoReadReply.get(position).getBbsDph().equals("3")) {   //세번째 덧글  사용안함..
            Util.setMargins(Vholder.llItem, 70, 0, 0, 0);
            Vholder.tv_reply_reply.setVisibility(View.GONE);
        }

        //좋아요 표시
        switch (arVoReadReply.get(position).getEmtStt()){
            case "" :   //감정없음
                Vholder.tvReplyLike.setText(R.string.like);
                break;

            case "1" :  //좋아요
                Vholder.tvReplyLike.setText(R.string.like_cancle); //좋아요 취소
                //moimEmotionRequest(arvalues.get(position).getMmId(),arvalues.get(position).getMsgId(),"0");//좋아요 취소
                break;

        }


        //좋아요
        Vholder.tvReplyLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (arVoReadReply.get(position).getEmtStt()){
                    case "" :   //감정없음
                        moimEmotionRequest(arVoReadReply.get(position).getMmId(),arVoReadReply.get(position).getMsgId(),"1");//좋아요
                        break;

                    case "1" :  //좋아요
                        moimEmotionRequest(arVoReadReply.get(position).getMmId(),arVoReadReply.get(position).getMsgId(),"0");//좋아요 취소
                        break;
                }
            }
        });


        Vholder.llItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                boolean isLeader;

                if (Util.getPermissionLv(Util.getMymLv(mContext), Util.getDelTy(mContext))) {
                    isLeader = true;
                } else {
                    isLeader = false;
                }

                //final Dialog_Reply dialog_reply = new Dialog_Reply(mContext, Util.isOwner(mContext, arVoReadReply.get(position).getRegUsr()) == true, isLeader);

                final Dialog_Reply dialog_reply = new Dialog_Reply(mContext, arVoReadReply.get(position));

                //if (Util.isOwner(mContext, arVoReadReply.get(position).getRegUsr()) == true) {
                    dialog_reply.show();
                    dialog_reply.setDeleteListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            moimBoardDeleteRequest(arVoReadReply.get(position).getMsgId(),arVoReadReply.get(position).getMmId());
                            dialog_reply.dismiss();
                        }
                    });

                //댓글수정
                dialog_reply.setModifyListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SharedObject.setProperty_string(mContext, Constants.MOIM_MSG, arVoReadReply.get(position).getMsg());
                        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, arVoReadReply.get(position).getMmId());    //모임 키값
                        SharedObject.setProperty_string(mContext, Constants.MOIM_MODIFY_MSGID, arVoReadReply.get(position).getMsgId());  //메시지 키값


                        Intent intent = new Intent(mContext, Activity_MoimReplyModify.class);
                        ((Activity_TimeLineDetail)mContext).startActivityForResult(intent, Constants.RESULT_REPLY_MODIFY);

                        dialog_reply.dismiss();
                    }
                });


                //댓글복사
                dialog_reply.setClipboardListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Util.copyToClipBoard(mContext, arMsgItem.get(0).getData());
                        Toast.makeText(mContext, "클립보드에 복사하였습니다.", Toast.LENGTH_SHORT).show();
                        dialog_reply.dismiss();
                    }
                });
                dialog_reply.setReportListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedObject.setProperty_string(mContext, Constants.MOIM_ID, arVoReadReply.get(position).getMmId());        //모임 키값
                        SharedObject.setProperty_string(mContext, Constants.MOIM_MSGID, arVoReadReply.get(position).getMsgId());    //메시지 아이디
                        SharedObject.setProperty_string(mContext, Constants.MOIM_REGUSER, arVoReadReply.get(position).getRegUsr()); //작성자
                        SharedObject.setProperty_string(mContext, Constants.MOIM_TTL, arMsgItem.get(0).getData()); //첫줄

                        Intent intent = new Intent(mContext, Activity_MoimContentReport.class);
                        mContext.startActivity(intent);

                        //Toast.makeText(mContext, "클립보드에 복사하였습니다.", Toast.LENGTH_SHORT).show();
                        dialog_reply.dismiss();
                    }
                });



                return false;
            }
        });

        //대댓글 달기
        Vholder.tv_reply_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentTimeLineDetail(arVoReadReply.get(position), true);


            }
        });

        //프로필
        /*Util.setMoimProfileGlide(mContext
                , SharedObject.getProperty_string(mContext, Constants.MOIM_ID, null)
                , arVoReadReply.get(position).getPfImg()
                , Vholder.imgvProfile
        );*/

        Util.setMoimProfileGlide2(mContext, arVoReadReply.get(position).getRegUsr(), Vholder.imgvProfile);








        //프로필 화면이동
        Vholder.imgvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentmProfile(arVoReadReply.get(position).getRegUsr(),arVoReadReply.get(position).getMmId());
            }
        });

    }

    /**
     * ArrayList 에서 이미지가 포함되어있으면 true
     * @param arMsgItem
     */
    private boolean isArMsgItemHaveImg(ArrayList<VoMsgItem> arMsgItem) {
        for (VoMsgItem VoMsgItem : arMsgItem){
            if(VoMsgItem.getType().equals("1") || VoMsgItem.getType().equals("4")){
                Log.e(TAG,"isArMsgItemHaveImg return true");
                return true;
            }
        }

        Log.e(TAG,"isArMsgItemHaveImg return false");
        return false;
    }


    /**
     * 프로필화면
     * @param
     * @param
     */

    private void intentmProfile(String regUsr, String mmid) {
        Log.e(TAG,"intentmProfile regUsr:"+ regUsr+" mmid: "+mmid);
        SharedObject.setProperty_string(mContext, Constants.MOIM_USER_PROFILE_ID, regUsr);    //글등록자
        SharedObject.setProperty_string(mContext, Constants.MOIM_ID,mmid);  //모임아이디


        //TODO 모임정보 추가
        Intent intent = new Intent(mContext, Activity_MoimProfile.class);

        mContext. startActivity(intent);
        //((Activity_MoimTimeLineList)mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }


    private void intentTimeLineDetail(VoBoardRead.VoReadReply voBoardItem, boolean reply) {
        {

            SharedObject.setProperty_string(mContext, Constants.MOIM_RE_MSGID, voBoardItem.getMsgId());
            SharedObject.setProperty_string(mContext, Constants.MOIM_RE_ID, voBoardItem.getMmId());
            SharedObject.setProperty_boolean(mContext, Constants.MOIM_REPLY, reply);

            Intent intent = new Intent(mContext, Activity_Re_Reply.class);
            intent.putExtra("rereply", voBoardItem);
            mContext.startActivity(intent);
        }

    }

    @Override
    public int getItemCount() {

        return arVoReadReply.size();
    }

    private void moimBoardDeleteRequest(String msgId,String mmid) {
        Moa.moimBoardDelete(mContext, msgId, mmid, mMainActvtHandler);

    }

    //결과 처리 핸들러
    private Handler mMainActvtHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {

                MOAData data = (MOAData) msg.obj;

                String strResult;

                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_BOARD_DELETE:    //모임 삭제
                        strResult = (String) data.body.get("result");

                        if (strResult.equals(Constants.SUCCESS)) {
                            Toast.makeText(mContext, "댓글 삭제 완료", Toast.LENGTH_SHORT).show();

                            String msgId = data.body.getJson("params").getString("msgId");


                            for (int i = 0; i < arVoReadReply.size(); i++) {
                                if (msgId.equals(arVoReadReply.get(i).getMsgId())) {
                                    removeAt(i);
                                    break;
                                }
                            }

                            if(Activity_MoimTimeLineList.activity!=null){
                                Activity_MoimTimeLineList.activity.refreshItems();
                            }
                            if (Fragment_MoimTab1_Timeline.instance != null) {
                                Fragment_MoimTab1_Timeline.instance.refreshItems();
                            }


                        } else {
                            Toast.makeText(mContext, "댓글삭제 실패", Toast.LENGTH_SHORT).show();

                        }
                        break;


                    case PacketTypes.PTC_IMS_MOIM_EMOTION :    //7.23 모임 글 감정표현
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);

                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());


                            String msgId = data.body.getJson("params").getString("msgId");
                            String emt = data.body.getJson("params").getString("emt");


                            Log.e(TAG,"Result  msgId: "+msgId);

                            for (int i = 0; i < arVoReadReply.size(); i++) {
                                if(msgId.equals(arVoReadReply.get(i).getMsgId())){

                                    if(emt.equals("0")){
                                        arVoReadReply.get(i).setEmtStt("");
                                        arVoReadReply.get(i).setLkCnt((Integer.parseInt(arVoReadReply.get(i).getLkCnt())-1)+"");  //좋아요 내리기
                                        Toast.makeText(mContext, "좋아요 취소", Toast.LENGTH_SHORT).show();
                                    }else{
                                        arVoReadReply.get(i).setEmtStt("1");
                                        arVoReadReply.get(i).setLkCnt((Integer.parseInt(arVoReadReply.get(i).getLkCnt())+1)+"");  //좋아요 올리기
                                        Toast.makeText(mContext, "좋아요 완료", Toast.LENGTH_SHORT).show();
                                    }

                                    break;
                                }
                            }
                            //arvalues.get(mnLikeNum).setLkCnt((Integer.parseInt(arvalues.get(mnLikeNum).getLkCnt())+1)+"");
                            //notifyDataSetChanged();

                            if(mContext instanceof Activity_TimeLineDetail){
                                ((Activity_TimeLineDetail)mContext).reFreshAdapter();
                            }else {
                                Fragment_MoimTab1_Timeline.instance.reFreshAdapter();
                            }

                            //Fragment_MoimTab1_Timeline.fragment.reFreshAdapter();


                        }else{
                            Toast.makeText(mContext, "좋아요 실패", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 롱크릭 삭제하기 결과 처리.
     * @param position
     */
    public void removeAt(int position) {

        arVoReadReply.remove(position);
        ((Activity_TimeLineDetail) mContext).RemoveAdapter(position, arVoReadReply.size());
    }

    /**
     * 7.23 모임 글 감정표현

     * @param mmId
     * @param msgId
     * @param emt  감정표현(int) 0:기존감정표현취소, 1:좋아요, 2:싫어요
     */
    private void moimEmotionRequest( String mmId, String msgId, String emt) {
        Moa.moimEmotionRequest(mContext, mmId, msgId, emt, mMainActvtHandler);
    }

}