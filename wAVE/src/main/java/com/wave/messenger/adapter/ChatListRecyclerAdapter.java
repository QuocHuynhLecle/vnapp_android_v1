package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wave.massenger.piggy.R;
import com.wave.messenger.viewholder.ChatListHolder;
import com.wave.messenger.vo.VoChatList;

import java.util.ArrayList;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class ChatListRecyclerAdapter extends RecyclerView.Adapter<ChatListHolder> {

    Context mContext;
    private ArrayList<VoChatList> chatLists;
    LayoutInflater mInflater;

    public ChatListRecyclerAdapter(Context context, ArrayList<VoChatList> list) {
        super();
        mContext = context;
        chatLists = list;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList<VoChatList> list) {
        chatLists = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public ChatListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_chatlist, parent, false);

        return new ChatListHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatListHolder holder, int position) {
        if(position == getItemCount() - 1) {
            holder.ll_chatline.setVisibility(View.INVISIBLE);
        }

        holder.tv_roomname.setText(chatLists.get(position).getRoom_name());
        holder.tv_thumb.setText(chatLists.get(position).getTitle());
        holder.tv_date.setText(chatLists.get(position).getLast_msg_date());
    }

    @Override
    public int getItemCount() {
        return chatLists.size();
    }
}
