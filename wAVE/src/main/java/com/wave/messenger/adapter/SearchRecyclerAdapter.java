package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.vo.VoStockItem;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 4. 10..
 *
 * 모임 타임라인 검색 어댑터
 */
public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.ViewHolder>{

    ArrayList<VoStockItem> arvalues;
    Context context;

    public SearchRecyclerAdapter(Context con, ArrayList<VoStockItem> values){
        arvalues = values;
        context = con;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvStock;
        public TextView tvNumber;
        public TextView tvCount;
        public LinearLayout llItem;

        public ViewHolder(View v){

            super(v);

            tvStock = (TextView) v.findViewById(R.id.tv_stock);
            tvNumber = (TextView) v.findViewById(R.id.tv_number);
            tvCount = (TextView) v.findViewById(R.id.tv_count);


            llItem =(LinearLayout) v.findViewById(R.id.ll_item);
        }
    }

    @Override
    public SearchRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View view1 = LayoutInflater.from(context).inflate(R.layout.item_stock,parent,false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, int position){

        Vholder.tvStock.setText(arvalues.get(position).getStock());
        Vholder.tvNumber.setText(arvalues.get(position).getNumber());
        Vholder.tvCount.setText(arvalues.get(position).getCounter());




/*        Vholder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(context, Activity_MoimTimeLineList.class);
                context.startActivity(mIntent);
            }
        });*/

    }

    @Override
    public int getItemCount(){

        return arvalues.size();
    }
}