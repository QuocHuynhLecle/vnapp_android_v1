package com.wave.messenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.Cell_Friend_ListTitle;
import com.wave.messenger.cell.FriendHolder;
import com.wave.messenger.mvp.Main.InvitePresenter;
import com.wave.messenger.util.CircleImageView;
import com.wave.messenger.util.Constants;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;


/**
 * Created by aveapp on 2017. 8. 31..
 */

public class FriendInviteListViewAdapter extends BaseExpandableListAdapter {

    private static final int CHILD_TYPE_1 = 0;
    private Context context;
    private ArrayList<String> groupList;
    private ArrayList<VoFriendList> childList;

    private boolean isInvite;


    public FriendInviteListViewAdapter(Context context, ArrayList<String> groupList, ArrayList<VoFriendList> childList, InvitePresenter presenter, boolean flag) {
        this.context = context;
        this.groupList = groupList;
        this.childList = childList;
        this.isInvite = flag;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).getList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childList.get(groupPosition).getList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getChildTypeCount() {
        return 2;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
        int result = 0;
        if (groupPosition == 1) {
            if (!item.isPro) {
                result = CHILD_TYPE_1;
            }
        }
        return result;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Cell_Friend_ListTitle itemView;
        itemView = new Cell_Friend_ListTitle(context);
        if (getChildrenCount(groupPosition) > 0)
            itemView.setTextViewTitle(groupList.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")");


        switch (groupList.get(groupPosition)) {
            case Constants.LISTTYPE_MYPROFILE:
                itemView.hideMoreLayout();
                itemView.hideGroupLayout();
                break;
            case Constants.LISTTYPE_PROFESSIONALFRIEND:
                itemView.hideGroupLayout();
                break;
            case Constants.LISTTYPE_MYFRIEND:
                itemView.hideMoreLayout();
                itemView.hideGroupLayout();
                break;
            default:
                if (getChildrenCount(groupPosition) > 0)
                    itemView.hideMoreLayout();
                else {
                    itemView.hideMoreLayout();
                    itemView.hideGroupLayout();
                }

        }

        /** ActivityInviteFriend에서 호출 되었을 경우*/
        if (isInvite) {
            if (groupList.get(groupPosition).equals(Constants.LISTTYPE_PROFESSIONALFRIEND)) {
                itemView.hideLayout();
            }
            if (childList.get(groupPosition).getList().size() == 0) {
                itemView.hideLayout();
            }
        }

        return itemView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final VoFriendList item = childList.get(groupPosition).getList().get(childPosition);
        LayoutInflater mInflater;
        FriendHolder holder;

        if (convertView == null) {
            holder = new FriendHolder();
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.cell_friendlist, null);

            holder.ll_friend = (LinearLayout) convertView.findViewById(R.id.ll_friend);
            holder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);
            holder.iv_profile = (CircleImageView) convertView.findViewById(R.id.imageViewTitle);
            holder.tv_name = (TextView) convertView.findViewById(R.id.textViewName);
            holder.vLiner = convertView.findViewById(R.id.vLiner);

            convertView.setTag(holder);
        } else {
            holder = (FriendHolder) convertView.getTag();
        }
        String url;

        url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();

        if (childList.get(groupPosition).getList().size() == childPosition + 1) {
            holder.vLiner.setVisibility(View.GONE);
        } else {
            holder.vLiner.setVisibility(View.VISIBLE);
        }

        if (groupList.get(groupPosition).equals(Constants.LISTTYPE_MYFRIEND)) {
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_profile);
            holder.tv_name.setText(item.getUserName());

        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
