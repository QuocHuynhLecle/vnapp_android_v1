package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.mvp.Profile.DataModel.ThumbnailBitmap;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoUsers;

import java.io.BufferedInputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 2016-09-18.
 */
public class DeliveryChatListAdapter extends BaseAdapter {

    private List<VoChatList> data = Collections.emptyList();
    private Context context;

    private List<VoChatList> cachedData = null;

    // Constructor
    public DeliveryChatListAdapter(List<VoChatList> data, Context context) {

        this.data = data;
        this.context=context;

        for(VoChatList voChatList : data){
            ErrorController.showMessage("Check RoomID : " + voChatList.getRoomId());
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public void setData(List<VoChatList> data){
        this.data = null;
        this.data = data;

    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(int position) {
        data.remove(position);
    }

    public void showPartialSearchedList(List<String> roomIds) {

        ErrorController.showMessage("[ChatListViewAdapter] showPartialSearchedList called");

        if(cachedData!=null){
            data.clear();
            for(VoChatList cData : cachedData){
                data.add(cData);
            }
        }

        List<VoChatList> searchedList = new ArrayList<>();

        for (String ids : roomIds) {
            VoChatList room = getDataFromId(ids);
            if (room != null) {
                searchedList.add(room);
            }
        }

        //검색 리스트와 전체 리스트를 스왑
        cachedData = new ArrayList<>();
        for(VoChatList totalData : data){
            cachedData.add(totalData);
        }

        data.clear();
        for(VoChatList searchData : searchedList){
            data.add(searchData);
        }
        ErrorController.showMessage("Check data size : " + data.size());
        notifyDataSetChanged();
    }


    public void resetSearch(){
        if(cachedData!=null){
            data.clear();
            for(VoChatList cData : cachedData){
                data.add(cData);
            }
            cachedData = null;
            notifyDataSetChanged();
        }
    }


    private List<VoChatList> getNonSearchedList(List<String> roomIds) {
        List<VoChatList> result = new ArrayList<>();

        for (VoChatList room : data) {
            boolean isIncluded = false;
            for (String id : roomIds) {
                if (room.getRuid().equals(id)) {
                    isIncluded = true;
                    break;
                }
            }

            if( !isIncluded && !result.contains(room)){
                result.add(room);
            }
        }
        return result;
    }

    public VoChatList getDataFromId(String id) {
        for (VoChatList room : data) {

            ErrorController.showMessage("[idcheck] id : " + id + ", Room ID : " + room.getRoomId());
            if (room.getRoomId().equals(id)) {
                ErrorController.showMessage("[getDataFromId] id equals room ID");
                return room;
            }
        }
        return null;
    }

    public List<VoChatList> getPartialList(List<String> roomIds) {

        List<VoChatList> result = new ArrayList<>();

        for (VoChatList room : data) {
            for (String roomId : roomIds) {
                if (room.getRoomId().equals(roomId) && !result.contains(room)) {
                    ErrorController.showMessage("[ChatListViewAdapter] getPartialList : " + room.getRoomId());
                    result.add(room);
                }
            }
        }
        return result;
    }

    public List<VoChatList> getList(){
        return data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChatListViewHolder holder;
        if (convertView == null) {
            holder = new ChatListViewHolder();
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cell_chatlist, null);

            holder.iv_cell_frag_frag_chat_user_image = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_user_image);
            //holder.iv_cell_frag_frag_chat_hana_members = (ImageView) convertView.findViewById(R.id.iv_cell_frag_frag_chat_hana_members);
            holder.tv_cell_frag_frag_chat_user_name = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_name);
            holder.tv_cell_frag_frag_chat_user_contents = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_user_contents);
            holder.tv_cell_frag_frag_chat_date = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_date);
            holder.tv_cell_frag_frag_chat_msg_count = (TextView) convertView.findViewById(R.id.tv_cell_frag_frag_chat_msg_count);
            holder.ivSelection = (ImageView) convertView.findViewById(R.id.ivSelection);
            holder.tvMultiUserCount = (TextView) convertView.findViewById(R.id.tvMultiUserCount);
            holder.ivMultiUserBell = (ImageView) convertView.findViewById(R.id.ivMultiUserBell);
            convertView.setTag(holder);
        } else {
            holder = (ChatListViewHolder) convertView.getTag();
        }

        VoChatList model = data.get(position);

        // TODO : 이미지 세팅
        if("0".equals(model.getRoom_type())){
            UsersDbHelper db = LocalDB.getUsersDbHelper(context);
            ArrayList<VoUsers> list = db.getUserList(model.getRoomId());
            VoUsers item = new VoUsers();

            for(int i=0;i<list.size();i++){
                if(!MessengerInfo.getUserId(context).equals(list.get(i).getUserId())) {
                    item = list.get(i);
                }
            }
            try {
                String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();
                Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);
            }catch (Exception e){
                holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_default_profile);
            }
        } else if("1".equals(model.getRoom_type())) {
            holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_main_group);
        } else if("2".equals(model.getRoom_type()) || "3".equals(model.getRoom_type())) {
            String url = Constants.CHATDAWN_PROC + "?type=7&serverfile=" + model.getRoom_profile_image();
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_go_meet).dontAnimate().into(holder.iv_cell_frag_frag_chat_user_image);
        } else {
            holder.iv_cell_frag_frag_chat_user_image.setImageResource(R.drawable.ic_default_profile);
        }

        if("4".equals(model.getRoom_type())) {
            holder.tv_cell_frag_frag_chat_user_name.setText(MessengerInfo.getUserName(context));
        }else {
            holder.tv_cell_frag_frag_chat_user_name.setText(model.getRoom_name());
        }

        holder.tv_cell_frag_frag_chat_user_contents.setText(model.getTitle());

        if(!isToday(model.getLast_msg_date())){
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedDate(model.getLast_msg_date()));
        }else{
            holder.tv_cell_frag_frag_chat_date.setText(getFormattedTime(model.getLast_msg_date()));
        }

        if("0".equals(model.getUnread())||TextUtils.isEmpty(model.getUnread())){
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.GONE);
        }else{
            holder.tv_cell_frag_frag_chat_msg_count.setVisibility(View.VISIBLE);
            holder.tv_cell_frag_frag_chat_msg_count.setText(model.getUnread() + "");
        }
//		// 멀티 유저 전용 ui세팅
        if (!"2".equals(model.getUsercount())&&!"1".equals(model.getUsercount())&&!"0".equals(model.getUsercount())) {
            // 방에 친구가 둘 이상이면...
            holder.tvMultiUserCount.setText(model.getUsercount());
            holder.tvMultiUserCount.setVisibility(View.VISIBLE);
        } else {
            // 방에 친구가 한명 이하면...
            holder.tvMultiUserCount.setVisibility(View.GONE);
        }

        if(model.getUseNoti().equals("0")) {
            holder.ivMultiUserBell.setVisibility(View.VISIBLE);
        } else {
            holder.ivMultiUserBell.setVisibility(View.GONE);
        }

        holder.tv_cell_frag_frag_chat_date.setVisibility(View.GONE);
        //채팅 목록 편집 - 선택/비선택
        if(model.isSelected()){
            //선택됨
            holder.ivSelection.setSelected(true);
            holder.ivSelection.setVisibility(View.VISIBLE);
        }else{
            //선택되지 않음.
            holder.ivSelection.setSelected(false);
            holder.ivSelection.setVisibility(View.GONE);
        }
        return convertView;
    }

    public boolean isToday(String time) {
        boolean result=false;
        try{
            SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd" );
            long now = System.currentTimeMillis();
            Date date = new Date(now);
            String today= format.format(date);
            Date today1=format.parse(today);
            Date day1 = format.parse(time);
            int comareTo=day1.compareTo(today1);
            if(comareTo<0){
                result=false;
            }else{
                result=true;
            }
        }catch(Exception e) {

        }
        return result;

    }

    public String getFormattedDate(String time) {
        String result = "";
        String temp1;
        try {
            temp1=time.substring(0,10);
            if (time != null) {
                String[] temp = temp1.split("-");


                result = temp[0] + "년 " + temp[1] + "월 " + temp[2] + "일";

            }
        }catch (Exception e){
            ErrorController.showMessage("[ChatLIstViewAdapter] getFormattedDate error occurred");
        }

        return result;
    }

    public String getFormattedTime(String regDate){
        String result = "";
        try{
            if(regDate!=null && !TextUtils.isEmpty(regDate)){
                String [] temp = regDate.split(" ");
                String time = temp[1];

                String [] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute =  Integer.parseInt(timeTemp[1]);
                String hourString ="";
                String minuteString ="";
                if(hour > 12){
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour+"";
                    if(hour <10)
                        hourString = "0"+hour;
                }else{
                    noonText = "오전";
                    hourString = hour+"";
                    if(hour <10)
                        hourString = "0"+hour;
                }

                minuteString = minute+"";
                if(minute<10){
                    minuteString = "0" + minute;
                }


                result = noonText + " " + hourString + ":" + minuteString;
            }else{
                result = "오전 00:00";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public void clear() {
        this.data.clear();
    }

    // ViewHolderItem Class
    private class ChatListViewHolder {
        public ImageView iv_cell_frag_frag_chat_user_image,
                iv_cell_frag_frag_chat_hana_members, ivSelection;
        public TextView tv_cell_frag_frag_chat_user_name,
                tv_cell_frag_frag_chat_user_contents,
                tv_cell_frag_frag_chat_date, tv_cell_frag_frag_chat_msg_count;
        public TextView tvMultiUserCount;
        public ImageView ivMultiUserBell;
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        private String address = Constants.CHATDAWN_PROC + "?";
        private final WeakReference<ImageView> imageViewReference;

        public ImageDownloader(ImageView imageView) {
            imageViewReference = new WeakReference<>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                String fullAddress = address + "type=" + params[0] + "&userid="
                        + params[1];
                ErrorController.showMessage("[Cell_Friend_list ImageDownloader] FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                Statics.getThumbnailMap().put(params[1], new ThumbnailBitmap(image, false));
                ErrorController.showMessage("[Cell_Friend_list ImageDownloader]end of doinBg");
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[Cell_Friend_list ImageDownloader] No Image in server to Download");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (imageViewReference != null && result != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(result);
                }
            }
        }//end of onPostExecute
    }//end of innerclass


}
