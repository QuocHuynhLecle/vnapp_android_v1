package com.wave.messenger.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.activity.Activity_CustomerList;
import com.wave.messenger.activity.Activity_Profile;
import com.wave.massenger.piggy.R;
import com.wave.messenger.db.ChatListDbHelper;
import com.wave.messenger.db.LocalDB;
import com.wave.messenger.db.UsersDbHelper;
import com.wave.messenger.helper.FragmentEventHelper;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.BusProvider;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.viewholder.CustomerListHolder;
import com.wave.messenger.vo.VoChatList;
import com.wave.messenger.vo.VoFriendList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aveapp on 2017. 4. 19..
 */

public class CustomerListRecyclerAdapter extends RecyclerView.Adapter<CustomerListHolder> {

    Context mContext;
    private ArrayList<VoFriendList> friendLists;
    LayoutInflater mInflater;

    public CustomerListRecyclerAdapter(Context context, ArrayList<VoFriendList> list) {
        super();
        mContext = context;
        friendLists = list;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList<VoFriendList> list) {
        friendLists = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public CustomerListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.cell_friendlist, parent, false);

        return new CustomerListHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomerListHolder holder, final int position) {
        final VoFriendList item = friendLists.get(position);
        holder.textViewName.setText(friendLists.get(position).getUserName());
        String url = Constants.CHATDAWN_PROC + "?type=3&userid=" + item.getUserId();
        Glide.with(mContext).load(url).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.imageViewTitle);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatListDbHelper listDb = LocalDB.getChatListDbHelper(mContext);
                List<VoFriendList> friends = new ArrayList<>();
                VoChatList chatRoomInfo = new VoChatList();
                String roomId;

                if (item.getUserId().equals(MessengerInfo.getUserId(mContext))) {
                    roomId = listDb.selectMyRoom();
                    chatRoomInfo.setRoom_type("4");
                    friends.add(item);
                } else {
                    UsersDbHelper userDb = LocalDB.getUsersDbHelper(mContext);
                    ArrayList<String> roomIdList = listDb.selectSingleRoom();
                    roomId = userDb.existUserCaht(roomIdList, item.getUserId());
                    chatRoomInfo.setRoom_type("0");
                    VoFriendList my = new VoFriendList();
                    my.setUserId(MessengerInfo.getUserId(mContext));
                    my.setUserName(MessengerInfo.getUserName(mContext));
                    my.setRealUserName(MessengerInfo.getRealUserName(mContext));
                    friends.add(my);
                    friends.add(item);
                }

                chatRoomInfo.setFriends(friends, mContext);

                if (!TextUtils.isEmpty(roomId)) {
                    chatRoomInfo.setRoomId(roomId);
                }
                Statics.ROOMINFO = chatRoomInfo;

                BusProvider.getInstance().post(new FragmentEventHelper("chatListUpdate", null, null));

                Intent mIntent = new Intent(mContext, Activity_ChatRoom.class);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity(mIntent);
                Activity_CustomerList.activity.finish();
            }
        });

        holder.imageViewTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Bundle bundle = new Bundle();
                bundle.putSerializable("friendInfo", item);

                if (item.getUserId().equals(MessengerInfo.getUserId(mContext))) {
                    bundle.putBoolean("isEdit", true);
                }
                else {
                    bundle.putBoolean("isEdit", false);
                }

                bundle.putBoolean("isShow", false);
                Intent mIntent = new Intent(mContext, Activity_Profile.class);
                mIntent.putExtras(bundle);
                mContext.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return friendLists.size();
    }
}
