package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.RadiusImageView;
import com.wave.messenger.util.Util;
import com.wave.messenger.vo.VoMoimHiddenList;

import java.util.ArrayList;

/**
 * Created by yunsu on 2017. 3. 15..
 * 모임관리 고정안함 리스트 어댑터
 * &
 * 숨김모임관리-숨김모임 어댑터, 노출중인 모임어댑터
 */
public class MoimManagePixRecyclerAdapter extends RecyclerView.Adapter<MoimManagePixRecyclerAdapter.ViewHolder> {

    String TAG=getClass().getSimpleName();
    //ArrayList<VoMoim> arvalues;
    ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> arvalues= new ArrayList<>();
    Context context;
    IonManageEventListener onManageEventListener;
    Constants.LIST_TYPE listType;

    public MoimManagePixRecyclerAdapter(Context con, ArrayList<VoMoimHiddenList.VoMoimHiddenListItem> values, IonManageEventListener onManageEventListener, Constants.LIST_TYPE listType) {
        this.arvalues = values;
        this.context = con;
        this.onManageEventListener = onManageEventListener;
        this.listType = listType;

        //Log.e(TAG,"values : "+values.size());

        for (VoMoimHiddenList.VoMoimHiddenListItem item:arvalues){
            Log.e(TAG,"MoimManagePixRecyclerAdapter getMmTy: "+item.getMmTy()+" getMmNm: "+item.getMmNm()+" MmId: "+item.getMmId()+" ImgTy: "+item.getImgTy()+" Hdn: "+item.getHdn()+" "+item.getMmTg());
        }


    }

    /*public MoimManagePixRecyclerAdapter(Context con, ArrayList<VoMoim> arVoMoim1, IonManageEventListener onManageEventListener1, Constants.LIST_TYPE listType) {
        this.arvalues = arVoMoim1;
        this.context = con;
        this.onManageEventListener = onManageEventListener1;
        this.listType = listType;
    }*/

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public RadiusImageView imgThum;
        public ImageView imgThumIcon;

        public LinearLayout llItem;
        public ImageView imgvLine;


        public Button btnPix;

        public ViewHolder(View v) {

            super(v);
            textView = (TextView) v.findViewById(R.id.tv_moim_title);
            imgThum=(RadiusImageView) v.findViewById(R.id.imgv_thum_img);
            imgThumIcon=(ImageView) v.findViewById(R.id.imgv_thum_icon);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);
            btnPix = (Button) v.findViewById(R.id.btn_pix);
            imgvLine = (ImageView) v.findViewById(R.id.imgv_line);

        }
    }

    @Override
    public MoimManagePixRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context).inflate(R.layout.item_moim_manage_pix, parent, false);


        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }


    public void removeAt(int position) {
        onManageEventListener.onClearEvent(position);
        //arvalues.remove(position);
        //notifyItemRemoved(position);
        //notifyItemRangeChanged(position, arvalues.size());
    }


    @Override
    public void onBindViewHolder(final ViewHolder Vholder, final int position) {
        Log.e(TAG,"onBindViewHolder : position "+position);


        if (listType == Constants.LIST_TYPE.nopix) {    //모임관리 고정리스트 //해제
            Vholder.btnPix.setText(context.getString(R.string.clear));
            //Vholder.btnPix.setBackgroundResource(R.drawable.ic_take);
            Vholder.btnPix.setBackgroundResource(R.drawable.round_outline_btn_gray_green);
            Vholder.btnPix.setTextColor(Util.getColor(context, R.color.font_green));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dp_43),(int) context.getResources().getDimension(R.dimen.dp_26));

            params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.dp_19), 0);
            Vholder.btnPix.setLayoutParams(params);

        } else if (listType == Constants.LIST_TYPE.pix) { //모임관리 고정안함 //고정
            Vholder.btnPix.setText(context.getString(R.string.pix));
            Vholder.btnPix.setBackgroundResource(R.drawable.round_outline_btn_green);
            Vholder.btnPix.setTextColor(Util.getColor(context, R.color.white));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dp_43),(int) context.getResources().getDimension(R.dimen.dp_26));

            params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.dp_19), 0);
            Vholder.btnPix.setLayoutParams(params);






        } else if (listType == Constants.LIST_TYPE.invisible) {  //숨김리스트
            Vholder.btnPix.setText(context.getString(R.string.pull));

            //Vholder.btnPix.setBackgroundResource(R.drawable.ic_take);
            //Vholder.btnPix.setTextColor(Util.getColor(context, R.color.gray_9e));
            Vholder.btnPix.setBackgroundResource(R.drawable.round_outline_btn_green);
            Vholder.btnPix.setTextColor(Util.getColor(context, R.color.white));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dp_53),
                    (int) context.getResources().getDimension(R.dimen.dp_26));

            params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.dp_19), 0);
            Vholder.btnPix.setLayoutParams(params);


        } else if (listType == Constants.LIST_TYPE.visible) {    //노출리스트
            Vholder.btnPix.setText(context.getString(R.string.hide));

            Vholder.btnPix.setBackgroundResource(R.drawable.round_outline_btn_gray_green);
            Vholder.btnPix.setTextColor(Util.getColor(context, R.color.font_green));


            //Vholder.btnPix.setBackgroundResource(R.drawable.selector_group_set_fix);
            //Vholder.btnPix.setTextColor(Util.getColor(context, R.color.font_green));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dp_53),
                    (int) context.getResources().getDimension(R.dimen.dp_26));

            params.setMargins(0, 0, (int) context.getResources().getDimension(R.dimen.dp_19), 0);
            Vholder.btnPix.setLayoutParams(params);
        }



        //Vholder.textView.setText(arvalues.get(position).getMmNm() + "");
        Vholder.textView.setText(arvalues.get(position).getMmNm() + "");

        if (position == getItemCount() - 1) { //하단 경계 라인
            Vholder.imgvLine.setVisibility(View.GONE);
        } else {
            Vholder.imgvLine.setVisibility(View.VISIBLE);
        }




        //숨김 노출 버튼
        Vholder.btnPix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("PixRecyclerAdapter", "position: " + position + " " + arvalues.get(position).getMmNm());
                removeAt(position);
            }
        });

        if(arvalues.get(position).getImgTy().equals("0")){

            Vholder.imgThumIcon.setVisibility(View.VISIBLE);
            Vholder.imgThum.setVisibility(View.GONE);


            switch (arvalues.get(position).getMmTg()) {
                case "1":   //1 : 주식
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_stock);
                    break;

                case "2":   //2 : 국내파생
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_derivation);
                    break;

                case "3":    //3 : 해외파생
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_derivation);
                    break;

                case "4":   //4 : 해외주식
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_global_stock);
                    break;

                case "5":   //5 : 금융상품
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_bank_product);
                    break;

                case "6":   //6 : 기타
                    Vholder.imgThumIcon.setImageResource(R.drawable.ic_etc);
                    break;
            }

        }else if(arvalues.get(position).getImgTy().equals("1")) {

            Vholder.imgThumIcon.setVisibility(View.GONE);
            Vholder.imgThum.setVisibility(View.VISIBLE);

            Glide.with(context).load(Constants.MOIM_LIST_IMAGE_URL + arvalues.get(position).getImgTmb()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate().into(Vholder.imgThum);      //.placeholder(R.drawable.ic_profile_big_default_android)

        }


    }

    @Override
    public int getItemCount() {

        Log.e(TAG,"getItemCount "+arvalues.size());
        return arvalues.size();
    }
}