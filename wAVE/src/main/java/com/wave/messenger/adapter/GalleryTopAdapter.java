package com.wave.messenger.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.wave.messenger.mvp.Gallery.GalleryDataModel;

import java.util.Collections;
import java.util.List;

public class GalleryTopAdapter extends BaseAdapter{

	private List<GalleryDataModel> data = Collections.emptyList();
	private Context context;
	
	
	public GalleryTopAdapter(List<GalleryDataModel> data, Context context) {
		this.data = data;
		this.context = context;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return data.get(position).getView(context);
	}

}
