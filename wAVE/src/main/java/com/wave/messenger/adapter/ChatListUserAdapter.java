package com.wave.messenger.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.wave.messenger.cell.Cell_Chat_Joined_user;
import com.wave.messenger.vo.VoFriendList;

import java.util.Collections;
import java.util.List;

public class ChatListUserAdapter extends BaseAdapter{

	private List<VoFriendList> data = Collections.emptyList();
	
	public ChatListUserAdapter(List<VoFriendList> data) {
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Cell_Chat_Joined_user view = new Cell_Chat_Joined_user(parent.getContext());
		view.setUserName(data.get(position).getUserName());
		view.setProfile(data.get(position).getUserId());
		/*if(data.get(position).getUserId().contains("$$")){
			view.setMember(false);
		}else{
			view.setMember(true);
		}*/
		
		return view;
	}

}
