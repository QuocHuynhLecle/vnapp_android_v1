package com.wave.messenger.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.massenger.piggy.R;
import com.wave.messenger.cell.OpenUserHolder;
import com.wave.messenger.dialog.Dialog_Notice;
import com.wave.messenger.fragment.ChatRoom.Fragment_OpenNavigation;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.LogTrace;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoFriendList;

import java.util.List;

/**
 * Created by aveapp on 2017. 2. 20..
 */

public class OpenUserAdapter extends RecyclerView.Adapter<OpenUserHolder> {
    Context mContext;
    String url = Constants.CHATDAWN_PROC + "?type=3&userid=";
    List<VoFriendList> userList;
    private Fragment_OpenNavigation.KickListener kickListener;

    public OpenUserAdapter(Context context) {
        mContext = context;
    }

    public void setList(List<VoFriendList> list, Fragment_OpenNavigation.KickListener listener) {
        userList = list;
        kickListener = listener;
        notifyDataSetChanged();
    }

    @Override
    public OpenUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = View.inflate(parent.getContext(), R.layout.cell_openuser, null);
        return new OpenUserHolder(v);
    }

    @Override
    public void onBindViewHolder(OpenUserHolder holder, final int position) {
        LogTrace.E("member url : " + url + userList.get(position).getUserId());

        Glide.with(mContext).load(url + userList.get(position).getUserId()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.ic_default_profile).dontAnimate().into(holder.iv_profile);
        holder.tv_name.setText(userList.get(position).getUserName());
//        holder.tv_id.setText(mContext.getString(R.string.open_user, userList.get(position).getUserId()));

        if("1".equals(userList.get(position).getStaff()) && !Statics.ROOMINFO.getOwnerId().equals(MessengerInfo.getUserId(mContext))) {
            holder.bt_kickuser.setVisibility(View.GONE);
        } else if(Statics.ROOMINFO.getOwnerId().equals(userList.get(position).getUserId())) {
            holder.bt_kickuser.setVisibility(View.GONE);
        } else {
            holder.bt_kickuser.setVisibility(View.GONE);
            holder.bt_kickuser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog_Notice dialog_notice = new Dialog_Notice(mContext);
                    dialog_notice.show();
                    dialog_notice.setData(mContext.getString(R.string.kick_title, Statics.ROOMINFO.getRoom_name()), mContext.getString(R.string.question_kick));
                    dialog_notice.setListener(R.id.bt_ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            kickListener.kick(userList.get(position).getUserId(), userList.get(position).getUserName());
                            dialog_notice.dismiss();
                        }
                    });
                    dialog_notice.setListener(R.id.bt_cancel, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_notice.dismiss();
                        }
                    });
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}
