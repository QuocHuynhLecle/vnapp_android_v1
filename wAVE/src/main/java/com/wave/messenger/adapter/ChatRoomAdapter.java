package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import com.wave.massenger.piggy.R;
import com.wave.messenger.Info.MessengerInfo;
import com.wave.messenger.activity.Activity_ChatRoom;
import com.wave.messenger.cell.chat.CellChatDefaultText;
import com.wave.messenger.cell.chat.CellChatEmoticon;
import com.wave.messenger.cell.chat.CellChatImage;
import com.wave.messenger.cell.chat.CellChatMoimText;
import com.wave.messenger.cell.chat.CellChatOpen;
import com.wave.messenger.cell.chat.CellChatSignal;
import com.wave.messenger.cell.chat.CellChatSystemInviteText;
import com.wave.messenger.cell.chat.CellChatSystemOutText;
import com.wave.messenger.db.UsersReadDbHelper;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoChatData;
import com.wave.messenger.vo.VoUserRead;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatRoomAdapter extends BaseAdapter {

    private List<VoChatData> data = Collections.emptyList();
    private List<VoUserRead> userData = Collections.emptyList();
    private Activity_ChatRoom context;
    private View.OnClickListener profileListener;
    private Activity_ChatRoom.PreViewListener preViewListener;
    private ReSendListener mReSendListener = new ReSendListener() {
        @Override
        public void reSend(String chatId, String cid, String attachment, String status) {
            updateCid(chatId, cid, attachment, status);
            notifyDataSetChanged();
        }

        @Override
        public void deleteMsg(String chatId) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getChatId().equals(chatId)) {
                    data.remove(i);

                    break;
                }
            }

            for (int i = 0; i < context.chatDataList.size(); i++) {
                if (context.chatDataList.get(i).getChatId().equals(chatId)) {
                    context.chatDataList.remove(i);

                    break;
                }
            }

            notifyDataSetChanged();
        }
    };

    public interface ReSendListener {
        void reSend(String chatId, String cid, String attachment, String status);

        void deleteMsg(String chatId);
    }

    // Constructor
    public ChatRoomAdapter(Activity_ChatRoom context) {
        this.context = context;
    }

    public List<VoChatData> getData() {
        return this.data;
    }

    public void setChatListData(List<VoChatData> data) {
//        setUsersData();

        List<VoChatData> filterList = new ArrayList<>();

        if (data != null) {
            for (VoChatData chat : data) {
                filterList.add(chat);
            }
        }

        this.data = filterList;

        Collections.sort(this.data, new Comparator<VoChatData>() {
            @Override
            public int compare(VoChatData lhs, VoChatData rhs) {
                return lhs.getReg_date().compareTo(rhs.getReg_date());
            }
        });
    }

    public void setProfileListener(View.OnClickListener profileListener) {
        this.profileListener = profileListener;
    }

    public void setPreViewListener(Activity_ChatRoom.PreViewListener preViewListener) {
        this.preViewListener = preViewListener;
    }

    public void setUsersData() {
        UsersReadDbHelper db = new UsersReadDbHelper(context);
        userData.clear();
        if (Statics.ROOMINFO != null)
            userData = db.getUsers(Statics.ROOMINFO.getRoomId());
    }

    public void updateStatus(String chatId, String status) {
        for (VoChatData voChatData : data) {
            if (chatId.equals(voChatData.getChatId())) {
                voChatData.setStatus(status);
                break;
            }
        }
    }

    public void updateCid(String chatId, String cid, String attachment, String status) {
        for (VoChatData voChatData : data) {

            if (voChatData.getChatId().equals(chatId)) {
                voChatData.setCid(cid);
                voChatData.setAttachment(attachment);
                voChatData.setStatus(status);
                break;
            }
        }
    }

    public boolean existChatmsg(String chatId) {
        boolean result = false;
        for (VoChatData voChatData : data) {
            if (voChatData.getChatId().equals(chatId)) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VoChatData item = data.get(position);

        try {
            if (position >= data.size() - 1) {
                ErrorController.showMessage("test");
            }

            item.setUnReadCount(unReadCount(item.getCid()));
            ErrorController.showMessage("아이템 CID:" + item.getChatId());
            if (position != data.size() - 1) {
                String nextunread = unReadCount(data.get(position + 1).getCid());
                if (!TextUtils.isEmpty(item.getUnReadCount()) && !TextUtils.isEmpty(nextunread)) {
                    if (Integer.parseInt(item.getUnReadCount()) > Integer.parseInt(nextunread)) {
                        item.setUnReadCount(nextunread);
                    }
                }
            }

            if (position != 0) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
                VoChatData befItem = data.get(position - 1);
                Date day1 = format.parse(item.getReg_date().substring(0, 10));
                Date day2 = format.parse(befItem.getReg_date().substring(0, 10));
                int compare = day1.compareTo(day2);
                if (compare > 0) {
                    item.setShowTimeLine(true);
                } else {
                    item.setShowTimeLine(false);
                }
            } else {
                item.setShowTimeLine(true);
            }

            if ("2".equals(item.getRoomType()) || "3".equals(item.getRoomType())) {
                item.setShowUnRead(false);
            } else {
                item.setShowUnRead(true);
            }

            item.setProfileListener(profileListener);
            item.setPreView(preViewListener);
            item.setListener(mReSendListener);

        } catch (Exception e) {
            ErrorController.showError("ChatRoomAdapter", "Error", e);
        }

        //LogTrace.E("messageType : " + item.getMessageType() + " / " + item.getSenderName() + " / " + item.getMessage());

        View view = getItemView(context, item);

        if (view != null) {
            return view;
        } else {
            return null;
        }

    }

    public View getItemView(Context context, VoChatData item) {
        //LogTrace.E("getChatType : " + item.getChatType());
        if ("1".equals(item.getChatType()) || "3".equals(item.getChatType())) {
            CellChatSystemInviteText view = new CellChatSystemInviteText(context);
            view.setUI(item);
            return view;
        } else if ("2".equals(item.getChatType())) {
            CellChatSystemOutText view = new CellChatSystemOutText(context);
            view.setUI(item);
            return view;
        } else if ("4".equals(item.getChatType())) {
            CellChatOpen view = new CellChatOpen(context, item);
            return view;
        } else {
            return getMessageView(context, item);
        }
    }

    public View getMessageView(Context context, VoChatData item) {
//        Log.e("", "getMessagType = " + item.getMessageType());

        switch (Integer.valueOf(item.getMessageType())) {
            case 0: {// text
                CellChatDefaultText view = new CellChatDefaultText(context, item);
                if (item.isSearched()) {
//					int search_idx = 0;
//					Activity_ChatRoom.getInstance().currentSearchWord.length();
//					String message = item.getMessage();
////					message.contains(Activity_ChatRoom.getInstance().currentSearchWord);
////					for(int i = 0; i < message.contains(Activity_ChatRoom.getInstance().currentSearchWord.toString()))
//							search_idx = message.indexOf(Activity_ChatRoom.getInstance().currentSearchWord);
//
//					SpannableString ssb = new SpannableString(item.getMessage());
//							ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#ffff0000")),search_idx,search_idx + Activity_ChatRoom.getInstance().currentSearchWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//							view.getTextViewContents(ssb);
//
//
                    view.getLinearLayoutContents().startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake_animation));

                    view.getTextViewContents().setTextColor(0xFFFF0000);
                } else {
                    view.getTextViewContents().setTextColor(Color.parseColor("#525252"));
                }
                return view;
            }
            case 1: {// emoticon
                CellChatEmoticon view = new CellChatEmoticon(context, item);
                return view;
            }
            case 2: {// image
                CellChatImage view = new CellChatImage(context, item);
                return view;
//				return  null;
            }
            case 3: {// file
                CellChatDefaultText view = new CellChatDefaultText(context, item);
                return view;
            }
            case 4: {// video
                /*CellChatVideo view = new CellChatVideo(context, item);
				return view;*/
                return null;
            }
            case 5: {// notice
				/*CellChatNotice view = new CellChatNotice(context, item);
				return view;*/
                return null;
            }
            case 6: {// boneyo
				/*CellChatBoneyo view = new CellChatBoneyo(context, item);
				return view;*/
                return null;
            }
            case 7: {// notice
				/*Cell_cancel view = new Cell_cancel(context, item);
				return view;*/
                return null;
            }
            case 8: {// 주세요
				/*CellChatJuseyo view = new CellChatJuseyo(context, item);
				return view;*/
                return null;
            }
            case 9: {// 더치페이
				/*CellChatDucthPay view = new CellChatDucthPay(context, item);
				return view;*/
                return null;
            }
            case 10: {// 추천메세지
                /*CellChatRecommend view = new CellChatRecommend(context, item);*/
                return null;
            }
            case 11: {// 하나 멤버스 메시지
				/*return new CellChatHanaEvent(context, item);*/
                return null;
            }
            case 12: {// 시그널공유
                CellChatSignal view = new CellChatSignal(context, item);
                return view;
            }
            case 13: {// 수익률공유
//				CellChatRateShare view = new CellChatRateShare(context, item);
//				return view;
                return null;
            }
            case 14: {// 포트폴리오
//				CellChatPotfolio view = new CellChatPotfolio(context, item);
//				return view;
                return null;
            }
            case 15: {// 전체포트폴리오
//				CellChatPotfolio view = new CellChatPotfolio(context, item);
//				return view;
                return null;
            }
            case 16: {// 모임 알림 메시지
                CellChatMoimText view = new CellChatMoimText(context, item);
                return view;
            }

            default: {
                CellChatDefaultText view = new CellChatDefaultText(context, item);
                return view;
            }
        }
    }

    private String unReadCount(String cid) {
        long intCid = 0;
        long totalCount = 0;
        long readCount = 0;

        if (TextUtils.isEmpty(cid)) {
            totalCount = userData.size() - 1;
        } else {
            try {
                intCid = Long.parseLong(cid);
            } catch (Exception e) {
                ErrorController.showError("unReadCount", "Error", e);
            }
            try {
                for (VoUserRead item : userData) {
                    if (!MessengerInfo.getUserId(context).equals(item.getUserId())) {
                        ErrorController.showMessage("스타트 CID : " + item.getStart_cid());
                        if (Long.parseLong(item.getStart_cid()) <= intCid) {
                            totalCount++;
                            ErrorController.showMessage("마지막 CID : " + item.getLast_read_cid());
                            if (Long.parseLong(item.getLast_read_cid()) >= intCid) {
                                readCount++;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                ErrorController.showError("unReadCount", "Error", e);
            }
        }
        return String.valueOf(Math.max(totalCount - readCount, 0));
    }

    public String getFormattedTime(String regDate) {
        String result = "";
        try {
            if (regDate != null && !TextUtils.isEmpty(regDate)) {
                String[] temp = regDate.split(" ");
                String time = temp[1];

                String[] timeTemp = time.split(":");
                String noonText = "";
                int hour = Integer.parseInt(timeTemp[0]);
                int minute = Integer.parseInt(timeTemp[1]);
                String hourString = "";
                String minuteString = "";
                if (hour > 12) {
                    noonText = "오후";
                    hour = hour - 12;
                    hourString = hour + "";
                    if (hour < 10)
                        hourString = "0" + hour;
                } else {
                    noonText = "오전";
                    if (hour < 10)
                        hourString = "0" + hour;
                }

                minuteString = minute + "";
                if (minute < 10) {
                    minuteString = "0" + minute;
                }
                result = noonText + " " + hourString + ":" + minuteString;
            } else {
                result = "오전 00:00";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
