package com.wave.messenger.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wave.messenger.activity.Activity_MemberApplicant;
import com.wave.messenger.activity.Activity_MoimMemberInfoList;
import com.wave.massenger.piggy.R;
import com.wave.messenger.dialog.Dialog_MemberApply;
import com.wave.messenger.util.Constants;
import com.wave.messenger.util.Util;
import com.wave.messenger.utility.Moa;
import com.wave.messenger.vo.VoMoimMemberList;

import java.util.ArrayList;

import moa.android.api.MOAData;
import moa.android.api.MOALog;
import moa.android.api.protocol.PacketTypes;

/**
 * Created by yunsu on 2017. 6. 12..
 * 가입신청자 어댑터
 */
public class ApplicantRecyclerAdapter extends RecyclerView.Adapter<ApplicantRecyclerAdapter.ViewHolder> {

    String TAG=this.getClass().getName();
    //ArrayList<VoMoimMemberList> arvalues;
    ArrayList<VoMoimMemberList.VoMoimMemberlistItem> arvalues= new ArrayList<>();
    Context context;

    /*public ApplicantRecyclerAdapter(Context con, ArrayList<VoMoimMember> values) {
        arvalues = values;
        context = con;
    }*/

    public ApplicantRecyclerAdapter(Context con) {
        context = con;
    }

    public void setItem(ArrayList<VoMoimMemberList.VoMoimMemberlistItem> params) {

        arvalues= new ArrayList<>(params);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public TextView tvDate;
       // public TextView tvProfile;
       // public TextView tvPhone;
        public ImageView imgvProfile;
        public TextView tvApply;
        public LinearLayout llItem;

        public ViewHolder(View v) {

            super(v);
            tvName = (TextView) v.findViewById(R.id.tv_name);
            tvDate= (TextView) v.findViewById(R.id.tv_date);
            imgvProfile= (ImageView) v.findViewById(R.id.imgv_profile);
            tvApply = (TextView) v.findViewById(R.id.tv_apply);
            llItem = (LinearLayout) v.findViewById(R.id.ll_item);


        }
    }

    @Override
    public ApplicantRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_member_applicant, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Vholder, final int position) {

        arvalues.get(position).setnNum(position);

        Vholder.tvName.setText(arvalues.get(position).getUsNm());

        Vholder.tvDate.setText(context.getString(R.string.date_apply, arvalues.get(position).getRegDt()));

        /*if(arvalues.get(position).getPfImgTy().equals("1")){
            Glide.with(context).load(arvalues.get(position).getPfImg()).diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_profile_big_default_android).dontAnimate().into(Vholder.imgvProfile);
        }*/

        //Util.setGlideProfile(context, arvalues.get(position).getUserId(), Vholder.imgvProfile);
        Util.setMoimProfileGlide2(context, arvalues.get(position).getUserId(), Vholder.imgvProfile);


        Vholder.tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showApplyDialog(arvalues.get(position),position);
            }
        });

    }

    /**
     * 신청자  다이얼로그
     * @param voMoimMemberList
     * @param position
     */
    private void showApplyDialog(final VoMoimMemberList.VoMoimMemberlistItem voMoimMemberList, int position) {
        final Dialog_MemberApply mDialog = new Dialog_MemberApply(context, voMoimMemberList, position);
        mDialog.show();

        mDialog.setRejectListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 가입거절 처리

                Log.e("showApplyDialog" , v.getId()+"");

                moimAllowRequest(voMoimMemberList.getUserId(),"0");
                //removeAt(v.getId());
                mDialog.dismiss();
            }
        });

        mDialog.setAcceptListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("showApplyDialog" , v.getId()+"");

                //TODO 가입수락 처리
                moimAllowRequest(voMoimMemberList.getUserId(),"1");

                //removeAt(v.getId());
                mDialog.dismiss();
            }
        });
    }


    @Override
    public int getItemCount() {

        return arvalues.size();
    }

    public void removeAt(int position) {

        arvalues.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arvalues.size());
    }

    /**
     *
      7.14 모임 가입 승인/거절
      alwUsr     승인받는 사용자 아이디
      alw     가입승인여부     0:가입승인거부, 1:가입승인
     */
    private void moimAllowRequest(String alwUsr, String alw) {
        MOALog.w(TAG+" requestMoimProfile");
        Moa.moimAllowRequest(context, alwUsr, alw,  mMainActivityHandler);
    }

    //모임결과 처리 핸들러
    private Handler mMainActivityHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            try {
                //boolean result = false;
                MOAData data = (MOAData) msg.obj;
                MOALog.e("PTC_IMS handleMessage : "+data.body.toString());
                Log.e(TAG,"data.ptc  "+data.ptc);

                String strResult;
                switch (data.ptc) {

                    case PacketTypes.PTC_IMS_MOIM_ALLOW :    //모임 가입 승인/거절
                        strResult = (String) data.body.get("result");
                        Log.e(TAG,"strResult: "+strResult);
                        if(strResult.equals(Constants.SUCCESS)){
                            Log.e(TAG,"params: "+data.body.get("params").toString());

                            try {

                                String alwUsr = data.body.getJson("params").getString("alwUsr");

                                //String alwUsr = (String) data.body.get("alwUsr");
                                removeAlwUsr(alwUsr);

                                Activity_MoimMemberInfoList.activity.bRestart=true; //멤버리스트 onRestart할때 리스트 다시 가저오기

                                //((Activity_MemberApplicant)context).itemRefresh();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.e(TAG,"PacketTypes.PTC_IMS_MOIM_ALLOW  Error");
                        }
                        ((Activity_MemberApplicant)context).itemRefresh(); //TODO 성공이든 아니든 새로고침;
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    });

    /**
     * 승인거절 결과
     * 사용자를 리스트에서 삭제한다
     * @param alwUsr
     */
    private void removeAlwUsr(String alwUsr) {
        Log.e(TAG,"removeAlwUsr alwUsr:"+alwUsr);

        for (VoMoimMemberList.VoMoimMemberlistItem  item: arvalues) {

            //TODO 사용자 찾아서 삭제 후 새로고침
            if(item.getUserId().equals(alwUsr)){

                removeAt(item.getnNum());

                break;
            }
        }
    }
}