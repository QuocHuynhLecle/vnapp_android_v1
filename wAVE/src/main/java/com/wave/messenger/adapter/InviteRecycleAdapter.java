package com.wave.messenger.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.wave.massenger.piggy.R;
import com.wave.messenger.fragment.dummy.TestInviteItem;

import java.util.ArrayList;

/**
 * 멤버초대하기 어댑터
 */
public final class InviteRecycleAdapter extends RecyclerView.Adapter<InviteRecycleAdapter.ViewHolder> { //extends FilterRecycleAdapter<TestInviteItem> {



    private ArrayList<TestInviteItem> mFriendBeanList;
    Context mContext;
    String mType;
    private ArrayList<TestInviteItem> item;


    public InviteRecycleAdapter(Activity context) {
        super();
        //super(friendBeanList);
        //this.mFriendBeanList = friendBeanList;
        this.mContext = context;

    }

    public void setItem(ArrayList<TestInviteItem> friendBeanList) {
        this.mFriendBeanList = friendBeanList;
    }

    /**
     * 검색한 아이템을 넣는다
     * @param item
     */
    /*public void setItem(ArrayList<TestInviteItem> item) {
        this.mFriendBeanList =  item;
        notifyDataSetChanged();
    }*/

    /*public Item_LeaderProfile getSelectedList() {

        ArrayList<Item_LeaderProfile> arSelectedFriend= new ArrayList<>();

        for (Item_LeaderProfile FriendBean : mFriendBeanList) {
            if (FriendBean.isSelected()) {
                return FriendBean;

            }
        }
        return null;
    }*/


    /**
     * 선택된 연락처 리턴
     */
    public ArrayList<TestInviteItem> getSelectedList() {

        ArrayList<TestInviteItem> arSelectedFriend = new ArrayList<>();

        for (TestInviteItem item : mFriendBeanList) {
            if (item.isSelected()) {
                arSelectedFriend.add(item);

            }
        }
        //mSelectAllCb.setChecked(isSelectAll);
        return arSelectedFriend;
    }

    /**
     *
     * @return
     */
    public ArrayList<TestInviteItem> getList() {
        return mFriendBeanList;
    }

    /*@Override
    public RecyclerView.ViewHolderItem onCreateViewHolder(ViewGroup viewGroup, int i) {
        //View convertView = parent.inflate(R.layout.listrow_friend, viewGroup, false);
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_invite, viewGroup, false);
        return new InviteRecycleAdapter.ViewHolderItem(v);
    }*/

    @Override
    public InviteRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(mContext).inflate(R.layout.item_invite, parent, false);
        InviteRecycleAdapter.ViewHolder viewHolder1 = new InviteRecycleAdapter.ViewHolder(view1);
        return viewHolder1;
    }


    @Override
    public void onBindViewHolder(InviteRecycleAdapter.ViewHolder viewHolder, final int position) {

        final InviteRecycleAdapter.ViewHolder holder = (InviteRecycleAdapter.ViewHolder) viewHolder;
        //TestInviteItem item = (TestInviteItem) getItem(position);

        TestInviteItem item = mFriendBeanList.get(position);


        holder.tvName.setText(item.getName());
        holder.tvPhone.setText(item.getPhone());
        holder.checkBox.setChecked(item.isSelected());
        holder.checkBox.setTag(position);
        holder.ll_invite.setTag(position);


        if (null != item.getPhoto()) {
            Glide.with(mContext).load(item.getPhoto()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imgvProfile);
        } else {
            Glide.with(mContext).load(R.drawable.ic_profile_big_default_android).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imgvProfile);
        }


        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                int position = (Integer) v.getTag();
                //TestInviteItem FriendBean = (TestInviteItem) getItem(position);
                TestInviteItem testInviteItem = mFriendBeanList.get(position);
                testInviteItem.setSelected(cb.isChecked());
                notifyDataSetChanged();
            }


        });

        holder.ll_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int position = (Integer) v.getTag();
//                CheckBox cb = (CheckBox) v.findViewById(R.id.checkBox);
//
//                TestInviteItem FriendBean = (TestInviteItem) getItem(position);
//                FriendBean.setSelected(cb.isChecked());
//                notifyDataSetChanged();
//                holder.checkBox.setOnClickListener(this);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFriendBeanList.size();
    }


    /**
     * 체크 모두 false로
     */
    /*private void deselectAllitem() {
        for (Item_LeaderProfile FriendBean : mFriendBeanList) {
            if (FriendBean.isSelected()) {
                FriendBean.setSelected(false);
            }
        }
    }*/
   /* public void filter(String charText) {
        Log.i("TEST charText", charText);
        mSearchFriendBeanList=new ArrayList<>();
            for (TestInviteItem potion : mFriendBeanList) {
//                String name = mContext.getResources().getString(Integer.parseInt(potion.getName()));
                if (potion.getName().contains(charText)) {
                    Log.i("TEST", "potion getName : " + potion.getName()+" "+charText);
                    mSearchFriendBeanList.add(potion);
                }
            }


        notifyDataSetChanged();
    }*/


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPhone;
        public ImageView imgvProfile;
        public CheckBox checkBox;
        public LinearLayout ll_invite;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_phone);
            imgvProfile = (ImageView) itemView.findViewById(R.id.imgv_profile);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            ll_invite = (LinearLayout) itemView.findViewById(R.id.ll_invite);
        }
    }



    /*public static boolean CheckPermission(Activity context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            //퍼미션 거부시 다시 요청
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.CAMERA)) {

                Log.e(context.getLocalClassName(), "Should we show an explanation?");
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_CAMERA);


            } else {
                // 퍼미션없을때 처리
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_REQUEST_CAMERA);
                Log.e(context.getLocalClassName(), "No explanation needed, we can request the permission");

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.

                *//*ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constants.MY_PERMISSIONS_READ_EXTERNAL_STORAGE);*//*

            }
        } else {
            return true;
        }

        return false;
    }*/



    /**
     * 주소록의 사진 번호로 비트맵을 가져온다
     *
     * @param imageDataRow
     * @return
     */
    private Bitmap queryContactImage(int imageDataRow) {
        Cursor c = mContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{
                ContactsContract.CommonDataKinds.Photo.PHOTO
        }, ContactsContract.Data._ID + "=?", new String[]{
                Integer.toString(imageDataRow)
        }, null);
        byte[] imageBytes = null;
        if (c != null) {
            if (c.moveToFirst()) {
                imageBytes = c.getBlob(0);
            }
            c.close();
        }

        if (imageBytes != null) {
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        } else {
            return null;
        }
    }
}
