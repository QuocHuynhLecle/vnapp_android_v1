package com.wave.messenger.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wave.massenger.piggy.R;
import com.wave.messenger.mvp.Profile.DataModel.ThumbnailBitmap;
import com.wave.messenger.util.Constants;
import com.wave.messenger.utility.ErrorController;
import com.wave.messenger.utility.Statics;
import com.wave.messenger.vo.VoFriendList;

import java.io.BufferedInputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 2016-09-05.
 */
public class InviteHorizontaListAdapter extends RecyclerView.Adapter<InviteHorizontaListAdapter.InviteViewHolder> {

    private OnXBtnClickedListener listener;
    private List<VoFriendList> data = Collections.emptyList();
    private Context context;

    public InviteHorizontaListAdapter(OnXBtnClickedListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.data = new ArrayList<>();
    }

    public void add(VoFriendList friend){
        data.add(friend);
        notifyDataSetChanged();
    }

    public void remove(VoFriendList friend){
        if(data!=null && data.size()>0){
            try{
                data.remove(friend);
                notifyDataSetChanged();
            }catch (Exception e){}
        }
    }

    @Override
    public InviteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_invitefriend, parent, false);
        return new InviteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InviteViewHolder holder, int position) {
        VoFriendList item = data.get(position);
        holder.tvName.setText(item.getUserName());
        if(item.isMember())
         loadUserPicture(item.getUserId(), holder.ivProfile);
        else
            holder.ivProfile.setImageResource(R.drawable.ic_list_friend_profile);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class InviteViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivProfile, ivX;
        public TextView tvName;

        public InviteViewHolder(View itemView) {
            super(itemView);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            ivX = (ImageView) itemView.findViewById(R.id.ivX);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivX.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onXClicked(data.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }//end of inner class

    public interface OnXBtnClickedListener{
        void onXClicked(VoFriendList friend, int position);
    }

    //프사 가져오기
    public void loadUserPicture(String userId, ImageView iv){
        ThumbnailBitmap bm = Statics.getProfileImageCache().get(userId);

        if(bm!=null && !bm.isChanged()){
            try{
                iv.setImageBitmap(bm.getThumbnail());
            }catch (Exception e){

            }
        }else
            new ImageDownloader(iv).execute("3", userId);
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        private String address = Constants.CHATDAWN_PROC + "?";
        private WeakReference<ImageView> wiv;
        public ImageDownloader(ImageView iv) {
            this.wiv = new WeakReference<ImageView>(iv);
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            try {
                String fullAddress = address + "type=" + params[0] + "&userid="
                        + params[1];
                ErrorController.showMessage("FULL ADDRESS : " + fullAddress);

                URL url = new URL(fullAddress);
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(
                        conn.getInputStream());
                Bitmap image = BitmapFactory.decodeStream(bis);
                bis.close();
                ErrorController.showMessage("end of doinBg");
                Statics.getProfileImageCache().put(params[1], new ThumbnailBitmap(image, false));
                return image;
            } catch (Exception e) {
                ErrorController.showMessage("[ProfileModel] downloadImage : error");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            try {
                if(wiv != null &&result == null){
                    ImageView iv = wiv.get();
                    iv.setImageResource(R.drawable.ic_default_profile);
                }

                if (wiv != null && result != null) {
                    ImageView iv = wiv.get();
                    iv.setImageBitmap(result);
                }// end of if
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
