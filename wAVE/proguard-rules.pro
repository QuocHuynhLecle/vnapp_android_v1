# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/apple/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


-dontwarn io.netty.channel.**
-dontwarn io.netty.handler.**
-dontwarn io.netty.util.internal.LongAdderCounter.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper$Adapter.**
-dontwarn io.netty.util.internal.**
-dontwarn timber.log.Timber.**
-dontwarn uk.co.senab.photoview.**

-dontwarn io.netty.resolver.dns.DefaultDnsServerAddressStreamProvider.**
-dontwarn io.netty.channel.sctp.SctpNotificationHandler.**
-dontwarn io.netty.handler.codec.marshalling.ChannelBufferByteInput.**
-dontwarn io.netty.handler.codec.marshalling.ChannelBufferByteOutput.**
-dontwarn io.netty.handler.codec.marshalling.LimitingByteInput.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ClientEngine$1.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ServerEngine$1.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ClientEngine$1.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ServerEngine$1.**
-dontwarn io.netty.handler.ssl.JettyNpnSslEngine$1.**
-dontwarn io.netty.handler.ssl.JettyNpnSslEngine$2.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslClientContext$OpenSslCertificateRequestedCallback.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslContext$AbstractCertificateVerifier.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslServerContext$OpenSslSniHostnameMatcher.**
-dontwarn io.netty.handler.ssl.util.X509TrustManagerWrapper.**
-dontwarn io.netty.util.internal.LongAdderCounter.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabase.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper$Adapter.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseStatement.**
-dontwarn io.netty.channel.rxtx.RxtxChannel.**
-dontwarn io.netty.channel.sctp.DefaultSctpChannelConfig.**
-dontwarn io.netty.channel.sctp.DefaultSctpServerChannelConfig.**
-dontwarn io.netty.channel.sctp.SctpChannel.**
-dontwarn io.netty.channel.sctp.SctpChannelConfig.**
-dontwarn io.netty.channel.sctp.SctpChannelOption.**
-dontwarn io.netty.channel.sctp.SctpMessage.**
-dontwarn io.netty.channel.sctp.SctpNotificationHandler.**
-dontwarn io.netty.channel.sctp.SctpServerChannelConfig.**
-dontwarn io.netty.channel.sctp.nio.NioSctpChannel.**
-dontwarn io.netty.channel.sctp.nio.NioSctpChannel$NioSctpChannelConfig.**
-dontwarn io.netty.channel.sctp.nio.NioSctpServerChannel.**
-dontwarn io.netty.channel.sctp.nio.NioSctpServerChannel$NioSctpServerChannelConfig.**
-dontwarn io.netty.channel.sctp.oio.OioSctpChannel.**
-dontwarn io.netty.channel.sctp.oio.OioSctpChannel$OioSctpChannelConfig.**
-dontwarn io.netty.channel.sctp.oio.OioSctpServerChannel.**
-dontwarn io.netty.channel.sctp.oio.OioSctpServerChannel$OioSctpServerChannelConfig.**
-dontwarn io.netty.channel.socket.nio.NioDatagramChannel.**
-dontwarn io.netty.channel.socket.nio.NioServerSocketChannel.**
-dontwarn io.netty.channel.socket.nio.NioSocketChannel.**
-dontwarn io.netty.channel.socket.nio.ProtocolFamilyConverter.**
-dontwarn io.netty.channel.udt.DefaultUdtChannelConfig.**
-dontwarn io.netty.channel.udt.DefaultUdtServerChannelConfig.**
-dontwarn io.netty.channel.udt.nio.NioUdtAcceptorChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtByteAcceptorChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtByteConnectorChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtByteRendezvousChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtMessageAcceptorChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtMessageConnectorChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtMessageConnectorChannel$1.**
-dontwarn io.netty.channel.udt.nio.NioUdtMessageConnectorChannel$2.**
-dontwarn io.netty.channel.udt.nio.NioUdtMessageRendezvousChannel.**
-dontwarn io.netty.channel.udt.nio.NioUdtProvider.**
-dontwarn io.netty.channel.udt.nio.NioUdtProvider$1.**
-dontwarn io.netty.handler.codec.compression.JZlibDecoder.**
-dontwarn io.netty.handler.codec.compression.JZlibEncoder.**
-dontwarn io.netty.handler.codec.compression.Lz4FrameDecoder.**
-dontwarn io.netty.handler.codec.compression.Lz4FrameEncoder.**
-dontwarn io.netty.handler.codec.compression.LzfDecoder.**
-dontwarn io.netty.handler.codec.compression.LzfEncoder.**
-dontwarn io.netty.handler.codec.compression.LzmaFrameEncoder.**
-dontwarn io.netty.handler.codec.compression.ZlibUtil.**
-dontwarn io.netty.handler.codec.marshalling.ChannelBufferByteInput.**
-dontwarn io.netty.handler.codec.marshalling.ChannelBufferByteOutput.**
-dontwarn io.netty.handler.codec.marshalling.CompatibleMarshallingDecoder.**
-dontwarn io.netty.handler.codec.marshalling.CompatibleMarshallingEncoder.**
-dontwarn io.netty.handler.codec.marshalling.ContextBoundUnmarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.DefaultMarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.DefaultUnmarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.LimitingByteInput.**
-dontwarn io.netty.handler.codec.marshalling.MarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.MarshallingDecoder.**
-dontwarn io.netty.handler.codec.marshalling.MarshallingEncoder.**
-dontwarn io.netty.handler.codec.marshalling.ThreadLocalMarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.ThreadLocalUnmarshallerProvider.**
-dontwarn io.netty.handler.codec.marshalling.UnmarshallerProvider.**
-dontwarn io.netty.handler.codec.protobuf.ProtobufDecoder.**
-dontwarn io.netty.handler.codec.protobuf.ProtobufEncoder.**
-dontwarn io.netty.handler.codec.protobuf.ProtobufEncoderNano.**
-dontwarn io.netty.handler.codec.spdy.SpdyHeaderBlockJZlibEncoder.**
-dontwarn io.netty.handler.codec.xml.XmlDecoder.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ClientEngine.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ClientEngine$1.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ServerEngine.**
-dontwarn io.netty.handler.ssl.ConscryptAlpnSslEngine$ServerEngine$1.**
-dontwarn io.netty.handler.ssl.Java7SslParametersUtils.**
-dontwarn io.netty.handler.ssl.Java8SslUtils.**
-dontwarn io.netty.handler.ssl.JdkSslEngine.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ClientEngine.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ClientEngine$1.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ServerEngine.**
-dontwarn io.netty.handler.ssl.JettyAlpnSslEngine$ServerEngine$1.**
-dontwarn io.netty.handler.ssl.JettyNpnSslEngine.**
-dontwarn io.netty.handler.ssl.JettyNpnSslEngine$1.**
-dontwarn io.netty.handler.ssl.JettyNpnSslEngine$2.**
-dontwarn io.netty.handler.ssl.OpenSsl.**
-dontwarn io.netty.handler.ssl.OpenSslCertificateException.**
-dontwarn io.netty.handler.ssl.OpenSslKeyMaterialManager.**
-dontwarn io.netty.handler.ssl.OpenSslServerSessionContext.**
-dontwarn io.netty.handler.ssl.OpenSslSessionContext.**
-dontwarn io.netty.handler.ssl.OpenSslSessionStats.**
-dontwarn io.netty.handler.ssl.OpenSslSessionTicketKey.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslClientContext.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslClientContext$ExtendedTrustManagerVerifyCallback.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslContext.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslContext$AbstractCertificateVerifier.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslEngine.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslEngine$OpenSslSession.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslServerContext.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslServerContext$ExtendedTrustManagerVerifyCallback.**
-dontwarn io.netty.handler.ssl.ReferenceCountedOpenSslServerContext$OpenSslSniHostnameMatcher.**
-dontwarn io.netty.handler.ssl.util.OpenJdkSelfSignedCertGenerator.**
-dontwarn io.netty.handler.ssl.util.SimpleTrustManagerFactory$SimpleTrustManagerFactorySpi.**
-dontwarn io.netty.handler.ssl.util.X509TrustManagerWrapper.**
-dontwarn io.netty.resolver.dns.DefaultDnsServerAddressStreamProvider
-dontwarn io.netty.util.internal.LongAdderCounter.**
-dontwarn io.netty.util.internal.NativeLibraryLoader$NoexecVolumeDetector.**
-dontwarn io.netty.util.internal.PlatformDependent0.**
-dontwarn io.netty.util.internal.PlatformDependent0$1.**
-dontwarn io.netty.util.internal.PlatformDependent0$2.**
-dontwarn io.netty.util.internal.PlatformDependent0$3.**
-dontwarn io.netty.util.internal.SocketUtils$4.**
-dontwarn io.netty.util.internal.SocketUtils$6.**
-dontwarn io.netty.util.internal.logging.Slf4JLogger.**
-dontwarn io.netty.util.internal.logging.Slf4JLoggerFactory.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.BaseLinkedQueueConsumerNodeRef.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.BaseLinkedQueueProducerNodeRef.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.BaseMpscLinkedArrayQueue.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.ConcurrentSequencedCircularArrayQueue.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.LinkedQueueNode.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.MpmcArrayQueueConsumerField.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.MpmcArrayQueueProducerField.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueConsumerField.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueHeadLimitField.**
-dontwarn io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueTailField.**
-dontwarn io.netty.util.internal.shaded.org.jctools.util.JvmInfo.**
-dontwarn io.netty.util.internal.shaded.org.jctools.util.UnsafeAccess.**
-dontwarn io.netty.util.internal.shaded.org.jctools.util.UnsafeRefArrayAccess.**
-dontwarn timber.log.Timber.**
-dontwarn io.netty.channel.rxtx.RxtxChannelConfig$Databits.**
-dontwarn io.netty.channel.rxtx.RxtxChannelConfig$Paritybit.**
-dontwarn io.netty.channel.rxtx.RxtxChannelConfig$Stopbits.**
-dontwarn io.netty.channel.udt.nio.NioUdtByteConnectorChannel$1.**
-dontwarn io.netty.channel.udt.nio.NioUdtByteConnectorChannel$2.**
-dontwarn io.netty.handler.codec.protobuf.ProtobufDecoderNano.**
-dontwarn io.netty.handler.ssl.OpenSslClientContext.**
-dontwarn io.netty.handler.ssl.OpenSslServerContext.**
-dontwarn io.netty.handler.ssl.util.BouncyCastleSelfSignedCertGenerator
-dontwarn io.netty.util.internal.logging.Log4J2Logger.**
-dontwarn io.netty.util.internal.logging.Log4J2LoggerFactory.**
-dontwarn io.netty.util.internal.logging.Log4JLogger.**
-dontwarn io.netty.util.internal.logging.Log4JLoggerFactory.**
-dontwarn uk.co.senab.photoview.PhotoViewAttacher.**
-dontwarn uk.co.senab.photoview.gestures.CupcakeGestureDetector.**
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper$Adapter
-dontwarn io.netty.build.checkstyle.NewlineCheck.**
-dontwarn io.netty.build.checkstyle.SuppressionFilter.**
-dontwarn io.netty.build.junit.TimedOutTestsListener
-dontwarn de.greenrobot.dao.database.EncryptedDatabase
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseOpenHelper$Adapter
-dontwarn de.greenrobot.dao.database.EncryptedDatabaseStatement
-dontwarn io.netty.build.checkstyle.NewlineCheck
-dontwarn io.netty.build.checkstyle.SuppressionFilter
-dontwarn io.netty.build.junit.TimedOutTestsListener
-dontwarn timber.log.Timber
-dontwarn org.greenrobot.greendao.database.**


-keep public class com.hanawm.massenger.activity.**
-keep public class * { public protected *; }
-keep public class com.hanawm.massenger.Emoticon.**

-keep class org.apache.commons.lang3.** { *; }
-keep class com.** { *; }
-keep class org.greenrobot.eventbus.** { *; }
-keep class com.bumptech.glide.**{ *; }
-keep class de.greenrobot.dao.**{ *; }
-keep class com.google.gson.**{ *; }
-keep class org.json.simple.**{ *; }
-keep class org.jsoup.**{ *; }
-keep class uk.co.senab.photoview.**{ *; }
-keep class moa.android.api.**{ *; }
-keep class io.netty.**{ *; }
-keep class com.squareup.otto.**{ *; }
-keep class timber.log.**{ *; }
#-keep class com.kevin.wraprecyclerview.**{ *; }



#-libraryjars libs/ccast.jar
#-libraryjars libs/commons-lang2-3.5.jar
#-libraryjars libs/eventbus-3.0.0.jar
#-libraryjars libs/glide-3.7.0.jar
#-libraryjars libs/greendao-encryption-2.2.2.jar
#-libraryjars libs/gson-2.4.jar
#-libraryjars libs/httpclientandroidlib-1.1.2.jar
#-libraryjars libs/json_simple-1.1_custom.jar
#-libraryjars libs/jsoup-1.9.2.jar
#-libraryjars libs/library-1.2.2.jar
#-libraryjars libs/moaandroidapi.jar
#-libraryjars libs/netty-all-4.1.13.Final.jar
#-libraryjars libs/otto-1.3.8.jar
#-libraryjars timber-4.1.2.jar
#-libraryjars wraprecyclerview.jar
